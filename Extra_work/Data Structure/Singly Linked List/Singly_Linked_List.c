#include<stdio.h>
#include<stdlib.h>

#define NO_DATA 0

typedef struct NODE{

    int info;
    struct NODE* next;
}NODE;

NODE* gnHead;

void CreateList(void);
void DisplayList(void);
void InsertNode(void);
int CountOfNodes(void);
int DeleteNode(void);

int main(void){

    int iChoice =0;
    int iNumberOfNodes=0;
    int iCount=0;
    int iDeletedData=0;
    int i=0;

    printf("\nSingly Linked List Application Started.");

    do{
        printf("\n\n~~~~SLL MENU~~~~");

        printf("\n\t1.Create List");
        printf("\n\t2.Display List");
        printf("\n\t3.Length of List");
        printf("\n\t4.Add a Node[At end]");
        printf("\n\t5.Insert node[at any position]");
        printf("\n\t6.Delete node[at any position]");
        printf("\n\t7.Exit SSL application\n\n");

        printf("\n\tEnter your choice:");
        scanf("%d",&iChoice);


        switch(iChoice){

            case 1:
                printf("\n\tEnter Number of node you want to create:");
                scanf("%d",&iNumberOfNodes);
                
                for(i=0;i<iNumberOfNodes;i++)
                {
                    CreateList();
                }

                break;

            case 2:
                DisplayList();

                break;

            case 3:

                iCount=CountOfNodes();
                printf("\n\tNumber of Nodes in list : [%d]",iCount);

                break;

            case 4:

                CreateList();
               
                break;
            
            case 5:    

                InsertNode();

                break;

            case 6:

                iDeletedData=DeleteNode();

                if(iDeletedData==NO_DATA){
                    printf("\n\tCannot Delete the Node");
                }else{
                    printf("\n\tData %d and Node is deleted from list",iDeletedData);
                }

                break;

            case 7:
            default:
                
                printf("\n\nExiting SSL Application.");
                exit(0);
                
        }

    }while(iChoice >0 && iChoice < 8);

    return (0);
}

void CreateList()
{

    int GetDtat(void);

    NODE* nNewNode=(NODE*)malloc(sizeof(NODE));
    NODE* nTempNode=NULL;

    if(nNewNode==NULL){
        printf("\nFailed to create new node");
        exit(0);
    }

    nNewNode->info=GetData();
    nNewNode->next=NULL;

    //jar list cha head node empty ahe tar list empty ahe 
    if(gnHead==NULL){
        //new node jo create jala te head node la assign kel ie list madhe ek node ala 
        gnHead=nNewNode;
    }else{
        //jar head node NULL nasel tar list empty nhi ahe 
        //ani list travel karnya sathi head node chi value temp node la dili
        //karan head node chi value harvali nhi pahije,parat list travel karaych asel tar list chi starting node mahit pahije  
        nTempNode=gnHead;
        //ata list chya shevti new node add karaych ahe
        //tar existing list chya shevti jav lagel
        //list travel karta na current node ,next node cha address hold karto
        while(nTempNode->next!=NULL){
            nTempNode=nTempNode->next;
        }
        //ani jeva NULL yet manje hi apli last node ahe list madhli
        //mag last node chya next madhe new node cha address assign karun new node list chya shevti add kela jato
        nTempNode->next=nNewNode;
    }

    //to avoid dangling pointer
    nTempNode=NULL;
    nNewNode=NULL;

}
int GetData(){
    
    int iData;

    printf("\n\tEnter number:");
    scanf("%d",&iData);
    
    return iData;

}

void DisplayList(void){
    
    NODE* nTempNode=NULL;
    nTempNode=gnHead;

    if(gnHead==NULL)
    {
        printf("\n\t List is empty!!");
        return;
    }

    printf("\n\n\t");

    while(nTempNode!=NULL)
    {
        printf("|%d|->",nTempNode->info);
        nTempNode=nTempNode->next;
    }

    nTempNode=NULL;
    printf("\n\n");

}

int CountOfNodes(void)
{
    
    NODE* nTempNode=NULL;
    nTempNode=gnHead;

    int iTotalCount=0;

    while(nTempNode!=NULL)
    {
        iTotalCount++;
        nTempNode=nTempNode->next;
    }
    nTempNode=NULL;

    return iTotalCount;

}

void InsertNode(void)
{
    NODE* nTempNode=NULL;
    nTempNode=gnHead;
    int iPosition=0,i;

    printf("\n\tAt which position youo want to insert new node:");
    scanf("%d",&iPosition);

    if(gnHead==NULL || (iPosition > CountOfNodes()  || iPosition <= 0))
    {
        printf("\n\tList is empty");
    }else{

        NODE* nNewNode=(NODE*)malloc(sizeof(NODE));
        nNewNode->info=GetData();
        nNewNode->next=NULL;

        if(iPosition==1){

            nNewNode->next=gnHead;
            gnHead=nNewNode;
        
        }else{
            for(i=1;i<iPosition-1;i++)
            {
                nTempNode=nTempNode->next;
            }
            nNewNode->next=nTempNode->next;
            nTempNode->next=nNewNode;
        }
        nNewNode=NULL;
        nTempNode=NULL;
    }
}

int DeleteNode(void){
    int iPosition;
    int iDeletedData=0,i;

    NODE* nTempNode=NULL;
    NODE* nDeleteNode=NULL;

    printf("\n\tWhich node want to delete [position of node in list]:");
    scanf("%d",&iPosition);

    if(gnHead==NULL || (iPosition > CountOfNodes() || iPosition <=0))
    {
        printf("\n\tList is empty");
        return NO_DATA;
    }else{
        for(i=1;i<iPosition-1;i++)
        {
            nTempNode=nTempNode->next;
        }
        iDeletedData=nTempNode->next->info;

        nDeleteNode=nTempNode->next;
        nTempNode->next=nDeleteNode->next;
        nDeleteNode->next=NULL;

        free(nDeleteNode);
    }
    nDeleteNode=NULL;
    nTempNode=NULL;

    return iDeletedData;
}