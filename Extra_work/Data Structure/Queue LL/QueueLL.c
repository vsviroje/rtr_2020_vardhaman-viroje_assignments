#include<stdio.h>
#include<stdlib.h>

typedef int DataType;

typedef struct LINKEDLIST
{
    DataType data;
    struct LINKEDLIST *next;
}Node,*pNode;

typedef struct QUEUE{
    struct LINKEDLIST *front;
    struct LINKEDLIST *rear;
}QNode,*pQNode;

int main()
{
    pQNode CreateQueue(void);
    pNode CreateNode(DataType);
    void EnQueue(pQNode ,pNode );
    void DisplayQueue(pQNode );
    DataType DeQueue(pQNode );
    void DeleteQueue(pQNode queue);

    pQNode Queue=NULL;
    pNode tempNode=NULL;
    int NumElement,i;
    DataType ElementValue;

    Queue=CreateQueue();

    printf("\nEnter the Number of element:\t");
    scanf("%d",&NumElement);

    for(i=0;i<NumElement;i++)
    {
        printf("\nEnter the element value:[%d]:\t",i+1);
        scanf("%d",&ElementValue);

        tempNode=CreateNode(ElementValue);
        EnQueue(Queue,tempNode);

    }

    DisplayQueue(Queue);
    
    ElementValue=DeQueue(Queue);
    printf("\nDeleted the element value:[%d]",ElementValue);
    
    DisplayQueue(Queue);

    DeleteQueue(Queue);

    DisplayQueue(Queue);

    return 0;   
}

pQNode CreateQueue(void)
{
    pQNode NewQueue=NULL;
    NewQueue=(pQNode)malloc(sizeof(pQNode));

    if(NewQueue==NULL)
    {
        printf("\nFailed to create Queue");
        exit(0);
    }

    NewQueue->front=NewQueue->rear=NULL;

    return NewQueue;
}

pNode CreateNode(DataType data)
{
    pNode NewNode=NULL;
    NewNode=(pNode)malloc(sizeof(Node));

    if(NewNode==NULL)
    {
        printf("\nFailed to create node");
        exit(0);
    }
    NewNode->data=data;
    NewNode->next=NULL;

    return NewNode;
}

void EnQueue(pQNode queue,pNode NewNode)
{
    if( NewNode == NULL)
    {
        printf("\nFailed because node  is empty");
        exit(0);
    }

    if(queue->rear!=NULL)
    {
        queue->rear->next=NewNode;
    }

    queue->rear=NewNode;

    if(queue->front==NULL)
    {
        queue->front= queue->rear;
    }
}

void DisplayQueue(pQNode queue)
{
    int isEmptyQueue(pQNode);
    
    pNode tempNode=NULL;

    if(isEmptyQueue(queue))
    {
        printf("\nDisplay Failed because queue is empty");
        exit(0);
    }

    tempNode=queue->front;
    printf("\nDisplaying all queues elements\n");
    
    while(tempNode!=NULL)
    {
        printf("[%d]->",tempNode->data);
        tempNode=tempNode->next;
    }
}

int isEmptyQueue(pQNode queue)
{
    return (queue->front==NULL);
}

DataType DeQueue(pQNode queue)
{
    int isEmptyQueue(pQNode);
    
    pNode tempNode=NULL;
    DataType data;

    if(isEmptyQueue(queue))
    {
        printf("\nDelete Failed because queue is empty");
        exit(0);
    }

    tempNode=queue->front;
    data=tempNode->data;
    
    queue->front=tempNode->next;

    free(tempNode);
    tempNode=NULL;
    
    return data;
}

void DeleteQueue(pQNode queue)
{
    pNode tempNode=NULL;

    printf("\nDeleting all queues elements\n");

    while(queue->front!=NULL)
    {
        tempNode=queue->front;
        queue->front=tempNode->next;
        printf("[%d]->",tempNode->data);
        free(tempNode);
    }
    free(queue);
    tempNode=NULL;
    queue->front=queue->rear=NULL;
}