// Binary Search Tree.cpp : This file contains the 'main' function. Program execution begins and ends there.

#include<stdio.h>
#include<stdlib.h>

typedef struct TREE_NODE {
    int data;
    struct TREE_NODE* right;
    struct TREE_NODE* left;
    struct TREE_NODE* parent;
}TreeNode, *PTreeNode;


PTreeNode rootnode=NULL;

int main() {

    PTreeNode CreateNode(int);
    void TreeInsert(PTreeNode);
    void InorderTreeWalk(PTreeNode);
    void TreeDelete( PTreeNode);
    PTreeNode TreeSearch(PTreeNode, int);


    int i,NumOfNode,NodeValue;
    PTreeNode pTNode = NULL;

    printf("\nEnter the number of node your going to insert in tree:\n");
    scanf_s("%d", &NumOfNode);

    for (i = 1; i <= NumOfNode; i++)
    {
        printf("\nEnter Data for %d:\t", i);
        scanf_s("%d", &NodeValue);
        TreeInsert(CreateNode(NodeValue));
    }

    InorderTreeWalk(rootnode);

    printf("\nEnter Number that you want to delete from tree:\t");
    scanf_s("%d", &NodeValue);

    pTNode = TreeSearch(rootnode, NodeValue);

    if (pTNode == NULL)
    {
        printf("\nEntered Number :%d that you want to delete from tree is not present.",NodeValue);
        InorderTreeWalk(rootnode);
        exit(0);
    }
    else 
    {
        printf("\nEntered Number :%d that you want to delete from tree is present will start deleting.", pTNode->data);

    }

    TreeDelete(pTNode);

    InorderTreeWalk(rootnode);


    return 0;
}

PTreeNode CreateNode(int NodeValue)
{
    PTreeNode NewNode;

    NewNode = (PTreeNode)malloc(sizeof(TreeNode));

    if (NewNode == NULL) {
        printf("\nMemory Allocation failed");
        exit(0);
    }

    NewNode->data = NodeValue;
    NewNode->left = NULL;
    NewNode->right = NULL;
    NewNode->parent = NULL;

    return NewNode;
}

void TreeInsert(PTreeNode pNewNode) 
{

    PTreeNode x = rootnode, y = NULL;

    if (pNewNode == NULL) {
        printf("New Node address not recieved.Something is wrong!");
        exit(0);
    }
    
    while (x != NULL) 
    {
        y = x;

        if (pNewNode->data < x->data)
        {
            x = x->left;
        }
        else
        {
            x = x->right;
        }

    }

    pNewNode->parent = y;

    if (y == NULL)
    {
        rootnode = pNewNode;
    }
    else if (pNewNode->data < y->data)
    {
        y->left = pNewNode;
    }
    else 
    {
        y->right = pNewNode;
    }
}

void InorderTreeWalk(PTreeNode pTreeWalk)
{
    if (pTreeWalk != NULL)
    {
        InorderTreeWalk(pTreeWalk->left);
        printf("\n%d", pTreeWalk->data);
        InorderTreeWalk(pTreeWalk->right);
    }
}

void Transaplant(PTreeNode pu, PTreeNode pv)
{

    if (pu->parent == NULL)
    {
        rootnode = pv;
    }
    else if (pu == pu->parent->left)
    {
        pu->parent->left = pv;
    }
    else
    {
        pu->parent->right = pv;
    }
    if( pv != NULL)
    {
        pv->parent = pu->parent;
    }
}

PTreeNode TreeSearch(PTreeNode x,int k)
{
    if (x == NULL || k == x->data)
    {
        return x;
    }
    if (k < x->data) 
    {
        return TreeSearch(x->left, k);
    }
    else 
    {
        return TreeSearch(x->right, k);
    }
}

PTreeNode TreeMinimum(PTreeNode x)
{
    while (x->left != NULL)
    {
        x = x->left;
    }
    return x;
}

PTreeNode TreeMaximum(PTreeNode x)
{
    while (x->right != NULL)
    {
        x = x->right;
    }
    return x;
}

void TreeDelete( PTreeNode pz)
{
    PTreeNode py=NULL,pTemp=pz;

    PTreeNode TreeMinimum(PTreeNode);
    void Transaplant( PTreeNode, PTreeNode);

    if (pz->left==NULL)
    {
        Transaplant( pz, pz->right);
    }
    else if (pz->right == NULL)
    {
        Transaplant( pz, pz->left);
    }
    else
    {
        py = TreeMinimum(pz->right);
        if (py->parent != pz)
        {
            Transaplant( py, py->right);
            py->right = pz->right;
            py->right->parent = py;
        }
        Transaplant( pz, py);
        py->left = pz->left;
        py->left->parent = py;

    }

    free(pTemp);
}