//Global
var canvas = null;
var gl = null;

var canvas_orignal_width;
var canvas_orignal_height;
var bFullscreen=false;

const WebGLMacros={
VSV_ATTRIBUTE_POSITION:0,
VSV_ATTRIBUTE_COLOR:1,
VSV_ATTRIBUTE_NORMAL:2,
VSV_ATTRIBUTE_TEXTURE:3,
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var vao_cube;

var vbo_position;
var vbo_Normal;

var mvpMatrixUniform;
var lKeyPressedUniform;

var perspectiveGraphicProjectionMatrix;

var modelViewMatrixUniform;
var projectionMatrixUniform;

var lightAmbientUniform;
var lightDiffuseUniform;
var lightSpecularUniform;
var lightPositionUniform;

var materialAmbientUniform;
var materialDiffuseUniform;
var materialSpecularUniform;
var materialShininessUniform;

var LKeyPressedUniform;

var LightAmbient= [ 0.1,0.1,0.1,1.0 ];
var LightDefuse = [ 1.0,1.0,1.0,1.0 ];
var LightSpecular = [ 1.0,1.0,1.0,1.0 ];
var LightPosition = [ 100.0,100.0,100.0,1.0 ];

var materialAmbient = [ 0.0,0.0,0.0,1.0 ];
var materilaDefuse = [ 0.5,0.2,0.7,1.0 ];
var materialSpecular = [ 1.0,1.0,1.0,1.0 ];
var materialShininess = 128.0;

var bAnimate ;
var bLights ;

var sphere=null;

var requestAnimationFrame = window.requestAnimationFrame || 
                            window.webkitRequestAnimationFrame ||
                            window.mozRequestAnimationFrame ||
                            window.oRequestAnimationFrame || //opera
                            window.msRequestAnimationFrame ||
                            null;

var cancelAnimationFrame =  window.cancelAnimationFrame ||
                            window.webkitCancelRequestAnimationFrame ||  window.webkitCancelAnimation ||
                            window.mozCancelRequestAnimationFrame ||  window.mozCancelAnimationFrame ||
                            window.oCancelRequestAnimationFrame ||  window.oCancelAnimationFrame ||
                            window.msCancelRequestAnimationFrame ||  window.msCancelAnimationFrame ||
                            null;

function main()
{
    //step 1 : Get canvas from html dom
    //document is inbuild variable/DOM object    
    canvas=document.getElementById("canvasID_vsv");
    if(!canvas)
    {
        console.log("ERROR:Failed to obtain Canvas");
        unitialize();
    }else{
        console.log("INFO:Obtained Canvas Successfully");
    }

    //Step 2 : Retreive the height and width of canvas
    //console is inbuild variable/DOM object 
    console.log("INFO:Canvas width=" + canvas.width + " height=" + canvas.height + "\n" );

    canvas_orignal_width = canvas.width;
    canvas_orignal_height = canvas.height;

    //step 3 : Get drawing webgl2 from the canvas
    gl = canvas.getContext("webgl2");
    if(!gl)
    {
        console.log("ERROR:Failed to obtain webgl2 context");
        unitialize();
    }else{
        console.log("INFO:Obtained webgl2 context Successfully");
    }

    if (requestAnimationFrame == null)
    {
        console.log("ERROR:requestAnimationFrame is null");
        unitialize();
        return
    }

    //step 10 : Add event listener
    //window is inbuild variable/DOM object    
    window.addEventListener("keydown",//EVENT TYPE
                            keyDown,//FUNCTION NAME
                            false);//false-captured delegation/propagation or true-bubble delegation/propagation
    
    window.addEventListener("click",
                            mouseDown,
                            false);

    window.addEventListener("resize",resize,false);

    Init();

    resize();//Warm up resize call

    draw();//Warm up redraw call
}

function toggleFullscreen()
{
    var fullscreen_element =    document.fullscreenElement || 
                                document.webkitFullscreenElement || 
                                document.mozFullScreenElement ||
                                document.msFullscreenElement ||
                                null;

    if(fullscreen_element==null)
    {
        if (canvas.requestFullscreen)
        {   
            canvas.requestFullscreen();

        }else if(canvas.webkitRequestFullscreen)
        {
            canvas.webkitRequestFullscreen();

        }else if(canvas.mozRequestFullScreen)
        {
            canvas.mozRequestFullScreen();

        }else if(canvas.msRequestFullscreen)
        {
            canvas.msRequestFullscreen();
        }

        bFullscreen=true;

    }else{
        if(document.exitFullscreen)
        {
            document.exitFullscreen();

        }else if(document.webkitExitFullscreen)
        {
            document.webkitExitFullScreen();

        }else if(document.mozCancelFullScreen)
        {
            document.mozCancelFullScreen();
            
        }else if(document.msExitFullScreen)
        {
            document.msExitFullScreen();
        }

        bFullscreen=false;
    }
}

function keyDown(event)
{
    console.log(event);
   switch(event.keyCode)
   {
        case 70:
            toggleFullscreen();
            break;
        case 27:
            unitialize();
            window.close();
            break;
        case 65:
            bAnimate = !bAnimate;
            break;
        case 76:
            bLights = !bLights;
            break; 
   }
}

function mouseDown()
{
    console.log("INFO:Mouse is pressed");
}

function Init()
{

    gl.viewportWidth = canvas.width;
    gl.viewportHeight = canvas.height;

	var vertexShaderSourceCode=
		"#version 300 es" +
		"\n" +
		"in vec4 vPosition;\n" +
		"in vec3 vNormal;\n" +
		// "in vec3 vTexCoord;\n" +
		
        "uniform mat4 u_modelViewMatrix;\n" +
		"uniform mat4 u_projectionMatrix;\n" +

		"uniform vec3 u_lightAmbient;\n"+
		"uniform vec3 u_lightDiffuse;\n"+
		"uniform vec3 u_lightSpecular;\n"+
		"uniform vec4 u_lightPosition;\n"+
		
		"uniform vec3 u_materialAmbient;\n"+
		"uniform vec3 u_materialDiffuse;\n"+
		"uniform vec3 u_materialSpecular;\n"+
		"uniform float u_materialShininess;\n"+
		
		"uniform int u_lKeyPressed;\n"+
		
		"out vec3 out_phongADS_Light;\n"+

        "precision highp float;"+
        "precision highp int;"+
		
		"void main(void)\n" +
		"{\n" +
            "if (u_lKeyPressed == 1)\n"+
            "{\n"+

                "vec4 eyeCoord = u_modelViewMatrix * vPosition;\n"+
                
                "vec3 transformedNormal = normalize( mat3(u_modelViewMatrix) * vNormal );\n"+

                "vec3 lightDirection = normalize( vec3( u_lightPosition - eyeCoord ) );\n"+
                
                "vec3 reflectionVector = reflect( -lightDirection, transformedNormal );\n"+
                
                "vec3 viewVector = normalize( -eyeCoord.xyz);\n"+
                
                "vec3 ambient = u_lightAmbient * u_materialAmbient;\n"+
                
                "vec3 diffuse = u_lightDiffuse * u_materialDiffuse * max( dot(lightDirection , transformedNormal), 0.0);\n"+
                
                "vec3 specular = u_lightSpecular * u_materialSpecular * pow( max( dot(reflectionVector , viewVector), 0.0f) , u_materialShininess);\n"+
                
                "out_phongADS_Light= ambient + diffuse + specular;\n"+

            "}\n"+
            "else\n"+
            "{\n"+

                "out_phongADS_Light = vec3(1.0f, 1.0f, 1.0f);\n"+
            
            "}\n"+
        
			"gl_Position = u_projectionMatrix * u_modelViewMatrix * vPosition;\n" +
		"}\n";

    vertexShaderObject=gl.createShader(gl.VERTEX_SHADER);

	gl.shaderSource(vertexShaderObject,vertexShaderSourceCode);

	gl.compileShader(vertexShaderObject);

	if(gl.getShaderParameter(vertexShaderObject,gl.COMPILE_STATUS)==false)
	{
		var error;
        error = gl.getShaderInfoLog(vertexShaderObject);
        if(error.length > 0)
        {
            alert(error);
            unitialize();
            return
        }
    }	

	//---------Fragment--------------

	var fragmentShaderSourceCode=
		"#version 300 es" +
		"\n" +
        
        "precision highp float;"+
        "precision highp int;"+
		
        "out vec4 fragColor;"+
		"in vec3 out_phongADS_Light;\n"+

		"void main(void)" +
		"{"+    
			"fragColor=vec4(out_phongADS_Light, 1.0f);"+
		"}";

    fragmentShaderObject=gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObject,fragmentShaderSourceCode);

	gl.compileShader(fragmentShaderObject);

	if(gl.getShaderParameter(fragmentShaderObject,gl.COMPILE_STATUS)==false)
	{
        var error;
        error = gl.getShaderInfoLog(fragmentShaderObject);
        if(error.length > 0)
        {
            alert(error);
            unitialize();
            return
        }
	}

	//-------------Shader Program---------------------------
	
	shaderProgramObject = gl.createProgram();

	gl.attachShader(shaderProgramObject,vertexShaderObject);
	gl.attachShader(shaderProgramObject,fragmentShaderObject);
	//Pre Link attachment
	gl.bindAttribLocation(shaderProgramObject,WebGLMacros.VSV_ATTRIBUTE_POSITION,"vPosition");
	gl.bindAttribLocation(shaderProgramObject,WebGLMacros.VSV_ATTRIBUTE_NORMAL,"vNormal");
	// gl.bindAttribLocation(shaderProgramObject,WebGLMacros.VSV_ATTRIBUTE_TEXTURE,"vTexCoord");

	gl.linkProgram(shaderProgramObject);

	if(!gl.getProgramParameter(shaderProgramObject,gl.LINK_STATUS))
	{
		var error;
		error = gl.getProgramInfoLog(shaderProgramObject);
        if(error.length > 0)
        {
            alert(error);
            unitialize();
            return
        }		
	}
	//Post Link attachment
	modelViewMatrixUniform = gl.getUniformLocation(shaderProgramObject,"u_modelViewMatrix");
	projectionMatrixUniform = gl.getUniformLocation(shaderProgramObject,"u_projectionMatrix");

    lightAmbientUniform = gl.getUniformLocation(shaderProgramObject,"u_lightAmbient");
	lightDiffuseUniform = gl.getUniformLocation(shaderProgramObject,"u_lightDiffuse");
	lightSpecularUniform = gl.getUniformLocation(shaderProgramObject,"u_lightSpecular");
	lightPositionUniform = gl.getUniformLocation(shaderProgramObject,"u_lightPosition");

	materialAmbientUniform = gl.getUniformLocation(shaderProgramObject,"u_materialAmbient");
	materialDiffuseUniform = gl.getUniformLocation(shaderProgramObject,"u_materialDiffuse");
	materialSpecularUniform = gl.getUniformLocation(shaderProgramObject,"u_materialSpecular");
	materialShininessUniform = gl.getUniformLocation(shaderProgramObject,"u_materialShininess");

	LKeyPressedUniform = gl.getUniformLocation(shaderProgramObject,"u_lKeyPressed");


	//----------------Data prepration-------------------
    ///----vao 
    sphere=new Mesh();
    makeSphere(sphere,2,50,50);
    
	//Initialize ortho matrix
	perspectiveGraphicProjectionMatrix=mat4.create();

	//-----OpenGL state Machine States-----------------------
    gl.clearColor(0.0, 0.0, 0.0, 1.0);//RGBA
	gl.clearDepth(1.0);
	gl.enable(gl.DEPTH_TEST);
	gl.depthFunc(gl.LEQUAL);

}

function resize()
{
    if(bFullscreen==true)
    {
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
    }else{
        canvas.width = canvas_orignal_width;
        canvas.height = canvas_orignal_height;
    }

    gl.viewport(0,0,canvas.width, canvas.height);

    mat4.perspective(perspectiveGraphicProjectionMatrix,
                    45.0,
                    parseFloat(canvas.width)/parseFloat(canvas.height),
                    0.1,
                    100.0);
}

function draw()
{
    var modelViewProjectMatrix = mat4.create();
    var modelViewMatrix = mat4.create();
    
    gl.clear(gl.COLOR_BUFFER_BIT);

	gl.useProgram(shaderProgramObject);

        mat4.translate(modelViewMatrix,modelViewMatrix,[0.0,0.0,-5.0]);
       
		gl.uniformMatrix4fv(modelViewMatrixUniform,
							false,
							modelViewMatrix);
        
        gl.uniformMatrix4fv(projectionMatrixUniform,
                            false,
                            perspectiveGraphicProjectionMatrix);

        if (bLights == true)
		{
			gl.uniform1i(LKeyPressedUniform, 1);

			gl.uniform3fv(lightAmbientUniform, [LightAmbient[0],LightAmbient[1],LightAmbient[2]]);
			gl.uniform3fv(lightDiffuseUniform,  [LightDefuse[0],LightDefuse[1],LightDefuse[2]]);
			gl.uniform3fv(lightSpecularUniform,  [LightSpecular[0],LightSpecular[1],LightSpecular[2]]);
			
			gl.uniform4fv(lightPositionUniform,  LightPosition);//posiitional light

			gl.uniform3fv(materialAmbientUniform,  [materialAmbient[0],materialAmbient[1],materialAmbient[2]]);
			gl.uniform3fv(materialDiffuseUniform,  [materilaDefuse[0],materilaDefuse[1],materilaDefuse[2]]);
			gl.uniform3fv(materialSpecularUniform,  [materialSpecular[0],materialSpecular[1],materialSpecular[2]] );

			gl.uniform1f(materialShininessUniform, materialShininess);
		}
		else
		{
			gl.uniform1i(LKeyPressedUniform, 0);
		}   

        sphere.draw();

	gl.useProgram(null);

    // requestAnimationFrame(update,canvas); //if using update() then use this way
    requestAnimationFrame(draw,canvas);
}

function degToRad(degrees)
{
    return ( degrees*Math.PI /180.0)
}

function unitialize()
{
    sphere.deallocate();

	if(shaderProgramObject)
	{
        if(fragmentShaderObject)
        {
            gl.detachShader(shaderProgramObject,fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject=null;
        }
			
        if(vertexShaderObject)
        {
            gl.detachShader(shaderProgramObject,vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject=null;
        }
          
		gl.deleteProgram(shaderProgramObject);
		shaderProgramObject=null;
	}
}

