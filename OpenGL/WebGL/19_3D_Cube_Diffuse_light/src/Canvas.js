//Global
var canvas = null;
var gl = null;

var canvas_orignal_width;
var canvas_orignal_height;
var bFullscreen=false;

const WebGLMacros={
VSV_ATTRIBUTE_POSITION:0,
VSV_ATTRIBUTE_COLOR:1,
VSV_ATTRIBUTE_NORMAL:2,
VSV_ATTRIBUTE_TEXTURE:3,
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var vao_cube;

var vbo_position;
var vbo_Normal;

var mvpMatrixUniform;
var lKeyPressedUniform;

var perspectiveGraphicProjectionMatrix;

var modelViewMatrixUniform;
var projectionMatrixUniform;
var LKeyPressedUniform;

var LDUniform;//Light diffuse
var KDUniform;//Material diffuse
var lightPositionUniform;

var bAnimate ;
var bLights ;

var requestAnimationFrame = window.requestAnimationFrame || 
                            window.webkitRequestAnimationFrame ||
                            window.mozRequestAnimationFrame ||
                            window.oRequestAnimationFrame || //opera
                            window.msRequestAnimationFrame ||
                            null;

var cancelAnimationFrame =  window.cancelAnimationFrame ||
                            window.webkitCancelRequestAnimationFrame ||  window.webkitCancelAnimation ||
                            window.mozCancelRequestAnimationFrame ||  window.mozCancelAnimationFrame ||
                            window.oCancelRequestAnimationFrame ||  window.oCancelAnimationFrame ||
                            window.msCancelRequestAnimationFrame ||  window.msCancelAnimationFrame ||
                            null;

function main()
{
    //step 1 : Get canvas from html dom
    //document is inbuild variable/DOM object    
    canvas=document.getElementById("canvasID_vsv");
    if(!canvas)
    {
        console.log("ERROR:Failed to obtain Canvas");
        unitialize();
    }else{
        console.log("INFO:Obtained Canvas Successfully");
    }

    //Step 2 : Retreive the height and width of canvas
    //console is inbuild variable/DOM object 
    console.log("INFO:Canvas width=" + canvas.width + " height=" + canvas.height + "\n" );

    canvas_orignal_width = canvas.width;
    canvas_orignal_height = canvas.height;

    //step 3 : Get drawing webgl2 from the canvas
    gl = canvas.getContext("webgl2");
    if(!gl)
    {
        console.log("ERROR:Failed to obtain webgl2 context");
        unitialize();
    }else{
        console.log("INFO:Obtained webgl2 context Successfully");
    }

    if (requestAnimationFrame == null)
    {
        console.log("ERROR:requestAnimationFrame is null");
        unitialize();
        return
    }

    //step 10 : Add event listener
    //window is inbuild variable/DOM object    
    window.addEventListener("keydown",//EVENT TYPE
                            keyDown,//FUNCTION NAME
                            false);//false-captured delegation/propagation or true-bubble delegation/propagation
    
    window.addEventListener("click",
                            mouseDown,
                            false);

    window.addEventListener("resize",resize,false);

    Init();

    resize();//Warm up resize call

    draw();//Warm up redraw call
}

function toggleFullscreen()
{
    var fullscreen_element =    document.fullscreenElement || 
                                document.webkitFullscreenElement || 
                                document.mozFullScreenElement ||
                                document.msFullscreenElement ||
                                null;

    if(fullscreen_element==null)
    {
        if (canvas.requestFullscreen)
        {   
            canvas.requestFullscreen();

        }else if(canvas.webkitRequestFullscreen)
        {
            canvas.webkitRequestFullscreen();

        }else if(canvas.mozRequestFullScreen)
        {
            canvas.mozRequestFullScreen();

        }else if(canvas.msRequestFullscreen)
        {
            canvas.msRequestFullscreen();
        }

        bFullscreen=true;

    }else{
        if(document.exitFullscreen)
        {
            document.exitFullscreen();

        }else if(document.webkitExitFullscreen)
        {
            document.webkitExitFullScreen();

        }else if(document.mozCancelFullScreen)
        {
            document.mozCancelFullScreen();
            
        }else if(document.msExitFullScreen)
        {
            document.msExitFullScreen();
        }

        bFullscreen=false;
    }
}

function keyDown(event)
{
    console.log(event);
   switch(event.keyCode)
   {
        case 70:
            toggleFullscreen();
            break;
        case 27:
            unitialize();
            window.close();
            break;
        case 65:
            bAnimate = !bAnimate;
            break;
        case 76:
            bLights = !bLights;
            break; 
   }
}

function mouseDown()
{
    console.log("INFO:Mouse is pressed");
}

function Init()
{
    gl.viewportWidth = canvas.width;
    gl.viewportHeight = canvas.height;

	var vertexShaderSourceCode=
		"#version 300 es" +
		"\n" +
		"in vec4 vPosition;\n" +
		"in vec3 vNormal;\n" +
		"uniform mat4 u_modelViewMatrix;\n" +
		"uniform mat4 u_projectionMatrix;\n" +
		"uniform int u_LKeyPressed;\n" +
		"uniform vec3 u_LD;\n" +
		"uniform vec3 u_KD;\n" +
		"uniform vec4 u_lightPosition;\n" +
		"out vec3 out_diffuseLight;\n" +
		"void main(void)\n" +
		"{\n" +
			"if (u_LKeyPressed == 1)\n" +
			"{\n" +
				"vec4 eyeCoord = u_modelViewMatrix * vPosition;\n" +
				"mat3 normalMatrix = mat3( transpose( inverse( u_modelViewMatrix )));\n" +
				"vec3 transformedNormal = normalize( normalMatrix * vNormal );\n" +
				"vec3 srcLight = normalize( vec3( u_lightPosition - eyeCoord ));\n" +
				"out_diffuseLight = u_LD * u_KD * max( dot(srcLight, transformedNormal), 0.0);\n" +
			"}\n" +
			"gl_Position = u_projectionMatrix * u_modelViewMatrix * vPosition;\n" +
		"}\n";

    vertexShaderObject=gl.createShader(gl.VERTEX_SHADER);

	gl.shaderSource(vertexShaderObject,vertexShaderSourceCode);

	gl.compileShader(vertexShaderObject);

	if(gl.getShaderParameter(vertexShaderObject,gl.COMPILE_STATUS)==false)
	{
		var error;
        error = gl.getShaderInfoLog(vertexShaderObject);
        if(error.length > 0)
        {
            alert(error);
            unitialize();
            return
        }
    }	

	//---------Fragment--------------

	var fragmentShaderSourceCode=
		"#version 300 es" +
		"\n" +
        "precision highp float;"+
        "precision highp int;"+
		
        "in vec3 out_diffuseLight;"+
		"out vec4 fragColor;"+
		"uniform int u_LKeyPressed;"+

		"void main(void)" +
		"{"+
			"vec4 color;"+
			"if (u_LKeyPressed == 1)"+
			"{"+
				"color=vec4( out_diffuseLight, 1.0 );"+
			"}"+
			"else"+
			"{"+
				"color=vec4(1.0f, 1.0f, 1.0f, 1.0f);"+
			"}"+
            
			"fragColor=color;"+
		"}";

    fragmentShaderObject=gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObject,fragmentShaderSourceCode);

	gl.compileShader(fragmentShaderObject);

	if(gl.getShaderParameter(fragmentShaderObject,gl.COMPILE_STATUS)==false)
	{
        var error;
        error = gl.getShaderInfoLog(fragmentShaderObject);
        if(error.length > 0)
        {
            alert(error);
            unitialize();
            return
        }
	}

	//-------------Shader Program---------------------------
	
	shaderProgramObject = gl.createProgram();

	gl.attachShader(shaderProgramObject,vertexShaderObject);
	gl.attachShader(shaderProgramObject,fragmentShaderObject);
	//Pre Link attachment
	gl.bindAttribLocation(shaderProgramObject,WebGLMacros.VSV_ATTRIBUTE_POSITION,"vPosition");
	gl.bindAttribLocation(shaderProgramObject,WebGLMacros.VSV_ATTRIBUTE_NORMAL,"vNormal");

	gl.linkProgram(shaderProgramObject);

	if(!gl.getProgramParameter(shaderProgramObject,gl.LINK_STATUS))
	{
		var error;
		error = gl.getProgramInfoLog(shaderProgramObject);
        if(error.length > 0)
        {
            alert(error);
            unitialize();
            return
        }		
	}
	//Post Link attachment
	modelViewMatrixUniform = gl.getUniformLocation(shaderProgramObject,"u_modelViewMatrix");
	projectionMatrixUniform = gl.getUniformLocation(shaderProgramObject,"u_projectionMatrix");
	
    LKeyPressedUniform = gl.getUniformLocation(shaderProgramObject,"u_LKeyPressed");
	LDUniform = gl.getUniformLocation(shaderProgramObject,"u_LD");
	KDUniform = gl.getUniformLocation(shaderProgramObject,"u_KD");
	lightPositionUniform = gl.getUniformLocation(shaderProgramObject,"u_lightPosition");


	//----------------Data prepration-------------------
	var cubeVertices = new Float32Array([
                                            1.0, 1.0, 1.0,
                                            -1.0, 1.0, 1.0,
                                            -1.0, -1.0, 1.0,
                                            1.0, -1.0, 1.0,

                                            1.0, 1.0, -1.0,
                                            1.0, 1.0, 1.0,
                                            1.0, -1.0, 1.0,
                                            1.0, -1.0, -1.0,

                                            -1.0, 1.0, -1.0,
                                            1.0, 1.0, -1.0,
                                            1.0, -1.0, -1.0,
                                            -1.0, -1.0, -1.0,

                                            -1.0, 1.0, 1.0,
                                            -1.0, 1.0, -1.0,
                                            -1.0, -1.0, -1.0,
                                            -1.0, -1.0, 1.0,

                                            1.0, 1.0, -1.0,
                                            -1.0, 1.0, -1.0,
                                            -1.0, 1.0, 1.0,
                                            1.0, 1.0, 1.0,

                                            1.0, -1.0, -1.0,
                                            -1.0, -1.0, -1.0,
                                            -1.0, -1.0, 1.0,
                                            1.0, -1.0, 1.0
                                            ]);
    
    var cubeNormal = new Float32Array([
                                    0.0, 0.0, 1.0,
                                    0.0, 0.0, 1.0,
                                    0.0, 0.0, 1.0,
                                    0.0, 0.0, 1.0,

                                    1.0, 0.0, 0.0,
                                    1.0, 0.0, 0.0,
                                    1.0, 0.0, 0.0,
                                    1.0, 0.0, 0.0,

                                    0.0, 0.0, -1.0,
                                    0.0, 0.0, -1.0,
                                    0.0, 0.0, -1.0,
                                    0.0, 0.0, -1.0,

                                    -1.0, 0.0, 0.0,
                                    -1.0, 0.0, 0.0,
                                    -1.0, 0.0, 0.0,
                                    -1.0, 0.0, 0.0,

                                    0.0, 1.0, 0.0,
                                    0.0, 1.0, 0.0,
                                    0.0, 1.0, 0.0,
                                    0.0, 1.0, 0.0,

                                    0.0, -1.0, 0.0,
                                    0.0, -1.0, 0.0,
                                    0.0, -1.0, 0.0,
                                    0.0, -1.0, 0.0
                                    ]);
    
    ///----vao cube
    
    vao_cube = gl.createVertexArray();

	gl.bindVertexArray(vao_cube);

        vbo_position=gl.createBuffer();
		
		gl.bindBuffer(gl.ARRAY_BUFFER,
					vbo_position);
		
			gl.bufferData(gl.ARRAY_BUFFER,
                        cubeVertices,
						gl.STATIC_DRAW);

			gl.vertexAttribPointer(WebGLMacros.VSV_ATTRIBUTE_POSITION,
								3,
								gl.FLOAT,
								false,
								0,
								0);
			
			gl.enableVertexAttribArray(WebGLMacros.VSV_ATTRIBUTE_POSITION);

		gl.bindBuffer(gl.ARRAY_BUFFER,
					null);

        vbo_Normal=gl.createBuffer();
		
            gl.bindBuffer(gl.ARRAY_BUFFER,
                vbo_Normal);
                    
                gl.bufferData(gl.ARRAY_BUFFER,
                    cubeNormal,
                    gl.STATIC_DRAW);

                gl.vertexAttribPointer(WebGLMacros.VSV_ATTRIBUTE_NORMAL,
                                    3,
                                    gl.FLOAT,
                                    false,
                                    0,
                                    0);
        
                gl.enableVertexAttribArray(WebGLMacros.VSV_ATTRIBUTE_NORMAL);

            gl.bindBuffer(gl.ARRAY_BUFFER,
                                null);

	gl.bindVertexArray(null);

    
    bAnimate =false;
    bLights =false;


	//Initialize ortho matrix
	perspectiveGraphicProjectionMatrix=mat4.create();

	//-----OpenGL state Machine States-----------------------
    gl.clearColor(0.0, 0.0, 0.0, 1.0);//RGBA
	gl.clearDepth(1.0);
	gl.enable(gl.DEPTH_TEST);
	gl.depthFunc(gl.LEQUAL);

}

function resize()
{
    if(bFullscreen==true)
    {
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
    }else{
        canvas.width = canvas_orignal_width;
        canvas.height = canvas_orignal_height;
    }

    gl.viewport(0,0,canvas.width, canvas.height);

    mat4.perspective(perspectiveGraphicProjectionMatrix,
                    45.0,
                    parseFloat(canvas.width)/parseFloat(canvas.height),
                    0.1,
                    100.0);
}
var Angle=0;
function draw()
{
    var modelViewProjectMatrix = mat4.create();
    var modelViewMatrix = mat4.create();
    
    gl.clear(gl.COLOR_BUFFER_BIT);

	gl.useProgram(shaderProgramObject);

        if (bLights == true)
        {
            gl.uniform1i(LKeyPressedUniform, 1);

            gl.uniform3f(LDUniform, 1.0, 1.0, 1.0);

            gl.uniform3f(KDUniform, 0.5, 0.5, 0.5);

            var lightPosition = [ 0.0,0.0,2.0,1.0];//4 th element defines Positional light

            gl.uniform4fv(lightPositionUniform,
                        lightPosition);
        }
        else 
        {
            gl.uniform1i(LKeyPressedUniform, 0);
        }

        //cube
        mat4.translate(modelViewMatrix,modelViewMatrix,[0.0,0.0,-6.0]);
        mat4.rotateX(modelViewMatrix,modelViewMatrix,degToRad(Angle));
        mat4.rotateY(modelViewMatrix,modelViewMatrix,degToRad(Angle));
        mat4.rotateZ(modelViewMatrix,modelViewMatrix,degToRad(Angle));

		// mat4.multiply(modelViewProjectMatrix,
        //             perspectiveGraphicProjectionMatrix ,
        //             modelViewMatrix);

		gl.uniformMatrix4fv(modelViewMatrixUniform,
							false,
							modelViewMatrix);
        
        gl.uniformMatrix4fv(projectionMatrixUniform,
                            false,
                            perspectiveGraphicProjectionMatrix);
		
		gl.bindVertexArray(vao_cube);

            gl.drawArrays(gl.TRIANGLE_FAN,0,4);
            gl.drawArrays(gl.TRIANGLE_FAN,4,4);
            gl.drawArrays(gl.TRIANGLE_FAN,8,4);
            gl.drawArrays(gl.TRIANGLE_FAN,12,4);
            gl.drawArrays(gl.TRIANGLE_FAN,16,4);
            gl.drawArrays(gl.TRIANGLE_FAN,20,4);

		gl.bindVertexArray(null);

	gl.useProgram(null);

    if (bAnimate == true)
	{
        Angle+=3;
	}
   
    // requestAnimationFrame(update,canvas); //if using update() then use this way
    requestAnimationFrame(draw,canvas);
}

function degToRad(degrees)
{
    return ( degrees*Math.PI /180.0)
}

function unitialize()
{
    if(vao_cube)
	{
		gl.deleteVertexArray(vao_cube);
		vao_cube=null;
	}

	if(vbo_position)
	{
		gl.deleteBuffer(vbo_position);
		vbo_position=null;
	}

    if(vbo_Normal)
	{
		gl.deleteBuffer(vbo_Normal);
		vbo_Normal=null;
	}


	if(shaderProgramObject)
	{
        if(fragmentShaderObject)
        {
            gl.detachShader(shaderProgramObject,fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject=null;
        }
			
        if(vertexShaderObject)
        {
            gl.detachShader(shaderProgramObject,vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject=null;
        }
          
		gl.deleteProgram(shaderProgramObject);
		shaderProgramObject=null;
	}
}

