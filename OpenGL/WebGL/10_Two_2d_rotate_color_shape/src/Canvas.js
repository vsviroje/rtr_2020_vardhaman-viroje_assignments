//Global
var canvas = null;
var gl = null;

var canvas_orignal_width;
var canvas_orignal_height;
var bFullscreen=false;

const WebGLMacros={
VSV_ATTRIBUTE_POSITION:0,
VSV_ATTRIBUTE_COLOR:1,
VSV_ATTRIBUTE_NORMAL:2,
VSV_ATTRIBUTE_TEXTURE:3,
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var vao_triangle;
var vao_rectangle;

var vbo_position;
var vbo_color;
var mvpMatrixUniform;

var perspectiveGraphicProjectionMatrix;


var requestAnimationFrame = window.requestAnimationFrame || 
                            window.webkitRequestAnimationFrame ||
                            window.mozRequestAnimationFrame ||
                            window.oRequestAnimationFrame || //opera
                            window.msRequestAnimationFrame ||
                            null;

var cancelAnimationFrame =  window.cancelAnimationFrame ||
                            window.webkitCancelRequestAnimationFrame ||  window.webkitCancelAnimation ||
                            window.mozCancelRequestAnimationFrame ||  window.mozCancelAnimationFrame ||
                            window.oCancelRequestAnimationFrame ||  window.oCancelAnimationFrame ||
                            window.msCancelRequestAnimationFrame ||  window.msCancelAnimationFrame ||
                            null;

function main()
{
    //step 1 : Get canvas from html dom
    //document is inbuild variable/DOM object    
    canvas=document.getElementById("canvasID_vsv");
    if(!canvas)
    {
        console.log("ERROR:Failed to obtain Canvas");
        unitialize();
    }else{
        console.log("INFO:Obtained Canvas Successfully");
    }

    //Step 2 : Retreive the height and width of canvas
    //console is inbuild variable/DOM object 
    console.log("INFO:Canvas width=" + canvas.width + " height=" + canvas.height + "\n" );

    canvas_orignal_width = canvas.width;
    canvas_orignal_height = canvas.height;

    //step 3 : Get drawing webgl2 from the canvas
    gl = canvas.getContext("webgl2");
    if(!gl)
    {
        console.log("ERROR:Failed to obtain webgl2 context");
        unitialize();
    }else{
        console.log("INFO:Obtained webgl2 context Successfully");
    }

    if (requestAnimationFrame == null)
    {
        console.log("ERROR:requestAnimationFrame is null");
        unitialize();
        return
    }

    //step 10 : Add event listener
    //window is inbuild variable/DOM object    
    window.addEventListener("keydown",//EVENT TYPE
                            keyDown,//FUNCTION NAME
                            false);//false-captured delegation/propagation or true-bubble delegation/propagation
    
    window.addEventListener("click",
                            mouseDown,
                            false);

    window.addEventListener("resize",resize,false);

    Init();

    resize();//Warm up resize call

    draw();//Warm up redraw call
}

function toggleFullscreen()
{
    var fullscreen_element =    document.fullscreenElement || 
                                document.webkitFullscreenElement || 
                                document.mozFullScreenElement ||
                                document.msFullscreenElement ||
                                null;

    if(fullscreen_element==null)
    {
        if (canvas.requestFullscreen)
        {   
            canvas.requestFullscreen();

        }else if(canvas.webkitRequestFullscreen)
        {
            canvas.webkitRequestFullscreen();

        }else if(canvas.mozRequestFullScreen)
        {
            canvas.mozRequestFullScreen();

        }else if(canvas.msRequestFullscreen)
        {
            canvas.msRequestFullscreen();
        }

        bFullscreen=true;

    }else{
        if(document.exitFullscreen)
        {
            document.exitFullscreen();

        }else if(document.webkitExitFullscreen)
        {
            document.webkitExitFullScreen();

        }else if(document.mozCancelFullScreen)
        {
            document.mozCancelFullScreen();
            
        }else if(document.msExitFullScreen)
        {
            document.msExitFullScreen();
        }

        bFullscreen=false;
    }
}

function keyDown(event)
{
    console.log(event);
   switch(event.keyCode)
   {
        case 70:
            toggleFullscreen();
            break;
        case 27:
                unitialize();
                window.close();
                break;
   }
}

function mouseDown()
{
    console.log("INFO:Mouse is pressed");
}

function Init()
{
    gl.viewportWidth = canvas.width;
    gl.viewportHeight = canvas.height;

	var vertexShaderSourceCode=
		"#version 300 es" +
		"\n" +
		"in vec4 vPosition;" +
		"in vec4 vColor;" +
		"out vec4 out_color;" +
		"uniform mat4 u_mvpMatrix;" +
		"void main(void)" +
		"{" +
            "out_color=vColor;"+
			"gl_Position=u_mvpMatrix * vPosition;" +
		"}";

    vertexShaderObject=gl.createShader(gl.VERTEX_SHADER);

	gl.shaderSource(vertexShaderObject,vertexShaderSourceCode);

	gl.compileShader(vertexShaderObject);

	if(gl.getShaderParameter(vertexShaderObject,gl.COMPILE_STATUS)==false)
	{
		var error;
        error = gl.getShaderInfoLog(vertexShaderObject);
        if(error.length > 0)
        {
            alert(error);
            unitialize();
            return
        }
    }	

	//---------Fragment--------------

	var fragmentShaderSourceCode=
		"#version 300 es" +
		"\n" +
        "precision highp float;"+
		"in vec4 out_color;" +
		"out vec4 fragColor;" +
		"void main(void)" +
		"{"+
			"fragColor = out_color;" +
		"}";

    fragmentShaderObject=gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObject,fragmentShaderSourceCode);

	gl.compileShader(fragmentShaderObject);

	if(gl.getShaderParameter(fragmentShaderObject,gl.COMPILE_STATUS)==false)
	{
        var error;
        error = gl.getShaderInfoLog(fragmentShaderObject);
        if(error.length > 0)
        {
            alert(error);
            unitialize();
            return
        }
	}

	//-------------Shader Program---------------------------
	
	shaderProgramObject = gl.createProgram();

	gl.attachShader(shaderProgramObject,vertexShaderObject);
	gl.attachShader(shaderProgramObject,fragmentShaderObject);
	//Pre Link attachment
	gl.bindAttribLocation(shaderProgramObject,WebGLMacros.VSV_ATTRIBUTE_POSITION,"vPosition");
	gl.bindAttribLocation(shaderProgramObject,WebGLMacros.VSV_ATTRIBUTE_COLOR,"vColor");

	gl.linkProgram(shaderProgramObject);

	if(!gl.getProgramParameter(shaderProgramObject,gl.LINK_STATUS))
	{
		var error;
		error = gl.getProgramInfoLog(shaderProgramObject);
        if(error.length > 0)
        {
            alert(error);
            unitialize();
            return
        }		
	}
	//Post Link attachment
	mvpMatrixUniform = gl.getUniformLocation(shaderProgramObject,"u_mvpMatrix");

	//----------------Data prepration-------------------
	var squareVertices = new Float32Array([
                                                1.0, 1.0, 0.0,
                                                -1.0, 1.0, 0.0,
                                                -1.0, -1.0, 0.0,
                                                1.0, -1.0, 0.0
                                            ]);
    
    var triangleVertices = new Float32Array([
                                                0.0,1.0,0.0,
                                                -1.0,-1.0,0.0,
                                                1.0,-1.0,0.0
                                            ]);

    var triangleColor = new Float32Array([
                                                1.0, 0.0, 0.0,
                                                0.0, 1.0, 0.0,
                                                0.0, 0.0, 1.0
                                            ]);
    
    ///----vao rectangle
    
    vao_rectangle = gl.createVertexArray();

	gl.bindVertexArray(vao_rectangle);

        vbo_position=gl.createBuffer();
		
		gl.bindBuffer(gl.ARRAY_BUFFER,
					vbo_position);
		
			gl.bufferData(gl.ARRAY_BUFFER,
                        squareVertices,
						gl.STATIC_DRAW);

			gl.vertexAttribPointer(WebGLMacros.VSV_ATTRIBUTE_POSITION,
								3,
								gl.FLOAT,
								false,
								0,
								0);
			
			gl.enableVertexAttribArray(WebGLMacros.VSV_ATTRIBUTE_POSITION);

		gl.bindBuffer(gl.ARRAY_BUFFER,
					null);

        vbo_color=gl.createBuffer();
		
            gl.bindBuffer(gl.ARRAY_BUFFER,
                vbo_color);
                    
                gl.vertexAttrib3f(WebGLMacros.VSV_ATTRIBUTE_COLOR,
                                    0.0, 0.0, 1.0);
                

            gl.bindBuffer(gl.ARRAY_BUFFER,
                                null);

	gl.bindVertexArray(null);

    ///----vao triangle

    vao_triangle = gl.createVertexArray();

	gl.bindVertexArray(vao_triangle);

        vbo_position=gl.createBuffer();
		
		gl.bindBuffer(gl.ARRAY_BUFFER,
					vbo_position);
		
			gl.bufferData(gl.ARRAY_BUFFER,
                        triangleVertices,
						gl.STATIC_DRAW);

			gl.vertexAttribPointer(WebGLMacros.VSV_ATTRIBUTE_POSITION,
								3,
								gl.FLOAT,
								false,
								0,
								0);
			
			gl.enableVertexAttribArray(WebGLMacros.VSV_ATTRIBUTE_POSITION);

		gl.bindBuffer(gl.ARRAY_BUFFER,
					null);

        vbo_color=gl.createBuffer();
		
            gl.bindBuffer(gl.ARRAY_BUFFER,
                vbo_color);
                    
                gl.bufferData(gl.ARRAY_BUFFER,
                        triangleColor,
						gl.STATIC_DRAW);

                gl.vertexAttribPointer(WebGLMacros.VSV_ATTRIBUTE_COLOR,
                                    3,
                                    gl.FLOAT,
                                    false,
                                    0,
                                    0);
			
			    gl.enableVertexAttribArray(WebGLMacros.VSV_ATTRIBUTE_COLOR);

            gl.bindBuffer(gl.ARRAY_BUFFER,
                                null);

	gl.bindVertexArray(null);

	//Initialize ortho matrix
	perspectiveGraphicProjectionMatrix=mat4.create();

	//-----OpenGL state Machine States-----------------------
    gl.clearColor(0.0, 0.0, 0.0, 1.0);//RGBA
	// gl.clearDepth(1.0);
	// gl.enable(gl.DEPTH_TEST);
	// gl.depthFunc(gl.LEQUAL);

}

function resize()
{
    if(bFullscreen==true)
    {
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
    }else{
        canvas.width = canvas_orignal_width;
        canvas.height = canvas_orignal_height;
    }

    gl.viewport(0,0,canvas.width, canvas.height);

    mat4.perspective(perspectiveGraphicProjectionMatrix,
                    45.0,
                    parseFloat(canvas.width)/parseFloat(canvas.height),
                    0.1,
                    100.0);
}
var Angle=0;
function draw()
{
    var modelViewProjectMatrix = mat4.create();
    var modelViewMatrix = mat4.create();
    
    gl.clear(gl.COLOR_BUFFER_BIT);

	gl.useProgram(shaderProgramObject);

        //rectangle
        mat4.translate(modelViewMatrix,modelViewMatrix,[-1.5,0.0,-6.0]);
        // mat4.rotate(modelViewMatrix,modelViewMatrix,Angle,[1.0,0.0,0.0]); 
        mat4.rotateX(modelViewMatrix,modelViewMatrix,degToRad(Angle));

		mat4.multiply(modelViewProjectMatrix,
                    perspectiveGraphicProjectionMatrix ,
                    modelViewMatrix);

		gl.uniformMatrix4fv(mvpMatrixUniform,
							false,
							modelViewProjectMatrix);
		
		gl.bindVertexArray(vao_rectangle);

            gl.drawArrays(gl.TRIANGLE_FAN,
                        0,
                        4);
		gl.bindVertexArray(null);
        
        //Triangle
        modelViewMatrix = mat4.create();
                
        mat4.translate(modelViewMatrix,modelViewMatrix,[1.5,0.0,-6.0]);
        // mat4.rotate(modelViewMatrix,modelViewMatrix,Angle,[0.0,1.0,0.0]);
        mat4.rotateY(modelViewMatrix,modelViewMatrix,degToRad(Angle));
        
		mat4.multiply(modelViewProjectMatrix,
                    perspectiveGraphicProjectionMatrix ,
                    modelViewMatrix);

		gl.uniformMatrix4fv(mvpMatrixUniform,
							false,
							modelViewProjectMatrix);
		
		gl.bindVertexArray(vao_triangle);
            gl.drawArrays(gl.TRIANGLES,
                        0,
                        3);
		gl.bindVertexArray(null);

	gl.useProgram(null);
    
    Angle+=3;
    requestAnimationFrame(draw,canvas);
}

function degToRad(degrees)
{
    return ( degrees*Math.PI /180.0)
}

function unitialize()
{
    if(vao)
	{
		gl.deleteVertexArray(vao);
		vao=null;
	}

	if(vbo_position)
	{
		gl.deleteBuffer(vbo_position);
		vbo_position=null;
	}

	if(shaderProgramObject)
	{
        if(fragmentShaderObject)
        {
            gl.detachShader(shaderProgramObject,fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject=null;
        }
			
        if(vertexShaderObject)
        {
            gl.detachShader(shaderProgramObject,vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject=null;
        }
          
		gl.deleteProgram(shaderProgramObject);
		shaderProgramObject=null;
	}
}

