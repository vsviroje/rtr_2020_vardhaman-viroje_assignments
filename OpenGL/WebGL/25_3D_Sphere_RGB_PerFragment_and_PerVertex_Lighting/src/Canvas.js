//Global
var canvas = null;
var gl = null;

var canvas_orignal_width;
var canvas_orignal_height;
var bFullscreen=false;

const WebGLMacros={
VSV_ATTRIBUTE_POSITION:0,
VSV_ATTRIBUTE_COLOR:1,
VSV_ATTRIBUTE_NORMAL:2,
VSV_ATTRIBUTE_TEXTURE:3,
};


var perspectiveGraphicProjectionMatrix;


//---Per Fragment---------------
var PF_vertexShaderObject;
var PF_fragmentShaderObject;
var PF_shaderProgramObject;

var PF_lightAmbientUniform=[];
var PF_lightDiffuseUniform=[];
var PF_lightSpecularUniform=[];
var PF_lightPositionUniform=[];

var PF_modelViewMatrixUniform;
var PF_projectionMatrixUniform;

var PF_materialAmbientUniform;
var PF_materialDiffuseUniform;
var PF_materialSpecularUniform;
var PF_materialShininessUniform;

var PF_LKeyPressedUniform;
//----------------

//---Per Vertex-------------

var PV_vertexShaderObject;
var PV_fragmentShaderObject;
var PV_shaderProgramObject;

var PV_modelViewMatrixUniform;
var PV_projectionMatrixUniform;

var PV_lightAmbientUniform=[];
var PV_lightDiffuseUniform=[];
var PV_lightSpecularUniform=[];
var PV_lightPositionUniform=[];

var PV_materialAmbientUniform;
var PV_materialDiffuseUniform;
var PV_materialSpecularUniform;
var PV_materialShininessUniform;

var PV_LKeyPressedUniform;
//---------------------------

var light= [
    [
        [0.0,0.0,0.0,1.0],
        [1.0,0.0,0.0,1.0],
        [1.0,0.0,0.0,1.0],
        [0.0,0.0,0.0,1.0]
    ],
    [
        [0.0,0.0,0.0,1.0],
        [0.0,1.0,0.0,1.0],
        [0.0,1.0,0.0,1.0],
        [0.0,0.0,0.0,1.0]
    ],
    [
        [0.0,0.0,0.0,1.0],
        [0.0,0.0,1.0,1.0],
        [0.0,0.0,1.0,1.0],
        [0.0,0.0,0.0,1.0]
    ]
];


var materialAmbient = [ 0.0,0.0,0.0,1.0 ];
var materilaDefuse = [ 1.0,1.0,1.0,1.0 ];
var materialSpecular = [ 1.0,1.0,1.0,1.0 ];
var materialShininess = 128.0;

var bTogglePVxOrPF=false;//True-Per Fragment and False-Per Vertex
var bLights ;

var sphere=null;

var requestAnimationFrame = window.requestAnimationFrame || 
                            window.webkitRequestAnimationFrame ||
                            window.mozRequestAnimationFrame ||
                            window.oRequestAnimationFrame || //opera
                            window.msRequestAnimationFrame ||
                            null;

var cancelAnimationFrame =  window.cancelAnimationFrame ||
                            window.webkitCancelRequestAnimationFrame ||  window.webkitCancelAnimation ||
                            window.mozCancelRequestAnimationFrame ||  window.mozCancelAnimationFrame ||
                            window.oCancelRequestAnimationFrame ||  window.oCancelAnimationFrame ||
                            window.msCancelRequestAnimationFrame ||  window.msCancelAnimationFrame ||
                            null;

function main()
{
    //step 1 : Get canvas from html dom
    //document is inbuild variable/DOM object    
    canvas=document.getElementById("canvasID_vsv");
    if(!canvas)
    {
        console.log("ERROR:Failed to obtain Canvas");
        unitialize();
    }else{
        console.log("INFO:Obtained Canvas Successfully");
    }

    //Step 2 : Retreive the height and width of canvas
    //console is inbuild variable/DOM object 
    console.log("INFO:Canvas width=" + canvas.width + " height=" + canvas.height + "\n" );

    canvas_orignal_width = canvas.width;
    canvas_orignal_height = canvas.height;

    //step 3 : Get drawing webgl2 from the canvas
    gl = canvas.getContext("webgl2");
    if(!gl)
    {
        console.log("ERROR:Failed to obtain webgl2 context");
        unitialize();
    }else{
        console.log("INFO:Obtained webgl2 context Successfully");
    }

    if (requestAnimationFrame == null)
    {
        console.log("ERROR:requestAnimationFrame is null");
        unitialize();
        return
    }

    //step 10 : Add event listener
    //window is inbuild variable/DOM object    
    window.addEventListener("keydown",//EVENT TYPE
                            keyDown,//FUNCTION NAME
                            false);//false-captured delegation/propagation or true-bubble delegation/propagation
    
    window.addEventListener("click",
                            mouseDown,
                            false);

    window.addEventListener("resize",resize,false);

    Init();

    resize();//Warm up resize call

    draw();//Warm up redraw call
}

function toggleFullscreen()
{
    var fullscreen_element =    document.fullscreenElement || 
                                document.webkitFullscreenElement || 
                                document.mozFullScreenElement ||
                                document.msFullscreenElement ||
                                null;

    if(fullscreen_element==null)
    {
        if (canvas.requestFullscreen)
        {   
            canvas.requestFullscreen();

        }else if(canvas.webkitRequestFullscreen)
        {
            canvas.webkitRequestFullscreen();

        }else if(canvas.mozRequestFullScreen)
        {
            canvas.mozRequestFullScreen();

        }else if(canvas.msRequestFullscreen)
        {
            canvas.msRequestFullscreen();
        }

        bFullscreen=true;

    }else{
        if(document.exitFullscreen)
        {
            document.exitFullscreen();

        }else if(document.webkitExitFullscreen)
        {
            document.webkitExitFullScreen();

        }else if(document.mozCancelFullScreen)
        {
            document.mozCancelFullScreen();
            
        }else if(document.msExitFullScreen)
        {
            document.msExitFullScreen();
        }

        bFullscreen=false;
    }
}

function keyDown(event)
{
    console.log(event);
   switch(event.keyCode)
   {
        case 27://Escape
            toggleFullscreen();
            break;
        case 81://Q
            unitialize();
            window.close();
            break;
        case 86://v
            bTogglePVxOrPF = false;
            break;
        case 70://f
            bTogglePVxOrPF = true;
            break;
        case 76://L
            bLights = !bLights;
            if (bLights)
            {
                bTogglePVxOrPF = false;
            }            
            break; 
   }
}

function mouseDown()
{
    console.log("INFO:Mouse is pressed");
}

function Init()
{

    gl.viewportWidth = canvas.width;
    gl.viewportHeight = canvas.height;

    //---Per Vertex----------
    
        var vertexShaderSourceCode=
        "#version 300 es" +
        "\n" +
        "in vec4 vPosition;\n" +
        "in vec3 vNormal;\n" +
        
        "uniform mat4 u_modelViewMatrix;\n" +
        "uniform mat4 u_projectionMatrix;\n" +

        "uniform vec3 u_lightAmbient[3];\n"+
        "uniform vec3 u_lightDiffuse[3];\n"+
        "uniform vec3 u_lightSpecular[3];\n"+
        "uniform vec4 u_lightPosition[3];\n"+
        
        "uniform vec3 u_materialAmbient;\n"+
        "uniform vec3 u_materialDiffuse;\n"+
        "uniform vec3 u_materialSpecular;\n"+
        "uniform float u_materialShininess;\n"+
        
        "uniform int u_lKeyPressed;\n"+
        
        "out vec3 out_phongADS_Light;\n"+

        "precision highp float;"+
        "precision highp int;"+
        
        "void main(void)\n" +
        "{\n" +
            "if (u_lKeyPressed == 1)\n"+
            "{\n"+
                "for (int i = 0; i < 3; i++)\n"+
                "{\n"+

                    "vec4 eyeCoord = u_modelViewMatrix * vPosition;\n"+
                    
                    "vec3 transformedNormal = normalize( mat3(u_modelViewMatrix) * vNormal );\n"+

                    "vec3 lightDirection = normalize( vec3( u_lightPosition[i] - eyeCoord ) );\n"+
                    
                    "vec3 reflectionVector = reflect( -lightDirection, transformedNormal );\n"+
                    
                    "vec3 viewVector = normalize( -eyeCoord.xyz);\n"+
                    
                    "vec3 ambient = u_lightAmbient[i] * u_materialAmbient;\n"+
                    
                    "vec3 diffuse = u_lightDiffuse[i] * u_materialDiffuse * max( dot(lightDirection , transformedNormal), 0.0);\n"+
                    
                    "vec3 specular = u_lightSpecular[i] * u_materialSpecular * pow( max( dot(reflectionVector , viewVector), 0.0f) , u_materialShininess);\n"+
                    
                    "out_phongADS_Light += ambient + diffuse + specular;\n"+

                "}\n"+
            "}\n"+
            "else\n"+
            "{\n"+

                "out_phongADS_Light = vec3(1.0f, 1.0f, 1.0f);\n"+
            
            "}\n"+
        
            "gl_Position = u_projectionMatrix * u_modelViewMatrix * vPosition;\n" +
        "}\n";

        PV_vertexShaderObject=gl.createShader(gl.VERTEX_SHADER);

        gl.shaderSource(PV_vertexShaderObject,vertexShaderSourceCode);

        gl.compileShader(PV_vertexShaderObject);

        if(gl.getShaderParameter(PV_vertexShaderObject,gl.COMPILE_STATUS)==false)
        {
            var error;
            error = gl.getShaderInfoLog(PV_vertexShaderObject);
            if(error.length > 0)
            {
                alert(error);
                unitialize();
                return
            }
        }	

        //---------Fragment--------------

        var fragmentShaderSourceCode=
            "#version 300 es" +
            "\n" +
            
            "precision highp float;"+
            "precision highp int;"+
            
            "out vec4 fragColor;"+
            "in vec3 out_phongADS_Light;\n"+

            "void main(void)" +
            "{"+    
                "fragColor=vec4(out_phongADS_Light, 1.0f);"+
            "}";

        PV_fragmentShaderObject=gl.createShader(gl.FRAGMENT_SHADER);
        gl.shaderSource(PV_fragmentShaderObject,fragmentShaderSourceCode);

        gl.compileShader(PV_fragmentShaderObject);

        if(gl.getShaderParameter(PV_fragmentShaderObject,gl.COMPILE_STATUS)==false)
        {
            var error;
            error = gl.getShaderInfoLog(PV_fragmentShaderObject);
            if(error.length > 0)
            {
                alert(error);
                unitialize();
                return
            }
        }

        //-------------Shader Program---------------------------

        PV_shaderProgramObject = gl.createProgram();

        gl.attachShader(PV_shaderProgramObject,PV_vertexShaderObject);
        gl.attachShader(PV_shaderProgramObject,PV_fragmentShaderObject);
        //Pre Link attachment
        gl.bindAttribLocation(PV_shaderProgramObject,WebGLMacros.VSV_ATTRIBUTE_POSITION,"vPosition");
        gl.bindAttribLocation(PV_shaderProgramObject,WebGLMacros.VSV_ATTRIBUTE_NORMAL,"vNormal");
        // gl.bindAttribLocation(PV_shaderProgramObject,WebGLMacros.VSV_ATTRIBUTE_TEXTURE,"vTexCoord");

        gl.linkProgram(PV_shaderProgramObject);

        if(!gl.getProgramParameter(PV_shaderProgramObject,gl.LINK_STATUS))
        {
            var error;
            error = gl.getProgramInfoLog(PV_shaderProgramObject);
            if(error.length > 0)    
            {
                alert(error);
                unitialize();
                return
            }		
        }
        //Post Link attachment
        PV_modelViewMatrixUniform = gl.getUniformLocation(PV_shaderProgramObject,"u_modelViewMatrix");
        PV_projectionMatrixUniform = gl.getUniformLocation(PV_shaderProgramObject,"u_projectionMatrix");

        PV_lightAmbientUniform[0] = gl.getUniformLocation(PV_shaderProgramObject,"u_lightAmbient[0]");
        PV_lightDiffuseUniform[0] = gl.getUniformLocation(PV_shaderProgramObject,"u_lightDiffuse[0]");
        PV_lightSpecularUniform[0] = gl.getUniformLocation(PV_shaderProgramObject,"u_lightSpecular[0]");
        PV_lightPositionUniform[0] = gl.getUniformLocation(PV_shaderProgramObject,"u_lightPosition[0]");

        PV_lightAmbientUniform[1] = gl.getUniformLocation(PV_shaderProgramObject,"u_lightAmbient[1]");
        PV_lightDiffuseUniform[1] = gl.getUniformLocation(PV_shaderProgramObject,"u_lightDiffuse[1]");
        PV_lightSpecularUniform[1] = gl.getUniformLocation(PV_shaderProgramObject,"u_lightSpecular[1]");
        PV_lightPositionUniform[1] = gl.getUniformLocation(PV_shaderProgramObject,"u_lightPosition[1]");

        PV_lightAmbientUniform[2] = gl.getUniformLocation(PV_shaderProgramObject,"u_lightAmbient[2]");
        PV_lightDiffuseUniform[2] = gl.getUniformLocation(PV_shaderProgramObject,"u_lightDiffuse[2]");
        PV_lightSpecularUniform[2] = gl.getUniformLocation(PV_shaderProgramObject,"u_lightSpecular[2]");
        PV_lightPositionUniform[2] = gl.getUniformLocation(PV_shaderProgramObject,"u_lightPosition[2]");

        PV_materialAmbientUniform = gl.getUniformLocation(PV_shaderProgramObject,"u_materialAmbient");
        PV_materialDiffuseUniform = gl.getUniformLocation(PV_shaderProgramObject,"u_materialDiffuse");
        PV_materialSpecularUniform = gl.getUniformLocation(PV_shaderProgramObject,"u_materialSpecular");
        PV_materialShininessUniform = gl.getUniformLocation(PV_shaderProgramObject,"u_materialShininess");

        PV_LKeyPressedUniform = gl.getUniformLocation(PV_shaderProgramObject,"u_lKeyPressed");


    //-----------------------

    //--- Per Fragment-----------
	var PF_vertexShaderSourceCode=
		"#version 300 es" +
		"\n" +
		"in vec4 vPosition;\n" +
		"in vec3 vNormal;\n" +
		
        "uniform mat4 u_modelViewMatrix;\n" +
		"uniform mat4 u_projectionMatrix;\n" +

        "uniform int u_lKeyPressed;\n"+

        "uniform vec4 u_lightPosition[3];\n"+

		"out vec3 out_transformedNormal;\n"+
		"out vec3 out_lightDirection[3];\n"+
		"out vec3 out_viewVector;\n"+
		
		"void main(void)\n" +
		"{\n" +
            "if (u_lKeyPressed == 1)\n"+
            "{\n"+

                "vec4 eyeCoord = u_modelViewMatrix * vPosition;\n"+
                
                "out_transformedNormal= normalize( mat3(u_modelViewMatrix) * vNormal );\n"+
                
                "for (int i = 0; i < 3; i++)\n"+
				"{\n"+
                    "out_lightDirection[i] = normalize( vec3( u_lightPosition[i] - eyeCoord ) );\n"+
                "}\n"+

                "out_viewVector = normalize( -eyeCoord.xyz);\n"+
              
            "}\n"+
            
			"gl_Position = u_projectionMatrix * u_modelViewMatrix * vPosition;\n" +
		"}\n";

    PF_vertexShaderObject=gl.createShader(gl.VERTEX_SHADER);

	gl.shaderSource(PF_vertexShaderObject,PF_vertexShaderSourceCode);

	gl.compileShader(PF_vertexShaderObject);

	if(gl.getShaderParameter(PF_vertexShaderObject,gl.COMPILE_STATUS)==false)
	{
		var error;
        error = gl.getShaderInfoLog(PF_vertexShaderObject);
        if(error.length > 0)
        {
            alert(error);
            unitialize();
            return
        }
    }	

	//---------Fragment--------------

	var PF_fragmentShaderSourceCode=
		"#version 300 es" +
		"\n" +
        
        "precision highp float;"+
        "precision highp int;"+
		
        "out vec4 fragColor;"+
		"vec3 phongADS_Light;\n"+

        "in vec3 out_transformedNormal;\n"+
		"in vec3 out_lightDirection[3];\n"+
		"in vec3 out_viewVector;\n"+

        "uniform vec3 u_lightAmbient[3];\n"+
		"uniform vec3 u_lightDiffuse[3];\n"+
		"uniform vec3 u_lightSpecular[3];\n"+
		
		"uniform vec3 u_materialAmbient;\n"+
		"uniform vec3 u_materialDiffuse;\n"+
		"uniform vec3 u_materialSpecular;\n"+
		"uniform float u_materialShininess;\n"+
		
		"uniform int u_lKeyPressed;\n"+
		
		"void main(void)" +
		"{"+  
            "if (u_lKeyPressed == 1)\n"+
            "{\n"+
                "for (int i = 0; i < 3; i++)\n"+
                "{\n"+
                    "vec3 normalizedTransformedNormal = normalize( out_transformedNormal );\n"+
                    "vec3 normalizedLightDirection = normalize( out_lightDirection[i] );\n"+
                    "vec3 normalizedViewVector = normalize( out_viewVector );\n"+

                    "vec3 reflectionVector = reflect( -normalizedLightDirection, normalizedTransformedNormal );\n"+
                    
                    "vec3 ambient = u_lightAmbient[i] * u_materialAmbient;\n"+

                    "vec3 diffuse = u_lightDiffuse[i] * u_materialDiffuse * max( dot(normalizedLightDirection , normalizedTransformedNormal), 0.0);\n"+

                    "vec3 specular = u_lightSpecular[i] * u_materialSpecular * pow( max( dot(reflectionVector , normalizedViewVector), 0.0f) , u_materialShininess);\n"+

                    "phongADS_Light += ambient+ diffuse + specular;\n"+
                "}\n"+                
            "}\n"+
            "else\n"+
            "{\n"+
                "phongADS_Light=vec3(1.0f, 1.0f, 1.0f);\n"+
            "}\n"+
        
			"fragColor=vec4(phongADS_Light, 1.0f);"+
		"}";

    PF_fragmentShaderObject=gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(PF_fragmentShaderObject,PF_fragmentShaderSourceCode);

	gl.compileShader(PF_fragmentShaderObject);

	if(gl.getShaderParameter(PF_fragmentShaderObject,gl.COMPILE_STATUS)==false)
	{
        var error;
        error = gl.getShaderInfoLog(PF_fragmentShaderObject);
        if(error.length > 0)
        {
            alert(error);
            unitialize();
            return
        }
	}

	//-------------Shader Program---------------------------
	
	PF_shaderProgramObject = gl.createProgram();

	gl.attachShader(PF_shaderProgramObject,PF_vertexShaderObject);
	gl.attachShader(PF_shaderProgramObject,PF_fragmentShaderObject);
	//Pre Link attachment
	gl.bindAttribLocation(PF_shaderProgramObject,WebGLMacros.VSV_ATTRIBUTE_POSITION,"vPosition");
	gl.bindAttribLocation(PF_shaderProgramObject,WebGLMacros.VSV_ATTRIBUTE_NORMAL,"vNormal");
	// gl.bindAttribLocation(PF_shaderProgramObject,WebGLMacros.VSV_ATTRIBUTE_TEXTURE,"vTexCoord");

	gl.linkProgram(PF_shaderProgramObject);

	if(!gl.getProgramParameter(PF_shaderProgramObject,gl.LINK_STATUS))
	{
		var error;
		error = gl.getProgramInfoLog(PF_shaderProgramObject);
        if(error.length > 0)
        {
            alert(error);
            unitialize();
            return
        }		
	}
	//Post Link attachment
	PF_modelViewMatrixUniform = gl.getUniformLocation(PF_shaderProgramObject,"u_modelViewMatrix");
	PF_projectionMatrixUniform = gl.getUniformLocation(PF_shaderProgramObject,"u_projectionMatrix");

    PF_lightAmbientUniform[0] = gl.getUniformLocation(PF_shaderProgramObject,"u_lightAmbient[0]");
	PF_lightDiffuseUniform[0] = gl.getUniformLocation(PF_shaderProgramObject,"u_lightDiffuse[0]");
	PF_lightSpecularUniform[0] = gl.getUniformLocation(PF_shaderProgramObject,"u_lightSpecular[0]");
	PF_lightPositionUniform[0] = gl.getUniformLocation(PF_shaderProgramObject,"u_lightPosition[0]");

    PF_lightAmbientUniform[1] = gl.getUniformLocation(PF_shaderProgramObject,"u_lightAmbient[1]");
	PF_lightDiffuseUniform[1] = gl.getUniformLocation(PF_shaderProgramObject,"u_lightDiffuse[1]");
	PF_lightSpecularUniform[1] = gl.getUniformLocation(PF_shaderProgramObject,"u_lightSpecular[1]");
	PF_lightPositionUniform[1] = gl.getUniformLocation(PF_shaderProgramObject,"u_lightPosition[1]");

    PF_lightAmbientUniform[2] = gl.getUniformLocation(PF_shaderProgramObject,"u_lightAmbient[2]");
	PF_lightDiffuseUniform[2] = gl.getUniformLocation(PF_shaderProgramObject,"u_lightDiffuse[2]");
	PF_lightSpecularUniform[2] = gl.getUniformLocation(PF_shaderProgramObject,"u_lightSpecular[2]");
	PF_lightPositionUniform[2] = gl.getUniformLocation(PF_shaderProgramObject,"u_lightPosition[2]");

	PF_materialAmbientUniform = gl.getUniformLocation(PF_shaderProgramObject,"u_materialAmbient");
	PF_materialDiffuseUniform = gl.getUniformLocation(PF_shaderProgramObject,"u_materialDiffuse");
	PF_materialSpecularUniform = gl.getUniformLocation(PF_shaderProgramObject,"u_materialSpecular");
	PF_materialShininessUniform = gl.getUniformLocation(PF_shaderProgramObject,"u_materialShininess");

	PF_LKeyPressedUniform = gl.getUniformLocation(PF_shaderProgramObject,"u_lKeyPressed");


	//----------------Data prepration-------------------
    ///----vao 
    sphere=new Mesh();
    makeSphere(sphere,2,50,50);
    
	//Initialize ortho matrix
	perspectiveGraphicProjectionMatrix=mat4.create();

	//-----OpenGL state Machine States-----------------------
    gl.clearColor(0.0, 0.0, 0.0, 1.0);//RGBA
	gl.clearDepth(1.0);
	gl.enable(gl.DEPTH_TEST);
	gl.depthFunc(gl.LEQUAL);

}

function resize()
{
    if(bFullscreen==true)
    {
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
    }else{
        canvas.width = canvas_orignal_width;
        canvas.height = canvas_orignal_height;
    }

    gl.viewport(0,0,canvas.width, canvas.height);

    mat4.perspective(perspectiveGraphicProjectionMatrix,
                    45.0,
                    parseFloat(canvas.width)/parseFloat(canvas.height),
                    0.1,
                    100.0);
}

var Angle = 0.0;

var radius = 6.0;

function draw()
{
    var modelViewMatrix = mat4.create();
    
    gl.clear(gl.COLOR_BUFFER_BIT);

    mat4.translate(modelViewMatrix,modelViewMatrix,[0.0,0.0,-5.0]);

    if (bTogglePVxOrPF===false)
	{

	    gl.useProgram(PV_shaderProgramObject);

    
        gl.uniformMatrix4fv(PV_modelViewMatrixUniform,
                            false,
                            modelViewMatrix);
        
        gl.uniformMatrix4fv(PV_projectionMatrixUniform,
                            false,
                            perspectiveGraphicProjectionMatrix);

        if (bLights == true)
        {
            gl.uniform1i(PV_LKeyPressedUniform, 1);
            for (var i = 0; i < 3; i++)
			{
                gl.uniform3fv(PV_lightAmbientUniform[i], [light[i][0][0],light[i][0][1],light[i][0][2]]);
                gl.uniform3fv(PV_lightDiffuseUniform[i], [light[i][1][0],light[i][1][1],light[i][1][2]]);
                gl.uniform3fv(PV_lightSpecularUniform[i],[light[i][2][0],light[i][2][1],light[i][2][2]]);
                
                if (i == 0) {
                    light[i][3] = [0.0, radius * Math.sin(Angle), radius * Math.cos(Angle), 1.0];
				}
				else if (i == 1) {
                    light[i][3] = [radius * Math.sin(Angle), 0.0, radius * Math.cos(Angle), 1.0];
				}
				else if (i == 2) {
                    light[i][3] = [radius * Math.sin(Angle), radius * Math.cos(Angle), 0.0, 1.0];
				}

                gl.uniform4fv(PV_lightPositionUniform[i],   light[i][3]);//posiitional light
            }

            gl.uniform3fv(PV_materialAmbientUniform,  [materialAmbient[0],materialAmbient[1],materialAmbient[2]]);
            gl.uniform3fv(PV_materialDiffuseUniform,  [materilaDefuse[0],materilaDefuse[1],materilaDefuse[2]]);
            gl.uniform3fv(PV_materialSpecularUniform,  [materialSpecular[0],materialSpecular[1],materialSpecular[2]] );

            gl.uniform1f(PV_materialShininessUniform, materialShininess);
        }
        else
        {
            gl.uniform1i(PV_LKeyPressedUniform, 0);
        }   

    }else{

	    gl.useProgram(PF_shaderProgramObject);
       
		gl.uniformMatrix4fv(PF_modelViewMatrixUniform,
							false,
							modelViewMatrix);
        
        gl.uniformMatrix4fv(PF_projectionMatrixUniform,
                            false,
                            perspectiveGraphicProjectionMatrix);

        if (bLights == true)
		{
			gl.uniform1i(PF_LKeyPressedUniform, 1);
            for (var i = 0; i < 3; i++)
			{
                gl.uniform3fv(PF_lightAmbientUniform[i], [light[i][0][0],light[i][0][1],light[i][0][2]]);
                gl.uniform3fv(PF_lightDiffuseUniform[i], [light[i][1][0],light[i][1][1],light[i][1][2]]);
                gl.uniform3fv(PF_lightSpecularUniform[i],[light[i][2][0],light[i][2][1],light[i][2][2]]);

                if (i == 0) {
					light[i][3] = [0.0, radius * Math.sin(Angle), radius * Math.cos(Angle), 1.0];
				}
				else if (i == 1) {
					light[i][3] = [radius * Math.sin(Angle), 0.0, radius * Math.cos(Angle), 1.0];
				}
				else if (i == 2) {
					light[i][3] = [radius * Math.sin(Angle), radius * Math.cos(Angle), 0.0, 1.0];
				}

                gl.uniform4fv(PF_lightPositionUniform[i],  light[i][3]);//posiitional light
            }
			gl.uniform3fv(PF_materialAmbientUniform,  [materialAmbient[0],materialAmbient[1],materialAmbient[2]]);
			gl.uniform3fv(PF_materialDiffuseUniform,  [materilaDefuse[0],materilaDefuse[1],materilaDefuse[2]]);
			gl.uniform3fv(PF_materialSpecularUniform,  [materialSpecular[0],materialSpecular[1],materialSpecular[2]] );

			gl.uniform1f(PF_materialShininessUniform, materialShininess);
		}
		else
		{
			gl.uniform1i(PF_LKeyPressedUniform, 0);
		}   
    }
        sphere.draw();

	gl.useProgram(null);
   
    modelViewMatrix = mat4.create();

    // requestAnimationFrame(update,canvas); //if using update() then use this way
    requestAnimationFrame(draw,canvas);
    Angle += 0.05;
}

function degToRad(degrees)
{
    return ( degrees*Math.PI /180.0)
}

function unitialize()
{
    sphere.deallocate();

    
	if(PV_shaderProgramObject)
	{
        if(PV_fragmentShaderObject)
        {
            gl.detachShader(PV_shaderProgramObject,PV_fragmentShaderObject);
			gl.deleteShader(PV_fragmentShaderObject);
			PV_fragmentShaderObject=null;
        }
			
        if(PV_vertexShaderObject)
        {
            gl.detachShader(PV_shaderProgramObject,PV_vertexShaderObject);
			gl.deleteShader(PV_vertexShaderObject);
			PV_vertexShaderObject=null;
        }
          
		gl.deleteProgram(PV_shaderProgramObject);
		PV_shaderProgramObject=null;
	}

	if(PF_shaderProgramObject)
	{
        if(PF_fragmentShaderObject)
        {
            gl.detachShader(PF_shaderProgramObject,PF_fragmentShaderObject);
			gl.deleteShader(PF_fragmentShaderObject);
			PF_fragmentShaderObject=null;
        }
			
        if(PF_vertexShaderObject)
        {
            gl.detachShader(PF_shaderProgramObject,PF_vertexShaderObject);
			gl.deleteShader(PF_vertexShaderObject);
			PF_vertexShaderObject=null;
        }
          
		gl.deleteProgram(PF_shaderProgramObject);
		PF_shaderProgramObject=null;
	}
}

