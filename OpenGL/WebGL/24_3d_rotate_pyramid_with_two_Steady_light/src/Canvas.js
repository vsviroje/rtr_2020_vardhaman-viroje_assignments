//Global
var canvas = null;
var gl = null;

var canvas_orignal_width;
var canvas_orignal_height;
var bFullscreen=false;

const WebGLMacros={
VSV_ATTRIBUTE_POSITION:0,
VSV_ATTRIBUTE_COLOR:1,
VSV_ATTRIBUTE_NORMAL:2,
VSV_ATTRIBUTE_TEXTURE:3,
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var vao_pyramid;

var vbo_position;
var vbo_Normal;

var modelMatrixUniform;
var viewMatrixUniform;
var projectionMatrixUniform;

var perspectiveGraphicProjectionMatrix;

var lightAmbientUniform=[];
var lightDiffuseUniform=[];
var lightSpecularUniform=[];
var lightPositionUniform=[];

var materialAmbientUniform;
var materialDiffuseUniform;
var materialSpecularUniform;
var materialShininessUniform;

var LKeyPressedUniform;

var bLights;

var light= [
    [
        [0.0,0.0,0.0,1.0],
        [1.0,0.0,0.0,1.0],
        [1.0,0.0,0.0,1.0],
        [2.0,0.0,0.0,1.0]
    ],
    [
        [0.0,0.0,0.0,1.0],
        [0.0,0.0,1.0,1.0],
        [0.0,0.0,1.0,1.0],
        [-2.0,0.0,0.0,1.0]
    ]
];

var materialAmbient =[ 0.0,0.0,0.0,1.0];
var materilaDefuse = [ 1.0,1.0,1.0,1.0 ];
var materialSpecular= [ 1.0,1.0,1.0,1.0 ];
var materialShininess = 50.0;


var requestAnimationFrame = window.requestAnimationFrame || 
                            window.webkitRequestAnimationFrame ||
                            window.mozRequestAnimationFrame ||
                            window.oRequestAnimationFrame || //opera
                            window.msRequestAnimationFrame ||
                            null;

var cancelAnimationFrame =  window.cancelAnimationFrame ||
                            window.webkitCancelRequestAnimationFrame ||  window.webkitCancelAnimation ||
                            window.mozCancelRequestAnimationFrame ||  window.mozCancelAnimationFrame ||
                            window.oCancelRequestAnimationFrame ||  window.oCancelAnimationFrame ||
                            window.msCancelRequestAnimationFrame ||  window.msCancelAnimationFrame ||
                            null;

function main()
{
    //step 1 : Get canvas from html dom
    //document is inbuild variable/DOM object    
    canvas=document.getElementById("canvasID_vsv");
    if(!canvas)
    {
        console.log("ERROR:Failed to obtain Canvas");
        unitialize();
    }else{
        console.log("INFO:Obtained Canvas Successfully");
    }

    //Step 2 : Retreive the height and width of canvas
    //console is inbuild variable/DOM object 
    console.log("INFO:Canvas width=" + canvas.width + " height=" + canvas.height + "\n" );

    canvas_orignal_width = canvas.width;
    canvas_orignal_height = canvas.height;

    //step 3 : Get drawing webgl2 from the canvas
    gl = canvas.getContext("webgl2");
    if(!gl)
    {
        console.log("ERROR:Failed to obtain webgl2 context");
        unitialize();
    }else{
        console.log("INFO:Obtained webgl2 context Successfully");
    }

    if (requestAnimationFrame == null)
    {
        console.log("ERROR:requestAnimationFrame is null");
        unitialize();
        return
    }

    //step 10 : Add event listener
    //window is inbuild variable/DOM object    
    window.addEventListener("keydown",//EVENT TYPE
                            keyDown,//FUNCTION NAME
                            false);//false-captured delegation/propagation or true-bubble delegation/propagation
    
    window.addEventListener("click",
                            mouseDown,
                            false);

    window.addEventListener("resize",resize,false);

    Init();

    resize();//Warm up resize call

    draw();//Warm up redraw call
}

function toggleFullscreen()
{
    var fullscreen_element =    document.fullscreenElement || 
                                document.webkitFullscreenElement || 
                                document.mozFullScreenElement ||
                                document.msFullscreenElement ||
                                null;

    if(fullscreen_element==null)
    {
        if (canvas.requestFullscreen)
        {   
            canvas.requestFullscreen();

        }else if(canvas.webkitRequestFullscreen)
        {
            canvas.webkitRequestFullscreen();

        }else if(canvas.mozRequestFullScreen)
        {
            canvas.mozRequestFullScreen();

        }else if(canvas.msRequestFullscreen)
        {
            canvas.msRequestFullscreen();
        }

        bFullscreen=true;

    }else{
        if(document.exitFullscreen)
        {
            document.exitFullscreen();

        }else if(document.webkitExitFullscreen)
        {
            document.webkitExitFullScreen();

        }else if(document.mozCancelFullScreen)
        {
            document.mozCancelFullScreen();
            
        }else if(document.msExitFullScreen)
        {
            document.msExitFullScreen();
        }

        bFullscreen=false;
    }
}

function keyDown(event)
{
    console.log(event);
   switch(event.keyCode)
   {
        case 70:
            toggleFullscreen();
            break;
        case 27:
                unitialize();
                window.close();
                break;
        case 76:
            bLights = !bLights;
            break;
   }
}

function mouseDown()
{
    console.log("INFO:Mouse is pressed");
}

function Init()
{
    gl.viewportWidth = canvas.width;
    gl.viewportHeight = canvas.height;

	var vertexShaderSourceCode=
		"#version 300 es" +
		"\n" +
		"in vec4 vPosition;" +
        "in vec3 vNormal;\n"+

		"uniform mat4 u_modelMatrix;\n"+
		"uniform mat4 u_viewMatrix;\n"+
		"uniform mat4 u_projectionMatrix;\n"+

		"uniform vec3 u_lightAmbient[2];\n"+
		"uniform vec3 u_lightDiffuse[2];\n"+
		"uniform vec3 u_lightSpecular[2];\n"+
		"uniform vec4 u_lightPosition[2];\n"+

		"uniform vec3 u_materialAmbient;\n"+
		"uniform vec3 u_materialDiffuse;\n"+
		"uniform vec3 u_materialSpecular;\n"+
		"uniform float u_materialShininess;\n"+

		"uniform int u_lKeyPressed;\n"+

		"out vec3 out_phongADS_Light;\n"+

		"uniform mat4 u_mvpMatrix;" +
		
        "void main(void)" +
		"{" +

        "if (u_lKeyPressed == 1)\n"+
		"{\n"+

			"vec4 eyeCoord = u_viewMatrix * u_modelMatrix * vPosition;\n"+
			"vec3 viewVector = normalize( -eyeCoord.xyz);\n"+
			"vec3 transformedNormal = normalize( mat3(u_viewMatrix * u_modelMatrix) * vNormal );\n"+
		
			"vec3 lightDirection[2];\n"+
			"vec3 reflectionVector[2];\n"+

			"vec3 ambient[2];\n"+
			"vec3 diffuse[2];\n"+
			"vec3 specular[2];\n"+

			"for (int i = 0; i < 2; i++)\n"+
			"{\n"+

				"lightDirection[i] = normalize( vec3( u_lightPosition[i] - eyeCoord ) );\n"+

				"reflectionVector[i] = reflect( -lightDirection[i], transformedNormal );\n"+

				"ambient[i]= u_lightAmbient[i] * u_materialAmbient;\n"+

				"diffuse[i] = u_lightDiffuse[i] * u_materialDiffuse * max( dot(lightDirection[i] , transformedNormal), 0.0);\n"+

				"specular[i] = u_lightSpecular[i] * u_materialSpecular * pow( max( dot(reflectionVector[i] , viewVector), 0.0f) , u_materialShininess);\n"+

				"out_phongADS_Light += ambient[i] + diffuse[i] + specular[i];\n"+

			"}\n"+

		"}\n"+
		"else\n"+
		"{\n"+

			"out_phongADS_Light = vec3(1.0f, 1.0f, 1.0f);\n"+

		"}\n"+

		    "gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix * vPosition;\n"+
		"}";

    vertexShaderObject=gl.createShader(gl.VERTEX_SHADER);

	gl.shaderSource(vertexShaderObject,vertexShaderSourceCode);

	gl.compileShader(vertexShaderObject);

	if(gl.getShaderParameter(vertexShaderObject,gl.COMPILE_STATUS)==false)
	{
		var error;
        error = gl.getShaderInfoLog(vertexShaderObject);
        if(error.length > 0)
        {
            alert(error);
            unitialize();
            return
        }
    }	

	//---------Fragment--------------

	var fragmentShaderSourceCode=
		"#version 300 es" +
		"\n" +
        "precision highp float;"+
		"out vec4 fragColor;" +
        "in vec3 out_phongADS_Light;\n"+
		"void main(void)" +
		"{"+
			"fragColor = vec4(out_phongADS_Light,1.0f);" +
		"}";

    fragmentShaderObject=gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObject,fragmentShaderSourceCode);

	gl.compileShader(fragmentShaderObject);

	if(gl.getShaderParameter(fragmentShaderObject,gl.COMPILE_STATUS)==false)
	{
        var error;
        error = gl.getShaderInfoLog(fragmentShaderObject);
        if(error.length > 0)
        {
            alert(error);
            unitialize();
            return
        }
	}

	//-------------Shader Program---------------------------
	
	shaderProgramObject = gl.createProgram();

	gl.attachShader(shaderProgramObject,vertexShaderObject);
	gl.attachShader(shaderProgramObject,fragmentShaderObject);
	//Pre Link attachment
	gl.bindAttribLocation(shaderProgramObject,WebGLMacros.VSV_ATTRIBUTE_POSITION,"vPosition");
	gl.bindAttribLocation(shaderProgramObject,WebGLMacros.VSV_ATTRIBUTE_NORMAL,"vNormal");

	gl.linkProgram(shaderProgramObject);

	if(!gl.getProgramParameter(shaderProgramObject,gl.LINK_STATUS))
	{
		var error;
		error = gl.getProgramInfoLog(shaderProgramObject);
        if(error.length > 0)
        {
            alert(error);
            unitialize();
            return
        }		
	}
	//Post Link attachment
    modelMatrixUniform = gl.getUniformLocation(shaderProgramObject, "u_modelMatrix");
	viewMatrixUniform = gl.getUniformLocation(shaderProgramObject, "u_viewMatrix");
	projectionlMatrixUniform = gl.getUniformLocation(shaderProgramObject, "u_projectionMatrix");

	
	lightAmbientUniform[0] = gl.getUniformLocation(shaderProgramObject, "u_lightAmbient[0]");
	lightDiffuseUniform[0] = gl.getUniformLocation(shaderProgramObject, "u_lightDiffuse[0]");
	lightSpecularUniform[0] = gl.getUniformLocation(shaderProgramObject, "u_lightSpecular[0]");
	lightPositionUniform[0] = gl.getUniformLocation(shaderProgramObject, "u_lightPosition[0]");
	
	lightAmbientUniform[1] = gl.getUniformLocation(shaderProgramObject, "u_lightAmbient[1]");
	lightDiffuseUniform[1] = gl.getUniformLocation(shaderProgramObject, "u_lightDiffuse[1]");
	lightSpecularUniform[1] = gl.getUniformLocation(shaderProgramObject, "u_lightSpecular[1]");
	lightPositionUniform[1] = gl.getUniformLocation(shaderProgramObject, "u_lightPosition[1]");

	materialAmbientUniform = gl.getUniformLocation(shaderProgramObject, "u_materialAmbient");
	materialDiffuseUniform = gl.getUniformLocation(shaderProgramObject, "u_materialDiffuse");
	materialSpecularUniform = gl.getUniformLocation(shaderProgramObject, "u_materialSpecular");
	materialShininessUniform = gl.getUniformLocation(shaderProgramObject, "u_materialShininess");

	LKeyPressedUniform = gl.getUniformLocation(shaderProgramObject, "u_lKeyPressed");

	//----------------Data prepration-------------------
    
    var pyramidVertices = new Float32Array([
                                                0.0, 1.0, 0.0,
                                                -1.0, -1.0, 1.0,
                                                1.0, -1.0, 1.0,

                                                0.0, 1.0, 0.0,
                                                1.0, -1.0, 1.0,
                                                1.0, -1.0, -1.0,

                                                0.0, 1.0, 0.0,
                                                1.0, -1.0, -1.0,
                                                -1.0, -1.0, -1.0,

                                                0.0, 1.0, 0.0,
                                                -1.0, -1.0, -1.0,
                                                -1.0, -1.0, 1.0
                                            ]);

    var pyramidNormals= new Float32Array([
			0.0, 0.447214, 0.894427,
			0.0, 0.447214, 0.894427,
			0.0, 0.447214, 0.894427,

			0.894427, 0.447214, 0.0,
			0.894427, 0.447214, 0.0,
			0.894427, 0.447214, 0.0,

			0.0, 0.447214, -0.894427,
			0.0, 0.447214, -0.894427,
			0.0, 0.447214, -0.894427,

			-0.894427, 0.447214, 0.0,
			-0.894427, 0.447214, 0.0,
			-0.894427, 0.447214, 0.0
        ]); 
    

    ///----vao pyramid

    vao_pyramid = gl.createVertexArray();

	gl.bindVertexArray(vao_pyramid);

        vbo_position=gl.createBuffer();
		
		gl.bindBuffer(gl.ARRAY_BUFFER,
					vbo_position);
		
			gl.bufferData(gl.ARRAY_BUFFER,
                        pyramidVertices,
						gl.STATIC_DRAW);

			gl.vertexAttribPointer(WebGLMacros.VSV_ATTRIBUTE_POSITION,
								3,
								gl.FLOAT,
								false,
								0,
								0);
			
			gl.enableVertexAttribArray(WebGLMacros.VSV_ATTRIBUTE_POSITION);

		gl.bindBuffer(gl.ARRAY_BUFFER,
					null);

        vbo_Normal=gl.createBuffer();
		
            gl.bindBuffer(gl.ARRAY_BUFFER,
                vbo_Normal);
                    
                gl.bufferData(gl.ARRAY_BUFFER,
                        pyramidNormals,
						gl.STATIC_DRAW);

                gl.vertexAttribPointer(WebGLMacros.VSV_ATTRIBUTE_NORMAL,
                                    3,
                                    gl.FLOAT,
                                    false,
                                    0,
                                    0);
			
			    gl.enableVertexAttribArray(WebGLMacros.VSV_ATTRIBUTE_NORMAL);

            gl.bindBuffer(gl.ARRAY_BUFFER,
                                null);

	gl.bindVertexArray(null);

	//Initialize ortho matrix
	perspectiveGraphicProjectionMatrix=mat4.create();

	//-----OpenGL state Machine States-----------------------
    gl.clearColor(0.0, 0.0, 0.0, 1.0);//RGBA
	gl.clearDepth(1.0);
	gl.enable(gl.DEPTH_TEST);
	gl.depthFunc(gl.LEQUAL);

}

function resize()
{
    if(bFullscreen==true)
    {
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
    }else{
        canvas.width = canvas_orignal_width;
        canvas.height = canvas_orignal_height;
    }

    gl.viewport(0,0,canvas.width, canvas.height);

    mat4.perspective(perspectiveGraphicProjectionMatrix,
                    45.0,
                    parseFloat(canvas.width)/parseFloat(canvas.height),
                    0.1,
                    100.0);
}
var Angle=0;
function draw()
{
    var modelMatrix = mat4.create();
    var viewMatrix = mat4.create();
    
    gl.clear(gl.COLOR_BUFFER_BIT);

	gl.useProgram(shaderProgramObject);

        //Pyramid
        mat4.translate(modelMatrix,modelMatrix,[0.0,0.0,-4.0]);
        mat4.rotateY(modelMatrix,modelMatrix,degToRad(Angle));

		gl.uniformMatrix4fv(modelMatrixUniform,
							false,
							modelMatrix);

        gl.uniformMatrix4fv(viewMatrixUniform,
							false,
							viewMatrix);

        gl.uniformMatrix4fv(projectionlMatrixUniform,
							false,
							perspectiveGraphicProjectionMatrix);      
                            
        if (bLights === true)
		{
			gl.uniform1i(LKeyPressedUniform, 1);

			for (var i = 0; i < 2; i++)
			{
				gl.uniform3fv(lightAmbientUniform[i], [light[i][0][0],light[i][0][1],light[i][0][2]]);
				gl.uniform3fv(lightDiffuseUniform[i],  [light[i][1][0],light[i][1][1],light[i][1][2]]);
				gl.uniform3fv(lightSpecularUniform[i],  [light[i][2][0],light[i][2][1],light[i][2][2]]);

				gl.uniform4fv(lightPositionUniform[i],  light[i][3]);//posiitional light
			}
			
			gl.uniform3fv(materialAmbientUniform,  [materialAmbient[0],materialAmbient[1],materialAmbient[2]]);
			gl.uniform3fv(materialDiffuseUniform,  [materilaDefuse[0],materilaDefuse[1],materilaDefuse[2]]);
			gl.uniform3fv(materialSpecularUniform,  [materialSpecular[0],materialSpecular[1],materialSpecular[2]]);

			gl.uniform1f(materialShininessUniform, materialShininess);
		}
		else
		{
			gl.uniform1i(LKeyPressedUniform, 0);
		}

		
		gl.bindVertexArray(vao_pyramid);
            gl.drawArrays(gl.TRIANGLES,
                        0,
                        12);
		gl.bindVertexArray(null);

	gl.useProgram(null);
    
    Angle+=3;
    // requestAnimationFrame(update,canvas); //if using update() then use this way
    requestAnimationFrame(draw,canvas);
}

function degToRad(degrees)
{
    return ( degrees*Math.PI /180.0)
}

function unitialize()
{
    if(vao_pyramid)
	{
		gl.deleteVertexArray(vao_pyramid);
		vao_pyramid=null;
	}

	if(vbo_position)
	{
		gl.deleteBuffer(vbo_position);
		vbo_position=null;
	}

    if(vbo_Normal)
	{
		gl.deleteBuffer(vbo_Normal);
		vbo_Normal=null;
	}

	if(shaderProgramObject)
	{
        if(fragmentShaderObject)
        {
            gl.detachShader(shaderProgramObject,fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject=null;
        }
			
        if(vertexShaderObject)
        {
            gl.detachShader(shaderProgramObject,vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject=null;
        }
          
		gl.deleteProgram(shaderProgramObject);
		shaderProgramObject=null;
	}
}

