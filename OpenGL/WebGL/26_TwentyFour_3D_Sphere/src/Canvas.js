//Global
var canvas = null;
var gl = null;

var canvas_orignal_width;
var canvas_orignal_height;
var bFullscreen=false;

const WebGLMacros={
VSV_ATTRIBUTE_POSITION:0,
VSV_ATTRIBUTE_COLOR:1,
VSV_ATTRIBUTE_NORMAL:2,
VSV_ATTRIBUTE_TEXTURE:3,
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var vao_cube;

var vbo_position;
var vbo_Normal;

var mvpMatrixUniform;
var lKeyPressedUniform;

var perspectiveGraphicProjectionMatrix;

var modelViewMatrixUniform;
var projectionMatrixUniform;

var lightAmbientUniform;
var lightDiffuseUniform;
var lightSpecularUniform;
var lightPositionUniform;

var materialAmbientUniform;
var materialDiffuseUniform;
var materialSpecularUniform;
var materialShininessUniform;

var LKeyPressedUniform;

var LightAmbient= [ 0.1,0.1,0.1,1.0 ];
var LightDefuse = [ 1.0,1.0,1.0,1.0 ];
var LightSpecular = [ 1.0,1.0,1.0,1.0 ];
var LightPosition = [ 0.0,0.0,0.0,1.0 ];

var RotationType;//x-0,y-1,z-2

var materialArray=[
    {
        materialAmbient     :   [ 0.0215,0.1745,0.0215,1.0 ],
        materilaDefuse      :   [ 0.07568,0.61424,0.07568,1.0 ],
        materialSpecular    :   [ 0.633,0.727811,0.633,1.0 ],
        materialShininess   :   0.6 * 128
    },
    {
        materialAmbient     :   [ 0.135,0.2225,0.1575,1.0 ],
        materilaDefuse      :   [ 0.54,0.89 ,0.63,1.0 ],
        materialSpecular    :   [ 0.316228,0.316228,0.316228,1.0 ],
        materialShininess   :   0.1 * 128
    },
    {
        materialAmbient     :   [ 0.05375,0.05,0.06625,1.0 ],
        materilaDefuse      :   [ 0.18275,0.17,0.22525,1.0 ],
        materialSpecular    :   [ 0.332741,0.328634,0.346435,1.0 ],
        materialShininess   :   0.3 * 128
    },
    {
        materialAmbient     :   [ 0.25,0.20725,0.20725,1.0 ],
        materilaDefuse      :   [ 1.0,0.829,0.829,1.0 ],
        materialSpecular    :   [ 0.296648,0.296648,0.296648,1.0 ],
        materialShininess   :   0.088 * 128
    },
    {
        materialAmbient     :   [ 0.1745,0.01175,0.01175,1.0 ],
        materilaDefuse      :   [ 0.61424,0.04136,0.04136,1.0 ],
        materialSpecular    :   [ 0.727811,0.626959,0.626959,1.0 ],
        materialShininess   :   0.6 * 128
    },
    {
        materialAmbient     :   [ 0.1,0.18725,0.1745,1.0 ],
        materilaDefuse      :   [ 0.396,0.74151,0.69102,1.0 ],
        materialSpecular    :   [ 0.297254,0.30829,0.306678,1.0 ],
        materialShininess   :   0.1 * 128
    },
    {
        materialAmbient     :   [ 0.329412,0.223529,0.027451,1.0 ],
        materilaDefuse      :   [ 0.780392,0.568627,0.113725,1.0 ],
        materialSpecular    :   [ 0.992157,0.941176,0.807843,1.0 ],
        materialShininess   :   0.21794872 * 128
    },
    {
        materialAmbient     :   [ 0.2125,0.1275,0.054,1.0 ],
        materilaDefuse      :   [ 0.714,0.4284,0.18144,1.0 ],
        materialSpecular    :   [ 0.393548,0.271906,0.166721,1.0 ],
        materialShininess   :   0.2 * 128
    },
    {
        materialAmbient     :   [ 0.25,0.25,0.25,1.0 ],
        materilaDefuse      :   [ 0.4,0.4,0.4,1.0 ],
        materialSpecular    :   [ 0.774597,0.774597,0.774597,1.0 ],
        materialShininess   :   0.6 * 128
    },
    {
        materialAmbient     :   [ 0.19125,0.0735,0.0225,1.0 ],
        materilaDefuse      :   [ 0.7038,0.27048,0.0828,1.0 ],
        materialSpecular    :   [ 0.256777,0.137622,0.086014,1.0],
        materialShininess   :   0.1 * 128
    },
    {
        materialAmbient     :   [ 0.24725,0.1995,0.0745,1.0 ],
        materilaDefuse      :   [ 0.75164,0.60648,0.22648,1.0 ],
        materialSpecular    :   [ 0.628281,0.555802,0.366065,1.0 ],
        materialShininess   :   0.4 * 128
    },
    {
        materialAmbient     :   [ 0.19225,0.19225,0.19225,1.0 ],
        materilaDefuse      :   [ 0.50754,0.50754,0.50754,1.0 ],
        materialSpecular    :   [ 0.508273,0.508273,0.508273,1.0 ],
        materialShininess   :   0.4 * 128
    },
    {
        materialAmbient     :   [ 0.0,0.0,0.0,1.0 ],
        materilaDefuse      :   [ 0.01,0.01,0.01,1.0 ],
        materialSpecular    :   [ 0.50,0.50,0.50,1.0 ],
        materialShininess   :   0.25 * 128
    },
    {
        materialAmbient     :   [ 0.0,0.1,0.06,1.0 ],
        materilaDefuse      :   [ 0.0,0.50980392,0.50980392,1.0 ],
        materialSpecular    :   [ 0.50196078,0.50196078,0.50196078,1.0 ],
        materialShininess   :   0.25 * 128
    },
    {
        materialAmbient     :   [ 0.0,0.0,0.0,1.0 ],
        materilaDefuse      :   [ 0.1,0.35,0.1,1.0],
        materialSpecular    :   [ 0.45,0.55,0.45,1.0 ],
        materialShininess   :   0.25 * 128
    },
    {
        materialAmbient     :   [ 0.0,0.0,0.0,1.0 ],
        materilaDefuse      :   [ 0.5,0.0,0.0,1.0 ],
        materialSpecular    :   [ 0.7,0.6,0.6,1.0 ],
        materialShininess   :   0.25 * 128
    },
    {
        materialAmbient     :   [ 0.0,0.0,0.0,1.0 ],
        materilaDefuse      :   [ 0.55,0.55,0.55,1.0 ],
        materialSpecular    :   [ 0.70,0.70,0.70,1.0 ],
        materialShininess   :   0.25 * 128
    },
    {
        materialAmbient     :   [ 0.0,0.0,0.0,1.0 ],
        materilaDefuse      :   [ 0.5,0.5,0.0,1.0 ],
        materialSpecular    :   [ 0.60,0.60,0.60,1.0 ],
        materialShininess   :   0.25 * 128
    },
    {
        materialAmbient     :   [ 0.02,0.02,0.02,1.0 ],
        materilaDefuse      :   [ 0.01,0.01,0.01,1.0 ],
        materialSpecular    :   [ 0.4,0.4,0.4,1.0 ],
        materialShininess   :   0.078125 * 128
    },
    {
        materialAmbient     :   [ 0.0,0.05,0.05,1.0 ],
        materilaDefuse      :   [ 0.4,0.5,0.5,1.0 ],
        materialSpecular    :   [ 0.04,0.7,0.7,1.0 ],
        materialShininess   :   0.078125 * 128
    },
    {
        materialAmbient     :   [ 0.0,0.05,0.0,1.0 ],
        materilaDefuse      :   [ 0.4,0.5,0.4,1.0 ],
        materialSpecular    :   [ 0.04,0.7,0.04,1.0 ],
        materialShininess   :   0.078125 * 128
    },
    {
        materialAmbient     :   [ 0.05,0.0,0.0,1.0 ],
        materilaDefuse      :   [ 0.5,0.4,0.4,1.0 ],
        materialSpecular    :   [ 0.7,0.04,0.04,1.0 ],
        materialShininess   :   0.078125 * 128
    },
    {
        materialAmbient     :   [ 0.05,0.05,0.05,1.0 ],
        materilaDefuse      :   [ 0.5,0.5,0.5,1.0 ],
        materialSpecular    :   [ 0.7,0.7,0.7,1.0 ],
        materialShininess   :   0.078125 * 128
    },
    {
        materialAmbient     :   [ 0.05,0.05,0.0,1.0 ],
        materilaDefuse      :   [ 0.5,0.5,0.4,1.0 ],
        materialSpecular    :   [ 0.7,0.7,0.04,1.0 ],
        materialShininess   :   0.078125 * 128
    }
];

var bAnimate ;
var bLights ;

var sphere=null;

var requestAnimationFrame = window.requestAnimationFrame || 
                            window.webkitRequestAnimationFrame ||
                            window.mozRequestAnimationFrame ||
                            window.oRequestAnimationFrame || //opera
                            window.msRequestAnimationFrame ||
                            null;

var cancelAnimationFrame =  window.cancelAnimationFrame ||
                            window.webkitCancelRequestAnimationFrame ||  window.webkitCancelAnimation ||
                            window.mozCancelRequestAnimationFrame ||  window.mozCancelAnimationFrame ||
                            window.oCancelRequestAnimationFrame ||  window.oCancelAnimationFrame ||
                            window.msCancelRequestAnimationFrame ||  window.msCancelAnimationFrame ||
                            null;

function main()
{
    //step 1 : Get canvas from html dom
    //document is inbuild variable/DOM object    
    canvas=document.getElementById("canvasID_vsv");
    if(!canvas)
    {
        console.log("ERROR:Failed to obtain Canvas");
        unitialize();
    }else{
        console.log("INFO:Obtained Canvas Successfully");
    }

    //Step 2 : Retreive the height and width of canvas
    //console is inbuild variable/DOM object 
    console.log("INFO:Canvas width=" + canvas.width + " height=" + canvas.height + "\n" );

    canvas_orignal_width = canvas.width;
    canvas_orignal_height = canvas.height;

    //step 3 : Get drawing webgl2 from the canvas
    gl = canvas.getContext("webgl2");
    if(!gl)
    {
        console.log("ERROR:Failed to obtain webgl2 context");
        unitialize();
    }else{
        console.log("INFO:Obtained webgl2 context Successfully");
    }

    if (requestAnimationFrame == null)
    {
        console.log("ERROR:requestAnimationFrame is null");
        unitialize();
        return
    }

    //step 10 : Add event listener
    //window is inbuild variable/DOM object    
    window.addEventListener("keydown",//EVENT TYPE
                            keyDown,//FUNCTION NAME
                            false);//false-captured delegation/propagation or true-bubble delegation/propagation
    
    window.addEventListener("click",
                            mouseDown,
                            false);

    window.addEventListener("resize",resize,false);

    Init();

    resize();//Warm up resize call

    draw();//Warm up redraw call
}

function toggleFullscreen()
{
    var fullscreen_element =    document.fullscreenElement || 
                                document.webkitFullscreenElement || 
                                document.mozFullScreenElement ||
                                document.msFullscreenElement ||
                                null;

    if(fullscreen_element==null)
    {
        if (canvas.requestFullscreen)
        {   
            canvas.requestFullscreen();

        }else if(canvas.webkitRequestFullscreen)
        {
            canvas.webkitRequestFullscreen();

        }else if(canvas.mozRequestFullScreen)
        {
            canvas.mozRequestFullScreen();

        }else if(canvas.msRequestFullscreen)
        {
            canvas.msRequestFullscreen();
        }

        bFullscreen=true;

    }else{
        if(document.exitFullscreen)
        {
            document.exitFullscreen();

        }else if(document.webkitExitFullscreen)
        {
            document.webkitExitFullScreen();

        }else if(document.mozCancelFullScreen)
        {
            document.mozCancelFullScreen();
            
        }else if(document.msExitFullScreen)
        {
            document.msExitFullScreen();
        }

        bFullscreen=false;
    }
}

function keyDown(event)
{
    console.log(event);
   switch(event.keyCode)
   {
        case 70:
            toggleFullscreen();
            break;
        case 27:
            unitialize();
            window.close();
            break;
        case 65:
            bAnimate = !bAnimate;
            break;
        case 76:
            bLights = !bLights;
            break;
        case 88:
			RotationType = 1;
			break;
		case 89:
			RotationType = 2;
			break;
		case 90:
			RotationType = 3;
			break; 
   }
}

function mouseDown()
{
    console.log("INFO:Mouse is pressed");
}

function Init()
{

    gl.viewportWidth = canvas.width;
    gl.viewportHeight = canvas.height;

	var vertexShaderSourceCode=
		"#version 300 es" +
		"\n" +
		"in vec4 vPosition;\n" +
		"in vec3 vNormal;\n" +
		
        "uniform mat4 u_modelViewMatrix;\n" +
		"uniform mat4 u_projectionMatrix;\n" +

        "uniform int u_lKeyPressed;\n"+

        "uniform vec4 u_lightPosition;\n"+

		"out vec3 out_transformedNormal;\n"+
		"out vec3 out_lightDirection;\n"+
		"out vec3 out_viewVector;\n"+
		
		"void main(void)\n" +
		"{\n" +
            "if (u_lKeyPressed == 1)\n"+
            "{\n"+

                "vec4 eyeCoord = u_modelViewMatrix * vPosition;\n"+
                
                "out_transformedNormal= normalize( mat3(u_modelViewMatrix) * vNormal );\n"+

                "out_lightDirection = normalize( vec3( u_lightPosition - eyeCoord ) );\n"+
                
                "out_viewVector = normalize( -eyeCoord.xyz);\n"+
              
            "}\n"+
            
			"gl_Position = u_projectionMatrix * u_modelViewMatrix * vPosition;\n" +
		"}\n";

    vertexShaderObject=gl.createShader(gl.VERTEX_SHADER);

	gl.shaderSource(vertexShaderObject,vertexShaderSourceCode);

	gl.compileShader(vertexShaderObject);

	if(gl.getShaderParameter(vertexShaderObject,gl.COMPILE_STATUS)==false)
	{
		var error;
        error = gl.getShaderInfoLog(vertexShaderObject);
        if(error.length > 0)
        {
            alert(error);
            unitialize();
            return
        }
    }	

	//---------Fragment--------------

	var fragmentShaderSourceCode=
		"#version 300 es" +
		"\n" +
        
        "precision highp float;"+
        "precision highp int;"+
		
        "out vec4 fragColor;"+
		"vec3 phongADS_Light;\n"+

        "in vec3 out_transformedNormal;\n"+
		"in vec3 out_lightDirection;\n"+
		"in vec3 out_viewVector;\n"+

        "uniform vec3 u_lightAmbient;\n"+
		"uniform vec3 u_lightDiffuse;\n"+
		"uniform vec3 u_lightSpecular;\n"+
		
		"uniform vec3 u_materialAmbient;\n"+
		"uniform vec3 u_materialDiffuse;\n"+
		"uniform vec3 u_materialSpecular;\n"+
		"uniform float u_materialShininess;\n"+
		
		"uniform int u_lKeyPressed;\n"+
		
		"void main(void)" +
		"{"+  
            "if (u_lKeyPressed == 1)\n"+
            "{\n"+
                "vec3 normalizedTransformedNormal = normalize( out_transformedNormal );\n"+
                "vec3 normalizedLightDirection = normalize( out_lightDirection );\n"+
                "vec3 normalizedViewVector = normalize( out_viewVector );\n"+

                "vec3 reflectionVector = reflect( -normalizedLightDirection, normalizedTransformedNormal );\n"+
                
                "vec3 ambient = u_lightAmbient * u_materialAmbient;\n"+

                "vec3 diffuse = u_lightDiffuse * u_materialDiffuse * max( dot(normalizedLightDirection , normalizedTransformedNormal), 0.0);\n"+

                "vec3 specular = u_lightSpecular * u_materialSpecular * pow( max( dot(reflectionVector , normalizedViewVector), 0.0f) , u_materialShininess);\n"+

                "phongADS_Light= ambient+ diffuse + specular;\n"+
                
            "}\n"+
            "else\n"+
            "{\n"+
                "phongADS_Light=vec3(1.0f, 1.0f, 1.0f);\n"+
            "}\n"+
        
			"fragColor=vec4(phongADS_Light, 1.0f);"+
		"}";

    fragmentShaderObject=gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObject,fragmentShaderSourceCode);

	gl.compileShader(fragmentShaderObject);

	if(gl.getShaderParameter(fragmentShaderObject,gl.COMPILE_STATUS)==false)
	{
        var error;
        error = gl.getShaderInfoLog(fragmentShaderObject);
        if(error.length > 0)
        {
            alert(error);
            unitialize();
            return
        }
	}

	//-------------Shader Program---------------------------
	
	shaderProgramObject = gl.createProgram();

	gl.attachShader(shaderProgramObject,vertexShaderObject);
	gl.attachShader(shaderProgramObject,fragmentShaderObject);
	//Pre Link attachment
	gl.bindAttribLocation(shaderProgramObject,WebGLMacros.VSV_ATTRIBUTE_POSITION,"vPosition");
	gl.bindAttribLocation(shaderProgramObject,WebGLMacros.VSV_ATTRIBUTE_NORMAL,"vNormal");
	// gl.bindAttribLocation(shaderProgramObject,WebGLMacros.VSV_ATTRIBUTE_TEXTURE,"vTexCoord");

	gl.linkProgram(shaderProgramObject);

	if(!gl.getProgramParameter(shaderProgramObject,gl.LINK_STATUS))
	{
		var error;
		error = gl.getProgramInfoLog(shaderProgramObject);
        if(error.length > 0)
        {
            alert(error);
            unitialize();
            return
        }		
	}
	//Post Link attachment
	modelViewMatrixUniform = gl.getUniformLocation(shaderProgramObject,"u_modelViewMatrix");
	projectionMatrixUniform = gl.getUniformLocation(shaderProgramObject,"u_projectionMatrix");

    lightAmbientUniform = gl.getUniformLocation(shaderProgramObject,"u_lightAmbient");
	lightDiffuseUniform = gl.getUniformLocation(shaderProgramObject,"u_lightDiffuse");
	lightSpecularUniform = gl.getUniformLocation(shaderProgramObject,"u_lightSpecular");
	lightPositionUniform = gl.getUniformLocation(shaderProgramObject,"u_lightPosition");

	materialAmbientUniform = gl.getUniformLocation(shaderProgramObject,"u_materialAmbient");
	materialDiffuseUniform = gl.getUniformLocation(shaderProgramObject,"u_materialDiffuse");
	materialSpecularUniform = gl.getUniformLocation(shaderProgramObject,"u_materialSpecular");
	materialShininessUniform = gl.getUniformLocation(shaderProgramObject,"u_materialShininess");

	LKeyPressedUniform = gl.getUniformLocation(shaderProgramObject,"u_lKeyPressed");


	//----------------Data prepration-------------------
    ///----vao 
    sphere=new Mesh();
    makeSphere(sphere,2,50,50);
    
	//Initialize ortho matrix
	perspectiveGraphicProjectionMatrix=mat4.create();

	//-----OpenGL state Machine States-----------------------
    gl.clearColor(0.0, 0.0, 0.0, 1.0);//RGBA
	gl.clearDepth(1.0);
	gl.enable(gl.DEPTH_TEST);
	gl.depthFunc(gl.LEQUAL);

}

function resize()
{
    if(bFullscreen==true)
    {
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
    }else{
        canvas.width = canvas_orignal_width;
        canvas.height = canvas_orignal_height;
    }

    gl.viewport(0,0,canvas.width, canvas.height);

    mat4.perspective(perspectiveGraphicProjectionMatrix,
                    45.0,
                    parseFloat(canvas.width)/parseFloat(canvas.height),
                    0.1,
                    100.0);
}

var Angle = 0.0;

var radius = 6.0;

function draw()
{
    var modelViewProjectMatrix = mat4.create();
    var modelViewMatrix = mat4.create();
    
    gl.clear(gl.COLOR_BUFFER_BIT);

	gl.useProgram(shaderProgramObject);

        var unit_width = canvas.width / 6;
        var unit_height = canvas.height  / 4;
		var k = 0;

        for (var i = 0; i < 4; i++)
		{

			for (var j = 0; j < 6; j++)
			{			
                gl.viewport(j * unit_width, i * unit_height, unit_width, canvas.height / 6);

                mat4.translate(modelViewMatrix,modelViewMatrix,[0.0,0.0,-5.0]);

                gl.uniformMatrix4fv(modelViewMatrixUniform,
                                    false,
                                    modelViewMatrix);
                
                gl.uniformMatrix4fv(projectionMatrixUniform,
                                    false,
                                    perspectiveGraphicProjectionMatrix);

                if (bLights == true)
                {
                    gl.uniform1i(LKeyPressedUniform, 1);

                    gl.uniform3fv(lightAmbientUniform, [LightAmbient[0],LightAmbient[1],LightAmbient[2]]);
                    gl.uniform3fv(lightDiffuseUniform,  [LightDefuse[0],LightDefuse[1],LightDefuse[2]]);
                    gl.uniform3fv(lightSpecularUniform,  [LightSpecular[0],LightSpecular[1],LightSpecular[2]]);
                    
                    if (RotationType == 1) {

						LightPosition[0] = 0.0;
						LightPosition[1] = radius * Math.sin(Angle);
						LightPosition[2] = radius * Math.cos(Angle);
						LightPosition[3] = 1.0;

					}
					else if (RotationType == 2) {

						LightPosition[0] = radius * Math.sin(Angle);
						LightPosition[1] = 0.0;
						LightPosition[2] = radius * Math.cos(Angle);
						LightPosition[3] = 1.0;

					}
					else if (RotationType == 3) {

						LightPosition[0] = radius * Math.sin(Angle);
						LightPosition[1] = radius * Math.cos(Angle);
						LightPosition[2] = 0.0,
						LightPosition[3] = 1.0;

					}
					else if (RotationType == 0)
					{
						LightPosition[0] = radius;
						LightPosition[1] = radius;
						LightPosition[2] = radius,
						LightPosition[3] = 1.0;
					}

                    gl.uniform4fv(lightPositionUniform,  LightPosition);//posiitional light

                    gl.uniform3fv(materialAmbientUniform,  [materialArray[k].materialAmbient[0],materialArray[k].materialAmbient[1],materialArray[k].materialAmbient[2]]);
                    gl.uniform3fv(materialDiffuseUniform,  [materialArray[k].materilaDefuse[0],materialArray[k].materilaDefuse[1],materialArray[k].materilaDefuse[2]]);
                    gl.uniform3fv(materialSpecularUniform,  [materialArray[k].materialSpecular[0],materialArray[k].materialSpecular[1],materialArray[k].materialSpecular[2]] );

                    gl.uniform1f(materialShininessUniform, materialArray[k].materialShininess);
                }
                else
                {
                    gl.uniform1i(LKeyPressedUniform, 0);
                }   
                modelViewMatrix= mat4.create();
                sphere.draw();
                k++;
            }
        }

	gl.useProgram(null);

    // requestAnimationFrame(update,canvas); //if using update() then use this way
    requestAnimationFrame(draw,canvas);
    Angle += 0.05;

}

function degToRad(degrees)
{
    return ( degrees*Math.PI /180.0)
}

function unitialize()
{
    sphere.deallocate();

	if(shaderProgramObject)
	{
        if(fragmentShaderObject)
        {
            gl.detachShader(shaderProgramObject,fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject=null;
        }
			
        if(vertexShaderObject)
        {
            gl.detachShader(shaderProgramObject,vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject=null;
        }
          
		gl.deleteProgram(shaderProgramObject);
		shaderProgramObject=null;
	}
}

