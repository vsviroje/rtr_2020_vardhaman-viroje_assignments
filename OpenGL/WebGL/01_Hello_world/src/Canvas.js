function main()
{
    //step 1 : Get canvas from html dom
    var canvas=document.getElementById("canvasID_vsv");
    if(!canvas)
    {
        console.log("ERROR:Failed to obtain Canvas");
    }else{
        console.log("INFO:Obtained Canvas Successfully");
    }

    //Step 2 : Retreive the height and width of canvas
    console.log("INFO:Canvas width=" + canvas.width + " height=" + canvas.height + "\n" );

    //step 3 : Get drawing context from the canvas
    var context = canvas.getContext("2d");
    if(!context)
    {
        console.log("ERROR:Failed to obtain Context");
    }else{
        console.log("INFO:Obtained Context Successfully");
    }

    //step 4 : paint background by black color
    context.fillStyle="black";
    context.fillRect(0,0,canvas.width,canvas.height);

    //step 5 : Set Text position at center
    context.textAlign="center";     //horizontal
    context.baseline="middle";      //vertical

    //step 6 : Set font of text
    context.font="48px sans-serrif";

    //step 7 : Delcare string to be displayed for text
    var str="!!!Hello World!!!";

    //step 8 : Color text for foreground
    context.fillStyle="white";

    //step 9 : Display the text
    context.fillText(str,canvas.width/2,canvas.height/2);
}