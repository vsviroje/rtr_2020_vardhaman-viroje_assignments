#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<memory.h>

#include<GL/glew.h>
#include<GL/glx.h>
#include<GL/gl.h>

#include"vmath.h"

#include<X11/Xlib.h>
#include<X11/Xutil.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>

#include<SOIL/SOIL.h>

using namespace std;
using namespace vmath;

bool bFullScreen=false;
Display *gpDisplay=NULL;
XVisualInfo *gpXVisualInfo=NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth=800;
int giWindowHeight=600;

GLXContext gGLXContext;

//PP Changes
typedef GLXContext (*glxCreateContextAttribsARBProc)(Display *, GLXFBConfig, GLXContext, Bool, const int *);
glxCreateContextAttribsARBProc glxCreateContextAttribsARB=NULL;

GLXFBConfig gGLXFBConfig;

//Shader Changes

enum{
	VSV_ATTRIBUTE_POSITION=0,
	VSV_ATTRIBUTE_COLOR,
	VSV_ATTRIBUTE_TEXCOORD,
	VSV_ATTRIBUTE_NORMAL,
};

GLuint gVertexShaderObject_simple;
GLuint gFragmentShaderObject_simple;
GLuint gShaderProgramObject_simple;

GLuint gVertexShaderObject_render_to_texture;
GLuint gFragmentShaderObject_render_to_texture;
GLuint gShaderProgramObject_render_to_texture;

GLuint vao_pyramid;
GLuint vao_cube;

GLuint vbo_pyramidColor;
GLuint vbo_pyramidPosition;
GLuint vbo_pyramidTexture;

GLuint vbo_cubePosition;
GLuint vbo_cubeColor;
GLuint vbo_cubeTexture;

GLuint mvpMatrixUniform_simple;
GLuint mvpMatrixUniform_render_to_texture;

GLuint textureSamplerUniform_simple;
GLuint textureSamplerUniform_render_to_texture;

//Frame_buffer changes
GLuint gFrameBuffers = 0;
GLuint gColorTexture = 0;
GLuint gDepthTexture = 0;

int gWidth, gHeight;


//common
mat4 perspectiveProjectionMatrix;

GLuint Kundali_Texture, Stone_Texture;//texture variable

int main(void)
{
	void CreateWindow(void);
	void ToggleFullscreen(void);
	void UnInitialize(void);

	void Initialize(void);
	void Resize(int,int);
	void Draw(void);

	bool bDone=false;

	int winWidth=giWindowWidth;
	int winHeight=giWindowHeight;

	CreateWindow();
	Initialize();

	XEvent event;
	KeySym keysym;

	while(bDone==false)
	{
		while(XPending(gpDisplay))
		{
			XNextEvent(gpDisplay,
						&event);

			switch(event.type)
			{
				case MapNotify:
					break;
				case KeyPress:
					keysym=XkbKeycodeToKeysym(gpDisplay,
												event.xkey.keycode,
												0,
												0);
					switch(keysym)
					{
						case XK_Escape:
							bDone=true;
							break;

						case XK_F:
						case XK_f:
							if(bFullScreen==false)
							{
								ToggleFullscreen();
								bFullScreen=true;
							}else
							{
								ToggleFullscreen();
								bFullScreen=false;
							}
							break;
						default:
							break;
					}
					break;

				case MotionNotify:
					break;
				case ConfigureNotify:
					winWidth=event.xconfigure.width;
					winHeight=event.xconfigure.height;
					Resize(winWidth,winHeight);
					break;

				case Expose:
					break;
				case 33:
					bDone=true;
					break;
				default:
					break;
			}

		}
		Draw();
	}
	UnInitialize();
	return(0);
}

void CreateWindow(void)
{
	void UnInitialize(void);
	XSetWindowAttributes winAttribs;
	int defaultScreen;
	int styleMask;

	//PP Changes
	GLXFBConfig *pGLXFBConfig=NULL;
	GLXFBConfig bestGLXFBConfig;
	XVisualInfo *pTempXVisualInfo=NULL;
	int numFBConfigs=0;

	static int FrameBufferAttributes[]={
										GLX_X_RENDERABLE,True,
										GLX_DRAWABLE_TYPE,GLX_WINDOW_BIT,
										GLX_RENDER_TYPE,GLX_RGBA_BIT,
										GLX_X_VISUAL_TYPE,GLX_TRUE_COLOR,

										GLX_RED_SIZE,8,
										GLX_GREEN_SIZE,8,
										GLX_BLUE_SIZE,8,
										GLX_ALPHA_SIZE,8,
										
										GLX_STENCIL_SIZE,8,
										
										GLX_DOUBLEBUFFER,True,
										GLX_DEPTH_SIZE,24,
										None};	

	gpDisplay=XOpenDisplay(NULL);
	if(gpDisplay==NULL)
	{
		printf("\nERROR:Unable to Open X Display\n");
		UnInitialize();
		exit(1);
	}

	defaultScreen=XDefaultScreen(gpDisplay);
	
	//PP Changes
	pGLXFBConfig = glXChooseFBConfig(gpDisplay,
									defaultScreen,
									FrameBufferAttributes,
									&numFBConfigs);
	if(numFBConfigs == 0)
	{
		printf("\nERROR:%d-Number of Frame Buffer Config.So no need to proceed futher.\n",numFBConfigs);
		UnInitialize();
		exit(1);
	}

	printf("\nINFO:%d-Found Number of Frame Buffer Config.\n",numFBConfigs);


	int bestFrameBufferConfig=-1;
	int worstFrameBufferConfig=-1;
	int bestNumberOfSamples=-1;
	int worstNumberOfSamples=999;

	for(int i=0; i < numFBConfigs; i++)
	{
		pTempXVisualInfo=glXGetVisualFromFBConfig(gpDisplay,
												pGLXFBConfig[i]);

		if(pTempXVisualInfo != NULL)
		{
			int SampleBuffers,Samples;

			glXGetFBConfigAttrib(gpDisplay,
								pGLXFBConfig[i],
								GLX_SAMPLE_BUFFERS,
								&SampleBuffers);

			glXGetFBConfigAttrib(gpDisplay,
								pGLXFBConfig[i],
								GLX_SAMPLES,
								&Samples);

			if(bestFrameBufferConfig < 0 || SampleBuffers && Samples > bestNumberOfSamples )
			{
				bestFrameBufferConfig = i;
				bestNumberOfSamples = Samples;
			}

			if(worstFrameBufferConfig < 0 || !SampleBuffers || Samples < worstNumberOfSamples)
			{
				worstFrameBufferConfig = i;
				worstNumberOfSamples = Samples;
			}

			printf("\nINFO:When i = %d, Samples = %d, SampleBuffers = %d, pTempXVisualInfo->VisualID = %ld \n",i, Samples, SampleBuffers, pTempXVisualInfo->visualid);
		}

		XFree(pTempXVisualInfo);
		pTempXVisualInfo = NULL;
	}
	
	printf("\nINFO:bestFrameBufferConfig index = %d \n",bestFrameBufferConfig);

	bestGLXFBConfig = pGLXFBConfig[bestFrameBufferConfig];

	gGLXFBConfig = bestGLXFBConfig;

	XFree(pGLXFBConfig);
	pGLXFBConfig = NULL;

	gpXVisualInfo=(XVisualInfo *)malloc(sizeof(XVisualInfo));
	if(gpXVisualInfo==NULL)
	{
		printf("\nERROR:Unable to allocate visual info\n");
		UnInitialize();
		exit(1);
	}

	gpXVisualInfo = glXGetVisualFromFBConfig(gpDisplay,
											bestGLXFBConfig);

	winAttribs.border_pixel=0;
	winAttribs.background_pixmap=0;
	winAttribs.colormap=XCreateColormap(gpDisplay,
										RootWindow(gpDisplay,
													gpXVisualInfo->screen),
										gpXVisualInfo->visual,
										AllocNone);

	gColormap=winAttribs.colormap;

	winAttribs.background_pixel=BlackPixel(gpDisplay,
											defaultScreen);

	winAttribs.event_mask=ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;

	styleMask=CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

	gWindow=XCreateWindow(	gpDisplay,
							RootWindow(gpDisplay,
										gpXVisualInfo->screen),
							0,
							0,
							giWindowWidth,
							giWindowHeight,
							0,
							gpXVisualInfo->depth,
							InputOutput,
							gpXVisualInfo->visual,
							styleMask,
							&winAttribs);

	if(!gWindow)
	{
		printf("\nERROR:Failed to Create main window\n");
		UnInitialize();
		exit(1);
	}

	XStoreName(gpDisplay,
				gWindow,
				"XWindow-PP Render To texture");

	Atom windowManagerDelete=XInternAtom(gpDisplay,
										"WM_DELETE_WINDOW",
										True);

	XSetWMProtocols(gpDisplay,
					gWindow,
					&windowManagerDelete,
					1);

	XMapWindow(gpDisplay,
				gWindow);

}

void ToggleFullscreen(void)
{
	Atom wm_state;
	Atom fullscreen;
	XEvent xev={0};

	wm_state=XInternAtom(gpDisplay,
						"_NET_WM_STATE",
						False);

	memset(&xev,
			0,
			sizeof(xev));

	xev.type=ClientMessage;
	xev.xclient.window=gWindow;
	xev.xclient.message_type=wm_state;
	xev.xclient.format=32;
	xev.xclient.data.l[0]=bFullScreen?0:1;

	fullscreen=XInternAtom(gpDisplay,
							"_NET_WM_STATE_FULLSCREEN",
							False);

	xev.xclient.data.l[1]=fullscreen;

	XSendEvent(gpDisplay,
				RootWindow(gpDisplay,
							gpXVisualInfo->screen),
				False,
				StructureNotifyMask,
				&xev);
}

void Initialize(void)
{

	void Resize(int,int);
	void UnInitialize(void);
	GLuint LoadBitmapTexture(const char *);

	glxCreateContextAttribsARB = (glxCreateContextAttribsARBProc)glXGetProcAddressARB((GLubyte *)"glXCreateContextAttribsARB");

	const int attribs[] = { GLX_CONTEXT_MAJOR_VERSION_ARB, 4,
							GLX_CONTEXT_MINOR_VERSION_ARB, 5,
							GLX_CONTEXT_PROFILE_MASK_ARB, GLX_CONTEXT_CORE_PROFILE_BIT_ARB,
							None};

	gGLXContext = glxCreateContextAttribsARB(gpDisplay,
											gGLXFBConfig,
											0,
											True,
											attribs);

	if(!gGLXContext)
	{
		const int lowestAttribs[] = { 	GLX_CONTEXT_MAJOR_VERSION_ARB, 1,
										GLX_CONTEXT_MINOR_VERSION_ARB, 0,
										None};

		gGLXContext = glxCreateContextAttribsARB(gpDisplay,
											gGLXFBConfig,
											0,
											True,
											lowestAttribs);

			printf("\nINFO:Got 1.0 OpenGL Context Version\n");

	}else{
			printf("\nINFO:Got 4.5 OpenGL Context Version\n");
	}



	if(gGLXContext==NULL)
	{
		printf("\nERROR:Unable to create Rendering Context\n");
		UnInitialize();
		exit(1);
	}

	Bool isDirectContext = glXIsDirect(gpDisplay,
										gGLXContext);

	if (isDirectContext == True)
	{
		printf("\nINFO:Rendering Context is direct Hardware rendering context.\n");
	}else{
		printf("\nINFO:Rendering Context is Software rendering context.\n");
	}

	glXMakeCurrent(gpDisplay,
					gWindow,
					gGLXContext);

	GLenum glew_error=glewInit();
	if(glew_error != GLEW_OK)
	{
		printf("\nglewInit failed to enable extension for Programmable Pipeline");
		UnInitialize();
		exit(1);
	}

	printf( "\nOpenGL Information are availble below:-");
	printf( "\n*****************************************************");

	printf( "\nOpenGL Vender:%s",glGetString(GL_VENDOR));
	printf( "\nOpenGL Renderer:%s",glGetString(GL_RENDERER));
	printf( "\nOpenGL Version:%s",glGetString(GL_VERSION));
	printf( "\nOpenGL GLSL Version:%s",glGetString(GL_SHADING_LANGUAGE_VERSION));
	
	printf("\nOpenGL Extension's list:\n");
	GLint numExt;
	glGetIntegerv(GL_NUM_EXTENSIONS,&numExt);

	for(int i=0;i<numExt;i++)
	{
		printf( "\n%s",glGetStringi(GL_EXTENSIONS,i));
	}
	printf( "\n*****************************************************\n");
		
	GLint infoLogLength;
	GLint shaderCompileStatus;
	GLint shaderProgramLinkStatus;
	GLchar *szBuffer;
	GLsizei written;
	
	//Shader Code Start--------------------
	gVertexShaderObject_simple =glCreateShader(GL_VERTEX_SHADER);

		const GLchar *vertexShaderSourceCode=
			"#version 460 core" \
			"\n" \
			
			"in vec4 vPosition;"\
			"in vec4 vColor;"\
			"in vec2 vTexCoord;"\
			"out vec4 out_color;"\
			"out vec2 out_TexCoord;"\
			"uniform mat4 u_mvpMatrix;"\
			"void main(void)" \
			"{" \
				"out_color=vColor;"\
				"out_TexCoord=vTexCoord;"\
				"gl_Position=u_mvpMatrix * vPosition;"\
			"}";

		glShaderSource(gVertexShaderObject_simple,
						1,
						(const GLchar **)&vertexShaderSourceCode,
						NULL);

		glCompileShader(gVertexShaderObject_simple);

		glGetShaderiv(gVertexShaderObject_simple,
						GL_COMPILE_STATUS,
						&shaderCompileStatus);

						
		if(shaderCompileStatus==GL_FALSE)
		{
			glGetShaderiv(gVertexShaderObject_simple,
							GL_INFO_LOG_LENGTH,
							&infoLogLength);
			if(infoLogLength>0)
			{
				szBuffer=(GLchar*)malloc(sizeof(GLchar) * infoLogLength);

				if(szBuffer!=NULL)
				{
					glGetShaderInfoLog(gVertexShaderObject_simple,
										infoLogLength,
										&written,
										szBuffer);
					
					printf("\nERROR:Vertex shader compilation failed log: %s",szBuffer);
					free(szBuffer);
					UnInitialize();
					exit(1);
				}
			}
		}

		infoLogLength=0;
		shaderCompileStatus=0;
		shaderProgramLinkStatus=0;
		szBuffer=NULL;
		written=0;

		gFragmentShaderObject_simple =glCreateShader(GL_FRAGMENT_SHADER);

		const GLchar *fragmentShaderSourceCode=
			"#version 450" \
			"\n" \

			"in vec4 out_color;"\
			"in vec2 out_TexCoord;"\
			"uniform sampler2D u_texture_sampler;"\
			"out vec4 fragColor;"\
			"void main(void)" \
			"{"\
				"fragColor=out_color;"\
				"fragColor=texture(u_texture_sampler,out_TexCoord);"\
			"}";

		glShaderSource(gFragmentShaderObject_simple,
						1,
						(const GLchar**)&fragmentShaderSourceCode,
						NULL);

		glCompileShader(gFragmentShaderObject_simple);

		glGetShaderiv(gFragmentShaderObject_simple,
					GL_COMPILE_STATUS,
					&shaderCompileStatus);

		
		if(shaderCompileStatus==GL_FALSE)
		{
			glGetShaderiv(gFragmentShaderObject_simple,
							GL_INFO_LOG_LENGTH,
							&infoLogLength);

			if(infoLogLength>0)
			{
				szBuffer=(GLchar*)malloc(sizeof(GLchar) * infoLogLength);

				if(szBuffer!=NULL)
				{
					glGetShaderInfoLog(gFragmentShaderObject_simple,
										infoLogLength,
										&written,
										szBuffer);
					
					printf("\nERROR:Fragment shader compilation failed log: %s",szBuffer);
					free(szBuffer);
					UnInitialize();
					exit(1);
				}
			}
		}
		
		infoLogLength=0;
		shaderCompileStatus=0;
		shaderProgramLinkStatus=0;
		szBuffer=NULL;
		written=0;

		gShaderProgramObject_simple =glCreateProgram();

		glAttachShader(gShaderProgramObject_simple, gVertexShaderObject_simple);
		glAttachShader(gShaderProgramObject_simple, gFragmentShaderObject_simple);
		//Shader Attribute Binding Before Pre Linking of attached Shader
			glBindAttribLocation(gShaderProgramObject_simple,VSV_ATTRIBUTE_POSITION,"vPosition");
			glBindAttribLocation(gShaderProgramObject_simple,VSV_ATTRIBUTE_COLOR,"vColor");
			glBindAttribLocation(gShaderProgramObject_simple, VSV_ATTRIBUTE_TEXCOORD, "vTexCoord");

		glLinkProgram(gShaderProgramObject_simple);

		glGetProgramiv(gShaderProgramObject_simple,
						GL_LINK_STATUS,
						&shaderProgramLinkStatus);
		if(shaderProgramLinkStatus==GL_FALSE)
		{
			glGetProgramiv(gShaderProgramObject_simple,
						GL_INFO_LOG_LENGTH,
						&infoLogLength);
			if(infoLogLength>0)
			{
				szBuffer=(GLchar*)malloc(sizeof(GLchar)*infoLogLength);
				if(szBuffer!=NULL)
				{
					glGetProgramInfoLog(gShaderProgramObject_simple,
										infoLogLength,
										&written,
										szBuffer);

					printf("\nERROR:Shader Program Link failed log: %s",szBuffer);
					free(szBuffer);
					UnInitialize();
					exit(1);
				}
			}
		}
		//Uniform variable Binding after post linking of attached shader
		mvpMatrixUniform_simple =glGetUniformLocation(gShaderProgramObject_simple,
												"u_mvpMatrix");

		textureSamplerUniform_simple = glGetUniformLocation(gShaderProgramObject_simple,
							"u_texture_sampler");
		
	//Shader Code End--------------------


	//Shader Code Start--------------------
		gVertexShaderObject_render_to_texture = glCreateShader(GL_VERTEX_SHADER);

		const GLchar* vertexShaderSourceCode_render_to_texture =
			"#version 460 core" \
			"\n" \
			"in vec4 vPosition;"\
			"in vec4 vColor;"\
			"in vec2 vTexCoord;"\

			"out vec4 out_color;"\
			"out vec2 out_TexCoord;"\

			"uniform mat4 u_mvpMatrix;"\
			"void main(void)" \
			"{" \
				"out_color=vColor;"\
				"out_TexCoord=vTexCoord;"\

				"gl_Position=u_mvpMatrix * vPosition;"\
			"}";

		glShaderSource(gVertexShaderObject_render_to_texture,
			1,
			(const GLchar**)&vertexShaderSourceCode_render_to_texture,
			NULL);

		glCompileShader(gVertexShaderObject_render_to_texture);

		glGetShaderiv(gVertexShaderObject_render_to_texture,
			GL_COMPILE_STATUS,
			&shaderCompileStatus);


		if (shaderCompileStatus == GL_FALSE)
		{
			glGetShaderiv(gVertexShaderObject_render_to_texture,
				GL_INFO_LOG_LENGTH,
				&infoLogLength);
			if (infoLogLength > 0)
			{
				szBuffer = (GLchar*)malloc(sizeof(GLchar) * infoLogLength);

				if (szBuffer != NULL)
				{
					glGetShaderInfoLog(gVertexShaderObject_render_to_texture,
						infoLogLength,
						&written,
						szBuffer);

					printf( "\nERROR:Vertex shader compilation failed log: %s", szBuffer);
					free(szBuffer);
					UnInitialize();
					exit(1);
				}
			}
		}

		infoLogLength = 0;
		shaderCompileStatus = 0;
		shaderProgramLinkStatus = 0;
		szBuffer = NULL;
		written = 0;

		gFragmentShaderObject_render_to_texture = glCreateShader(GL_FRAGMENT_SHADER);

		const GLchar* fragmentShaderSourceCode_render_to_texture =
			"#version 450" \
			"\n" \
			"in vec2 out_TexCoord;"\
			"in vec4 out_color;"\
			"out vec4 fragColor;"\
			"uniform sampler2D u_texture_sampler;"\
			"void main(void)" \
			"{"\
				"fragColor=out_color;"\
				"fragColor=texture(u_texture_sampler,out_TexCoord);"\
			"}";

		glShaderSource(gFragmentShaderObject_render_to_texture,
			1,
			(const GLchar**)&fragmentShaderSourceCode_render_to_texture,
			NULL);

		glCompileShader(gFragmentShaderObject_render_to_texture);

		glGetShaderiv(gFragmentShaderObject_render_to_texture,
			GL_COMPILE_STATUS,
			&shaderCompileStatus);


		if (shaderCompileStatus == GL_FALSE)
		{
			glGetShaderiv(gFragmentShaderObject_render_to_texture,
				GL_INFO_LOG_LENGTH,
				&infoLogLength);

			if (infoLogLength > 0)
			{
				szBuffer = (GLchar*)malloc(sizeof(GLchar) * infoLogLength);

				if (szBuffer != NULL)
				{
					glGetShaderInfoLog(gFragmentShaderObject_render_to_texture,
						infoLogLength,
						&written,
						szBuffer);

					printf( "\nERROR:Fragment shader compilation failed log: %s", szBuffer);
					free(szBuffer);
					UnInitialize();
					exit(1);
				}
			}
		}

		infoLogLength = 0;
		shaderCompileStatus = 0;
		shaderProgramLinkStatus = 0;
		szBuffer = NULL;
		written = 0;

		gShaderProgramObject_render_to_texture = glCreateProgram();

		glAttachShader(gShaderProgramObject_render_to_texture, gVertexShaderObject_render_to_texture);
		glAttachShader(gShaderProgramObject_render_to_texture, gFragmentShaderObject_render_to_texture);
		//Shader Attribute Binding Before Pre Linking of attached Shader
		glBindAttribLocation(gShaderProgramObject_render_to_texture, VSV_ATTRIBUTE_POSITION, "vPosition");
		glBindAttribLocation(gShaderProgramObject_render_to_texture, VSV_ATTRIBUTE_COLOR, "vColor");
		glBindAttribLocation(gShaderProgramObject_render_to_texture, VSV_ATTRIBUTE_TEXCOORD, "vTexCoord");

		glLinkProgram(gShaderProgramObject_render_to_texture);

		glGetProgramiv(gShaderProgramObject_render_to_texture,
			GL_LINK_STATUS,
			&shaderProgramLinkStatus);
		if (shaderProgramLinkStatus == GL_FALSE)
		{
			glGetProgramiv(gShaderProgramObject_render_to_texture,
				GL_INFO_LOG_LENGTH,
				&infoLogLength);
			if (infoLogLength > 0)
			{
				szBuffer = (GLchar*)malloc(sizeof(GLchar) * infoLogLength);
				if (szBuffer != NULL)
				{
					glGetProgramInfoLog(gShaderProgramObject_render_to_texture,
						infoLogLength,
						&written,
						szBuffer);

					printf( "\nERROR:Shader Program Link failed log: %s", szBuffer);
					free(szBuffer);
					UnInitialize();
					exit(1);
				}
			}
		}
		//Uniform variable Binding after post linking of attached shader
		mvpMatrixUniform_render_to_texture = glGetUniformLocation(gShaderProgramObject_render_to_texture,
			"u_mvpMatrix");
		textureSamplerUniform_render_to_texture = glGetUniformLocation(gShaderProgramObject_render_to_texture,
													"u_texture_sampler");

		//Shader Code End--------------------


	//VAO and its respective VBO preparation
		const GLfloat pyramidVertices[]=
		{
			0.0f, 1.0f, 0.0f,
			-1.0f, -1.0f, 1.0f,
			1.0f, -1.0f, 1.0f,

			0.0f, 1.0f, 0.0f,
			1.0f, -1.0f, 1.0f,
			1.0f, -1.0f, -1.0f,

			0.0f, 1.0f, 0.0f,
			1.0f, -1.0f, -1.0f,
			-1.0f, -1.0f, -1.0f,

			0.0f, 1.0f, 0.0f,
			-1.0f, -1.0f, -1.0f,
			-1.0f, -1.0f, 1.0f
		};

		const GLfloat pyramidColors[]=
		{
			1.0f, 0.0f, 0.0f,
			0.0f, 1.0f, 0.0f,
			0.0f, 0.0f, 1.0f,

			1.0f, 0.0f, 0.0f,
			0.0f, 0.0f, 1.0f,
			0.0f, 1.0f, 0.0f,

			1.0f, 0.0f, 0.0f,
			0.0f, 1.0f, 0.0f,
			0.0f, 0.0f, 1.0f,

			1.0f, 0.0f, 0.0f,
			0.0f, 0.0f, 1.0f,
			0.0f, 1.0f, 0.0f
		};

		const GLfloat pyramidTexCoord[] =
		{
			0.5f,1.0f,
			0.0f,0.0f,
			1.0f,0.0f,

			0.5f,1.0f,
			1.0f,0.0f,
			0.0f,0.0f,

			0.5f,1.0f,
			1.0f,0.0f,
			0.0f,0.0f,

			0.5f,1.0f,
			0.0f,0.0f,
			1.0f,0.0f
		};

		const GLfloat cubeVertices[]=
		{
			1.0f, 1.0f, 1.0f,
			-1.0f, 1.0f, 1.0f,
			-1.0f, -1.0f, 1.0f,
			1.0f, -1.0f, 1.0f,

			1.0f, 1.0f, -1.0f,
			1.0f, 1.0f, 1.0f,
			1.0f, -1.0f, 1.0f,
			1.0f, -1.0f, -1.0f,

			-1.0f, 1.0f, -1.0f,
			1.0f, 1.0f, -1.0f,
			1.0f, -1.0f, -1.0f,
			-1.0f, -1.0f, -1.0f,

			-1.0f, 1.0f, 1.0f,
			-1.0f, 1.0f, -1.0f,
			-1.0f, -1.0f, -1.0f,
			-1.0f, -1.0f, 1.0f,

			1.0f, 1.0f, -1.0f,
			-1.0f, 1.0f, -1.0f,
			-1.0f, 1.0f, 1.0f,
			1.0f, 1.0f, 1.0f,

			1.0f, -1.0f, -1.0f,
			-1.0f, -1.0f, -1.0f,
			-1.0f, -1.0f, 1.0f,
			1.0f, -1.0f, 1.0f
		};

		const GLfloat cubeColors[]=
		{
			1.0f, 0.0f, 0.0f,
			1.0f, 0.0f, 0.0f,
			1.0f, 0.0f, 0.0f,
			1.0f, 0.0f, 0.0f,
			
			0.0f, 1.0f, 0.0f,
			0.0f, 1.0f, 0.0f,
			0.0f, 1.0f, 0.0f,
			0.0f, 1.0f, 0.0f,

			0.0f, 0.0f, 1.0f,
			0.0f, 0.0f, 1.0f,
			0.0f, 0.0f, 1.0f,
			0.0f, 0.0f, 1.0f,

			0.0f, 1.0f, 1.0f,
			0.0f, 1.0f, 1.0f,
			0.0f, 1.0f, 1.0f,
			0.0f, 1.0f, 1.0f,

			1.0f, 0.0f, 1.0f,
			1.0f, 0.0f, 1.0f,
			1.0f, 0.0f, 1.0f,
			1.0f, 0.0f, 1.0f,

			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f
		};


		const GLfloat cubeTexCoord[] =
		{
			0.0f,0.0f,
			1.0f,0.0f,
			1.0f,1.0f,
			0.0f,1.0f,

			1.0f,0.0f,
			1.0f,1.0f,
			0.0f,1.0f,
			0.0f,0.0f,

			1.0f,0.0f,
			1.0f,1.0f,
			0.0f,1.0f,
			0.0f,0.0f,

			0.0f,0.0f,
			1.0f,0.0f,
			1.0f,1.0f,
			0.0f,1.0f,

			0.0f,1.0f,
			0.0f,0.0f,
			1.0f,0.0f,
			1.0f,1.0f,

			1.0f,1.0f,
			0.0f,1.0f,
			0.0f,0.0f,
			1.0f,0.0f

		};

		glGenVertexArrays(1,&vao_pyramid);
		glBindVertexArray(vao_pyramid);

			glGenBuffers(1,&vbo_pyramidPosition);
			glBindBuffer(GL_ARRAY_BUFFER,vbo_pyramidPosition);

				glBufferData(GL_ARRAY_BUFFER,
							sizeof(pyramidVertices),
							pyramidVertices,
							GL_STATIC_DRAW);

				glVertexAttribPointer(VSV_ATTRIBUTE_POSITION,
									3,
									GL_FLOAT,
									GL_FALSE,
									0,
									NULL);

				glEnableVertexAttribArray(VSV_ATTRIBUTE_POSITION);
			
			glBindBuffer(GL_ARRAY_BUFFER,0);
			
			glGenBuffers(1,&vbo_pyramidColor);
			glBindBuffer(GL_ARRAY_BUFFER,vbo_pyramidColor);
				
				glBufferData(GL_ARRAY_BUFFER,
							sizeof(pyramidColors),
							pyramidColors,
							GL_STATIC_DRAW);
				
				glVertexAttribPointer(VSV_ATTRIBUTE_COLOR,
									3,
									GL_FLOAT,
									GL_FALSE,
									0,
									NULL);

				glEnableVertexAttribArray(VSV_ATTRIBUTE_COLOR);
			
			glBindBuffer(GL_ARRAY_BUFFER,0);

			glGenBuffers(1, &vbo_pyramidTexture);
			glBindBuffer(GL_ARRAY_BUFFER, vbo_pyramidTexture);

				glBufferData(GL_ARRAY_BUFFER,
					sizeof(pyramidTexCoord),
					pyramidTexCoord,
					GL_STATIC_DRAW);

				glVertexAttribPointer(VSV_ATTRIBUTE_TEXCOORD,
					2,
					GL_FLOAT,
					GL_FALSE,
					0,
					NULL);

				glEnableVertexAttribArray(VSV_ATTRIBUTE_TEXCOORD);

			glBindBuffer(GL_ARRAY_BUFFER, 0);

		glBindVertexArray(0);

		glGenVertexArrays(1,&vao_cube);
		glBindVertexArray(vao_cube);
			glGenBuffers(1,&vbo_cubePosition);
			glBindBuffer(GL_ARRAY_BUFFER,vbo_cubePosition);

				glBufferData(GL_ARRAY_BUFFER,
							sizeof(cubeVertices),
							cubeVertices,
							GL_STATIC_DRAW);

				glVertexAttribPointer(VSV_ATTRIBUTE_POSITION,
									3,
									GL_FLOAT,
									GL_FALSE,
									0,
									NULL);

				glEnableVertexAttribArray(VSV_ATTRIBUTE_POSITION);
			glBindBuffer(GL_ARRAY_BUFFER,0);

			glGenBuffers(1,&vbo_cubeColor);
			glBindBuffer(GL_ARRAY_BUFFER,vbo_cubeColor);
				glBufferData(GL_ARRAY_BUFFER,
							sizeof(cubeColors),
							cubeColors,
							GL_STATIC_DRAW);
				glVertexAttribPointer(VSV_ATTRIBUTE_COLOR,
										3,
										GL_FLOAT,
										GL_FALSE,
										0,
										NULL);
				glEnableVertexAttribArray(VSV_ATTRIBUTE_COLOR);
			glBindBuffer(GL_ARRAY_BUFFER,0);

			glGenBuffers(1, &vbo_cubeTexture);
				glBindBuffer(GL_ARRAY_BUFFER, vbo_cubeTexture);
				glBufferData(GL_ARRAY_BUFFER,
					sizeof(cubeTexCoord),
					cubeTexCoord,
					GL_STATIC_DRAW);
				glVertexAttribPointer(VSV_ATTRIBUTE_TEXCOORD,
					2,
					GL_FLOAT,
					GL_FALSE,
					0,
					NULL);
				glEnableVertexAttribArray(VSV_ATTRIBUTE_TEXCOORD);
			glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindVertexArray(0);


	glGenFramebuffers(1, &gFrameBuffers);
	glBindFramebuffer(GL_FRAMEBUFFER,gFrameBuffers);
		//color 
		glGenTextures(1, &gColorTexture);
		glBindTexture(GL_TEXTURE_2D,gColorTexture);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexImage2D(GL_TEXTURE_2D,
						0,
						GL_RGB,
						800,
						600,
						0,
						GL_RGB,
						GL_UNSIGNED_BYTE,
						NULL);
		glBindTexture(GL_TEXTURE_2D, 0);

		//depth
		glGenTextures(1, &gDepthTexture);
		glBindTexture(GL_TEXTURE_2D, gDepthTexture);
		/*glTexStorage2D(GL_TEXTURE_2D,
						1,
						GL_DEPTH_COMPONENT32F,
						800,
						600);*/


			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_NONE);

			glTexImage2D(GL_TEXTURE_2D,
				0,
				GL_DEPTH_COMPONENT,
				800,
				600,
				0,
				GL_DEPTH_COMPONENT,
				GL_UNSIGNED_BYTE,
				NULL);
		glBindTexture(GL_TEXTURE_2D, 0);

		glFramebufferTexture(GL_FRAMEBUFFER, 
							GL_COLOR_ATTACHMENT0, 
							gColorTexture, 
							0);

		glFramebufferTexture(GL_FRAMEBUFFER,
						GL_DEPTH_ATTACHMENT,
						gDepthTexture,
						0);

	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	glClearColor(0.0,0.0,0.0f,0.0f);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	glEnable(GL_TEXTURE_2D);

	Stone_Texture=LoadBitmapTexture("stone.bmp");
	Kundali_Texture=LoadBitmapTexture("kundali.bmp");

	perspectiveProjectionMatrix=mat4::identity();

	Resize(giWindowWidth,giWindowHeight);
}

void Resize(int width,int height)
{
	GLuint tempColorTexture = 0;
	GLuint tempgDepthTexture = 0;

	if(height==0)
	{
		height=1;
	}
	
	gWidth = width;
	gHeight = height;

	//glViewport(0,0,(GLsizei)width,(GLsizei)height);

	perspectiveProjectionMatrix=vmath::perspective(45.0f,
													(GLfloat)width/(GLfloat)height,
													0.1f,
													100.0f);

	if (gFrameBuffers == 0)
		return;

	glBindFramebuffer(GL_FRAMEBUFFER, gFrameBuffers);
		
		if (gColorTexture == 0)
			return;
		//color 
		//glGenTextures(1, &gColorTexture);
		glBindTexture(GL_TEXTURE_2D, gColorTexture);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTexImage2D(GL_TEXTURE_2D,
				0,
				GL_RGB,
				(GLsizei)width,
				(GLsizei)height,
				0,
				GL_RGB,
				GL_UNSIGNED_BYTE,
				NULL);
		glBindTexture(GL_TEXTURE_2D, 0);
		//gColorTexture = tempColorTexture;

		if (gDepthTexture == 0)
			return;
		//depth
		//glGenTextures(1, &gDepthTexture);
		glBindTexture(GL_TEXTURE_2D, gDepthTexture);
			/*glTexStorage2D(GL_TEXTURE_2D,
				1,
				GL_DEPTH_COMPONENT32F,
				(GLsizei)width,
				(GLsizei)height);*/
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_NONE);
			glTexImage2D(GL_TEXTURE_2D,
				0,
				GL_DEPTH_COMPONENT,
				(GLsizei)width,
				(GLsizei)height,
				0,
				GL_DEPTH_COMPONENT,
				GL_UNSIGNED_BYTE,
				NULL);
		glBindTexture(GL_TEXTURE_2D, 0);
		//gDepthTexture = tempgDepthTexture;

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

GLfloat cubeRotate=0.0f;

void Draw(void)
{
	
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	mat4 modelViewMatrix;
	mat4 modelViewProjectionMatrix;
	mat4 translateMatrix;
	mat4 rotateMatrix;
	mat4 scaleMatrix;


	glBindFramebuffer(GL_FRAMEBUFFER, gFrameBuffers);
		glViewport(0, 0, (GLsizei)gWidth, (GLsizei)gHeight);
		//glViewport(0, 0, 800, 600);

		glClearBufferfv(GL_COLOR, 0, vec4(0.0f, 1.0f, 0.0f, 1.0f));
		glClearBufferfv(GL_DEPTH, 0, vec4(1.0f, 1.0f, 1.0f, 1.0f));

		glUseProgram(gShaderProgramObject_simple);

			rotateMatrix = vmath::rotate(cubeRotate, 1.0f, 0.0f, 0.0f);
			rotateMatrix *= vmath::rotate(cubeRotate, 0.0f, 1.0f, 0.0f);
			rotateMatrix *= vmath::rotate(cubeRotate, 0.0f, 0.0f, 1.0f);

			translateMatrix = vmath::translate(0.0f, 0.0f, -6.0f);
			scaleMatrix = vmath::scale(0.75f, 0.75f, 0.75f);

			modelViewMatrix = translateMatrix * scaleMatrix * rotateMatrix;
			modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

			glUniformMatrix4fv(mvpMatrixUniform_simple,
				1,
				GL_FALSE,
				modelViewProjectionMatrix);

			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, Kundali_Texture);
			glUniform1i(textureSamplerUniform_simple,
							0);

			glBindVertexArray(vao_cube);
			glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
			glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
			glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
			glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
			glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
			glDrawArrays(GL_TRIANGLE_FAN, 20, 4);
			glBindVertexArray(0);

		glUseProgram(0);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	glUseProgram(gShaderProgramObject_render_to_texture);
	
		rotateMatrix=vmath::rotate(cubeRotate,1.0f,0.0f,0.0f);
		rotateMatrix*=vmath::rotate(cubeRotate,0.0f,1.0f,0.0f);
		rotateMatrix*=vmath::rotate(cubeRotate,0.0f,0.0f,1.0f);

		translateMatrix=vmath::translate(0.0f,0.0f,-6.0f);
		scaleMatrix=vmath::scale(0.75f,0.75f,0.75f);

		modelViewMatrix=translateMatrix * scaleMatrix  * rotateMatrix  ;
		modelViewProjectionMatrix=perspectiveProjectionMatrix * modelViewMatrix;

		glUniformMatrix4fv(mvpMatrixUniform_render_to_texture,
						1,
						GL_FALSE,
						modelViewProjectionMatrix);
		
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, gColorTexture);
		glUniform1i(glGetUniformLocation(gShaderProgramObject_render_to_texture,
			"u_texture_sampler"), 0);
		
		glBindVertexArray(vao_cube);
			glDrawArrays(GL_TRIANGLE_FAN,0,4);
			glDrawArrays(GL_TRIANGLE_FAN,4,4);
			glDrawArrays(GL_TRIANGLE_FAN,8,4);
			glDrawArrays(GL_TRIANGLE_FAN,12,4);
			glDrawArrays(GL_TRIANGLE_FAN,16,4);
			glDrawArrays(GL_TRIANGLE_FAN,20,4);
		glBindVertexArray(0);

	glUseProgram(0);

	glXSwapBuffers(gpDisplay,
					gWindow);

	cubeRotate+=1.0f;
}

void UnInitialize(void)
{
	if(vao_cube)
	{
		glDeleteVertexArrays(1,
							&vao_cube);
	}
	vao_cube=0;

	if(vao_pyramid)
	{
		glDeleteVertexArrays(1,
							&vao_pyramid);
	}
	vao_pyramid=0;

	if(vbo_pyramidColor){
		glDeleteBuffers(1,
						&vbo_pyramidColor);
	}
	vbo_pyramidColor=0;

	if(vbo_pyramidPosition)
	{
		glDeleteBuffers(1,
					&vbo_pyramidPosition);
	}
	vbo_pyramidPosition=0;

	if(vbo_cubePosition)
	{
		glDeleteBuffers(1,
					&vbo_cubePosition);
	}
	vbo_cubePosition=0;

	if(vbo_cubeColor)
	{
		glDeleteBuffers(1,
					&vbo_cubeColor);
	}
	vbo_cubeColor=0;

	if (gFrameBuffers)
	{
		glDeleteFramebuffers(1,
			&gFrameBuffers);
	}
	gFrameBuffers = 0;

	if (gFrameBuffers)
	{
		glDeleteFramebuffers(1,
			&gFrameBuffers);
	}
	gFrameBuffers = 0;

	glDeleteTextures(1, &gColorTexture);
	glDeleteTextures(1, &gDepthTexture);
	
	glDeleteTextures(1, &Kundali_Texture);
	glDeleteTextures(1, &Stone_Texture);

	if(gShaderProgramObject_simple)
	{
		glUseProgram(gShaderProgramObject_simple);

		GLsizei shaderCount;
		GLsizei tempShaderCount;
		GLuint *pShader=NULL;

		glGetProgramiv(gShaderProgramObject_simple,GL_ATTACHED_SHADERS,&shaderCount);

		pShader=(GLuint *)malloc(sizeof(GLuint)*shaderCount);
		if(pShader==NULL)
		{
			printf("\nFailed to allocate memory for pShader");
		}else{
			glGetAttachedShaders(gShaderProgramObject_simple,shaderCount,&tempShaderCount,pShader);
			
			printf("\nShader program has actual attached shader count is:%d",tempShaderCount);
			
			for(GLsizei i=0;i<shaderCount;i++)
			{
				glDetachShader(gShaderProgramObject_simple,pShader[i]);
				glDeleteShader(pShader[i]);
				pShader[i]=0;
			}
			free(pShader);
			
			glDeleteProgram(gShaderProgramObject_simple);
			gShaderProgramObject_simple =0;
			glUseProgram(0);
		}
	}

	GLXContext currentGLXContext;

	currentGLXContext=glXGetCurrentContext();

	if(currentGLXContext==gGLXContext)
	{
		glXMakeCurrent(gpDisplay,
						0,
						0);
	}

	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay,
							gGLXContext);
	}

	gGLXContext=NULL;
	currentGLXContext=NULL;

	if(gWindow)
	{
		XDestroyWindow(gpDisplay,
						gWindow);
	}

	if(gColormap)
	{
		XFreeColormap(gpDisplay,
						gColormap);
	}

	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
	}
		gpXVisualInfo=NULL;

	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
	}
		gpDisplay=NULL;
}

GLuint LoadBitmapTexture(const char *path)
{
	int width,height;
	unsigned char* imageData=NULL;
	GLuint texture_id;

	imageData=SOIL_load_image(	path,
								&width,
								&height,
								NULL,
								SOIL_LOAD_RGB);

	glPixelStorei(	GL_UNPACK_ALIGNMENT,
					1);

	glGenTextures(	1,
					&texture_id);


	glBindTexture(	GL_TEXTURE_2D,
					texture_id);

	glTexParameteri(GL_TEXTURE_2D,
					GL_TEXTURE_MIN_FILTER,
					GL_LINEAR_MIPMAP_LINEAR);

	glTexParameteri(GL_TEXTURE_2D,
					GL_TEXTURE_MAG_FILTER,
					GL_LINEAR);

	glTexImage2D(GL_TEXTURE_2D,
					0,
					GL_RGB,
					width,
					height,
					0,
					GL_RGB,
					GL_UNSIGNED_BYTE,
					imageData);

	glGenerateMipmap(GL_TEXTURE_2D);

	SOIL_free_image_data(imageData);

	// if  (imageData !=NULL ){
	// 	printf("INFO | %d \n",texture_id);
	// }
	return texture_id;
}