#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<memory.h>
#include<math.h>

#include "Sphere.h"
#include "Stack_LinkedList.h"

#include<GL/glew.h>
#include<GL/glx.h>
#include<GL/gl.h>

#include"vmath.h"

#include<X11/Xlib.h>
#include<X11/Xutil.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>


using namespace std;
using namespace vmath;

bool bFullScreen=false;
Display *gpDisplay=NULL;
XVisualInfo *gpXVisualInfo=NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth=800;
int giWindowHeight=600;

GLXContext gGLXContext;

//PP Changes
typedef GLXContext (*glxCreateContextAttribsARBProc)(Display *, GLXFBConfig, GLXContext, Bool, const int *);
glxCreateContextAttribsARBProc glxCreateContextAttribsARB=NULL;

GLXFBConfig gGLXFBConfig;

//Shader Changes

pNode MatrixStack = NULL;

enum{
	VSV_ATTRIBUTE_POSITION=0,
	VSV_ATTRIBUTE_COLOR,
	VSV_ATTRIBUTE_NORMAL,
	VSV_ATTRIBUTE_TEXCOORD,
};

GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

GLuint vaoSphere;
GLuint vboSphere_position;
GLuint vboSphere_normal;
GLuint vboSphere_elements;

GLuint modelMatrixUniform;
GLuint viewMatrixUniform;
GLuint projectionlMatrixUniform;

GLuint lightAmbientUniform;
GLuint lightDiffuseUniform;
GLuint lightSpecularUniform;
GLuint lightPositionUniform;

GLuint materialAmbientUniform;
GLuint materialDiffuseUniform;
GLuint materialSpecularUniform;
GLuint materialShininessUniform;

GLuint LKeyPressedUniform;

mat4 perspectiveProjectionMatrix;

bool bLights;

float spherePosition[1146];
float sphereNormals[1146];
float sphereTexcoord[764];
unsigned short sphereElement[2280];//Face Indices

GLfloat LightAmbient[] = { 0.1f,0.1f,0.1f,1.0f };
GLfloat LightDefuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat LightSpecular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat LightPosition[] = { 100.0f,100.0f,100.0f,1.0f };

GLfloat materialAmbient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat materilaDefuse[] = { 0.5f,0.2f,0.7f,1.0f };
GLfloat materialSpecular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat materialShininess = 128.0f;

int shoulder = 0;
int elbow = 0;

GLuint gNumSphereVertices, gNumSphereElements;

int main(void)
{
	void CreateWindow(void);
	void ToggleFullscreen(void);
	void UnInitialize(void);

	void Initialize(void);
	void Resize(int,int);
	void Draw(void);

	bool bDone=false;

	int winWidth=giWindowWidth;
	int winHeight=giWindowHeight;

	CreateWindow();
	Initialize();

	XEvent event;
	KeySym keysym;

	while(bDone==false)
	{
		while(XPending(gpDisplay))
		{
			XNextEvent(gpDisplay,
						&event);

			switch(event.type)
			{
				case MapNotify:
					break;
				case KeyPress:
					keysym=XkbKeycodeToKeysym(gpDisplay,
												event.xkey.keycode,
												0,
												0);
					switch(keysym)
					{
						case XK_Escape:
							bDone=true;
							break;

						case XK_F:
						case XK_f:
							if(bFullScreen==false)
							{
								ToggleFullscreen();
								bFullScreen=true;
							}else
							{
								ToggleFullscreen();
								bFullScreen=false;
							}
							break;
						case XK_L:
						case XK_l:
							bLights = !bLights;
							break;
						case XK_S:
							shoulder = (shoulder + 3) % 360;
							break;
						case XK_s:
							shoulder = (shoulder - 3) % 360;
							break;
						case XK_E:
							elbow = (elbow + 3) % 360;
							break;
						case XK_e:
							elbow = (elbow - 3) % 360;
							break;
						default:
							break;
					}
					break;

				case MotionNotify:
					break;
				case ConfigureNotify:
					winWidth=event.xconfigure.width;
					winHeight=event.xconfigure.height;
					Resize(winWidth,winHeight);
					break;

				case Expose:
					break;
				case 33:
					bDone=true;
					break;
				default:
					break;
			}

		}
		Draw();
	}
	UnInitialize();
	return(0);
}

void CreateWindow(void)
{
	void UnInitialize(void);
	XSetWindowAttributes winAttribs;
	int defaultScreen;
	int styleMask;

	//PP Changes
	GLXFBConfig *pGLXFBConfig=NULL;
	GLXFBConfig bestGLXFBConfig;
	XVisualInfo *pTempXVisualInfo=NULL;
	int numFBConfigs=0;

	static int FrameBufferAttributes[]={
										GLX_X_RENDERABLE,True,
										GLX_DRAWABLE_TYPE,GLX_WINDOW_BIT,
										GLX_RENDER_TYPE,GLX_RGBA_BIT,
										GLX_X_VISUAL_TYPE,GLX_TRUE_COLOR,

										GLX_RED_SIZE,8,
										GLX_GREEN_SIZE,8,
										GLX_BLUE_SIZE,8,
										GLX_ALPHA_SIZE,8,
										
										GLX_STENCIL_SIZE,8,
										
										GLX_DOUBLEBUFFER,True,
										GLX_DEPTH_SIZE,24,
										None};	

	gpDisplay=XOpenDisplay(NULL);
	if(gpDisplay==NULL)
	{
		printf("\nERROR:Unable to Open X Display\n");
		UnInitialize();
		exit(1);
	}

	defaultScreen=XDefaultScreen(gpDisplay);
	
	//PP Changes
	pGLXFBConfig = glXChooseFBConfig(gpDisplay,
									defaultScreen,
									FrameBufferAttributes,
									&numFBConfigs);
	if(numFBConfigs == 0)
	{
		printf("\nERROR:%d-Number of Frame Buffer Config.So no need to proceed futher.\n",numFBConfigs);
		UnInitialize();
		exit(1);
	}

	printf("\nINFO:%d-Found Number of Frame Buffer Config.\n",numFBConfigs);


	int bestFrameBufferConfig=-1;
	int worstFrameBufferConfig=-1;
	int bestNumberOfSamples=-1;
	int worstNumberOfSamples=999;

	for(int i=0; i < numFBConfigs; i++)
	{
		pTempXVisualInfo=glXGetVisualFromFBConfig(gpDisplay,
												pGLXFBConfig[i]);

		if(pTempXVisualInfo != NULL)
		{
			int SampleBuffers,Samples;

			glXGetFBConfigAttrib(gpDisplay,
								pGLXFBConfig[i],
								GLX_SAMPLE_BUFFERS,
								&SampleBuffers);

			glXGetFBConfigAttrib(gpDisplay,
								pGLXFBConfig[i],
								GLX_SAMPLES,
								&Samples);

			if(bestFrameBufferConfig < 0 || SampleBuffers && Samples > bestNumberOfSamples )
			{
				bestFrameBufferConfig = i;
				bestNumberOfSamples = Samples;
			}

			if(worstFrameBufferConfig < 0 || !SampleBuffers || Samples < worstNumberOfSamples)
			{
				worstFrameBufferConfig = i;
				worstNumberOfSamples = Samples;
			}

			printf("\nINFO:When i = %d, Samples = %d, SampleBuffers = %d, pTempXVisualInfo->VisualID = %ld \n",i, Samples, SampleBuffers, pTempXVisualInfo->visualid);
		}

		XFree(pTempXVisualInfo);
		pTempXVisualInfo = NULL;
	}
	
	printf("\nINFO:bestFrameBufferConfig index = %d \n",bestFrameBufferConfig);

	bestGLXFBConfig = pGLXFBConfig[bestFrameBufferConfig];

	gGLXFBConfig = bestGLXFBConfig;

	XFree(pGLXFBConfig);
	pGLXFBConfig = NULL;

	gpXVisualInfo=(XVisualInfo *)malloc(sizeof(XVisualInfo));
	if(gpXVisualInfo==NULL)
	{
		printf("\nERROR:Unable to allocate visual info\n");
		UnInitialize();
		exit(1);
	}

	gpXVisualInfo = glXGetVisualFromFBConfig(gpDisplay,
											bestGLXFBConfig);

	winAttribs.border_pixel=0;
	winAttribs.background_pixmap=0;
	winAttribs.colormap=XCreateColormap(gpDisplay,
										RootWindow(gpDisplay,
													gpXVisualInfo->screen),
										gpXVisualInfo->visual,
										AllocNone);

	gColormap=winAttribs.colormap;

	winAttribs.background_pixel=BlackPixel(gpDisplay,
											defaultScreen);

	winAttribs.event_mask=ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;

	styleMask=CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

	gWindow=XCreateWindow(	gpDisplay,
							RootWindow(gpDisplay,
										gpXVisualInfo->screen),
							0,
							0,
							giWindowWidth,
							giWindowHeight,
							0,
							gpXVisualInfo->depth,
							InputOutput,
							gpXVisualInfo->visual,
							styleMask,
							&winAttribs);

	if(!gWindow)
	{
		printf("\nERROR:Failed to Create main window\n");
		UnInitialize();
		exit(1);
	}

	XStoreName(gpDisplay,
				gWindow,
				"XWindow-PP Robotic Arm");

	Atom windowManagerDelete=XInternAtom(gpDisplay,
										"WM_DELETE_WINDOW",
										True);

	XSetWMProtocols(gpDisplay,
					gWindow,
					&windowManagerDelete,
					1);

	XMapWindow(gpDisplay,
				gWindow);

}

void ToggleFullscreen(void)
{
	Atom wm_state;
	Atom fullscreen;
	XEvent xev={0};

	wm_state=XInternAtom(gpDisplay,
						"_NET_WM_STATE",
						False);

	memset(&xev,
			0,
			sizeof(xev));

	xev.type=ClientMessage;
	xev.xclient.window=gWindow;
	xev.xclient.message_type=wm_state;
	xev.xclient.format=32;
	xev.xclient.data.l[0]=bFullScreen?0:1;

	fullscreen=XInternAtom(gpDisplay,
							"_NET_WM_STATE_FULLSCREEN",
							False);

	xev.xclient.data.l[1]=fullscreen;

	XSendEvent(gpDisplay,
				RootWindow(gpDisplay,
							gpXVisualInfo->screen),
				False,
				StructureNotifyMask,
				&xev);
}

void glpushMatrix(datatype data)
{
	PushIntoStack(&MatrixStack, CreateStackNode(data));
}

datatype glpopMatrix(void)
{
	return PopFromStack(&MatrixStack);
}

void Initialize(void)
{

	void Resize(int,int);
	void UnInitialize(void);

	glxCreateContextAttribsARB = (glxCreateContextAttribsARBProc)glXGetProcAddressARB((GLubyte *)"glXCreateContextAttribsARB");

	const int attribs[] = { GLX_CONTEXT_MAJOR_VERSION_ARB, 4,
							GLX_CONTEXT_MINOR_VERSION_ARB, 5,
							GLX_CONTEXT_PROFILE_MASK_ARB, GLX_CONTEXT_CORE_PROFILE_BIT_ARB,
							None};

	gGLXContext = glxCreateContextAttribsARB(gpDisplay,
											gGLXFBConfig,
											0,
											True,
											attribs);

	if(!gGLXContext)
	{
		const int lowestAttribs[] = { 	GLX_CONTEXT_MAJOR_VERSION_ARB, 1,
										GLX_CONTEXT_MINOR_VERSION_ARB, 0,
										None};

		gGLXContext = glxCreateContextAttribsARB(gpDisplay,
											gGLXFBConfig,
											0,
											True,
											lowestAttribs);

			printf("\nINFO:Got 1.0 OpenGL Context Version\n");

	}else{
			printf("\nINFO:Got 4.5 OpenGL Context Version\n");
	}



	if(gGLXContext==NULL)
	{
		printf("\nERROR:Unable to create Rendering Context\n");
		UnInitialize();
		exit(1);
	}

	Bool isDirectContext = glXIsDirect(gpDisplay,
										gGLXContext);

	if (isDirectContext == True)
	{
		printf("\nINFO:Rendering Context is direct Hardware rendering context.\n");
	}else{
		printf("\nINFO:Rendering Context is Software rendering context.\n");
	}

	glXMakeCurrent(gpDisplay,
					gWindow,
					gGLXContext);

	GLenum glew_error=glewInit();
	if(glew_error != GLEW_OK)
	{
		printf("\nglewInit failed to enable extension for Programmable Pipeline");
		UnInitialize();
		exit(1);
	}

	printf( "\nOpenGL Information are availble below:-");
	printf( "\n*****************************************************");

	printf( "\nOpenGL Vender:%s",glGetString(GL_VENDOR));
	printf( "\nOpenGL Renderer:%s",glGetString(GL_RENDERER));
	printf( "\nOpenGL Version:%s",glGetString(GL_VERSION));
	printf( "\nOpenGL GLSL Version:%s",glGetString(GL_SHADING_LANGUAGE_VERSION));
	
	printf("\nOpenGL Extension's list:\n");
	GLint numExt;
	glGetIntegerv(GL_NUM_EXTENSIONS,&numExt);

	for(int i=0;i<numExt;i++)
	{
		printf( "\n%s",glGetStringi(GL_EXTENSIONS,i));
	}
	printf( "\n*****************************************************\n");

	GLint infoLogLength;
	GLint shaderCompileStatus;
	GLint shaderProgramLinkStatus;
	GLchar * szBuffer=NULL;
	GLsizei written;

	//Shader Code Start--------------------
		gVertexShaderObject=glCreateShader(GL_VERTEX_SHADER);

		const GLchar *vertexShaderSourceCode=
		"#version 460 core\n"\
		"\n"\
		"in vec4 vPosition;\n"\
		"in vec3 vNormal;\n"\

		"uniform mat4 u_modelMatrix;\n"\
		"uniform mat4 u_viewMatrix;\n"\
		"uniform mat4 u_projectionMatrix;\n"\

		"uniform vec4 u_lightPosition;\n"\
		
		"uniform int u_lKeyPressed;\n"\
		
		"out vec3 out_phongADS_Light;\n"\
		
		"out vec3 out_transformedNormal;\n"\
		"out vec3 out_lightDirection;\n"\
		"out vec3 out_viewVector;\n"\

		"void main(void)\n"\

		"{\n"\
			"if (u_lKeyPressed == 1)\n"\
			"{\n"\

				"vec4 eyeCoord = u_viewMatrix * u_modelMatrix * vPosition;\n"\
				
				"vec3 transformedNormal = mat3(u_viewMatrix * u_modelMatrix) * vNormal ;\n"\
				"out_transformedNormal = transformedNormal;\n"\

				"vec3 lightDirection = vec3( u_lightPosition - eyeCoord ) ;\n"\
				"out_lightDirection = lightDirection;\n"\
				
				"vec3 viewVector =  -eyeCoord.xyz ;\n"\
				"out_viewVector = viewVector;\n"\

			"}\n"\
			
			"gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix * vPosition;\n"\
		
		"}";

		glShaderSource(gVertexShaderObject,
						1,
						(const GLchar**)&vertexShaderSourceCode,
						NULL);
		
		glCompileShader(gVertexShaderObject);

		glGetShaderiv(gVertexShaderObject,
						GL_COMPILE_STATUS,
						&shaderCompileStatus);

		if(shaderCompileStatus==GL_FALSE)
		{
			glGetShaderiv(gVertexShaderObject,
						GL_INFO_LOG_LENGTH,
						&infoLogLength);
			
			if(infoLogLength > 0)
			{
				szBuffer=(GLchar*)malloc(sizeof(GLchar)*infoLogLength);
				if(szBuffer!=NULL)
				{
					glGetShaderInfoLog(gVertexShaderObject,
										infoLogLength,
										&written,
										szBuffer);
					printf("\nERROR:Vertex Shader Object Compilation Failed log : %s",szBuffer);
					free(szBuffer);
					UnInitialize();
					exit(1);
				}
			}
		}

		infoLogLength=0;
		shaderCompileStatus=0;
		shaderProgramLinkStatus=0;
		szBuffer=NULL;
		written=0;

		gFragmentShaderObject=glCreateShader(GL_FRAGMENT_SHADER);

		const GLchar *fragmentShaderSourceCode=
		"#version 460 core\n"\
		"\n"\
		"vec3 phongADS_Light;\n"
		"out vec4 fragColor;\n"\

		"in vec3 out_transformedNormal;\n"\
		"in vec3 out_lightDirection;\n"\
		"in vec3 out_viewVector;\n"\

		"uniform vec3 u_lightAmbient;\n"\
		"uniform vec3 u_lightDiffuse;\n"\
		"uniform vec3 u_lightSpecular;\n"\

		"uniform vec3 u_materialAmbient;\n"\
		"uniform vec3 u_materialDiffuse;\n"\
		"uniform vec3 u_materialSpecular;\n"\
		"uniform float u_materialShininess;\n"\

		"uniform int u_lKeyPressed;\n"\

		"void main(void)\n"\
		"{"\
			"if (u_lKeyPressed == 1)\n"\
			"{\n"\
				"vec3 normalizedTransformedNormal = normalize( out_transformedNormal );\n"\
				"vec3 normalizedLightDirection = normalize( out_lightDirection );\n"\
				"vec3 normalizedViewVector = normalize( out_viewVector );\n"\

				"vec3 reflectionVector = reflect( -normalizedLightDirection, normalizedTransformedNormal );\n"\
				
				"vec3 ambient = u_lightAmbient * u_materialAmbient;\n"\

				"vec3 diffuse = u_lightDiffuse * u_materialDiffuse * max( dot(normalizedLightDirection , normalizedTransformedNormal), 0.0);\n"\

				"vec3 specular = u_lightSpecular * u_materialSpecular * pow( max( dot(reflectionVector , normalizedViewVector), 0.0f) , u_materialShininess);\n"\

				"phongADS_Light= ambient+ diffuse + specular;\n"\
				
			"}\n"\
			"else\n"\
			"{\n"\

				"phongADS_Light=vec3(1.0f, 1.0f, 1.0f);\n"\
			
			"}\n"\
			
			"fragColor = vec4( phongADS_Light, 1.0f );\n"\

		"}";

		glShaderSource(gFragmentShaderObject,
						1,
						(const GLchar**)&fragmentShaderSourceCode,
						NULL);
		
		glCompileShader(gFragmentShaderObject);

		glGetShaderiv(gFragmentShaderObject,
					GL_COMPILE_STATUS,
					&shaderCompileStatus);

		if(shaderCompileStatus==GL_FALSE)
		{
			glGetShaderiv(gFragmentShaderObject,
						GL_INFO_LOG_LENGTH,
						&infoLogLength);
			
			if(infoLogLength > 0)
			{
				szBuffer=(GLchar*)malloc(sizeof(GLchar)*infoLogLength);
				if(szBuffer!=NULL)
				{
					glGetShaderInfoLog(gFragmentShaderObject,
										infoLogLength,
										&written,
										szBuffer);
					printf("\nERROR:Fragment Shader Object Compilation Failed log : %s",szBuffer);
					free(szBuffer);
					UnInitialize();
					exit(1);
				}
			}
		}

		infoLogLength=0;
		shaderCompileStatus=0;
		shaderProgramLinkStatus=0;
		szBuffer=NULL;
		written=0;

		gShaderProgramObject=glCreateProgram();

		glAttachShader(gShaderProgramObject,gVertexShaderObject);
		glAttachShader(gShaderProgramObject,gFragmentShaderObject);

		//Shader Attribute Binding Before Pre Linking of attached Shader
			glBindAttribLocation(gShaderProgramObject,
								VSV_ATTRIBUTE_POSITION,
								"vPosition");
			glBindAttribLocation(gShaderProgramObject,
								VSV_ATTRIBUTE_NORMAL,
								"vNormal");

		glLinkProgram(gShaderProgramObject);

		glGetProgramiv(gShaderProgramObject,
						GL_LINK_STATUS,
						&shaderProgramLinkStatus);
		
		if(shaderProgramLinkStatus==GL_FALSE)
		{
			glGetProgramiv(gShaderProgramObject,
							GL_INFO_LOG_LENGTH,
							&infoLogLength);
			if(infoLogLength > 0)
			{
				szBuffer=(GLchar *)malloc(sizeof(GLchar)*infoLogLength);

				if(szBuffer!=NULL)
				{
					glGetProgramInfoLog(gShaderProgramObject,
										infoLogLength,
										&written,
										szBuffer);

					printf("\nERROR:Shader program Link failed log : %s",szBuffer);
					free(szBuffer);
					UnInitialize();
					exit(1);
				}
			}
		}

	//Shader Code End--------------------

	//Uniform variable Binding after post linking of attached shader 
		modelMatrixUniform = glGetUniformLocation(gShaderProgramObject,"u_modelMatrix");
		viewMatrixUniform = glGetUniformLocation(gShaderProgramObject,"u_viewMatrix");
		projectionlMatrixUniform = glGetUniformLocation(gShaderProgramObject,"u_projectionMatrix");

		lightAmbientUniform = glGetUniformLocation(gShaderProgramObject,"u_lightAmbient");
		lightDiffuseUniform = glGetUniformLocation(gShaderProgramObject,"u_lightDiffuse");
		lightSpecularUniform = glGetUniformLocation(gShaderProgramObject,"u_lightSpecular");
		lightPositionUniform = glGetUniformLocation(gShaderProgramObject,"u_lightPosition");

		materialAmbientUniform = glGetUniformLocation(gShaderProgramObject,"u_materialAmbient");
		materialDiffuseUniform = glGetUniformLocation(gShaderProgramObject,"u_materialDiffuse");
		materialSpecularUniform = glGetUniformLocation(gShaderProgramObject,"u_materialSpecular");
		materialShininessUniform = glGetUniformLocation(gShaderProgramObject,"u_materialShininess");

		LKeyPressedUniform = glGetUniformLocation(gShaderProgramObject,"u_lKeyPressed");

	//VAO and its respective VBO preparation	

		getSphereVertexData(spherePosition, sphereNormals, sphereTexcoord, sphereElement);
		gNumSphereVertices = getNumberOfSphereVertices();
		gNumSphereElements = getNumberOfSphereElements();

		glGenVertexArrays(1,
						&vaoSphere);

		glBindVertexArray(vaoSphere);

			glGenBuffers(1,
						&vboSphere_position);

			glBindBuffer(GL_ARRAY_BUFFER,
						vboSphere_position);

				glBufferData(GL_ARRAY_BUFFER,
							sizeof(spherePosition),
							spherePosition,
							GL_STATIC_DRAW);
				
				glVertexAttribPointer(VSV_ATTRIBUTE_POSITION,
									3,
									GL_FLOAT,
									GL_FALSE,
									0,
									NULL);

				glEnableVertexAttribArray(VSV_ATTRIBUTE_POSITION);
			
			glBindBuffer(GL_ARRAY_BUFFER,
						0);
			
			glGenBuffers(1,
						&vboSphere_normal);

			glBindBuffer(GL_ARRAY_BUFFER,
						vboSphere_normal);

				glBufferData(GL_ARRAY_BUFFER,
								sizeof(sphereNormals),
								sphereNormals,
								GL_STATIC_DRAW);

				glVertexAttribPointer(VSV_ATTRIBUTE_NORMAL,
					3,
					GL_FLOAT,
					GL_FALSE,
					0,
					NULL);

				glEnableVertexAttribArray(VSV_ATTRIBUTE_NORMAL);

			glBindBuffer(GL_ARRAY_BUFFER,
				0);

			glGenBuffers(1,
				&vboSphere_normal);

			glBindBuffer(GL_ARRAY_BUFFER,
				vboSphere_normal);

				glBufferData(GL_ARRAY_BUFFER,
					sizeof(sphereNormals),
					sphereNormals,
					GL_STATIC_DRAW);

				glVertexAttribPointer(VSV_ATTRIBUTE_NORMAL,
					3,
					GL_FLOAT,
					GL_FALSE,
					0,
					NULL);

				glEnableVertexAttribArray(VSV_ATTRIBUTE_NORMAL);

			glBindBuffer(GL_ARRAY_BUFFER,
				0);


			glGenBuffers(1,
				&vboSphere_elements);

			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,
				vboSphere_elements);

				glBufferData(GL_ELEMENT_ARRAY_BUFFER,
					sizeof(sphereElement),
					sphereElement,
					GL_STATIC_DRAW);

			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,
				0);

		glBindVertexArray(0);


	//SetColor
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	//Warmup resize call
	perspectiveProjectionMatrix=mat4::identity(); 

	Resize(giWindowWidth,giWindowHeight);
}

void Resize(int width,int height)
{
	if(height==0)
	{
		height=1;
	}

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	
	perspectiveProjectionMatrix=vmath::perspective(45.0f,
													(GLfloat)width/(GLfloat)height,
													0.1f,
													100.0f);

}

void Draw(void)
{
	mat4 translateMatrix;
	mat4 rotateMatrix;
	mat4 scaleMatrix;

	mat4 modelMatrix;
	mat4 viewMatrix = mat4::identity();

	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

	glUseProgram(gShaderProgramObject);
	
		if (bLights == true)
		{
			glUniform1i(LKeyPressedUniform, 1);

			glUniform3fv(lightAmbientUniform, 1, LightAmbient);
			glUniform3fv(lightDiffuseUniform, 1, LightDefuse);
			glUniform3fv(lightSpecularUniform, 1, LightSpecular);
			glUniform4fv(lightPositionUniform, 1, LightPosition);//posiitional light

			glUniform3fv(materialAmbientUniform, 1, materialAmbient);
			glUniform3fv(materialDiffuseUniform, 1, materilaDefuse);
			glUniform3fv(materialSpecularUniform, 1, materialSpecular);

			glUniform1f(materialShininessUniform, materialShininess);
		}
		else
		{
			glUniform1i(LKeyPressedUniform, 0);
		}

		translateMatrix = vmath::translate(0.0f,0.0f,-12.0f);
		modelMatrix = translateMatrix;

		glpushMatrix(modelMatrix);
			
			rotateMatrix = vmath::rotate((GLfloat)shoulder, 0.0f, 0.0f, 1.0f);
			translateMatrix = vmath::translate(1.0f, 0.0f, 0.0f);

			modelMatrix = modelMatrix  * rotateMatrix * translateMatrix;

			glpushMatrix(modelMatrix);

				scaleMatrix = vmath::scale(2.0f, 0.5f, 1.0f);

				modelMatrix = modelMatrix * scaleMatrix;

				glUniformMatrix4fv(modelMatrixUniform,1,GL_FALSE,modelMatrix);

				glUniformMatrix4fv(viewMatrixUniform,1,GL_FALSE,viewMatrix);
		
				glUniformMatrix4fv(projectionlMatrixUniform,1,GL_FALSE,perspectiveProjectionMatrix);

				glBindVertexArray(vaoSphere);
					glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboSphere_elements);
						glDrawElements(GL_TRIANGLES, gNumSphereElements, GL_UNSIGNED_SHORT, 0);
					glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
				glBindVertexArray(0);

			modelMatrix = glpopMatrix();
			
			translateMatrix = vmath::translate(1.0f, 0.0f, 0.0f);
			rotateMatrix = vmath::rotate((GLfloat)elbow, 0.0f, 0.0f, 1.0f);

			modelMatrix = modelMatrix * translateMatrix * rotateMatrix;

			translateMatrix = vmath::translate(0.9f, 0.0f, 0.0f);

			modelMatrix = modelMatrix * translateMatrix;

			glpushMatrix(modelMatrix);

				scaleMatrix = vmath::scale(2.0f, 0.5f, 1.0f);
				modelMatrix = modelMatrix * scaleMatrix;

				glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix);

				glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);

				glUniformMatrix4fv(projectionlMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

				glBindVertexArray(vaoSphere);
					glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboSphere_elements);
						glDrawElements(GL_TRIANGLES, gNumSphereElements, GL_UNSIGNED_SHORT, 0);
					glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
				glBindVertexArray(0);

			modelMatrix = glpopMatrix();
		modelMatrix = glpopMatrix();
	glUseProgram(0);

	glXSwapBuffers(gpDisplay,
					gWindow);

}

void UnInitialize(void)
{
	if(vaoSphere)
	{
		glDeleteVertexArrays(1,
							&vaoSphere);
	}
	vaoSphere =0;

	if(vboSphere_position){
		glDeleteBuffers(1,
						&vboSphere_position);
	}
	vboSphere_position =0;

	if(vboSphere_normal)
	{
		glDeleteBuffers(1,
					&vboSphere_normal);
	}
	vboSphere_normal =0;

	if (vboSphere_elements)
	{
		glDeleteBuffers(1,
			&vboSphere_elements);
	}
	vboSphere_elements = 0;

	if(gShaderProgramObject)
	{
		glUseProgram(gShaderProgramObject);

			GLsizei shaderCount;
			GLsizei tempShaderCount;
			GLuint *pShader=NULL;

			glGetProgramiv(gShaderProgramObject,
							GL_ATTACHED_SHADERS,
							&shaderCount);

			pShader=(GLuint *)malloc(sizeof(GLuint)*shaderCount);
			if(pShader==NULL)
			{
				printf("\nFailed to allocate memory for pShader");
			}else{
				glGetAttachedShaders(gShaderProgramObject,
									shaderCount,
									&tempShaderCount,
									pShader);
				
				printf("\nShader program has actual attached shader count is:%d",tempShaderCount);
				
				for(GLsizei i=0;i<shaderCount;i++)
				{
					glDetachShader(gShaderProgramObject,
									pShader[i]);

					glDeleteShader(pShader[i]);
					
					pShader[i]=0;
				}
				free(pShader);
				
				glDeleteProgram(gShaderProgramObject);
				
				gShaderProgramObject=0;
			}
		glUseProgram(0);
	}


	GLXContext currentGLXContext;

	currentGLXContext=glXGetCurrentContext();

	if(currentGLXContext==gGLXContext)
	{
		glXMakeCurrent(gpDisplay,
						0,
						0);
	}

	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay,
							gGLXContext);
	}

	gGLXContext=NULL;
	currentGLXContext=NULL;

	if(gWindow)
	{
		XDestroyWindow(gpDisplay,
						gWindow);
	}

	if(gColormap)
	{
		XFreeColormap(gpDisplay,
						gColormap);
	}

	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
	}
		gpXVisualInfo=NULL;

	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
	}
		gpDisplay=NULL;
}