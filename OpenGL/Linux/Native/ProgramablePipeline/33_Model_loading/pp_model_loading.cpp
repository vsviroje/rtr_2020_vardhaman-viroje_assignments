#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<memory.h>

#include<GL/glew.h>
#include<GL/glx.h>
#include<GL/gl.h>

#include"vmath.h"

#include<X11/Xlib.h>
#include<X11/Xutil.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>

using namespace std;
using namespace vmath;

bool bFullScreen=false;
Display *gpDisplay=NULL;
XVisualInfo *gpXVisualInfo=NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth=800;
int giWindowHeight=600;

GLXContext gGLXContext;

//PP Changes
typedef GLXContext (*glxCreateContextAttribsARBProc)(Display *, GLXFBConfig, GLXContext, Bool, const int *);
glxCreateContextAttribsARBProc glxCreateContextAttribsARB=NULL;

GLXFBConfig gGLXFBConfig;

//Shader Changes

enum{
	VSV_ATTRIBUTE_POSITION=0,
	VSV_ATTRIBUTE_COLOR,
	VSV_ATTRIBUTE_TEXCOORD,
	VSV_ATTRIBUTE_NORMAL,
};

GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

GLuint vao;
GLuint vbo_position;
GLuint vbo_index;
GLuint mvpMatrixUniform;

mat4 perspectiveProjectionMatrix;


//Yogeshwar sir's globals for Model Loading
#define BUFFER_SIZE 			1024
#define S_EQUAL 				0

typedef struct vec_int {
	int* pi;
	size_t size;
}vec_int_t;

typedef struct vec_float {
	float* pf;
	size_t size;
}vec_float_t;

vec_float_t* gp_vertices;
vec_float_t* gp_texture;
vec_float_t* gp_normals;

vec_float_t* gp_vertices_sorted;
vec_float_t* gp_texture_sorted;
vec_float_t* gp_normals_sorted;


vec_int_t* gp_face_tri;
vec_int_t* gp_face_texture;
vec_int_t* gp_face_normals;

FILE* g_fp_meshfile = NULL;
char g_line[BUFFER_SIZE];
//


int main(void)
{
	void CreateWindow(void);
	void ToggleFullscreen(void);
	void UnInitialize(void);

	void Initialize(void);
	void Resize(int,int);
	void Draw(void);

	bool bDone=false;

	int winWidth=giWindowWidth;
	int winHeight=giWindowHeight;

	CreateWindow();
	Initialize();

	XEvent event;
	KeySym keysym;

	while(bDone==false)
	{
		while(XPending(gpDisplay))
		{
			XNextEvent(gpDisplay,
						&event);

			switch(event.type)
			{
				case MapNotify:
					break;
				case KeyPress:
					keysym=XkbKeycodeToKeysym(gpDisplay,
												event.xkey.keycode,
												0,
												0);
					switch(keysym)
					{
						case XK_Escape:
							bDone=true;
							break;

						case XK_F:
						case XK_f:
							if(bFullScreen==false)
							{
								ToggleFullscreen();
								bFullScreen=true;
							}else
							{
								ToggleFullscreen();
								bFullScreen=false;
							}
							break;
						default:
							break;
					}
					break;

				case MotionNotify:
					break;
				case ConfigureNotify:
					winWidth=event.xconfigure.width;
					winHeight=event.xconfigure.height;
					Resize(winWidth,winHeight);
					break;

				case Expose:
					break;
				case 33:
					bDone=true;
					break;
				default:
					break;
			}

		}
		Draw();
	}
	UnInitialize();
	return(0);
}

void CreateWindow(void)
{
	void UnInitialize(void);
	XSetWindowAttributes winAttribs;
	int defaultScreen;
	int styleMask;

	//PP Changes
	GLXFBConfig *pGLXFBConfig=NULL;
	GLXFBConfig bestGLXFBConfig;
	XVisualInfo *pTempXVisualInfo=NULL;
	int numFBConfigs=0;

	static int FrameBufferAttributes[]={
										GLX_X_RENDERABLE,True,
										GLX_DRAWABLE_TYPE,GLX_WINDOW_BIT,
										GLX_RENDER_TYPE,GLX_RGBA_BIT,
										GLX_X_VISUAL_TYPE,GLX_TRUE_COLOR,

										GLX_RED_SIZE,8,
										GLX_GREEN_SIZE,8,
										GLX_BLUE_SIZE,8,
										GLX_ALPHA_SIZE,8,
										
										GLX_STENCIL_SIZE,8,
										
										GLX_DOUBLEBUFFER,True,
										GLX_DEPTH_SIZE,24,
										None};	

	gpDisplay=XOpenDisplay(NULL);
	if(gpDisplay==NULL)
	{
		printf("\nERROR:Unable to Open X Display\n");
		UnInitialize();
		exit(1);
	}

	defaultScreen=XDefaultScreen(gpDisplay);
	
	//PP Changes
	pGLXFBConfig = glXChooseFBConfig(gpDisplay,
									defaultScreen,
									FrameBufferAttributes,
									&numFBConfigs);
	if(numFBConfigs == 0)
	{
		printf("\nERROR:%d-Number of Frame Buffer Config.So no need to proceed futher.\n",numFBConfigs);
		UnInitialize();
		exit(1);
	}

	printf("\nINFO:%d-Found Number of Frame Buffer Config.\n",numFBConfigs);


	int bestFrameBufferConfig=-1;
	int worstFrameBufferConfig=-1;
	int bestNumberOfSamples=-1;
	int worstNumberOfSamples=999;

	for(int i=0; i < numFBConfigs; i++)
	{
		pTempXVisualInfo=glXGetVisualFromFBConfig(gpDisplay,
												pGLXFBConfig[i]);

		if(pTempXVisualInfo != NULL)
		{
			int SampleBuffers,Samples;

			glXGetFBConfigAttrib(gpDisplay,
								pGLXFBConfig[i],
								GLX_SAMPLE_BUFFERS,
								&SampleBuffers);

			glXGetFBConfigAttrib(gpDisplay,
								pGLXFBConfig[i],
								GLX_SAMPLES,
								&Samples);

			if(bestFrameBufferConfig < 0 || SampleBuffers && Samples > bestNumberOfSamples )
			{
				bestFrameBufferConfig = i;
				bestNumberOfSamples = Samples;
			}

			if(worstFrameBufferConfig < 0 || !SampleBuffers || Samples < worstNumberOfSamples)
			{
				worstFrameBufferConfig = i;
				worstNumberOfSamples = Samples;
			}

			printf("\nINFO:When i = %d, Samples = %d, SampleBuffers = %d, pTempXVisualInfo->VisualID = %ld \n",i, Samples, SampleBuffers, pTempXVisualInfo->visualid);
		}

		XFree(pTempXVisualInfo);
		pTempXVisualInfo = NULL;
	}
	
	printf("\nINFO:bestFrameBufferConfig index = %d \n",bestFrameBufferConfig);

	bestGLXFBConfig = pGLXFBConfig[bestFrameBufferConfig];

	gGLXFBConfig = bestGLXFBConfig;

	XFree(pGLXFBConfig);
	pGLXFBConfig = NULL;

	gpXVisualInfo=(XVisualInfo *)malloc(sizeof(XVisualInfo));
	if(gpXVisualInfo==NULL)
	{
		printf("\nERROR:Unable to allocate visual info\n");
		UnInitialize();
		exit(1);
	}

	gpXVisualInfo = glXGetVisualFromFBConfig(gpDisplay,
											bestGLXFBConfig);

	winAttribs.border_pixel=0;
	winAttribs.background_pixmap=0;
	winAttribs.colormap=XCreateColormap(gpDisplay,
										RootWindow(gpDisplay,
													gpXVisualInfo->screen),
										gpXVisualInfo->visual,
										AllocNone);

	gColormap=winAttribs.colormap;

	winAttribs.background_pixel=BlackPixel(gpDisplay,
											defaultScreen);

	winAttribs.event_mask=ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;

	styleMask=CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

	gWindow=XCreateWindow(	gpDisplay,
							RootWindow(gpDisplay,
										gpXVisualInfo->screen),
							0,
							0,
							giWindowWidth,
							giWindowHeight,
							0,
							gpXVisualInfo->depth,
							InputOutput,
							gpXVisualInfo->visual,
							styleMask,
							&winAttribs);

	if(!gWindow)
	{
		printf("\nERROR:Failed to Create main window\n");
		UnInitialize();
		exit(1);
	}

	XStoreName(gpDisplay,
				gWindow,
				"XWindow-PP Model Loading");

	Atom windowManagerDelete=XInternAtom(gpDisplay,
										"WM_DELETE_WINDOW",
										True);

	XSetWMProtocols(gpDisplay,
					gWindow,
					&windowManagerDelete,
					1);

	XMapWindow(gpDisplay,
				gWindow);

}

void ToggleFullscreen(void)
{
	Atom wm_state;
	Atom fullscreen;
	XEvent xev={0};

	wm_state=XInternAtom(gpDisplay,
						"_NET_WM_STATE",
						False);

	memset(&xev,
			0,
			sizeof(xev));

	xev.type=ClientMessage;
	xev.xclient.window=gWindow;
	xev.xclient.message_type=wm_state;
	xev.xclient.format=32;
	xev.xclient.data.l[0]=bFullScreen?0:1;

	fullscreen=XInternAtom(gpDisplay,
							"_NET_WM_STATE_FULLSCREEN",
							False);

	xev.xclient.data.l[1]=fullscreen;

	XSendEvent(gpDisplay,
				RootWindow(gpDisplay,
							gpXVisualInfo->screen),
				False,
				StructureNotifyMask,
				&xev);
}

void Initialize(void)
{

	void Resize(int,int);
	void UnInitialize(void);
	void LoadMeshData(void);

	glxCreateContextAttribsARB = (glxCreateContextAttribsARBProc)glXGetProcAddressARB((GLubyte *)"glXCreateContextAttribsARB");

	const int attribs[] = { GLX_CONTEXT_MAJOR_VERSION_ARB, 4,
							GLX_CONTEXT_MINOR_VERSION_ARB, 5,
							GLX_CONTEXT_PROFILE_MASK_ARB, GLX_CONTEXT_CORE_PROFILE_BIT_ARB,
							None};

	gGLXContext = glxCreateContextAttribsARB(gpDisplay,
											gGLXFBConfig,
											0,
											True,
											attribs);

	if(!gGLXContext)
	{
		const int lowestAttribs[] = { 	GLX_CONTEXT_MAJOR_VERSION_ARB, 1,
										GLX_CONTEXT_MINOR_VERSION_ARB, 0,
										None};

		gGLXContext = glxCreateContextAttribsARB(gpDisplay,
											gGLXFBConfig,
											0,
											True,
											lowestAttribs);

			printf("\nINFO:Got 1.0 OpenGL Context Version\n");

	}else{
			printf("\nINFO:Got 4.5 OpenGL Context Version\n");
	}



	if(gGLXContext==NULL)
	{
		printf("\nERROR:Unable to create Rendering Context\n");
		UnInitialize();
		exit(1);
	}

	Bool isDirectContext = glXIsDirect(gpDisplay,
										gGLXContext);

	if (isDirectContext == True)
	{
		printf("\nINFO:Rendering Context is direct Hardware rendering context.\n");
	}else{
		printf("\nINFO:Rendering Context is Software rendering context.\n");
	}

	glXMakeCurrent(gpDisplay,
					gWindow,
					gGLXContext);

	GLenum glew_error=glewInit();
	if(glew_error != GLEW_OK)
	{
		printf("\nglewInit failed to enable extension for Programmable Pipeline");
		UnInitialize();
		exit(1);
	}

	printf( "\nOpenGL Information are availble below:-");
	printf( "\n*****************************************************");

	printf( "\nOpenGL Vender:%s",glGetString(GL_VENDOR));
	printf( "\nOpenGL Renderer:%s",glGetString(GL_RENDERER));
	printf( "\nOpenGL Version:%s",glGetString(GL_VERSION));
	printf( "\nOpenGL GLSL Version:%s",glGetString(GL_SHADING_LANGUAGE_VERSION));
	
	printf("\nOpenGL Extension's list:\n");
	GLint numExt;
	glGetIntegerv(GL_NUM_EXTENSIONS,&numExt);

	for(int i=0;i<numExt;i++)
	{
		printf( "\n%s",glGetStringi(GL_EXTENSIONS,i));
	}
	printf( "\n*****************************************************\n");
	GLint infoLogLength=0;
	GLint shaderCompileStatus=0;
	GLint shaderProgramLinkStatus=0;
	GLchar *szBuffer=NULL;
	GLsizei written;

	//Shader Code Start--------------------
		gVertexShaderObject=glCreateShader(GL_VERTEX_SHADER);

		const GLchar *vertexShaderSourceCode=
		"#version 460 core"\
		"\n"\
		"in vec4 vPosition;"\
		"uniform mat4 u_mvpMatrix;"\
		"void main(void)"\
		"{"\
			"gl_Position=u_mvpMatrix * vPosition;"\
		"}";

		glShaderSource(gVertexShaderObject,
						1,
						(const GLchar **)&vertexShaderSourceCode,
						NULL);

		glCompileShader(gVertexShaderObject);

		glGetShaderiv(gVertexShaderObject,
						GL_COMPILE_STATUS,
						&shaderCompileStatus);
		
		if(shaderCompileStatus==GL_FALSE)
		{
			glGetShaderiv(gVertexShaderObject,
						GL_INFO_LOG_LENGTH,
						&infoLogLength);
			
			if(infoLogLength > 0)
			{
				szBuffer=(GLchar *)malloc(sizeof(GLchar)*infoLogLength);
				if(szBuffer!=NULL)
				{
					glGetShaderInfoLog(gVertexShaderObject,
										infoLogLength,
										&written,
										szBuffer);

					printf("\nERROR Vertex Shader Object compilation failed log : %s",szBuffer);

					free(szBuffer);
					UnInitialize();
					exit(1);
				}
			}
		}

		infoLogLength=0;
		shaderCompileStatus=0;
		shaderProgramLinkStatus=0;
		szBuffer=NULL;
		written=0;

		gFragmentShaderObject=glCreateShader(GL_FRAGMENT_SHADER);

		const GLchar *fragmentShaderSourceCode=
		"#version 460 core"\
		"\n" \
		"out vec4 fragColor;"\
		"void main(void)"\
		"{"\
			"fragColor=vec4(1.0,1.0,1.0,1.0);"\
		"}";

		glShaderSource(gFragmentShaderObject,
						1,
						(const GLchar **)&fragmentShaderSourceCode,
						NULL);
		
		glCompileShader(gFragmentShaderObject);

		glGetShaderiv(gFragmentShaderObject,
						GL_COMPILE_STATUS,
						&shaderCompileStatus);

		if(shaderCompileStatus==GL_FALSE)
		{
			glGetShaderiv(gFragmentShaderObject,
						GL_INFO_LOG_LENGTH,
						&infoLogLength);

			if(infoLogLength > 0)
			{
				szBuffer=(GLchar *)malloc(sizeof(GLchar) * infoLogLength);
				if(szBuffer!=NULL)
				{
					glGetShaderInfoLog(gFragmentShaderObject,
										infoLogLength,
										&written,
										szBuffer);
					printf("\nERROR Fragment shader object compilation failed log : %s",szBuffer);
					free(szBuffer);
					UnInitialize();
					exit(1);
				}
			}
		}

		infoLogLength=0;
		shaderCompileStatus=0;
		shaderProgramLinkStatus=0;
		szBuffer=NULL;
		written=0;

		gShaderProgramObject=glCreateProgram();

		glAttachShader(gShaderProgramObject,gVertexShaderObject);
		glAttachShader(gShaderProgramObject,gFragmentShaderObject);
		
		//Shader Attribute Binding Before Pre Linking of attached Shader
			glBindAttribLocation(gShaderProgramObject,
								VSV_ATTRIBUTE_POSITION,
								"vPosition");

		glLinkProgram(gShaderProgramObject);

		glGetProgramiv(gShaderProgramObject,
						GL_LINK_STATUS,
						&shaderProgramLinkStatus);

		if(shaderProgramLinkStatus == GL_FALSE)
		{
			glGetProgramiv(gShaderProgramObject,
						GL_INFO_LOG_LENGTH,
						&infoLogLength);
			
			if(infoLogLength > 0)
			{
				szBuffer=(GLchar *)malloc(sizeof(GLchar) * infoLogLength);
				if(szBuffer!=NULL)
				{
					glGetProgramInfoLog(gShaderProgramObject,
										infoLogLength,
										&written,
										szBuffer);
					
					printf(
							"\nERROR Shader Program Object Link failed Log : %s",
							szBuffer);	
					
					free(szBuffer);
					UnInitialize();
					exit(1);
				}
			}
		}
		
	//Shader Code End--------------------

	//Uniform variable Binding after post linking of attached shader 

		mvpMatrixUniform=glGetUniformLocation(gShaderProgramObject,
												"u_mvpMatrix");

	LoadMeshData();

	//VAO and its respective VBO preparation	

		glGenVertexArrays(1,
						&vao);

		glBindVertexArray(vao);

			glGenBuffers(1,
						&vbo_position);
			
			glBindBuffer(GL_ARRAY_BUFFER,
						vbo_position);
				
				glBufferData(GL_ARRAY_BUFFER,
							gp_vertices_sorted->size * sizeof(GLfloat),
							gp_vertices_sorted->pf,
							GL_STATIC_DRAW);
				
				glVertexAttribPointer(VSV_ATTRIBUTE_POSITION,
										3,
										GL_FLOAT,
										GL_FALSE,
										0,
										NULL);

				glEnableVertexAttribArray(VSV_ATTRIBUTE_POSITION);

			glBindBuffer(GL_ARRAY_BUFFER,
						0);

			glGenBuffers(1,
				&vbo_index);

			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,
				vbo_index);

			glBufferData(GL_ELEMENT_ARRAY_BUFFER,
				gp_face_tri->size * sizeof(int),
				gp_face_tri->pi,
				GL_STATIC_DRAW);

		glBindVertexArray(0);

	//SetColor
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	//Warmup resize call

	perspectiveProjectionMatrix=mat4::identity();
	
	Resize(giWindowWidth,giWindowHeight);
}

void Resize(int width,int height)
{
	if(height==0)
	{
		height=1;
	}

	glViewport( 0,
				0,
				(GLsizei)width,
				(GLsizei)height);

	perspectiveProjectionMatrix=vmath::perspective(45.0f,
												(GLfloat)width/(GLfloat)height,
												0.1f,
												100.0f);

}

void Draw(void)
{
	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
	
	glUseProgram(gShaderProgramObject);

		mat4 moodelViewMatrix=mat4::identity();
		mat4 translateMatrix;
		mat4 modelViewProjectionMatix=mat4::identity();

		translateMatrix=vmath::translate(0.0f,0.0f,-3.0f);
		
		moodelViewMatrix=translateMatrix;

		modelViewProjectionMatix=perspectiveProjectionMatrix * moodelViewMatrix;

		glUniformMatrix4fv(mvpMatrixUniform,
							1,
							GL_FALSE,
							modelViewProjectionMatix);

		glBindVertexArray(vao);

		glDrawElements(GL_TRIANGLES,
			gp_vertices_sorted->size,//size of array
			GL_UNSIGNED_INT,//data type
			0);//starting Index


		glBindVertexArray(0);	

	glUseProgram(0);


	glXSwapBuffers(gpDisplay,
					gWindow);

}

void UnInitialize(void)
{
		
	if(vao)
	{
		glDeleteVertexArrays(1,
							&vao);
	}
	vao=0;

	if(vbo_position)
	{
		glDeleteBuffers(1,
						&vbo_position);
	}
	vbo_position=0;

	if(gShaderProgramObject)
	{
		glUseProgram(gShaderProgramObject);

		GLsizei shaderCount;
		GLsizei tempShaderCount;
		GLuint *pShader=NULL;

		glGetProgramiv(gShaderProgramObject,GL_ATTACHED_SHADERS,&shaderCount);

		pShader=(GLuint *)malloc(sizeof(GLuint)*shaderCount);
		if(pShader==NULL)
		{
			printf("\nFailed to allocate memory for pShader");
		}else{
			glGetAttachedShaders(gShaderProgramObject,shaderCount,&tempShaderCount,pShader);
			
			printf("\nShader program has actual attached shader count is:%d",tempShaderCount);
			
			for(GLsizei i=0;i<shaderCount;i++)
			{
				glDetachShader(gShaderProgramObject,pShader[i]);
				glDeleteShader(pShader[i]);
				pShader[i]=0;
			}
			free(pShader);
			
			glDeleteProgram(gShaderProgramObject);
			gShaderProgramObject=0;
			glUseProgram(0);
		}
	}


	GLXContext currentGLXContext;

	currentGLXContext=glXGetCurrentContext();

	if(currentGLXContext==gGLXContext)
	{
		glXMakeCurrent(gpDisplay,
						0,
						0);
	}

	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay,
							gGLXContext);
	}

	gGLXContext=NULL;
	currentGLXContext=NULL;

	if(gWindow)
	{
		XDestroyWindow(gpDisplay,
						gWindow);
	}

	if(gColormap)
	{
		XFreeColormap(gpDisplay,
						gColormap);
	}

	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
	}
		gpXVisualInfo=NULL;

	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
	}
		gpDisplay=NULL;
}


void LoadMeshData(void)
{
	void UnInitialize(void);

	vec_int_t* create_vec_int(void);
	vec_float_t* create_vec_float(void);

	void push_back_vec_int(vec_int_t*, int);
	void push_back_vec_float(vec_float_t*, float);

	void* xcalloc(int, size_t);

	const char* sep_space = " ";
	const char* sep_fslash = "/";

	char* first_token = NULL;
	char* token = NULL;
	char* next_token = NULL;

	char* f_enteries[3] = { NULL,NULL,NULL };

	int nr_pos_coords = 0, nr_tex_coords = 0, nr_normal_coords = 0, nr_faces = 0;

	g_fp_meshfile=fopen("MonkeyHead.obj", "r");
	if (g_fp_meshfile ==NULL)
	{
		UnInitialize();
	}

	gp_vertices = create_vec_float();
	gp_texture = create_vec_float();
	gp_normals = create_vec_float();

	gp_face_tri = create_vec_int();
	gp_face_texture = create_vec_int();
	gp_face_normals = create_vec_int();

	gp_vertices_sorted = create_vec_float();
	gp_texture_sorted = create_vec_float();
	gp_normals_sorted = create_vec_float();

	while (fgets(g_line, BUFFER_SIZE, g_fp_meshfile) != NULL)
	{

		first_token = strtok_r(g_line, sep_space, &next_token);

		if (strcmp(first_token, "v") == S_EQUAL)
		{
			nr_pos_coords++;
			while ((token = strtok_r(NULL, sep_space, &next_token)) != NULL)
				push_back_vec_float(gp_vertices, atof(token));

		}
		else if (strcmp(first_token, "vt") == S_EQUAL)
		{
			nr_tex_coords++;
			while ((token = strtok_r(NULL, sep_space, &next_token)) != NULL)
				push_back_vec_float(gp_texture, atof(token));

		}
		else if (strcmp(first_token, "vn") == S_EQUAL)
		{
			nr_normal_coords++;
			while ((token = strtok_r(NULL, sep_space, &next_token)) != NULL)
				push_back_vec_float(gp_normals, atof(token));

		}
		else if (strcmp(first_token, "f") == S_EQUAL)
		{
			nr_faces++;

			for (int i = 0; i < 3; i++)
			{
				f_enteries[i] = strtok_r(NULL, sep_space, &next_token);
			}

			for (int i = 0; i < 3; i++)
			{
				token = strtok_r(f_enteries[i], sep_fslash, &next_token);
				push_back_vec_int(gp_face_tri, atoi(token) - 1);

				token = strtok_r(NULL, sep_fslash, &next_token);
					push_back_vec_int(gp_face_texture, atoi(token) - 1);

				token = strtok_r(NULL, sep_fslash, &next_token);
					push_back_vec_int(gp_face_normals, atoi(token) - 1);

			}
		}

		memset((void*)g_line, (int)'\0', BUFFER_SIZE);
	}

	//create data according to faces/index 
	for (int i = 0; i < gp_face_tri->size; i++)
		push_back_vec_float(gp_vertices_sorted, gp_vertices->pf[i]);

	for (int i = 0; i < gp_face_texture->size; i++)
		push_back_vec_float(gp_texture_sorted, gp_texture->pf[i]);

	for (int i = 0; i < gp_face_normals->size; i++)
		push_back_vec_float(gp_normals_sorted, gp_normals->pf[i]);

	fclose(g_fp_meshfile);
	g_fp_meshfile = NULL;
}

vec_int_t* create_vec_int(void)
{
	void* xcalloc(int, size_t);

	return (vec_int_t*)xcalloc(1, sizeof(vec_int_t));
}

vec_float_t* create_vec_float(void)
{
	void* xcalloc(int, size_t);

	return (vec_float_t*)xcalloc(1, sizeof(vec_float_t));
}

void push_back_vec_int(vec_int_t* p_vec, int data)
{
	void* xrealloc(void*, size_t);

	p_vec->pi = (int*)xrealloc(p_vec->pi, (p_vec->size + 1) * sizeof(int));
	p_vec->size++;
	p_vec->pi[p_vec->size - 1] = data;
}


void push_back_vec_float(vec_float_t* p_vec, float data)
{
	void* xrealloc(void*, size_t);

	p_vec->pf = (float*)xrealloc(p_vec->pf, (p_vec->size + 1) * sizeof(float));
	p_vec->size++;
	p_vec->pf[p_vec->size - 1] = data;
}

void* xcalloc(int nr_elements, size_t size_per_element)
{
	void UnInitialize(void);

	void* p = calloc(nr_elements, size_per_element);
	if (!p)
	{
		UnInitialize();
	}

	memset(p, 0, size_per_element);

	return (p);
}

void* xrealloc(void* p, size_t new_size)
{
	void UnInitialize(void);

	void* ptr = realloc(p, new_size);
	if (!ptr)
	{
		UnInitialize();
	}
	return (ptr);
}

void clean_vec_float(vec_float_t* p_vec)
{
	free(p_vec->pf);
	free(p_vec);
	p_vec = NULL;
}

void clean_vec_int(vec_int* p_vec)
{
	free(p_vec->pi);
	free(p_vec);
	p_vec = NULL;
}