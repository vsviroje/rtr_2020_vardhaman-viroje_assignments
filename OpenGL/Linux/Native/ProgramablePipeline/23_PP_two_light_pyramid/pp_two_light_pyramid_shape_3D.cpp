#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<memory.h>

#include<GL/glew.h>
#include<GL/glx.h>
#include<GL/gl.h>

#include"vmath.h"

#include<X11/Xlib.h>
#include<X11/Xutil.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>

using namespace std;
using namespace vmath;

bool bFullScreen=false;
Display *gpDisplay=NULL;
XVisualInfo *gpXVisualInfo=NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth=800;
int giWindowHeight=600;

GLXContext gGLXContext;

//PP Changes
typedef GLXContext (*glxCreateContextAttribsARBProc)(Display *, GLXFBConfig, GLXContext, Bool, const int *);
glxCreateContextAttribsARBProc glxCreateContextAttribsARB=NULL;

GLXFBConfig gGLXFBConfig;

//Shader Changes

enum{
	VSV_ATTRIBUTE_POSITION=0,
	VSV_ATTRIBUTE_COLOR,
	VSV_ATTRIBUTE_TEXCOORD,
	VSV_ATTRIBUTE_NORMAL,
};

GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

GLuint vao_pyramid;

GLuint vbo_position;
GLuint vbo_normal;

mat4 perspectiveProjectionMatrix;

GLuint modelMatrixUniform;
GLuint viewMatrixUniform;
GLuint projectionlMatrixUniform;

GLuint lightAmbientUniform[2];
GLuint lightDiffuseUniform[2];
GLuint lightSpecularUniform[2];
GLuint lightPositionUniform[2];

GLuint materialAmbientUniform;
GLuint materialDiffuseUniform;
GLuint materialSpecularUniform;
GLuint materialShininessUniform;

GLuint LKeyPressedUniform;

bool bLights;

typedef struct Light {
	vec4 LightAmbient;
	vec4 LightDefuse;
	vec4 LightSpecular;
	vec4 LightPosition;
}LightStruct;

LightStruct light[2] = { 
						{
							vec4(0.0f,0.0f,0.0f,1.0f),
							vec4(1.0f,0.0f,0.0f,1.0f),
							vec4(1.0f,0.0f,0.0f,1.0f),
							vec4(2.0f,0.0f,0.0f,1.0f)
						},
						{
							vec4(0.0f,0.0f,0.0f,1.0f),
							vec4(0.0f,0.0f,1.0f,1.0f),
							vec4(0.0f,0.0f,1.0f,1.0f),
							vec4(-2.0f,0.0f,0.0f,1.0f)
						}
					};

GLfloat materialAmbient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat materilaDefuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat materialSpecular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat materialShininess = 50.0f;

int main(void)
{
	void CreateWindow(void);
	void ToggleFullscreen(void);
	void UnInitialize(void);

	void Initialize(void);
	void Resize(int,int);
	void Draw(void);

	bool bDone=false;

	int winWidth=giWindowWidth;
	int winHeight=giWindowHeight;

	CreateWindow();
	Initialize();

	XEvent event;
	KeySym keysym;

	while(bDone==false)
	{
		while(XPending(gpDisplay))
		{
			XNextEvent(gpDisplay,
						&event);

			switch(event.type)
			{
				case MapNotify:
					break;
				case KeyPress:
					keysym=XkbKeycodeToKeysym(gpDisplay,
												event.xkey.keycode,
												0,
												0);
					switch(keysym)
					{
						case XK_Escape:
							bDone=true;
							break;

						case XK_F:
						case XK_f:
							if(bFullScreen==false)
							{
								ToggleFullscreen();
								bFullScreen=true;
							}else
							{
								ToggleFullscreen();
								bFullScreen=false;
							}
							break;
						case XK_L:
						case XK_l:
							bLights = !bLights;
							break;
						default:
							break;
					}
					break;

				case MotionNotify:
					break;
				case ConfigureNotify:
					winWidth=event.xconfigure.width;
					winHeight=event.xconfigure.height;
					Resize(winWidth,winHeight);
					break;

				case Expose:
					break;
				case 33:
					bDone=true;
					break;
				default:
					break;
			}

		}
		Draw();
	}
	UnInitialize();
	return(0);
}

void CreateWindow(void)
{
	void UnInitialize(void);
	XSetWindowAttributes winAttribs;
	int defaultScreen;
	int styleMask;

	//PP Changes
	GLXFBConfig *pGLXFBConfig=NULL;
	GLXFBConfig bestGLXFBConfig;
	XVisualInfo *pTempXVisualInfo=NULL;
	int numFBConfigs=0;

	static int FrameBufferAttributes[]={
										GLX_X_RENDERABLE,True,
										GLX_DRAWABLE_TYPE,GLX_WINDOW_BIT,
										GLX_RENDER_TYPE,GLX_RGBA_BIT,
										GLX_X_VISUAL_TYPE,GLX_TRUE_COLOR,

										GLX_RED_SIZE,8,
										GLX_GREEN_SIZE,8,
										GLX_BLUE_SIZE,8,
										GLX_ALPHA_SIZE,8,
										
										GLX_STENCIL_SIZE,8,
										
										GLX_DOUBLEBUFFER,True,
										GLX_DEPTH_SIZE,24,
										None};	

	gpDisplay=XOpenDisplay(NULL);
	if(gpDisplay==NULL)
	{
		printf("\nERROR:Unable to Open X Display\n");
		UnInitialize();
		exit(1);
	}

	defaultScreen=XDefaultScreen(gpDisplay);
	
	//PP Changes
	pGLXFBConfig = glXChooseFBConfig(gpDisplay,
									defaultScreen,
									FrameBufferAttributes,
									&numFBConfigs);
	if(numFBConfigs == 0)
	{
		printf("\nERROR:%d-Number of Frame Buffer Config.So no need to proceed futher.\n",numFBConfigs);
		UnInitialize();
		exit(1);
	}

	printf("\nINFO:%d-Found Number of Frame Buffer Config.\n",numFBConfigs);


	int bestFrameBufferConfig=-1;
	int worstFrameBufferConfig=-1;
	int bestNumberOfSamples=-1;
	int worstNumberOfSamples=999;

	for(int i=0; i < numFBConfigs; i++)
	{
		pTempXVisualInfo=glXGetVisualFromFBConfig(gpDisplay,
												pGLXFBConfig[i]);

		if(pTempXVisualInfo != NULL)
		{
			int SampleBuffers,Samples;

			glXGetFBConfigAttrib(gpDisplay,
								pGLXFBConfig[i],
								GLX_SAMPLE_BUFFERS,
								&SampleBuffers);

			glXGetFBConfigAttrib(gpDisplay,
								pGLXFBConfig[i],
								GLX_SAMPLES,
								&Samples);

			if(bestFrameBufferConfig < 0 || SampleBuffers && Samples > bestNumberOfSamples )
			{
				bestFrameBufferConfig = i;
				bestNumberOfSamples = Samples;
			}

			if(worstFrameBufferConfig < 0 || !SampleBuffers || Samples < worstNumberOfSamples)
			{
				worstFrameBufferConfig = i;
				worstNumberOfSamples = Samples;
			}

			printf("\nINFO:When i = %d, Samples = %d, SampleBuffers = %d, pTempXVisualInfo->VisualID = %ld \n",i, Samples, SampleBuffers, pTempXVisualInfo->visualid);
		}

		XFree(pTempXVisualInfo);
		pTempXVisualInfo = NULL;
	}
	
	printf("\nINFO:bestFrameBufferConfig index = %d \n",bestFrameBufferConfig);

	bestGLXFBConfig = pGLXFBConfig[bestFrameBufferConfig];

	gGLXFBConfig = bestGLXFBConfig;

	XFree(pGLXFBConfig);
	pGLXFBConfig = NULL;

	gpXVisualInfo=(XVisualInfo *)malloc(sizeof(XVisualInfo));
	if(gpXVisualInfo==NULL)
	{
		printf("\nERROR:Unable to allocate visual info\n");
		UnInitialize();
		exit(1);
	}

	gpXVisualInfo = glXGetVisualFromFBConfig(gpDisplay,
											bestGLXFBConfig);

	winAttribs.border_pixel=0;
	winAttribs.background_pixmap=0;
	winAttribs.colormap=XCreateColormap(gpDisplay,
										RootWindow(gpDisplay,
													gpXVisualInfo->screen),
										gpXVisualInfo->visual,
										AllocNone);

	gColormap=winAttribs.colormap;

	winAttribs.background_pixel=BlackPixel(gpDisplay,
											defaultScreen);

	winAttribs.event_mask=ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;

	styleMask=CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

	gWindow=XCreateWindow(	gpDisplay,
							RootWindow(gpDisplay,
										gpXVisualInfo->screen),
							0,
							0,
							giWindowWidth,
							giWindowHeight,
							0,
							gpXVisualInfo->depth,
							InputOutput,
							gpXVisualInfo->visual,
							styleMask,
							&winAttribs);

	if(!gWindow)
	{
		printf("\nERROR:Failed to Create main window\n");
		UnInitialize();
		exit(1);
	}

	XStoreName(gpDisplay,
				gWindow,
				"XWindow-PP two light pyramid");

	Atom windowManagerDelete=XInternAtom(gpDisplay,
										"WM_DELETE_WINDOW",
										True);

	XSetWMProtocols(gpDisplay,
					gWindow,
					&windowManagerDelete,
					1);

	XMapWindow(gpDisplay,
				gWindow);

}

void ToggleFullscreen(void)
{
	Atom wm_state;
	Atom fullscreen;
	XEvent xev={0};

	wm_state=XInternAtom(gpDisplay,
						"_NET_WM_STATE",
						False);

	memset(&xev,
			0,
			sizeof(xev));

	xev.type=ClientMessage;
	xev.xclient.window=gWindow;
	xev.xclient.message_type=wm_state;
	xev.xclient.format=32;
	xev.xclient.data.l[0]=bFullScreen?0:1;

	fullscreen=XInternAtom(gpDisplay,
							"_NET_WM_STATE_FULLSCREEN",
							False);

	xev.xclient.data.l[1]=fullscreen;

	XSendEvent(gpDisplay,
				RootWindow(gpDisplay,
							gpXVisualInfo->screen),
				False,
				StructureNotifyMask,
				&xev);
}

void Initialize(void)
{

	void Resize(int,int);
	void UnInitialize(void);

	glxCreateContextAttribsARB = (glxCreateContextAttribsARBProc)glXGetProcAddressARB((GLubyte *)"glXCreateContextAttribsARB");

	const int attribs[] = { GLX_CONTEXT_MAJOR_VERSION_ARB, 4,
							GLX_CONTEXT_MINOR_VERSION_ARB, 5,
							GLX_CONTEXT_PROFILE_MASK_ARB, GLX_CONTEXT_CORE_PROFILE_BIT_ARB,
							None};

	gGLXContext = glxCreateContextAttribsARB(gpDisplay,
											gGLXFBConfig,
											0,
											True,
											attribs);

	if(!gGLXContext)
	{
		const int lowestAttribs[] = { 	GLX_CONTEXT_MAJOR_VERSION_ARB, 1,
										GLX_CONTEXT_MINOR_VERSION_ARB, 0,
										None};

		gGLXContext = glxCreateContextAttribsARB(gpDisplay,
											gGLXFBConfig,
											0,
											True,
											lowestAttribs);

			printf("\nINFO:Got 1.0 OpenGL Context Version\n");

	}else{
			printf("\nINFO:Got 4.5 OpenGL Context Version\n");
	}



	if(gGLXContext==NULL)
	{
		printf("\nERROR:Unable to create Rendering Context\n");
		UnInitialize();
		exit(1);
	}

	Bool isDirectContext = glXIsDirect(gpDisplay,
										gGLXContext);

	if (isDirectContext == True)
	{
		printf("\nINFO:Rendering Context is direct Hardware rendering context.\n");
	}else{
		printf("\nINFO:Rendering Context is Software rendering context.\n");
	}

	glXMakeCurrent(gpDisplay,
					gWindow,
					gGLXContext);

	GLenum glew_error=glewInit();
	if(glew_error != GLEW_OK)
	{
		printf("\nglewInit failed to enable extension for Programmable Pipeline");
		UnInitialize();
		exit(1);
	}

	printf( "\nOpenGL Information are availble below:-");
	printf( "\n*****************************************************");

	printf( "\nOpenGL Vender:%s",glGetString(GL_VENDOR));
	printf( "\nOpenGL Renderer:%s",glGetString(GL_RENDERER));
	printf( "\nOpenGL Version:%s",glGetString(GL_VERSION));
	printf( "\nOpenGL GLSL Version:%s",glGetString(GL_SHADING_LANGUAGE_VERSION));
	
	printf("\nOpenGL Extension's list:\n");
	GLint numExt;
	glGetIntegerv(GL_NUM_EXTENSIONS,&numExt);

	for(int i=0;i<numExt;i++)
	{
		printf( "\n%s",glGetStringi(GL_EXTENSIONS,i));
	}
	printf( "\n*****************************************************\n");

	GLint infoLogLenth=0;
	GLint shaderCompileStatus=0;
	GLint shaderProgramLinkStatus=0;
	GLchar *szBuffer=NULL;
	GLsizei written;

	gVertexShaderObject =glCreateShader(GL_VERTEX_SHADER);

	const GLchar *vertexShaderSourceCode=
		"#version 450 core"\
		"\n"\
		"in vec4 vPosition;\n"\
		"in vec3 vNormal;\n"\

		"uniform mat4 u_modelMatrix;\n"\
		"uniform mat4 u_viewMatrix;\n"\
		"uniform mat4 u_projectionMatrix;\n"\

		"uniform vec3 u_lightAmbient[2];\n"\
		"uniform vec3 u_lightDiffuse[2];\n"\
		"uniform vec3 u_lightSpecular[2];\n"\
		"uniform vec4 u_lightPosition[2];\n"\

		"uniform vec3 u_materialAmbient;\n"\
		"uniform vec3 u_materialDiffuse;\n"\
		"uniform vec3 u_materialSpecular;\n"\
		"uniform float u_materialShininess;\n"\

		"uniform int u_lKeyPressed;\n"\

		"out vec3 out_phongADS_Light;\n"\

		"void main(void)\n"\

		"{\n"\
		"if (u_lKeyPressed == 1)\n"\
		"{\n"\

			"vec4 eyeCoord = u_viewMatrix * u_modelMatrix * vPosition;\n"\
			"vec3 viewVector = normalize( -eyeCoord.xyz);\n"\
			"vec3 transformedNormal = normalize( mat3(u_viewMatrix * u_modelMatrix) * vNormal );\n"\
		
			"vec3 lightDirection[2];\n"\
			"vec3 reflectionVector[2];\n"\

			"vec3 ambient[2];\n"\
			"vec3 diffuse[2];\n"\
			"vec3 specular[2];\n"\

			"for (int i = 0; i < 2; i++)\n"\
			"{\n"\

				"lightDirection[i] = normalize( vec3( u_lightPosition[i] - eyeCoord ) );\n"\

				"reflectionVector[i] = reflect( -lightDirection[i], transformedNormal );\n"\

				"ambient[i]= u_lightAmbient[i] * u_materialAmbient;\n"\

				"diffuse[i] = u_lightDiffuse[i] * u_materialDiffuse * max( dot(lightDirection[i] , transformedNormal), 0.0);\n"\

				"specular[i] = u_lightSpecular[i] * u_materialSpecular * pow( max( dot(reflectionVector[i] , viewVector), 0.0f) , u_materialShininess);\n"\

				"out_phongADS_Light += ambient[i] + diffuse[i] + specular[i];\n"\

			"}\n"\

		"}\n"\
		"else\n"\
		"{\n"\

			"out_phongADS_Light = vec3(1.0f, 1.0f, 1.0f);\n"\

		"}\n"\

		"gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix * vPosition;\n"\

		"}";


	glShaderSource(gVertexShaderObject,
					1,
					(const GLchar **)&vertexShaderSourceCode,
					NULL);

	glCompileShader(gVertexShaderObject);


	glGetShaderiv(gVertexShaderObject,
				GL_COMPILE_STATUS,
				&shaderCompileStatus);

	if(shaderCompileStatus==false)
	{
		glGetShaderiv(gVertexShaderObject,
					GL_INFO_LOG_LENGTH,
					&infoLogLenth);
		
		if(infoLogLenth > 0)
		{
			szBuffer=(GLchar *)malloc(sizeof(GLchar) * infoLogLenth);
			if(szBuffer != NULL)
			{
				glGetShaderInfoLog(gVertexShaderObject,
									infoLogLenth,
									&written,
									szBuffer);

				printf("\nERROR:[SHADER][Vertex] Compilation failed | Log :[%s]",
						szBuffer);
				
				free(szBuffer);
				UnInitialize();
				exit(1);
			}
		}
	}	

	infoLogLenth=0;
	shaderCompileStatus=0;
	szBuffer=NULL;
	written=0;

	gFragmentShaderObject=glCreateShader(GL_FRAGMENT_SHADER);

	const GLchar *fragmentShaderSourceCode=
		"#version 450 core" \
		"\n"\
		"in vec3 out_phongADS_Light;\n"
		"out vec4 fragColor;"\
		"void main(void)"\
		"{"\
		"fragColor = vec4( out_phongADS_Light, 1.0f );"\
		"}";

	
	glShaderSource(gFragmentShaderObject,
					1,
					(const GLchar **)&fragmentShaderSourceCode,
					NULL);

	glCompileShader(gFragmentShaderObject);

	glGetShaderiv(gFragmentShaderObject,
				GL_COMPILE_STATUS,
				&shaderCompileStatus);

	if(shaderCompileStatus==GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject,
					GL_INFO_LOG_LENGTH,
					&infoLogLenth);

		if(infoLogLenth > 0)
		{
			szBuffer=(GLchar *)malloc(sizeof(GLchar) * infoLogLenth);

			if(szBuffer != NULL)
			{
				glGetShaderInfoLog(gFragmentShaderObject,
									infoLogLenth,
									&written,
									szBuffer);
				printf("\nERROR:[SHADER][Fragment] Compilation failed | Log :[%s]",
						szBuffer);

				free(szBuffer);
				UnInitialize();
				exit(1);
			}
		}
	}

	infoLogLenth=0;
	shaderCompileStatus=0;
	szBuffer=NULL;
	written=0;

	gShaderProgramObject=glCreateProgram();

	glAttachShader(gShaderProgramObject,gVertexShaderObject);
	glAttachShader(gShaderProgramObject,gFragmentShaderObject);

	//Pre Link attachment
	glBindAttribLocation(gShaderProgramObject,
						VSV_ATTRIBUTE_POSITION,
						"vPosition");
	glBindAttribLocation(gShaderProgramObject,
						VSV_ATTRIBUTE_NORMAL,
						"vNormal");

	glLinkProgram(gShaderProgramObject);
	
	glGetProgramiv(gShaderProgramObject,
					GL_LINK_STATUS,
					&shaderProgramLinkStatus);

	if(shaderProgramLinkStatus==GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject,
						GL_INFO_LOG_LENGTH,
						&infoLogLenth);
		if(infoLogLenth>0)
		{
			szBuffer=(GLchar *)malloc( sizeof(GLchar) * infoLogLenth );
			if(szBuffer!=NULL)
			{
				glGetProgramInfoLog(gShaderProgramObject,
									infoLogLenth,
									&written,
									szBuffer);
				printf("\nERROR:[SHADER_PROGRAM] Linking failed | Log :[%s]",
					szBuffer);

				free(szBuffer);
				UnInitialize();
				exit(1);
			}
		}	
	}

	//Post Link attachment
	modelMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_modelMatrix");
	viewMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_viewMatrix");
	projectionlMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_projectionMatrix");

	
	lightAmbientUniform[0] = glGetUniformLocation(gShaderProgramObject, "u_lightAmbient[0]");
	lightDiffuseUniform[0] = glGetUniformLocation(gShaderProgramObject, "u_lightDiffuse[0]");
	lightSpecularUniform[0] = glGetUniformLocation(gShaderProgramObject, "u_lightSpecular[0]");
	lightPositionUniform[0] = glGetUniformLocation(gShaderProgramObject, "u_lightPosition[0]");
	
	lightAmbientUniform[1] = glGetUniformLocation(gShaderProgramObject, "u_lightAmbient[1]");
	lightDiffuseUniform[1] = glGetUniformLocation(gShaderProgramObject, "u_lightDiffuse[1]");
	lightSpecularUniform[1] = glGetUniformLocation(gShaderProgramObject, "u_lightSpecular[1]");
	lightPositionUniform[1] = glGetUniformLocation(gShaderProgramObject, "u_lightPosition[1]");

	materialAmbientUniform = glGetUniformLocation(gShaderProgramObject, "u_materialAmbient");
	materialDiffuseUniform = glGetUniformLocation(gShaderProgramObject, "u_materialDiffuse");
	materialSpecularUniform = glGetUniformLocation(gShaderProgramObject, "u_materialSpecular");
	materialShininessUniform = glGetUniformLocation(gShaderProgramObject, "u_materialShininess");

	LKeyPressedUniform = glGetUniformLocation(gShaderProgramObject, "u_lKeyPressed");

	//----------------Data prepration-------------------
	const GLfloat pyramidVertices[]=
		{
			0.0f, 1.0f, 0.0f,
			-1.0f, -1.0f, 1.0f,
			1.0f, -1.0f, 1.0f,

			0.0f, 1.0f, 0.0f,
			1.0f, -1.0f, 1.0f,
			1.0f, -1.0f, -1.0f,

			0.0f, 1.0f, 0.0f,
			1.0f, -1.0f, -1.0f,
			-1.0f, -1.0f, -1.0f,

			0.0f, 1.0f, 0.0f,
			-1.0f, -1.0f, -1.0f,
			-1.0f, -1.0f, 1.0f
		};

		const GLfloat pyramidNormals[] =
		{
			0.0f, 0.447214f, 0.894427f,
			0.0f, 0.447214f, 0.894427f,
			0.0f, 0.447214f, 0.894427f,

			0.894427f, 0.447214f, 0.0f,
			0.894427f, 0.447214f, 0.0f,
			0.894427f, 0.447214f, 0.0f,

			0.0f, 0.447214f, -0.894427f,
			0.0f, 0.447214f, -0.894427f,
			0.0f, 0.447214f, -0.894427f,

			-0.894427f, 0.447214f, 0.0f,
			-0.894427f, 0.447214f, 0.0f,
			-0.894427f, 0.447214f, 0.0f
		};


	glGenVertexArrays(1,
					&vao_pyramid);

	glBindVertexArray(vao_pyramid);

		glGenBuffers(1,
					&vbo_position);
		
		glBindBuffer(GL_ARRAY_BUFFER,
					vbo_position);
		
			glBufferData(GL_ARRAY_BUFFER,
						sizeof(pyramidVertices),
						pyramidVertices,
						GL_STATIC_DRAW);

			glVertexAttribPointer(VSV_ATTRIBUTE_POSITION,
								3,
								GL_FLOAT,
								GL_FALSE,
								0,
								NULL);
			
			glEnableVertexAttribArray(VSV_ATTRIBUTE_POSITION);

		glBindBuffer(GL_ARRAY_BUFFER,
					0);

		glGenBuffers(1, &vbo_normal);
		glBindBuffer(GL_ARRAY_BUFFER, vbo_normal);

			glBufferData(GL_ARRAY_BUFFER,
				sizeof(pyramidNormals),
				pyramidNormals,
				GL_STATIC_DRAW);

			glVertexAttribPointer(VSV_ATTRIBUTE_NORMAL,
				3,
				GL_FLOAT,
				GL_FALSE,
				0,
				NULL);

			glEnableVertexAttribArray(VSV_ATTRIBUTE_NORMAL);

		glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	glBindVertexArray(0);

	
	perspectiveProjectionMatrix=mat4::identity();

	glClearColor(0.0f,0.0f,0.0f,0.0f);

	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	Resize(giWindowWidth,giWindowHeight);
}

void Resize(int width,int height)
{
	if(height==0)
	{
		height=1;
	}

	glViewport( 0,
				0,
				(GLsizei)width,
				(GLsizei)height);

	perspectiveProjectionMatrix=vmath::perspective(45.0f,
												(GLfloat)width/(GLfloat)height,
												0.1f,
												100.0f);

}

GLfloat rotatespeeed=0.0f;

void Draw(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUseProgram(gShaderProgramObject);
			mat4 translateMatrix;
		mat4 rotateMatrix;

		mat4 modelMatrix;
		mat4 viewMatrix = mat4::identity();

		rotateMatrix = vmath::rotate(rotatespeeed, 0.0f, 1.0f, 0.0f);
		translateMatrix = vmath::translate(0.0f, 0.0f, -4.0f);
		modelMatrix = translateMatrix * rotateMatrix;

		glUniformMatrix4fv(modelMatrixUniform,
			1,
			GL_FALSE,
			modelMatrix);

		glUniformMatrix4fv(viewMatrixUniform,
			1,
			GL_FALSE,
			viewMatrix);

		glUniformMatrix4fv(projectionlMatrixUniform,
			1,
			GL_FALSE,
			perspectiveProjectionMatrix);

		if (bLights == true)
		{
			glUniform1i(LKeyPressedUniform, 1);

			for (int i = 0; i < 2; i++)
			{
				glUniform3fv(lightAmbientUniform[i], 1, light[i].LightAmbient);
				glUniform3fv(lightDiffuseUniform[i], 1, light[i].LightDefuse);
				glUniform3fv(lightSpecularUniform[i], 1, light[i].LightSpecular);

				glUniform4fv(lightPositionUniform[i], 1, light[i].LightPosition);//posiitional light
			}
			
			glUniform3fv(materialAmbientUniform, 1, materialAmbient);
			glUniform3fv(materialDiffuseUniform, 1, materilaDefuse);
			glUniform3fv(materialSpecularUniform, 1, materialSpecular);

			glUniform1f(materialShininessUniform, materialShininess);
		}
		else
		{
			glUniform1i(LKeyPressedUniform, 0);
		}



		glBindVertexArray(vao_pyramid);
			glDrawArrays(GL_TRIANGLES,
							0,
							12);
		glBindVertexArray(0);


	glUseProgram(0);

	glXSwapBuffers(gpDisplay,
					gWindow);

	rotatespeeed+=1.0f;
}

void UnInitialize(void)
{
	if(vao_pyramid)
	{
		glDeleteVertexArrays(1,
							&vao_pyramid);
	}
	vao_pyramid=0;
	
	if(vbo_normal){
		glDeleteBuffers(1,
						&vbo_normal);
	}
	vbo_normal=0;

	if(vbo_position)
	{
		glDeleteBuffers(1,
						&vbo_position);

		vbo_position=0;
	}

	if(gShaderProgramObject)
	{
		glUseProgram(gShaderProgramObject);

		GLsizei shaderCount;
		GLsizei tempShaderCount;
		GLuint *pShader=NULL;

		glGetProgramiv(gShaderProgramObject,GL_ATTACHED_SHADERS,&shaderCount);

		pShader=(GLuint *)malloc(sizeof(GLuint)*shaderCount);
		if(pShader==NULL)
		{
			printf("\nFailed to allocate memory for pShader\n");
		}else{
			glGetAttachedShaders(gShaderProgramObject,shaderCount,&tempShaderCount,pShader);
			
			printf("\nShader program has actual attached shader count is:%d\n",tempShaderCount);
			
			for(GLsizei i=0;i<shaderCount;i++)
			{
				glDetachShader(gShaderProgramObject,pShader[i]);
				glDeleteShader(pShader[i]);
				pShader[i]=0;
			}
			free(pShader);
			
			glDeleteProgram(gShaderProgramObject);
			gShaderProgramObject=0;
			glUseProgram(0);
		}
	}

	GLXContext currentGLXContext;

	currentGLXContext=glXGetCurrentContext();

	if(currentGLXContext==gGLXContext)
	{
		glXMakeCurrent(gpDisplay,
						0,
						0);
	}

	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay,
							gGLXContext);
	}

	gGLXContext=NULL;
	currentGLXContext=NULL;

	if(gWindow)
	{
		XDestroyWindow(gpDisplay,
						gWindow);
	}

	if(gColormap)
	{
		XFreeColormap(gpDisplay,
						gColormap);
	}

	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
	}
		gpXVisualInfo=NULL;

	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
	}
		gpDisplay=NULL;
}