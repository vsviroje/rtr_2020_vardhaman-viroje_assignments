#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<memory.h>

#include<GL/glew.h>
#include<GL/glx.h>
#include<GL/gl.h>

#include"vmath.h"

#include<X11/Xlib.h>
#include<X11/Xutil.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>

#include<SOIL/SOIL.h>

using namespace std;
using namespace vmath;

bool bFullScreen=false;
Display *gpDisplay=NULL;
XVisualInfo *gpXVisualInfo=NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth=800;
int giWindowHeight=600;

GLXContext gGLXContext;

//PP Changes
typedef GLXContext (*glxCreateContextAttribsARBProc)(Display *, GLXFBConfig, GLXContext, Bool, const int *);
glxCreateContextAttribsARBProc glxCreateContextAttribsARB=NULL;

GLXFBConfig gGLXFBConfig;

//Shader Changes


enum{
	VSV_ATTRIBUTE_POSITION=0,
	VSV_ATTRIBUTE_COLOR,
	VSV_ATTRIBUTE_TEXCOORD,
	VSV_ATTRIBUTE_NORMAL,
};

GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

GLuint vao_cube;
GLuint vbo_interleaved;

GLuint modelViewMatrixUniform;
GLuint projectionMatrixUniform;

GLuint LDUniform;//Light diffuse
GLuint KDUniform;//Material diffuse
GLuint lightPositionUniform; 

GLuint textureSamplerUniform;


mat4 perspectiveProjectionMatrix;

GLuint marble_Texture;

int main(void)
{
	void CreateWindow(void);
	void ToggleFullscreen(void);
	void UnInitialize(void);

	void Initialize(void);
	void Resize(int,int);
	void Draw(void);

	bool bDone=false;

	int winWidth=giWindowWidth;
	int winHeight=giWindowHeight;

	CreateWindow();
	Initialize();

	XEvent event;
	KeySym keysym;

	while(bDone==false)
	{
		while(XPending(gpDisplay))
		{
			XNextEvent(gpDisplay,
						&event);

			switch(event.type)
			{
				case MapNotify:
					break;
				case KeyPress:
					keysym=XkbKeycodeToKeysym(gpDisplay,
												event.xkey.keycode,
												0,
												0);
					switch(keysym)
					{
						case XK_Escape:
							bDone=true;
							break;

						case XK_F:
						case XK_f:
							if(bFullScreen==false)
							{
								ToggleFullscreen();
								bFullScreen=true;
							}else
							{
								ToggleFullscreen();
								bFullScreen=false;
							}
							break;
						default:
							break;
					}
					break;

				case MotionNotify:
					break;
				case ConfigureNotify:
					winWidth=event.xconfigure.width;
					winHeight=event.xconfigure.height;
					Resize(winWidth,winHeight);
					break;

				case Expose:
					break;
				case 33:
					bDone=true;
					break;
				default:
					break;
			}

		}
		Draw();
	}
	UnInitialize();
	return(0);
}

void CreateWindow(void)
{
	void UnInitialize(void);
	XSetWindowAttributes winAttribs;
	int defaultScreen;
	int styleMask;

	//PP Changes
	GLXFBConfig *pGLXFBConfig=NULL;
	GLXFBConfig bestGLXFBConfig;
	XVisualInfo *pTempXVisualInfo=NULL;
	int numFBConfigs=0;

	static int FrameBufferAttributes[]={
										GLX_X_RENDERABLE,True,
										GLX_DRAWABLE_TYPE,GLX_WINDOW_BIT,
										GLX_RENDER_TYPE,GLX_RGBA_BIT,
										GLX_X_VISUAL_TYPE,GLX_TRUE_COLOR,

										GLX_RED_SIZE,8,
										GLX_GREEN_SIZE,8,
										GLX_BLUE_SIZE,8,
										GLX_ALPHA_SIZE,8,
										
										GLX_STENCIL_SIZE,8,
										
										GLX_DOUBLEBUFFER,True,
										GLX_DEPTH_SIZE,24,
										None};	

	gpDisplay=XOpenDisplay(NULL);
	if(gpDisplay==NULL)
	{
		printf("\nERROR:Unable to Open X Display\n");
		UnInitialize();
		exit(1);
	}

	defaultScreen=XDefaultScreen(gpDisplay);
	
	//PP Changes
	pGLXFBConfig = glXChooseFBConfig(gpDisplay,
									defaultScreen,
									FrameBufferAttributes,
									&numFBConfigs);
	if(numFBConfigs == 0)
	{
		printf("\nERROR:%d-Number of Frame Buffer Config.So no need to proceed futher.\n",numFBConfigs);
		UnInitialize();
		exit(1);
	}

	printf("\nINFO:%d-Found Number of Frame Buffer Config.\n",numFBConfigs);


	int bestFrameBufferConfig=-1;
	int worstFrameBufferConfig=-1;
	int bestNumberOfSamples=-1;
	int worstNumberOfSamples=999;

	for(int i=0; i < numFBConfigs; i++)
	{
		pTempXVisualInfo=glXGetVisualFromFBConfig(gpDisplay,
												pGLXFBConfig[i]);

		if(pTempXVisualInfo != NULL)
		{
			int SampleBuffers,Samples;

			glXGetFBConfigAttrib(gpDisplay,
								pGLXFBConfig[i],
								GLX_SAMPLE_BUFFERS,
								&SampleBuffers);

			glXGetFBConfigAttrib(gpDisplay,
								pGLXFBConfig[i],
								GLX_SAMPLES,
								&Samples);

			if(bestFrameBufferConfig < 0 || SampleBuffers && Samples > bestNumberOfSamples )
			{
				bestFrameBufferConfig = i;
				bestNumberOfSamples = Samples;
			}

			if(worstFrameBufferConfig < 0 || !SampleBuffers || Samples < worstNumberOfSamples)
			{
				worstFrameBufferConfig = i;
				worstNumberOfSamples = Samples;
			}

			printf("\nINFO:When i = %d, Samples = %d, SampleBuffers = %d, pTempXVisualInfo->VisualID = %ld \n",i, Samples, SampleBuffers, pTempXVisualInfo->visualid);
		}

		XFree(pTempXVisualInfo);
		pTempXVisualInfo = NULL;
	}
	
	printf("\nINFO:bestFrameBufferConfig index = %d \n",bestFrameBufferConfig);

	bestGLXFBConfig = pGLXFBConfig[bestFrameBufferConfig];

	gGLXFBConfig = bestGLXFBConfig;

	XFree(pGLXFBConfig);
	pGLXFBConfig = NULL;

	gpXVisualInfo=(XVisualInfo *)malloc(sizeof(XVisualInfo));
	if(gpXVisualInfo==NULL)
	{
		printf("\nERROR:Unable to allocate visual info\n");
		UnInitialize();
		exit(1);
	}

	gpXVisualInfo = glXGetVisualFromFBConfig(gpDisplay,
											bestGLXFBConfig);

	winAttribs.border_pixel=0;
	winAttribs.background_pixmap=0;
	winAttribs.colormap=XCreateColormap(gpDisplay,
										RootWindow(gpDisplay,
													gpXVisualInfo->screen),
										gpXVisualInfo->visual,
										AllocNone);

	gColormap=winAttribs.colormap;

	winAttribs.background_pixel=BlackPixel(gpDisplay,
											defaultScreen);

	winAttribs.event_mask=ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;

	styleMask=CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

	gWindow=XCreateWindow(	gpDisplay,
							RootWindow(gpDisplay,
										gpXVisualInfo->screen),
							0,
							0,
							giWindowWidth,
							giWindowHeight,
							0,
							gpXVisualInfo->depth,
							InputOutput,
							gpXVisualInfo->visual,
							styleMask,
							&winAttribs);

	if(!gWindow)
	{
		printf("\nERROR:Failed to Create main window\n");
		UnInitialize();
		exit(1);
	}

	XStoreName(gpDisplay,
				gWindow,
				"XWindow-PP Interleaved cube");

	Atom windowManagerDelete=XInternAtom(gpDisplay,
										"WM_DELETE_WINDOW",
										True);

	XSetWMProtocols(gpDisplay,
					gWindow,
					&windowManagerDelete,
					1);

	XMapWindow(gpDisplay,
				gWindow);

}

void ToggleFullscreen(void)
{
	Atom wm_state;
	Atom fullscreen;
	XEvent xev={0};

	wm_state=XInternAtom(gpDisplay,
						"_NET_WM_STATE",
						False);

	memset(&xev,
			0,
			sizeof(xev));

	xev.type=ClientMessage;
	xev.xclient.window=gWindow;
	xev.xclient.message_type=wm_state;
	xev.xclient.format=32;
	xev.xclient.data.l[0]=bFullScreen?0:1;

	fullscreen=XInternAtom(gpDisplay,
							"_NET_WM_STATE_FULLSCREEN",
							False);

	xev.xclient.data.l[1]=fullscreen;

	XSendEvent(gpDisplay,
				RootWindow(gpDisplay,
							gpXVisualInfo->screen),
				False,
				StructureNotifyMask,
				&xev);
}

void Initialize(void)
{

	void Resize(int,int);
	void UnInitialize(void);
	GLuint LoadBitmapTexture(const char *);

	glxCreateContextAttribsARB = (glxCreateContextAttribsARBProc)glXGetProcAddressARB((GLubyte *)"glXCreateContextAttribsARB");

	const int attribs[] = { GLX_CONTEXT_MAJOR_VERSION_ARB, 4,
							GLX_CONTEXT_MINOR_VERSION_ARB, 5,
							GLX_CONTEXT_PROFILE_MASK_ARB, GLX_CONTEXT_CORE_PROFILE_BIT_ARB,
							None};

	gGLXContext = glxCreateContextAttribsARB(gpDisplay,
											gGLXFBConfig,
											0,
											True,
											attribs);

	if(!gGLXContext)
	{
		const int lowestAttribs[] = { 	GLX_CONTEXT_MAJOR_VERSION_ARB, 1,
										GLX_CONTEXT_MINOR_VERSION_ARB, 0,
										None};

		gGLXContext = glxCreateContextAttribsARB(gpDisplay,
											gGLXFBConfig,
											0,
											True,
											lowestAttribs);

			printf("\nINFO:Got 1.0 OpenGL Context Version\n");

	}else{
			printf("\nINFO:Got 4.5 OpenGL Context Version\n");
	}



	if(gGLXContext==NULL)
	{
		printf("\nERROR:Unable to create Rendering Context\n");
		UnInitialize();
		exit(1);
	}

	Bool isDirectContext = glXIsDirect(gpDisplay,
										gGLXContext);

	if (isDirectContext == True)
	{
		printf("\nINFO:Rendering Context is direct Hardware rendering context.\n");
	}else{
		printf("\nINFO:Rendering Context is Software rendering context.\n");
	}

	glXMakeCurrent(gpDisplay,
					gWindow,
					gGLXContext);

	GLenum glew_error=glewInit();
	if(glew_error != GLEW_OK)
	{
		printf("\nglewInit failed to enable extension for Programmable Pipeline");
		UnInitialize();
		exit(1);
	}

	printf( "\nOpenGL Information are availble below:-");
	printf( "\n*****************************************************");

	printf( "\nOpenGL Vender:%s",glGetString(GL_VENDOR));
	printf( "\nOpenGL Renderer:%s",glGetString(GL_RENDERER));
	printf( "\nOpenGL Version:%s",glGetString(GL_VERSION));
	printf( "\nOpenGL GLSL Version:%s",glGetString(GL_SHADING_LANGUAGE_VERSION));
	
	printf("\nOpenGL Extension's list:\n");
	GLint numExt;
	glGetIntegerv(GL_NUM_EXTENSIONS,&numExt);

	for(int i=0;i<numExt;i++)
	{
		printf( "\n%s",glGetStringi(GL_EXTENSIONS,i));
	}
	printf( "\n*****************************************************\n");
	GLint infoLogLength;
	GLint shaderCompileStatus;
	GLint shaderProgramLinkStatus;
	GLchar *szBuffer;
	GLsizei written;
	
	//Shader Code Start--------------------
		gVertexShaderObject=glCreateShader(GL_VERTEX_SHADER);

		const GLchar *vertexShaderSourceCode=
			"#version 460 core" \
			"\n" \
			"in vec4 vPosition;"\

			"in vec4 vColor;"\
			"out vec4 out_color;"\

			"in vec2 vTexCoord;"\
			"out vec2 out_TexCoord;"\

			"in vec3 vNormal;\n" \

			"uniform vec3 u_LD;\n" \
			"uniform vec3 u_KD;\n" \
			"uniform vec4 u_lightPosition;\n" \
			"out vec3 out_diffuseLight;\n" \

			"uniform mat4 u_modelViewMatrix;\n" \
			"uniform mat4 u_projectionMatrix;\n" \

			"void main(void)" \
			"{" \
				"out_color=vColor;"\
				"out_TexCoord=vTexCoord;"\

				"vec4 eyeCoord = u_modelViewMatrix * vPosition;\n" \
				"mat3 normalMatrix = mat3( transpose( inverse( u_modelViewMatrix )));\n" \
				"vec3 transformedNormal = normalize( normalMatrix * vNormal );\n" \
				"vec3 srcLight = normalize( vec3( u_lightPosition - eyeCoord ));\n" \
				"out_diffuseLight = u_LD * u_KD * max( dot(srcLight, transformedNormal), 0.0);\n" \

				"gl_Position = u_projectionMatrix * u_modelViewMatrix * vPosition;\n" \

			"}";

		glShaderSource(gVertexShaderObject,
						1,
						(const GLchar **)&vertexShaderSourceCode,
						NULL);

		glCompileShader(gVertexShaderObject);

		glGetShaderiv(gVertexShaderObject,
						GL_COMPILE_STATUS,
						&shaderCompileStatus);

						
		if(shaderCompileStatus==GL_FALSE)
		{
			glGetShaderiv(gVertexShaderObject,
							GL_INFO_LOG_LENGTH,
							&infoLogLength);
			if(infoLogLength>0)
			{
				szBuffer=(GLchar*)malloc(sizeof(GLchar) * infoLogLength);

				if(szBuffer!=NULL)
				{
					glGetShaderInfoLog(gVertexShaderObject,
										infoLogLength,
										&written,
										szBuffer);
					
					printf("\nERROR:Vertex shader compilation failed log: %s",szBuffer);
					free(szBuffer);
					UnInitialize();
					exit(1);
				}
			}
		}

		infoLogLength=0;
		shaderCompileStatus=0;
		shaderProgramLinkStatus=0;
		szBuffer=NULL;
		written=0;

		gFragmentShaderObject=glCreateShader(GL_FRAGMENT_SHADER);

		const GLchar *fragmentShaderSourceCode=
			"#version 450" \
			"\n" \
			"out vec4 fragColor;"\
			
			"in vec4 out_color;"\
			"in vec2 out_TexCoord;"\

			"uniform sampler2D u_texture_sampler;"\

			"in vec3 out_diffuseLight;"\

			"void main(void)" \
			"{"\
				"fragColor = out_color;"\
				"fragColor *= vec4( out_diffuseLight, 1.0 );"\
				"fragColor *= texture(u_texture_sampler,out_TexCoord);"\
			"}";

		glShaderSource(gFragmentShaderObject,
						1,
						(const GLchar**)&fragmentShaderSourceCode,
						NULL);

		glCompileShader(gFragmentShaderObject);

		glGetShaderiv(gFragmentShaderObject,
					GL_COMPILE_STATUS,
					&shaderCompileStatus);

		
		if(shaderCompileStatus==GL_FALSE)
		{
			glGetShaderiv(gVertexShaderObject,
							GL_INFO_LOG_LENGTH,
							&infoLogLength);

			if(infoLogLength>0)
			{
				szBuffer=(GLchar*)malloc(sizeof(GLchar) * infoLogLength);

				if(szBuffer!=NULL)
				{
					glGetShaderInfoLog(gFragmentShaderObject,
										infoLogLength,
										&written,
										szBuffer);
					
					printf("\nERROR:Fragment shader compilation failed log: %s",szBuffer);
					free(szBuffer);
					UnInitialize();
					exit(1);
				}
			}
		}
		
		infoLogLength=0;
		shaderCompileStatus=0;
		shaderProgramLinkStatus=0;
		szBuffer=NULL;
		written=0;

		gShaderProgramObject=glCreateProgram();

		glAttachShader(gShaderProgramObject,gVertexShaderObject);
		glAttachShader(gShaderProgramObject,gFragmentShaderObject);
		//Shader Attribute Binding Before Pre Linking of attached Shader

			glBindAttribLocation(gShaderProgramObject,VSV_ATTRIBUTE_POSITION,"vPosition");
			glBindAttribLocation(gShaderProgramObject, VSV_ATTRIBUTE_COLOR, "vColor");
			glBindAttribLocation(gShaderProgramObject, VSV_ATTRIBUTE_NORMAL, "vNormal");
			glBindAttribLocation(gShaderProgramObject, VSV_ATTRIBUTE_TEXCOORD, "vTexCoord");

		glLinkProgram(gShaderProgramObject);

		glGetProgramiv(gShaderProgramObject,
						GL_LINK_STATUS,
						&shaderProgramLinkStatus);
		if(shaderProgramLinkStatus==GL_FALSE)
		{
			glGetProgramiv(gShaderProgramObject,
						GL_INFO_LOG_LENGTH,
						&infoLogLength);
			if(infoLogLength>0)
			{
				szBuffer=(GLchar*)malloc(sizeof(GLchar)*infoLogLength);
				if(szBuffer!=NULL)
				{
					glGetProgramInfoLog(gShaderProgramObject,
										infoLogLength,
										&written,
										szBuffer);

					printf("\nERROR:Shader Program Link failed log: %s",szBuffer);
					free(szBuffer);
					UnInitialize();
					exit(1);
				}
			}
		}
		//Uniform variable Binding after post linking of attached shader
		modelViewMatrixUniform = glGetUniformLocation(gShaderProgramObject,
			"u_modelViewMatrix");

		projectionMatrixUniform = glGetUniformLocation(gShaderProgramObject,
			"u_projectionMatrix");


		LDUniform = glGetUniformLocation(gShaderProgramObject,
			"u_LD");

		KDUniform = glGetUniformLocation(gShaderProgramObject,
			"u_KD");

		lightPositionUniform = glGetUniformLocation(gShaderProgramObject,
			"u_lightPosition");

		textureSamplerUniform = glGetUniformLocation(gShaderProgramObject,
			"u_texture_sampler");

	//Shader Code End--------------------

	//VAO and its respective VBO preparation

		const GLfloat cube_PCNT[]=
		//	position(xyz)				Color(rgb)			Normal(xyz)			texture(st)
		{
			1.0f, 1.0f, 1.0f,		1.0f, 0.0f, 0.0f,	0.0f, 0.0f, 1.0f,		0.0f, 0.0f,
			-1.0f, 1.0f, 1.0f,		1.0f, 0.0f, 0.0f,	0.0f, 0.0f, 1.0f,		1.0f, 0.0f,
			-1.0f, -1.0f, 1.0f,		1.0f, 0.0f, 0.0f,	0.0f, 0.0f, 1.0f,		1.0f, 1.0f,
			1.0f, -1.0f, 1.0f,		1.0f, 0.0f, 0.0f,	0.0f, 0.0f, 1.0f,		0.0f, 1.0f,

			1.0f, 1.0f, -1.0f,		0.0f, 1.0f, 0.0f,	1.0f, 0.0f, 0.0f,		1.0f, 0.0f,
			1.0f, 1.0f, 1.0f,		0.0f, 1.0f, 0.0f,	1.0f, 0.0f, 0.0f,		1.0f, 1.0f,
			1.0f, -1.0f, 1.0f,		0.0f, 1.0f, 0.0f,	1.0f, 0.0f, 0.0f,		0.0f, 1.0f,
			1.0f, -1.0f, -1.0f,		0.0f, 1.0f, 0.0f,	1.0f, 0.0f, 0.0f,		0.0f, 0.0f,

			-1.0f, 1.0f, -1.0f,		0.0f, 0.0f, 1.0f,	0.0f, 0.0f, -1.0f,		1.0f, 0.0f,
			1.0f, 1.0f, -1.0f,		0.0f, 0.0f, 1.0f,	0.0f, 0.0f, -1.0f,		1.0f, 1.0f,
			1.0f, -1.0f, -1.0f,		0.0f, 0.0f, 1.0f,	0.0f, 0.0f, -1.0f,		0.0f, 1.0f,
			-1.0f, -1.0f, -1.0f,	0.0f, 0.0f, 1.0f,	0.0f, 0.0f, -1.0f,		0.0f, 0.0f,

			-1.0f, 1.0f, 1.0f,		0.0f, 1.0f, 1.0f,	-1.0f, 0.0f, 0.0f,		0.0f, 0.0f,
			-1.0f, 1.0f, -1.0f,		0.0f, 1.0f, 1.0f,	-1.0f, 0.0f, 0.0f,		1.0f, 0.0f,
			-1.0f, -1.0f, -1.0f,	0.0f, 1.0f, 1.0f,	-1.0f, 0.0f, 0.0f,		1.0f, 1.0f,
			-1.0f, -1.0f, 1.0f,		0.0f, 1.0f, 1.0f,	-1.0f, 0.0f, 0.0f,		0.0f, 1.0f,

			1.0f, 1.0f, -1.0f,		1.0f, 0.0f, 1.0f,	0.0f, 1.0f, 0.0f,		0.0f, 1.0f,
			-1.0f, 1.0f, -1.0f,		1.0f, 0.0f, 1.0f,	0.0f, 1.0f, 0.0f,		0.0f, 0.0f,
			-1.0f, 1.0f, 1.0f,		1.0f, 0.0f, 1.0f,	0.0f, 1.0f, 0.0f,		1.0f, 0.0f,
			1.0f, 1.0f, 1.0f,		1.0f, 0.0f, 1.0f,	0.0f, 1.0f, 0.0f,		1.0f, 1.0f,

			1.0f, -1.0f, -1.0f,		1.0f, 1.0f, 0.0f,	0.0f, -1.0f, 0.0f,		1.0f, 1.0f,
			-1.0f, -1.0f, -1.0f,	1.0f, 1.0f, 0.0f,	0.0f, -1.0f, 0.0f,		0.0f, 1.0f,
			-1.0f, -1.0f, 1.0f,		1.0f, 1.0f, 0.0f,	0.0f, -1.0f, 0.0f,		0.0f, 0.0f,
			1.0f, -1.0f, 1.0f,		1.0f, 1.0f, 0.0f,	0.0f, -1.0f, 0.0f,		1.0f, 0.0f
		};

	
		glGenVertexArrays(1,&vao_cube);
		glBindVertexArray(vao_cube);
			glGenBuffers(1,&vbo_interleaved);
			glBindBuffer(GL_ARRAY_BUFFER, vbo_interleaved);

				glBufferData(GL_ARRAY_BUFFER,
							sizeof(cube_PCNT),	//24*11*sizeof(float)
							cube_PCNT,
							GL_STATIC_DRAW);

				//vertex
				glVertexAttribPointer(VSV_ATTRIBUTE_POSITION,
									3,
									GL_FLOAT,
									GL_FALSE,
									11*sizeof(float),
									(void*)0);

				glEnableVertexAttribArray(VSV_ATTRIBUTE_POSITION);

				//color
				glVertexAttribPointer(VSV_ATTRIBUTE_COLOR,
										3,
										GL_FLOAT,
										GL_FALSE,
										11 * sizeof(float),
										(void*)(3 * sizeof(float)));

				glEnableVertexAttribArray(VSV_ATTRIBUTE_COLOR);

				//normal
				glVertexAttribPointer(VSV_ATTRIBUTE_NORMAL,
										3,
										GL_FLOAT,
										GL_FALSE,
										11 * sizeof(float),
										(void*)(6 * sizeof(float)));

				glEnableVertexAttribArray(VSV_ATTRIBUTE_NORMAL);

				//texture
				glVertexAttribPointer(VSV_ATTRIBUTE_TEXCOORD,
										2,
										GL_FLOAT,
										GL_FALSE,
										11 * sizeof(float),
										(void*)(9 * sizeof(float)));

				glEnableVertexAttribArray(VSV_ATTRIBUTE_TEXCOORD);

			glBindBuffer(GL_ARRAY_BUFFER,0);

		glBindVertexArray(0);


	glClearColor(0.0,0.0,0.0f,0.0f);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	glEnable(GL_TEXTURE_2D);
	marble_Texture=LoadBitmapTexture("marble.bmp");

	perspectiveProjectionMatrix=mat4::identity();

	Resize(giWindowWidth,giWindowHeight);
}

void Resize(int width,int height)
{
	if(height==0)
	{
		height=1;
	}

	glViewport( 0,
				0,
				(GLsizei)width,
				(GLsizei)height);

	perspectiveProjectionMatrix=vmath::perspective(45.0f,
													(GLfloat)width/(GLfloat)height,
													0.1f,
													100.0f);

}

GLfloat cubeRotate=0.0f;

void Draw(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glUseProgram(gShaderProgramObject);
		mat4 modelViewMatrix;
		mat4 translateMatrix;
		mat4 rotateMatrix;

		glUniform3f(LDUniform, 1.0f, 1.0f, 1.0f);

		glUniform3f(KDUniform, 1.0f, 1.0f, 1.0f);

		GLfloat lightPosition[] = { 0.0f,0.0f,2.0f , 1.0f };//4 th element defines Positional light

		glUniform4fv(lightPositionUniform,
			1,//Number of array 
			(GLfloat*)lightPosition);
	

		rotateMatrix=vmath::rotate(cubeRotate,1.0f,0.0f,0.0f);
		rotateMatrix*=vmath::rotate(cubeRotate,0.0f,1.0f,0.0f);
		rotateMatrix*=vmath::rotate(cubeRotate,0.0f,0.0f,1.0f);

		translateMatrix=vmath::translate(0.0f,0.0f,-6.0f);

		modelViewMatrix=translateMatrix * rotateMatrix  ;

		
		glUniformMatrix4fv(modelViewMatrixUniform,
						1,
						GL_FALSE,
						modelViewMatrix);

		glUniformMatrix4fv(projectionMatrixUniform,
							1,
							GL_FALSE,
							perspectiveProjectionMatrix);

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, marble_Texture);
		glUniform1i(textureSamplerUniform,
			0);

		glBindVertexArray(vao_cube);
			glDrawArrays(GL_TRIANGLE_FAN,0,4);
			glDrawArrays(GL_TRIANGLE_FAN,4,4);
			glDrawArrays(GL_TRIANGLE_FAN,8,4);
			glDrawArrays(GL_TRIANGLE_FAN,12,4);
			glDrawArrays(GL_TRIANGLE_FAN,16,4);
			glDrawArrays(GL_TRIANGLE_FAN,20,4);
		glBindVertexArray(0);

	glUseProgram(0);

	glXSwapBuffers(gpDisplay,
					gWindow);

	cubeRotate+=1.0f;
}

void UnInitialize(void)
{
	if(vao_cube)
	{
		glDeleteVertexArrays(1,
							&vao_cube);
	}
	vao_cube=0;

	if(vbo_interleaved)
	{
		glDeleteBuffers(1,
					&vbo_interleaved);
	}
	vbo_interleaved =0;

	if(gShaderProgramObject)
	{
		glUseProgram(gShaderProgramObject);

		GLsizei shaderCount;
		GLsizei tempShaderCount;
		GLuint *pShader=NULL;

		glGetProgramiv(gShaderProgramObject,GL_ATTACHED_SHADERS,&shaderCount);

		pShader=(GLuint *)malloc(sizeof(GLuint)*shaderCount);
		if(pShader==NULL)
		{
			printf("\nFailed to allocate memory for pShader");
		}else{
			glGetAttachedShaders(gShaderProgramObject,shaderCount,&tempShaderCount,pShader);
			
			printf("\nShader program has actual attached shader count is:%d",tempShaderCount);
			
			for(GLsizei i=0;i<shaderCount;i++)
			{
				glDetachShader(gShaderProgramObject,pShader[i]);
				glDeleteShader(pShader[i]);
				pShader[i]=0;
			}
			free(pShader);
			
			glDeleteProgram(gShaderProgramObject);
			gShaderProgramObject=0;
			glUseProgram(0);
		}
	}

	GLXContext currentGLXContext;

	currentGLXContext=glXGetCurrentContext();

	if(currentGLXContext==gGLXContext)
	{
		glXMakeCurrent(gpDisplay,
						0,
						0);
	}

	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay,
							gGLXContext);
	}

	gGLXContext=NULL;
	currentGLXContext=NULL;

	if(gWindow)
	{
		XDestroyWindow(gpDisplay,
						gWindow);
	}

	if(gColormap)
	{
		XFreeColormap(gpDisplay,
						gColormap);
	}

	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
	}
		gpXVisualInfo=NULL;

	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
	}
		gpDisplay=NULL;
}

GLuint LoadBitmapTexture(const char *path)
{
	int width,height;
	unsigned char* imageData=NULL;
	GLuint texture_id;

	imageData=SOIL_load_image(	path,
								&width,
								&height,
								NULL,
								SOIL_LOAD_RGB);

	glPixelStorei(	GL_UNPACK_ALIGNMENT,
					1);

	glGenTextures(	1,
					&texture_id);


	glBindTexture(	GL_TEXTURE_2D,
					texture_id);

	glTexParameteri(GL_TEXTURE_2D,
					GL_TEXTURE_MIN_FILTER,
					GL_LINEAR_MIPMAP_LINEAR);

	glTexParameteri(GL_TEXTURE_2D,
					GL_TEXTURE_MAG_FILTER,
					GL_LINEAR);

	glTexImage2D(GL_TEXTURE_2D,
					0,
					GL_RGB,
					width,
					height,
					0,
					GL_RGB,
					GL_UNSIGNED_BYTE,
					imageData);

	glGenerateMipmap(GL_TEXTURE_2D);

	SOIL_free_image_data(imageData);

	// if  (imageData !=NULL ){
	// 	printf("INFO | %d \n",texture_id);
	// }
	return texture_id;
}