#include<stdio.h>
#include<stdlib.h>

#include "vmath.h"
using namespace vmath;

typedef mat4 datatype;

typedef struct STACK{
    datatype data;
    struct STACK *next; 
}node,*pNode,**ppNode;

/*
int main()
{  
    pNode CreateStackNode(datatype);
    void PushIntoStack(ppNode,pNode);
    void DisplayStack(ppNode);
    datatype TopData(ppNode);
    datatype PopFromStack(ppNode);
    void DeleteAllStackNode(ppNode);

    int NumElement,i;
    datatype ElementValue;

    pNode temp=NULL;  
    pNode TOP=NULL;

    printf("\nEnter Number of element to add in stack:\t");
    scanf("%d",&NumElement); 
    
    for(i=0;i<NumElement;i++)
    {
        printf("\nEnter Data:%d:\t",i+1);
        scanf("%d",&ElementValue);
        temp=CreateStackNode(ElementValue);
        PushIntoStack(&TOP,temp);
    }

    DisplayStack(&TOP);

    ElementValue=TopData(&TOP);
    printf("\nTop Data:%d",ElementValue);

    ElementValue=PopFromStack(&TOP);
    printf("\nPoped Data:%d",ElementValue);

    DisplayStack(&TOP);

    DeleteAllStackNode(&TOP);

    DisplayStack(&TOP);


    return 0;
}

*/

pNode CreateStackNode(datatype data)
{

    pNode temp=NULL;
    temp=(pNode)malloc(sizeof(node));

    if(temp==NULL)
    {
        printf("\nFailed to Create node.");
        exit(0);
    }

    temp->data=data;
    temp->next=NULL;

    return temp;
}

void PushIntoStack(ppNode top,pNode currentNode)
{
    if (currentNode==NULL)
    {
        printf("\ncurrentNode is NULL");
        exit(0);
    }
    currentNode->next=*top;
    *top=currentNode;
}

void DisplayStack(ppNode top)
{
    int isEmptyStack(ppNode);

    if(isEmptyStack(top))
    {
        printf("\nStack is Empty");
        exit(0);
    }

    pNode temp=NULL;
    temp=*top;

    printf("\nDisplaying all node in stack:\n");
    
    while(temp!=NULL)
    {
        printf("|%d|->",temp->data);
        temp=temp->next;
    }
}

int isEmptyStack(ppNode top)
{
    if(top==NULL || *top==NULL)
    {
        return 1;
    }
    return 0;
}

datatype TopData(ppNode top)
{
    int isEmptyStack(ppNode);

    if(isEmptyStack(top))
    {
        printf("\nStack is Empty");
        exit(0);
    }
    return (*top)->data;
}

datatype PopFromStack(ppNode top)
{
    int isEmptyStack(ppNode);

    if(isEmptyStack(top))
    {
        printf("\nStack is Empty");
        exit(0);
    }

    datatype data;
    pNode temp=NULL;

    temp=*top;
   
    *top=(*top)->next;
    data=temp->data;
    
    free(temp);
    //printf("\n|%d|->",temp->data);

    return data;

}

void DeleteAllStackNode(ppNode top)
{
    int isEmptyStack(ppNode);

    pNode walk=NULL,temp=NULL;
   
    if(isEmptyStack(top))
    {
        printf("\nStack is Empty");
        exit(0);
    }

    walk=*top;
    printf("\nDeleting all node in stack:\n");

    while(walk->next!=NULL)
    {
        temp=walk->next;
        walk->next=temp->next;
        printf("|%d|->",temp->data);
        free(temp);
    }
    
    printf("|%d|->",walk->data);
    free(walk);   
    
    // printf("\n|%d|->",walk->data);

    *top=NULL;

}