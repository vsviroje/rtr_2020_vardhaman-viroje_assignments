#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<memory.h>
#include "Sphere.h"

#include<GL/glew.h>
#include<GL/glx.h>
#include<GL/gl.h>

#include"vmath.h"

#include<X11/Xlib.h>
#include<X11/Xutil.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>


using namespace std;
using namespace vmath;

bool bFullScreen=false;
Display *gpDisplay=NULL;
XVisualInfo *gpXVisualInfo=NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth=800;
int giWindowHeight=600;

GLXContext gGLXContext;

//PP Changes
typedef GLXContext (*glxCreateContextAttribsARBProc)(Display *, GLXFBConfig, GLXContext, Bool, const int *);
glxCreateContextAttribsARBProc glxCreateContextAttribsARB=NULL;

GLXFBConfig gGLXFBConfig;

//Shader Changes

enum{
	VSV_ATTRIBUTE_POSITION=0,
	VSV_ATTRIBUTE_COLOR,
	VSV_ATTRIBUTE_TEXCOORD,
	VSV_ATTRIBUTE_NORMAL,
};

GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;

GLuint gPV_ShaderProgramObject;
GLuint gPF_ShaderProgramObject;

GLuint vao_sphere;

GLuint vbo_position;
GLuint vbo_normal;
GLuint vbo_element;

GLuint modelMatrixUniform_PV;
GLuint viewMatrixUniform_PV;
GLuint projectionlMatrixUniform_PV;

GLuint lightAmbientUniform_PV[3];
GLuint lightDiffuseUniform_PV[3];
GLuint lightSpecularUniform_PV[3];
GLuint lightPositionUniform_PV[3];

GLuint materialAmbientUniform_PV;
GLuint materialDiffuseUniform_PV;
GLuint materialSpecularUniform_PV;
GLuint materialShininessUniform_PV;

GLuint LKeyPressedUniform_PV;

//------------------------------
GLuint modelMatrixUniform_PF;
GLuint viewMatrixUniform_PF;
GLuint projectionlMatrixUniform_PF;

GLuint lightAmbientUniform_PF[3];
GLuint lightDiffuseUniform_PF[3];
GLuint lightSpecularUniform_PF[3];
GLuint lightPositionUniform_PF[3];

GLuint materialAmbientUniform_PF;
GLuint materialDiffuseUniform_PF;
GLuint materialSpecularUniform_PF;
GLuint materialShininessUniform_PF;

GLuint LKeyPressedUniform_PF;

bool bLights;
bool bTogglePVxOrPF;//True-Per Fragment and False-Per Vertex


typedef struct Light {
	vec4 LightAmbient;
	vec4 LightDefuse;
	vec4 LightSpecular;
	vec4 LightPosition;
}LightStruct;

LightStruct light[3] = { 
						{
							vec4(0.0f,0.0f,0.0f,1.0f),
							vec4(1.0f,0.0f,0.0f,1.0f),
							vec4(1.0f,0.0f,0.0f,1.0f),
							vec4(2.0f,0.0f,0.0f,1.0f)
						},
						{
							vec4(0.0f,0.0f,0.0f,1.0f),
							vec4(0.0f,1.0f,0.0f,1.0f),
							vec4(0.0f,1.0f,0.0f,1.0f),
							vec4(-2.0f,0.0f,0.0f,1.0f)
						},
						{
							vec4(0.0f,0.0f,0.0f,1.0f),
							vec4(0.0f,0.0f,1.0f,1.0f),
							vec4(0.0f,0.0f,1.0f,1.0f),
							vec4(0.0f,0.0f,0.0f,1.0f)
						}
					};


GLfloat materialAmbient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat materilaDefuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat materialSpecular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat materialShininess = 128.0f;


mat4 perspectiveProjectionMatrix;

float spherePosition[1146];
float sphereNormals[1146];
float sphereTexcoord[764];
unsigned short sphereElement[2280];//Face Indices

GLuint gNumSphereVertices, gNumSphereElements;

int main(void)
{
	void CreateWindow(void);
	void ToggleFullscreen(void);
	void UnInitialize(void);

	void Initialize(void);
	void Resize(int,int);
	void Draw(void);

	bool bDone=false;

	int winWidth=giWindowWidth;
	int winHeight=giWindowHeight;

	CreateWindow();
	Initialize();

	XEvent event;
	KeySym keysym;

	while(bDone==false)
	{
		while(XPending(gpDisplay))
		{
			XNextEvent(gpDisplay,
						&event);

			switch(event.type)
			{
				case MapNotify:
					break;
				case KeyPress:
					keysym=XkbKeycodeToKeysym(gpDisplay,
												event.xkey.keycode,
												0,
												0);
					switch(keysym)
					{
						case XK_Escape:
							if(bFullScreen==false)
							{
								ToggleFullscreen();
								bFullScreen=true;
							}else
							{
								ToggleFullscreen();
								bFullScreen=false;
							}
							break;

						case XK_F:
						case XK_f:
							bTogglePVxOrPF = true;			
							break;

						case XK_V:
						case XK_v:
							bTogglePVxOrPF = false;
							break;

						case XK_L:
						case XK_l:
							bLights = !bLights;
							if (bLights)
							{
								bTogglePVxOrPF = false;
							}
							break;

						case XK_Q:
						case XK_q:
							bDone=true;
							break;
						default:
							break;
					}
					break;

				case MotionNotify:
					break;
				case ConfigureNotify:
					winWidth=event.xconfigure.width;
					winHeight=event.xconfigure.height;
					Resize(winWidth,winHeight);
					break;

				case Expose:
					break;
				case 33:
					bDone=true;
					break;
				default:
					break;
			}

		}
		Draw();
	}
	UnInitialize();
	return(0);
}

void CreateWindow(void)
{
	void UnInitialize(void);
	XSetWindowAttributes winAttribs;
	int defaultScreen;
	int styleMask;

	//PP Changes
	GLXFBConfig *pGLXFBConfig=NULL;
	GLXFBConfig bestGLXFBConfig;
	XVisualInfo *pTempXVisualInfo=NULL;
	int numFBConfigs=0;

	static int FrameBufferAttributes[]={
										GLX_X_RENDERABLE,True,
										GLX_DRAWABLE_TYPE,GLX_WINDOW_BIT,
										GLX_RENDER_TYPE,GLX_RGBA_BIT,
										GLX_X_VISUAL_TYPE,GLX_TRUE_COLOR,

										GLX_RED_SIZE,8,
										GLX_GREEN_SIZE,8,
										GLX_BLUE_SIZE,8,
										GLX_ALPHA_SIZE,8,
										
										GLX_STENCIL_SIZE,8,
										
										GLX_DOUBLEBUFFER,True,
										GLX_DEPTH_SIZE,24,
										None};	

	gpDisplay=XOpenDisplay(NULL);
	if(gpDisplay==NULL)
	{
		printf("\nERROR:Unable to Open X Display\n");
		UnInitialize();
		exit(1);
	}

	defaultScreen=XDefaultScreen(gpDisplay);
	
	//PP Changes
	pGLXFBConfig = glXChooseFBConfig(gpDisplay,
									defaultScreen,
									FrameBufferAttributes,
									&numFBConfigs);
	if(numFBConfigs == 0)
	{
		printf("\nERROR:%d-Number of Frame Buffer Config.So no need to proceed futher.\n",numFBConfigs);
		UnInitialize();
		exit(1);
	}

	printf("\nINFO:%d-Found Number of Frame Buffer Config.\n",numFBConfigs);


	int bestFrameBufferConfig=-1;
	int worstFrameBufferConfig=-1;
	int bestNumberOfSamples=-1;
	int worstNumberOfSamples=999;

	for(int i=0; i < numFBConfigs; i++)
	{
		pTempXVisualInfo=glXGetVisualFromFBConfig(gpDisplay,
												pGLXFBConfig[i]);

		if(pTempXVisualInfo != NULL)
		{
			int SampleBuffers,Samples;

			glXGetFBConfigAttrib(gpDisplay,
								pGLXFBConfig[i],
								GLX_SAMPLE_BUFFERS,
								&SampleBuffers);

			glXGetFBConfigAttrib(gpDisplay,
								pGLXFBConfig[i],
								GLX_SAMPLES,
								&Samples);

			if(bestFrameBufferConfig < 0 || SampleBuffers && Samples > bestNumberOfSamples )
			{
				bestFrameBufferConfig = i;
				bestNumberOfSamples = Samples;
			}

			if(worstFrameBufferConfig < 0 || !SampleBuffers || Samples < worstNumberOfSamples)
			{
				worstFrameBufferConfig = i;
				worstNumberOfSamples = Samples;
			}

			printf("\nINFO:When i = %d, Samples = %d, SampleBuffers = %d, pTempXVisualInfo->VisualID = %ld \n",i, Samples, SampleBuffers, pTempXVisualInfo->visualid);
		}

		XFree(pTempXVisualInfo);
		pTempXVisualInfo = NULL;
	}
	
	printf("\nINFO:bestFrameBufferConfig index = %d \n",bestFrameBufferConfig);

	bestGLXFBConfig = pGLXFBConfig[bestFrameBufferConfig];

	gGLXFBConfig = bestGLXFBConfig;

	XFree(pGLXFBConfig);
	pGLXFBConfig = NULL;

	gpXVisualInfo=(XVisualInfo *)malloc(sizeof(XVisualInfo));
	if(gpXVisualInfo==NULL)
	{
		printf("\nERROR:Unable to allocate visual info\n");
		UnInitialize();
		exit(1);
	}

	gpXVisualInfo = glXGetVisualFromFBConfig(gpDisplay,
											bestGLXFBConfig);

	winAttribs.border_pixel=0;
	winAttribs.background_pixmap=0;
	winAttribs.colormap=XCreateColormap(gpDisplay,
										RootWindow(gpDisplay,
													gpXVisualInfo->screen),
										gpXVisualInfo->visual,
										AllocNone);

	gColormap=winAttribs.colormap;

	winAttribs.background_pixel=BlackPixel(gpDisplay,
											defaultScreen);

	winAttribs.event_mask=ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;

	styleMask=CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

	gWindow=XCreateWindow(	gpDisplay,
							RootWindow(gpDisplay,
										gpXVisualInfo->screen),
							0,
							0,
							giWindowWidth,
							giWindowHeight,
							0,
							gpXVisualInfo->depth,
							InputOutput,
							gpXVisualInfo->visual,
							styleMask,
							&winAttribs);

	if(!gWindow)
	{
		printf("\nERROR:Failed to Create main window\n");
		UnInitialize();
		exit(1);
	}

	XStoreName(gpDisplay,
				gWindow,
				"XWindow-PP per vertex lighting sphere");

	Atom windowManagerDelete=XInternAtom(gpDisplay,
										"WM_DELETE_WINDOW",
										True);

	XSetWMProtocols(gpDisplay,
					gWindow,
					&windowManagerDelete,
					1);

	XMapWindow(gpDisplay,
				gWindow);

}

void ToggleFullscreen(void)
{
	Atom wm_state;
	Atom fullscreen;
	XEvent xev={0};

	wm_state=XInternAtom(gpDisplay,
						"_NET_WM_STATE",
						False);

	memset(&xev,
			0,
			sizeof(xev));

	xev.type=ClientMessage;
	xev.xclient.window=gWindow;
	xev.xclient.message_type=wm_state;
	xev.xclient.format=32;
	xev.xclient.data.l[0]=bFullScreen?0:1;

	fullscreen=XInternAtom(gpDisplay,
							"_NET_WM_STATE_FULLSCREEN",
							False);

	xev.xclient.data.l[1]=fullscreen;

	XSendEvent(gpDisplay,
				RootWindow(gpDisplay,
							gpXVisualInfo->screen),
				False,
				StructureNotifyMask,
				&xev);
}

void Initialize(void)
{

	void Resize(int,int);
	void UnInitialize(void);

	glxCreateContextAttribsARB = (glxCreateContextAttribsARBProc)glXGetProcAddressARB((GLubyte *)"glXCreateContextAttribsARB");

	const int attribs[] = { GLX_CONTEXT_MAJOR_VERSION_ARB, 4,
							GLX_CONTEXT_MINOR_VERSION_ARB, 5,
							GLX_CONTEXT_PROFILE_MASK_ARB, GLX_CONTEXT_CORE_PROFILE_BIT_ARB,
							None};

	gGLXContext = glxCreateContextAttribsARB(gpDisplay,
											gGLXFBConfig,
											0,
											True,
											attribs);

	if(!gGLXContext)
	{
		const int lowestAttribs[] = { 	GLX_CONTEXT_MAJOR_VERSION_ARB, 1,
										GLX_CONTEXT_MINOR_VERSION_ARB, 0,
										None};

		gGLXContext = glxCreateContextAttribsARB(gpDisplay,
											gGLXFBConfig,
											0,
											True,
											lowestAttribs);

			printf("\nINFO:Got 1.0 OpenGL Context Version\n");

	}else{
			printf("\nINFO:Got 4.5 OpenGL Context Version\n");
	}



	if(gGLXContext==NULL)
	{
		printf("\nERROR:Unable to create Rendering Context\n");
		UnInitialize();
		exit(1);
	}

	Bool isDirectContext = glXIsDirect(gpDisplay,
										gGLXContext);

	if (isDirectContext == True)
	{
		printf("\nINFO:Rendering Context is direct Hardware rendering context.\n");
	}else{
		printf("\nINFO:Rendering Context is Software rendering context.\n");
	}

	glXMakeCurrent(gpDisplay,
					gWindow,
					gGLXContext);

	GLenum glew_error=glewInit();
	if(glew_error != GLEW_OK)
	{
		printf("\nglewInit failed to enable extension for Programmable Pipeline");
		UnInitialize();
		exit(1);
	}

	printf( "\nOpenGL Information are availble below:-");
	printf( "\n*****************************************************");

	printf( "\nOpenGL Vender:%s",glGetString(GL_VENDOR));
	printf( "\nOpenGL Renderer:%s",glGetString(GL_RENDERER));
	printf( "\nOpenGL Version:%s",glGetString(GL_VERSION));
	printf( "\nOpenGL GLSL Version:%s",glGetString(GL_SHADING_LANGUAGE_VERSION));
	
	printf("\nOpenGL Extension's list:\n");
	GLint numExt;
	glGetIntegerv(GL_NUM_EXTENSIONS,&numExt);

	for(int i=0;i<numExt;i++)
	{
		printf( "\n%s",glGetStringi(GL_EXTENSIONS,i));
	}
	printf( "\n*****************************************************\n");

	GLint infoLogLength=0;
	GLint shaderCompileStatus=0;
	GLint shaderProgramLinkStatus=0;
	GLchar *szBuffer=NULL;
	GLsizei written;


	//Per Vertex Shader Code Start--------------------
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	const GLchar* vertexShaderSourceCode_PV =
		"#version 460 core\n"\
		"\n"\
		"in vec4 vPosition;\n"\
		"in vec3 vNormal;\n"\

		"uniform mat4 u_modelMatrix;\n"\
		"uniform mat4 u_viewMatrix;\n"\
		"uniform mat4 u_projectionMatrix;\n"\

		"uniform vec3 u_lightAmbient[3];\n"\
		"uniform vec3 u_lightDiffuse[3];\n"\
		"uniform vec3 u_lightSpecular[3];\n"\
		"uniform vec4 u_lightPosition[3];\n"\

		"uniform vec3 u_materialAmbient;\n"\
		"uniform vec3 u_materialDiffuse;\n"\
		"uniform vec3 u_materialSpecular;\n"\
		"uniform float u_materialShininess;\n"\

		"uniform int u_lKeyPressed;\n"\

		"out vec3 out_phongADS_Light;\n"\

		"void main(void)\n"\

		"{\n"\
			"if (u_lKeyPressed == 1)\n"\
			"{\n"\

				"vec4 eyeCoord = u_viewMatrix * u_modelMatrix * vPosition;\n"\
				"vec3 viewVector = normalize( -eyeCoord.xyz);\n"\
				"vec3 transformedNormal = normalize( mat3(u_viewMatrix * u_modelMatrix) * vNormal );\n"\

				"vec3 lightDirection[3];\n"\
				"vec3 reflectionVector[3];\n"\

				"vec3 ambient[3];\n"\
				"vec3 diffuse[3];\n"\
				"vec3 specular[3];\n"\

				"for (int i = 0; i < 3; i++)\n"\
				"{\n"\

					"lightDirection[i] = normalize( vec3( u_lightPosition[i] - eyeCoord ) );\n"\

					"reflectionVector[i] = reflect( -lightDirection[i], transformedNormal );\n"\

					"ambient[i]= u_lightAmbient[i] * u_materialAmbient;\n"\

					"diffuse[i] = u_lightDiffuse[i] * u_materialDiffuse * max( dot(lightDirection[i] , transformedNormal), 0.0);\n"\

					"specular[i] = u_lightSpecular[i] * u_materialSpecular * pow( max( dot(reflectionVector[i] , viewVector), 0.0f) , u_materialShininess);\n"\

					"out_phongADS_Light += ambient[i] + diffuse[i] + specular[i];\n"\

				"}\n"\

				"}\n"\
			"else\n"\
			"{\n"\

				"out_phongADS_Light = vec3(1.0f, 1.0f, 1.0f);\n"\

			"}\n"\

			"gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix * vPosition;\n"\

		"}";

	glShaderSource(gVertexShaderObject,
		1,
		(const GLchar**)&vertexShaderSourceCode_PV,
		NULL);

	glCompileShader(gVertexShaderObject);

	glGetShaderiv(gVertexShaderObject,
		GL_COMPILE_STATUS,
		&shaderCompileStatus);

	if (shaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject,
			GL_INFO_LOG_LENGTH,
			&infoLogLength);

		if (infoLogLength > 0)
		{
			szBuffer = (GLchar*)malloc(sizeof(GLchar) * infoLogLength);
			if (szBuffer != NULL)
			{
				glGetShaderInfoLog(gVertexShaderObject,
					infoLogLength,
					&written,
					szBuffer);
				printf( "\nERROR:Vertex Shader Object Compilation Failed log : %s", szBuffer);
				free(szBuffer);
				
			}
		}
	}

	infoLogLength = 0;
	shaderCompileStatus = 0;
	shaderProgramLinkStatus = 0;
	szBuffer = NULL;
	written = 0;

	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	const GLchar* fragmentShaderSourceCode_PV =
		"#version 460 core"\
		"\n"\
		"in vec3 out_phongADS_Light;\n"
		"out vec4 fragColor;"\
		"void main(void)"\
		"{"\
		"fragColor = vec4( out_phongADS_Light, 1.0f );"\
		"}";

	glShaderSource(gFragmentShaderObject,
		1,
		(const GLchar**)&fragmentShaderSourceCode_PV,
		NULL);

	glCompileShader(gFragmentShaderObject);

	glGetShaderiv(gFragmentShaderObject,
		GL_COMPILE_STATUS,
		&shaderCompileStatus);

	if (shaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject,
			GL_INFO_LOG_LENGTH,
			&infoLogLength);

		if (infoLogLength > 0)
		{
			szBuffer = (GLchar*)malloc(sizeof(GLchar) * infoLogLength);
			if (szBuffer != NULL)
			{
				glGetShaderInfoLog(gFragmentShaderObject,
					infoLogLength,
					&written,
					szBuffer);
				printf( "\nERROR:Fragment Shader Object Compilation Failed log : %s", szBuffer);
				free(szBuffer);
				
			}
		}
	}

	infoLogLength = 0;
	shaderCompileStatus = 0;
	shaderProgramLinkStatus = 0;
	szBuffer = NULL;
	written = 0;

	gPV_ShaderProgramObject = glCreateProgram();

	glAttachShader(gPV_ShaderProgramObject, gVertexShaderObject);
	glAttachShader(gPV_ShaderProgramObject, gFragmentShaderObject);

	//Shader Attribute Binding Before Pre Linking of attached Shader
	glBindAttribLocation(gPV_ShaderProgramObject,
		VSV_ATTRIBUTE_POSITION,
		"vPosition");
	glBindAttribLocation(gPV_ShaderProgramObject,
		VSV_ATTRIBUTE_NORMAL,
		"vNormal");

	glLinkProgram(gPV_ShaderProgramObject);

	glGetProgramiv(gPV_ShaderProgramObject,
		GL_LINK_STATUS,
		&shaderProgramLinkStatus);

	if (shaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gPV_ShaderProgramObject,
			GL_INFO_LOG_LENGTH,
			&infoLogLength);
		if (infoLogLength > 0)
		{
			szBuffer = (GLchar*)malloc(sizeof(GLchar) * infoLogLength);

			if (szBuffer != NULL)
			{
				glGetProgramInfoLog(gPV_ShaderProgramObject,
					infoLogLength,
					&written,
					szBuffer);

				printf( "\nERROR:Shader program Link failed log : %s", szBuffer);
				free(szBuffer);
				
			}
		}
	}

	//Shader Code End--------------------

	//Uniform variable Binding after post linking of attached shader 
	modelMatrixUniform_PV = glGetUniformLocation(gPV_ShaderProgramObject, "u_modelMatrix");
	viewMatrixUniform_PV = glGetUniformLocation(gPV_ShaderProgramObject, "u_viewMatrix");
	projectionlMatrixUniform_PV = glGetUniformLocation(gPV_ShaderProgramObject, "u_projectionMatrix");


	lightAmbientUniform_PV[0] = glGetUniformLocation(gPV_ShaderProgramObject, "u_lightAmbient[0]");
	lightDiffuseUniform_PV[0] = glGetUniformLocation(gPV_ShaderProgramObject, "u_lightDiffuse[0]");
	lightSpecularUniform_PV[0] = glGetUniformLocation(gPV_ShaderProgramObject, "u_lightSpecular[0]");
	lightPositionUniform_PV[0] = glGetUniformLocation(gPV_ShaderProgramObject, "u_lightPosition[0]");

	lightAmbientUniform_PV[1] = glGetUniformLocation(gPV_ShaderProgramObject, "u_lightAmbient[1]");
	lightDiffuseUniform_PV[1] = glGetUniformLocation(gPV_ShaderProgramObject, "u_lightDiffuse[1]");
	lightSpecularUniform_PV[1] = glGetUniformLocation(gPV_ShaderProgramObject, "u_lightSpecular[1]");
	lightPositionUniform_PV[1] = glGetUniformLocation(gPV_ShaderProgramObject, "u_lightPosition[1]");

	lightAmbientUniform_PV[2] = glGetUniformLocation(gPV_ShaderProgramObject, "u_lightAmbient[2]");
	lightDiffuseUniform_PV[2] = glGetUniformLocation(gPV_ShaderProgramObject, "u_lightDiffuse[2]");
	lightSpecularUniform_PV[2] = glGetUniformLocation(gPV_ShaderProgramObject, "u_lightSpecular[2]");
	lightPositionUniform_PV[2] = glGetUniformLocation(gPV_ShaderProgramObject, "u_lightPosition[2]");

	materialAmbientUniform_PV = glGetUniformLocation(gPV_ShaderProgramObject, "u_materialAmbient");
	materialDiffuseUniform_PV = glGetUniformLocation(gPV_ShaderProgramObject, "u_materialDiffuse");
	materialSpecularUniform_PV = glGetUniformLocation(gPV_ShaderProgramObject, "u_materialSpecular");
	materialShininessUniform_PV = glGetUniformLocation(gPV_ShaderProgramObject, "u_materialShininess");

	LKeyPressedUniform_PV = glGetUniformLocation(gPV_ShaderProgramObject, "u_lKeyPressed");



	//###########################################################################################
	//###########################################################################################

	//Per Fragment Shader Code Start--------------------
		gVertexShaderObject=glCreateShader(GL_VERTEX_SHADER);

		const GLchar *vertexShaderSourceCode_PF=
		"#version 460 core\n"\
		"\n"\
		"in vec4 vPosition;\n"\
		"in vec3 vNormal;\n"\

		"uniform mat4 u_modelMatrix;\n"\
		"uniform mat4 u_viewMatrix;\n"\
		"uniform mat4 u_projectionMatrix;\n"\

		"uniform vec4 u_lightPosition[3];\n"\
		
		"uniform int u_lKeyPressed;\n"\
		
		"out vec3 out_phongADS_Light;\n"\
		
		"out vec3 out_transformedNormal;\n"\
		"out vec3 out_lightDirection[3];\n"\
		"out vec3 out_viewVector;\n"\

		"void main(void)\n"\

		"{\n"\
			"if (u_lKeyPressed == 1)\n"\
			"{\n"\

				"vec4 eyeCoord = u_viewMatrix * u_modelMatrix * vPosition;\n"\

				"out_viewVector = -eyeCoord.xyz;\n"\

				"out_transformedNormal = mat3(u_viewMatrix * u_modelMatrix) * vNormal ;\n"\

				"for (int i = 0; i < 3; i++)\n"\
				"{\n"\
					"out_lightDirection[i] =  vec3( u_lightPosition[i] - eyeCoord ) ;\n"\
				"}\n"\

			"}\n"\

			"gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix * vPosition;\n"\
		
		"}";

		glShaderSource(gVertexShaderObject,
						1,
						(const GLchar**)&vertexShaderSourceCode_PF,
						NULL);
		
		glCompileShader(gVertexShaderObject);

		glGetShaderiv(gVertexShaderObject,
						GL_COMPILE_STATUS,
						&shaderCompileStatus);

		if(shaderCompileStatus==GL_FALSE)
		{
			glGetShaderiv(gVertexShaderObject,
						GL_INFO_LOG_LENGTH,
						&infoLogLength);
			
			if(infoLogLength > 0)
			{
				szBuffer=(GLchar*)malloc(sizeof(GLchar)*infoLogLength);
				if(szBuffer!=NULL)
				{
					glGetShaderInfoLog(gVertexShaderObject,
										infoLogLength,
										&written,
										szBuffer);
					printf("\nERROR:Vertex Shader Object Compilation Failed log : %s",szBuffer);
					free(szBuffer);
					
				}
			}
		}

		infoLogLength=0;
		shaderCompileStatus=0;
		shaderProgramLinkStatus=0;
		szBuffer=NULL;
		written=0;

		gFragmentShaderObject=glCreateShader(GL_FRAGMENT_SHADER);

		const GLchar *fragmentShaderSourceCode_PF=
		"#version 460 core\n"\
		"\n"\
		"vec3 phongADS_Light;\n"
		"out vec4 fragColor;\n"\

		"in vec3 out_transformedNormal;\n"\
		"in vec3 out_lightDirection[3];\n"\
		"in vec3 out_viewVector;\n"\

		"uniform vec3 u_lightAmbient[3];\n"\
		"uniform vec3 u_lightDiffuse[3];\n"\
		"uniform vec3 u_lightSpecular[3];\n"\

		"uniform vec3 u_materialAmbient;\n"\
		"uniform vec3 u_materialDiffuse;\n"\
		"uniform vec3 u_materialSpecular;\n"\
		"uniform float u_materialShininess;\n"\

		"uniform int u_lKeyPressed;\n"\

		"void main(void)\n"\
		"{"\
			"if (u_lKeyPressed == 1)\n"\
			"{\n"\

				"vec3 normalizedTransformedNormal = normalize( out_transformedNormal );\n"\

				"vec3 normalizedLightDirection[3];\n"\

				"for (int i = 0; i < 3; i++)\n"\
				"{\n"\
					" normalizedLightDirection[i] = normalize( out_lightDirection[i] );\n"\
				"}\n"\
			
				"vec3 normalizedViewVector = normalize( out_viewVector );\n"\

				"vec3 reflectionVector[3];\n"\
				"vec3 ambient[3];\n"\
				"vec3 diffuse[3];\n"\
				"vec3 specular[3];\n"\

				"for (int i = 0; i < 3; i++)\n"\
				"{\n"\
				
					"reflectionVector[i] = reflect( -normalizedLightDirection[i], normalizedTransformedNormal );\n"\

					"ambient[i]= u_lightAmbient[i] * u_materialAmbient;\n"\

					"diffuse[i] = u_lightDiffuse[i] * u_materialDiffuse * max( dot(normalizedLightDirection[i] , normalizedTransformedNormal), 0.0);\n"\

					"specular[i] = u_lightSpecular[i] * u_materialSpecular * pow( max( dot(reflectionVector[i] , normalizedViewVector), 0.0f) , u_materialShininess);\n"\

					"phongADS_Light += ambient[i] + diffuse[i] + specular[i];\n"\

				"}\n"\

			"}\n"\
			"else\n"\
			"{\n"\
				"phongADS_Light=vec3(1.0f,1.0f,1.0f);\n"\
			"}\n"\

			"fragColor = vec4( phongADS_Light, 1.0f );"\

		"}";

		glShaderSource(gFragmentShaderObject,
						1,
						(const GLchar**)&fragmentShaderSourceCode_PF,
						NULL);
		
		glCompileShader(gFragmentShaderObject);

		glGetShaderiv(gFragmentShaderObject,
					GL_COMPILE_STATUS,
					&shaderCompileStatus);

		if(shaderCompileStatus==GL_FALSE)
		{
			glGetShaderiv(gFragmentShaderObject,
						GL_INFO_LOG_LENGTH,
						&infoLogLength);
			
			if(infoLogLength > 0)
			{
				szBuffer=(GLchar*)malloc(sizeof(GLchar)*infoLogLength);
				if(szBuffer!=NULL)
				{
					glGetShaderInfoLog(gFragmentShaderObject,
										infoLogLength,
										&written,
										szBuffer);
					printf("\nERROR:Fragment Shader Object Compilation Failed log : %s",szBuffer);
					free(szBuffer);
					
				}
			}
		}

		infoLogLength=0;
		shaderCompileStatus=0;
		shaderProgramLinkStatus=0;
		szBuffer=NULL;
		written=0;

		gPF_ShaderProgramObject =glCreateProgram();

		glAttachShader(gPF_ShaderProgramObject,gVertexShaderObject);
		glAttachShader(gPF_ShaderProgramObject,gFragmentShaderObject);

		//Shader Attribute Binding Before Pre Linking of attached Shader
			glBindAttribLocation(gPF_ShaderProgramObject,
								VSV_ATTRIBUTE_POSITION,
								"vPosition");
			glBindAttribLocation(gPF_ShaderProgramObject,
								VSV_ATTRIBUTE_NORMAL,
								"vNormal");

		glLinkProgram(gPF_ShaderProgramObject);

		glGetProgramiv(gPF_ShaderProgramObject,
						GL_LINK_STATUS,
						&shaderProgramLinkStatus);
		
		if(shaderProgramLinkStatus==GL_FALSE)
		{
			glGetProgramiv(gPF_ShaderProgramObject,
							GL_INFO_LOG_LENGTH,
							&infoLogLength);
			if(infoLogLength > 0)
			{
				szBuffer=(GLchar *)malloc(sizeof(GLchar)*infoLogLength);

				if(szBuffer!=NULL)
				{
					glGetProgramInfoLog(gPF_ShaderProgramObject,
										infoLogLength,
										&written,
										szBuffer);

					printf("\nERROR:Shader program Link failed log : %s",szBuffer);
					free(szBuffer);
					
				}
			}
		}

	//Shader Code End--------------------

	
	//Uniform variable Binding after post linking of attached shader 
	modelMatrixUniform_PF = glGetUniformLocation(gPF_ShaderProgramObject, "u_modelMatrix");
	viewMatrixUniform_PF = glGetUniformLocation(gPF_ShaderProgramObject, "u_viewMatrix");
	projectionlMatrixUniform_PF = glGetUniformLocation(gPF_ShaderProgramObject, "u_projectionMatrix");

	
	lightAmbientUniform_PF[0] = glGetUniformLocation(gPF_ShaderProgramObject, "u_lightAmbient[0]");
	lightDiffuseUniform_PF[0] = glGetUniformLocation(gPF_ShaderProgramObject, "u_lightDiffuse[0]");
	lightSpecularUniform_PF[0] = glGetUniformLocation(gPF_ShaderProgramObject, "u_lightSpecular[0]");
	lightPositionUniform_PF[0] = glGetUniformLocation(gPF_ShaderProgramObject, "u_lightPosition[0]");
	
	lightAmbientUniform_PF[1] = glGetUniformLocation(gPF_ShaderProgramObject, "u_lightAmbient[1]");
	lightDiffuseUniform_PF[1] = glGetUniformLocation(gPF_ShaderProgramObject, "u_lightDiffuse[1]");
	lightSpecularUniform_PF[1] = glGetUniformLocation(gPF_ShaderProgramObject, "u_lightSpecular[1]");
	lightPositionUniform_PF[1] = glGetUniformLocation(gPF_ShaderProgramObject, "u_lightPosition[1]");

	lightAmbientUniform_PF[2] = glGetUniformLocation(gPF_ShaderProgramObject, "u_lightAmbient[2]");
	lightDiffuseUniform_PF[2] = glGetUniformLocation(gPF_ShaderProgramObject, "u_lightDiffuse[2]");
	lightSpecularUniform_PF[2] = glGetUniformLocation(gPF_ShaderProgramObject, "u_lightSpecular[2]");
	lightPositionUniform_PF[2] = glGetUniformLocation(gPF_ShaderProgramObject, "u_lightPosition[2]");

	materialAmbientUniform_PF = glGetUniformLocation(gPF_ShaderProgramObject, "u_materialAmbient");
	materialDiffuseUniform_PF = glGetUniformLocation(gPF_ShaderProgramObject, "u_materialDiffuse");
	materialSpecularUniform_PF = glGetUniformLocation(gPF_ShaderProgramObject, "u_materialSpecular");
	materialShininessUniform_PF = glGetUniformLocation(gPF_ShaderProgramObject, "u_materialShininess");

	LKeyPressedUniform_PF = glGetUniformLocation(gPF_ShaderProgramObject, "u_lKeyPressed");

	//----------------Data prepration-------------------

	getSphereVertexData(spherePosition, sphereNormals, sphereTexcoord, sphereElement);
	gNumSphereVertices = getNumberOfSphereVertices();
	gNumSphereElements = getNumberOfSphereElements();


	glGenVertexArrays(1,
					&vao_sphere);

	glBindVertexArray(vao_sphere);

		glGenBuffers(1,
					&vbo_position);
		
		glBindBuffer(GL_ARRAY_BUFFER,
					vbo_position);
		
			glBufferData(GL_ARRAY_BUFFER,
						sizeof(spherePosition),
						spherePosition,
						GL_STATIC_DRAW);

			glVertexAttribPointer(VSV_ATTRIBUTE_POSITION,
								3,
								GL_FLOAT,
								GL_FALSE,
								0,
								NULL);
			
			glEnableVertexAttribArray(VSV_ATTRIBUTE_POSITION);

		glBindBuffer(GL_ARRAY_BUFFER,
					0);

		glGenBuffers(1,
						&vbo_normal);

		glBindBuffer(GL_ARRAY_BUFFER,
						vbo_normal);

				glBufferData(GL_ARRAY_BUFFER,
								sizeof(sphereNormals),
								sphereNormals,
								GL_STATIC_DRAW);

				glVertexAttribPointer(VSV_ATTRIBUTE_NORMAL,
					3,
					GL_FLOAT,
					GL_FALSE,
					0,
					NULL);

				glEnableVertexAttribArray(VSV_ATTRIBUTE_NORMAL);

		glBindBuffer(GL_ARRAY_BUFFER,
				0);


		glGenBuffers(1,
				&vbo_element);

			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,
				vbo_element);

				glBufferData(GL_ELEMENT_ARRAY_BUFFER,
					sizeof(sphereElement),
					sphereElement,
					GL_STATIC_DRAW);

			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,
				0);

	glBindVertexArray(0);

	
	perspectiveProjectionMatrix=mat4::identity();

	glClearColor(0.0f,0.0f,0.0f,0.0f);

	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	Resize(giWindowWidth,giWindowHeight);
}

void Resize(int width,int height)
{
	if(height==0)
	{
		height=1;
	}

	glViewport( 0,
				0,
				(GLsizei)width,
				(GLsizei)height);

	perspectiveProjectionMatrix=vmath::perspective(45.0f,
												(GLfloat)width/(GLfloat)height,
												0.1f,
												100.0f);

}

GLfloat Angle = 0.0f;

GLfloat radius = 3.0f;
void Draw(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		mat4 translateMatrix;
		mat4 modelMatrix ;
		mat4 viewMatrix = mat4::identity();

		translateMatrix = vmath::translate(0.0f,0.0f,-1.5f);
		modelMatrix = translateMatrix;

	if (bTogglePVxOrPF)
	{
		glUseProgram(gPF_ShaderProgramObject);
		mat4 translateMatrix;
		mat4 rotateMatrix;

		mat4 modelMatrix;
		mat4 viewMatrix = mat4::identity();

		translateMatrix = vmath::translate(0.0f, 0.0f, -1.5f);
		modelMatrix = translateMatrix ;

		glUniformMatrix4fv(modelMatrixUniform_PF,
			1,
			GL_FALSE,
			modelMatrix);

		glUniformMatrix4fv(viewMatrixUniform_PF,
			1,
			GL_FALSE,
			viewMatrix);

		glUniformMatrix4fv(projectionlMatrixUniform_PF,
			1,
			GL_FALSE,
			perspectiveProjectionMatrix);

		if (bLights == true)
		{
			glUniform1i(LKeyPressedUniform_PF, 1);

			for (int i = 0; i < 3; i++)
			{
				glUniform3fv(lightAmbientUniform_PF[i], 1, light[i].LightAmbient);
				glUniform3fv(lightDiffuseUniform_PF[i], 1, light[i].LightDefuse);
				glUniform3fv(lightSpecularUniform_PF[i], 1, light[i].LightSpecular);

				if (i == 0){
					light[i].LightPosition = vec4(0.0f, radius * sin(Angle), radius * cos(Angle), 1.0f);
				}
				else if (i == 1){
					light[i].LightPosition = vec4(radius * sin(Angle), 0.0f, radius * cos(Angle), 1.0f);
				}
				else if (i == 2){
					light[i].LightPosition = vec4(radius * sin(Angle), radius * cos(Angle), 0.0f, 1.0f);
				}

				glUniform4fv(lightPositionUniform_PF[i], 1, light[i].LightPosition);//posiitional light
			}
			
			glUniform3fv(materialAmbientUniform_PF, 1, materialAmbient);
			glUniform3fv(materialDiffuseUniform_PF, 1, materilaDefuse);
			glUniform3fv(materialSpecularUniform_PF, 1, materialSpecular);

			glUniform1f(materialShininessUniform_PF, materialShininess);
		}
		else
		{
			glUniform1i(LKeyPressedUniform_PF, 0);
		}

	}
	else {
		glUseProgram(gPV_ShaderProgramObject);
		mat4 translateMatrix;
		mat4 rotateMatrix;

		mat4 modelMatrix;
		mat4 viewMatrix = mat4::identity();

		translateMatrix = vmath::translate(0.0f, 0.0f, -1.5f);
		modelMatrix = translateMatrix;

		glUniformMatrix4fv(modelMatrixUniform_PV,
			1,
			GL_FALSE,
			modelMatrix);

		glUniformMatrix4fv(viewMatrixUniform_PV,
			1,
			GL_FALSE,
			viewMatrix);

		glUniformMatrix4fv(projectionlMatrixUniform_PV,
			1,
			GL_FALSE,
			perspectiveProjectionMatrix);

		if (bLights == true)
		{
			glUniform1i(LKeyPressedUniform_PV, 1);

			for (int i = 0; i < 3; i++)
			{
				glUniform3fv(lightAmbientUniform_PV[i], 1, light[i].LightAmbient);
				glUniform3fv(lightDiffuseUniform_PV[i], 1, light[i].LightDefuse);
				glUniform3fv(lightSpecularUniform_PV[i], 1, light[i].LightSpecular);

				if (i == 0) {
					light[i].LightPosition = vec4(0.0f, radius * sin(Angle), radius * cos(Angle), 1.0f);
				}
				else if (i == 1) {
					light[i].LightPosition = vec4(radius * sin(Angle), 0.0f, radius * cos(Angle), 1.0f);
				}
				else if (i == 2) {
					light[i].LightPosition = vec4(radius * sin(Angle), radius * cos(Angle), 0.0f, 1.0f);
				}

				glUniform4fv(lightPositionUniform_PV[i], 1, light[i].LightPosition);//posiitional light
			}

			glUniform3fv(materialAmbientUniform_PV, 1, materialAmbient);
			glUniform3fv(materialDiffuseUniform_PV, 1, materilaDefuse);
			glUniform3fv(materialSpecularUniform_PV, 1, materialSpecular);

			glUniform1f(materialShininessUniform_PV, materialShininess);
		}
		else
		{
			glUniform1i(LKeyPressedUniform_PV, 0);
		}
	}


		glBindVertexArray(vao_sphere);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_element);
				glDrawElements(GL_TRIANGLES, gNumSphereElements, GL_UNSIGNED_SHORT, 0);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		glBindVertexArray(0);

	glUseProgram(0);



	glXSwapBuffers(gpDisplay,
					gWindow);

	Angle += 0.05f;
}

void UnInitialize(void)
{
	if(vao_sphere)
	{
		glDeleteVertexArrays(1,
							&vao_sphere);
	}
	vao_sphere=0;

	
	if(vbo_position)
	{
		glDeleteBuffers(1,
						&vbo_position);

		vbo_position=0;
	}

	if(vbo_normal)
	{
		glDeleteBuffers(1,
						&vbo_normal);

		vbo_normal=0;
	}

	if(vbo_element)
	{
		glDeleteBuffers(1,
						&vbo_element);

		vbo_element=0;
	}
if(gPF_ShaderProgramObject)
	{
		glUseProgram(gPF_ShaderProgramObject);

			GLsizei shaderCount;
			GLsizei tempShaderCount;
			GLuint *pShader=NULL;

			glGetProgramiv(gPF_ShaderProgramObject,
							GL_ATTACHED_SHADERS,
							&shaderCount);

			pShader=(GLuint *)malloc(sizeof(GLuint)*shaderCount);
			if(pShader==NULL)
			{
				printf("\nFailed to allocate memory for pShader");
			}else{
				glGetAttachedShaders(gPF_ShaderProgramObject,
									shaderCount,
									&tempShaderCount,
									pShader);
				
				printf("\nShader program has actual attached shader count is:%d",tempShaderCount);
				
				for(GLsizei i=0;i<shaderCount;i++)
				{
					glDetachShader(gPF_ShaderProgramObject,
									pShader[i]);

					glDeleteShader(pShader[i]);
					
					pShader[i]=0;
				}
				free(pShader);
				
				glDeleteProgram(gPF_ShaderProgramObject);
				
				gPF_ShaderProgramObject = 0;
			}
		glUseProgram(0);
	}

	if (gPV_ShaderProgramObject)
	{
		glUseProgram(gPV_ShaderProgramObject);

		GLsizei shaderCount;
		GLsizei tempShaderCount;
		GLuint* pShader = NULL;

		glGetProgramiv(gPV_ShaderProgramObject,
			GL_ATTACHED_SHADERS,
			&shaderCount);

		pShader = (GLuint*)malloc(sizeof(GLuint) * shaderCount);
		if (pShader == NULL)
		{
			printf( "\nFailed to allocate memory for pShader");
		}
		else {
			glGetAttachedShaders(gPV_ShaderProgramObject,
				shaderCount,
				&tempShaderCount,
				pShader);

			printf( "\nShader program has actual attached shader count is:%d", tempShaderCount);

			for (GLsizei i = 0; i < shaderCount; i++)
			{
				glDetachShader(gPV_ShaderProgramObject,
					pShader[i]);

				glDeleteShader(pShader[i]);

				pShader[i] = 0;
			}
			free(pShader);

			glDeleteProgram(gPV_ShaderProgramObject);

			gPV_ShaderProgramObject = 0;
		}
		glUseProgram(0);
	}

	GLXContext currentGLXContext;

	currentGLXContext=glXGetCurrentContext();

	if(currentGLXContext==gGLXContext)
	{
		glXMakeCurrent(gpDisplay,
						0,
						0);
	}

	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay,
							gGLXContext);
	}

	gGLXContext=NULL;
	currentGLXContext=NULL;

	if(gWindow)
	{
		XDestroyWindow(gpDisplay,
						gWindow);
	}

	if(gColormap)
	{
		XFreeColormap(gpDisplay,
						gColormap);
	}

	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
	}
		gpXVisualInfo=NULL;

	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
	}
		gpDisplay=NULL;
}