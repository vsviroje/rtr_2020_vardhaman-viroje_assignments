#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<memory.h>

#include<GL/glew.h>
#include<GL/glx.h>
#include<GL/gl.h>

#include"vmath.h"

#include<X11/Xlib.h>
#include<X11/Xutil.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>

using namespace std;
using namespace vmath;

bool bFullScreen=false;
Display *gpDisplay=NULL;
XVisualInfo *gpXVisualInfo=NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth=800;
int giWindowHeight=600;

GLXContext gGLXContext;

//PP Changes
typedef GLXContext (*glxCreateContextAttribsARBProc)(Display *, GLXFBConfig, GLXContext, Bool, const int *);
glxCreateContextAttribsARBProc glxCreateContextAttribsARB=NULL;

GLXFBConfig gGLXFBConfig;

//Shader Changes


enum{
	VSV_ATTRIBUTE_POSITION,
	VSV_ATTRIBUTE_COLOR,
	VSV_ATTRIBUTE_TEXCOORD,
	VSV_ATTRIBUTE_NORMAL,
};

GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

GLuint vao_triangle;
GLuint vao_square;
GLuint vao_verticalLine;
GLuint vao_horizontalLine;
GLuint vao_circle;


GLuint vbo_positionTriangle;
GLuint vbo_colorTriangle;

GLuint vbo_positionSquare;
GLuint vbo_colorSquare;

GLuint vbo_position;
GLuint vbo_colorVerticalLine;
GLuint vbo_colorHorizontalLine;
GLuint vbo_colorCircle;


GLuint mvpMatrixUniform;
mat4 perspectiveProjectionMatrix;

//------
#define XYZ 3
#define CIRCLE_FlOAT_MEMBER_COUNT 360
#define CIRCLE_POINT_COUNT ((CIRCLE_FlOAT_MEMBER_COUNT+1) * XYZ) 

GLfloat circle_Points[CIRCLE_POINT_COUNT];

GLfloat blueColor[] =
{
	0.0f, 0.0f, 1.0f,
	0.0f, 0.0f, 1.0f
};

GLfloat greenColor[] =
{
	0.0f, 1.0f, 0.0f,
	0.0f, 1.0f, 0.0f
};

GLfloat redColor[] =
{
	1.0f, 0.0f, 0.0f,
	1.0f, 0.0f, 0.0f
};

int main(void)
{
	void CreateWindow(void);
	void ToggleFullscreen(void);
	void UnInitialize(void);

	void Initialize(void);
	void Resize(int,int);
	void Draw(void);

	bool bDone=false;

	int winWidth=giWindowWidth;
	int winHeight=giWindowHeight;

	CreateWindow();
	Initialize();

	XEvent event;
	KeySym keysym;

	while(bDone==false)
	{
		while(XPending(gpDisplay))
		{
			XNextEvent(gpDisplay,
						&event);

			switch(event.type)
			{
				case MapNotify:
					break;
				case KeyPress:
					keysym=XkbKeycodeToKeysym(gpDisplay,
												event.xkey.keycode,
												0,
												0);
					switch(keysym)
					{
						case XK_Escape:
							bDone=true;
							break;

						case XK_F:
						case XK_f:
							if(bFullScreen==false)
							{
								ToggleFullscreen();
								bFullScreen=true;
							}else
							{
								ToggleFullscreen();
								bFullScreen=false;
							}
							break;
						default:
							break;
					}
					break;

				case MotionNotify:
					break;
				case ConfigureNotify:
					winWidth=event.xconfigure.width;
					winHeight=event.xconfigure.height;
					Resize(winWidth,winHeight);
					break;

				case Expose:
					break;
				case 33:
					bDone=true;
					break;
				default:
					break;
			}

		}
		Draw();
	}
	UnInitialize();
	return(0);
}

void CreateWindow(void)
{
	void UnInitialize(void);
	XSetWindowAttributes winAttribs;
	int defaultScreen;
	int styleMask;

	//PP Changes
	GLXFBConfig *pGLXFBConfig=NULL;
	GLXFBConfig bestGLXFBConfig;
	XVisualInfo *pTempXVisualInfo=NULL;
	int numFBConfigs=0;

	static int FrameBufferAttributes[]={
										GLX_X_RENDERABLE,True,
										GLX_DRAWABLE_TYPE,GLX_WINDOW_BIT,
										GLX_RENDER_TYPE,GLX_RGBA_BIT,
										GLX_X_VISUAL_TYPE,GLX_TRUE_COLOR,

										GLX_RED_SIZE,8,
										GLX_GREEN_SIZE,8,
										GLX_BLUE_SIZE,8,
										GLX_ALPHA_SIZE,8,
										
										GLX_STENCIL_SIZE,8,
										
										GLX_DOUBLEBUFFER,True,
										GLX_DEPTH_SIZE,24,
										None};	

	gpDisplay=XOpenDisplay(NULL);
	if(gpDisplay==NULL)
	{
		printf("\nERROR:Unable to Open X Display\n");
		UnInitialize();
		exit(1);
	}

	defaultScreen=XDefaultScreen(gpDisplay);
	
	//PP Changes
	pGLXFBConfig = glXChooseFBConfig(gpDisplay,
									defaultScreen,
									FrameBufferAttributes,
									&numFBConfigs);
	if(numFBConfigs == 0)
	{
		printf("\nERROR:%d-Number of Frame Buffer Config.So no need to proceed futher.\n",numFBConfigs);
		UnInitialize();
		exit(1);
	}

	printf("\nINFO:%d-Found Number of Frame Buffer Config.\n",numFBConfigs);


	int bestFrameBufferConfig=-1;
	int worstFrameBufferConfig=-1;
	int bestNumberOfSamples=-1;
	int worstNumberOfSamples=999;

	for(int i=0; i < numFBConfigs; i++)
	{
		pTempXVisualInfo=glXGetVisualFromFBConfig(gpDisplay,
												pGLXFBConfig[i]);

		if(pTempXVisualInfo != NULL)
		{
			int SampleBuffers,Samples;

			glXGetFBConfigAttrib(gpDisplay,
								pGLXFBConfig[i],
								GLX_SAMPLE_BUFFERS,
								&SampleBuffers);

			glXGetFBConfigAttrib(gpDisplay,
								pGLXFBConfig[i],
								GLX_SAMPLES,
								&Samples);

			if(bestFrameBufferConfig < 0 || SampleBuffers && Samples > bestNumberOfSamples )
			{
				bestFrameBufferConfig = i;
				bestNumberOfSamples = Samples;
			}

			if(worstFrameBufferConfig < 0 || !SampleBuffers || Samples < worstNumberOfSamples)
			{
				worstFrameBufferConfig = i;
				worstNumberOfSamples = Samples;
			}

			printf("\nINFO:When i = %d, Samples = %d, SampleBuffers = %d, pTempXVisualInfo->VisualID = %ld \n",i, Samples, SampleBuffers, pTempXVisualInfo->visualid);
		}

		XFree(pTempXVisualInfo);
		pTempXVisualInfo = NULL;
	}
	
	printf("\nINFO:bestFrameBufferConfig index = %d \n",bestFrameBufferConfig);

	bestGLXFBConfig = pGLXFBConfig[bestFrameBufferConfig];

	gGLXFBConfig = bestGLXFBConfig;

	XFree(pGLXFBConfig);
	pGLXFBConfig = NULL;

	gpXVisualInfo=(XVisualInfo *)malloc(sizeof(XVisualInfo));
	if(gpXVisualInfo==NULL)
	{
		printf("\nERROR:Unable to allocate visual info\n");
		UnInitialize();
		exit(1);
	}

	gpXVisualInfo = glXGetVisualFromFBConfig(gpDisplay,
											bestGLXFBConfig);

	winAttribs.border_pixel=0;
	winAttribs.background_pixmap=0;
	winAttribs.colormap=XCreateColormap(gpDisplay,
										RootWindow(gpDisplay,
													gpXVisualInfo->screen),
										gpXVisualInfo->visual,
										AllocNone);

	gColormap=winAttribs.colormap;

	winAttribs.background_pixel=BlackPixel(gpDisplay,
											defaultScreen);

	winAttribs.event_mask=ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;

	styleMask=CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

	gWindow=XCreateWindow(	gpDisplay,
							RootWindow(gpDisplay,
										gpXVisualInfo->screen),
							0,
							0,
							giWindowWidth,
							giWindowHeight,
							0,
							gpXVisualInfo->depth,
							InputOutput,
							gpXVisualInfo->visual,
							styleMask,
							&winAttribs);

	if(!gWindow)
	{
		printf("\nERROR:Failed to Create main window\n");
		UnInitialize();
		exit(1);
	}

	XStoreName(gpDisplay,
				gWindow,
				"XWindow-PP Graph");

	Atom windowManagerDelete=XInternAtom(gpDisplay,
										"WM_DELETE_WINDOW",
										True);

	XSetWMProtocols(gpDisplay,
					gWindow,
					&windowManagerDelete,
					1);

	XMapWindow(gpDisplay,
				gWindow);

}

void ToggleFullscreen(void)
{
	Atom wm_state;
	Atom fullscreen;
	XEvent xev={0};

	wm_state=XInternAtom(gpDisplay,
						"_NET_WM_STATE",
						False);

	memset(&xev,
			0,
			sizeof(xev));

	xev.type=ClientMessage;
	xev.xclient.window=gWindow;
	xev.xclient.message_type=wm_state;
	xev.xclient.format=32;
	xev.xclient.data.l[0]=bFullScreen?0:1;

	fullscreen=XInternAtom(gpDisplay,
							"_NET_WM_STATE_FULLSCREEN",
							False);

	xev.xclient.data.l[1]=fullscreen;

	XSendEvent(gpDisplay,
				RootWindow(gpDisplay,
							gpXVisualInfo->screen),
				False,
				StructureNotifyMask,
				&xev);
}

void Initialize(void)
{

	void Resize(int,int);
	void UnInitialize(void);
	void DrawCircle(void);

	glxCreateContextAttribsARB = (glxCreateContextAttribsARBProc)glXGetProcAddressARB((GLubyte *)"glXCreateContextAttribsARB");

	const int attribs[] = { GLX_CONTEXT_MAJOR_VERSION_ARB, 4,
							GLX_CONTEXT_MINOR_VERSION_ARB, 5,
							GLX_CONTEXT_PROFILE_MASK_ARB, GLX_CONTEXT_CORE_PROFILE_BIT_ARB,
							None};

	gGLXContext = glxCreateContextAttribsARB(gpDisplay,
											gGLXFBConfig,
											0,
											True,
											attribs);

	if(!gGLXContext)
	{
		const int lowestAttribs[] = { 	GLX_CONTEXT_MAJOR_VERSION_ARB, 1,
										GLX_CONTEXT_MINOR_VERSION_ARB, 0,
										None};

		gGLXContext = glxCreateContextAttribsARB(gpDisplay,
											gGLXFBConfig,
											0,
											True,
											lowestAttribs);

			printf("\nINFO:Got 1.0 OpenGL Context Version\n");

	}else{
			printf("\nINFO:Got 4.5 OpenGL Context Version\n");
	}



	if(gGLXContext==NULL)
	{
		printf("\nERROR:Unable to create Rendering Context\n");
		UnInitialize();
		exit(1);
	}

	Bool isDirectContext = glXIsDirect(gpDisplay,
										gGLXContext);

	if (isDirectContext == True)
	{
		printf("\nINFO:Rendering Context is direct Hardware rendering context.\n");
	}else{
		printf("\nINFO:Rendering Context is Software rendering context.\n");
	}

	glXMakeCurrent(gpDisplay,
					gWindow,
					gGLXContext);

	GLenum glew_error=glewInit();
	if(glew_error != GLEW_OK)
	{
		printf("\nglewInit failed to enable extension for Programmable Pipeline");
		UnInitialize();
		exit(1);
	}

	printf( "\nOpenGL Information are availble below:-");
	printf( "\n*****************************************************");

	printf( "\nOpenGL Vender:%s",glGetString(GL_VENDOR));
	printf( "\nOpenGL Renderer:%s",glGetString(GL_RENDERER));
	printf( "\nOpenGL Version:%s",glGetString(GL_VERSION));
	printf( "\nOpenGL GLSL Version:%s",glGetString(GL_SHADING_LANGUAGE_VERSION));
	
	printf("\nOpenGL Extension's list:\n");
	GLint numExt;
	glGetIntegerv(GL_NUM_EXTENSIONS,&numExt);

	for(int i=0;i<numExt;i++)
	{
		printf( "\n%s",glGetStringi(GL_EXTENSIONS,i));
	}
	printf( "\n*****************************************************\n");
	
	DrawCircle();
	GLint infoLogLength;
	GLint shaderCompileStatus;
	GLint shaderProgramLinkStatus;
	GLchar *szBuffer;
	GLsizei written;

	//Shader Code Start--------------------
		gVertexShaderObject=glCreateShader(GL_VERTEX_SHADER);

		const GLchar* vertexShaderSourceCode=
		"#version 460 core"\
		"\n"\
		"in vec4 vPosition;"\
		"in vec4 vColor;"\
		"out vec4 out_color;"\
		"uniform mat4 u_mvpMatrix;"\
		"void main(void)"\
		"{"\
			"gl_Position=u_mvpMatrix * vPosition;"\
			"out_color=vColor;"\
		"}";

		glShaderSource(gVertexShaderObject,
						1,
						(const GLchar **)&vertexShaderSourceCode,
						NULL);

		glCompileShader(gVertexShaderObject);

		glGetShaderiv(gVertexShaderObject,
						GL_COMPILE_STATUS,
						&shaderCompileStatus);
		if(shaderCompileStatus==GL_FALSE)
		{
			glGetShaderiv(gVertexShaderObject,
						GL_INFO_LOG_LENGTH,
						&infoLogLength);
			
			if(infoLogLength>0)
			{
				szBuffer=(GLchar *)malloc(sizeof(GLchar) * infoLogLength);
				if(szBuffer!=NULL)
				{
					glGetShaderInfoLog(gVertexShaderObject,
										infoLogLength,
										&written,
										szBuffer);
					
					printf("\nERROR:Vertex Shader Compilation failed log : %s",szBuffer);
					free(szBuffer);
					UnInitialize();
					exit(1);
				}
			}
		}

		infoLogLength=0;
		shaderCompileStatus=0;
		shaderProgramLinkStatus=0;
		szBuffer=NULL;
		written=0;

		gFragmentShaderObject=glCreateShader(GL_FRAGMENT_SHADER);

		const GLchar * fragmentShaderSourceCode=
		"#version 460 core"\
		"\n"\
		"in vec4 out_color;"\
		"out vec4 fragColor;"\
		"void main(void)"\
		"{"\
			"fragColor=out_color;"\
		"}";

		glShaderSource(gFragmentShaderObject,
						1,
						(const GLchar**)&fragmentShaderSourceCode,
						NULL );
		
		glCompileShader(gFragmentShaderObject);

		glGetShaderiv(gFragmentShaderObject,
						GL_COMPILE_STATUS,
						&shaderCompileStatus);
		
		if(shaderCompileStatus==GL_FALSE)
		{
			glGetShaderiv(gFragmentShaderObject,
						GL_INFO_LOG_LENGTH,
						&infoLogLength);
			
			if(infoLogLength > 0)
			{
				szBuffer=(GLchar *)malloc(sizeof(GLchar) * infoLogLength);
				if(szBuffer != NULL)
				{
					glGetShaderInfoLog(gFragmentShaderObject,
										infoLogLength,
										&written,
										szBuffer);
					
					printf(
							"\nERROR Fragment Shader Compilation failed log : %s",
							szBuffer);

					free(szBuffer);
					UnInitialize();
					exit(1);	
				}
			}
		}

		infoLogLength=0;
		shaderCompileStatus=0;
		shaderProgramLinkStatus=0;
		szBuffer=NULL;
		written=0;

		gShaderProgramObject=glCreateProgram();

		glAttachShader(gShaderProgramObject,gVertexShaderObject);
		glAttachShader(gShaderProgramObject,gFragmentShaderObject);

		//Shader Attribute Binding Before Pre Linking of attached Shader
			glBindAttribLocation(gShaderProgramObject,
								VSV_ATTRIBUTE_POSITION,
								"vPosition");
			glBindAttribLocation(gShaderProgramObject,
								VSV_ATTRIBUTE_COLOR,
								"vColor");
		
		glLinkProgram(gShaderProgramObject);

		glGetProgramiv(gShaderProgramObject,
					GL_LINK_STATUS,
					&shaderProgramLinkStatus);
		
		if(shaderProgramLinkStatus==GL_FALSE)
		{
			glGetProgramiv(gShaderProgramObject,
							GL_INFO_LOG_LENGTH,
							&infoLogLength);

			if(infoLogLength > 0)
			{
				szBuffer=(GLchar *)malloc(sizeof(GLchar) * infoLogLength);
				if(szBuffer!=NULL)
				{
					glGetProgramInfoLog(gShaderProgramObject,
										infoLogLength,
										&written,
										szBuffer);
					
					printf("\nERROR Shader Program Link Failed log : %s",szBuffer);

					free(szBuffer);
					UnInitialize();
					exit(1);
				}
			}	
		}

	//Shader Code End--------------------

	//Uniform variable Binding after post linking of attached shader
		mvpMatrixUniform=glGetUniformLocation(gShaderProgramObject,
											"u_mvpMatrix");

	//VAO and its respective VBO preparation
		const GLfloat triangleVertices[]=
		{
			0.0f,1.0f,0.0f,
			-1.0f,-1.0f,0.0f,
			1.0f,-1.0f,0.0f
		};

		const GLfloat verticalLineVertices[] =
		{
			0.0f,1.0f,0.0f,
			0.0f,-1.0f,0.0f
		};

		const GLfloat horizontalLineVertices[] =
		{
			1.0f,0.0f,0.0f,
			-1.0f,0.0f,0.0f
		};

		const GLfloat triangleColor[]=
		{
			1.0f, 1.0f, 1.0f,
			1.0f, 1.0f, 1.0f,
			1.0f, 1.0f, 1.0f
		};

		const GLfloat squareVertices[]=
		{
			1.0f, 1.0f, 0.0f,
			-1.0f, 1.0f, 0.0f,
			-1.0f, -1.0f, 0.0f,
			1.0f, -1.0f, 0.0f
		};

		const GLfloat squareColor[]=
		{
			1.0f, 1.0f, 1.0f,
			1.0f, 1.0f, 1.0f,
			1.0f, 1.0f, 1.0f,
			1.0f, 1.0f, 1.0f
		};

		glGenVertexArrays(1,
						&vao_triangle);

		glBindVertexArray(vao_triangle);
			glGenBuffers(1,
						&vbo_positionTriangle);

			glBindBuffer(GL_ARRAY_BUFFER,
						vbo_positionTriangle);

				glBufferData(GL_ARRAY_BUFFER,
							sizeof(triangleVertices),
							triangleVertices,
							GL_STATIC_DRAW);

				glVertexAttribPointer(VSV_ATTRIBUTE_POSITION,
									3,
									GL_FLOAT,
									GL_FALSE,
									0,
									NULL);

				glEnableVertexAttribArray(VSV_ATTRIBUTE_POSITION);
			
			glBindBuffer(GL_ARRAY_BUFFER,0);
			
			glGenBuffers(1,
						&vbo_colorTriangle);

			glBindBuffer(GL_ARRAY_BUFFER,
						vbo_colorTriangle);

				glBufferData(GL_ARRAY_BUFFER,
							sizeof(triangleColor),
							triangleColor,
							GL_STATIC_DRAW);

				glVertexAttribPointer(VSV_ATTRIBUTE_COLOR,
										3,
										GL_FLOAT,
										GL_FALSE,
										0,
										NULL);

				glEnableVertexAttribArray(VSV_ATTRIBUTE_COLOR);
				
			glBindBuffer(GL_ARRAY_BUFFER,0);
		glBindVertexArray(0);

		glGenVertexArrays(1,
						&vao_square);

		glBindVertexArray(vao_square);

			glGenBuffers(1,
						&vbo_positionSquare);

			glBindBuffer(GL_ARRAY_BUFFER,
						vbo_positionSquare);

				glBufferData(GL_ARRAY_BUFFER,
							sizeof(squareVertices),
							squareVertices,
							GL_STATIC_DRAW);

				glVertexAttribPointer(VSV_ATTRIBUTE_POSITION,
									3,
									GL_FLOAT,
									GL_FALSE,
									0,
									NULL);

				glEnableVertexAttribArray(VSV_ATTRIBUTE_POSITION);
			
			glBindBuffer(GL_ARRAY_BUFFER,0);

			glGenBuffers(1,
						&vbo_colorSquare);

			glBindBuffer(GL_ARRAY_BUFFER,
						vbo_colorSquare);
				
				glBufferData(GL_ARRAY_BUFFER,
							sizeof(squareColor),
							squareColor,
							GL_STATIC_DRAW);

				glVertexAttribPointer(VSV_ATTRIBUTE_COLOR,
										3,
										GL_FLOAT,
										GL_FALSE,
										0,
										NULL);

				glEnableVertexAttribArray(VSV_ATTRIBUTE_COLOR);

			glBindBuffer(GL_ARRAY_BUFFER,0);	
		
		glBindVertexArray(0);

		glGenVertexArrays(1,
			&vao_verticalLine);

		glBindVertexArray(vao_verticalLine);
		glGenBuffers(1,
			&vbo_position);

		glBindBuffer(GL_ARRAY_BUFFER,
			vbo_position);

		glBufferData(GL_ARRAY_BUFFER,
			sizeof(verticalLineVertices),
			verticalLineVertices,
			GL_STATIC_DRAW);

		glVertexAttribPointer(VSV_ATTRIBUTE_POSITION,
			3,
			GL_FLOAT,
			GL_FALSE,
			0,
			NULL);

		glEnableVertexAttribArray(VSV_ATTRIBUTE_POSITION);

		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glGenBuffers(1,
			&vbo_colorVerticalLine);

		glBindBuffer(GL_ARRAY_BUFFER,
			vbo_colorVerticalLine);

		glBufferData(GL_ARRAY_BUFFER,
			sizeof(GLfloat)*3*2,
			NULL,
			GL_DYNAMIC_DRAW);

		glVertexAttribPointer(VSV_ATTRIBUTE_COLOR,
			3,
			GL_FLOAT,
			GL_FALSE,
			0,
			NULL);

		glEnableVertexAttribArray(VSV_ATTRIBUTE_COLOR);

		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindVertexArray(0);


		glGenVertexArrays(1,
			&vao_horizontalLine);

		glBindVertexArray(vao_horizontalLine);
		glGenBuffers(1,
			&vbo_position);

		glBindBuffer(GL_ARRAY_BUFFER,
			vbo_position);

		glBufferData(GL_ARRAY_BUFFER,
			sizeof(horizontalLineVertices),
			horizontalLineVertices,
			GL_STATIC_DRAW);

		glVertexAttribPointer(VSV_ATTRIBUTE_POSITION,
			3,
			GL_FLOAT,
			GL_FALSE,
			0,
			NULL);

		glEnableVertexAttribArray(VSV_ATTRIBUTE_POSITION);

		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glGenBuffers(1,
			&vbo_colorHorizontalLine);

		glBindBuffer(GL_ARRAY_BUFFER,
			vbo_colorHorizontalLine);

		glBufferData(GL_ARRAY_BUFFER,
			sizeof(GLfloat) * 3 * 2,
			NULL,
			GL_DYNAMIC_DRAW);

		glVertexAttribPointer(VSV_ATTRIBUTE_COLOR,
			3,
			GL_FLOAT,
			GL_FALSE,
			0,
			NULL);

		glEnableVertexAttribArray(VSV_ATTRIBUTE_COLOR);

		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindVertexArray(0);

		glGenVertexArrays(1, &vao_circle);
		glBindVertexArray(vao_circle);

		glGenBuffers(1, &vbo_position);
		glBindBuffer(GL_ARRAY_BUFFER, vbo_position);

			glBufferData(GL_ARRAY_BUFFER,
				sizeof(circle_Points),
				circle_Points,
				GL_STATIC_DRAW);

			glVertexAttribPointer(VSV_ATTRIBUTE_POSITION,
				3,
				GL_FLOAT,
				GL_FALSE,
				0,
				NULL);

			glEnableVertexAttribArray(VSV_ATTRIBUTE_POSITION);

		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glGenBuffers(1, &vbo_colorCircle);
		glBindBuffer(GL_ARRAY_BUFFER, vbo_colorCircle);
			glVertexAttrib3f(VSV_ATTRIBUTE_COLOR, 1.0f, 1.0f, 1.0f);
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glBindVertexArray(0);
		

	//SetColor
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	//Warmup resize call

	perspectiveProjectionMatrix=mat4::identity();

	Resize(giWindowWidth,giWindowHeight);
}

void Resize(int width,int height)
{
	if(height==0)
	{
		height=1;
	}

	glViewport( 0,
				0,
				(GLsizei)width,
				(GLsizei)height);

	perspectiveProjectionMatrix=vmath::perspective(45.0f,
													(GLfloat)width/(GLfloat)height,
													0.1f,
													100.0f);

}

void Draw(void)
{
	GLfloat *lineColor;

	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
	glUseProgram(gShaderProgramObject);	
		mat4 modelViewMatrix=mat4::identity();
		mat4 modelViewProjectionMatrix=mat4::identity();
		mat4 translateMatrix;
		mat4 rotateMatrix;
		mat4 scaleMatrix;

		translateMatrix=vmath::translate(0.0f,0.0f,-8.7f);
		modelViewMatrix=translateMatrix;

		modelViewProjectionMatrix=perspectiveProjectionMatrix * modelViewMatrix;

		glUniformMatrix4fv(mvpMatrixUniform,
							1,
							GL_FALSE,
							modelViewProjectionMatrix);
		glBindVertexArray(vao_triangle);
			glDrawArrays(GL_LINE_LOOP, 0, 3);
		glBindVertexArray(0);
 		
		modelViewMatrix=mat4::identity();
		modelViewProjectionMatrix=mat4::identity();
		translateMatrix=mat4::identity();

		translateMatrix=vmath::translate(0.0f,0.0f,-8.7f);
		modelViewMatrix=translateMatrix;

		modelViewProjectionMatrix=perspectiveProjectionMatrix * modelViewMatrix;
		
		glUniformMatrix4fv(mvpMatrixUniform,
							1,
							GL_FALSE,
							modelViewProjectionMatrix);
		
		glBindVertexArray(vao_square);
			glDrawArrays(GL_LINE_LOOP,0,4);
		glBindVertexArray(0);

		modelViewMatrix = mat4::identity();
		modelViewProjectionMatrix = mat4::identity();
		translateMatrix = mat4::identity();

		translateMatrix = vmath::translate(0.0f, 0.0f, -6.0f);
		rotateMatrix = vmath::rotate(90.0f, 1.0f, 0.0f, 0.0f);
		modelViewMatrix = translateMatrix * rotateMatrix;

		modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

		glUniformMatrix4fv(mvpMatrixUniform,
			1,
			GL_FALSE,
			modelViewProjectionMatrix);

		glBindVertexArray(vao_circle);
		glDrawArrays(GL_LINE_LOOP,
			0,
			CIRCLE_POINT_COUNT);
		glBindVertexArray(0);

		modelViewMatrix = mat4::identity();
		modelViewProjectionMatrix = mat4::identity();
		translateMatrix = mat4::identity();
		scaleMatrix = mat4::identity();

		translateMatrix = vmath::translate(0.0f, -0.275f, -6.0f);
		rotateMatrix = vmath::rotate(90.0f, 1.0f, 0.0f, 0.0f);
		scaleMatrix = vmath::scale(0.4225f, 0.4225f, 0.4225f);
		modelViewMatrix = translateMatrix * scaleMatrix * rotateMatrix;

		modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

		glUniformMatrix4fv(mvpMatrixUniform,
			1,
			GL_FALSE,
			modelViewProjectionMatrix);

		glBindVertexArray(vao_circle);
		glDrawArrays(GL_LINE_LOOP,
			0,
			CIRCLE_POINT_COUNT);
		glBindVertexArray(0);

		for (GLfloat x = -1.0f; x < 1.05f; x += 0.05f)
		{

			modelViewMatrix = mat4::identity();
			modelViewProjectionMatrix = mat4::identity();
			translateMatrix = mat4::identity();

			translateMatrix = vmath::translate(x, 0.0f, -3.0f);
			modelViewMatrix = translateMatrix;

			modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

			glUniformMatrix4fv(mvpMatrixUniform,
				1,
				GL_FALSE,
				modelViewProjectionMatrix);

			glBindVertexArray(vao_verticalLine);

				if (x > -0.000000 && x < 0.050000)
				{
					lineColor = redColor;
				}
				else {
					lineColor = blueColor;
				}
				glBindBuffer(GL_ARRAY_BUFFER,
					vbo_colorVerticalLine);

				glBufferData(GL_ARRAY_BUFFER,
					sizeof(GLfloat) * 3 * 2,
					lineColor,
					GL_DYNAMIC_DRAW);

				glVertexAttribPointer(VSV_ATTRIBUTE_COLOR,
					3,
					GL_FLOAT,
					GL_FALSE,
					0,
					NULL);

				glEnableVertexAttribArray(VSV_ATTRIBUTE_COLOR);

				glBindBuffer(GL_ARRAY_BUFFER, 0);

			glDrawArrays(GL_LINES, 0, 2);
			glBindVertexArray(0);
		}

		for (GLfloat y = -1.0f; y < 1.05f; y += 0.05f)
		{
			modelViewMatrix = mat4::identity();
			modelViewProjectionMatrix = mat4::identity();
			translateMatrix = mat4::identity();

			translateMatrix = vmath::translate(0.0f, y, -3.0f);
			modelViewMatrix = translateMatrix;

			modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

			glUniformMatrix4fv(mvpMatrixUniform,
				1,
				GL_FALSE,
				modelViewProjectionMatrix);

			glBindVertexArray(vao_horizontalLine);
			if (y > -0.000000 && y < 0.050000)
			{
				lineColor = greenColor;
			}
			else {
				lineColor = blueColor;
			}
			glBindBuffer(GL_ARRAY_BUFFER,
				vbo_colorHorizontalLine);

			glBufferData(GL_ARRAY_BUFFER,
				sizeof(GLfloat) * 3 * 2,
				lineColor,
				GL_DYNAMIC_DRAW);

			glVertexAttribPointer(VSV_ATTRIBUTE_COLOR,
				3,
				GL_FLOAT,
				GL_FALSE,
				0,
				NULL);

			glEnableVertexAttribArray(VSV_ATTRIBUTE_COLOR);

			glBindBuffer(GL_ARRAY_BUFFER, 0);
			glDrawArrays(GL_LINES, 0, 2);
			glBindVertexArray(0);
		}

	glUseProgram(0);	


	glXSwapBuffers(gpDisplay,
					gWindow);

}

void UnInitialize(void)
{
	
	if(vao_square)
	{
		glDeleteVertexArrays(1,
							&vao_square);
	}
	vao_square=0;

	if(vao_triangle)
	{
		glDeleteVertexArrays(1,
							&vao_triangle);
	}
	vao_triangle=0;

	if(vbo_colorTriangle){
		glDeleteBuffers(1,
						&vbo_colorTriangle);
	}
	vbo_colorTriangle=0;

	if(vbo_positionTriangle)
	{
		glDeleteBuffers(1,
					&vbo_positionTriangle);
	}
	vbo_positionTriangle=0;

	if(vbo_colorSquare){
		glDeleteBuffers(1,
						&vbo_colorSquare);
	}
	vbo_colorSquare=0;

	if(vbo_positionSquare)
	{
		glDeleteBuffers(1,
					&vbo_positionSquare);
	}
	vbo_positionSquare=0;

	if(gShaderProgramObject)
	{
		glUseProgram(gShaderProgramObject);

			GLsizei shaderCount;
			GLsizei tempShaderCount;
			GLuint *pShader=NULL;

			glGetProgramiv(gShaderProgramObject,
							GL_ATTACHED_SHADERS,
							&shaderCount);

			pShader=(GLuint *)malloc(sizeof(GLuint)*shaderCount);
			if(pShader==NULL)
			{
				printf("\nFailed to allocate memory for pShader");
			}else{
				glGetAttachedShaders(gShaderProgramObject,
									shaderCount,
									&tempShaderCount,
									pShader);
				
				printf("\nShader program has actual attached shader count is:%d",tempShaderCount);
				
				for(GLsizei i=0;i<shaderCount;i++)
				{
					glDetachShader(gShaderProgramObject,
									pShader[i]);

					glDeleteShader(pShader[i]);
					
					pShader[i]=0;
				}
				free(pShader);
				
				glDeleteProgram(gShaderProgramObject);
				
				gShaderProgramObject=0;
			}
		glUseProgram(0);
	}


	GLXContext currentGLXContext;

	currentGLXContext=glXGetCurrentContext();

	if(currentGLXContext==gGLXContext)
	{
		glXMakeCurrent(gpDisplay,
						0,
						0);
	}

	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay,
							gGLXContext);
	}

	gGLXContext=NULL;
	currentGLXContext=NULL;

	if(gWindow)
	{
		XDestroyWindow(gpDisplay,
						gWindow);
	}

	if(gColormap)
	{
		XFreeColormap(gpDisplay,
						gColormap);
	}

	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
	}
		gpXVisualInfo=NULL;

	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
	}
		gpDisplay=NULL;
}



void DrawCircle(void)
{
	double a, rad, x, y, z;
	int i;

	a = 0.0;
	int j = 0;
	for (i = 0; i <=CIRCLE_FlOAT_MEMBER_COUNT; i+=1)
	{

		rad = (2 * M_PI * i) / CIRCLE_FlOAT_MEMBER_COUNT;

		x = sin(rad);
		y = 0.0;
		z = cos(rad);

		printf( "\n i=%d j=%d \t x,y,z-%f,%f,%f", i,j, x,y,z);


		circle_Points[j] = x;
		j++;
		circle_Points[j] = y;
		j++;
		circle_Points[j] = z;
		j++;
	}

	printf("\n j-%d sizof-%d",j,sizeof(circle_Points)/sizeof(circle_Points[0]));

}
