#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<memory.h>

#include<GL/gl.h>
#include<GL/glx.h>
#include<GL/glu.h>

#include<X11/Xlib.h>
#include<X11/Xutil.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>

#include<SOIL/SOIL.h>
#include"TeaPot.h"

using namespace std;

bool bFullscreen=false;
Display *gpDisplay=NULL;
XVisualInfo *gpXVisualInfo=NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth=800;
int giWindowHeight=600;

GLXContext  gGLXContext;

GLuint Marbal_Texture;

bool gbLight=false;
bool gbTexture=false;
bool gbAnimate=false;

char keys[26];

GLfloat Light0Ambient[]={0.0f,0.0f,0.0f,1.0f};
GLfloat Light0Defuse[]={1.0f,1.0f,1.0f,1.0f};//White Light
GLfloat Light0Specular[]={1.0f,1.0f,1.0f,1.0f};
GLfloat Light0Position[]={100.0f,100.0f,100.0f,1.0f};//Positional Light

int main(void)
{
	void CreateWindow(void);
	void ToggleFullscreen(void);
	void UnInitialize(void);

	void Initialize(void);
	void Resize(int,int);
	void Draw(void);

	bool bDone=false;

	int winWidth=giWindowWidth;
	int winHeight=giWindowHeight;

	CreateWindow();
	Initialize();

	XEvent event;
	KeySym keysym;


	while(bDone==false)
	{
		while(XPending(gpDisplay))
		{
			XNextEvent(gpDisplay,
						&event);
			switch(event.type)
			{
				case MapNotify:
					break;
				case KeyPress:
					keysym=XkbKeycodeToKeysym(gpDisplay,
												event.xkey.keycode,
												0,
												0);

					switch(keysym)
					{
						case XK_Escape:
							bDone=true;
							break;
						case XK_F:
						case XK_f:
							if(bFullscreen==false)
							{
								ToggleFullscreen();
								bFullscreen=true;
							}	
							else
							{
								ToggleFullscreen();
								bFullscreen=false;
							}	
							break;
														
						default:
							break;
					}

					XLookupString(&event.xkey,keys,sizeof(keys),NULL,NULL);
					switch(keys[0])
					{
						case 'A':
						case 'a':
							if (gbAnimate==true){
								gbAnimate=false;
							}else{
								gbAnimate=true;
							}
							break;
						case 'L':
						case 'l':
							if (gbLight==true){
								glDisable(GL_LIGHTING );
								gbLight=false;
							}else{
								glEnable(GL_LIGHTING);
								gbLight=true;
							}
							break;
						case 'T':
						case 't':
							if (gbTexture==true){
								glDisable(GL_TEXTURE_2D);
								gbTexture=false;
							}else{
								glEnable(GL_TEXTURE_2D);
								gbTexture=true;
							}
							break;	
						default:
							break;
					}

					break;

				case MotionNotify:
					break;
				case ConfigureNotify:
					winWidth=event.xconfigure.width;
					winHeight=event.xconfigure.height;
					Resize(winWidth,winHeight);
					break;
				case Expose:
					break;
				case 33:
					bDone=true;
					break;
				default:
					break;
			}
		}
		Draw();
	}
	UnInitialize();
	return (0);
}

void CreateWindow(void)
{
	void UnInitialize(void);
	XSetWindowAttributes winAttribs;
	int defaultScreen;
	int styleMask;

	static int FrameBufferAttributes[]={GLX_DOUBLEBUFFER,True,
										GLX_RGBA,
										GLX_RED_SIZE,8,
										GLX_GREEN_SIZE,8,
										GLX_BLUE_SIZE,8,
										GLX_ALPHA_SIZE,8,
										GLX_DEPTH_SIZE,24,
										None};

	gpDisplay=XOpenDisplay(NULL);
	if(NULL==gpDisplay)
	{
		printf("\nERROR:Unable to Open X Display.\n");
		UnInitialize();
		exit(1);
	}

	defaultScreen=XDefaultScreen(gpDisplay);

	gpXVisualInfo=(XVisualInfo *)malloc(sizeof(XVisualInfo));
	if(NULL==gpXVisualInfo)
	{
		printf("\nERROR:Unable to allocate memory for visual info\n");
		UnInitialize();
		exit(1);
	}

	gpXVisualInfo=glXChooseVisual(gpDisplay,
								defaultScreen,
								FrameBufferAttributes);

	winAttribs.border_pixel=0;
	winAttribs.background_pixmap=0;
	winAttribs.colormap=XCreateColormap(gpDisplay,
										RootWindow(	gpDisplay,
													gpXVisualInfo->screen),
										gpXVisualInfo->visual,
										AllocNone);

	gColormap=winAttribs.colormap;

	winAttribs.event_mask=ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;

	styleMask=CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

	gWindow=XCreateWindow(	gpDisplay,
							RootWindow(	gpDisplay,
										gpXVisualInfo->screen),
							0,
							0,
							giWindowWidth,
							giWindowHeight,
							0,
							gpXVisualInfo->depth,
							InputOutput,
							gpXVisualInfo->visual,
							styleMask,
							&winAttribs);

	if(!gWindow)
	{
		printf("\nERROR:Failed to create main window\n");
		UnInitialize();
		exit(1);
	}

	XStoreName(	gpDisplay,
				gWindow,
				"XWindow-Tea Pot Model Loading with Light");

	Atom windowManagerDelete=XInternAtom(	gpDisplay,
											"WM_DELETE_WINDOW",
											True);

	XSetWMProtocols(gpDisplay,
					gWindow,
					&windowManagerDelete,
					1);

	XMapWindow(	gpDisplay,
				gWindow);

}

void ToggleFullscreen(void)
{
	Atom wm_state;
	Atom fullscreen;
	XEvent xev={0};

	wm_state=XInternAtom(gpDisplay,
						"_NET_WM_STATE",
						False);

	memset(&xev,
			0,
			sizeof(xev));

	xev.type=ClientMessage;
	xev.xclient.window=gWindow;
	xev.xclient.message_type=wm_state;
	xev.xclient.format=32;
	xev.xclient.data.l[0]=bFullscreen?0:1;

	fullscreen=XInternAtom(gpDisplay,
							"_NET_WM_STATE_FULLSCREEN",
							False);

	xev.xclient.data.l[1]=fullscreen;

	XSendEvent(gpDisplay,
				RootWindow(gpDisplay,
							gpXVisualInfo->screen),
				False,
				StructureNotifyMask,
				&xev);
}


void Initialize(void)
{
	void Resize(int,int);
	void UnInitialize(void);

	GLuint LoadBitmapTexture(const char*);

	gGLXContext=glXCreateContext(	gpDisplay,
									gpXVisualInfo,
									NULL,
									GL_TRUE);

	if(NULL==gGLXContext)
	{
		printf("\nERROR:Unable to create rendering context\n");
		UnInitialize();
		exit(1);
	}

	glXMakeCurrent(gpDisplay,
					gWindow,
					gGLXContext);

	glClearColor(0.0f,0.0f,0.0f,0.0f);

	glClearDepth(1.0f);

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glShadeModel(GL_SMOOTH);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT,GL_NICEST);

	//Set Light 0
	glLightfv(GL_LIGHT0,GL_AMBIENT,Light0Ambient);
	glLightfv(GL_LIGHT0,GL_DIFFUSE,Light0Defuse);
	glLightfv(GL_LIGHT0,GL_POSITION,Light0Position);
	glLightfv(GL_LIGHT0,GL_SPECULAR,Light0Specular);
	glEnable(GL_LIGHT0);


	Marbal_Texture=LoadBitmapTexture("marble.bmp");

	glBindTexture(	GL_TEXTURE_2D,
					Marbal_Texture);

	Resize(giWindowWidth,giWindowHeight);
}

void Resize(int width,int height)
{
	if(height==0)
	{
		height=1;
	}

	glViewport(0,
				0,
				(GLsizei)width,
				(GLsizei)height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f,
					(GLfloat)width/(GLfloat)height,
					0.1f,
					100.0f);
}

GLfloat RotateAngle=0.0f;

void Draw(void)
{

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(0.0f,0.0f,-1.0f);

	glRotatef(RotateAngle, 0.0f, 1.0f, 0.0f);
	
	glBegin(GL_TRIANGLES);

		for(int i=0;i<(sizeof(face_indicies)/sizeof(face_indicies[0]));i++)
		{
			for(int j=0;j<3;j++)
			{
				int vi=face_indicies[i][j];
				int ni=face_indicies[i][j+3];
				int ti=face_indicies[i][j+6];
				glNormal3f(normals[ni][0],normals[ni][1],normals[ni][2]);
				glTexCoord2f(textures[ti][0],textures[ti][1]);
				glVertex3f(vertices[vi][0],vertices[vi][1],vertices[vi][2]);
			}
		}

	glEnd();
	

	if (gbAnimate==GL_TRUE){

		RotateAngle += 1.0f;
	}

	glXSwapBuffers(	gpDisplay,
					gWindow);
}

GLuint LoadBitmapTexture(const char *path)
{
	int width,height;
	unsigned char* imageData=NULL;
	GLuint texture_id;

	imageData=SOIL_load_image(	path,
								&width,
								&height,
								NULL,
								SOIL_LOAD_RGB);

	glPixelStorei(	GL_UNPACK_ALIGNMENT,
					4);

	glGenTextures(	1,
					&texture_id);


	glBindTexture(	GL_TEXTURE_2D,
					texture_id);

	glTexParameteri(GL_TEXTURE_2D,
					GL_TEXTURE_MIN_FILTER,
					GL_LINEAR_MIPMAP_LINEAR);

	glTexParameteri(GL_TEXTURE_2D,
					GL_TEXTURE_MAG_FILTER,
					GL_LINEAR);

	gluBuild2DMipmaps(	GL_TEXTURE_2D,
						3,
						width,
						height,
						GL_RGB,
						GL_UNSIGNED_BYTE,
						imageData);

	SOIL_free_image_data(imageData);


	return texture_id;
}

void UnInitialize(void)
{
	GLXContext currentGLXContext;

	if(Marbal_Texture)
	{
		glDeleteTextures(1,&Marbal_Texture);
	}
		Marbal_Texture=0;

	currentGLXContext=glXGetCurrentContext();

	if(currentGLXContext==gGLXContext)
	{
		glXMakeCurrent(gpDisplay,
						0,
						0);
	}

	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay,
							gGLXContext);
	}

	gGLXContext=NULL;
	currentGLXContext=NULL;

	if(gWindow)
	{
		XDestroyWindow(gpDisplay,
						gWindow);
	}

	if(gColormap)
	{
		XFreeColormap(gpDisplay,
						gColormap);
	}

	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
	}
		gpXVisualInfo=NULL;

	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
	}

		gpDisplay=NULL;
}



void PressedDigit_1(void)
{
	glBegin(GL_QUADS);
		
		glTexCoord2f(1.0f,1.0f);
			glVertex3f(1.0f, 1.0f, 0.0f);
		glTexCoord2f(0.0f,1.0f);
			glVertex3f(-1.0f, 1.0f, 0.0f);
		glTexCoord2f(0.0f,0.0f);
			glVertex3f(-1.0f, -1.0f, 0.0f);
		glTexCoord2f(1.0f,0.0f);
			glVertex3f(1.0f, -1.0f, 0.0f);

	
	glEnd();
}
void PressedDigit_2(void)
{
	glBegin(GL_QUADS);
		
		glTexCoord2f(0.5f,0.5f);
			glVertex3f(1.0f, 1.0f, 0.0f);
		glTexCoord2f(0.0f,0.5f);
			glVertex3f(-1.0f, 1.0f, 0.0f);
		glTexCoord2f(0.0f,0.0f);
			glVertex3f(-1.0f, -1.0f, 0.0f);
		glTexCoord2f(0.5f,0.0f);
			glVertex3f(1.0f, -1.0f, 0.0f);
		
	
	glEnd();
}
void PressedDigit_3(void)
{
	glBegin(GL_QUADS);
		
		glTexCoord2f(0.0f,0.0f);
			glVertex3f(1.0f, 1.0f, 0.0f);
		glTexCoord2f(2.0f,0.0f);
			glVertex3f(-1.0f, 1.0f, 0.0f);

			
		glTexCoord2f(2.0f,2.0f);
			glVertex3f(-1.0f, -1.0f, 0.0f);
		glTexCoord2f(0.0f,2.0f);
			glVertex3f(1.0f, -1.0f, 0.0f);
		
	
	glEnd();
}
void PressedDigit_4(void)
{
	glBegin(GL_QUADS);
		glTexCoord2f(0.5f,0.5f);
			glVertex3f(1.0f, 1.0f, 0.0f);
		glTexCoord2f(0.5f,0.5f);
			glVertex3f(-1.0f, 1.0f, 0.0f);
		glTexCoord2f(0.5f,0.5f);
			glVertex3f(-1.0f, -1.0f, 0.0f);
		glTexCoord2f(0.5f,0.5f);
			glVertex3f(1.0f, -1.0f, 0.0f);
		
	
	glEnd();
}

void PressedDigit_0(void)
{
	glColor3f(1.0f,1.0f,1.0f);
	glBegin(GL_QUADS);
		glVertex3f(-1.0f, -1.0f, 0.0f);
		glVertex3f(1.0f, -1.0f, 0.0f);
		glVertex3f(1.0f, 1.0f, 0.0f);
		glVertex3f(-1.0f, 1.0f, 0.0f);
	glEnd();
}