#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<memory.h>

#include<GL/gl.h>
#include<GL/glx.h>
#include<GL/glu.h>

#include<X11/Xlib.h>
#include<X11/Xutil.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>

using namespace std;

#define CHECK_IMAGE_WIDTH 64
#define CHECK_IMAGE_HEIGHT 64

bool bFullscreen=false;
Display *gpDisplay=NULL;
XVisualInfo *gpXVisualInfo=NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth=800;
int giWindowHeight=600;

GLXContext  gGLXContext;

GLuint Tex_Image;//texture variable
GLubyte checkImage[CHECK_IMAGE_HEIGHT][CHECK_IMAGE_WIDTH][4];

int main(void)
{
	void CreateWindow(void);
	void ToggleFullscreen(void);
	void UnInitialize(void);

	void Initialize(void);
	void Resize(int,int);
	void Draw(void);

	bool bDone=false;

	int winWidth=giWindowWidth;
	int winHeight=giWindowHeight;

	CreateWindow();
	Initialize();

	XEvent event;
	KeySym keysym;


	while(bDone==false)
	{
		while(XPending(gpDisplay))
		{
			XNextEvent(gpDisplay,
						&event);
			switch(event.type)
			{
				case MapNotify:
					break;
				case KeyPress:
					keysym=XkbKeycodeToKeysym(gpDisplay,
												event.xkey.keycode,
												0,
												0);

					switch(keysym)
					{
						case XK_Escape:
							bDone=true;
							break;
						case XK_F:
						case XK_f:
							if(bFullscreen==false)
							{
								ToggleFullscreen();
								bFullscreen=true;
							}	
							else
							{
								ToggleFullscreen();
								bFullscreen=false;
							}	
							break;									
						default:
							break;
					}
					break;

				case MotionNotify:
					break;
				case ConfigureNotify:
					winWidth=event.xconfigure.width;
					winHeight=event.xconfigure.height;
					Resize(winWidth,winHeight);
					break;
				case Expose:
					break;
				case 33:
					bDone=true;
					break;
				default:
					break;
			}
		}
		Draw();
	}
	UnInitialize();
	return (0);
}

void CreateWindow(void)
{
	void UnInitialize(void);
	XSetWindowAttributes winAttribs;
	int defaultScreen;
	int styleMask;

	static int FrameBufferAttributes[]={GLX_DOUBLEBUFFER,True,
										GLX_RGBA,
										GLX_RED_SIZE,8,
										GLX_GREEN_SIZE,8,
										GLX_BLUE_SIZE,8,
										GLX_ALPHA_SIZE,8,
										GLX_DEPTH_SIZE,24,
										None};

	gpDisplay=XOpenDisplay(NULL);
	if(NULL==gpDisplay)
	{
		printf("\nERROR:Unable to Open X Display.\n");
		UnInitialize();
		exit(1);
	}

	defaultScreen=XDefaultScreen(gpDisplay);

	gpXVisualInfo=(XVisualInfo *)malloc(sizeof(XVisualInfo));
	if(NULL==gpXVisualInfo)
	{
		printf("\nERROR:Unable to allocate memory for visual info\n");
		UnInitialize();
		exit(1);
	}

	gpXVisualInfo=glXChooseVisual(gpDisplay,
								defaultScreen,
								FrameBufferAttributes);

	winAttribs.border_pixel=0;
	winAttribs.background_pixmap=0;
	winAttribs.colormap=XCreateColormap(gpDisplay,
										RootWindow(	gpDisplay,
													gpXVisualInfo->screen),
										gpXVisualInfo->visual,
										AllocNone);

	gColormap=winAttribs.colormap;

	winAttribs.event_mask=ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;

	styleMask=CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

	gWindow=XCreateWindow(	gpDisplay,
							RootWindow(	gpDisplay,
										gpXVisualInfo->screen),
							0,
							0,
							giWindowWidth,
							giWindowHeight,
							0,
							gpXVisualInfo->depth,
							InputOutput,
							gpXVisualInfo->visual,
							styleMask,
							&winAttribs);

	if(!gWindow)
	{
		printf("\nERROR:Failed to create main window\n");
		UnInitialize();
		exit(1);
	}

	XStoreName(	gpDisplay,
				gWindow,
				"XWindow-CheckerBoard");

	Atom windowManagerDelete=XInternAtom(	gpDisplay,
											"WM_DELETE_WINDOW",
											True);

	XSetWMProtocols(gpDisplay,
					gWindow,
					&windowManagerDelete,
					1);

	XMapWindow(	gpDisplay,
				gWindow);

}

void ToggleFullscreen(void)
{
	Atom wm_state;
	Atom fullscreen;
	XEvent xev={0};

	wm_state=XInternAtom(gpDisplay,
						"_NET_WM_STATE",
						False);

	memset(&xev,
			0,
			sizeof(xev));

	xev.type=ClientMessage;
	xev.xclient.window=gWindow;
	xev.xclient.message_type=wm_state;
	xev.xclient.format=32;
	xev.xclient.data.l[0]=bFullscreen?0:1;

	fullscreen=XInternAtom(gpDisplay,
							"_NET_WM_STATE_FULLSCREEN",
							False);

	xev.xclient.data.l[1]=fullscreen;

	XSendEvent(gpDisplay,
				RootWindow(gpDisplay,
							gpXVisualInfo->screen),
				False,
				StructureNotifyMask,
				&xev);
}


void Initialize(void)
{
	void Resize(int,int);
	void UnInitialize(void);

	void LoadGLTexture(void);

	gGLXContext=glXCreateContext(	gpDisplay,
									gpXVisualInfo,
									NULL,
									GL_TRUE);

	if(NULL==gGLXContext)
	{
		printf("\nERROR:Unable to create rendering context\n");
		UnInitialize();
		exit(1);
	}

	glXMakeCurrent(gpDisplay,
					gWindow,
					gGLXContext);

	glClearColor(0.0f,0.0f,0.0f,0.0f);

	glClearDepth(1.0f);

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glShadeModel(GL_SMOOTH);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT,GL_NICEST);

	glEnable(GL_TEXTURE_2D);

	LoadGLTexture();

	Resize(giWindowWidth,giWindowHeight);
}

void Resize(int width,int height)
{
	if(height==0)
	{
		height=1;
	}

	glViewport(0,
				0,
				(GLsizei)width,
				(GLsizei)height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f,
					(GLfloat)width/(GLfloat)height,
					0.1f,
					100.0f);
}

GLfloat RotateAngle=0.0f;

void Draw(void)
{


	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(0.0f,0.0f,-6.0f);

	//Texuture render logic

	glBindTexture(	GL_TEXTURE_2D,
					Tex_Image);
	
	glBegin(GL_QUADS);
		
		glTexCoord2f(0.0f,0.0f);
			glVertex3f(-2.0f, -1.0f, 0.0f);
		glTexCoord2f(0.0f,1.0f);
			glVertex3f(-2.0f, 1.0f, 0.0f);
		glTexCoord2f(1.0f,1.0f);
			glVertex3f(0.0f, 1.0f, 0.0f);	
		glTexCoord2f(1.0f,0.0f);
			glVertex3f(0.0f, -1.0f, 0.0f);
	
	glEnd();

	glBegin(GL_QUADS);
		
		glTexCoord2f(0.0f,0.0f);
			glVertex3f(1.0f, -1.0f, 0.0f);
		glTexCoord2f(0.0f,1.0f);
			glVertex3f(1.0f, 1.0f, 0.0f);
		glTexCoord2f(1.0f,1.0f);
			glVertex3f(2.41421f, 1.0f, -1.41421f);	
		glTexCoord2f(1.0f,0.0f);
			glVertex3f(2.41421f, -1.0f, -1.41421f);
	
	glEnd();


	if(RotateAngle>360.0f)
		RotateAngle=0.0f;
	
	RotateAngle += 0.01f;

	glXSwapBuffers(	gpDisplay,
					gWindow);
}


void UnInitialize(void)
{
	GLXContext currentGLXContext;

	if(Tex_Image)
	{
		glDeleteTextures(1,&Tex_Image);	
	}
		Tex_Image=0;

	currentGLXContext=glXGetCurrentContext();

	if(currentGLXContext==gGLXContext)
	{
		glXMakeCurrent(gpDisplay,
						0,
						0);
	}

	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay,
							gGLXContext);
	}

	gGLXContext=NULL;
	currentGLXContext=NULL;

	if(gWindow)
	{
		XDestroyWindow(gpDisplay,
						gWindow);
	}

	if(gColormap)
	{
		XFreeColormap(gpDisplay,
						gColormap);
	}

	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
	}
		gpXVisualInfo=NULL;

	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
	}

		gpDisplay=NULL;
}




void LoadGLTexture(void)
{
	void MakeCheckImage(void);
	MakeCheckImage();

		glGenTextures( 1,
					&Tex_Image
					);

		glBindTexture( GL_TEXTURE_2D,
						Tex_Image
					);

		glPixelStorei( GL_UNPACK_ALIGNMENT,
						1
					);			

		//Setting Texture Parameter
		glTexParameteri( GL_TEXTURE_2D,
						GL_TEXTURE_MAG_FILTER,
						GL_NEAREST
						);
		glTexParameteri( GL_TEXTURE_2D,
						GL_TEXTURE_MIN_FILTER,
						GL_NEAREST
					   );
		glTexParameteri( GL_TEXTURE_2D,
						GL_TEXTURE_WRAP_S,//Wrap Image/geometry at end (Horizontal)
						GL_REPEAT
						);
		glTexParameteri( GL_TEXTURE_2D,
						GL_TEXTURE_WRAP_T,//Wrap Image/geometry at end (vertical)
						GL_REPEAT
					   );

		glTexImage2D(GL_TEXTURE_2D,
					0,//mipmap level-use default
					4,//for RGBA-Internal texture Format or also can use GL_RGBA 
					CHECK_IMAGE_WIDTH,//Image Width
					CHECK_IMAGE_HEIGHT,//Image Height
					0,//Boarder width if were to set a value then it should be in power of 2
					GL_RGBA,
					GL_UNSIGNED_BYTE,
					checkImage
					);
}


void MakeCheckImage(void)
{
	int vsv_i,vsv_j,vsv_c;

    for(vsv_i=0;vsv_i<CHECK_IMAGE_HEIGHT;vsv_i++){

        for(vsv_j=0;vsv_j<CHECK_IMAGE_WIDTH;vsv_j++){

            vsv_c=( (vsv_i & 0x8 )==0)^( (vsv_j & 0x8 )==0);
			vsv_c*=255;
			checkImage[vsv_i][vsv_j][0]=(GLubyte)vsv_c;
			checkImage[vsv_i][vsv_j][1]=(GLubyte)vsv_c;
			checkImage[vsv_i][vsv_j][2]=(GLubyte)vsv_c;
			checkImage[vsv_i][vsv_j][3]=255;
        }
    }
}
