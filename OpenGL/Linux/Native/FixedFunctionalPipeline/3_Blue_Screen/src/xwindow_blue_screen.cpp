#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<memory.h>

#include<GL/gl.h>
#include<GL/glx.h>

#include<X11/Xlib.h>
#include<X11/Xutil.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>

using namespace std;

bool bFullscreen=false;
Display *gpDisplay=NULL;
XVisualInfo *gpXVisualInfo=NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth=800;
int giWindowHeight=600;

GLXContext gGLXContext;

int main(void)
{
   
   //printf("ERROR:inside main()");

   void CreateWindow(void);
   void ToggleFullscreen(void);
   void uninitialize();

   void Initialize(void);
   void Resize(int,int);
   void Draw(void);

   bool bDone=false;


   int winWidth=giWindowWidth;
   int winHeight=giWindowHeight;

   // printf("INFO:Main Code Starts");
    //printf("INFO:Initialize() finish");
    CreateWindow();
    Initialize();
   
   XEvent event;
   KeySym keysym;
    
   // printf("ERROR:Occured after CreateWindow()");

    while(bDone==false)
    {
        while(XPending(gpDisplay))
        {
            XNextEvent(gpDisplay,&event);
            switch(event.type)
            {
                case MapNotify:
                    break;
                case KeyPress:
                    keysym=XkbKeycodeToKeysym(gpDisplay,event.xkey.keycode,0,0);
                    switch(keysym)
                    {
                        case XK_Escape:
                           bDone=true;
                           break;
                        case XK_F:
                        case XK_f:
                            if(bFullscreen==false)
                            {
                                ToggleFullscreen();
                                bFullscreen=true;
                            }else{
                                ToggleFullscreen();
                                bFullscreen=false;
                            }
                            break;
                        default:
                            break;
                    }
                    break;
                case ButtonPress:
                    switch(event.xbutton.button)
                    {
                        case 1:
                            break;
                        case 2:
                            break;
                        case 3:
                            break;
                        default:
                            break;
                    }
                    break;
                case MotionNotify:
                    break;
                case ConfigureNotify:
                    winWidth=event.xconfigure.width;
                    winHeight=event.xconfigure.height;
                    Resize(winWidth,winHeight);
                    break;
                case Expose:
                    break;
                case 33:
                    bDone=true;
                    break;
                default:
                    break;
            }
        }
        Draw();
    }
    uninitialize();
    return(0);
}

void CreateWindow(void)
{
    void uninitialize(void);

    XSetWindowAttributes winAttribs;
    int defaultScreen;
    //int defaultDepth;
    int styleMask;

    static int FrameBufferAttributes[]={GLX_RGBA,
                                        GLX_RED_SIZE,1,
                                        GLX_GREEN_SIZE,1,
                                        GLX_BLUE_SIZE,1,
                                        GLX_ALPHA_SIZE,1,
                                        None};

    gpDisplay=XOpenDisplay(NULL);
    if(gpDisplay==NULL)
    {
        printf("ERROR:Unable to Open X Display.\nExitting Now..!\n");
        uninitialize();
        exit(1);        
    }

    defaultScreen=XDefaultScreen(gpDisplay);
    //defaultDepth=DefaultDepth(gpDisplay,defaultScreen);

    gpXVisualInfo=(XVisualInfo *)malloc(sizeof(XVisualInfo));
    if(gpXVisualInfo==NULL)
    {
        printf("ERROR:Unable to allocate memory for visual info.\nExitting Now..\n");
        uninitialize();
        exit(1);
    }

    // XMatchVisualInfo(gpDisplay,defaultScreen,defaultDepth,TrueColor,gpXVisualInfo);
    // if(gpXVisualInfo==NULL)
    // {
    //     printf("ERROR:Unable to get a visual\nExitting Now..\n");
    //     uninitialize();
    //     exit(1);
    // }

    gpXVisualInfo=glXChooseVisual(gpDisplay,
                                defaultScreen,  
                                FrameBufferAttributes);

    winAttribs.border_pixel=0;
    winAttribs.background_pixmap=0;
    winAttribs.colormap=XCreateColormap(gpDisplay,
                                    RootWindow(gpDisplay,gpXVisualInfo->screen),
                                    gpXVisualInfo->visual,
                                    AllocNone);
    
    gColormap=winAttribs.colormap;
    winAttribs.background_pixel=BlackPixel(gpDisplay,defaultScreen);

    winAttribs.event_mask=ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;

    styleMask=CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

    gWindow=XCreateWindow(gpDisplay,
                        RootWindow(gpDisplay,gpXVisualInfo->screen),
                        0,
                        0,
                        giWindowWidth,
                        giWindowHeight,
                        0,
                        gpXVisualInfo->depth,
                        InputOutput,
                        gpXVisualInfo->visual,
                        styleMask,
                        &winAttribs);
    
    if(!gWindow)
    {
        printf("ERROR:Failed to create main window.\nExitting Now..\n");
        uninitialize();
        exit(1);
    }

    XStoreName(gpDisplay,gWindow,"VSViroje:Blue Screen");

    Atom windowManagerDelete=XInternAtom(gpDisplay,"WM_DELETE_WINDOW",True);
    XSetWMProtocols(gpDisplay,gWindow,&windowManagerDelete,1);

    XMapWindow(gpDisplay,gWindow);
}

void ToggleFullscreen(void)
{
    Atom wm_state;
    Atom fullscreen;
    XEvent xev={0};

    wm_state=XInternAtom(gpDisplay,"_NET_WM_STATE",False);
    memset(&xev,0,sizeof(xev));

    xev.type=ClientMessage;
    xev.xclient.window=gWindow;
    xev.xclient.message_type=wm_state;
    xev.xclient.format=32;
    xev.xclient.data.l[0]=bFullscreen?0:1;
    fullscreen=XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN",False);
    xev.xclient.data.l[1]=fullscreen;

    XSendEvent(gpDisplay,
                RootWindow(gpDisplay,gpXVisualInfo->screen),
                False,
                StructureNotifyMask,
                &xev);

}

void uninitialize(void)
{
    GLXContext currentGLXContext;

    currentGLXContext=glXGetCurrentContext();

    if(currentGLXContext==gGLXContext)
    {
        glXMakeCurrent(gpDisplay,
                        0,
                        0);
    }

    if(gGLXContext)
    {
        glXDestroyContext(gpDisplay,
                            gGLXContext);
    }

    gGLXContext=NULL;
    currentGLXContext=NULL;

    if(gWindow)
    {
        XDestroyWindow(gpDisplay,gWindow);
    }

    if(gColormap)
    {
        XFreeColormap(gpDisplay,gColormap);
    }

    if(gpXVisualInfo)
    {
        free(gpXVisualInfo);
        gpXVisualInfo=NULL;
    }

    if(gpDisplay)
    {
        XCloseDisplay(gpDisplay);
        gpDisplay=NULL;
    }
}

void Initialize(void)
{
    void Resize(int,int);

    //printf("ERROR:Occured inside Initialize()");

    gGLXContext=glXCreateContext(gpDisplay,
                                gpXVisualInfo,
                                NULL,
                                GL_TRUE);
    if(gGLXContext==NULL)
    {
        printf("ERROR:Unable to gate Rendring Context.\nExitting Now..!\n");
        uninitialize();
        exit(1);  
    }

    glXMakeCurrent(gpDisplay,
                    gWindow,
                    gGLXContext);
    
    glClearColor(0.0f,0.0f,1.0f,0.0f);
    Resize(giWindowWidth,giWindowHeight);
   // printf("ERROR:exited Initialize()");

}

void Resize(int width,int height)
{
    if(height==0)
    {
        height=1;
    }
    glViewport(0,0,(GLsizei)width,(GLsizei)height);
}

void Draw(void)
{
    glClear(GL_COLOR_BUFFER_BIT);
    glFlush();
}