#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<memory.h>

#include<GL/gl.h>
#include<GL/glx.h>
#include<GL/glu.h>

#include<X11/Xlib.h>
#include<X11/Xutil.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>

using namespace std;

bool bFullscreen=false;
Display *gpDisplay=NULL;
XVisualInfo *gpXVisualInfo=NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth=800;
int giWindowHeight=600;

GLXContext gGLXContext;

bool gbLight=false;


GLfloat LightAmbientZero[]={0.0f,0.0f,0.0f,1.0f};
GLfloat LightDefuseZero[]={1.0f,0.0f,0.0f,1.0f};//Red Light
GLfloat LightSpecularZero[]={1.0f,0.0f,0.0f,1.0f};
GLfloat LightPositionZero[]={0.0f,0.0f,0.0f,1.0f};//positional light

GLfloat LightAmbientOne[]={0.0f,0.0f,0.0f,1.0f};
GLfloat LightDefuseOne[]={0.0f,1.0f,0.0f,1.0f};//Green Light
GLfloat LightSpecularOne[]={0.0f,1.0f,0.0f,1.0f};
GLfloat LightPositionOne[]={0.0f,0.0f,0.0f,1.0f};//positional light


GLfloat LightAmbientTwo[]={0.0f,0.0f,0.0f,1.0f};
GLfloat LightDefuseTwo[]={0.0f,0.0f,1.0f,1.0f};//Blue Light
GLfloat LightSpecularTwo[]={0.0f,0.0f,1.0f,1.0f};
GLfloat LightPositionTwo[]={0.0f,0.0f,0.0f,1.0f};//positional light

GLfloat materialAmbient[]={0.0f,0.0f,0.0f,1.0f};
GLfloat materilaDefuse[]={1.0f,1.0f,1.0f,1.0f};
GLfloat materialSpecular[]={1.0f,1.0f,1.0f,1.0f};
GLfloat materialShininess=128.0f;

GLfloat LightAngle0=0.0f;
GLfloat LightAngle1=0.0f;
GLfloat LightAngle2=0.0f;

GLUquadric *SphereQuadric=NULL;


int main(void)
{
	void CreateWindow(void);
	void ToggleFullscreen(void);
	void UnInitialize(void);

	void Initialize(void);
	void Resize(int,int);
	void Draw(void);

	bool bDone=false;

	int winWidth,winHeight;

	CreateWindow();
	Initialize();

	XEvent event;
	KeySym keysym;

	while(bDone==false)
	{
		while(XPending(gpDisplay))
		{
			XNextEvent(gpDisplay,
						&event);
			switch(event.type)
			{
				case MapNotify:
					break;
				case KeyPress:
					keysym=XkbKeycodeToKeysym(gpDisplay,
												event.xkey.keycode,
												0,
												0);
					switch(keysym)
					{
						case XK_Escape:
							bDone=true;
							break;
						case XK_F:
						case XK_f:
							if(bFullscreen==false)
							{
								ToggleFullscreen();
								bFullscreen=true;
							}
							else
							{
								ToggleFullscreen();
								bFullscreen=false;
							}
							break;
						case XK_L:
						case XK_l:
							if (gbLight==true){
								glDisable(GL_LIGHTING );
								gbLight=false;
							}else{
								glEnable(GL_LIGHTING);
								gbLight=true;
							}
							break;
						default:
							break;
					}
					break;

				case MotionNotify:
					break;
				case ConfigureNotify:
					winWidth=event.xconfigure.width;
					winHeight=event.xconfigure.height;
					Resize(winWidth,winHeight);
					break;
				case Expose:
					break;
				case 33:
					bDone=true;
					break;
				default:
					break;
			}
		}
		Draw();
	}
	UnInitialize();
	return (0);
}

void CreateWindow(void)
{
	void UnInitialize(void);
	XSetWindowAttributes winAttribs;
	int defaultScreen;
	int styleMask;

	static int FrameBufferAttributes[]={GLX_DOUBLEBUFFER,True,
										GLX_RGBA,
										GLX_RED_SIZE,8,
										GLX_GREEN_SIZE,8,
										GLX_BLUE_SIZE,8,
										GLX_ALPHA_SIZE,8,
										GLX_DEPTH_SIZE,24,
										None};

	gpDisplay=XOpenDisplay(NULL);
	if(gpDisplay==NULL)
	{
		printf("\nERROR:Unable to Open X Display\n");
		UnInitialize();
		exit(1);
	}

	defaultScreen=XDefaultScreen(gpDisplay);

	gpXVisualInfo=(XVisualInfo *)malloc(sizeof(XVisualInfo));
	if(gpXVisualInfo==NULL)
	{
		printf("\nERROR:Unable to allocate visual info\n");
		UnInitialize();
		exit(1);
	}

	gpXVisualInfo=glXChooseVisual(gpDisplay,
								defaultScreen,
								FrameBufferAttributes);

	winAttribs.border_pixel=0;
	winAttribs.background_pixmap=0;
	winAttribs.colormap=XCreateColormap(gpDisplay,
										RootWindow(gpDisplay,
													gpXVisualInfo->screen),
										gpXVisualInfo->visual,
										AllocNone);
	gColormap=winAttribs.colormap;
	winAttribs.background_pixel=BlackPixel(gpDisplay,
											defaultScreen);

	winAttribs.event_mask=ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;

	styleMask=CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

	gWindow=XCreateWindow(gpDisplay,
							RootWindow(gpDisplay,
										gpXVisualInfo->screen),
							0,
							0,
							giWindowWidth,
							giWindowHeight,
							0,
							gpXVisualInfo->depth,
							InputOutput,
							gpXVisualInfo->visual,
							styleMask,
							&winAttribs);

	if(!gWindow)
	{
		printf("\nERROR:Failed to create main window\n");
		UnInitialize();
		exit(1);
	}

	XStoreName(gpDisplay,
				gWindow,
				"XWindow-Three Rotating Light Sphere");

	Atom windowManagerDelete=XInternAtom(gpDisplay,
										"WM_DELETE_WINDOW",
										True);

	XSetWMProtocols(gpDisplay,
					gWindow,
					&windowManagerDelete,
					1);

	XMapWindow(gpDisplay,
				gWindow);

}

void ToggleFullscreen(void)
{
	Atom wm_state;
	Atom fullScreen;
	XEvent xev={0};

	wm_state=XInternAtom(gpDisplay,
						"_NET_WM_STATE",
						False);

	xev.type=ClientMessage;
	xev.xclient.window=gWindow;
	xev.xclient.message_type=wm_state;
	xev.xclient.format=32;
	xev.xclient.data.l[0]=bFullscreen?0:1;

	fullScreen=XInternAtom(gpDisplay,
							"_NET_WM_STATE_FULLSCREEN",
							False);

	xev.xclient.data.l[1]=fullScreen;
	XSendEvent(gpDisplay,
				RootWindow(gpDisplay,
							gpXVisualInfo->screen),
				False,
				StructureNotifyMask,
				&xev);
}

void Initialize(void)
{
	void Resize(int,int);
	void UnInitialize(void);

	gGLXContext=glXCreateContext(gpDisplay,
								gpXVisualInfo,
								NULL,
								GL_TRUE);

	if(gGLXContext==NULL)
	{
		printf("\nERROR:Unable to create Rendering Context\n");
		UnInitialize();
		exit(1);
	}

	glXMakeCurrent(gpDisplay,
					gWindow,
					gGLXContext);

	glClearColor(0.0f,0.0f,0.0f,0.0f);

	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glShadeModel(GL_SMOOTH);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT,GL_NICEST);

		// Set Lighting
	glLightfv(GL_LIGHT0,GL_AMBIENT,LightAmbientZero);
	glLightfv(GL_LIGHT0,GL_DIFFUSE,LightDefuseZero);
	glLightfv(GL_LIGHT0,GL_SPECULAR,LightSpecularZero);
	glEnable(GL_LIGHT0);

	glLightfv(GL_LIGHT1,GL_AMBIENT,LightAmbientOne);
	glLightfv(GL_LIGHT1,GL_DIFFUSE,LightDefuseOne);
	glLightfv(GL_LIGHT1,GL_SPECULAR,LightSpecularOne);
	glEnable(GL_LIGHT1);

	glLightfv(GL_LIGHT2,GL_AMBIENT,LightAmbientTwo);
	glLightfv(GL_LIGHT2,GL_DIFFUSE,LightDefuseTwo);
	glLightfv(GL_LIGHT2,GL_SPECULAR,LightSpecularTwo);
	glEnable(GL_LIGHT2);

	//Set Material
	glMaterialfv(GL_FRONT,GL_AMBIENT,materialAmbient);
	glMaterialfv(GL_FRONT,GL_DIFFUSE,materilaDefuse);
	glMaterialfv(GL_FRONT,GL_SPECULAR,materialSpecular);
	glMaterialf(GL_FRONT,GL_SHININESS,materialShininess);

	//Quadric Initailization
	SphereQuadric=gluNewQuadric();


	Resize(giWindowWidth,giWindowHeight);
}

void Resize(int width,int height)
{
	if(height==0)
	{
		height=1;
	}

	glViewport(0,
				0,
				(GLsizei)width,
				(GLsizei)height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f,
					(GLfloat)width/(GLfloat)height,
					0.1f,
					100.0f);
}

void Draw(void)
{
	void RenderGLUSphere(GLUquadric *,GLdouble,GLint,GLint);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glPushMatrix();
		gluLookAt(
			0.0f,0.0f,2.0f,//camera/eye parameters
			0.0f,0.0f,0.0f,//center/focus coordinate parameter
			0.0f,1.0f,0.0f//Up vector/axis parameter
		);
		glPushMatrix();
			glRotatef(LightAngle0,1.0f,0.0f,0.0f);
			LightPositionZero[1]=LightAngle0;
			glLightfv(GL_LIGHT0,GL_POSITION,LightPositionZero);
		glPopMatrix();

		glPushMatrix();
			glRotatef(LightAngle1,0.0f,1.0f,0.0f);
			LightPositionOne[0]=LightAngle1;
			glLightfv(GL_LIGHT1,GL_POSITION,LightPositionOne);
		glPopMatrix();

		glPushMatrix();
			glRotatef(LightAngle2,0.0f,0.0f,1.0f);
			LightPositionTwo[1]=LightAngle2;
			glLightfv(GL_LIGHT2,GL_POSITION,LightPositionTwo);
		glPopMatrix();

		glTranslatef(0.0f,0.0f,-1.0f);
		RenderGLUSphere(SphereQuadric,1.0f,60,60);//Normal vector Are calculated by GLU sphere itself we dont need to specify Nprmal vector.
	
	glPopMatrix();


	glXSwapBuffers(gpDisplay,
					gWindow);

	
	LightAngle0 += 0.01f;

	LightAngle1 += 0.01f;
	
	LightAngle2 += 0.01f;
}

void UnInitialize(void)
{
	GLXContext currentGLXContext;

	currentGLXContext=glXGetCurrentContext();

	if(currentGLXContext==gGLXContext)
	{
		glXMakeCurrent(gpDisplay,
						0,
						0);
	}

	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay,
							gGLXContext);
	}

	gGLXContext=NULL;
	currentGLXContext=NULL;

	if(gWindow)
	{
		XDestroyWindow(gpDisplay,
						gWindow);
	}

	if(gColormap)
	{
		XFreeColormap(gpDisplay,
					gColormap);
	}

	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
	}
		gpXVisualInfo=NULL;

	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
	}
		gpDisplay=NULL;


	gluDeleteQuadric(SphereQuadric);
	SphereQuadric=NULL;
}

void RenderGLUSphere(GLUquadric *qobj,/*The quadric object*/
   GLdouble   radius,/*radius of sphere*/
   GLint      slices,/*horizontal//longitude*/
   GLint      stacks)/*vertical//latitude*/
{
	gluSphere(qobj,radius,slices,stacks);
} 