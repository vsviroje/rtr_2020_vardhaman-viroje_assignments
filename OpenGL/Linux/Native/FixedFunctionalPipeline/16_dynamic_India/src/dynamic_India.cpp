#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<memory.h>
#include<math.h>

#include<GL/gl.h>
#include<GL/glu.h>
#include<GL/glx.h>

#include<X11/Xlib.h>
#include<X11/Xutil.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>
/*
//OpenAL
#include<AL/al.h>
#include<AL/alc.h>
#include"CWaves.h" //Change the path
*/

using namespace std;

bool bFullscreen=false;
Display *gpDisplay=NULL;
XVisualInfo *gpXVisualInfo=NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth=800;
int giWindowHeight=600;

GLXContext gGLXContext;

GLboolean PlaneSceen1stPart=GL_FALSE;
GLboolean PlaneSceen2ndPart=GL_FALSE;

GLboolean FirstISceenPart=GL_TRUE;
GLboolean NSceenPart=GL_FALSE;
GLboolean DSceenPart=GL_FALSE;
GLboolean SecondISceenPart=GL_FALSE;
GLboolean ASceenPart=GL_FALSE;
GLboolean PlaneSceenPart=GL_FALSE;


/*---OpenAL Configuration
	static ALCdevice *g_alcDevice=NULL;
	static const ALCchar *gDefaultALCDeviceName=NULL;
	static ALCcontext *g_alcContext=NULL;

	class Sound{
		public:
			CWaves cWaves;
			WAVEID waveID;
			ALuint Source,Buffer;
			ALCenum error;
			ALint	iDataSize, iFrequency;
			ALenum	eBufferFormat;
			ALchar	*pData;
			const char *szFileName;

		void Initialize(const char *sz_File_Name);
		void DeInitialize(void);
	};

	void Sound::Initialize(const char *sz_File_Name)
	{
		if (sz_File_Name==NULL)
		{
			fprintf(gpFile, "\nFailed to get sound file name");
			DestroyWindow(ghwnd);
		}

		szFileName=sz_File_Name;

		alGenSources((ALuint)1,&Source);
		alSourcef(Source,AL_GAIN,1);
		
		if ((error = alGetError()) != AL_NO_ERROR) 
		{
			fprintf(gpFile, "\nalSourcef() failed to Failed to set volume");
			DestroyWindow(ghwnd);
		}

		alSource3f(Source,AL_POSITION,0.0,0.0,0.0);
		
		alGenBuffers((ALuint)1,&Buffer);

		cWaves.LoadWaveFile(szFileName,&waveID);
		cWaves.GetWaveSize(waveID, (unsigned long*)&iDataSize);
		cWaves.GetWaveData(waveID, (void**)&pData);
		cWaves.GetWaveFrequency(waveID, (unsigned long*)&iFrequency);
		cWaves.GetWaveALBufferFormat(waveID, &alGetEnumValue, (unsigned long*)&eBufferFormat);
					
		alBufferData(Buffer,eBufferFormat,pData,iDataSize,iFrequency);

		alSourcei(Source,AL_BUFFER,Buffer);

		cWaves.DeleteWaveFile(waveID);
	}
	void Sound::DeInitialize(void)
	{
		alDeleteSources(1, &Source);
		alDeleteBuffers(1, &Buffer);
	}

	Sound dynamicIndiaMusic;

*/	

int main(void)
{
	void CreateWindow(void);
	void ToggleFullscreen(void);
	void UnInitialize(void);

	void Initialize(void);
	void Draw(void);
	void Resize(int,int);
	void Update(void);

	bool bDone=false;
	int winWidth,winHeight;

	CreateWindow();
	Initialize();

	XEvent event;
	KeySym keysym;

	while(bDone==false)
	{
		while(XPending(gpDisplay))
		{
			XNextEvent(gpDisplay,&event);
			switch(event.type)
			{
				case MapNotify:
					break;
				case KeyPress:
					keysym=XkbKeycodeToKeysym(gpDisplay,
												event.xkey.keycode,
												0,
												0);
					switch(keysym)
					{
						case XK_Escape:
							bDone=true;
							break;
						case XK_F:
						case XK_f:
							if(bFullscreen==false)
							{
								ToggleFullscreen();
								bFullscreen=true;
							}
							else
							{
								ToggleFullscreen();
								bFullscreen=false;
							}
							break;
						default:
							break;
					}
					break;

				case ButtonPressMask:
					switch(event.xbutton.button)
					{
						case 1:
						case 2:
						case 3:
						default:
							break;
					}
					break;
				case MotionNotify:
					break;
				case ConfigureNotify:
					winWidth=event.xconfigure.width;
					winHeight=event.xconfigure.height;
					Resize(winWidth,winHeight);
					break;

				case Expose:
					break;
				case 33:
					bDone=true;
					break;
				default:
					break;
			}
		}
		Draw();
		Update();
	}
	UnInitialize();
	return(0);
}

void CreateWindow(void)
{
	void UnInitialize(void);
	XSetWindowAttributes winAttribs;
	int defaultScreen;
	int styleMask;

	static int FrameBufferAttributes[]={GLX_DOUBLEBUFFER,True,
										GLX_RGBA,
										GLX_RED_SIZE,8,
										GLX_GREEN_SIZE,8,
										GLX_BLUE_SIZE,8,
										GLX_ALPHA_SIZE,8,
										None};

	gpDisplay=XOpenDisplay(NULL);
	if(gpDisplay==NULL)
	{
		printf("\nERROR:Unable to Open X Display.\n");
		UnInitialize();
		exit(1);
	}

	defaultScreen=XDefaultScreen(gpDisplay);

	gpXVisualInfo=(XVisualInfo *)malloc(sizeof(XVisualInfo));
	if(gpXVisualInfo==NULL)
	{
		printf("\nERROR:Unable to allocate visual info.\n");
		UnInitialize();
		exit(1);
	}

	gpXVisualInfo=glXChooseVisual(	gpDisplay,
									defaultScreen,
									FrameBufferAttributes);

	winAttribs.border_pixel=0;
	winAttribs.background_pixmap=0;
	winAttribs.colormap=XCreateColormap(gpDisplay,
										RootWindow(gpDisplay,
													gpXVisualInfo->screen),
													gpXVisualInfo->visual,
													AllocNone);
	gColormap=winAttribs.colormap;
	winAttribs.background_pixel=BlackPixel(gpDisplay,
											defaultScreen);

	winAttribs.event_mask=ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask ;
	styleMask=CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

	gWindow=XCreateWindow(	gpDisplay,
							RootWindow(gpDisplay,
										gpXVisualInfo->screen),
							0,
							0,
							giWindowWidth,
							giWindowHeight,
							0,
							gpXVisualInfo->depth,
							InputOutput,
							gpXVisualInfo->visual,
							styleMask,
							&winAttribs);
	if(!gWindow)
	{
		printf("\nERROR:Failed to create main window\n");
		UnInitialize();
		exit(1);
	}

	XStoreName(gpDisplay,
				gWindow,
				"XWindow-Dynamic India");

	Atom windowManagerDelete=XInternAtom(gpDisplay,
										"WM_DELETE_WINDOW",
										True);

	XSetWMProtocols(gpDisplay,
					gWindow,
					&windowManagerDelete,
					1);

	XMapWindow(gpDisplay,
				gWindow);
}

void ToggleFullscreen(void)
{
	Atom wm_state;
	Atom fullScreen;
	XEvent xev={0};

	wm_state=XInternAtom(gpDisplay,
						"_NET_WM_STATE",
						False);

	xev.type=ClientMessage;
	xev.xclient.window=gWindow;
	xev.xclient.message_type=wm_state;
	xev.xclient.format=32;
	xev.xclient.data.l[0]=bFullscreen?0:1;

	fullScreen=XInternAtom(gpDisplay,
							"_NET_WM_STATE_FULLSCREEN",
							False);
	
	xev.xclient.data.l[1]=fullScreen;
	XSendEvent(gpDisplay,
				RootWindow(gpDisplay,
							gpXVisualInfo->screen),
				False,
				StructureNotifyMask,
				&xev);
}

void Initialize(void)
{
	void Resize(int,int);
	void UnInitialize(void);

	gGLXContext=glXCreateContext(gpDisplay,
								gpXVisualInfo,
								NULL,
								GL_TRUE);

	if(gGLXContext==NULL)
	{
		printf("\nERROR:Unable to create Rendering Context\n");
		UnInitialize();
		exit(1);
	}

	glXMakeCurrent(gpDisplay,
					gWindow,
					gGLXContext);

	glClearColor(0.0f,0.0f,0.0f,0.0f);

/*
	//Initailize OpenAL
		//Step1:Get Default Device Name
			gDefaultALCDeviceName=alcGetString(NULL,ALC_DEFAULT_DEVICE_SPECIFIER);
		//Step2:Open Default Device
			g_alcDevice=alcOpenDevice(gDefaultALCDeviceName);
			//Check Failed to get Device
			if(g_alcDevice==NULL)
			{
				printf("\nalcOpenDevice() failed to open the ALC device by name :%s",gDefaultALCDeviceName);
				UnInitialize();
				exit(1);
			}
		//Step3:Create Context to OpenGL
			g_alcContext=alcCreateContext(g_alcDevice,NULL);
			if(g_alcContext==NULL)
			{
				printf("\nalcCreateContext() failed to create the ALC context");
				UnInitialize();
				exit(1);
			}
		//Step4:Update the ALC context to current Context
			if(!alcMakeContextCurrent(g_alcContext))
			{
				printf("\nalcMakeContextCurrent() failed to update the ALC context to current context");
				UnInitialize();
				exit(1);
			}
		//Step5:Initialize all sound
			dynamicIndiaMusic.Initialize("music.wav");
	//

	//Play Audio
		alSourcePlay(dynamicIndiaMusic.Source);
*/

	Resize(giWindowWidth,giWindowHeight);

}

void Resize(int width,int height)
{
	if(height==0)
	{
		height=1;
	}

	glViewport(	0,
				0,
				(GLsizei)width,
				(GLsizei)height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f,
					(GLfloat)width/(GLfloat)height,
					0.1f,
					100.0f);
}

GLfloat First_I_x=-6.5f;
GLfloat N_y=4.0f;
GLfloat Second_I_y=-4.0f;
GLfloat A_x=6.5f;

GLfloat AngleArcLeftTop=180.0f;
GLfloat AngleArcLeftBottom=180.0f;
GLfloat AngleArcRightTop=270.0f;
GLfloat AngleArcRightBottom=90.0f;

GLfloat Plane_Middle_x=-8.0f;

GLfloat Green[]={0.0f,0.0f,0.0f};
GLfloat Orange[]={0.0f,0.0f,0.0f};

void Draw(void)
{
	void Display_I(void);
	void Display_N(void);
	void Display_D(void);
	void Display_A(void);
	void Plane2(void);
	void ArcLeftTop(void);
	void ArcLeftBottom(void);
	void ArcRightTop(void);
	void ArcRightBottom(void);

	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	
	if (FirstISceenPart){
		glLoadIdentity();
		glTranslatef(0.0f,0.0f,-10.0f);

			glTranslatef(First_I_x,0.0f,0.0f);
			Display_I();
	}

	if (NSceenPart){
		glLoadIdentity();
		glTranslatef(0.0f,0.0f,-10.0f);

			glTranslatef(-1.25f,N_y,0.0f);
			Display_N();
	}

	if (DSceenPart){
		glLoadIdentity();
		glTranslatef(0.0f,0.0f,-10.0f);

			glTranslatef(0.0f,0.0f,0.0f);
			Display_D();
	}

	if (SecondISceenPart){	
		glLoadIdentity();
		glTranslatef(0.0f,0.0f,-10.0f);

			glTranslatef(1.25f,Second_I_y,0.0f);
			Display_I();
	}

	if (ASceenPart){	
		glLoadIdentity();
		glTranslatef(0.0f,0.0f,-10.0f);

			glTranslatef(A_x,0.0f,0.0f);
			Display_A();
	}
	
	if(PlaneSceenPart)
	{
		if(PlaneSceen1stPart){
			glLoadIdentity();
			glTranslatef(0.0f,0.0f,-10.0f);
				glTranslatef(-4.0f,3.5f,0.0f);
				ArcLeftTop();

			glLoadIdentity();
			glTranslatef(0.0f,0.0f,-10.0f);
				glTranslatef(-4.0f,-3.5f,0.0f);
				ArcLeftBottom();
		}

		if(PlaneSceen2ndPart){
			glLoadIdentity();
			glTranslatef(0.0f,0.0f,-10.0f);
				glTranslatef(4.0f,-3.5f,0.0f);
				ArcRightBottom();

			glLoadIdentity();
			glTranslatef(0.0f,0.0f,-10.0f);
				glTranslatef(4.0f,3.5f,0.0f);
				ArcRightTop();
		}
			
		glLoadIdentity();
		glTranslatef(0.0f,0.0f,-10.0f);
			glTranslatef(Plane_Middle_x,0.0f,0.0f);
			Plane2();
	}

	glXSwapBuffers(gpDisplay,
					gWindow);

}

void UnInitialize(void)
{
	GLXContext currentGLXContext;
	currentGLXContext=glXGetCurrentContext();

	if(currentGLXContext==gGLXContext)
	{
		glXMakeCurrent(gpDisplay,
						0,
						0);
	}

	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay,
							gGLXContext);
	}

	gGLXContext=NULL;
	currentGLXContext=NULL;

	if(gWindow)
	{
		XDestroyWindow(gpDisplay,
						gWindow);
	}

	if(gColormap)
	{
		XFreeColormap(gpDisplay,
						gColormap);
	}

	/*OpenAL DeInitialize
		dynamicIndiaMusic.DeInitialize();

		alcMakeContextCurrent(NULL);
		alcDestroyContext(g_alcContext);
		alcCloseDevice(g_alcDevice);
	*/
	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
	}
		gpXVisualInfo=NULL;
	
	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
	}
		gpDisplay=NULL;

}


void Update(void){
	if (FirstISceenPart){
		if (First_I_x <= -2.5f)
		{
			First_I_x += 0.01f;
		}else{
			NSceenPart=GL_TRUE;
		}
	}	
	if(NSceenPart){
		if (N_y >=0.0f)
		{
			N_y -= 0.01f;
		}else{
			DSceenPart=GL_TRUE;
		}
	}

	if (SecondISceenPart){
		if (Second_I_y<=0.0f){
			Second_I_y+=0.01f;
		}else{
			ASceenPart=GL_TRUE;
		}
	}

	if(ASceenPart)
	{
		if (A_x >= 2.5f)
		{
			A_x -= 0.01f;
		}else{
			PlaneSceenPart=GL_TRUE;
		}
	}

	//GREEN-0.0f, 1.0f, 0.0f
	//Orange-1.0f, 0.5f, 0.0f
	if(DSceenPart){
		if (Orange[0] <= 1.0f)
		{
			Orange[0]+=0.02;
		}
		if (Orange[1] <= 0.5f)
		{
			Orange[1]+=0.02;
		}
		if (Green[1] <= 1.0f)
		{
			Green[1] +=0.02;
		}else{
			SecondISceenPart=GL_TRUE;
		}
	}
	if(PlaneSceenPart)
	{
		if (AngleArcLeftTop<=270.0f)
		{
			AngleArcLeftTop+=0.1f;
		}

		if (AngleArcLeftBottom>=90.0f)
		{
			AngleArcLeftBottom-=0.1f;
		}

		if(Plane_Middle_x >= 4.0f) {
			
			PlaneSceen2ndPart=GL_TRUE;

			if (AngleArcRightTop<=360.0f)
			{
				AngleArcRightTop+=0.1f;
			}

			if (AngleArcRightBottom>=0.0f)
			{
				AngleArcRightBottom-=0.1f;
			}
		}
		
		if(Plane_Middle_x >= -4.0f) {
			PlaneSceen1stPart=GL_FALSE;	
		}else{
			PlaneSceen1stPart=GL_TRUE;
		}

		if (Plane_Middle_x<=8.0f)
		{
			Plane_Middle_x+=0.00446f;
		}
	}
}


void Display_I(void)
{
	glBegin(GL_QUADS);

		glColor3f(1.0f, 0.5f, 0.0f);
			glVertex3f(0.5f, 0.5f, 0.0f);
			glVertex3f(-0.5f, 0.5f, 0.0f);
			glVertex3f(-0.5f, 0.3f, 0.0f);
			glVertex3f(0.5f, 0.3f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

		glColor3f(1.0f, 0.5f, 0.0f);
			glVertex3f(0.1f, 0.3f, 0.0f);
			glVertex3f(-0.1f, 0.3f, 0.0f);
		glColor3f(0.0f, 1.0f, 0.0f);
			glVertex3f(-0.1f, -0.3f, 0.0f);
			glVertex3f(0.1f, -0.3f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

		glColor3f(0.0f, 1.0f, 0.0f);
			glVertex3f(0.5f, -0.3f, 0.0f);
			glVertex3f(-0.5f, -0.3f, 0.0f);
			glVertex3f(-0.5f, -0.5f, 0.0f);
			glVertex3f(0.5f, -0.5f, 0.0f);

	glEnd();
}

void Display_N(void)
{
	glBegin(GL_QUADS);

		glColor3f(1.0f, 0.5f, 0.0f);
			glVertex3f(-0.3f, 0.5f, 0.0f);
			glVertex3f(-0.5f, 0.5f, 0.0f);
		glColor3f(0.0f, 1.0f, 0.0f);		
			glVertex3f(-0.5f, -0.5f, 0.0f);
			glVertex3f(-0.3f, -0.5f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

		glColor3f(1.0f, 0.5f, 0.0f);
			glVertex3f(0.5f, 0.5f, 0.0f);
			glVertex3f(0.3f, 0.5f, 0.0f);
		glColor3f(0.0f, 1.0f, 0.0f);		
			glVertex3f(0.3f, -0.5f, 0.0f);
			glVertex3f(0.5f, -0.5f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

		glColor3f(0.0f, 1.0f, 0.0f);		
			glVertex3f(0.3f, -0.3f, 0.0f);
		glColor3f(1.0f, 0.5f, 0.0f);
			glVertex3f(-0.3f, 0.5f, 0.0f);
			glVertex3f(-0.3f, 0.3f, 0.0f);
		glColor3f(0.0f, 1.0f, 0.0f);		
			glVertex3f(0.3f, -0.5f, 0.0f);

	glEnd();
}

void Display_D(void)
{
	glBegin(GL_QUADS);

		glColor3fv(Orange);
			glVertex3f(-0.3f, 0.5f, 0.0f);
			glVertex3f(-0.5f, 0.5f, 0.0f);
		glColor3fv(Green);		
			glVertex3f(-0.5f, -0.5f, 0.0f);
			glVertex3f(-0.3f, -0.5f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

		glColor3fv(Orange);
			glVertex3f(0.2, 0.5f, 0.0f);
			glVertex3f(-0.3f, 0.5f, 0.0f);
			glVertex3f(-0.3f, 0.3f, 0.0f);
			glVertex3f(0.1f, 0.3f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

		glColor3fv(Green);		
			glVertex3f(0.1f, -0.3f, 0.0f);
			glVertex3f(-0.3f, -0.3f, 0.0f);
			glVertex3f(-0.3f, -0.5f, 0.0f);
			glVertex3f(0.2f, -0.5f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

		glColor3fv(Orange);
			glVertex3f(0.4f, 0.3f, 0.0f);
			glVertex3f(0.2f, 0.5f, 0.0f);
			glVertex3f(0.1f, 0.3f, 0.0f);
			glVertex3f(0.3f, 0.1f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

		glColor3fv(Green);		
			glVertex3f(0.4f, -0.3f, 0.0f);
			glVertex3f(0.3f, -0.1f, 0.0f);
			glVertex3f(0.1f, -0.3f, 0.0f);
			glVertex3f(0.2f, -0.5f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

		glColor3fv(Green);		
			glVertex3f(0.4f, -0.3f, 0.0f);
		glColor3fv(Orange);
			glVertex3f(0.4f, 0.3f, 0.0f);
			glVertex3f(0.3f, 0.1f, 0.0f);
		glColor3fv(Green);		
			glVertex3f(0.3f, -0.1f, 0.0f);

	glEnd();
}

void Display_A(void)
{
	glBegin(GL_QUADS);

		glColor3f(0.0f, 1.0f, 0.0f);		
			glVertex3f(0.5f, -0.5f, 0.0f);
		glColor3f(1.0f, 0.5f, 0.0f);
			glVertex3f(0.0f, 0.5f, 0.0f);
			glVertex3f(0.0f, 0.3f, 0.0f);
		glColor3f(0.0f, 1.0f, 0.0f);		
			glVertex3f(0.4f, -0.5f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

		glColor3f(0.0f, 1.0f, 0.0f);		
			glVertex3f(-0.4f, -0.5f, 0.0f);
		glColor3f(1.0f, 0.5f, 0.0f);
			glVertex3f(0.0f, 0.3f, 0.0f);
			glVertex3f(0.0f, 0.5f, 0.0f);
		glColor3f(0.0f, 1.0f, 0.0f);		
			glVertex3f(-0.5f, -0.5f, 0.0f);

	glEnd();
	//Flag
	if(Plane_Middle_x >= 2.7f) {
		glBegin(GL_QUADS);

			glColor3f(1.0f, 0.5f, 0.0f);
				glVertex3f(0.19f, -0.07f, 0.0f);
				glVertex3f(0.15f, 0.0f, 0.0f);
				glVertex3f(-0.15f, 0.0f, 0.0f);
				glVertex3f(-0.19f, -0.07f, 0.0f);

		glEnd();

		glBegin(GL_QUADS);

			glColor3f(1.0f, 1.0f, 1.0f);
				glVertex3f(0.22f, -0.13f, 0.0f);
				glVertex3f(0.19f, -0.07f, 0.0f);
				glVertex3f(-0.19f, -0.07f, 0.0f);
				glVertex3f(-0.22f, -0.13f, 0.0f);
		glEnd();

		glBegin(GL_QUADS);

			glColor3f(0.0f, 1.0f, 0.0f);		
				glVertex3f(0.22f, -0.13f, 0.0f);
				glVertex3f(-0.22f, -0.13f, 0.0f);
				glVertex3f(-0.25f, -0.2f, 0.0f);
				glVertex3f(0.25f, -0.2f, 0.0f);

		glEnd();
	}
	
}


void ArcLeftTop(void)
{
	GLfloat x,y,z,r;
	GLdouble pi;
	void Plane2(void);
	pi=3.14159265358;
	r=3.5f;
	
	z=0.0f;
	
	x=r* cos((AngleArcLeftTop/180)*pi);
	y=r* sin((AngleArcLeftTop/180)*pi);

	glTranslatef(x,y,z);
		Plane2();
	
}


void ArcLeftBottom(void)
{
	GLfloat x,y,z,r;
	GLdouble pi;
	void Plane2(void);
	pi=3.14159265358;
	r=3.5f;
	
	z=0.0f;
	
	
	x=r* cos((AngleArcLeftBottom/180)*pi);
	y=r* sin((AngleArcLeftBottom/180)*pi);

	glTranslatef(x,y,z);
		Plane2();
	
}


void ArcRightTop(void)
{
	GLfloat x,y,z,r;
	GLdouble pi;
	void Plane2(void);
	pi=3.14159265358;
	r=3.5f;
	
	z=0.0f;

	
	x=r* cos((AngleArcRightTop/180)*pi);
	y=r* sin((AngleArcRightTop/180)*pi);

	glTranslatef(x,y,z);
		Plane2();
	
}


void ArcRightBottom(void)
{
	GLfloat x,y,z,r;
	GLdouble pi;
	void Plane2(void);
	pi=3.14159265358;
	r=3.5f;
	
	z=0.0f;
	
	
	x=r* cos((AngleArcRightBottom/180)*pi);
	y=r* sin((AngleArcRightBottom/180)*pi);

	glTranslatef(x,y,z);
		Plane2();
	
}

void Plane2(void)
{
	glBegin(GL_TRIANGLES);
		glColor3f(0.0f, 0.0f, 1.0f);
			glVertex3f(0.9f, -0.1f, 0.0f);
			glVertex3f(0.7f, 0.0f, 0.0f);
			glVertex3f(0.7f, -0.2f, 0.0f);
	glEnd();
	glBegin(GL_QUADS);
		glColor3f(0.0f, 0.0f, 1.0f);
			glVertex3f(0.7f, 0.0f, 0.0f);
			glVertex3f(0.4f, 0.1f, 0.0f);
			glVertex3f(0.4f, -0.3f, 0.0f);
			glVertex3f(0.7f, -0.2f, 0.0f);
	glEnd();
	glBegin(GL_POLYGON);
		glColor3f(0.0f, 0.0f, 1.0f);
			glVertex3f(0.4f, -0.3f, 0.0f);
			glVertex3f(0.4f, 0.1f, 0.0f);
			glVertex3f(0.1f, 0.3f, 0.0f);
			glVertex3f(0.15f, -0.05f, 0.0f);
			glVertex3f(0.15f, -0.15f, 0.0f);
			glVertex3f(0.1f, -0.5f, 0.0f);
	glEnd();
	glBegin(GL_QUADS);
		glColor3f(0.0f, 0.0f, 1.0f);
			glVertex3f(0.15f,-0.05f, 0.0f);
			glVertex3f(0.1f, -0.05f, 0.0f);
			glVertex3f(0.1f, -0.15f, 0.0f);
			glVertex3f(0.15f, -0.15f, 0.0f);
	glEnd();

	glBegin(GL_QUADS);
		glColor3f(0.0f, 0.0f, 1.0f);
			glVertex3f(0.1f,-0.2f, 0.0f);
			glVertex3f(0.1f, 0.0f, 0.0f);
			glVertex3f(0.0f, 0.1f, 0.0f);
			glVertex3f(0.0f, -0.3f, 0.0f);
	glEnd();
	//Flag Start
	glBegin(GL_QUADS);
	//Orange
		glColor3f(1.0f, 0.5f, 0.0f);
			glVertex3f(-0.05f,-0.07f, 0.0f);
			glVertex3f(-0.05f, 0.0f, 0.0f);
			glVertex3f(-0.4f, -0.0f, 0.0f);
			glVertex3f(-0.4f, -0.07f, 0.0f);
	glEnd();
	glBegin(GL_QUADS);
	//White
		glColor3f(1.0f, 1.0f, 1.0f);
			glVertex3f(-0.05f,-0.13f, 0.0f);
			glVertex3f(-0.05f, -0.07f, 0.0f);
			glVertex3f(-0.4f, -0.07f, 0.0f);
			glVertex3f(-0.4f, -0.13f, 0.0f);
	glEnd();
	glBegin(GL_QUADS);
	//Green
		glColor3f(0.0f, 1.0f, 0.0f);
			glVertex3f(-0.05f,-0.2f, 0.0f);
			glVertex3f(-0.05f, -0.13f, 0.0f);
			glVertex3f(-0.4f, -0.13f, 0.0f);
			glVertex3f(-0.4f, -0.2f, 0.0f);
	glEnd();
	//Flag end
	glBegin(GL_LINES);
	glColor3f(1.0f, 1.0f, 1.0f);
    	glVertex3f(0.0f,-0.05f, 0.0f);
    	glVertex3f(-0.05f,-0.05f, 0.0f);
	glEnd();
	glBegin(GL_LINES);
	glColor3f(1.0f, 1.0f, 1.0f);
    	glVertex3f(0.0f,-0.15f, 0.0f);
    	glVertex3f(-0.05f,-0.15f, 0.0f);
	glEnd();

	//IAF START
	glBegin(GL_LINES);
	glColor3f(1.0f, 1.0f, 1.0f);
    	glVertex3f(0.35f,-0.05f, 0.0f);
    	glVertex3f(0.25f,-0.05f, 0.0f);

		glVertex3f(0.3f,-0.05f, 0.0f);
    	glVertex3f(0.3f,-0.15f, 0.0f);

		glVertex3f(0.35f,-0.15f, 0.0f);
    	glVertex3f(0.25f,-0.15f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glColor3f(1.0f, 1.0f, 1.0f);
    	glVertex3f(0.5f,-0.15f, 0.0f);
    	glVertex3f(0.45f,-0.05f, 0.0f);

		glVertex3f(0.4f,-0.15f, 0.0f);
    	glVertex3f(0.45f,-0.05f, 0.0f);

		glVertex3f(0.47f,-0.1f, 0.0f);
    	glVertex3f(0.43f,-0.1f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glColor3f(1.0f, 1.0f, 1.0f);
    	glVertex3f(0.65f,-0.05f, 0.0f);
    	glVertex3f(0.55f,-0.05f, 0.0f);

		glVertex3f(0.6f,-0.1f, 0.0f);
    	glVertex3f(0.55f,-0.1f, 0.0f);

		glVertex3f(0.55f,-0.15f, 0.0f);
    	glVertex3f(0.55f,-0.05f, 0.0f);
	glEnd();

	//End
}
