#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<memory.h>

#include<GL/gl.h>
#include<GL/glx.h>
#include<GL/glu.h>

#include<X11/Xlib.h>
#include<X11/Xutil.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>

using namespace std;

bool bFullscreen=false;
Display *gpDisplay=NULL;
XVisualInfo *gpXVisualInfo=NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth=800;
int giWindowHeight=600;

GLXContext gGLXContext;

int gViewPortMode=0;

int main(void)
{
	void CreateWindow(void);
	void ToggleFullscreen(void);
	void UnInitialize(void);

	void Initialize(void);
	void Resize(int,int);
	void Draw(void);

	bool bDone=false;

	int winWidth=giWindowWidth;
	int winHeight=giWindowHeight;

	CreateWindow();
	Initialize();

	XEvent event;
	KeySym keysym;

	while(bDone==false)
	{
		while(XPending(gpDisplay))
		{
			XNextEvent(gpDisplay,&event);
			switch(event.type)
			{
				 case MapNotify:
				 	break;
				 case KeyPress:
				 	
				 	keysym=XkbKeycodeToKeysym(gpDisplay,
				 								event.xkey.keycode,
				 								0,
				 								0);
				 	switch(keysym)
				 	{
				 		case XK_Escape:
				 			bDone=true;
				 			break;
				 		case XK_F:
				 		case XK_f:
				 			if(bFullscreen==false)
				 			{
				 				ToggleFullscreen();
				 				bFullscreen=true;
				 			}else{
				 				ToggleFullscreen();
				 				bFullscreen=false;
				 			}
				 			break;

						case XK_0:
							gViewPortMode=0;
							break;

						case XK_1:
							gViewPortMode=1;
							break;

						case XK_2:
							gViewPortMode=2;
							break;		
						
						case XK_3:
							gViewPortMode=3;
							break;

						case XK_4:
							gViewPortMode=4;
							break;
						
						case XK_5:
							gViewPortMode=5;
							break;

						case XK_6:
							gViewPortMode=6;
							break;

						case XK_7:
							gViewPortMode=7;
							break;

						case XK_8:
							gViewPortMode=8;
							break;

						case XK_9:
							gViewPortMode=9;
							break;
			
				 		default:
				 			break;
				 	}

				 	break;

				 case ButtonPress:
				 	switch(event.xbutton.button)
				 	{
				 		case 1:
				 		case 2:
				 		case 3:
				 		default:
				 			break;
				 	}
				 	break;
				 case MotionNotify:
				 	break;

				 case ConfigureNotify:

				 	winWidth=event.xconfigure.width;
				 	winHeight=event.xconfigure.height;
				 	
				 	giWindowWidth=winWidth;
					giWindowHeight=winHeight;

				 	Resize(winWidth,winHeight);
				 	break;

				 case Expose:
				 	break;

				 case 33:
				 	bDone=true;
				 	break;

				default:
					break;
			}
		}
		Draw();
	}

	UnInitialize();
	return(0);
}

void CreateWindow(void)
{
	void UnInitialize(void);
	XSetWindowAttributes winAttribs;
	int defaultScreen;
	int styleMask;

	static int FrameBufferAttributes[]={GLX_DOUBLEBUFFER,True,
										GLX_RGBA,
										GLX_RED_SIZE,8,
										GLX_GREEN_SIZE,8,
										GLX_BLUE_SIZE,8,
										GLX_ALPHA_SIZE,8,
										None};

	gpDisplay=XOpenDisplay(NULL);
	if(gpDisplay==NULL)
	{
		printf("\nERROR:Unable to Open X Display;\n");
		UnInitialize();
		exit(1);
	}

	defaultScreen=XDefaultScreen(gpDisplay);

	gpXVisualInfo=(XVisualInfo *)malloc(sizeof(XVisualInfo));
	if(gpXVisualInfo==NULL)
	{
		printf("\nERROR:Unalbe to allocate visual info.\n");
		UnInitialize();
		exit(1);
	}

	gpXVisualInfo=glXChooseVisual(	gpDisplay,
									defaultScreen,
									FrameBufferAttributes);

	winAttribs.border_pixel=0;
	winAttribs.background_pixmap=0;
	winAttribs.colormap=XCreateColormap(gpDisplay,
										RootWindow(gpDisplay,
													gpXVisualInfo->screen),
										gpXVisualInfo->visual,
										AllocNone);
	gColormap=winAttribs.colormap;
	winAttribs.background_pixel=BlackPixel(gpDisplay,
											defaultScreen);

	winAttribs.event_mask=ExposureMask |VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;

	styleMask=CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

	gWindow=XCreateWindow(	gpDisplay,
							RootWindow(gpDisplay,
										gpXVisualInfo->screen),
							0,
							0,
							giWindowWidth,
							giWindowHeight,
							0,
							gpXVisualInfo->depth,
							InputOutput,
							gpXVisualInfo->visual,
							styleMask,
							&winAttribs);

	if(!gWindow)
	{
		printf("\nERROR:Failed to create main window\n");
		UnInitialize();
		exit(1);
	}

	XStoreName(gpDisplay,
				gWindow,
				"XWindow-Multi Viewport");

	Atom windowManagerDelete=XInternAtom(gpDisplay,
										"WM_DELETE_WINDOW",
										True);

	XSetWMProtocols(gpDisplay,
					gWindow,
					&windowManagerDelete,
					1);

	XMapWindow(gpDisplay,
				gWindow);
}

void ToggleFullscreen(void)
{
	Atom wm_state;
	Atom fullscreen;
	XEvent xev={0};

	wm_state=XInternAtom(gpDisplay,
						"_NET_WM_STATE",
						False);

	memset(&xev,0,sizeof(xev));

	xev.type=ClientMessage;
	xev.xclient.window=gWindow;
	xev.xclient.message_type=wm_state;
	xev.xclient.format=32;
	xev.xclient.data.l[0]=bFullscreen?0:1;

	fullscreen=XInternAtom(gpDisplay,
							"_NET_WM_STATE_FULLSCREEN",
							False);

	xev.xclient.data.l[1]=fullscreen;

	XSendEvent(gpDisplay,
				RootWindow(gpDisplay,
							gpXVisualInfo->screen),
				False,
				StructureNotifyMask,
				&xev);
}

void Initialize(void)
{
	void Resize(int,int);
	void UnInitialize(void);

	gGLXContext=glXCreateContext(gpDisplay,
								gpXVisualInfo,
								NULL,
								GL_TRUE);

	if(gGLXContext==NULL)
	{
		printf("\nERROR:Unable  to create Rendering Context\n");
		UnInitialize();
		exit(1);
	}

	glXMakeCurrent(gpDisplay,
					gWindow,
					gGLXContext);

	glClearColor(0.0f,0.0f,0.0f,0.0f);
	Resize(giWindowWidth,giWindowHeight);
}

void Resize(int width,int height)
{
	if(height==0)
	{
		height=1;
	}

	glViewport( 0,
				0,
				(GLsizei)width,
				(GLsizei)height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f,
					(GLfloat)width/(GLfloat)height,
					0.1f,
					100.0f);
}

void Draw(void)
{
	int width,height;

	width= giWindowWidth;
	height= giWindowHeight;
	if (height == 0)
	{
		height = 1;
	}
	switch (gViewPortMode)
	{
		case 1:
			glViewport(0, height/2, ((GLsizei)width/2), ((GLsizei)height/2));//1//left-top-corner
			break;
		case 2:
			glViewport(width/2, height/2, ((GLsizei)width/2), ((GLsizei)height/2));//2//right-top-corner
			break;
		case 3:
			glViewport(0, 0, ((GLsizei)width/2), ((GLsizei)height/2));//3//left-Bottom-corner
			break;
		case 4:
			glViewport(width/2, 0, ((GLsizei)width/2), ((GLsizei)height/2));//4//right-Bottom-corne
			break;
		case 5:
			glViewport(0, height/2, ((GLsizei)width), ((GLsizei)height/2));//5//top-half
			break;
		case 6:
			glViewport(0, 0, ((GLsizei)width), ((GLsizei)height/2));//6//Bottom-half
			break;
		case 7:
			glViewport(0, 0, ((GLsizei)width/2), ((GLsizei)height));//7//left-half
			break;
		case 8:
			glViewport(width/2, 0, ((GLsizei)width/2), ((GLsizei)height));//8//righ-thalf
			break;
		case 9:
			glViewport(width/4, height/4, ((GLsizei)width/2), ((GLsizei)height/2));//9//center
			break;

		case 0:
		default:
			glViewport(0, 0, (GLsizei)width, (GLsizei)height);//0//Normal
			break;
	}

	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(0.0f,0.0f,-4.0f);

	glBegin(GL_TRIANGLES);
		glVertex3f(0.0f,1.0f,0.0f);
		glVertex3f(-1.0f,-1.0f,0.0f);
		glVertex3f(1.0f,-1.0f,0.0f);
	glEnd();

	glXSwapBuffers(gpDisplay,
					gWindow);
}

void UnInitialize(void)
{
	GLXContext currentGLXContext;
	currentGLXContext=glXGetCurrentContext();

	if(currentGLXContext==gGLXContext)
	{
		glXMakeCurrent(gpDisplay,
						0,
						0);
	}

	if(gGLXContext)
	{
		glXDestroyContext(	gpDisplay,
							gGLXContext);
	}

	gGLXContext=NULL;
	currentGLXContext=NULL;

	if(gWindow)
	{
		XDestroyWindow(	gpDisplay,
							gWindow);
	}

	if(gColormap)
	{
		XFreeColormap(	gpDisplay,
						gColormap);
	}

	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
	}
		gpXVisualInfo=NULL;

	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
	}
		gpDisplay=NULL;
}