#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<memory.h>

#include<GL/gl.h>
#include<GL/glx.h>
#include<GL/glu.h>

#include<X11/Xlib.h>
#include<X11/Xutil.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>

using namespace std;

bool bFullscreen=false;
Display *gpDisplay=NULL;
XVisualInfo *gpXVisualInfo=NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth=800;
int giWindowHeight=600;

GLXContext gGLXContext;

bool gbLight=false;

GLfloat LightAmbient[]={0.0f,0.0f,0.0f,1.0f};//White Light
GLfloat LightDefuse[]={1.0f,1.0f,1.0f,1.0f};
GLfloat LightPosition[]={0.0f,3.0f,3.0f,0.0f};//directional light like sun

GLfloat LightModelAmbient[]={0.2f,0.2f,0.2f,1.0f};
GLfloat LightModelLocalViewer[]={0.0f};

GLfloat AngleXRotation=0.0f;
GLfloat AngleYRotation=0.0f;
GLfloat AngleZRotation=0.0f;

GLint KeyPressed=0;

GLUquadric *SphereQuadric[24];


int main(void)
{
	void CreateWindow(void);
	void ToggleFullscreen(void);
	void UnInitialize(void);

	void Initialize(void);
	void Resize(int,int);
	void Draw(void);

	bool bDone=false;

	int winWidth,winHeight;

	CreateWindow();
	Initialize();

	XEvent event;
	KeySym keysym;

	while(bDone==false)
	{
		while(XPending(gpDisplay))
		{
			XNextEvent(gpDisplay,
						&event);
			switch(event.type)
			{
				case MapNotify:
					break;
				case KeyPress:
					keysym=XkbKeycodeToKeysym(gpDisplay,
												event.xkey.keycode,
												0,
												0);
					switch(keysym)
					{
						case XK_Escape:
							bDone=true;
							break;
						case XK_F:
						case XK_f:
							if(bFullscreen==false)
							{
								ToggleFullscreen();
								bFullscreen=true;
							}
							else
							{
								ToggleFullscreen();
								bFullscreen=false;
							}
							break;
						case XK_L:
						case XK_l:
							if (gbLight==true){
								glDisable(GL_LIGHTING );
								gbLight=false;
							}else{
								glEnable(GL_LIGHTING);
								gbLight=true;
							}
							break;
						case XK_X:
						case XK_x:
							KeyPressed=1;
							AngleXRotation=0.0f;
							break;
						case XK_Y:
						case XK_y:
							KeyPressed=2;
							AngleYRotation=0.0f;
							break;
						case XK_Z:
						case XK_z:
							KeyPressed=3;
							AngleZRotation=0.0f;
							break;
						default:
							break;
					}
					break;

				case MotionNotify:
					break;
				case ConfigureNotify:
					winWidth=event.xconfigure.width;
					winHeight=event.xconfigure.height;
					Resize(winWidth,winHeight);
					break;
				case Expose:
					break;
				case 33:
					bDone=true;
					break;
				default:
					break;
			}
		}
		Draw();
	}
	UnInitialize();
	return (0);
}

void CreateWindow(void)
{
	void UnInitialize(void);
	XSetWindowAttributes winAttribs;
	int defaultScreen;
	int styleMask;

	static int FrameBufferAttributes[]={GLX_DOUBLEBUFFER,True,
										GLX_RGBA,
										GLX_RED_SIZE,8,
										GLX_GREEN_SIZE,8,
										GLX_BLUE_SIZE,8,
										GLX_ALPHA_SIZE,8,
										GLX_DEPTH_SIZE,24,
										None};

	gpDisplay=XOpenDisplay(NULL);
	if(gpDisplay==NULL)
	{
		printf("\nERROR:Unable to Open X Display\n");
		UnInitialize();
		exit(1);
	}

	defaultScreen=XDefaultScreen(gpDisplay);

	gpXVisualInfo=(XVisualInfo *)malloc(sizeof(XVisualInfo));
	if(gpXVisualInfo==NULL)
	{
		printf("\nERROR:Unable to allocate visual info\n");
		UnInitialize();
		exit(1);
	}

	gpXVisualInfo=glXChooseVisual(gpDisplay,
								defaultScreen,
								FrameBufferAttributes);

	winAttribs.border_pixel=0;
	winAttribs.background_pixmap=0;
	winAttribs.colormap=XCreateColormap(gpDisplay,
										RootWindow(gpDisplay,
													gpXVisualInfo->screen),
										gpXVisualInfo->visual,
										AllocNone);
	gColormap=winAttribs.colormap;
	winAttribs.background_pixel=BlackPixel(gpDisplay,
											defaultScreen);

	winAttribs.event_mask=ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;

	styleMask=CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

	gWindow=XCreateWindow(gpDisplay,
							RootWindow(gpDisplay,
										gpXVisualInfo->screen),
							0,
							0,
							giWindowWidth,
							giWindowHeight,
							0,
							gpXVisualInfo->depth,
							InputOutput,
							gpXVisualInfo->visual,
							styleMask,
							&winAttribs);

	if(!gWindow)
	{
		printf("\nERROR:Failed to create main window\n");
		UnInitialize();
		exit(1);
	}

	XStoreName(gpDisplay,
				gWindow,
				"XWindow-24 Sphere Material");

	Atom windowManagerDelete=XInternAtom(gpDisplay,
										"WM_DELETE_WINDOW",
										True);

	XSetWMProtocols(gpDisplay,
					gWindow,
					&windowManagerDelete,
					1);

	XMapWindow(gpDisplay,
				gWindow);

}

void ToggleFullscreen(void)
{
	Atom wm_state;
	Atom fullScreen;
	XEvent xev={0};

	wm_state=XInternAtom(gpDisplay,
						"_NET_WM_STATE",
						False);

	xev.type=ClientMessage;
	xev.xclient.window=gWindow;
	xev.xclient.message_type=wm_state;
	xev.xclient.format=32;
	xev.xclient.data.l[0]=bFullscreen?0:1;

	fullScreen=XInternAtom(gpDisplay,
							"_NET_WM_STATE_FULLSCREEN",
							False);

	xev.xclient.data.l[1]=fullScreen;
	XSendEvent(gpDisplay,
				RootWindow(gpDisplay,
							gpXVisualInfo->screen),
				False,
				StructureNotifyMask,
				&xev);
}

void Initialize(void)
{
	void Resize(int,int);
	void UnInitialize(void);

	gGLXContext=glXCreateContext(gpDisplay,
								gpXVisualInfo,
								NULL,
								GL_TRUE);

	if(gGLXContext==NULL)
	{
		printf("\nERROR:Unable to create Rendering Context\n");
		UnInitialize();
		exit(1);
	}

	glXMakeCurrent(gpDisplay,
					gWindow,
					gGLXContext);

	glClearColor(0.25f,0.25f,0.25f,0.25f);

	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glShadeModel(GL_SMOOTH);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT,GL_NICEST);

	//Set Light Model
	glEnable(GL_AUTO_NORMAL);
	glEnable(GL_NORMALIZE);
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT,LightModelAmbient);
	glLightModelfv(GL_LIGHT_MODEL_LOCAL_VIEWER,LightModelLocalViewer);

	//Set Lighting
	glLightfv(GL_LIGHT0,GL_AMBIENT,LightAmbient);
	glLightfv(GL_LIGHT0,GL_DIFFUSE,LightDefuse);
	glEnable(GL_LIGHT0);


	//Quadric Initailization
	for(int i=0;i<=24;i++)
	{
		SphereQuadric[i]=gluNewQuadric();
	}

	Resize(giWindowWidth,giWindowHeight);
}

void Resize(int width,int height)
{
	if(height==0)
	{
		height=1;
	}

	glViewport(0,
				0,
				(GLsizei)width,
				(GLsizei)height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	// gluPerspective(45.0f,
	// 				(GLfloat)width/(GLfloat)height,
	// 				0.1f,
	// 				100.0f);

	if(width<=height)
	{
		glOrtho(-10.0f,
				10.0f,
				-10.0f*((GLfloat)height/(GLfloat)width),
				10.0f*((GLfloat)height/(GLfloat)width),
				-100.0f,
				100.0f
				);
	}
	else
	{
		glOrtho(-10.0f*((GLfloat)width/(GLfloat)height),
				10.0f*((GLfloat)width/(GLfloat)height),
				-10.0f,
				10.0f,
				-100.0f,
				100.0f
				);
	}

	// if(width<=height)
	// {
	// 	glOrtho(0.0f,
	// 			15.5f,
	// 			0.0f,
	// 			15.5f*((GLfloat)height/(GLfloat)width),
	// 			-100.0f,
	// 			100.0f
	// 			);
	// }
	// else
	// {
	// 		glOrtho(0.0f,
	// 			15.5f*((GLfloat)width/(GLfloat)height),
	// 			0.0f,
	// 			15.5f,
	// 			-100.0f,
	// 			100.0f
	// 			);
	// }
}

void Draw(void)
{
	void RenderGLUSphere(GLUquadric *,GLdouble,GLint,GLint);
	void Draw24Sphere(void);
	
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	
	if (KeyPressed==1)
	{
		glRotatef(AngleXRotation,1.0f,0.0f,0.0f);
		LightPosition[1]=AngleXRotation;

		LightPosition[0]=0.0f;
		LightPosition[2]=0.0f;

	}
	
	if (KeyPressed==2)
	{
		glRotatef(AngleYRotation,0.0f,1.0f,0.0f);
		LightPosition[2]=AngleYRotation;

		LightPosition[0]=0.0f;
		LightPosition[1]=0.0f;
	}
	
	if (KeyPressed==3)
	{
		glRotatef(AngleZRotation,0.0f,0.0f,1.0f);
		LightPosition[0]=AngleZRotation;
		
		LightPosition[1]=0.0f;
		LightPosition[2]=0.0f;
	}

	glLightfv(GL_LIGHT0,GL_POSITION,LightPosition);

	Draw24Sphere();

	glXSwapBuffers(gpDisplay,
					gWindow);

	
	AngleXRotation += 0.01f;
	AngleYRotation += 0.01f;
	AngleZRotation += 0.01f;
}

void UnInitialize(void)
{
	GLXContext currentGLXContext;

	currentGLXContext=glXGetCurrentContext();

	if(currentGLXContext==gGLXContext)
	{
		glXMakeCurrent(gpDisplay,
						0,
						0);
	}

	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay,
							gGLXContext);
	}

	gGLXContext=NULL;
	currentGLXContext=NULL;

	if(gWindow)
	{
		XDestroyWindow(gpDisplay,
						gWindow);
	}

	if(gColormap)
	{
		XFreeColormap(gpDisplay,
					gColormap);
	}

	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
	}
		gpXVisualInfo=NULL;

	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
	}
		gpDisplay=NULL;


	for(int i=0;i<=24;i++)
	{
		gluDeleteQuadric(SphereQuadric[i]);
		SphereQuadric[i]=NULL;
	}
}


void RenderGLUSphere(GLUquadric *qobj,/*The quadric object*/
   GLdouble   radius,/*radius of sphere*/
   GLint      slices,/*horizontal//longitude*/
   GLint      stacks)/*vertical//latitude*/
{
	gluSphere(qobj,radius,slices,stacks);
} 


void Draw24Sphere(void){
	GLfloat materialAmbient[4];
	GLfloat materilaDefuse[4];
	GLfloat materialSpecular[4];
	GLfloat materialShininess=0.0f;

	//Polygon Mode
	glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);

	//Emerald Sphere
		materialAmbient[0]=0.0215f;
		materialAmbient[1]=0.1745f;
		materialAmbient[2]=0.0215f;
		materialAmbient[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_AMBIENT,materialAmbient);

		materilaDefuse[0]=0.07568f;
		materilaDefuse[1]=0.61424f;
		materilaDefuse[2]=0.07568f;
		materilaDefuse[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_DIFFUSE,materilaDefuse);

		materialSpecular[0]=0.633f;
		materialSpecular[1]=0.727811f;
		materialSpecular[2]=0.633f;
		materialSpecular[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_SPECULAR,materialSpecular);

		materialShininess=0.6f * 128.0f;
		glMaterialf(GL_FRONT,GL_SHININESS,materialShininess);

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(-10.0f,6.0f,-20.0f);

		RenderGLUSphere(SphereQuadric[0],1.0f,30,30);// Normal vector Are calculated by GLU sphere itself we dont need to specify Nprmal vector.

	//Jade Sphere
		materialAmbient[0]=0.135f;
		materialAmbient[1]=0.2225f;
		materialAmbient[2]=0.1575f;
		materialAmbient[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_AMBIENT,materialAmbient);

		materilaDefuse[0]=0.54f;
		materilaDefuse[1]=0.89f;
		materilaDefuse[2]=0.63f;
		materilaDefuse[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_DIFFUSE,materilaDefuse);

		materialSpecular[0]=0.316228f;
		materialSpecular[1]=0.316228f;
		materialSpecular[2]=0.316228f;
		materialSpecular[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_SPECULAR,materialSpecular);

		materialShininess=0.1f * 128.0f;
		glMaterialf(GL_FRONT,GL_SHININESS,materialShininess);

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(-10.0f,3.5f,-20.0f);

		RenderGLUSphere(SphereQuadric[1],1.0f,30,30);// Normal vector Are calculated by GLU sphere itself we dont need to specify Nprmal vector.

	//Obsidian  Sphere
		materialAmbient[0]=0.05375f;
		materialAmbient[1]=0.05f;
		materialAmbient[2]=0.06625f;
		materialAmbient[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_AMBIENT,materialAmbient);

		materilaDefuse[0]=0.18275f;
		materilaDefuse[1]=0.17f;
		materilaDefuse[2]=0.22525f;
		materilaDefuse[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_DIFFUSE,materilaDefuse);

		materialSpecular[0]=0.332741f;
		materialSpecular[1]=0.328634f;
		materialSpecular[2]=0.346435f;
		materialSpecular[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_SPECULAR,materialSpecular);

		materialShininess=0.3f * 128.0f;
		glMaterialf(GL_FRONT,GL_SHININESS,materialShininess);

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(-10.0f,1.0f,-20.0f);

		RenderGLUSphere(SphereQuadric[2],1.0f,30,30);// Normal vector Are calculated by GLU sphere itself we dont need to specify Nprmal vector.

	
	//Pearl Sphere
		materialAmbient[0]=0.25f;
		materialAmbient[1]=0.20725f;
		materialAmbient[2]=0.20725f;
		materialAmbient[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_AMBIENT,materialAmbient);

		materilaDefuse[0]=1.0f;
		materilaDefuse[1]=0.829f;
		materilaDefuse[2]=0.829f;
		materilaDefuse[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_DIFFUSE,materilaDefuse);

		materialSpecular[0]=0.296648f;
		materialSpecular[1]=0.296648f;
		materialSpecular[2]=0.296648f;
		materialSpecular[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_SPECULAR,materialSpecular);

		materialShininess=0.088f * 128.0f;
		glMaterialf(GL_FRONT,GL_SHININESS,materialShininess);

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(-10.0f,-1.5f,-20.0f);

		RenderGLUSphere(SphereQuadric[3],1.0f,30,30);// Normal vector Are calculated by GLU sphere itself we dont need to specify Nprmal vector.

	//Ruby  Sphere
		materialAmbient[0]=0.1745f;
		materialAmbient[1]=0.01175f;
		materialAmbient[2]=0.01175f;
		materialAmbient[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_AMBIENT,materialAmbient);

		materilaDefuse[0]=0.61424f;
		materilaDefuse[1]=0.04136f;
		materilaDefuse[2]=0.04136f;
		materilaDefuse[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_DIFFUSE,materilaDefuse);

		materialSpecular[0]=0.727811f;
		materialSpecular[1]=0.626959f;
		materialSpecular[2]=0.626959f;
		materialSpecular[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_SPECULAR,materialSpecular);

		materialShininess=0.6f * 128.0f;
		glMaterialf(GL_FRONT,GL_SHININESS,materialShininess);

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(-10.0f,-4.0f,-20.0f);

		RenderGLUSphere(SphereQuadric[4],1.0f,30,30);// Normal vector Are calculated by GLU sphere itself we dont need to specify Nprmal vector.

	//Turquoise   Sphere	
		materialAmbient[0]=0.1f;
		materialAmbient[1]=0.18725f;
		materialAmbient[2]=0.1745f;
		materialAmbient[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_AMBIENT,materialAmbient);

		materilaDefuse[0]=0.396f;
		materilaDefuse[1]=0.74151f;
		materilaDefuse[2]=0.69102f;
		materilaDefuse[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_DIFFUSE,materilaDefuse);

		materialSpecular[0]=0.297254f;
		materialSpecular[1]=0.30829f;
		materialSpecular[2]=0.306678f;
		materialSpecular[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_SPECULAR,materialSpecular);

		materialShininess=0.1f * 128.0f;
		glMaterialf(GL_FRONT,GL_SHININESS,materialShininess);

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(-10.0f,-6.5f,-20.0f);

		RenderGLUSphere(SphereQuadric[5],1.0f,30,30);// Normal vector Are calculated by GLU sphere itself we dont need to specify Nprmal vector.

	//Brass Sphere	
		materialAmbient[0]=0.329412f;
		materialAmbient[1]=0.223529f;
		materialAmbient[2]=0.027451f;
		materialAmbient[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_AMBIENT,materialAmbient);

		materilaDefuse[0]=0.780392f;
		materilaDefuse[1]=0.568627f;
		materilaDefuse[2]=0.113725f;
		materilaDefuse[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_DIFFUSE,materilaDefuse);

		materialSpecular[0]=0.992157f;
		materialSpecular[1]=0.941176f;
		materialSpecular[2]=0.807843f;
		materialSpecular[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_SPECULAR,materialSpecular);

		materialShininess=0.21794872f * 128.0f;
		glMaterialf(GL_FRONT,GL_SHININESS,materialShininess);

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(-4.0f,6.0f,-20.0f);

		RenderGLUSphere(SphereQuadric[6],1.0f,30,30);// Normal vector Are calculated by GLU sphere itself we dont need to specify Nprmal vector.

	//Bronze  Sphere
		materialAmbient[0]=0.2125f;
		materialAmbient[1]=0.1275f;
		materialAmbient[2]=0.054f;
		materialAmbient[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_AMBIENT,materialAmbient);

		materilaDefuse[0]=0.714f;
		materilaDefuse[1]=0.4284f;
		materilaDefuse[2]=0.18144f;
		materilaDefuse[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_DIFFUSE,materilaDefuse);

		materialSpecular[0]=0.393548f;
		materialSpecular[1]=0.271906f;
		materialSpecular[2]=0.166721f;
		materialSpecular[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_SPECULAR,materialSpecular);

		materialShininess=0.2f * 128.0f;
		glMaterialf(GL_FRONT,GL_SHININESS,materialShininess);

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(-4.0f,3.5f,-20.0f);

		RenderGLUSphere(SphereQuadric[7],1.0f,30,30);// Normal vector Are calculated by GLU sphere itself we dont need to specify Nprmal vector.
	
	//Chrome  Sphere
		materialAmbient[0]=0.25f;
		materialAmbient[1]=0.25f;
		materialAmbient[2]=0.25f;
		materialAmbient[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_AMBIENT,materialAmbient);

		materilaDefuse[0]=0.4f;
		materilaDefuse[1]=0.4f;
		materilaDefuse[2]=0.4f;
		materilaDefuse[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_DIFFUSE,materilaDefuse);

		materialSpecular[0]=0.774597f;
		materialSpecular[1]=0.774597f;
		materialSpecular[2]=0.774597f;
		materialSpecular[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_SPECULAR,materialSpecular);

		materialShininess=0.6f * 128.0f;
		glMaterialf(GL_FRONT,GL_SHININESS,materialShininess);

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(-4.0f,1.0f,-20.0f);

		RenderGLUSphere(SphereQuadric[8],1.0f,30,30);// Normal vector Are calculated by GLU sphere itself we dont need to specify Nprmal vector.

	//Copper Sphere
		materialAmbient[0]=0.19125f;
		materialAmbient[1]=0.0735f;
		materialAmbient[2]=0.0225f;
		materialAmbient[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_AMBIENT,materialAmbient);

		materilaDefuse[0]=0.7038f;
		materilaDefuse[1]=0.27048f;
		materilaDefuse[2]=0.0828f;
		materilaDefuse[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_DIFFUSE,materilaDefuse);

		materialSpecular[0]=0.256777f;
		materialSpecular[1]=0.137622f;
		materialSpecular[2]=0.086014f;
		materialSpecular[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_SPECULAR,materialSpecular);

		materialShininess=0.1f * 128.0f;
		glMaterialf(GL_FRONT,GL_SHININESS,materialShininess);

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(-4.0f,-1.5f,-20.0f);

		RenderGLUSphere(SphereQuadric[9],1.0f,30,30);// Normal vector Are calculated by GLU sphere itself we dont need to specify Nprmal vector.

	//Gold Sphere
		materialAmbient[0]=0.24725f;
		materialAmbient[1]=0.1995f;
		materialAmbient[2]=0.0745f;
		materialAmbient[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_AMBIENT,materialAmbient);

		materilaDefuse[0]=0.75164f;
		materilaDefuse[1]=0.60648f;
		materilaDefuse[2]=0.22648f;
		materilaDefuse[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_DIFFUSE,materilaDefuse);

		materialSpecular[0]=0.628281f;
		materialSpecular[1]=0.555802f;
		materialSpecular[2]=0.366065f;
		materialSpecular[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_SPECULAR,materialSpecular);

		materialShininess=0.4f * 128.0f;
		glMaterialf(GL_FRONT,GL_SHININESS,materialShininess);

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(-4.0f,-4.0f,-20.0f);

		RenderGLUSphere(SphereQuadric[10],1.0f,30,30);// Normal vector Are calculated by GLU sphere itself we dont need to specify Nprmal vector.

	//Silver Sphere
		materialAmbient[0]=0.19225f;
		materialAmbient[1]=0.19225f;
		materialAmbient[2]=0.19225f;
		materialAmbient[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_AMBIENT,materialAmbient);

		materilaDefuse[0]=0.50754f;
		materilaDefuse[1]=0.50754f;
		materilaDefuse[2]=0.50754f;
		materilaDefuse[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_DIFFUSE,materilaDefuse);

		materialSpecular[0]=0.508273f;
		materialSpecular[1]=0.508273f;
		materialSpecular[2]=0.508273f;
		materialSpecular[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_SPECULAR,materialSpecular);

		materialShininess=0.4f * 128.0f;
		glMaterialf(GL_FRONT,GL_SHININESS,materialShininess);

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(-4.0f,-6.5f,-20.0f);

		RenderGLUSphere(SphereQuadric[11],1.0f,30,30);// Normal vector Are calculated by GLU sphere itself we dont need to specify Nprmal vector.

	//Black  Sphere
		materialAmbient[0]=0.0f;
		materialAmbient[1]=0.0f;
		materialAmbient[2]=0.0f;
		materialAmbient[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_AMBIENT,materialAmbient);

		materilaDefuse[0]=0.01f;
		materilaDefuse[1]=0.01f;
		materilaDefuse[2]=0.01f;
		materilaDefuse[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_DIFFUSE,materilaDefuse);

		materialSpecular[0]=0.50f;
		materialSpecular[1]=0.50f;
		materialSpecular[2]=0.50f;
		materialSpecular[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_SPECULAR,materialSpecular);

		materialShininess=0.25f * 128.0f;
		glMaterialf(GL_FRONT,GL_SHININESS,materialShininess);

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(2.0f,6.0f,-20.0f);

		RenderGLUSphere(SphereQuadric[12],1.0f,30,30);// Normal vector Are calculated by GLU sphere itself we dont need to specify Nprmal vector.

	//Cyan Sphere
		materialAmbient[0]=0.0f;
		materialAmbient[1]=0.1f;
		materialAmbient[2]=0.06f;
		materialAmbient[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_AMBIENT,materialAmbient);

		materilaDefuse[0]=0.0f;
		materilaDefuse[1]=0.50980392f;
		materilaDefuse[2]=0.50980392f;
		materilaDefuse[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_DIFFUSE,materilaDefuse);

		materialSpecular[0]=0.50196078f;
		materialSpecular[1]=0.50196078f;
		materialSpecular[2]=0.50196078f;
		materialSpecular[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_SPECULAR,materialSpecular);

		materialShininess=0.25f * 128.0f;
		glMaterialf(GL_FRONT,GL_SHININESS,materialShininess);

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(2.0f,3.5f,-20.0f);

		RenderGLUSphere(SphereQuadric[13],1.0f,30,30);// Normal vector Are calculated by GLU sphere itself we dont need to specify Nprmal vector.

	//Green Sphere
		materialAmbient[0]=0.0f;
		materialAmbient[1]=0.0f;
		materialAmbient[2]=0.0f;
		materialAmbient[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_AMBIENT,materialAmbient);

		materilaDefuse[0]=0.1f;
		materilaDefuse[1]=0.35f;
		materilaDefuse[2]=0.1f;
		materilaDefuse[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_DIFFUSE,materilaDefuse);

		materialSpecular[0]=0.45f;
		materialSpecular[1]=0.55f;
		materialSpecular[2]=0.45f;
		materialSpecular[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_SPECULAR,materialSpecular);

		materialShininess=0.25f * 128.0f;
		glMaterialf(GL_FRONT,GL_SHININESS,materialShininess);

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(2.0f,1.0f,-20.0f);

		RenderGLUSphere(SphereQuadric[14],1.0f,30,30);// Normal vector Are calculated by GLU sphere itself we dont need to specify Nprmal vector.

	//Red Sphere
		materialAmbient[0]=0.0f;
		materialAmbient[1]=0.0f;
		materialAmbient[2]=0.0f;
		materialAmbient[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_AMBIENT,materialAmbient);

		materilaDefuse[0]=0.5f;
		materilaDefuse[1]=0.0f;
		materilaDefuse[2]=0.0f;
		materilaDefuse[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_DIFFUSE,materilaDefuse);

		materialSpecular[0]=0.7f;
		materialSpecular[1]=0.6f;
		materialSpecular[2]=0.6f;
		materialSpecular[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_SPECULAR,materialSpecular);

		materialShininess=0.25f * 128.0f;
		glMaterialf(GL_FRONT,GL_SHININESS,materialShininess);

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(2.0f,-1.5f,-20.0f);

		RenderGLUSphere(SphereQuadric[15],1.0f,30,30);// Normal vector Are calculated by GLU sphere itself we dont need to specify Nprmal vector.

	//White Sphere
		materialAmbient[0]=0.0f;
		materialAmbient[1]=0.0f;
		materialAmbient[2]=0.0f;
		materialAmbient[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_AMBIENT,materialAmbient);

		materilaDefuse[0]=0.55f;
		materilaDefuse[1]=0.55f;
		materilaDefuse[2]=0.55f;
		materilaDefuse[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_DIFFUSE,materilaDefuse);

		materialSpecular[0]=0.70f;
		materialSpecular[1]=0.70f;
		materialSpecular[2]=0.70f;
		materialSpecular[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_SPECULAR,materialSpecular);

		materialShininess=0.25f * 128.0f;
		glMaterialf(GL_FRONT,GL_SHININESS,materialShininess);

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(2.0f,-4.0f,-20.0f);

		RenderGLUSphere(SphereQuadric[16],1.0f,30,30);// Normal vector Are calculated by GLU sphere itself we dont need to specify Nprmal vector.

	//Yellow Plastic Sphere
		materialAmbient[0]=0.0f;
		materialAmbient[1]=0.0f;
		materialAmbient[2]=0.0f;
		materialAmbient[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_AMBIENT,materialAmbient);

		materilaDefuse[0]=0.5f;
		materilaDefuse[1]=0.5f;
		materilaDefuse[2]=0.0f;
		materilaDefuse[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_DIFFUSE,materilaDefuse);

		materialSpecular[0]=0.60f;
		materialSpecular[1]=0.60f;
		materialSpecular[2]=0.50f;
		materialSpecular[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_SPECULAR,materialSpecular);

		materialShininess=0.25f * 128.0f;
		glMaterialf(GL_FRONT,GL_SHININESS,materialShininess);

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(2.0f,-6.5f,-20.0f);

		RenderGLUSphere(SphereQuadric[17],1.0f,30,30);// Normal vector Are calculated by GLU sphere itself we dont need to specify Nprmal vector.

	//Black Sphere
		materialAmbient[0]=0.02f;
		materialAmbient[1]=0.02f;
		materialAmbient[2]=0.02f;
		materialAmbient[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_AMBIENT,materialAmbient);

		materilaDefuse[0]=0.01f;
		materilaDefuse[1]=0.01f;
		materilaDefuse[2]=0.01f;
		materilaDefuse[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_DIFFUSE,materilaDefuse);

		materialSpecular[0]=0.4f;
		materialSpecular[1]=0.4f;
		materialSpecular[2]=0.4f;
		materialSpecular[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_SPECULAR,materialSpecular);

		materialShininess=0.078125f * 128.0f;
		glMaterialf(GL_FRONT,GL_SHININESS,materialShininess);

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(8.0f,6.0f,-20.0f);

		RenderGLUSphere(SphereQuadric[18],1.0f,30,30);// Normal vector Are calculated by GLU sphere itself we dont need to specify Nprmal vector.

	//Cyan Sphere
		materialAmbient[0]=0.0f;
		materialAmbient[1]=0.05f;
		materialAmbient[2]=0.05f;
		materialAmbient[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_AMBIENT,materialAmbient);

		materilaDefuse[0]=0.4f;
		materilaDefuse[1]=0.5f;
		materilaDefuse[2]=0.5f;
		materilaDefuse[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_DIFFUSE,materilaDefuse);

		materialSpecular[0]=0.04f;
		materialSpecular[1]=0.7f;
		materialSpecular[2]=0.7f;
		materialSpecular[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_SPECULAR,materialSpecular);

		materialShininess=0.078125f * 128.0f;
		glMaterialf(GL_FRONT,GL_SHININESS,materialShininess);

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(8.0f,3.5f,-20.0f);

		RenderGLUSphere(SphereQuadric[19],1.0f,30,30);// Normal vector Are calculated by GLU sphere itself we dont need to specify Nprmal vector.

	//Green Sphere
		materialAmbient[0]=0.0f;
		materialAmbient[1]=0.05f;
		materialAmbient[2]=0.0f;
		materialAmbient[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_AMBIENT,materialAmbient);

		materilaDefuse[0]=0.4f;
		materilaDefuse[1]=0.5f;
		materilaDefuse[2]=0.4f;
		materilaDefuse[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_DIFFUSE,materilaDefuse);

		materialSpecular[0]=0.04f;
		materialSpecular[1]=0.7f;
		materialSpecular[2]=0.047f;
		materialSpecular[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_SPECULAR,materialSpecular);

		materialShininess=0.078125f * 128.0f;
		glMaterialf(GL_FRONT,GL_SHININESS,materialShininess);

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(8.0f,1.0f,-20.0f);

		RenderGLUSphere(SphereQuadric[20],1.0f,30,30);// Normal vector Are calculated by GLU sphere itself we dont need to specify Nprmal vector.

	//Red Sphere
		materialAmbient[0]=0.05f;
		materialAmbient[1]=0.0f;
		materialAmbient[2]=0.0f;
		materialAmbient[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_AMBIENT,materialAmbient);

		materilaDefuse[0]=0.5f;
		materilaDefuse[1]=0.4f;
		materilaDefuse[2]=0.4f;
		materilaDefuse[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_DIFFUSE,materilaDefuse);

		materialSpecular[0]=0.7f;
		materialSpecular[1]=0.04f;
		materialSpecular[2]=0.04f;
		materialSpecular[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_SPECULAR,materialSpecular);

		materialShininess=0.078125f * 128.0f;
		glMaterialf(GL_FRONT,GL_SHININESS,materialShininess);

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(8.0f,-1.5f,-20.0f);

		RenderGLUSphere(SphereQuadric[21],1.0f,30,30);// Normal vector Are calculated by GLU sphere itself we dont need to specify Nprmal vector.

	//White Sphere
		materialAmbient[0]=0.05f;
		materialAmbient[1]=0.05f;
		materialAmbient[2]=0.05f;
		materialAmbient[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_AMBIENT,materialAmbient);

		materilaDefuse[0]=0.5f;
		materilaDefuse[1]=0.5f;
		materilaDefuse[2]=0.5f;
		materilaDefuse[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_DIFFUSE,materilaDefuse);

		materialSpecular[0]=0.7f;
		materialSpecular[1]=0.7f;
		materialSpecular[2]=0.7f;
		materialSpecular[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_SPECULAR,materialSpecular);

		materialShininess=0.078125f * 128.0f;
		glMaterialf(GL_FRONT,GL_SHININESS,materialShininess);

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(8.0f,-4.0f,-20.0f);

		RenderGLUSphere(SphereQuadric[22],1.0f,30,30);// Normal vector Are calculated by GLU sphere itself we dont need to specify Nprmal vector.
	
	//Yellow Sphere
		materialAmbient[0]=0.05f;
		materialAmbient[1]=0.05f;
		materialAmbient[2]=0.0f;
		materialAmbient[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_AMBIENT,materialAmbient);

		materilaDefuse[0]=0.5f;
		materilaDefuse[1]=0.5f;
		materilaDefuse[2]=0.4f;
		materilaDefuse[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_DIFFUSE,materilaDefuse);

		materialSpecular[0]=0.7f;
		materialSpecular[1]=0.7f;
		materialSpecular[2]=0.04f;
		materialSpecular[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_SPECULAR,materialSpecular);

		materialShininess=0.078125f * 128.0f;
		glMaterialf(GL_FRONT,GL_SHININESS,materialShininess);

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(8.0f,-6.5f,-20.0f);

		RenderGLUSphere(SphereQuadric[23],1.0f,30,30);// Normal vector Are calculated by GLU sphere itself we dont need to specify Nprmal vector.


}
