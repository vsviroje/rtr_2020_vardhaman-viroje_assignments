package com.example.render_to_texture;

import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;

import android.content.Context;
import android.view.MotionEvent;

import android.opengl.GLSurfaceView;
import android.opengl.GLES32;//for opengl embedded system 3.2 version core API support(PP)

import javax.microedition.khronos.opengles.GL10;//OpenGL ES 1.0 for legacy API support(FFP)
import javax.microedition.khronos.egl.EGLConfig;//embedded GL Bridging API to provide bridge between OpenGL ES's NDK and OpenGL ES's SDK 

import android.opengl.Matrix;

//non blocking IO buffer
import java.nio.ByteBuffer;
import java.nio.ByteOrder; // used to set/get bigEndian/littleEndian of byte
import java.nio.FloatBuffer;

import android.graphics.BitmapFactory;
import android.graphics.Bitmap;
import android.opengl.GLUtils;

public class GLESView extends GLSurfaceView implements OnGestureListener,OnDoubleTapListener,GLSurfaceView.Renderer
{
    private GestureDetector gestureDetector;
    private final Context context;//final is similar to constant

    private int gVertexShaderObject_simple;
    private int gFragmentShaderObject_simple;
    private int gShaderProgramObject_simple;

    private int gVertexShaderObject_render_to_texture;
    private int gFragmentShaderObject_render_to_texture;
    private int gShaderProgramObject_render_to_texture;

    private int[] vao_cube =new int[1];

    private int[] vbo_position =new int[1];
    private int[] vbo_texture =new int[1];
    private int[] vbo_color =new int[1];


    private int mvpMatrixUniform_simple;
    private int mvpMatrixUniform_render_to_texture;

    private int textureSamplerUniform_simple;
    private int textureSamplerUniform_render_to_texture;
    private int cube_texture;
    
    private int gWidth, gHeight;

    private int[] gFrameBuffers=new int[1];
    private int[] gColorTexture=new int[1];
    private int[] gDepthTexture=new int[1];

    private float perspectiveProjectionMatrix[]=new float[16]; //4*4 matrix

    private int angle;

    final float greenColor[]=new float[]{
                                            0.0f, 1.0f, 0.0f, 1.0f
                                                    };  
    final float depthValue[]=new float[]{
                                            0.0f, 1.0f, 0.0f, 1.0f
                                                    }; 


    FloatBuffer greenColorFloatBuffer;
    FloatBuffer depthValueFloatBuffer;       

    public GLESView(Context drawingContext)
    {
        super(drawingContext);

        System.out.println("VSV-LOG | GLESView constructor");

        setEGLContextClientVersion(3);// set EGLContext to supporting OpenGL-ES version 3
        
        setRenderer(this);

        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);//just like invalidateRect

        gestureDetector = new GestureDetector(drawingContext,this,null,false);

        gestureDetector.setOnDoubleTapListener(this);

        context=drawingContext;
    }

    //Triggers all gesture and tap events
    @Override 
    public boolean onTouchEvent(MotionEvent event)
    {
        //code
        int eventaction = event.getAction();
        if(!gestureDetector.onTouchEvent(event))
        {
            super.onTouchEvent(event);
        }
        return (true);
    }

    //OnDoubleTapListener interface
    @Override
    public boolean onDoubleTap(MotionEvent e)
    {
        System.out.println("VSV-LOG | Double Tap");

        return true;
    }

    //OnDoubleTapListener interface
    @Override
    public boolean onDoubleTapEvent(MotionEvent e)
    {
        return true;
    }

    //OnDoubleTapListener interface
    @Override
    public boolean onSingleTapConfirmed(MotionEvent e)
    {
        System.out.println("VSV-LOG | Single Tap");

        return true;    
    }

    //OnGestureListener interface
    @Override
    public boolean onDown(MotionEvent e)
    {
        return true;    
    }

    //OnGestureListener interface
    @Override
    public boolean onFling(MotionEvent e1,MotionEvent e2,float velocityX,float velocityY)
    {
        return true;    
    }

    //OnGestureListener interface
    @Override
    public void onLongPress(MotionEvent e)
    {
        System.out.println("VSV-LOG | Long press");
    }

    //OnGestureListener interface
    @Override
    public boolean onScroll(MotionEvent e1,MotionEvent e2,float velocityX,float velocityY)
    {
        System.out.println("VSV-LOG | Scroll");
        uninitialize();
        System.exit(0);
        return true;    
    }

    //OnGestureListener interface
    @Override
    public void onShowPress(MotionEvent e)
    {
    }

    //OnGestureListener interface
    @Override
    public boolean onSingleTapUp(MotionEvent e)
    {
        return true;    
    }

    // GLSurfaceView.Renderer interface
    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config)
    {
        String version = gl.glGetString(GL10.GL_VERSION);
        System.out.println("VSV-LOG | OpenGL-ES Version:-"+ version);

        String glsl_version = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
        System.out.println("VSV-LOG | GLSL Version:-"+ glsl_version);

        initialize(gl);
    }

    // GLSurfaceView.Renderer interface
    @Override
    public void onSurfaceChanged(GL10 unused, int width ,int height)
    {
        gWidth=width; 
        gHeight=height;

        System.out.println("VSV-LOG | onSurfaceChanged"+width +","+height);

        resize(width,height);
    }


    // GLSurfaceView.Renderer interface
    @Override
    public void onDrawFrame(GL10 unused)
    {
        draw();
    }

    private void initialize(GL10 gl)
    {
        int[] iShaderCompiledStatus= new int[1];
        int[] iShaderProgramLinkStatus= new int[1];
        int[] iInfoLogLength = new int[1];
        String szInfoLog=null;

        gVertexShaderObject_simple=GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);

        String vertexShaderSourceCode = String.format(
            "#version 320 es" +
            "\n" +
            "in vec4 vPosition;" +
            "in vec2 vTexCoord;" +
            "out vec2 out_TexCoord;" +
            "uniform mat4 u_mvpMatrix;" +
            "void main(void)" +
            "{" +
                "out_TexCoord=vTexCoord;"+
                "gl_Position=u_mvpMatrix * vPosition;" +
            "}"
        );

        GLES32.glShaderSource(gVertexShaderObject_simple,vertexShaderSourceCode);

        GLES32.glCompileShader(gVertexShaderObject_simple);

        GLES32.glGetShaderiv(gVertexShaderObject_simple,GLES32.GL_COMPILE_STATUS,iShaderCompiledStatus,0);

        if(iShaderCompiledStatus[0]==GLES32.GL_FALSE)
        {
            GLES32.glGetShaderiv(gVertexShaderObject_simple,GLES32.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
            if (iInfoLogLength[0]>0)
            {
                szInfoLog=GLES32.glGetShaderInfoLog(gVertexShaderObject_simple);
                System.out.println("VSV-LOG | Vertex Shader compilation Log="+szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }

        iShaderCompiledStatus[0]=0;
        iInfoLogLength[0]=0;
        szInfoLog=null;

        gFragmentShaderObject_simple=GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);

        String  fragmentShaderSourceCode= String.format(
            "#version 320 es" +
            "\n" +
            "precision highp float;"+
            "in vec2 out_TexCoord;" +
            "out vec4 fragColor;" +
            "uniform highp sampler2D u_texture_sampler;"+
            "void main(void)" +
            "{"+
                "fragColor = texture(u_texture_sampler,out_TexCoord);" +
            "}"
        );

        GLES32.glShaderSource(gFragmentShaderObject_simple,fragmentShaderSourceCode);

        GLES32.glCompileShader(gFragmentShaderObject_simple);

        GLES32.glGetShaderiv(gFragmentShaderObject_simple,GLES32.GL_COMPILE_STATUS,iShaderCompiledStatus,0);

        if(iShaderCompiledStatus[0]==GLES32.GL_FALSE)
        {
            GLES32.glGetShaderiv(gFragmentShaderObject_simple,GLES32.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
            if (iInfoLogLength[0]>0)
            {
                szInfoLog=GLES32.glGetShaderInfoLog(gFragmentShaderObject_simple);
                System.out.println("VSV-LOG | Fragment Shader compilation Log="+szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }

        iShaderCompiledStatus[0]=0;
        iInfoLogLength[0]=0;
        szInfoLog=null;

        gShaderProgramObject_simple=GLES32.glCreateProgram();

        GLES32.glAttachShader(gShaderProgramObject_simple,gVertexShaderObject_simple);
        GLES32.glAttachShader(gShaderProgramObject_simple,gFragmentShaderObject_simple);

        GLES32.glBindAttribLocation(gShaderProgramObject_simple,GLESMacros.VSV_ATTRIBUTE_POSITION,"vPosition");
        GLES32.glBindAttribLocation(gShaderProgramObject_simple,GLESMacros.VSV_ATTRIBUTE_TEXTURE,"vTexCoord");

        GLES32.glLinkProgram(gShaderProgramObject_simple);
        GLES32.glGetProgramiv(gShaderProgramObject_simple,GLES32.GL_LINK_STATUS,iShaderProgramLinkStatus,0);
        if (iShaderProgramLinkStatus[0]==GLES32.GL_FALSE)
        {
             GLES32.glGetShaderiv(gShaderProgramObject_simple,GLES32.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
            if (iInfoLogLength[0]>0)
            {
                szInfoLog=GLES32.glGetShaderInfoLog(gShaderProgramObject_simple);
                System.out.println("VSV-LOG | Shader program link Log="+szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }

        mvpMatrixUniform_simple=GLES32.glGetUniformLocation(gShaderProgramObject_simple,"u_mvpMatrix");
        textureSamplerUniform_simple=GLES32.glGetUniformLocation(gShaderProgramObject_simple,"u_texture_sampler");

        gVertexShaderObject_render_to_texture=GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);

        vertexShaderSourceCode = String.format(
            "#version 320 es" +
            "\n" +
            "in vec4 vPosition;" +
            "in vec2 vTexCoord;" +
            "out vec2 out_TexCoord;" +
            "uniform mat4 u_mvpMatrix;" +
            "void main(void)" +
            "{" +
                "out_TexCoord=vTexCoord;"+
                "gl_Position=u_mvpMatrix * vPosition;" +
            "}"
        );

        GLES32.glShaderSource(gVertexShaderObject_render_to_texture,vertexShaderSourceCode);

        GLES32.glCompileShader(gVertexShaderObject_render_to_texture);

        GLES32.glGetShaderiv(gVertexShaderObject_render_to_texture,GLES32.GL_COMPILE_STATUS,iShaderCompiledStatus,0);

        if(iShaderCompiledStatus[0]==GLES32.GL_FALSE)
        {
            GLES32.glGetShaderiv(gVertexShaderObject_render_to_texture,GLES32.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
            if (iInfoLogLength[0]>0)
            {
                szInfoLog=GLES32.glGetShaderInfoLog(gVertexShaderObject_render_to_texture);
                System.out.println("VSV-LOG | Vertex Shader compilation Log="+szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }

        iShaderCompiledStatus[0]=0;
        iInfoLogLength[0]=0;
        szInfoLog=null;

        gFragmentShaderObject_render_to_texture=GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);

        fragmentShaderSourceCode= String.format(
            "#version 320 es" +
            "\n" +
            "precision highp float;"+
            "in vec2 out_TexCoord;" +
            "out vec4 fragColor;" +
            "uniform highp sampler2D u_texture_sampler;"+
            "void main(void)" +
            "{"+
                "fragColor = texture(u_texture_sampler,out_TexCoord);" +
            "}"
        );

        GLES32.glShaderSource(gFragmentShaderObject_render_to_texture,fragmentShaderSourceCode);

        GLES32.glCompileShader(gFragmentShaderObject_render_to_texture);

        GLES32.glGetShaderiv(gFragmentShaderObject_render_to_texture,GLES32.GL_COMPILE_STATUS,iShaderCompiledStatus,0);

        if(iShaderCompiledStatus[0]==GLES32.GL_FALSE)
        {
            GLES32.glGetShaderiv(gFragmentShaderObject_render_to_texture,GLES32.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
            if (iInfoLogLength[0]>0)
            {
                szInfoLog=GLES32.glGetShaderInfoLog(gFragmentShaderObject_render_to_texture);
                System.out.println("VSV-LOG | Fragment Shader compilation Log="+szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }

        iShaderCompiledStatus[0]=0;
        iInfoLogLength[0]=0;
        szInfoLog=null;

        gShaderProgramObject_render_to_texture=GLES32.glCreateProgram();

        GLES32.glAttachShader(gShaderProgramObject_render_to_texture,gVertexShaderObject_render_to_texture);
        GLES32.glAttachShader(gShaderProgramObject_render_to_texture,gFragmentShaderObject_render_to_texture);

        GLES32.glBindAttribLocation(gShaderProgramObject_render_to_texture,GLESMacros.VSV_ATTRIBUTE_POSITION,"vPosition");
        GLES32.glBindAttribLocation(gShaderProgramObject_render_to_texture,GLESMacros.VSV_ATTRIBUTE_TEXTURE,"vTexCoord");

        GLES32.glLinkProgram(gShaderProgramObject_render_to_texture);
        GLES32.glGetProgramiv(gShaderProgramObject_render_to_texture,GLES32.GL_LINK_STATUS,iShaderProgramLinkStatus,0);
        if (iShaderProgramLinkStatus[0]==GLES32.GL_FALSE)
        {
            GLES32.glGetShaderiv(gShaderProgramObject_render_to_texture,GLES32.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
            if (iInfoLogLength[0]>0)
            {
                szInfoLog=GLES32.glGetShaderInfoLog(gShaderProgramObject_render_to_texture);
                System.out.println("VSV-LOG | Shader program link Log="+szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }

        mvpMatrixUniform_render_to_texture=GLES32.glGetUniformLocation(gShaderProgramObject_render_to_texture,"u_mvpMatrix");
        textureSamplerUniform_render_to_texture=GLES32.glGetUniformLocation(gShaderProgramObject_render_to_texture,"u_texture_sampler");


        final float cubeVertices[]=new float[]{
                                            1.0f, 1.0f, 1.0f,
                                            -1.0f, 1.0f, 1.0f,
                                            -1.0f, -1.0f, 1.0f,
                                            1.0f, -1.0f, 1.0f,

                                            1.0f, 1.0f, -1.0f,
                                            1.0f, 1.0f, 1.0f,
                                            1.0f, -1.0f, 1.0f,
                                            1.0f, -1.0f, -1.0f,

                                            -1.0f, 1.0f, -1.0f,
                                            1.0f, 1.0f, -1.0f,
                                            1.0f, -1.0f, -1.0f,
                                            -1.0f, -1.0f, -1.0f,

                                            -1.0f, 1.0f, 1.0f,
                                            -1.0f, 1.0f, -1.0f,
                                            -1.0f, -1.0f, -1.0f,
                                            -1.0f, -1.0f, 1.0f,

                                            1.0f, 1.0f, -1.0f,
                                            -1.0f, 1.0f, -1.0f,
                                            -1.0f, 1.0f, 1.0f,
                                            1.0f, 1.0f, 1.0f,

                                            1.0f, -1.0f, -1.0f,
                                            -1.0f, -1.0f, -1.0f,
                                            -1.0f, -1.0f, 1.0f,
                                            1.0f, -1.0f, 1.0f
                                                };
        
        final float cubeTexCord[]=new float[]{	
                                                0.0f,0.0f,
                                                1.0f,0.0f,
                                                1.0f,1.0f,
                                                0.0f,1.0f,

                                                1.0f,0.0f,
                                                1.0f,1.0f,
                                                0.0f,1.0f,
                                                0.0f,0.0f,

                                                1.0f,0.0f,
                                                1.0f,1.0f,
                                                0.0f,1.0f,
                                                0.0f,0.0f,

                                                0.0f,0.0f,
                                                1.0f,0.0f,
                                                1.0f,1.0f,
                                                0.0f,1.0f,

                                                0.0f,1.0f,
                                                0.0f,0.0f,
                                                1.0f,0.0f,
                                                1.0f,1.0f,

                                                1.0f,1.0f,
                                                0.0f,1.0f,
                                                0.0f,0.0f,
                                                1.0f,0.0f
                                            };
            
        final float cubeColor[]=new float[]{
                                    1.0f, 0.0f, 0.0f,
                                    1.0f, 0.0f, 0.0f,
                                    1.0f, 0.0f, 0.0f,
                                    1.0f, 0.0f, 0.0f,
                                    
                                    0.0f, 1.0f, 0.0f,
                                    0.0f, 1.0f, 0.0f,
                                    0.0f, 1.0f, 0.0f,
                                    0.0f, 1.0f, 0.0f,

                                    0.0f, 0.0f, 1.0f,
                                    0.0f, 0.0f, 1.0f,
                                    0.0f, 0.0f, 1.0f,
                                    0.0f, 0.0f, 1.0f,

                                    0.0f, 1.0f, 1.0f,
                                    0.0f, 1.0f, 1.0f,
                                    0.0f, 1.0f, 1.0f,
                                    0.0f, 1.0f, 1.0f,

                                    1.0f, 0.0f, 1.0f,
                                    1.0f, 0.0f, 1.0f,
                                    1.0f, 0.0f, 1.0f,
                                    1.0f, 0.0f, 1.0f,

                                    1.0f, 1.0f, 0.0f,
                                    1.0f, 1.0f, 0.0f,
                                    1.0f, 1.0f, 0.0f,
                                    1.0f, 1.0f, 0.0f
                                            };  

        

        ByteBuffer byteBuffer;
        FloatBuffer verticesBuffer;
        FloatBuffer colorBuffer;
        FloatBuffer texCordBuffer;

        GLES32.glGenVertexArrays(1,vao_cube,0);
        GLES32.glBindVertexArray(vao_cube[0]);

            GLES32.glGenBuffers(1,vbo_position,0);
            GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_position[0]);

                byteBuffer=ByteBuffer.allocateDirect(cubeVertices.length * 4);// 4 is size of float
                byteBuffer.order(ByteOrder.nativeOrder());
                verticesBuffer=byteBuffer.asFloatBuffer();

                verticesBuffer.put(cubeVertices);
                verticesBuffer.position(0);

                GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                                    cubeVertices.length * 4, 
                                    verticesBuffer,
                                    GLES32.GL_STATIC_DRAW);

                GLES32.glVertexAttribPointer(GLESMacros.VSV_ATTRIBUTE_POSITION,
                                            3,
                                            GLES32.GL_FLOAT,
                                            false,
                                            0,
                                            0);
                
                GLES32.glEnableVertexAttribArray(GLESMacros.VSV_ATTRIBUTE_POSITION);

            GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);

            GLES32.glGenBuffers(1,vbo_texture,0);
            GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_texture[0]);

                byteBuffer=ByteBuffer.allocateDirect(cubeTexCord.length * 4);// 4 is size of float
                byteBuffer.order(ByteOrder.nativeOrder());
                texCordBuffer=byteBuffer.asFloatBuffer();

                texCordBuffer.put(cubeTexCord);
                texCordBuffer.position(0);

                GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                                    cubeTexCord.length * 4, 
                                    texCordBuffer,
                                    GLES32.GL_STATIC_DRAW);

                GLES32.glVertexAttribPointer(GLESMacros.VSV_ATTRIBUTE_TEXTURE,
                                            2,
                                            GLES32.GL_FLOAT,
                                            false,
                                            0,
                                            0);
                
                GLES32.glEnableVertexAttribArray(GLESMacros.VSV_ATTRIBUTE_TEXTURE);
           
        GLES32.glBindVertexArray(0);
        
        gWidth=2160;
        gHeight=1080;

        GLES32.glGenFramebuffers(1, gFrameBuffers,0);
	    GLES32.glBindFramebuffer(GLES32.GL_FRAMEBUFFER,gFrameBuffers[0]);
            //color 
            GLES32.glGenTextures(1, gColorTexture,0);
            GLES32.glBindTexture(GLES32.GL_TEXTURE_2D,gColorTexture[0]);
            GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_MAG_FILTER, GLES32.GL_LINEAR);
            GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_MIN_FILTER, GLES32.GL_LINEAR);
            GLES32.glTexImage2D(GLES32.GL_TEXTURE_2D,
                            0,
                            GLES32.GL_RGB,
                            gWidth,
                            gHeight,
                            0,
                            GLES32.GL_RGB,
                            GLES32.GL_UNSIGNED_BYTE,
                            null);
            GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, 0);

            //depth
            GLES32.glGenTextures(1, gDepthTexture,0);
            GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, gDepthTexture[0]);

                GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_MAG_FILTER, GLES32.GL_NEAREST);
                GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_MIN_FILTER, GLES32.GL_NEAREST);
                GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_WRAP_S, GLES32.GL_CLAMP_TO_EDGE);
                GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_WRAP_T, GLES32.GL_CLAMP_TO_EDGE);

                GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_COMPARE_FUNC, GLES32.GL_LEQUAL);
                GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_COMPARE_MODE, GLES32.GL_NONE);

                GLES32.glTexImage2D(GLES32.GL_TEXTURE_2D,
                    0,
                    GLES32.GL_DEPTH_COMPONENT,
                    gWidth,
                    gHeight,
                    0,
                    GLES32.GL_DEPTH_COMPONENT,
                    GLES32.GL_UNSIGNED_BYTE,
                    null);
            GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, 0);

            GLES32.glFramebufferTexture(GLES32.GL_FRAMEBUFFER, 
                                GLES32.GL_COLOR_ATTACHMENT0, 
                                gColorTexture[0], 
                                0);

            GLES32.glFramebufferTexture(GLES32.GL_FRAMEBUFFER,
                            GLES32.GL_DEPTH_ATTACHMENT,
                            gDepthTexture[0],
                            0);

        GLES32.glBindFramebuffer(GLES32.GL_FRAMEBUFFER, 0);

        GLES32.glEnable(GLES32.GL_DEPTH_TEST);
        GLES32.glDepthFunc(GLES32.GL_LEQUAL);

        GLES32.glClearColor(0.0f,0.0f,0.0f,1.0f);
	    GLES32.glClearDepthf(1.0f);

        cube_texture=loadGLTexture(R.raw.kundali);

        byteBuffer=ByteBuffer.allocateDirect(greenColor.length * 4);// 4 is size of float
        byteBuffer.order(ByteOrder.nativeOrder());
        greenColorFloatBuffer=byteBuffer.asFloatBuffer();

        greenColorFloatBuffer.put(greenColor);
        greenColorFloatBuffer.position(0);
        //--------
        byteBuffer=ByteBuffer.allocateDirect(depthValue.length * 4);// 4 is size of float
        byteBuffer.order(ByteOrder.nativeOrder());
        depthValueFloatBuffer=byteBuffer.asFloatBuffer();

        depthValueFloatBuffer.put(depthValue);
        depthValueFloatBuffer.position(0);
 
        System.out.println("VSV-LOG |  gWidth, gHeight:-"+  gWidth+","+ gHeight);

        Matrix.setIdentityM(perspectiveProjectionMatrix,0);
    }

    private void resize(int width ,int height)
    {
        GLES32.glViewport(0,0,width,height);
        Matrix.perspectiveM(perspectiveProjectionMatrix,0,
                            45.0f,
                            (float)width/(float)height,
                            0.1f,
                            100.0f);
    }

    public void draw()
    {
        GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);

            float modelViewMatrix[] = new float[16];
            float modelViewProjectionMatrix[] = new float[16];
            float translateMatrix[] = new float[16];
            float rotateMatrix[] = new float[16];
            float scaleMatrix[] = new float[16];

            Matrix.setIdentityM(modelViewMatrix,0);
            Matrix.setIdentityM(modelViewProjectionMatrix,0);
            Matrix.setIdentityM(translateMatrix,0);
            Matrix.setIdentityM(rotateMatrix,0);
            Matrix.setIdentityM(scaleMatrix,0);

        GLES32.glBindFramebuffer(GLES32.GL_FRAMEBUFFER, gFrameBuffers[0]);

		GLES32.glClearBufferfv(GLES32.GL_COLOR, 0, greenColorFloatBuffer);
		GLES32.glClearBufferfv(GLES32.GL_DEPTH, 0, depthValueFloatBuffer);
            GLES32.glUseProgram(gShaderProgramObject_simple);
                Matrix.translateM(translateMatrix,0,0.0f,0.0f,-6.0f);

                Matrix.rotateM(rotateMatrix,0,angle,1.0f,0.0f,0.0f);
                Matrix.rotateM(rotateMatrix,0,angle,0.0f,1.0f,0.0f);            
                Matrix.rotateM(rotateMatrix,0,angle,0.0f,0.0f,1.0f);            

                Matrix.multiplyMM(modelViewMatrix,0,translateMatrix,0,rotateMatrix,0);

                Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);

                GLES32.glUniformMatrix4fv(mvpMatrixUniform_simple,1,false,modelViewProjectionMatrix,0);

                GLES32.glActiveTexture(GLES32.GL_TEXTURE0);
                GLES32.glBindTexture(GLES32.GL_TEXTURE_2D,cube_texture);
                GLES32.glUniform1i(textureSamplerUniform_simple,0);
                
                GLES32.glBindVertexArray(vao_cube[0]);
                    GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN,0,4);
                    GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN,4,4);
                    GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN,8,4);
                    GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN,12,4);
                    GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN,16,4);
                    GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN,20,4);
                GLES32.glBindVertexArray(0);

            GLES32.glUseProgram(0);    
	    GLES32.glBindFramebuffer(GLES32.GL_FRAMEBUFFER, 0);

        Matrix.setIdentityM(modelViewMatrix,0);
        Matrix.setIdentityM(modelViewProjectionMatrix,0);
        Matrix.setIdentityM(translateMatrix,0);
        Matrix.setIdentityM(rotateMatrix,0);
        Matrix.setIdentityM(scaleMatrix,0);

        GLES32.glUseProgram(gShaderProgramObject_render_to_texture);
            Matrix.translateM(translateMatrix,0,0.0f,0.0f,-6.0f);
 
            Matrix.rotateM(rotateMatrix,0,angle,1.0f,0.0f,0.0f);
            Matrix.rotateM(rotateMatrix,0,angle,0.0f,1.0f,0.0f);            
            Matrix.rotateM(rotateMatrix,0,angle,0.0f,0.0f,1.0f);            

            Matrix.multiplyMM(modelViewMatrix,0,translateMatrix,0,rotateMatrix,0);

            Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);

            GLES32.glUniformMatrix4fv(mvpMatrixUniform_simple,1,false,modelViewProjectionMatrix,0);

            GLES32.glActiveTexture(GLES32.GL_TEXTURE0);
            GLES32.glBindTexture(GLES32.GL_TEXTURE_2D,gColorTexture[0]);
            GLES32.glUniform1i(GLES32.glGetUniformLocation(gShaderProgramObject_render_to_texture,"u_texture_sampler"),0);
            
            GLES32.glBindVertexArray(vao_cube[0]);
                GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN,0,4);
                GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN,4,4);
                GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN,8,4);
                GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN,12,4);
                GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN,16,4);
                GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN,20,4);
            GLES32.glBindVertexArray(0);

        GLES32.glUseProgram(0); 
        
        angle+=1;
        requestRender();//like render/flush/swap Double Buffer
    }

    private void uninitialize()
    {
        if(vao_cube[0] != 0)
       {
           GLES32.glDeleteVertexArrays(1,vao_cube,0);
           vao_cube[0]=0;
       }

       if(vbo_position[0]!=0)
       {
           GLES32.glDeleteBuffers(1,vbo_position,0);
           vbo_position[0]=0;
       }

       if(vbo_texture[0]!=0)
       {
           GLES32.glDeleteBuffers(1,vbo_texture,0);
           vbo_texture[0]=0;
       }

        if(gShaderProgramObject_simple != 0 )
        {
            if(gFragmentShaderObject_simple != 0)
            {
                GLES32.glDetachShader(gShaderProgramObject_simple,gFragmentShaderObject_simple);
                GLES32.glDeleteShader(gFragmentShaderObject_simple);
                gFragmentShaderObject_simple=0;
            }
                
            if(gVertexShaderObject_simple != 0)
            {
                GLES32.glDetachShader(gShaderProgramObject_simple,gVertexShaderObject_simple);
                GLES32.glDeleteShader(gVertexShaderObject_simple);
                gVertexShaderObject_simple=0;
            }
            
            GLES32.glDeleteProgram(gShaderProgramObject_simple);
            gShaderProgramObject_simple=0;
        }

         if(gShaderProgramObject_render_to_texture != 0 )
        {
            if(gFragmentShaderObject_render_to_texture != 0)
            {
                GLES32.glDetachShader(gShaderProgramObject_render_to_texture,gFragmentShaderObject_render_to_texture);
                GLES32.glDeleteShader(gFragmentShaderObject_render_to_texture);
                gFragmentShaderObject_render_to_texture=0;
            }
                
            if(gVertexShaderObject_render_to_texture != 0)
            {
                GLES32.glDetachShader(gShaderProgramObject_render_to_texture,gVertexShaderObject_render_to_texture);
                GLES32.glDeleteShader(gVertexShaderObject_render_to_texture);
                gVertexShaderObject_render_to_texture=0;
            }
            
            GLES32.glDeleteProgram(gShaderProgramObject_render_to_texture);
            gShaderProgramObject_render_to_texture=0;
        }
    }

    private int loadGLTexture(int imageFileResourceID)
    {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inScaled=false;
        Bitmap bitmap=BitmapFactory.decodeResource(context.getResources(),imageFileResourceID,options);

        int[] textureID = new int[1];

        GLES32.glPixelStorei( GLES32.GL_UNPACK_ALIGNMENT,1);

		GLES32.glGenTextures(1,textureID,0);

		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D,textureID[0]);

		GLES32.glTexParameteri( GLES32.GL_TEXTURE_2D,
						GLES32.GL_TEXTURE_MAG_FILTER,
						GLES32.GL_LINEAR);

		GLES32.glTexParameteri( GLES32.GL_TEXTURE_2D,
						GLES32.GL_TEXTURE_MIN_FILTER,
						GLES32.GL_LINEAR_MIPMAP_LINEAR);
        
        GLUtils.texImage2D(GLES32.GL_TEXTURE_2D,
                            0,
                            bitmap,
                            0);

		GLES32.glGenerateMipmap(GLES32.GL_TEXTURE_2D);


        return textureID[0];
    }
}