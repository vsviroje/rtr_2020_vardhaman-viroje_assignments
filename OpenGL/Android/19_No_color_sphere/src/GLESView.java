package com.example.no_color_sphere;

import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;

import android.content.Context;
import android.view.MotionEvent;

import android.opengl.GLSurfaceView;
import android.opengl.GLES32;//for opengl embedded system 3.2 version core API support(PP)

import javax.microedition.khronos.opengles.GL10;//OpenGL ES 1.0 for legacy API support(FFP)
import javax.microedition.khronos.egl.EGLConfig;//embedded GL Bridging API to provide bridge between OpenGL ES's NDK and OpenGL ES's SDK 

import android.opengl.Matrix;

//non blocking IO buffer
import java.nio.ByteBuffer;
import java.nio.ByteOrder; // used to set/get bigEndian/littleEndian of byte
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

public class GLESView extends GLSurfaceView implements OnGestureListener,OnDoubleTapListener,GLSurfaceView.Renderer
{
    private GestureDetector gestureDetector;
    private final Context context;//final is similar to constant

    private int vertexShaderObject;
    private int fragmentShaderObject;
    private int shaderProgramObject;

    private int[] vao_sphere =new int[1];

    private int[] vbo_position =new int[1];
    private int[] vbo_normal =new int[1];
    private int[] vbo_element =new int[1];
    private int[] vbo_texCord =new int[1];

    private int mvpUniform;

    private float perspectiveProjectionMatrix[]=new float[16]; //4*4 matrix

    private int angle;

    private int numElements;
    private int numVertices;

    public GLESView(Context drawingContext)
    {
        super(drawingContext);

        System.out.println("VSV-LOG | GLESView constructor");

        setEGLContextClientVersion(3);// set EGLContext to supporting OpenGL-ES version 3
        
        setRenderer(this);

        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);//just like invalidateRect

        gestureDetector = new GestureDetector(drawingContext,this,null,false);

        gestureDetector.setOnDoubleTapListener(this);

        context=drawingContext;
    }

    //Triggers all gesture and tap events
    @Override 
    public boolean onTouchEvent(MotionEvent event)
    {
        //code
        int eventaction = event.getAction();
        if(!gestureDetector.onTouchEvent(event))
        {
            super.onTouchEvent(event);
        }
        return (true);
    }

    //OnDoubleTapListener interface
    @Override
    public boolean onDoubleTap(MotionEvent e)
    {
        System.out.println("VSV-LOG | Double Tap");

        return true;
    }

    //OnDoubleTapListener interface
    @Override
    public boolean onDoubleTapEvent(MotionEvent e)
    {
        return true;
    }

    //OnDoubleTapListener interface
    @Override
    public boolean onSingleTapConfirmed(MotionEvent e)
    {
        System.out.println("VSV-LOG | Single Tap");

        return true;    
    }

    //OnGestureListener interface
    @Override
    public boolean onDown(MotionEvent e)
    {
        return true;    
    }

    //OnGestureListener interface
    @Override
    public boolean onFling(MotionEvent e1,MotionEvent e2,float velocityX,float velocityY)
    {
        return true;    
    }

    //OnGestureListener interface
    @Override
    public void onLongPress(MotionEvent e)
    {
        System.out.println("VSV-LOG | Long press");
    }

    //OnGestureListener interface
    @Override
    public boolean onScroll(MotionEvent e1,MotionEvent e2,float velocityX,float velocityY)
    {
        System.out.println("VSV-LOG | Scroll");
        uninitialize();
        System.exit(0);
        return true;    
    }

    //OnGestureListener interface
    @Override
    public void onShowPress(MotionEvent e)
    {
    }

    //OnGestureListener interface
    @Override
    public boolean onSingleTapUp(MotionEvent e)
    {
        return true;    
    }

    // GLSurfaceView.Renderer interface
    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config)
    {
        String version = gl.glGetString(GL10.GL_VERSION);
        System.out.println("VSV-LOG | OpenGL-ES Version:-"+ version);

        String glsl_version = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
        System.out.println("VSV-LOG | GLSL Version:-"+ glsl_version);

        initialize(gl);
    }

    // GLSurfaceView.Renderer interface
    @Override
    public void onSurfaceChanged(GL10 unused, int width ,int height)
    {
        resize(width,height);
    }


    // GLSurfaceView.Renderer interface
    @Override
    public void onDrawFrame(GL10 unused)
    {
        draw();
    }

    private void initialize(GL10 gl)
    {
        int[] iShaderCompiledStatus= new int[1];
        int[] iShaderProgramLinkStatus= new int[1];
        int[] iInfoLogLength = new int[1];
        String szInfoLog=null;

        vertexShaderObject=GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);

        final String vertexShaderSourceCode = String.format(
            "#version 320 es" +
            "\n" +
            "in vec4 vPosition;" +
            // "in vec4 vColor;" +
		    // "out vec4 out_color;" +
            "uniform mat4 u_mvpMatrix;" +
            "void main(void)" +
            "{" +
                // "out_color=vColor;"+
                "gl_Position=u_mvpMatrix * vPosition;" +
            "}"
        );

        GLES32.glShaderSource(vertexShaderObject,vertexShaderSourceCode);

        GLES32.glCompileShader(vertexShaderObject);

        GLES32.glGetShaderiv(vertexShaderObject,GLES32.GL_COMPILE_STATUS,iShaderCompiledStatus,0);

        if(iShaderCompiledStatus[0]==GLES32.GL_FALSE)
        {
            GLES32.glGetShaderiv(vertexShaderObject,GLES32.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
            if (iInfoLogLength[0]>0)
            {
                szInfoLog=GLES32.glGetShaderInfoLog(vertexShaderObject);
                System.out.println("VSV-LOG | Vertex Shader compilation Log="+szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }

        iShaderCompiledStatus[0]=0;
        iInfoLogLength[0]=0;
        szInfoLog=null;

        fragmentShaderObject=GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);

        final String  fragmentShaderSourceCode= String.format(
            "#version 320 es" +
            "\n" +
            "precision highp float;"+
            "out vec4 fragColor;" +
            // "in vec4 out_color;" +
            "void main(void)" +
            "{"+
                "fragColor = vec4(1.0,1.0,1.0,1.0);" +
            "}"
        );

        GLES32.glShaderSource(fragmentShaderObject,fragmentShaderSourceCode);

        GLES32.glCompileShader(fragmentShaderObject);

        GLES32.glGetShaderiv(fragmentShaderObject,GLES32.GL_COMPILE_STATUS,iShaderCompiledStatus,0);

        if(iShaderCompiledStatus[0]==GLES32.GL_FALSE)
        {
            GLES32.glGetShaderiv(fragmentShaderObject,GLES32.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
            if (iInfoLogLength[0]>0)
            {
                szInfoLog=GLES32.glGetShaderInfoLog(fragmentShaderObject);
                System.out.println("VSV-LOG | Fragment Shader compilation Log="+szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }

        iShaderCompiledStatus[0]=0;
        iInfoLogLength[0]=0;
        szInfoLog=null;

        shaderProgramObject=GLES32.glCreateProgram();

        GLES32.glAttachShader(shaderProgramObject,vertexShaderObject);
        GLES32.glAttachShader(shaderProgramObject,fragmentShaderObject);

        GLES32.glBindAttribLocation(shaderProgramObject,GLESMacros.VSV_ATTRIBUTE_POSITION,"vPosition");
        // GLES32.glBindAttribLocation(shaderProgramObject,GLESMacros.VSV_ATTRIBUTE_COLOR,"vColor");

        GLES32.glLinkProgram(shaderProgramObject);
        GLES32.glGetProgramiv(shaderProgramObject,GLES32.GL_LINK_STATUS,iShaderProgramLinkStatus,0);
        if (iShaderProgramLinkStatus[0]==GLES32.GL_FALSE)
        {
             GLES32.glGetShaderiv(shaderProgramObject,GLES32.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
            if (iInfoLogLength[0]>0)
            {
                szInfoLog=GLES32.glGetShaderInfoLog(shaderProgramObject);
                System.out.println("VSV-LOG | Shader program link Log="+szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }

        mvpUniform=GLES32.glGetUniformLocation(shaderProgramObject,"u_mvpMatrix");

        Sphere sphere=new Sphere();
        float sphere_vertices[]=new float[1146];
        float sphere_normals[]=new float[1146];
        float sphere_textures[]=new float[764];
        short sphere_elements[]=new short[2280];
        sphere.getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
        numVertices = sphere.getNumberOfSphereVertices();
        numElements = sphere.getNumberOfSphereElements();

        ByteBuffer byteBuffer;
        FloatBuffer verticesBuffer;
        FloatBuffer colorBuffer;
        FloatBuffer normalBuffer;
        ShortBuffer elementsBuffer;

        GLES32.glGenVertexArrays(1,vao_sphere,0);
        GLES32.glBindVertexArray(vao_sphere[0]);

            GLES32.glGenBuffers(1,vbo_position,0);
            GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_position[0]);
            
            byteBuffer=ByteBuffer.allocateDirect(sphere_vertices.length * 4);
            byteBuffer.order(ByteOrder.nativeOrder());
            verticesBuffer=byteBuffer.asFloatBuffer();
            verticesBuffer.put(sphere_vertices);
            verticesBuffer.position(0);
            
            GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                                sphere_vertices.length * 4,
                                verticesBuffer,
                                GLES32.GL_STATIC_DRAW);
            
            GLES32.glVertexAttribPointer(GLESMacros.VSV_ATTRIBUTE_POSITION,
                                        3,
                                        GLES32.GL_FLOAT,
                                        false,0,0);
            
            GLES32.glEnableVertexAttribArray(GLESMacros.VSV_ATTRIBUTE_POSITION);
            
            GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);

            GLES32.glGenBuffers(1,vbo_normal,0);
            GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_normal[0]);
            
            byteBuffer=ByteBuffer.allocateDirect(sphere_normals.length * 4);//float size is 4 byte
            byteBuffer.order(ByteOrder.nativeOrder());
            normalBuffer=byteBuffer.asFloatBuffer();
            normalBuffer.put(sphere_normals);
            normalBuffer.position(0);
            
            GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                                sphere_normals.length * 4,
                                normalBuffer,
                                GLES32.GL_STATIC_DRAW);
            
            GLES32.glVertexAttribPointer(GLESMacros.VSV_ATTRIBUTE_NORMAL,
                                        3,
                                        GLES32.GL_FLOAT,
                                        false,0,0);
            
            GLES32.glEnableVertexAttribArray(GLESMacros.VSV_ATTRIBUTE_NORMAL);
            
            GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);

            GLES32.glGenBuffers(1,vbo_element,0);
            GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER,vbo_element[0]);
            
            byteBuffer=ByteBuffer.allocateDirect(sphere_elements.length * 2);// short size is 2 byte
            byteBuffer.order(ByteOrder.nativeOrder());
            elementsBuffer=byteBuffer.asShortBuffer();
            elementsBuffer.put(sphere_elements);
            elementsBuffer.position(0);
            
            GLES32.glBufferData(GLES32.GL_ELEMENT_ARRAY_BUFFER,
                                sphere_elements.length * 2,
                                elementsBuffer,
                                GLES32.GL_STATIC_DRAW);
            
            GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER,0);
        GLES32.glBindVertexArray(0);
        
        GLES32.glEnable(GLES32.GL_DEPTH_TEST);
        GLES32.glDepthFunc(GLES32.GL_LEQUAL);

        GLES32.glClearColor(0.0f,0.0f,0.0f,1.0f);
	    GLES32.glClearDepthf(1.0f);

        Matrix.setIdentityM(perspectiveProjectionMatrix,0);
    }

    private void resize(int width ,int height)
    {
        GLES32.glViewport(0,0,width,height);
        Matrix.perspectiveM(perspectiveProjectionMatrix,0,
                            45.0f,
                            (float)width/(float)height,
                            0.1f,
                            100.0f);
    }

    public void draw()
    {
        GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);

        GLES32.glUseProgram(shaderProgramObject);

            float modelViewMatrix[] = new float[16];
            float modelViewProjectionMatrix[] = new float[16];
            float translateMatrix[] = new float[16];
            float rotateMatrix[] = new float[16];
            float scaleMatrix[] = new float[16];


            Matrix.setIdentityM(modelViewMatrix,0);
            Matrix.setIdentityM(modelViewProjectionMatrix,0);
            Matrix.setIdentityM(translateMatrix,0);
            Matrix.setIdentityM(rotateMatrix,0);

            Matrix.translateM(translateMatrix,0,0.0f,0.0f,-2.0f);
            Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,translateMatrix,0);

            Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);

            GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
            
            GLES32.glBindVertexArray(vao_sphere[0]);
                GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_element[0]);
                GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);                
            GLES32.glBindVertexArray(0);

        GLES32.glUseProgram(0);    
        
        angle+=3;
        requestRender();//like render/flush/swap Double Buffer
    }

    private void uninitialize()
    {
        if(vao_sphere[0] != 0)
       {
           GLES32.glDeleteVertexArrays(1,vao_sphere,0);
           vao_sphere[0]=0;
       }

       if(vbo_position[0]!=0)
       {
           GLES32.glDeleteBuffers(1,vbo_position,0);
           vbo_position[0]=0;
       }

        if(vbo_normal[0]!=0)
       {
           GLES32.glDeleteBuffers(1,vbo_normal,0);
           vbo_normal[0]=0;
       }

        if(vbo_element[0]!=0)
       {
           GLES32.glDeleteBuffers(1,vbo_element,0);
           vbo_element[0]=0;
       }

       if(vbo_texCord[0]!=0)
       {
           GLES32.glDeleteBuffers(1,vbo_texCord,0);
           vbo_texCord[0]=0;
       }

        if(shaderProgramObject != 0 )
        {
            if(fragmentShaderObject != 0)
            {
                GLES32.glDetachShader(shaderProgramObject,fragmentShaderObject);
                GLES32.glDeleteShader(fragmentShaderObject);
                fragmentShaderObject=0;
            }
                
            if(vertexShaderObject != 0)
            {
                GLES32.glDetachShader(shaderProgramObject,vertexShaderObject);
                GLES32.glDeleteShader(vertexShaderObject);
                vertexShaderObject=0;
            }
            
            GLES32.glDeleteProgram(shaderProgramObject);
            shaderProgramObject=0;
        }
    }
}