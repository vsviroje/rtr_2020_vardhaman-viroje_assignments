package com.example.perFrag_perVer_light_sphere;

import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;

import android.content.Context;
import android.view.MotionEvent;

import android.opengl.GLSurfaceView;
import android.opengl.GLES32;//for opengl embedded system 3.2 version core API support(PP)

import javax.microedition.khronos.opengles.GL10;//OpenGL ES 1.0 for legacy API support(FFP)
import javax.microedition.khronos.egl.EGLConfig;//embedded GL Bridging API to provide bridge between OpenGL ES's NDK and OpenGL ES's SDK 

import android.opengl.Matrix;

//non blocking IO buffer
import java.nio.ByteBuffer;
import java.nio.ByteOrder; // used to set/get bigEndian/littleEndian of byte
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

public class GLESView extends GLSurfaceView implements OnGestureListener,OnDoubleTapListener,GLSurfaceView.Renderer
{
    private GestureDetector gestureDetector;
    private final Context context;//final is similar to constant

    private int[] vao_sphere =new int[1];

    private int[] vbo_position =new int[1];
    private int[] vbo_normal =new int[1];
    private int[] vbo_element =new int[1];
    private int[] vbo_texCord =new int[1];

//per fragment
    
    private int PF_vertexShaderObject;
    private int PF_fragmentShaderObject;
    private int PF_shaderProgramObject;

    private int PF_modelMatrixUniform;
    private int PF_viewMatrixUniform;
    private int PF_projectionMatrixUniform;

    private int  PF_lightAmbientUniform;
    private int  PF_lightDiffuseUniform;
    private int  PF_lightSpecularUniform;
    private int  PF_lightPositionUniform;

    private int  PF_materialAmbientUniform;
    private int  PF_materialDiffuseUniform;
    private int  PF_materialSpecularUniform;
    private int  PF_materialShininessUniform;
    private int  PF_LKeyPressedUniform;
//-----------------

//per vertex
    
    private int PV_vertexShaderObject;
    private int PV_fragmentShaderObject;
    private int PV_shaderProgramObject;

    private int PV_modelMatrixUniform;
    private int PV_viewMatrixUniform;
    private int PV_projectionMatrixUniform;

    private int  PV_lightAmbientUniform;
    private int  PV_lightDiffuseUniform;
    private int  PV_lightSpecularUniform;
    private int  PV_lightPositionUniform;

    private int  PV_materialAmbientUniform;
    private int  PV_materialDiffuseUniform;
    private int  PV_materialSpecularUniform;
    private int  PV_materialShininessUniform;
    private int  PV_LKeyPressedUniform;

//-----------------

    private float perspectiveProjectionMatrix[]=new float[16]; //4*4 matrix

    private int angle;

    private int numElements;
    private int numVertices;

    private boolean singleTapTogglePVxOrPF;//false-vertex ,true-fragment
    private boolean doubleTapLights;

    
    final float LightAmbient[] =new float[]{ 0.1f,0.1f,0.1f,1.0f };
    final float LightDefuse[] =new float[] { 1.0f,1.0f,1.0f,1.0f };
    final float LightSpecular[] =new float[] { 1.0f,1.0f,1.0f,1.0f };
    final float LightPosition[] =new float[]{ 100.0f,100.0f,100.0f,1.0f };

    final float materialAmbient[] =new float[] { 0.0f,0.0f,0.0f,1.0f };
    final float materilaDefuse[] =new float[] { 0.5f,0.2f,0.7f,1.0f };
    final float materialSpecular[] =new float[] { 1.0f,1.0f,1.0f,1.0f };
    final float materialShininess = 128.0f;

    public GLESView(Context drawingContext)
    {
        super(drawingContext);

        System.out.println("VSV-LOG | GLESView constructor");

        setEGLContextClientVersion(3);// set EGLContext to supporting OpenGL-ES version 3
        
        setRenderer(this);

        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);//just like invalidateRect

        gestureDetector = new GestureDetector(drawingContext,this,null,false);

        gestureDetector.setOnDoubleTapListener(this);

        context=drawingContext;
    }

    //Triggers all gesture and tap events
    @Override 
    public boolean onTouchEvent(MotionEvent event)
    {
        //code
        int eventaction = event.getAction();
        if(!gestureDetector.onTouchEvent(event))
        {
            super.onTouchEvent(event);
        }
        return (true);
    }

    //OnDoubleTapListener interface
    @Override
    public boolean onDoubleTap(MotionEvent e)
    {
        System.out.println("VSV-LOG | Double Tap");
        doubleTapLights =!doubleTapLights;
        if (doubleTapLights==true)
        {
            singleTapTogglePVxOrPF=false;
        }
        return true;
    }

    //OnDoubleTapListener interface
    @Override
    public boolean onDoubleTapEvent(MotionEvent e)
    {
        return true;
    }

    //OnDoubleTapListener interface
    @Override
    public boolean onSingleTapConfirmed(MotionEvent e)
    {
        System.out.println("VSV-LOG | Single Tap");
        singleTapTogglePVxOrPF= !singleTapTogglePVxOrPF;
        return true;    
    }

    //OnGestureListener interface
    @Override
    public boolean onDown(MotionEvent e)
    {
        return true;    
    }

    //OnGestureListener interface
    @Override
    public boolean onFling(MotionEvent e1,MotionEvent e2,float velocityX,float velocityY)
    {
        return true;    
    }

    //OnGestureListener interface
    @Override
    public void onLongPress(MotionEvent e)
    {
        System.out.println("VSV-LOG | Long press");
    }

    //OnGestureListener interface
    @Override
    public boolean onScroll(MotionEvent e1,MotionEvent e2,float velocityX,float velocityY)
    {
        System.out.println("VSV-LOG | Scroll");
        uninitialize();
        System.exit(0);
        return true;    
    }

    //OnGestureListener interface
    @Override
    public void onShowPress(MotionEvent e)
    {
    }

    //OnGestureListener interface
    @Override
    public boolean onSingleTapUp(MotionEvent e)
    {
        return true;    
    }

    // GLSurfaceView.Renderer interface
    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config)
    {
        String version = gl.glGetString(GL10.GL_VERSION);
        System.out.println("VSV-LOG | OpenGL-ES Version:-"+ version);

        String glsl_version = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
        System.out.println("VSV-LOG | GLSL Version:-"+ glsl_version);

        initialize(gl);
    }

    // GLSurfaceView.Renderer interface
    @Override
    public void onSurfaceChanged(GL10 unused, int width ,int height)
    {
        resize(width,height);
    }


    // GLSurfaceView.Renderer interface
    @Override
    public void onDrawFrame(GL10 unused)
    {
        draw();
    }

    private void initialize(GL10 gl)
    {
        int[] iShaderCompiledStatus= new int[1];
        int[] iShaderProgramLinkStatus= new int[1];
        int[] iInfoLogLength = new int[1];
        String szInfoLog=null;

        PF_vertexShaderObject=GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);

        final String vertexShaderSourceCode = String.format(
            "#version 320 es" +
            "\n" +
            "in vec4 vPosition;\n" +
            "in vec3 vNormal;\n" +
            
            "uniform mat4 u_modelMatrix;\n" +
            "uniform mat4 u_viewMatrix;\n" +
            "uniform mat4 u_projectionMatrix;\n" +

            "uniform int u_lKeyPressed;\n"+

            "uniform vec4 u_lightPosition;\n"+

            "out vec3 out_transformedNormal;\n"+
            "out vec3 out_lightDirection;\n"+
            "out vec3 out_viewVector;\n"+
            
            "void main(void)\n" +
            "{\n" +
                "if (u_lKeyPressed == 1)\n"+
                "{\n"+

                    "vec4 eyeCoord = u_viewMatrix * u_modelMatrix * vPosition;\n"+
                    
                    "out_transformedNormal= normalize( mat3(u_viewMatrix * u_modelMatrix) * vNormal );\n"+

                    "out_lightDirection = normalize( vec3( u_lightPosition - eyeCoord ) );\n"+
                    
                    "out_viewVector = normalize( -eyeCoord.xyz);\n"+
                
                "}\n"+
                
                "gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix * vPosition;\n" +
            "}\n"
        );

        GLES32.glShaderSource(PF_vertexShaderObject,vertexShaderSourceCode);

        GLES32.glCompileShader(PF_vertexShaderObject);

        GLES32.glGetShaderiv(PF_vertexShaderObject,GLES32.GL_COMPILE_STATUS,iShaderCompiledStatus,0);

        if(iShaderCompiledStatus[0]==GLES32.GL_FALSE)
        {
            GLES32.glGetShaderiv(PF_vertexShaderObject,GLES32.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
            if (iInfoLogLength[0]>0)
            {
                szInfoLog=GLES32.glGetShaderInfoLog(PF_vertexShaderObject);
                System.out.println("VSV-LOG | Vertex Shader compilation Log="+szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }

        iShaderCompiledStatus[0]=0;
        iInfoLogLength[0]=0;
        szInfoLog=null;

        PF_fragmentShaderObject=GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);

        final String  fragmentShaderSourceCode= String.format(
            "#version 320 es" +
            "\n" +
            "precision highp float;"+
            "precision highp int;"+
            
            "out vec4 fragColor;"+
            "vec3 phongADS_Light;\n"+

            "in vec3 out_transformedNormal;\n"+
            "in vec3 out_lightDirection;\n"+
            "in vec3 out_viewVector;\n"+

            "uniform vec3 u_lightAmbient;\n"+
            "uniform vec3 u_lightDiffuse;\n"+
            "uniform vec3 u_lightSpecular;\n"+
            
            "uniform vec3 u_materialAmbient;\n"+
            "uniform vec3 u_materialDiffuse;\n"+
            "uniform vec3 u_materialSpecular;\n"+
            "uniform float u_materialShininess;\n"+
            
            "uniform int u_lKeyPressed;\n"+
            
            "void main(void)" +
            "{"+  
                "if (u_lKeyPressed == 1)\n"+
                "{\n"+
                    "vec3 normalizedTransformedNormal = normalize( out_transformedNormal );\n"+
                    "vec3 normalizedLightDirection = normalize( out_lightDirection );\n"+
                    "vec3 normalizedViewVector = normalize( out_viewVector );\n"+

                    "vec3 reflectionVector = reflect( -normalizedLightDirection, normalizedTransformedNormal );\n"+
                    
                    "vec3 ambient = u_lightAmbient * u_materialAmbient;\n"+

                    "vec3 diffuse = u_lightDiffuse * u_materialDiffuse * max( dot(normalizedLightDirection , normalizedTransformedNormal), 0.0);\n"+

                    "vec3 specular = u_lightSpecular * u_materialSpecular * pow( max( dot(reflectionVector , normalizedViewVector), 0.0f) , u_materialShininess);\n"+

                    "phongADS_Light= ambient+ diffuse + specular;\n"+
                    
                "}\n"+
                "else\n"+
                "{\n"+
                    "phongADS_Light=vec3(1.0f, 1.0f, 1.0f);\n"+
                "}\n"+
            
                "fragColor=vec4(phongADS_Light, 1.0f);"+
            "}"
        );

        GLES32.glShaderSource(PF_fragmentShaderObject,fragmentShaderSourceCode);

        GLES32.glCompileShader(PF_fragmentShaderObject);

        GLES32.glGetShaderiv(PF_fragmentShaderObject,GLES32.GL_COMPILE_STATUS,iShaderCompiledStatus,0);

        if(iShaderCompiledStatus[0]==GLES32.GL_FALSE)
        {
            GLES32.glGetShaderiv(PF_fragmentShaderObject,GLES32.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
            if (iInfoLogLength[0]>0)
            {
                szInfoLog=GLES32.glGetShaderInfoLog(PF_fragmentShaderObject);
                System.out.println("VSV-LOG | Fragment Shader compilation Log="+szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }

        iShaderCompiledStatus[0]=0;
        iInfoLogLength[0]=0;
        szInfoLog=null;

        PF_shaderProgramObject=GLES32.glCreateProgram();

        GLES32.glAttachShader(PF_shaderProgramObject,PF_vertexShaderObject);
        GLES32.glAttachShader(PF_shaderProgramObject,PF_fragmentShaderObject);

        GLES32.glBindAttribLocation(PF_shaderProgramObject,GLESMacros.VSV_ATTRIBUTE_POSITION,"vPosition");
        GLES32.glBindAttribLocation(PF_shaderProgramObject,GLESMacros.VSV_ATTRIBUTE_NORMAL,"vNormal");

        GLES32.glLinkProgram(PF_shaderProgramObject);
        GLES32.glGetProgramiv(PF_shaderProgramObject,GLES32.GL_LINK_STATUS,iShaderProgramLinkStatus,0);
        if (iShaderProgramLinkStatus[0]==GLES32.GL_FALSE)
        {
             GLES32.glGetShaderiv(PF_shaderProgramObject,GLES32.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
            if (iInfoLogLength[0]>0)
            {
                szInfoLog=GLES32.glGetShaderInfoLog(PF_shaderProgramObject);
                System.out.println("VSV-LOG | Shader program link Log="+szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }

    	PF_modelMatrixUniform =GLES32.glGetUniformLocation(PF_shaderProgramObject,"u_modelMatrix");
		PF_viewMatrixUniform =GLES32.glGetUniformLocation(PF_shaderProgramObject,"u_viewMatrix");
		PF_projectionMatrixUniform =GLES32.glGetUniformLocation(PF_shaderProgramObject,"u_projectionMatrix");

		PF_lightAmbientUniform =GLES32.glGetUniformLocation(PF_shaderProgramObject,"u_lightAmbient");
		PF_lightDiffuseUniform =GLES32.glGetUniformLocation(PF_shaderProgramObject,"u_lightDiffuse");
		PF_lightSpecularUniform =GLES32.glGetUniformLocation(PF_shaderProgramObject,"u_lightSpecular");
		PF_lightPositionUniform =GLES32.glGetUniformLocation(PF_shaderProgramObject,"u_lightPosition");

		PF_materialAmbientUniform =GLES32.glGetUniformLocation(PF_shaderProgramObject,"u_materialAmbient");
		PF_materialDiffuseUniform =GLES32.glGetUniformLocation(PF_shaderProgramObject,"u_materialDiffuse");
		PF_materialSpecularUniform =GLES32.glGetUniformLocation(PF_shaderProgramObject,"u_materialSpecular");
		PF_materialShininessUniform =GLES32.glGetUniformLocation(PF_shaderProgramObject,"u_materialShininess");

		PF_LKeyPressedUniform =GLES32.glGetUniformLocation(PF_shaderProgramObject,"u_lKeyPressed");

        // vertex

        PV_vertexShaderObject=GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);

        final String vertexShaderSourceCode2 = String.format(
            "#version 320 es" +
            "\n" +
            "in vec4 vPosition;\n" +
            "in vec3 vNormal;\n" +
            
            "uniform mat4 u_modelMatrix;\n" +
            "uniform mat4 u_viewMatrix;\n" +
            "uniform mat4 u_projectionMatrix;\n" +

            "uniform vec3 u_lightAmbient;\n"+
            "uniform vec3 u_lightDiffuse;\n"+
            "uniform vec3 u_lightSpecular;\n"+
            "uniform vec4 u_lightPosition;\n"+
            
            "uniform vec3 u_materialAmbient;\n"+
            "uniform vec3 u_materialDiffuse;\n"+
            "uniform vec3 u_materialSpecular;\n"+
            "uniform float u_materialShininess;\n"+
            
            "uniform int u_lKeyPressed;\n"+
            
            "out vec3 out_phongADS_Light;\n"+

            "precision highp float;"+
            "precision highp int;"+
            
            "void main(void)\n" +
            "{\n" +
                "if (u_lKeyPressed == 1)\n"+
                "{\n"+

                    "vec4 eyeCoord =  u_viewMatrix * u_modelMatrix * vPosition;\n"+
                    
                    "vec3 transformedNormal = normalize( mat3(u_viewMatrix * u_modelMatrix) * vNormal );\n"+

                    "vec3 lightDirection = normalize( vec3( u_lightPosition - eyeCoord ) );\n"+
                    
                    "vec3 reflectionVector = reflect( -lightDirection, transformedNormal );\n"+
                    
                    "vec3 viewVector = normalize( -eyeCoord.xyz);\n"+
                    
                    "vec3 ambient = u_lightAmbient * u_materialAmbient;\n"+
                    
                    "vec3 diffuse = u_lightDiffuse * u_materialDiffuse * max( dot(lightDirection , transformedNormal), 0.0);\n"+
                    
                    "vec3 specular = u_lightSpecular * u_materialSpecular * pow( max( dot(reflectionVector , viewVector), 0.0f) , u_materialShininess);\n"+
                    
                    "out_phongADS_Light= ambient + diffuse + specular;\n"+

                "}\n"+
                "else\n"+
                "{\n"+

                    "out_phongADS_Light = vec3(1.0f, 1.0f, 1.0f);\n"+
                
                "}\n"+
            
                "gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix * vPosition;\n" +
            "}\n"
        );

        GLES32.glShaderSource(PV_vertexShaderObject,vertexShaderSourceCode2);

        GLES32.glCompileShader(PV_vertexShaderObject);

        GLES32.glGetShaderiv(PV_vertexShaderObject,GLES32.GL_COMPILE_STATUS,iShaderCompiledStatus,0);

        if(iShaderCompiledStatus[0]==GLES32.GL_FALSE)
        {
            GLES32.glGetShaderiv(PV_vertexShaderObject,GLES32.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
            if (iInfoLogLength[0]>0)
            {
                szInfoLog=GLES32.glGetShaderInfoLog(PV_vertexShaderObject);
                System.out.println("VSV-LOG | Vertex Shader compilation Log="+szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }

        iShaderCompiledStatus[0]=0;
        iInfoLogLength[0]=0;
        szInfoLog=null;

        PV_fragmentShaderObject=GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);

        final String  fragmentShaderSourceCode2= String.format(
            "#version 320 es" +
            "\n" +
            "precision highp float;"+
            "precision highp int;"+
            
            "out vec4 fragColor;"+
            "in vec3 out_phongADS_Light;\n"+

            "void main(void)" +
            "{"+    
                "fragColor=vec4(out_phongADS_Light, 1.0f);"+
            "}"
        );

        GLES32.glShaderSource(PV_fragmentShaderObject,fragmentShaderSourceCode2);

        GLES32.glCompileShader(PV_fragmentShaderObject);

        GLES32.glGetShaderiv(PV_fragmentShaderObject,GLES32.GL_COMPILE_STATUS,iShaderCompiledStatus,0);

        if(iShaderCompiledStatus[0]==GLES32.GL_FALSE)
        {
            GLES32.glGetShaderiv(PV_fragmentShaderObject,GLES32.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
            if (iInfoLogLength[0]>0)
            {
                szInfoLog=GLES32.glGetShaderInfoLog(PV_fragmentShaderObject);
                System.out.println("VSV-LOG | Fragment Shader compilation Log="+szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }

        iShaderCompiledStatus[0]=0;
        iInfoLogLength[0]=0;
        szInfoLog=null;

        PV_shaderProgramObject=GLES32.glCreateProgram();

        GLES32.glAttachShader(PV_shaderProgramObject,PV_vertexShaderObject);
        GLES32.glAttachShader(PV_shaderProgramObject,PV_fragmentShaderObject);

        GLES32.glBindAttribLocation(PV_shaderProgramObject,GLESMacros.VSV_ATTRIBUTE_POSITION,"vPosition");
        GLES32.glBindAttribLocation(PV_shaderProgramObject,GLESMacros.VSV_ATTRIBUTE_NORMAL,"vNormal");

        GLES32.glLinkProgram(PV_shaderProgramObject);
        GLES32.glGetProgramiv(PV_shaderProgramObject,GLES32.GL_LINK_STATUS,iShaderProgramLinkStatus,0);
        if (iShaderProgramLinkStatus[0]==GLES32.GL_FALSE)
        {
             GLES32.glGetShaderiv(PV_shaderProgramObject,GLES32.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
            if (iInfoLogLength[0]>0)
            {
                szInfoLog=GLES32.glGetShaderInfoLog(PV_shaderProgramObject);
                System.out.println("VSV-LOG | Shader program link Log="+szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }

    	PV_modelMatrixUniform =GLES32.glGetUniformLocation(PV_shaderProgramObject,"u_modelMatrix");
		PV_viewMatrixUniform =GLES32.glGetUniformLocation(PV_shaderProgramObject,"u_viewMatrix");
		PV_projectionMatrixUniform =GLES32.glGetUniformLocation(PV_shaderProgramObject,"u_projectionMatrix");

		PV_lightAmbientUniform =GLES32.glGetUniformLocation(PV_shaderProgramObject,"u_lightAmbient");
		PV_lightDiffuseUniform =GLES32.glGetUniformLocation(PV_shaderProgramObject,"u_lightDiffuse");
		PV_lightSpecularUniform =GLES32.glGetUniformLocation(PV_shaderProgramObject,"u_lightSpecular");
		PV_lightPositionUniform =GLES32.glGetUniformLocation(PV_shaderProgramObject,"u_lightPosition");

		PV_materialAmbientUniform =GLES32.glGetUniformLocation(PV_shaderProgramObject,"u_materialAmbient");
		PV_materialDiffuseUniform =GLES32.glGetUniformLocation(PV_shaderProgramObject,"u_materialDiffuse");
		PV_materialSpecularUniform =GLES32.glGetUniformLocation(PV_shaderProgramObject,"u_materialSpecular");
		PV_materialShininessUniform =GLES32.glGetUniformLocation(PV_shaderProgramObject,"u_materialShininess");

		PV_LKeyPressedUniform =GLES32.glGetUniformLocation(PV_shaderProgramObject,"u_lKeyPressed");


        Sphere sphere=new Sphere();
        float sphere_vertices[]=new float[1146];
        float sphere_normals[]=new float[1146];
        float sphere_textures[]=new float[764];
        short sphere_elements[]=new short[2280];
        sphere.getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
        numVertices = sphere.getNumberOfSphereVertices();
        numElements = sphere.getNumberOfSphereElements();

        ByteBuffer byteBuffer;
        FloatBuffer verticesBuffer;
        FloatBuffer colorBuffer;
        FloatBuffer normalBuffer;
        ShortBuffer elementsBuffer;

        GLES32.glGenVertexArrays(1,vao_sphere,0);
        GLES32.glBindVertexArray(vao_sphere[0]);

            GLES32.glGenBuffers(1,vbo_position,0);
            GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_position[0]);
            
            byteBuffer=ByteBuffer.allocateDirect(sphere_vertices.length * 4);
            byteBuffer.order(ByteOrder.nativeOrder());
            verticesBuffer=byteBuffer.asFloatBuffer();
            verticesBuffer.put(sphere_vertices);
            verticesBuffer.position(0);
            
            GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                                sphere_vertices.length * 4,
                                verticesBuffer,
                                GLES32.GL_STATIC_DRAW);
            
            GLES32.glVertexAttribPointer(GLESMacros.VSV_ATTRIBUTE_POSITION,
                                        3,
                                        GLES32.GL_FLOAT,
                                        false,0,0);
            
            GLES32.glEnableVertexAttribArray(GLESMacros.VSV_ATTRIBUTE_POSITION);
            
            GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);

            GLES32.glGenBuffers(1,vbo_normal,0);
            GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_normal[0]);
            
            byteBuffer=ByteBuffer.allocateDirect(sphere_normals.length * 4);//float size is 4 byte
            byteBuffer.order(ByteOrder.nativeOrder());
            normalBuffer=byteBuffer.asFloatBuffer();
            normalBuffer.put(sphere_normals);
            normalBuffer.position(0);
            
            GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                                sphere_normals.length * 4,
                                normalBuffer,
                                GLES32.GL_STATIC_DRAW);
            
            GLES32.glVertexAttribPointer(GLESMacros.VSV_ATTRIBUTE_NORMAL,
                                        3,
                                        GLES32.GL_FLOAT,
                                        false,0,0);
            
            GLES32.glEnableVertexAttribArray(GLESMacros.VSV_ATTRIBUTE_NORMAL);
            
            GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);

            GLES32.glGenBuffers(1,vbo_element,0);
            GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER,vbo_element[0]);
            
            byteBuffer=ByteBuffer.allocateDirect(sphere_elements.length * 2);// short size is 2 byte
            byteBuffer.order(ByteOrder.nativeOrder());
            elementsBuffer=byteBuffer.asShortBuffer();
            elementsBuffer.put(sphere_elements);
            elementsBuffer.position(0);
            
            GLES32.glBufferData(GLES32.GL_ELEMENT_ARRAY_BUFFER,
                                sphere_elements.length * 2,
                                elementsBuffer,
                                GLES32.GL_STATIC_DRAW);
            
            GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER,0);
        GLES32.glBindVertexArray(0);
        
        GLES32.glEnable(GLES32.GL_DEPTH_TEST);
        GLES32.glDepthFunc(GLES32.GL_LEQUAL);

        GLES32.glClearColor(0.0f,0.0f,0.0f,1.0f);
	    GLES32.glClearDepthf(1.0f);

        Matrix.setIdentityM(perspectiveProjectionMatrix,0);
    }

    private void resize(int width ,int height)
    {
        GLES32.glViewport(0,0,width,height);
        Matrix.perspectiveM(perspectiveProjectionMatrix,0,
                            45.0f,
                            (float)width/(float)height,
                            0.1f,
                            100.0f);
    }

    public void draw()
    {
        float modelMatrix[] = new float[16];
        float viewMatrix[] = new float[16];

        float translateMatrix[] = new float[16];
        float rotateMatrix[] = new float[16];
        float scaleMatrix[] = new float[16];

        GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);
        if (singleTapTogglePVxOrPF==true){
            
            GLES32.glUseProgram(PF_shaderProgramObject);

                Matrix.setIdentityM(modelMatrix,0);
                Matrix.setIdentityM(viewMatrix,0);

                Matrix.setIdentityM(translateMatrix,0);
                Matrix.setIdentityM(rotateMatrix,0);
                Matrix.setIdentityM(scaleMatrix,0);

                Matrix.translateM(translateMatrix,0,0.0f,0.0f,-1.5f);
                Matrix.multiplyMM(modelMatrix,0,modelMatrix,0,translateMatrix,0);
                
                GLES32.glUniformMatrix4fv(PF_modelMatrixUniform,1,false,modelMatrix,0);

                GLES32.glUniformMatrix4fv(PF_viewMatrixUniform,1,false,viewMatrix,0);
                
                GLES32.glUniformMatrix4fv(PF_projectionMatrixUniform,1,false,perspectiveProjectionMatrix,0);

                if (doubleTapLights == true)
                {
                    GLES32.glUniform1i(PF_LKeyPressedUniform, 1);

                    GLES32.glUniform3fv(PF_lightAmbientUniform, 1, LightAmbient,0);
                    GLES32.glUniform3fv(PF_lightDiffuseUniform, 1, LightDefuse,0);
                    GLES32.glUniform3fv(PF_lightSpecularUniform, 1, LightSpecular,0);
                    
                    GLES32.glUniform4fv(PF_lightPositionUniform, 1, LightPosition,0);//posiitional light

                    GLES32.glUniform3fv(PF_materialAmbientUniform, 1, materialAmbient,0);
                    GLES32.glUniform3fv(PF_materialDiffuseUniform, 1, materilaDefuse,0);
                    GLES32.glUniform3fv(PF_materialSpecularUniform, 1, materialSpecular,0);

                    GLES32.glUniform1f(PF_materialShininessUniform, materialShininess);
                }
                else
                {
                    GLES32.glUniform1i(PF_LKeyPressedUniform, 0);
                }

        }else{
            GLES32.glUseProgram(PV_shaderProgramObject);

                Matrix.setIdentityM(modelMatrix,0);
                Matrix.setIdentityM(viewMatrix,0);

                Matrix.setIdentityM(translateMatrix,0);
                Matrix.setIdentityM(rotateMatrix,0);
                Matrix.setIdentityM(scaleMatrix,0);

                Matrix.translateM(translateMatrix,0,0.0f,0.0f,-1.5f);
                Matrix.multiplyMM(modelMatrix,0,modelMatrix,0,translateMatrix,0);
                
                GLES32.glUniformMatrix4fv(PV_modelMatrixUniform,1,false,modelMatrix,0);

                GLES32.glUniformMatrix4fv(PV_viewMatrixUniform,1,false,viewMatrix,0);
                
                GLES32.glUniformMatrix4fv(PV_projectionMatrixUniform,1,false,perspectiveProjectionMatrix,0);

                if (doubleTapLights == true)
                {
                    GLES32.glUniform1i(PV_LKeyPressedUniform, 1);

                    GLES32.glUniform3fv(PV_lightAmbientUniform, 1, LightAmbient,0);
                    GLES32.glUniform3fv(PV_lightDiffuseUniform, 1, LightDefuse,0);
                    GLES32.glUniform3fv(PV_lightSpecularUniform, 1, LightSpecular,0);
                    
                    GLES32.glUniform4fv(PV_lightPositionUniform, 1, LightPosition,0);//posiitional light

                    GLES32.glUniform3fv(PV_materialAmbientUniform, 1, materialAmbient,0);
                    GLES32.glUniform3fv(PV_materialDiffuseUniform, 1, materilaDefuse,0);
                    GLES32.glUniform3fv(PV_materialSpecularUniform, 1, materialSpecular,0);

                    GLES32.glUniform1f(PV_materialShininessUniform, materialShininess);
                }
                else
                {
                    GLES32.glUniform1i(PV_LKeyPressedUniform, 0);
                }
        }
                GLES32.glBindVertexArray(vao_sphere[0]);
                    GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_element[0]);
                    GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);                
                GLES32.glBindVertexArray(0);

            GLES32.glUseProgram(0); 
        angle+=3;
        requestRender();//like render/flush/swap Double Buffer
    }

    private void uninitialize()
    {
        if(vao_sphere[0] != 0)
       {
           GLES32.glDeleteVertexArrays(1,vao_sphere,0);
           vao_sphere[0]=0;
       }

       if(vbo_position[0]!=0)
       {
           GLES32.glDeleteBuffers(1,vbo_position,0);
           vbo_position[0]=0;
       }

        if(vbo_normal[0]!=0)
       {
           GLES32.glDeleteBuffers(1,vbo_normal,0);
           vbo_normal[0]=0;
       }

        if(vbo_element[0]!=0)
       {
           GLES32.glDeleteBuffers(1,vbo_element,0);
           vbo_element[0]=0;
       }

       if(vbo_texCord[0]!=0)
       {
           GLES32.glDeleteBuffers(1,vbo_texCord,0);
           vbo_texCord[0]=0;
       }

        if(PF_shaderProgramObject != 0 )
        {
            if(PF_fragmentShaderObject != 0)
            {
                System.out.println("VSV-LOG | PF | fragment Shader object has actual deattached:"+PF_fragmentShaderObject);
                GLES32.glDetachShader(PF_shaderProgramObject,PF_fragmentShaderObject);
                GLES32.glDeleteShader(PF_fragmentShaderObject);
                PF_fragmentShaderObject=0;
            }
                
            if(PF_vertexShaderObject != 0)
            {
                System.out.println("VSV-LOG | PF | vertex Shader object has actual deattached:"+PF_vertexShaderObject);
                GLES32.glDetachShader(PF_shaderProgramObject,PF_vertexShaderObject);
                GLES32.glDeleteShader(PF_vertexShaderObject);
                PF_vertexShaderObject=0;
            }
            
            GLES32.glDeleteProgram(PF_shaderProgramObject);
            PF_shaderProgramObject=0;
        }

        if(PV_shaderProgramObject != 0 )
        {
            if(PV_fragmentShaderObject != 0)
            {
                System.out.println("VSV-LOG | PV | fragment Shader object has actual deattached:"+PV_fragmentShaderObject);
                GLES32.glDetachShader(PV_shaderProgramObject,PV_fragmentShaderObject);
                GLES32.glDeleteShader(PV_fragmentShaderObject);
                PV_fragmentShaderObject=0;
            }
                
            if(PV_vertexShaderObject != 0)
            {
                System.out.println("VSV-LOG |  PV | vertex Shader object has actual deattached:"+PV_vertexShaderObject);
                GLES32.glDetachShader(PV_shaderProgramObject,PV_vertexShaderObject);
                GLES32.glDeleteShader(PV_vertexShaderObject);
                PV_vertexShaderObject=0;
            }
            
            GLES32.glDeleteProgram(PV_shaderProgramObject);
            PV_shaderProgramObject=0;
        }

        // if(PF_shaderProgramObject != 0)
        // {
        //     GLES32.glUseProgram(PF_shaderProgramObject);

        //         int shaderCount=100;
        //         int[] tempShaderCount=new int[1];
        //         int[] pShader=new int[100];

        //         System.out.println("VSV-LOG | Shader program detach and deletion started");

        //         GLES32.glGetAttachedShaders(PF_shaderProgramObject,
        //                                     shaderCount,
        //                                     tempShaderCount,0,
        //                                     pShader,0);
                
        //         System.out.println("VSV-LOG | Shader program has actual attached shader count is:"+tempShaderCount[0]);
                
        //         int i;
        //         for(i=0;i<tempShaderCount[0];i++)
        //         {
        //             System.out.println("VSV-LOG | Shader program has actual deattached:"+pShader[i]);

        //             GLES32.glDetachShader(PF_shaderProgramObject,
        //                             pShader[i]);

        //             GLES32.glDeleteShader(pShader[i]);
                    
        //             pShader[i]=0;
        //         }

        //         GLES32.glDeleteProgram(PF_shaderProgramObject);
        //         PF_shaderProgramObject=0;

        //     GLES32.glUseProgram(0);
        // }

    }
}