package com.example.interleaved;


public class GLESMacros {
    public static final int VSV_ATTRIBUTE_POSITION=0;
    public static final int VSV_ATTRIBUTE_COLOR=1;
    public static final int VSV_ATTRIBUTE_NORMAL=2;
    public static final int VSV_ATTRIBUTE_TEXTURE=3;
}
