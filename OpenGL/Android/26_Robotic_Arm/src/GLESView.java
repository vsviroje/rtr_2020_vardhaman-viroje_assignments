package com.example.robotic_arm;

import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;

import android.content.Context;
import android.view.MotionEvent;

import android.opengl.GLSurfaceView;
import android.opengl.GLES32;//for opengl embedded system 3.2 version core API support(PP)

import javax.microedition.khronos.opengles.GL10;//OpenGL ES 1.0 for legacy API support(FFP)
import javax.microedition.khronos.egl.EGLConfig;//embedded GL Bridging API to provide bridge between OpenGL ES's NDK and OpenGL ES's SDK 

import android.opengl.Matrix;

//non blocking IO buffer
import java.nio.ByteBuffer;
import java.nio.ByteOrder; // used to set/get bigEndian/littleEndian of byte
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

//for stack
import java.util.*;  

public class GLESView extends GLSurfaceView implements OnGestureListener,OnDoubleTapListener,GLSurfaceView.Renderer
{
    private GestureDetector gestureDetector;
    private final Context context;//final is similar to constant

    private int vertexShaderObject;
    private int fragmentShaderObject;
    private int shaderProgramObject;

    private int[] vao_sphere =new int[1];

    private int[] vbo_position =new int[1];
    private int[] vbo_normal =new int[1];
    private int[] vbo_element =new int[1];
    private int[] vbo_texCord =new int[1];

    private int modelMatrixUniform;
    private int viewMatrixUniform;
    private int projectionMatrixUniform;

    private int  lightAmbientUniform;
    private int  lightDiffuseUniform;
    private int  lightSpecularUniform;
    private int  lightPositionUniform;

    private int  materialAmbientUniform;
    private int  materialDiffuseUniform;
    private int  materialSpecularUniform;
    private int  materialShininessUniform;
    private int  LKeyPressedUniform;

    private float perspectiveProjectionMatrix[]=new float[16]; //4*4 matrix

    private int angle;

    private int numElements;
    private int numVertices;

    private boolean singleTapAnimate;
    private boolean doubleTapLights;

    
    final float LightAmbient[] =new float[]{ 0.1f,0.1f,0.1f,1.0f };
    final float LightDefuse[] =new float[] { 1.0f,1.0f,1.0f,1.0f };
    final float LightSpecular[] =new float[] { 1.0f,1.0f,1.0f,1.0f };
    final float LightPosition[] =new float[]{ 0.0f,0.0f,0.0f,1.0f };

    final float materialAmbient[] =new float[] { 0.0f,0.0f,0.0f,1.0f };
    final float materilaDefuse[] =new float[] { 0.5f,0.2f,0.7f,1.0f };
    final float materialSpecular[] =new float[] { 1.0f,1.0f,1.0f,1.0f };
    final float materialShininess = 128.0f;

    Stack<float[]> MatrixStack= new Stack<>();  
    private int shoulder = 0;
    private int elbow = 0;

    public GLESView(Context drawingContext)
    {
        super(drawingContext);

        System.out.println("VSV-LOG | GLESView constructor");

        setEGLContextClientVersion(3);// set EGLContext to supporting OpenGL-ES version 3
        
        setRenderer(this);

        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);//just like invalidateRect

        gestureDetector = new GestureDetector(drawingContext,this,null,false);

        gestureDetector.setOnDoubleTapListener(this);

        context=drawingContext;
    }

    //Triggers all gesture and tap events
    @Override 
    public boolean onTouchEvent(MotionEvent event)
    {
        //code
        int eventaction = event.getAction();
        if(!gestureDetector.onTouchEvent(event))
        {
            super.onTouchEvent(event);
        }
        return (true);
    }

    //OnDoubleTapListener interface
    @Override
    public boolean onDoubleTap(MotionEvent e)
    {
        System.out.println("VSV-LOG | Double Tap");
        doubleTapLights =!doubleTapLights;
        return true;
    }

    //OnDoubleTapListener interface
    @Override
    public boolean onDoubleTapEvent(MotionEvent e)
    {
        return true;
    }

    //OnDoubleTapListener interface
    @Override
    public boolean onSingleTapConfirmed(MotionEvent e)
    {
        System.out.println("VSV-LOG | Single Tap");
        // singleTapAnimate= !singleTapAnimate;
        return true;    
    }

    //OnGestureListener interface
    @Override
    public boolean onDown(MotionEvent e)
    {
        return true;    
    }

    //OnGestureListener interface
    @Override
    public boolean onFling(MotionEvent e1,MotionEvent e2,float velocityX,float velocityY)
    {
        return true;    
    }

    //OnGestureListener interface
    @Override
    public void onLongPress(MotionEvent e)
    {
        System.out.println("VSV-LOG | Long press");
    }

    //OnGestureListener interface
    @Override
    public boolean onScroll(MotionEvent e1,MotionEvent e2,float velocityX,float velocityY)
    {
        System.out.println("VSV-LOG | Scroll");
        uninitialize();
        System.exit(0);
        return true;    
    }

    //OnGestureListener interface
    @Override
    public void onShowPress(MotionEvent e)
    {
    }

    //OnGestureListener interface
    @Override
    public boolean onSingleTapUp(MotionEvent e)
    {
        return true;    
    }

    // GLSurfaceView.Renderer interface
    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config)
    {
        String version = gl.glGetString(GL10.GL_VERSION);
        System.out.println("VSV-LOG | OpenGL-ES Version:-"+ version);

        String glsl_version = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
        System.out.println("VSV-LOG | GLSL Version:-"+ glsl_version);

        initialize(gl);
    }

    // GLSurfaceView.Renderer interface
    @Override
    public void onSurfaceChanged(GL10 unused, int width ,int height)
    {
        resize(width,height);
    }


    // GLSurfaceView.Renderer interface
    @Override
    public void onDrawFrame(GL10 unused)
    {
        draw();
    }

    private void initialize(GL10 gl)
    {
        int[] iShaderCompiledStatus= new int[1];
        int[] iShaderProgramLinkStatus= new int[1];
        int[] iInfoLogLength = new int[1];
        String szInfoLog=null;

        vertexShaderObject=GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);

        final String vertexShaderSourceCode = String.format(
            "#version 320 es" +
            "\n" +
            "in vec4 vPosition;\n" +
            "in vec3 vNormal;\n" +
            
            "uniform mat4 u_modelMatrix;\n" +
            "uniform mat4 u_viewMatrix;\n" +
            "uniform mat4 u_projectionMatrix;\n" +

            "uniform vec3 u_lightAmbient;\n"+
            "uniform vec3 u_lightDiffuse;\n"+
            "uniform vec3 u_lightSpecular;\n"+
            "uniform vec4 u_lightPosition;\n"+
            
            "uniform vec3 u_materialAmbient;\n"+
            "uniform vec3 u_materialDiffuse;\n"+
            "uniform vec3 u_materialSpecular;\n"+
            "uniform float u_materialShininess;\n"+
            
            "uniform int u_lKeyPressed;\n"+
            
            "out vec3 out_phongADS_Light;\n"+

            "precision highp float;"+
            "precision highp int;"+
            
            "void main(void)\n" +
            "{\n" +
                "if (u_lKeyPressed == 1)\n"+
                "{\n"+

                    "vec4 eyeCoord =  u_viewMatrix * u_modelMatrix * vPosition;\n"+
                    
                    "vec3 transformedNormal = normalize( mat3(u_viewMatrix * u_modelMatrix) * vNormal );\n"+

                    "vec3 lightDirection = normalize( vec3( u_lightPosition - eyeCoord ) );\n"+
                    
                    "vec3 reflectionVector = reflect( -lightDirection, transformedNormal );\n"+
                    
                    "vec3 viewVector = normalize( -eyeCoord.xyz);\n"+
                    
                    "vec3 ambient = u_lightAmbient * u_materialAmbient;\n"+
                    
                    "vec3 diffuse = u_lightDiffuse * u_materialDiffuse * max( dot(lightDirection , transformedNormal), 0.0);\n"+
                    
                    "vec3 specular = u_lightSpecular * u_materialSpecular * pow( max( dot(reflectionVector , viewVector), 0.0f) , u_materialShininess);\n"+
                    
                    "out_phongADS_Light= ambient + diffuse + specular;\n"+

                "}\n"+
                "else\n"+
                "{\n"+

                    "out_phongADS_Light = vec3(1.0f, 1.0f, 1.0f);\n"+
                
                "}\n"+
            
                "gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix * vPosition;\n" +
            "}\n"
        );

        GLES32.glShaderSource(vertexShaderObject,vertexShaderSourceCode);

        GLES32.glCompileShader(vertexShaderObject);

        GLES32.glGetShaderiv(vertexShaderObject,GLES32.GL_COMPILE_STATUS,iShaderCompiledStatus,0);

        if(iShaderCompiledStatus[0]==GLES32.GL_FALSE)
        {
            GLES32.glGetShaderiv(vertexShaderObject,GLES32.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
            if (iInfoLogLength[0]>0)
            {
                szInfoLog=GLES32.glGetShaderInfoLog(vertexShaderObject);
                System.out.println("VSV-LOG | Vertex Shader compilation Log="+szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }

        iShaderCompiledStatus[0]=0;
        iInfoLogLength[0]=0;
        szInfoLog=null;

        fragmentShaderObject=GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);

        final String  fragmentShaderSourceCode= String.format(
            "#version 320 es" +
            "\n" +
            "precision highp float;"+
            "precision highp int;"+
            
            "out vec4 fragColor;"+
            "in vec3 out_phongADS_Light;\n"+

            "void main(void)" +
            "{"+    
                "fragColor=vec4(out_phongADS_Light, 1.0f);"+
            "}"
        );

        GLES32.glShaderSource(fragmentShaderObject,fragmentShaderSourceCode);

        GLES32.glCompileShader(fragmentShaderObject);

        GLES32.glGetShaderiv(fragmentShaderObject,GLES32.GL_COMPILE_STATUS,iShaderCompiledStatus,0);

        if(iShaderCompiledStatus[0]==GLES32.GL_FALSE)
        {
            GLES32.glGetShaderiv(fragmentShaderObject,GLES32.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
            if (iInfoLogLength[0]>0)
            {
                szInfoLog=GLES32.glGetShaderInfoLog(fragmentShaderObject);
                System.out.println("VSV-LOG | Fragment Shader compilation Log="+szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }

        iShaderCompiledStatus[0]=0;
        iInfoLogLength[0]=0;
        szInfoLog=null;

        shaderProgramObject=GLES32.glCreateProgram();

        GLES32.glAttachShader(shaderProgramObject,vertexShaderObject);
        GLES32.glAttachShader(shaderProgramObject,fragmentShaderObject);

        GLES32.glBindAttribLocation(shaderProgramObject,GLESMacros.VSV_ATTRIBUTE_POSITION,"vPosition");
        GLES32.glBindAttribLocation(shaderProgramObject,GLESMacros.VSV_ATTRIBUTE_NORMAL,"vNormal");

        GLES32.glLinkProgram(shaderProgramObject);
        GLES32.glGetProgramiv(shaderProgramObject,GLES32.GL_LINK_STATUS,iShaderProgramLinkStatus,0);
        if (iShaderProgramLinkStatus[0]==GLES32.GL_FALSE)
        {
             GLES32.glGetShaderiv(shaderProgramObject,GLES32.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
            if (iInfoLogLength[0]>0)
            {
                szInfoLog=GLES32.glGetShaderInfoLog(shaderProgramObject);
                System.out.println("VSV-LOG | Shader program link Log="+szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }

    	modelMatrixUniform =GLES32.glGetUniformLocation(shaderProgramObject,"u_modelMatrix");
		viewMatrixUniform =GLES32.glGetUniformLocation(shaderProgramObject,"u_viewMatrix");
		projectionMatrixUniform =GLES32.glGetUniformLocation(shaderProgramObject,"u_projectionMatrix");

		lightAmbientUniform =GLES32.glGetUniformLocation(shaderProgramObject,"u_lightAmbient");
		lightDiffuseUniform =GLES32.glGetUniformLocation(shaderProgramObject,"u_lightDiffuse");
		lightSpecularUniform =GLES32.glGetUniformLocation(shaderProgramObject,"u_lightSpecular");
		lightPositionUniform =GLES32.glGetUniformLocation(shaderProgramObject,"u_lightPosition");

		materialAmbientUniform =GLES32.glGetUniformLocation(shaderProgramObject,"u_materialAmbient");
		materialDiffuseUniform =GLES32.glGetUniformLocation(shaderProgramObject,"u_materialDiffuse");
		materialSpecularUniform =GLES32.glGetUniformLocation(shaderProgramObject,"u_materialSpecular");
		materialShininessUniform =GLES32.glGetUniformLocation(shaderProgramObject,"u_materialShininess");

		LKeyPressedUniform =GLES32.glGetUniformLocation(shaderProgramObject,"u_lKeyPressed");

        Sphere sphere=new Sphere();
        float sphere_vertices[]=new float[1146];
        float sphere_normals[]=new float[1146];
        float sphere_textures[]=new float[764];
        short sphere_elements[]=new short[2280];
        sphere.getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
        numVertices = sphere.getNumberOfSphereVertices();
        numElements = sphere.getNumberOfSphereElements();

        ByteBuffer byteBuffer;
        FloatBuffer verticesBuffer;
        FloatBuffer colorBuffer;
        FloatBuffer normalBuffer;
        ShortBuffer elementsBuffer;

        GLES32.glGenVertexArrays(1,vao_sphere,0);
        GLES32.glBindVertexArray(vao_sphere[0]);

            GLES32.glGenBuffers(1,vbo_position,0);
            GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_position[0]);
            
            byteBuffer=ByteBuffer.allocateDirect(sphere_vertices.length * 4);
            byteBuffer.order(ByteOrder.nativeOrder());
            verticesBuffer=byteBuffer.asFloatBuffer();
            verticesBuffer.put(sphere_vertices);
            verticesBuffer.position(0);
            
            GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                                sphere_vertices.length * 4,
                                verticesBuffer,
                                GLES32.GL_STATIC_DRAW);
            
            GLES32.glVertexAttribPointer(GLESMacros.VSV_ATTRIBUTE_POSITION,
                                        3,
                                        GLES32.GL_FLOAT,
                                        false,0,0);
            
            GLES32.glEnableVertexAttribArray(GLESMacros.VSV_ATTRIBUTE_POSITION);
            
            GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);

            GLES32.glGenBuffers(1,vbo_normal,0);
            GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_normal[0]);
            
            byteBuffer=ByteBuffer.allocateDirect(sphere_normals.length * 4);//float size is 4 byte
            byteBuffer.order(ByteOrder.nativeOrder());
            normalBuffer=byteBuffer.asFloatBuffer();
            normalBuffer.put(sphere_normals);
            normalBuffer.position(0);
            
            GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                                sphere_normals.length * 4,
                                normalBuffer,
                                GLES32.GL_STATIC_DRAW);
            
            GLES32.glVertexAttribPointer(GLESMacros.VSV_ATTRIBUTE_NORMAL,
                                        3,
                                        GLES32.GL_FLOAT,
                                        false,0,0);
            
            GLES32.glEnableVertexAttribArray(GLESMacros.VSV_ATTRIBUTE_NORMAL);
            
            GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);

            GLES32.glGenBuffers(1,vbo_element,0);
            GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER,vbo_element[0]);
            
            byteBuffer=ByteBuffer.allocateDirect(sphere_elements.length * 2);// short size is 2 byte
            byteBuffer.order(ByteOrder.nativeOrder());
            elementsBuffer=byteBuffer.asShortBuffer();
            elementsBuffer.put(sphere_elements);
            elementsBuffer.position(0);
            
            GLES32.glBufferData(GLES32.GL_ELEMENT_ARRAY_BUFFER,
                                sphere_elements.length * 2,
                                elementsBuffer,
                                GLES32.GL_STATIC_DRAW);
            
            GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER,0);
        GLES32.glBindVertexArray(0);

        doubleTapLights = true;
        
        GLES32.glEnable(GLES32.GL_DEPTH_TEST);
        GLES32.glDepthFunc(GLES32.GL_LEQUAL);

        GLES32.glClearColor(0.0f,0.0f,0.0f,1.0f);
	    GLES32.glClearDepthf(1.0f);

        Matrix.setIdentityM(perspectiveProjectionMatrix,0);
    }

    private void resize(int width ,int height)
    {
        GLES32.glViewport(0,0,width,height);
        Matrix.perspectiveM(perspectiveProjectionMatrix,0,
                            45.0f,
                            (float)width/(float)height,
                            0.1f,
                            100.0f);
    }

    private void glpushMatrix(float[] data)
    {
        float[] temp=(float[])data.clone();
        System.out.println("VSV-LOG | Pushed="+temp);
        MatrixStack.push(temp);
    }

    private float[] glpopMatrix()
    {
        float[] temp=MatrixStack.pop();
        System.out.println("VSV-LOG | poped="+temp);
        return  temp;
    }

    public void draw()
    {
        GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);

        GLES32.glUseProgram(shaderProgramObject);

            float modelMatrix[] = new float[16];
            float tempMatrix[] = new float[16];
            float viewMatrix[] = new float[16];

            float translateMatrix[] = new float[16];
            float rotateMatrix[] = new float[16];
            float scaleMatrix[] = new float[16];

            Matrix.setIdentityM(viewMatrix,0);
            Matrix.setIdentityM(tempMatrix,0);

            Matrix.setIdentityM(modelMatrix,0);
            Matrix.setIdentityM(translateMatrix,0);
            Matrix.setIdentityM(rotateMatrix,0);
            Matrix.setIdentityM(scaleMatrix,0);

            GLES32.glUniform1i(LKeyPressedUniform, 1);

            GLES32.glUniform3fv(lightAmbientUniform, 1, LightAmbient,0);
            GLES32.glUniform3fv(lightDiffuseUniform, 1, LightDefuse,0);
            GLES32.glUniform3fv(lightSpecularUniform, 1, LightSpecular,0);
            
            GLES32.glUniform4fv(lightPositionUniform, 1, LightPosition,0);//posiitional light

            GLES32.glUniform3fv(materialAmbientUniform, 1, materialAmbient,0);
            GLES32.glUniform3fv(materialDiffuseUniform, 1, materilaDefuse,0);
            GLES32.glUniform3fv(materialSpecularUniform, 1, materialSpecular,0);

            GLES32.glUniform1f(materialShininessUniform, materialShininess);
            
            System.out.println("VSV-LOG | start");

            //----start----
            Matrix.translateM(translateMatrix,0,0.0f,0.0f,-6.0f);
            modelMatrix=(float[])translateMatrix.clone();
            glpushMatrix(modelMatrix);
                Matrix.setRotateM(rotateMatrix,0,(float)shoulder, 0.0f, 0.0f, 1.0f);
                Matrix.translateM(translateMatrix,0,1.0f,0.0f,0.0f);

            	//modelMatrix = modelMatrix  * rotateMatrix * translateMatrix;
                Matrix.multiplyMM(tempMatrix,0,rotateMatrix,0,translateMatrix,0); 
                Matrix.multiplyMM(modelMatrix,0,modelMatrix,0,tempMatrix,0); 

                glpushMatrix(modelMatrix);

                    Matrix.scaleM(scaleMatrix,0,2.0f, 0.5f, 1.0f); 
                    //modelMatrix = modelMatrix * scaleMatrix;
                    Matrix.multiplyMM(modelMatrix,0,modelMatrix,0,scaleMatrix,0); 


                    GLES32.glUniformMatrix4fv(modelMatrixUniform,1,false,modelMatrix,0);

                    GLES32.glUniformMatrix4fv(viewMatrixUniform,1,false,viewMatrix,0);
                    
                    GLES32.glUniformMatrix4fv(projectionMatrixUniform,1,false,perspectiveProjectionMatrix,0);

                    GLES32.glBindVertexArray(vao_sphere[0]);
                        GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_element[0]);
                        GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);                
                    GLES32.glBindVertexArray(0);

                modelMatrix = glpopMatrix();

                Matrix.translateM(translateMatrix,0,2.0f, 0.0f, 0.0f);
                Matrix.setRotateM(rotateMatrix,0,(float)elbow, 0.0f, 0.0f, 1.0f);
			
                //modelMatrix = modelMatrix * translateMatrix * rotateMatrix;
                Matrix.multiplyMM(tempMatrix,0,translateMatrix,0,rotateMatrix,0); 
                Matrix.multiplyMM(modelMatrix,0,modelMatrix,0,tempMatrix,0); 

                Matrix.translateM(translateMatrix,0,0.1f, 0.0f, 0.0f);
			    //modelMatrix = modelMatrix * translateMatrix;
                Matrix.multiplyMM(modelMatrix,0,modelMatrix,0,translateMatrix,0); 

                glpushMatrix(modelMatrix);
                    Matrix.scaleM(scaleMatrix,0,2.0f, 2.0f, 1.0f); 
                    //modelMatrix = modelMatrix * scaleMatrix;
                    Matrix.multiplyMM(modelMatrix,0,modelMatrix,0,scaleMatrix,0); 

                    GLES32.glUniformMatrix4fv(modelMatrixUniform,1,false,modelMatrix,0);

                    GLES32.glUniformMatrix4fv(viewMatrixUniform,1,false,viewMatrix,0);
                    
                    GLES32.glUniformMatrix4fv(projectionMatrixUniform,1,false,perspectiveProjectionMatrix,0);

                    GLES32.glBindVertexArray(vao_sphere[0]);
                        GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_element[0]);
                        GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);                
                    GLES32.glBindVertexArray(0);

                modelMatrix = glpopMatrix();
            modelMatrix = glpopMatrix();
            System.out.println("VSV-LOG | end");

        GLES32.glUseProgram(0);    

        if(doubleTapLights)
        {
			shoulder = (shoulder + 3) % 360;
			elbow = (elbow + 3) % 360;

        }else{
	        shoulder = (shoulder - 3) % 360;
			elbow = (elbow - 3) % 360;
        }     
        requestRender();//like render/flush/swap Double Buffer
    }

    private void uninitialize()
    {
        MatrixStack.clear();

        if(vao_sphere[0] != 0)
       {
           GLES32.glDeleteVertexArrays(1,vao_sphere,0);
           vao_sphere[0]=0;
       }

       if(vbo_position[0]!=0)
       {
           GLES32.glDeleteBuffers(1,vbo_position,0);
           vbo_position[0]=0;
       }

        if(vbo_normal[0]!=0)
       {
           GLES32.glDeleteBuffers(1,vbo_normal,0);
           vbo_normal[0]=0;
       }

        if(vbo_element[0]!=0)
       {
           GLES32.glDeleteBuffers(1,vbo_element,0);
           vbo_element[0]=0;
       }

       if(vbo_texCord[0]!=0)
       {
           GLES32.glDeleteBuffers(1,vbo_texCord,0);
           vbo_texCord[0]=0;
       }

        if(shaderProgramObject != 0 )
        {
            if(fragmentShaderObject != 0)
            {
                System.out.println("VSV-LOG | fragment Shader object has actual deattached:"+fragmentShaderObject);
                GLES32.glDetachShader(shaderProgramObject,fragmentShaderObject);
                GLES32.glDeleteShader(fragmentShaderObject);
                fragmentShaderObject=0;
            }
                
            if(vertexShaderObject != 0)
            {
                System.out.println("VSV-LOG | vertex Shader object has actual deattached:"+vertexShaderObject);
                GLES32.glDetachShader(shaderProgramObject,vertexShaderObject);
                GLES32.glDeleteShader(vertexShaderObject);
                vertexShaderObject=0;
            }
            
            GLES32.glDeleteProgram(shaderProgramObject);
            shaderProgramObject=0;
        }

        // if(shaderProgramObject != 0)
        // {
        //     GLES32.glUseProgram(shaderProgramObject);

        //         int shaderCount=100;
        //         int[] tempShaderCount=new int[1];
        //         int[] pShader=new int[100];

        //         System.out.println("VSV-LOG | Shader program detach and deletion started");

        //         GLES32.glGetAttachedShaders(shaderProgramObject,
        //                                     shaderCount,
        //                                     tempShaderCount,0,
        //                                     pShader,0);
                
        //         System.out.println("VSV-LOG | Shader program has actual attached shader count is:"+tempShaderCount[0]);
                
        //         int i;
        //         for(i=0;i<tempShaderCount[0];i++)
        //         {
        //             System.out.println("VSV-LOG | Shader program has actual deattached:"+pShader[i]);

        //             GLES32.glDetachShader(shaderProgramObject,
        //                             pShader[i]);

        //             GLES32.glDeleteShader(pShader[i]);
                    
        //             pShader[i]=0;
        //         }

        //         GLES32.glDeleteProgram(shaderProgramObject);
        //         shaderProgramObject=0;

        //     GLES32.glUseProgram(0);
        // }

    }
}