package com.example.sphere_material;

import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;

import android.content.Context;
import android.view.MotionEvent;

import android.opengl.GLSurfaceView;
import android.opengl.GLES32;//for opengl embedded system 3.2 version core API support(PP)

import javax.microedition.khronos.opengles.GL10;//OpenGL ES 1.0 for legacy API support(FFP)
import javax.microedition.khronos.egl.EGLConfig;//embedded GL Bridging API to provide bridge between OpenGL ES's NDK and OpenGL ES's SDK 

import android.opengl.Matrix;

//non blocking IO buffer
import java.nio.ByteBuffer;
import java.nio.ByteOrder; // used to set/get bigEndian/littleEndian of byte
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

public class GLESView extends GLSurfaceView implements OnGestureListener,OnDoubleTapListener,GLSurfaceView.Renderer
{
    private GestureDetector gestureDetector;
    private final Context context;//final is similar to constant

    private int vertexShaderObject;
    private int fragmentShaderObject;
    private int shaderProgramObject;

    private int[] vao_sphere =new int[1];

    private int[] vbo_position =new int[1];
    private int[] vbo_normal =new int[1];
    private int[] vbo_element =new int[1];
    private int[] vbo_texCord =new int[1];

    private int modelMatrixUniform;
    private int viewMatrixUniform;
    private int projectionMatrixUniform;

    private int  lightAmbientUniform;
    private int  lightDiffuseUniform;
    private int  lightSpecularUniform;
    private int  lightPositionUniform;

    private int  materialAmbientUniform;
    private int  materialDiffuseUniform;
    private int  materialSpecularUniform;
    private int  materialShininessUniform;
    private int  LKeyPressedUniform;

    private float perspectiveProjectionMatrix[]=new float[16]; //4*4 matrix

    private float angle;
    private float radius = 6.0f;

    private int numElements;
    private int numVertices;

    private boolean singleTapAnimate;
    private boolean doubleTapLights;
    
    final float LightAmbient[] =new float[]{ 0.1f,0.1f,0.1f,1.0f };
    final float LightDefuse[] =new float[] { 1.0f,1.0f,1.0f,1.0f };
    final float LightSpecular[] =new float[] { 1.0f,1.0f,1.0f,1.0f };
    float LightPosition[];

    GLESLightMaterial Materials[]=new GLESLightMaterial[24];

    private int orignal_width;
    private int orignal_height;
    private int singleTapRotationType;

    public GLESView(Context drawingContext)
    {
        super(drawingContext);

        System.out.println("VSV-LOG | GLESView constructor");

        setEGLContextClientVersion(3);// set EGLContext to supporting OpenGL-ES version 3
        
        setRenderer(this);

        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);//just like invalidateRect

        gestureDetector = new GestureDetector(drawingContext,this,null,false);

        gestureDetector.setOnDoubleTapListener(this);

        context=drawingContext;
    }

    //Triggers all gesture and tap events
    @Override 
    public boolean onTouchEvent(MotionEvent event)
    {
        //code
        int eventaction = event.getAction();
        if(!gestureDetector.onTouchEvent(event))
        {
            super.onTouchEvent(event);
        }
        return (true);
    }

    //OnDoubleTapListener interface
    @Override
    public boolean onDoubleTap(MotionEvent e)
    {
        System.out.println("VSV-LOG | Double Tap");
        doubleTapLights =!doubleTapLights;
        return true;
    }

    //OnDoubleTapListener interface
    @Override
    public boolean onDoubleTapEvent(MotionEvent e)
    {
        return true;
    }

    //OnDoubleTapListener interface
    @Override
    public boolean onSingleTapConfirmed(MotionEvent e)
    {
        System.out.println("VSV-LOG | Single Tap");
        // singleTapAnimate= !singleTapAnimate;

        singleTapRotationType += 1;
        if (singleTapRotationType > 3)
        {
            singleTapRotationType=0;
        }

        return true;    
    }

    //OnGestureListener interface
    @Override
    public boolean onDown(MotionEvent e)
    {
        return true;    
    }

    //OnGestureListener interface
    @Override
    public boolean onFling(MotionEvent e1,MotionEvent e2,float velocityX,float velocityY)
    {
        return true;    
    }

    //OnGestureListener interface
    @Override
    public void onLongPress(MotionEvent e)
    {
        System.out.println("VSV-LOG | Long press");
    }

    //OnGestureListener interface
    @Override
    public boolean onScroll(MotionEvent e1,MotionEvent e2,float velocityX,float velocityY)
    {
        System.out.println("VSV-LOG | Scroll");
        uninitialize();
        System.exit(0);
        return true;    
    }

    //OnGestureListener interface
    @Override
    public void onShowPress(MotionEvent e)
    {
    }

    //OnGestureListener interface
    @Override
    public boolean onSingleTapUp(MotionEvent e)
    {
        return true;    
    }

    // GLSurfaceView.Renderer interface
    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config)
    {
        String version = gl.glGetString(GL10.GL_VERSION);
        System.out.println("VSV-LOG | OpenGL-ES Version:-"+ version);

        String glsl_version = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
        System.out.println("VSV-LOG | GLSL Version:-"+ glsl_version);

        initialize(gl);
    }

    // GLSurfaceView.Renderer interface
    @Override
    public void onSurfaceChanged(GL10 unused, int width ,int height)
    {
        orignal_width=width;
        orignal_height=height;

        resize(width,height);
    }


    // GLSurfaceView.Renderer interface
    @Override
    public void onDrawFrame(GL10 unused)
    {
        draw();
    }

    private void initializeData()
    {
        int i;
        for(i=0;i<24;i++)
        {
            Materials[i]=new GLESLightMaterial();
        }

        Materials[0].materialAmbient=new float[]{0.0215f,0.1745f,0.0215f,1.0f};
        Materials[0].materilaDefuse=new float[]{0.07568f,0.61424f,0.07568f,1.0f};
        Materials[0].materialSpecular=new float[]{0.633f,0.727811f,0.633f,1.0f};
        Materials[0].materialShininess=0.6f * 128.0f;

        Materials[1].materialAmbient=new float[]{0.135f,0.2225f,0.1575f,1.0f};
        Materials[1].materilaDefuse=new float[]{0.54f,0.89f,0.63f,1.0f};
        Materials[1].materialSpecular=new float[]{0.316228f,0.316228f,0.316228f,1.0f};
        Materials[1].materialShininess=0.1f * 128f;

        Materials[2].materialAmbient=new float[]{0.05375f,0.05f,0.06625f,1.0f};
        Materials[2].materilaDefuse=new float[]{0.18275f,0.17f,0.22525f,1.0f};
        Materials[2].materialSpecular=new float[]{0.332741f,0.328634f,0.346435f,1.0f};
        Materials[2].materialShininess=0.3f * 128.0f;

        Materials[3].materialAmbient=new float[]{0.25f,0.20725f,0.20725f,1.0f};
        Materials[3].materilaDefuse=new float[]{1.0f,0.829f,0.829f,1.0f};
        Materials[3].materialSpecular=new float[]{0.296648f,0.296648f,0.296648f,1.0f};
        Materials[3].materialShininess=0.088f * 128.0f;

        Materials[4].materialAmbient=new float[]{0.1745f,0.01175f,0.01175f,1.0f};
        Materials[4].materilaDefuse=new float[]{0.61424f,0.04136f,0.04136f,1.0f};
        Materials[4].materialSpecular=new float[]{0.727811f,0.626959f,0.626959f,1.0f};
        Materials[4].materialShininess=0.6f * 128.0f;

        Materials[5].materialAmbient=new float[]{0.1f,0.18725f,0.1745f,1.0f};
        Materials[5].materilaDefuse=new float[]{0.396f,0.74151f,0.69102f,1.0f};
        Materials[5].materialSpecular=new float[]{0.297254f,0.30829f,0.306678f,1.0f};
        Materials[5].materialShininess=0.1f * 128.0f;

        Materials[6].materialAmbient=new float[]{0.329412f,0.223529f,0.027451f,1.0f};
        Materials[6].materilaDefuse=new float[]{0.780392f,0.568627f,0.113725f,1.0f};
        Materials[6].materialSpecular=new float[]{0.992157f,0.941176f,0.807843f,1.0f};
        Materials[6].materialShininess=0.21794872f * 128.0f;

        Materials[7].materialAmbient=new float[]{0.2125f,0.1275f,0.054f,1.0f};
        Materials[7].materilaDefuse=new float[]{0.714f,0.4284f,0.18144f,1.0f};
        Materials[7].materialSpecular=new float[]{0.393548f,0.271906f,0.166721f,1.0f};
        Materials[7].materialShininess=0.2f * 128.0f;

        Materials[8].materialAmbient=new float[]{0.25f,0.25f,0.25f,1.0f};
        Materials[8].materilaDefuse=new float[]{0.4f,0.4f,0.4f,1.0f};
        Materials[8].materialSpecular=new float[]{0.774597f,0.774597f,0.774597f,1.0f};
        Materials[8].materialShininess=0.6f * 128.0f;

        Materials[9].materialAmbient=new float[]{0.19125f,0.0735f,0.0225f,1.0f};
        Materials[9].materilaDefuse=new float[]{0.7038f,0.27048f,0.0828f,1.0f};
        Materials[9].materialSpecular=new float[]{0.256777f,0.137622f,0.086014f,1.0f};
        Materials[9].materialShininess=0.1f * 128.0f;

        Materials[10].materialAmbient=new float[]{0.24725f,0.1995f,0.0745f,1.0f};
        Materials[10].materilaDefuse=new float[]{0.75164f,0.60648f,0.22648f,1.0f};
        Materials[10].materialSpecular=new float[]{0.628281f,0.555802f,0.366065f,1.0f};
        Materials[10].materialShininess=0.4f * 128.0f;

        Materials[11].materialAmbient=new float[]{0.19225f,0.19225f,0.19225f,1.0f};
        Materials[11].materilaDefuse=new float[]{0.50754f,0.50754f,0.50754f,1.0f};
        Materials[11].materialSpecular=new float[]{0.508273f,0.508273f,0.508273f,1.0f};
        Materials[11].materialShininess=0.4f * 128.0f;

        Materials[12].materialAmbient=new float[]{0.0f,0.0f,0.0f,1.0f};
        Materials[12].materilaDefuse=new float[]{0.01f,0.01f,0.01f,1.0f};
        Materials[12].materialSpecular=new float[]{0.50f,0.50f,0.50f,1.0f};
        Materials[12].materialShininess=0.25f * 128.0f;

        Materials[13].materialAmbient=new float[]{0.0f,0.1f,0.06f,1.0f};
        Materials[13].materilaDefuse=new float[]{0.0f,0.50980392f,0.50980392f,1.0f};
        Materials[13].materialSpecular=new float[]{0.50196078f,0.50196078f,0.50196078f,1.0f};
        Materials[13].materialShininess=0.25f * 128.0f;

        Materials[14].materialAmbient=new float[]{0.0f,0.0f,0.0f,1.0f};
        Materials[14].materilaDefuse=new float[]{0.1f,0.35f,0.1f,1.0f};
        Materials[14].materialSpecular=new float[]{0.45f,0.55f,0.45f,1.0f};
        Materials[14].materialShininess=0.25f * 128.0f;

        Materials[15].materialAmbient=new float[]{0.0f,0.0f,0.0f,1.0f};
        Materials[15].materilaDefuse=new float[]{0.5f,0.0f,0.0f,1.0f};
        Materials[15].materialSpecular=new float[]{0.7f,0.6f,0.6f,1.0f};
        Materials[15].materialShininess=0.25f * 128.0f;

        Materials[16].materialAmbient=new float[]{0.0f,0.0f,0.0f,1.0f};
        Materials[16].materilaDefuse=new float[]{0.55f,0.55f,0.55f,1.0f};
        Materials[16].materialSpecular=new float[]{0.70f,0.70f,0.70f,1.0f};
        Materials[16].materialShininess=0.25f * 128.0f;

        Materials[17].materialAmbient=new float[]{0.0f,0.0f,0.0f,1.0f};
        Materials[17].materilaDefuse=new float[]{0.5f,0.5f,0.0f,1.0f};
        Materials[17].materialSpecular=new float[]{0.60f,0.60f,0.60f,1.0f};
        Materials[17].materialShininess=0.25f * 128.0f;

        Materials[18].materialAmbient=new float[]{0.02f,0.02f,0.02f,1.0f};
        Materials[18].materilaDefuse=new float[]{0.01f,0.01f,0.01f,1.0f};
        Materials[18].materialSpecular=new float[]{0.4f,0.4f,0.4f,1.0f};
        Materials[18].materialShininess=0.078125f * 128.0f;

        Materials[19].materialAmbient=new float[]{0.0f,0.05f,0.05f,1.0f};
        Materials[19].materilaDefuse=new float[]{0.4f,0.5f,0.5f,1.0f};
        Materials[19].materialSpecular=new float[]{0.04f,0.7f,0.7f,1.0f};
        Materials[19].materialShininess=0.078125f * 128.0f;

        Materials[20].materialAmbient=new float[]{0.0f,0.05f,0.0f,1.0f};
        Materials[20].materilaDefuse=new float[]{0.4f,0.5f,0.4f,1.0f};
        Materials[20].materialSpecular=new float[]{0.04f,0.7f,0.04f,1.0f};
        Materials[20].materialShininess=0.078125f * 128.0f;

        Materials[21].materialAmbient=new float[]{0.05f,0.0f,0.0f,1.0f};
        Materials[21].materilaDefuse=new float[]{0.5f,0.4f,0.4f,1.0f};
        Materials[21].materialSpecular=new float[]{0.7f,0.04f,0.04f,1.0f};
        Materials[21].materialShininess=0.078125f * 128.0f;

        Materials[22].materialAmbient=new float[]{0.05f,0.05f,0.05f,1.0f};
        Materials[22].materilaDefuse=new float[]{0.5f,0.5f,0.5f,1.0f};
        Materials[22].materialSpecular=new float[]{0.7f,0.7f,0.7f,1.0f};
        Materials[22].materialShininess=0.078125f * 128.0f;

        Materials[23].materialAmbient=new float[]{0.05f,0.05f,0.0f,1.0f};
        Materials[23].materilaDefuse=new float[]{0.5f,0.5f,0.4f,1.0f};
        Materials[23].materialSpecular=new float[]{0.7f,0.7f,0.04f,1.0f};
        Materials[23].materialShininess=0.078125f * 128.0f;

    }

    private void initialize(GL10 gl)
    {
        initializeData();

        int[] iShaderCompiledStatus= new int[1];
        int[] iShaderProgramLinkStatus= new int[1];
        int[] iInfoLogLength = new int[1];
        String szInfoLog=null;

        vertexShaderObject=GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);

        final String vertexShaderSourceCode = String.format(
            "#version 320 es" +
            "\n" +
            "in vec4 vPosition;\n" +
            "in vec3 vNormal;\n" +
            
            "uniform mat4 u_modelMatrix;\n" +
            "uniform mat4 u_viewMatrix;\n" +
            "uniform mat4 u_projectionMatrix;\n" +

            "uniform int u_lKeyPressed;\n"+

            "uniform vec4 u_lightPosition;\n"+

            "out vec3 out_transformedNormal;\n"+
            "out vec3 out_lightDirection;\n"+
            "out vec3 out_viewVector;\n"+
            
            "void main(void)\n" +
            "{\n" +
                "if (u_lKeyPressed == 1)\n"+
                "{\n"+

                    "vec4 eyeCoord = u_viewMatrix * u_modelMatrix * vPosition;\n"+
                    
                    "out_transformedNormal= normalize( mat3(u_viewMatrix * u_modelMatrix) * vNormal );\n"+

                    "out_lightDirection = normalize( vec3( u_lightPosition - eyeCoord ) );\n"+
                    
                    "out_viewVector = normalize( -eyeCoord.xyz);\n"+
                
                "}\n"+
                
                "gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix * vPosition;\n" +
            "}\n"
        );

        GLES32.glShaderSource(vertexShaderObject,vertexShaderSourceCode);

        GLES32.glCompileShader(vertexShaderObject);

        GLES32.glGetShaderiv(vertexShaderObject,GLES32.GL_COMPILE_STATUS,iShaderCompiledStatus,0);

        if(iShaderCompiledStatus[0]==GLES32.GL_FALSE)
        {
            GLES32.glGetShaderiv(vertexShaderObject,GLES32.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
            if (iInfoLogLength[0]>0)
            {
                szInfoLog=GLES32.glGetShaderInfoLog(vertexShaderObject);
                System.out.println("VSV-LOG | Vertex Shader compilation Log="+szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }

        iShaderCompiledStatus[0]=0;
        iInfoLogLength[0]=0;
        szInfoLog=null;

        fragmentShaderObject=GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);

        final String  fragmentShaderSourceCode= String.format(
            "#version 320 es" +
            "\n" +
            "precision highp float;"+
            "precision highp int;"+
            
            "out vec4 fragColor;"+
            "vec3 phongADS_Light;\n"+

            "in vec3 out_transformedNormal;\n"+
            "in vec3 out_lightDirection;\n"+
            "in vec3 out_viewVector;\n"+

            "uniform vec3 u_lightAmbient;\n"+
            "uniform vec3 u_lightDiffuse;\n"+
            "uniform vec3 u_lightSpecular;\n"+
            
            "uniform vec3 u_materialAmbient;\n"+
            "uniform vec3 u_materialDiffuse;\n"+
            "uniform vec3 u_materialSpecular;\n"+
            "uniform float u_materialShininess;\n"+
            
            "uniform int u_lKeyPressed;\n"+
            
            "void main(void)" +
            "{"+  
                "if (u_lKeyPressed == 1)\n"+
                "{\n"+
                    "vec3 normalizedTransformedNormal = normalize( out_transformedNormal );\n"+
                    "vec3 normalizedLightDirection = normalize( out_lightDirection );\n"+
                    "vec3 normalizedViewVector = normalize( out_viewVector );\n"+

                    "vec3 reflectionVector = reflect( -normalizedLightDirection, normalizedTransformedNormal );\n"+
                    
                    "vec3 ambient = u_lightAmbient * u_materialAmbient;\n"+

                    "vec3 diffuse = u_lightDiffuse * u_materialDiffuse * max( dot(normalizedLightDirection , normalizedTransformedNormal), 0.0);\n"+

                    "vec3 specular = u_lightSpecular * u_materialSpecular * pow( max( dot(reflectionVector , normalizedViewVector), 0.0f) , u_materialShininess);\n"+

                    "phongADS_Light= ambient+ diffuse + specular;\n"+
                    
                "}\n"+
                "else\n"+
                "{\n"+
                    "phongADS_Light=vec3(1.0f, 1.0f, 1.0f);\n"+
                "}\n"+
            
                "fragColor=vec4(phongADS_Light, 1.0f);"+
            "}"
        );

        GLES32.glShaderSource(fragmentShaderObject,fragmentShaderSourceCode);

        GLES32.glCompileShader(fragmentShaderObject);

        GLES32.glGetShaderiv(fragmentShaderObject,GLES32.GL_COMPILE_STATUS,iShaderCompiledStatus,0);

        if(iShaderCompiledStatus[0]==GLES32.GL_FALSE)
        {
            GLES32.glGetShaderiv(fragmentShaderObject,GLES32.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
            if (iInfoLogLength[0]>0)
            {
                szInfoLog=GLES32.glGetShaderInfoLog(fragmentShaderObject);
                System.out.println("VSV-LOG | Fragment Shader compilation Log="+szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }

        iShaderCompiledStatus[0]=0;
        iInfoLogLength[0]=0;
        szInfoLog=null;

        shaderProgramObject=GLES32.glCreateProgram();

        GLES32.glAttachShader(shaderProgramObject,vertexShaderObject);
        GLES32.glAttachShader(shaderProgramObject,fragmentShaderObject);

        GLES32.glBindAttribLocation(shaderProgramObject,GLESMacros.VSV_ATTRIBUTE_POSITION,"vPosition");
        GLES32.glBindAttribLocation(shaderProgramObject,GLESMacros.VSV_ATTRIBUTE_NORMAL,"vNormal");

        GLES32.glLinkProgram(shaderProgramObject);
        GLES32.glGetProgramiv(shaderProgramObject,GLES32.GL_LINK_STATUS,iShaderProgramLinkStatus,0);
        if (iShaderProgramLinkStatus[0]==GLES32.GL_FALSE)
        {
             GLES32.glGetShaderiv(shaderProgramObject,GLES32.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
            if (iInfoLogLength[0]>0)
            {
                szInfoLog=GLES32.glGetShaderInfoLog(shaderProgramObject);
                System.out.println("VSV-LOG | Shader program link Log="+szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }

    	modelMatrixUniform =GLES32.glGetUniformLocation(shaderProgramObject,"u_modelMatrix");
		viewMatrixUniform =GLES32.glGetUniformLocation(shaderProgramObject,"u_viewMatrix");
		projectionMatrixUniform =GLES32.glGetUniformLocation(shaderProgramObject,"u_projectionMatrix");

		lightAmbientUniform =GLES32.glGetUniformLocation(shaderProgramObject,"u_lightAmbient");
		lightDiffuseUniform =GLES32.glGetUniformLocation(shaderProgramObject,"u_lightDiffuse");
		lightSpecularUniform =GLES32.glGetUniformLocation(shaderProgramObject,"u_lightSpecular");
		lightPositionUniform =GLES32.glGetUniformLocation(shaderProgramObject,"u_lightPosition");

		materialAmbientUniform =GLES32.glGetUniformLocation(shaderProgramObject,"u_materialAmbient");
		materialDiffuseUniform =GLES32.glGetUniformLocation(shaderProgramObject,"u_materialDiffuse");
		materialSpecularUniform =GLES32.glGetUniformLocation(shaderProgramObject,"u_materialSpecular");
		materialShininessUniform =GLES32.glGetUniformLocation(shaderProgramObject,"u_materialShininess");

		LKeyPressedUniform =GLES32.glGetUniformLocation(shaderProgramObject,"u_lKeyPressed");

        Sphere sphere=new Sphere();
        float sphere_vertices[]=new float[1146];
        float sphere_normals[]=new float[1146];
        float sphere_textures[]=new float[764];
        short sphere_elements[]=new short[2280];
        sphere.getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
        numVertices = sphere.getNumberOfSphereVertices();
        numElements = sphere.getNumberOfSphereElements();

        ByteBuffer byteBuffer;
        FloatBuffer verticesBuffer;
        FloatBuffer colorBuffer;
        FloatBuffer normalBuffer;
        ShortBuffer elementsBuffer;

        GLES32.glGenVertexArrays(1,vao_sphere,0);
        GLES32.glBindVertexArray(vao_sphere[0]);

            GLES32.glGenBuffers(1,vbo_position,0);
            GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_position[0]);
            
            byteBuffer=ByteBuffer.allocateDirect(sphere_vertices.length * 4);
            byteBuffer.order(ByteOrder.nativeOrder());
            verticesBuffer=byteBuffer.asFloatBuffer();
            verticesBuffer.put(sphere_vertices);
            verticesBuffer.position(0);
            
            GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                                sphere_vertices.length * 4,
                                verticesBuffer,
                                GLES32.GL_STATIC_DRAW);
            
            GLES32.glVertexAttribPointer(GLESMacros.VSV_ATTRIBUTE_POSITION,
                                        3,
                                        GLES32.GL_FLOAT,
                                        false,0,0);
            
            GLES32.glEnableVertexAttribArray(GLESMacros.VSV_ATTRIBUTE_POSITION);
            
            GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);

            GLES32.glGenBuffers(1,vbo_normal,0);
            GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_normal[0]);
            
            byteBuffer=ByteBuffer.allocateDirect(sphere_normals.length * 4);//float size is 4 byte
            byteBuffer.order(ByteOrder.nativeOrder());
            normalBuffer=byteBuffer.asFloatBuffer();
            normalBuffer.put(sphere_normals);
            normalBuffer.position(0);
            
            GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                                sphere_normals.length * 4,
                                normalBuffer,
                                GLES32.GL_STATIC_DRAW);
            
            GLES32.glVertexAttribPointer(GLESMacros.VSV_ATTRIBUTE_NORMAL,
                                        3,
                                        GLES32.GL_FLOAT,
                                        false,0,0);
            
            GLES32.glEnableVertexAttribArray(GLESMacros.VSV_ATTRIBUTE_NORMAL);
            
            GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);

            GLES32.glGenBuffers(1,vbo_element,0);
            GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER,vbo_element[0]);
            
            byteBuffer=ByteBuffer.allocateDirect(sphere_elements.length * 2);// short size is 2 byte
            byteBuffer.order(ByteOrder.nativeOrder());
            elementsBuffer=byteBuffer.asShortBuffer();
            elementsBuffer.put(sphere_elements);
            elementsBuffer.position(0);
            
            GLES32.glBufferData(GLES32.GL_ELEMENT_ARRAY_BUFFER,
                                sphere_elements.length * 2,
                                elementsBuffer,
                                GLES32.GL_STATIC_DRAW);
            
            GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER,0);
        GLES32.glBindVertexArray(0);
        
        GLES32.glEnable(GLES32.GL_DEPTH_TEST);
        GLES32.glDepthFunc(GLES32.GL_LEQUAL);

        GLES32.glClearColor(0.0f,0.0f,0.0f,1.0f);
	    GLES32.glClearDepthf(1.0f);

        Matrix.setIdentityM(perspectiveProjectionMatrix,0);
    }

    private void resize(int width ,int height)
    {
        GLES32.glViewport(0,0,width,height);
        Matrix.perspectiveM(perspectiveProjectionMatrix,0,
                            45.0f,
                            (float)width/(float)height,
                            0.1f,
                            100.0f);
    }

    public void draw()
    {
        float modelMatrix[] = new float[16];
        float viewMatrix[] = new float[16];

        float translateMatrix[] = new float[16];
        float rotateMatrix[] = new float[16];
        float scaleMatrix[] = new float[16];

        int i,j,k=0;
        int unit_width=orignal_width/6;
        int unit_height=orignal_height/4;


        GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);

        GLES32.glUseProgram(shaderProgramObject);

            for ( i = 0; i < 4; i++)
            {

                for ( j = 0; j < 6; j++)
                {		
                    GLES32.glViewport(j * unit_width, i * unit_height, unit_width, orignal_height / 6);

                    Matrix.setIdentityM(modelMatrix,0);
                    Matrix.setIdentityM(viewMatrix,0);

                    Matrix.setIdentityM(translateMatrix,0);
                    Matrix.setIdentityM(rotateMatrix,0);
                    Matrix.setIdentityM(scaleMatrix,0);

                    Matrix.translateM(translateMatrix,0,0.0f,0.0f,-1.3f);
                    Matrix.multiplyMM(modelMatrix,0,modelMatrix,0,translateMatrix,0);
                    
                    GLES32.glUniformMatrix4fv(modelMatrixUniform,1,false,modelMatrix,0);

                    GLES32.glUniformMatrix4fv(viewMatrixUniform,1,false,viewMatrix,0);
                    
                    GLES32.glUniformMatrix4fv(projectionMatrixUniform,1,false,perspectiveProjectionMatrix,0);

                    if (doubleTapLights == true)
                    {
                        GLES32.glUniform1i(LKeyPressedUniform, 1);

                        GLES32.glUniform3fv(lightAmbientUniform, 1, LightAmbient,0);
                        GLES32.glUniform3fv(lightDiffuseUniform, 1, LightDefuse,0);
                        GLES32.glUniform3fv(lightSpecularUniform, 1, LightSpecular,0);

                        if (singleTapRotationType == 1) {

                            LightPosition =new float[]{ 0.0f, radius * (float)Math.sin(angle), radius * (float)Math.cos(angle), 1.0f};
                        }
                        else if (singleTapRotationType == 2) {

                            LightPosition =new float[]{ radius * (float)Math.sin(angle), 0.0f, radius * (float)Math.cos(angle), 1.0f};
                        }
                        else if (singleTapRotationType == 3) {

                            LightPosition =new float[]{ radius * (float)Math.sin(angle), radius * (float)Math.cos(angle), 0.0f, 1.0f};
                        }
                        else if (singleTapRotationType == 0)
                        {
                            LightPosition =new float[]{ radius, radius, radius, 1.0f};
                        }
                        
                        GLES32.glUniform4fv(lightPositionUniform, 1, LightPosition,0);

                        GLES32.glUniform3fv(materialAmbientUniform, 1, Materials[k].materialAmbient,0);
                        GLES32.glUniform3fv(materialDiffuseUniform, 1, Materials[k].materilaDefuse,0);
                        GLES32.glUniform3fv(materialSpecularUniform, 1, Materials[k].materialSpecular,0);

                        GLES32.glUniform1f(materialShininessUniform, Materials[k].materialShininess);

                    }
                    else
                    {
                        GLES32.glUniform1i(LKeyPressedUniform, 0);
                    }

                    
                    GLES32.glBindVertexArray(vao_sphere[0]);
                        GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_element[0]);
                        GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);                
                    GLES32.glBindVertexArray(0);
                    
                    k++;
                }
            }

        GLES32.glUseProgram(0);    
        
        angle+=0.05f;
        requestRender();//like render/flush/swap Double Buffer
    }

    private void uninitialize()
    {
        if(vao_sphere[0] != 0)
       {
           GLES32.glDeleteVertexArrays(1,vao_sphere,0);
           vao_sphere[0]=0;
       }

       if(vbo_position[0]!=0)
       {
           GLES32.glDeleteBuffers(1,vbo_position,0);
           vbo_position[0]=0;
       }

        if(vbo_normal[0]!=0)
       {
           GLES32.glDeleteBuffers(1,vbo_normal,0);
           vbo_normal[0]=0;
       }

        if(vbo_element[0]!=0)
       {
           GLES32.glDeleteBuffers(1,vbo_element,0);
           vbo_element[0]=0;
       }

       if(vbo_texCord[0]!=0)
       {
           GLES32.glDeleteBuffers(1,vbo_texCord,0);
           vbo_texCord[0]=0;
       }

        if(shaderProgramObject != 0 )
        {
            if(fragmentShaderObject != 0)
            {
                System.out.println("VSV-LOG | fragment Shader object has actual deattached:"+fragmentShaderObject);
                GLES32.glDetachShader(shaderProgramObject,fragmentShaderObject);
                GLES32.glDeleteShader(fragmentShaderObject);
                fragmentShaderObject=0;
            }
                
            if(vertexShaderObject != 0)
            {
                System.out.println("VSV-LOG | vertex Shader object has actual deattached:"+vertexShaderObject);
                GLES32.glDetachShader(shaderProgramObject,vertexShaderObject);
                GLES32.glDeleteShader(vertexShaderObject);
                vertexShaderObject=0;
            }
            
            GLES32.glDeleteProgram(shaderProgramObject);
            shaderProgramObject=0;
        }

        // if(shaderProgramObject != 0)
        // {
        //     GLES32.glUseProgram(shaderProgramObject);

        //         int shaderCount=100;
        //         int[] tempShaderCount=new int[1];
        //         int[] pShader=new int[100];

        //         System.out.println("VSV-LOG | Shader program detach and deletion started");

        //         GLES32.glGetAttachedShaders(shaderProgramObject,
        //                                     shaderCount,
        //                                     tempShaderCount,0,
        //                                     pShader,0);
                
        //         System.out.println("VSV-LOG | Shader program has actual attached shader count is:"+tempShaderCount[0]);
                
        //         int i;
        //         for(i=0;i<tempShaderCount[0];i++)
        //         {
        //             System.out.println("VSV-LOG | Shader program has actual deattached:"+pShader[i]);

        //             GLES32.glDetachShader(shaderProgramObject,
        //                             pShader[i]);

        //             GLES32.glDeleteShader(pShader[i]);
                    
        //             pShader[i]=0;
        //         }

        //         GLES32.glDeleteProgram(shaderProgramObject);
        //         shaderProgramObject=0;

        //     GLES32.glUseProgram(0);
        // }

    }
}