package com.example.rgb_light_sphere;


public class GLESLightMaterial {
    public float LightAmbient[];
	public float LightDefuse[];
	public float LightSpecular[];
	public float LightPosition[];
}
