package com.example.two_steady_light_pyramid;

import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;

import android.content.Context;
import android.view.MotionEvent;

import android.opengl.GLSurfaceView;
import android.opengl.GLES32;//for opengl embedded system 3.2 version core API support(PP)

import javax.microedition.khronos.opengles.GL10;//OpenGL ES 1.0 for legacy API support(FFP)
import javax.microedition.khronos.egl.EGLConfig;//embedded GL Bridging API to provide bridge between OpenGL ES's NDK and OpenGL ES's SDK 

import android.opengl.Matrix;

//non blocking IO buffer
import java.nio.ByteBuffer;
import java.nio.ByteOrder; // used to set/get bigEndian/littleEndian of byte
import java.nio.FloatBuffer;

public class GLESView extends GLSurfaceView implements OnGestureListener,OnDoubleTapListener,GLSurfaceView.Renderer
{
    private GestureDetector gestureDetector;
    private final Context context;//final is similar to constant

    private int vertexShaderObject;
    private int fragmentShaderObject;
    private int shaderProgramObject;

    private int[] vao_pyramid =new int[1];

    private int[] vbo_position =new int[1];
    private int[] vbo_normal =new int[1];
    // private int[] vbo_color =new int[1];
    
    private int modelMatrixUniform;
    private int viewMatrixUniform;
    private int projectionlMatrixUniform;

    private int lightAmbientUniform[]=new int[2];
    private int lightDiffuseUniform[]=new int[2];
    private int lightSpecularUniform[]=new int[2];
    private int lightPositionUniform[]=new int[2];

    private int materialAmbientUniform;
    private int materialDiffuseUniform;
    private int materialSpecularUniform;
    private int materialShininessUniform;

    private int LKeyPressedUniform;

    private float perspectiveProjectionMatrix[]=new float[16]; //4*4 matrix

    private final float materialAmbient[] = new float[]{ 0.0f,0.0f,0.0f,1.0f };
    private final float materilaDefuse[] = new float[]{ 1.0f,1.0f,1.0f,1.0f };
    private final float materialSpecular[] = new float[]{ 1.0f,1.0f,1.0f,1.0f };
    private final float materialShininess = 50.0f;

    GLESLightMaterial[] light=new GLESLightMaterial[2];

    private int angle;

    private boolean singleTapAnimate;//false-vertex ,true-fragment
    private boolean doubleTapLights;

    public GLESView(Context drawingContext)
    {
        super(drawingContext);

        System.out.println("VSV-LOG | GLESView constructor");

        setEGLContextClientVersion(3);// set EGLContext to supporting OpenGL-ES version 3
        
        setRenderer(this);

        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);//just like invalidateRect

        gestureDetector = new GestureDetector(drawingContext,this,null,false);

        gestureDetector.setOnDoubleTapListener(this);

        context=drawingContext;
    }

    //Triggers all gesture and tap events
    @Override 
    public boolean onTouchEvent(MotionEvent event)
    {
        //code
        int eventaction = event.getAction();
        if(!gestureDetector.onTouchEvent(event))
        {
            super.onTouchEvent(event);
        }
        return (true);
    }

    //OnDoubleTapListener interface
    @Override
    public boolean onDoubleTap(MotionEvent e)
    {
        System.out.println("VSV-LOG | Double Tap");
        doubleTapLights =! doubleTapLights;
        return true;
    }

    //OnDoubleTapListener interface
    @Override
    public boolean onDoubleTapEvent(MotionEvent e)
    {
        return true;
    }

    //OnDoubleTapListener interface
    @Override
    public boolean onSingleTapConfirmed(MotionEvent e)
    {
        System.out.println("VSV-LOG | Single Tap");
        singleTapAnimate =! singleTapAnimate;
        return true;    
    }

    //OnGestureListener interface
    @Override
    public boolean onDown(MotionEvent e)
    {
        return true;    
    }

    //OnGestureListener interface
    @Override
    public boolean onFling(MotionEvent e1,MotionEvent e2,float velocityX,float velocityY)
    {
        return true;    
    }

    //OnGestureListener interface
    @Override
    public void onLongPress(MotionEvent e)
    {
        System.out.println("VSV-LOG | Long press");
    }

    //OnGestureListener interface
    @Override
    public boolean onScroll(MotionEvent e1,MotionEvent e2,float velocityX,float velocityY)
    {
        System.out.println("VSV-LOG | Scroll");
        uninitialize();
        System.exit(0);
        return true;    
    }

    //OnGestureListener interface
    @Override
    public void onShowPress(MotionEvent e)
    {
    }

    //OnGestureListener interface
    @Override
    public boolean onSingleTapUp(MotionEvent e)
    {
        return true;    
    }

    // GLSurfaceView.Renderer interface
    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config)
    {
        String version = gl.glGetString(GL10.GL_VERSION);
        System.out.println("VSV-LOG | OpenGL-ES Version:-"+ version);

        String glsl_version = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
        System.out.println("VSV-LOG | GLSL Version:-"+ glsl_version);

        initialize(gl);
    }

    // GLSurfaceView.Renderer interface
    @Override
    public void onSurfaceChanged(GL10 unused, int width ,int height)
    {
        resize(width,height);
    }


    // GLSurfaceView.Renderer interface
    @Override
    public void onDrawFrame(GL10 unused)
    {
        draw();
    }

    private void initializeData()
    {
        light[0]=new GLESLightMaterial();
        light[1]=new GLESLightMaterial();

        light[0].LightAmbient=new float[]{0.0f,0.0f,0.0f,1.0f};
        light[0].LightDefuse=new float[]{1.0f,0.0f,0.0f,1.0f};
        light[0].LightSpecular=new float[]{1.0f,0.0f,0.0f,1.0f};
        light[0].LightPosition=new float[]{2.0f,0.0f,0.0f,1.0f};

        light[1].LightAmbient=new float[]{0.0f,0.0f,0.0f,1.0f};
        light[1].LightDefuse=new float[]{0.0f,0.0f,1.0f,1.0f};
        light[1].LightSpecular=new float[]{0.0f,0.0f,1.0f,1.0f};
        light[1].LightPosition=new float[]{-2.0f,0.0f,0.0f,1.0f};
    }

    private void initialize(GL10 gl)
    {
        
        initializeData();

        int[] iShaderCompiledStatus= new int[1];
        int[] iShaderProgramLinkStatus= new int[1];
        int[] iInfoLogLength = new int[1];
        String szInfoLog=null;

        vertexShaderObject=GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);

        final String vertexShaderSourceCode = String.format(
            "#version 320 es" +
            "\n" +
            "in vec4 vPosition;" +
            "in vec3 vNormal;\n"+

            "uniform mat4 u_modelMatrix;\n"+
            "uniform mat4 u_viewMatrix;\n"+
            "uniform mat4 u_projectionMatrix;\n"+

            "uniform vec3 u_lightAmbient[2];\n"+
            "uniform vec3 u_lightDiffuse[2];\n"+
            "uniform vec3 u_lightSpecular[2];\n"+
            "uniform vec4 u_lightPosition[2];\n"+

            "uniform vec3 u_materialAmbient;\n"+
            "uniform vec3 u_materialDiffuse;\n"+
            "uniform vec3 u_materialSpecular;\n"+
            "uniform float u_materialShininess;\n"+

            "uniform int u_lKeyPressed;\n"+

            "out vec3 out_phongADS_Light;\n"+

            "uniform mat4 u_mvpMatrix;" +
            
            "void main(void)" +
            "{" +

            "if (u_lKeyPressed == 1)\n"+
            "{\n"+

                "vec4 eyeCoord = u_viewMatrix * u_modelMatrix * vPosition;\n"+
                "vec3 viewVector = normalize( -eyeCoord.xyz);\n"+
                "vec3 transformedNormal = normalize( mat3(u_viewMatrix * u_modelMatrix) * vNormal );\n"+
            
                "vec3 lightDirection[2];\n"+
                "vec3 reflectionVector[2];\n"+

                "vec3 ambient[2];\n"+
                "vec3 diffuse[2];\n"+
                "vec3 specular[2];\n"+

                "for (int i = 0; i < 2; i++)\n"+
                "{\n"+

                    "lightDirection[i] = normalize( vec3( u_lightPosition[i] - eyeCoord ) );\n"+

                    "reflectionVector[i] = reflect( -lightDirection[i], transformedNormal );\n"+

                    "ambient[i]= u_lightAmbient[i] * u_materialAmbient;\n"+

                    "diffuse[i] = u_lightDiffuse[i] * u_materialDiffuse * max( dot(lightDirection[i] , transformedNormal), 0.0);\n"+

                    "specular[i] = u_lightSpecular[i] * u_materialSpecular * pow( max( dot(reflectionVector[i] , viewVector), 0.0f) , u_materialShininess);\n"+

                    "out_phongADS_Light += ambient[i] + diffuse[i] + specular[i];\n"+

                "}\n"+

            "}\n"+
            "else\n"+
            "{\n"+

                "out_phongADS_Light = vec3(1.0f, 1.0f, 1.0f);\n"+

            "}\n"+

                "gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix * vPosition;\n"+
            "}"
        );

        GLES32.glShaderSource(vertexShaderObject,vertexShaderSourceCode);

        GLES32.glCompileShader(vertexShaderObject);

        GLES32.glGetShaderiv(vertexShaderObject,GLES32.GL_COMPILE_STATUS,iShaderCompiledStatus,0);

        if(iShaderCompiledStatus[0]==GLES32.GL_FALSE)
        {
            GLES32.glGetShaderiv(vertexShaderObject,GLES32.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
            if (iInfoLogLength[0]>0)
            {
                szInfoLog=GLES32.glGetShaderInfoLog(vertexShaderObject);
                System.out.println("VSV-LOG | Vertex Shader compilation Log="+szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }

        iShaderCompiledStatus[0]=0;
        iInfoLogLength[0]=0;
        szInfoLog=null;

        fragmentShaderObject=GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);

        final String  fragmentShaderSourceCode= String.format(
            "#version 320 es" +
            "\n" +
            "precision highp float;"+
            "out vec4 fragColor;" +
            "in vec3 out_phongADS_Light;\n"+
            "void main(void)" +
            "{"+
                "fragColor = vec4(out_phongADS_Light,1.0f);" +
            "}"
        );

        GLES32.glShaderSource(fragmentShaderObject,fragmentShaderSourceCode);

        GLES32.glCompileShader(fragmentShaderObject);

        GLES32.glGetShaderiv(fragmentShaderObject,GLES32.GL_COMPILE_STATUS,iShaderCompiledStatus,0);

        if(iShaderCompiledStatus[0]==GLES32.GL_FALSE)
        {
            GLES32.glGetShaderiv(fragmentShaderObject,GLES32.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
            if (iInfoLogLength[0]>0)
            {
                szInfoLog=GLES32.glGetShaderInfoLog(fragmentShaderObject);
                System.out.println("VSV-LOG | Fragment Shader compilation Log="+szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }

        iShaderCompiledStatus[0]=0;
        iInfoLogLength[0]=0;
        szInfoLog=null;

        shaderProgramObject=GLES32.glCreateProgram();

        GLES32.glAttachShader(shaderProgramObject,vertexShaderObject);
        GLES32.glAttachShader(shaderProgramObject,fragmentShaderObject);

        GLES32.glBindAttribLocation(shaderProgramObject,GLESMacros.VSV_ATTRIBUTE_POSITION,"vPosition");
        GLES32.glBindAttribLocation(shaderProgramObject,GLESMacros.VSV_ATTRIBUTE_NORMAL,"vNormal");

        GLES32.glLinkProgram(shaderProgramObject);
        GLES32.glGetProgramiv(shaderProgramObject,GLES32.GL_LINK_STATUS,iShaderProgramLinkStatus,0);
        if (iShaderProgramLinkStatus[0]==GLES32.GL_FALSE)
        {
             GLES32.glGetShaderiv(shaderProgramObject,GLES32.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
            if (iInfoLogLength[0]>0)
            {
                szInfoLog=GLES32.glGetShaderInfoLog(shaderProgramObject);
                System.out.println("VSV-LOG | Shader program link Log="+szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }

        modelMatrixUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_modelMatrix");
        viewMatrixUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_viewMatrix");
        projectionlMatrixUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_projectionMatrix");

        
        lightAmbientUniform[0] = GLES32.glGetUniformLocation(shaderProgramObject, "u_lightAmbient[0]");
        lightDiffuseUniform[0] =GLES32.glGetUniformLocation(shaderProgramObject, "u_lightDiffuse[0]");
        lightSpecularUniform[0] =GLES32.glGetUniformLocation(shaderProgramObject,"u_lightSpecular[0]");
        lightPositionUniform[0] = GLES32.glGetUniformLocation(shaderProgramObject, "u_lightPosition[0]");
        
        lightAmbientUniform[1] =GLES32.glGetUniformLocation(shaderProgramObject, "u_lightAmbient[1]");
        lightDiffuseUniform[1] =GLES32.glGetUniformLocation(shaderProgramObject, "u_lightDiffuse[1]");
        lightSpecularUniform[1] =GLES32.glGetUniformLocation(shaderProgramObject,"u_lightSpecular[1]");
        lightPositionUniform[1] =GLES32.glGetUniformLocation(shaderProgramObject,"u_lightPosition[1]");

        materialAmbientUniform =GLES32.glGetUniformLocation(shaderProgramObject, "u_materialAmbient");
        materialDiffuseUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_materialDiffuse");
        materialSpecularUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_materialSpecular");
        materialShininessUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_materialShininess");

        LKeyPressedUniform =GLES32.glGetUniformLocation(shaderProgramObject,  "u_lKeyPressed");


        final float pyramidVertices[]=new float[]{
                                               0.0f, 1.0f, 0.0f,
                                            -1.0f, -1.0f, 1.0f,
                                            1.0f, -1.0f, 1.0f,

                                            0.0f, 1.0f, 0.0f,
                                            1.0f, -1.0f, 1.0f,
                                            1.0f, -1.0f, -1.0f,

                                            0.0f, 1.0f, 0.0f,
                                            1.0f, -1.0f, -1.0f,
                                            -1.0f, -1.0f, -1.0f,

                                            0.0f, 1.0f, 0.0f,
                                            -1.0f, -1.0f, -1.0f,
                                            -1.0f, -1.0f, 1.0f
                                            };  

        final float pyramidColor[]=new float[]{
                                           1.0f, 0.0f, 0.0f,
                                            0.0f, 1.0f, 0.0f,
                                            0.0f, 0.0f, 1.0f,

                                            1.0f, 0.0f, 0.0f,
                                            0.0f, 0.0f, 1.0f,
                                            0.0f, 1.0f, 0.0f,

                                            1.0f, 0.0f, 0.0f,
                                            0.0f, 1.0f, 0.0f,
                                            0.0f, 0.0f, 1.0f,

                                            1.0f, 0.0f, 0.0f,
                                            0.0f, 0.0f, 1.0f,
                                            0.0f, 1.0f, 0.0f
                                            };
        
        final float pyramidNormal[]=new float[]{
                                           0.0f, 0.447214f, 0.894427f,
                                            0.0f, 0.447214f, 0.894427f,
                                            0.0f, 0.447214f, 0.894427f,

                                            0.894427f, 0.447214f, 0.0f,
                                            0.894427f, 0.447214f, 0.0f,
                                            0.894427f, 0.447214f, 0.0f,

                                            0.0f, 0.447214f, -0.894427f,
                                            0.0f, 0.447214f, -0.894427f,
                                            0.0f, 0.447214f, -0.894427f,

                                            -0.894427f, 0.447214f, 0.0f,
                                            -0.894427f, 0.447214f, 0.0f,
                                            -0.894427f, 0.447214f, 0.0f
                                            };

        ByteBuffer byteBuffer;
        FloatBuffer verticesBuffer;
        FloatBuffer colorBuffer;
        FloatBuffer normalBuffer;

        GLES32.glGenVertexArrays(1,vao_pyramid,0);
        GLES32.glBindVertexArray(vao_pyramid[0]);

            GLES32.glGenBuffers(1,vbo_position,0);
            GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_position[0]);

                byteBuffer=ByteBuffer.allocateDirect(pyramidVertices.length * 4);// 4 is size of float
                byteBuffer.order(ByteOrder.nativeOrder());
                verticesBuffer=byteBuffer.asFloatBuffer();

                verticesBuffer.put(pyramidVertices);
                verticesBuffer.position(0);

                GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                                    pyramidVertices.length * 4, 
                                    verticesBuffer,
                                    GLES32.GL_STATIC_DRAW);

                GLES32.glVertexAttribPointer(GLESMacros.VSV_ATTRIBUTE_POSITION,
                                            3,
                                            GLES32.GL_FLOAT,
                                            false,
                                            0,
                                            0);
                
                GLES32.glEnableVertexAttribArray(GLESMacros.VSV_ATTRIBUTE_POSITION);

            GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);

            GLES32.glGenBuffers(1,vbo_normal,0);
            GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_normal[0]);

                byteBuffer=ByteBuffer.allocateDirect(pyramidNormal.length * 4);// 4 is size of float
                byteBuffer.order(ByteOrder.nativeOrder());
                normalBuffer=byteBuffer.asFloatBuffer();

                normalBuffer.put(pyramidNormal);
                normalBuffer.position(0);

                GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                                    pyramidNormal.length * 4, 
                                    normalBuffer,
                                    GLES32.GL_STATIC_DRAW);

                GLES32.glVertexAttribPointer(GLESMacros.VSV_ATTRIBUTE_NORMAL,
                                            3,
                                            GLES32.GL_FLOAT,
                                            false,
                                            0,
                                            0);
                
                GLES32.glEnableVertexAttribArray(GLESMacros.VSV_ATTRIBUTE_NORMAL);

            GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
            
        GLES32.glBindVertexArray(0);
        
        GLES32.glEnable(GLES32.GL_DEPTH_TEST);
        GLES32.glDepthFunc(GLES32.GL_LEQUAL);

        GLES32.glClearColor(0.0f,0.0f,0.0f,1.0f);
	    GLES32.glClearDepthf(1.0f);

        Matrix.setIdentityM(perspectiveProjectionMatrix,0);
    }

    private void resize(int width ,int height)
    {
        GLES32.glViewport(0,0,width,height);
        Matrix.perspectiveM(perspectiveProjectionMatrix,0,
                            45.0f,
                            (float)width/(float)height,
                            0.1f,
                            100.0f);
    }

    public void draw()
    {
        GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);

        GLES32.glUseProgram(shaderProgramObject);

            float modelMatrix[] = new float[16];
            float viewMatrix[] = new float[16];

            float translateMatrix[] = new float[16];
            float rotateMatrix[] = new float[16];
            float scaleMatrix[] = new float[16];


            Matrix.setIdentityM(modelMatrix,0);
            Matrix.setIdentityM(viewMatrix,0);
            Matrix.setIdentityM(translateMatrix,0);
            Matrix.setIdentityM(rotateMatrix,0);

            Matrix.translateM(translateMatrix,0,0.0f,0.0f,-4.0f);
            Matrix.rotateM(rotateMatrix,0,angle,0.0f,1.0f,0.0f);
            Matrix.multiplyMM(modelMatrix,0,translateMatrix,0,rotateMatrix,0);

            GLES32.glUniformMatrix4fv(modelMatrixUniform,1,false,modelMatrix,0);

            GLES32.glUniformMatrix4fv(viewMatrixUniform,1,false,viewMatrix,0);
                
            GLES32.glUniformMatrix4fv(projectionlMatrixUniform,1,false,perspectiveProjectionMatrix,0);

                if (doubleTapLights == true)
                {
                    GLES32.glUniform1i(LKeyPressedUniform, 1);

                    int i;
                    for (i = 0; i < 2; i++)
                    {
                        GLES32.glUniform3fv(lightAmbientUniform[i], 1, light[i].LightAmbient,0);
                        GLES32.glUniform3fv(lightDiffuseUniform[i], 1, light[i].LightDefuse,0);
                        GLES32.glUniform3fv(lightSpecularUniform[i], 1, light[i].LightSpecular,0);

                        GLES32.glUniform4fv(lightPositionUniform[i], 1, light[i].LightPosition,0);//posiitional light
                    }

                    GLES32.glUniform3fv(materialAmbientUniform, 1, materialAmbient,0);
                    GLES32.glUniform3fv(materialDiffuseUniform, 1, materilaDefuse,0);
                    GLES32.glUniform3fv(materialSpecularUniform, 1, materialSpecular,0);

                    GLES32.glUniform1f(materialShininessUniform, materialShininess);
                }
                else
                {
                    GLES32.glUniform1i(LKeyPressedUniform, 0);
                }
            
            GLES32.glBindVertexArray(vao_pyramid[0]);
                GLES32.glDrawArrays(GLES32.GL_TRIANGLES,0,12);
            GLES32.glBindVertexArray(0);

        GLES32.glUseProgram(0);    
        
       if (singleTapAnimate ==true)
       {
            angle+=3;
       }
        requestRender();//like render/flush/swap Double Buffer
    }

    private void uninitialize()
    {
        if(vao_pyramid[0] != 0)
       {
           GLES32.glDeleteVertexArrays(1,vao_pyramid,0);
           vao_pyramid[0]=0;
       }

       if(vbo_position[0]!=0)
       {
           GLES32.glDeleteBuffers(1,vbo_position,0);
           vbo_position[0]=0;
       }

        if(vbo_normal[0]!=0)
        {
            GLES32.glDeleteBuffers(1,vbo_normal,0);
            vbo_normal[0]=0;
        }

        if(shaderProgramObject != 0 )
        {
            if(fragmentShaderObject != 0)
            {
                GLES32.glDetachShader(shaderProgramObject,fragmentShaderObject);
                GLES32.glDeleteShader(fragmentShaderObject);
                fragmentShaderObject=0;
            }
                
            if(vertexShaderObject != 0)
            {
                GLES32.glDetachShader(shaderProgramObject,vertexShaderObject);
                GLES32.glDeleteShader(vertexShaderObject);
                vertexShaderObject=0;
            }
            
            GLES32.glDeleteProgram(shaderProgramObject);
            shaderProgramObject=0;
        }
    }
}