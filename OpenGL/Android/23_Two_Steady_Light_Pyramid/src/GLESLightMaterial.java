package com.example.two_steady_light_pyramid;


public class GLESLightMaterial {
    public float LightAmbient[];
	public float LightDefuse[];
	public float LightSpecular[];
	public float LightPosition[];
}
