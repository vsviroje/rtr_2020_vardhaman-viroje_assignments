package com.example.tesselation;

import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;

import android.content.Context;
import android.view.MotionEvent;

import android.opengl.GLSurfaceView;
import android.opengl.GLES32;//for opengl embedded system 3.2 version core API support(PP)

import javax.microedition.khronos.opengles.GL10;//OpenGL ES 1.0 for legacy API support(FFP)
import javax.microedition.khronos.egl.EGLConfig;//embedded GL Bridging API to provide bridge between OpenGL ES's NDK and OpenGL ES's SDK 

import android.opengl.Matrix;

//non blocking IO buffer
import java.nio.ByteBuffer;
import java.nio.ByteOrder; // used to set/get bigEndian/littleEndian of byte
import java.nio.FloatBuffer;

public class GLESView extends GLSurfaceView implements OnGestureListener,OnDoubleTapListener,GLSurfaceView.Renderer
{
    private GestureDetector gestureDetector;
    private final Context context;//final is similar to constant

    private int vertexShaderObject;
    private int fragmentShaderObject;
    private int shaderProgramObject;

    private int tessellationControlShaderObject;
    private int tessellationEvalutionShaderObject;

    private int[] vao =new int[1];
    private int[] vbo_position =new int[1];

    private int mvpUniform;

    private int numOfSegmentsUniform;
    private int numOfStripsUniform;
    private int lineColorUniform;

    private int uiNumOfSegment;

    private float perspectiveProjectionMatrix[]=new float[16]; //4*4 matrix

    public GLESView(Context drawingContext)
    {
        super(drawingContext);

        System.out.println("VSV-LOG | GLESView constructor");

        setEGLContextClientVersion(3);// set EGLContext to supporting OpenGL-ES version 3
        
        setRenderer(this);

        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);//just like invalidateRect

        gestureDetector = new GestureDetector(drawingContext,this,null,false);

        gestureDetector.setOnDoubleTapListener(this);

        context=drawingContext;
    }

    //Triggers all gesture and tap events
    @Override 
    public boolean onTouchEvent(MotionEvent event)
    {
        //code
        int eventaction = event.getAction();
        if(!gestureDetector.onTouchEvent(event))
        {
            super.onTouchEvent(event);
        }
        return (true);
    }

    //OnDoubleTapListener interface
    @Override
    public boolean onDoubleTap(MotionEvent e)
    {
        System.out.println("VSV-LOG | Double Tap");
        uiNumOfSegment++;
        if(uiNumOfSegment >=50)
        {
            uiNumOfSegment=50;
        }
        return true;
    }

    //OnDoubleTapListener interface
    @Override
    public boolean onDoubleTapEvent(MotionEvent e)
    {
        return true;
    }

    //OnDoubleTapListener interface
    @Override
    public boolean onSingleTapConfirmed(MotionEvent e)
    {
        System.out.println("VSV-LOG | Single Tap");
       
        return true;    
    }

    //OnGestureListener interface
    @Override
    public boolean onDown(MotionEvent e)
    {
        return true;    
    }

    //OnGestureListener interface
    @Override
    public boolean onFling(MotionEvent e1,MotionEvent e2,float velocityX,float velocityY)
    {
        return true;    
    }

    //OnGestureListener interface
    @Override
    public void onLongPress(MotionEvent e)
    {
         uiNumOfSegment--;
        if(uiNumOfSegment <= 0)
        {
            uiNumOfSegment=1;
        }
        System.out.println("VSV-LOG | Long press");
    }

    //OnGestureListener interface
    @Override
    public boolean onScroll(MotionEvent e1,MotionEvent e2,float velocityX,float velocityY)
    {
        System.out.println("VSV-LOG | Scroll");
        uninitialize();
        System.exit(0);
        return true;    
    }

    //OnGestureListener interface
    @Override
    public void onShowPress(MotionEvent e)
    {
    }

    //OnGestureListener interface
    @Override
    public boolean onSingleTapUp(MotionEvent e)
    {
        return true;    
    }

    // GLSurfaceView.Renderer interface
    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config)
    {
        String version = gl.glGetString(GL10.GL_VERSION);
        System.out.println("VSV-LOG | OpenGL-ES Version:-"+ version);

        String glsl_version = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
        System.out.println("VSV-LOG | GLSL Version:-"+ glsl_version);

        initialize(gl);
    }

    // GLSurfaceView.Renderer interface
    @Override
    public void onSurfaceChanged(GL10 unused, int width ,int height)
    {
        resize(width,height);
    }


    // GLSurfaceView.Renderer interface
    @Override
    public void onDrawFrame(GL10 unused)
    {
        draw();
    }

    private void initialize(GL10 gl)
    {
        int[] iShaderCompiledStatus= new int[1];
        int[] iShaderProgramLinkStatus= new int[1];
        int[] iInfoLogLength = new int[1];
        String szInfoLog=null;

        vertexShaderObject=GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);

        final String vertexShaderSourceCode = String.format(
            "#version 320 es" +
            "\n" +
            "precision highp float;"+
            "in vec4 vPosition;" +
            "void main(void)" +
            "{" +
                "gl_Position= vPosition;" +
            "}"
        );

        GLES32.glShaderSource(vertexShaderObject,vertexShaderSourceCode);

        GLES32.glCompileShader(vertexShaderObject);

        GLES32.glGetShaderiv(vertexShaderObject,GLES32.GL_COMPILE_STATUS,iShaderCompiledStatus,0);

        if(iShaderCompiledStatus[0]==GLES32.GL_FALSE)
        {
            GLES32.glGetShaderiv(vertexShaderObject,GLES32.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
            if (iInfoLogLength[0]>0)
            {
                szInfoLog=GLES32.glGetShaderInfoLog(vertexShaderObject);
                System.out.println("VSV-LOG | Vertex Shader compilation Log="+szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }

        iShaderCompiledStatus[0]=0;
        iInfoLogLength[0]=0;
        szInfoLog=null;

        tessellationControlShaderObject=GLES32.glCreateShader(GLES32.GL_TESS_CONTROL_SHADER);

        final String tessellationControlShaderSourceCode = String.format(
            "#version 320 es" +
            "\n" +
            "precision highp float;"+
            "layout(vertices = 4) out;" +
            "uniform int numOfSegment;" +
            "uniform int numOfStrip;" +

            "void main(void)" +
            "{" +
                "gl_out[gl_InvocationID].gl_Position = gl_in[gl_InvocationID].gl_Position;" +
                "gl_TessLevelOuter[0] = float(numOfStrip);" +
                "gl_TessLevelOuter[1] = float(numOfSegment);" +
            "}"
        );

        GLES32.glShaderSource(tessellationControlShaderObject,tessellationControlShaderSourceCode);

        GLES32.glCompileShader(tessellationControlShaderObject);

        GLES32.glGetShaderiv(tessellationControlShaderObject,GLES32.GL_COMPILE_STATUS,iShaderCompiledStatus,0);

        if(iShaderCompiledStatus[0]==GLES32.GL_FALSE)
        {
            GLES32.glGetShaderiv(tessellationControlShaderObject,GLES32.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
            if (iInfoLogLength[0]>0)
            {
                szInfoLog=GLES32.glGetShaderInfoLog(tessellationControlShaderObject);
                System.out.println("VSV-LOG | tessellation Control Shader compilation Log="+szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }

        iShaderCompiledStatus[0]=0;
        iInfoLogLength[0]=0;
        szInfoLog=null;

        tessellationEvalutionShaderObject=GLES32.glCreateShader(GLES32.GL_TESS_EVALUATION_SHADER);

        final String  tessellationEvalutionShaderSourceCode= String.format(
            "#version 320 es" +
            "\n" +
            "precision highp float;"+
            "layout (isolines) in;" +
            "uniform mat4 u_mvpMatrix;" +
        
            "void main(void)" +
            "{" +
                "float tessCoord = gl_TessCoord.x;" +
                
                "vec3 p0 = gl_in[0].gl_Position.xyz;" +
                "vec3 p1 = gl_in[1].gl_Position.xyz;" +
                "vec3 p2 = gl_in[2].gl_Position.xyz;" +
                "vec3 p3 = gl_in[3].gl_Position.xyz;" +

                "vec3 p = (p0 * (1.0-tessCoord) * (1.0-tessCoord) * (1.0-tessCoord)) ;" +
                "p = p + (p1 * 3.0 * tessCoord * (1.0-tessCoord) * (1.0-tessCoord) ) ;" +
                "p = p + (p2 * 3.0 * tessCoord * tessCoord * (1.0-tessCoord) ) ;" +
                "p = p + (p3 * tessCoord * tessCoord * tessCoord ) ;" +

                "gl_Position = u_mvpMatrix * vec4( p,1.0) ;" +
            "}"
        );

        GLES32.glShaderSource(tessellationEvalutionShaderObject,tessellationEvalutionShaderSourceCode);

        GLES32.glCompileShader(tessellationEvalutionShaderObject);

        GLES32.glGetShaderiv(tessellationEvalutionShaderObject,GLES32.GL_COMPILE_STATUS,iShaderCompiledStatus,0);

        if(iShaderCompiledStatus[0]==GLES32.GL_FALSE)
        {
            GLES32.glGetShaderiv(tessellationEvalutionShaderObject,GLES32.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
            if (iInfoLogLength[0]>0)
            {
                szInfoLog=GLES32.glGetShaderInfoLog(tessellationEvalutionShaderObject);
                System.out.println("VSV-LOG | tessellation Evalution Shader compilation Log="+szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }


        iShaderCompiledStatus[0]=0;
        iInfoLogLength[0]=0;
        szInfoLog=null;

        fragmentShaderObject=GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);

        final String  fragmentShaderSourceCode= String.format(
            "#version 320 es" +
            "\n" +
            "precision highp float;"+
            "out vec4 fragColor;" +
            "uniform vec4 lineColor;" +
            
            "void main(void)" +
            "{" +
                "fragColor=lineColor;" +
            "}"
        );

        GLES32.glShaderSource(fragmentShaderObject,fragmentShaderSourceCode);

        GLES32.glCompileShader(fragmentShaderObject);

        GLES32.glGetShaderiv(fragmentShaderObject,GLES32.GL_COMPILE_STATUS,iShaderCompiledStatus,0);

        if(iShaderCompiledStatus[0]==GLES32.GL_FALSE)
        {
            GLES32.glGetShaderiv(fragmentShaderObject,GLES32.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
            if (iInfoLogLength[0]>0)
            {
                szInfoLog=GLES32.glGetShaderInfoLog(fragmentShaderObject);
                System.out.println("VSV-LOG | Fragment Shader compilation Log="+szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }

        iShaderCompiledStatus[0]=0;
        iInfoLogLength[0]=0;
        szInfoLog=null;

        shaderProgramObject=GLES32.glCreateProgram();

        GLES32.glAttachShader(shaderProgramObject,vertexShaderObject);
        GLES32.glAttachShader(shaderProgramObject,tessellationControlShaderObject);
        GLES32.glAttachShader(shaderProgramObject,tessellationEvalutionShaderObject);
        GLES32.glAttachShader(shaderProgramObject,fragmentShaderObject);

        GLES32.glBindAttribLocation(shaderProgramObject,GLESMacros.VSV_ATTRIBUTE_POSITION,"vPosition");

        GLES32.glLinkProgram(shaderProgramObject);
        GLES32.glGetProgramiv(shaderProgramObject,GLES32.GL_LINK_STATUS,iShaderProgramLinkStatus,0);
        if (iShaderProgramLinkStatus[0]==GLES32.GL_FALSE)
        {
             GLES32.glGetShaderiv(shaderProgramObject,GLES32.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
            if (iInfoLogLength[0]>0)
            {
                szInfoLog=GLES32.glGetShaderInfoLog(shaderProgramObject);
                System.out.println("VSV-LOG | Shader program link Log="+szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }

        mvpUniform=GLES32.glGetUniformLocation(shaderProgramObject,"u_mvpMatrix");
        numOfSegmentsUniform=GLES32.glGetUniformLocation(shaderProgramObject,"numOfSegment");
        numOfStripsUniform=GLES32.glGetUniformLocation(shaderProgramObject,"numOfStrip");
        lineColorUniform=GLES32.glGetUniformLocation(shaderProgramObject,"lineColor");


        final float vertices[]=new float[]{
                                            -1.0f,-1.0f,
                                            -0.5f,1.0f,
                                            0.5f,-1.0f,
                                            1.0f,1.0f
                                                };
        
            

        ByteBuffer byteBuffer;
        GLES32.glGenVertexArrays(1,vao,0);
        GLES32.glBindVertexArray(vao[0]);

            GLES32.glGenBuffers(1,vbo_position,0);
            GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_position[0]);

                byteBuffer=ByteBuffer.allocateDirect(vertices.length * 4);// 4 is size of float
                byteBuffer.order(ByteOrder.nativeOrder());
                FloatBuffer verticesBuffer=byteBuffer.asFloatBuffer();

                verticesBuffer.put(vertices);
                verticesBuffer.position(0);

                GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                                    vertices.length * 4, 
                                    verticesBuffer,
                                    GLES32.GL_STATIC_DRAW);

                GLES32.glVertexAttribPointer(GLESMacros.VSV_ATTRIBUTE_POSITION,
                                            2,
                                            GLES32.GL_FLOAT,
                                            false,
                                            0,
                                            0);
                
                GLES32.glEnableVertexAttribArray(GLESMacros.VSV_ATTRIBUTE_POSITION);

            GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
            
        GLES32.glBindVertexArray(0);

        GLES32.glEnable(GLES32.GL_DEPTH_TEST);
        GLES32.glDepthFunc(GLES32.GL_LEQUAL);

        GLES32.glClearColor(0.0f,0.0f,0.0f,1.0f);
	    uiNumOfSegment = 1;

        Matrix.setIdentityM(perspectiveProjectionMatrix,0);
    }

    private void resize(int width ,int height)
    {
        GLES32.glViewport(0,0,width,height);
        Matrix.perspectiveM(perspectiveProjectionMatrix,0,
                            45.0f,
                            (float)width/(float)height,
                            0.1f,
                            100.0f);
    }

    public void draw()
    {
        final float yellowColor[]=new float[]{
                                          1.0f,1.0f,0.0f,1.0f
                                                };
        GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);

        GLES32.glUseProgram(shaderProgramObject);

            float modelViewMatrix[] = new float[16];
            float modelViewProjectionMatrix[] = new float[16];
            float translateMatrix[] = new float[16];


            Matrix.setIdentityM(modelViewMatrix,0);
            Matrix.setIdentityM(modelViewProjectionMatrix,0);
            Matrix.setIdentityM(translateMatrix,0);

            Matrix.translateM(translateMatrix,0,0.0f,0.0f,-4.0f);
            Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,translateMatrix,0);

            Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);

            GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);

            GLES32.glUniform1i(numOfSegmentsUniform,uiNumOfSegment);
            GLES32.glUniform1i(numOfStripsUniform,1);

            GLES32.glUniform4fv(lineColorUniform, 1, yellowColor,0);//posiitional light
            
            GLES32.glBindVertexArray(vao[0]);
                GLES32.glPatchParameteri(GLES32.GL_PATCH_VERTICES,4);
                GLES32.glDrawArrays(GLES32.GL_PATCHES,0,4);
            GLES32.glBindVertexArray(0);

        GLES32.glUseProgram(0);    
    
        requestRender();//like render/flush/swap Double Buffer
    }

    private void uninitialize()
    {
       if(vao[0] != 0)
       {
           GLES32.glDeleteVertexArrays(1,vao,0);
           vao[0]=0;
       }

       if(vbo_position[0]!=0)
       {
           GLES32.glDeleteBuffers(1,vbo_position,0);
           vbo_position[0]=0;
       }


        if(shaderProgramObject != 0 )
        {
            if(fragmentShaderObject != 0)
            {
                GLES32.glDetachShader(shaderProgramObject,fragmentShaderObject);
                GLES32.glDeleteShader(fragmentShaderObject);
                fragmentShaderObject=0;
            }
                
            if(vertexShaderObject != 0)
            {
                GLES32.glDetachShader(shaderProgramObject,vertexShaderObject);
                GLES32.glDeleteShader(vertexShaderObject);
                vertexShaderObject=0;
            }

             if(tessellationControlShaderObject != 0)
            {
                GLES32.glDetachShader(shaderProgramObject,tessellationControlShaderObject);
                GLES32.glDeleteShader(tessellationControlShaderObject);
                tessellationControlShaderObject=0;
            }

             if(tessellationEvalutionShaderObject != 0)
            {
                GLES32.glDetachShader(shaderProgramObject,tessellationEvalutionShaderObject);
                GLES32.glDeleteShader(tessellationEvalutionShaderObject);
                tessellationEvalutionShaderObject=0;
            }
            
            GLES32.glDeleteProgram(shaderProgramObject);
            shaderProgramObject=0;
        }
    }
}