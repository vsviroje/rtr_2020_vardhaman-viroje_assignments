//
//  MyView.m
//  Window
//
//  Created by user205082 on 11/6/21.
//
#import<OpenGLES/ES3/gl.h>
#import<OpenGLES/ES3/glext.h>

#import "GLESView.h"
#import"vmath.h"
using namespace vmath;

enum{
    VSV_ATTRIBUTE_POSITION=0,
    VSV_ATTRIBUTE_COLOR,
    VSV_ATTRIBUTE_TEXCOORD,
    VSV_ATTRIBUTE_NORMAL,
};

@implementation GLESView
{
    @private
    //Embedded Apple GL
    EAGLContext *eaglContext;
    
    GLuint defaultFramebuffer;
    GLuint colorRenderbuffer;
    GLuint depthRenderbuffer;
    
    //core animation
    //CADisplayLink --- underlying GL technology might change in future,so we are not directly going to use CADisplayLink pointer
    //thats why `id` is used, it will provide underlying GL driver dynamically
    id displayLink;
    NSInteger animationFrameInterval;
    bool isAnimating;
    
    
    GLuint gVertexShaderObject;
    GLuint gFragmentShaderObject;
    GLuint gShaderProgramObject;

    GLuint vao_staticSmiley;
    GLuint vbo_smileyTexcoord;
    GLuint vbo_smileyPosition;
    GLuint vbo_smileysWhitelColor;


    GLuint mvpMatrixUniform;
    GLuint textureSamplerUniform;

    mat4 perspectiveProjectionMatrix;

    GLuint smileyTextureID;

    unsigned int PressedDigit;
    
    
}

-(id)initWithFrame:(CGRect)frame
{
    // code
    self = [super initWithFrame:frame];
    if (self)
    {
        //getting render mode display layer
        CAEAGLLayer *eaglLayer =(CAEAGLLayer *)[super layer];
        
        //set Opaque ie make display layer transparent
        [eaglLayer setOpaque:YES];
        
        //set Drawable Properties,define color format:RGBA8 and allow to store window in backing:YES
        [eaglLayer setDrawableProperties:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:NO],kEAGLDrawablePropertyRetainedBacking,
                                          kEAGLColorFormatRGBA8,kEAGLDrawablePropertyColorFormat,nil]];
        
        //create context of OpenFLES 3.0
        eaglContext = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES3];
        if(eaglContext == nil)
        {
            printf("\nOpenGLES Context creation failed");
            return (nil);
        }
    
        //set current context
        [EAGLContext setCurrentContext:eaglContext];
        
        //Create our framebuffer
        glGenFramebuffers(1, &defaultFramebuffer);
        glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
        //Create color Render buffer
            glGenRenderbuffers(1, &colorRenderbuffer);
            glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
        
            //Provide storage to color buffer using Apple/driver/native API
            [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
        
            //set attachment of render buffer ie for color buffer attachment is set to 0 th location of color attachment
            glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderbuffer);
            
            //get backing width and height of render buffer
            //getting width and height from display layer(ie eaglLayer)
            //because renderbuffer storage was provided by display layer
            GLint backingWidth;
            GLint backingHeight;
            glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
            glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
        
        //Create depth Render buffer
            glGenRenderbuffers(1, &depthRenderbuffer);
            glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
            
            //Provide storage to depth buffer using opengl API with width and height
            //IOS default 16 bit support for depth render buffer
            glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
            
            //set attachment of render buffer ie for depth buffer attachment of depth attachment
            glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
            
            if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
            {
                printf("\nGL_FRAMEBUFFER is not COMPLETE");
                [self uninitialize];
                return (nil);
            }
    
        animationFrameInterval = 60;//60 FPS //actually its default to 60 from IOS 8.2
        isAnimating = NO;
        
        printf("\nOpenGL Vender:%s",glGetString(GL_VENDOR));
        printf("\nOpenGL Render:%s",glGetString(GL_RENDERER));
        printf("\nOpenGL Version:%s",glGetString(GL_VERSION));
        printf("\nOpenGL GLSL Version:%s",glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        //***********shader initialize start***************************************
        
             GLint infoLogLength;
             GLint shaderCompileStatus;
             GLint shaderProgramLinkStatus;
             GLchar *szBuffer;
             GLsizei written;

             //Shader Code Start--------------------
                 gVertexShaderObject=glCreateShader(GL_VERTEX_SHADER);

                 const GLchar *vertexShaderSrc=
                 "#version 300 es"\
                 "\n"\
                 "in vec4 vPosition;"\

                 "in vec2 vTexCoord;"\
                 "out vec2 out_TexCoord;"\
                 
                 "in vec4 vColor;"\
                 "out vec4 out_color;"\
                 
                 "uniform mat4 u_mvpMatrix;"\

                 "void main(void)"\
                 "{"\
                     "out_TexCoord=vTexCoord;"\
                     "out_color=vColor;"\
                     "gl_Position=u_mvpMatrix * vPosition;"\
                 "}";

                 glShaderSource(gVertexShaderObject,
                                 1,
                                 (const GLchar **)&vertexShaderSrc,
                                 NULL);

                 glCompileShader(gVertexShaderObject);

                 glGetShaderiv(gVertexShaderObject,
                                 GL_COMPILE_STATUS,
                                 &shaderCompileStatus);

                 if(shaderCompileStatus ==GL_FALSE)
                 {
                     glGetShaderiv(gVertexShaderObject,
                                 GL_INFO_LOG_LENGTH,
                                 &infoLogLength);

                     if(infoLogLength > 0)
                     {
                         szBuffer=(GLchar *)malloc(sizeof(GLchar) * infoLogLength);

                         if(szBuffer!=NULL)
                         {
                             glGetShaderInfoLog(gVertexShaderObject,
                                                 infoLogLength,
                                                 &written,
                                                 szBuffer);
                             printf(
                                     "\nERROR | Vertex Shader Compilation Failed Log : %s",
                                     szBuffer);
                             free(szBuffer);
                             [self uninitialize];
                             return nil;
                         }
                     }
                 }

                 infoLogLength=0;
                 shaderCompileStatus=0;
                 shaderProgramLinkStatus=0;
                 szBuffer=NULL;
                 written=0;

                 gFragmentShaderObject=glCreateShader(GL_FRAGMENT_SHADER);

                 const GLchar *fragmentShaderSrc=
                 "#version 300 es"\
                 "\n"\
        "precision highp float;" \
        "precision mediump int;" \
                 "in vec2 out_TexCoord;"\
                 
                 "in vec4 out_color;"\
                 
                 "out vec4 fragColor;"\
                 
                 "uniform sampler2D u_texture_sampler;"\
                 
                 "void main(void)"\
                 "{"\
                     "if(out_TexCoord[0] != 0.0f){"\
                         "fragColor=texture(u_texture_sampler , out_TexCoord) ;"\
                     "}else{"\
                         "fragColor=out_color;"\
                     "}"\
                 "}";

                 glShaderSource(gFragmentShaderObject,
                                 1,
                                 (const GLchar**)&fragmentShaderSrc,
                                 NULL);
                 
                 glCompileShader(gFragmentShaderObject);

                 glGetShaderiv(gFragmentShaderObject,
                                 GL_COMPILE_STATUS,
                                 &shaderCompileStatus);

                 if(shaderCompileStatus==GL_FALSE)
                 {
                     glGetShaderiv(gFragmentShaderObject,
                                 GL_INFO_LOG_LENGTH,
                                 &infoLogLength);

                     if(infoLogLength > 0 )
                     {
                         szBuffer=(GLchar*)malloc(sizeof(GLchar) * infoLogLength);

                         glGetShaderInfoLog(gFragmentShaderObject,
                                                 infoLogLength,
                                                 &written,
                                                 szBuffer);

                         printf(
                                 "\nERROR | Fragment Shader Compilation failed log %s",
                                 szBuffer);

                         free(szBuffer);
                         [self uninitialize];
                         return nil;
                     }
                 }

                 infoLogLength=0;
                 shaderCompileStatus=0;
                 shaderProgramLinkStatus=0;
                 szBuffer=NULL;
                 written=0;

                 gShaderProgramObject=glCreateProgram();
                 glAttachShader(gShaderProgramObject,gVertexShaderObject);
                 glAttachShader(gShaderProgramObject,gFragmentShaderObject);

                 //Shader Attribute Binding Before Pre Linking of attached Shader
                     glBindAttribLocation(gShaderProgramObject,VSV_ATTRIBUTE_POSITION,"vPosition");
                     glBindAttribLocation(gShaderProgramObject,VSV_ATTRIBUTE_TEXCOORD,"vTexCoord");
                     glBindAttribLocation(gShaderProgramObject,VSV_ATTRIBUTE_COLOR,"vColor");

                 glLinkProgram(gShaderProgramObject);

                 glGetProgramiv(gShaderProgramObject,
                                 GL_LINK_STATUS,
                                 &shaderProgramLinkStatus);

                 if(shaderProgramLinkStatus==GL_FALSE)
                 {
                     glGetProgramiv(gShaderProgramObject,
                                     GL_INFO_LOG_LENGTH,
                                     &infoLogLength);
                     
                     if(infoLogLength > 0)
                     {
                         szBuffer=(GLchar *)malloc(sizeof(GLchar) * infoLogLength);
                         if(szBuffer!=NULL)
                         {
                             glGetProgramInfoLog(gShaderProgramObject,
                                                 infoLogLength,
                                                 &written,
                                                 szBuffer);
                             printf(
                                     "\nERROR | Shader Program Link Failed Log %s",
                                     szBuffer);
                             free(szBuffer);
                             [self uninitialize];
                             return nil;
                         }
                     }
                 }
                 
                 //Uniform variable Binding after post linking of attached shader
                 mvpMatrixUniform = glGetUniformLocation(gShaderProgramObject,
                                                         "u_mvpMatrix");
                 textureSamplerUniform = glGetUniformLocation(gShaderProgramObject,
                                                             "u_texture_sampler");

             //Shader Code End--------------------

                 
             //VAO and its respective VBO preparation
             const GLfloat rectangleVertices[]=
             {
                 -1.0f, -1.0f, 0.0f,
                 1.0f, -1.0f, 0.0f,
                 1.0f, 1.0f, 0.0f,
                 -1.0f, 1.0f, 0.0f
             };

             const GLfloat rectangleColor[]=
             {
                     1.0f,1.0f,1.0f,
                     1.0f,1.0f,1.0f,
                     1.0f,1.0f,1.0f,
                     1.0f,1.0f,1.0f
             };

             glGenVertexArrays(1 , &vao_staticSmiley);
             
             glBindVertexArray(vao_staticSmiley);

                 glGenBuffers(1 , &vbo_smileyPosition);
                 
                 glBindBuffer(GL_ARRAY_BUFFER , vbo_smileyPosition);

                     glBufferData(GL_ARRAY_BUFFER,
                                 sizeof(rectangleVertices),
                                 rectangleVertices,
                                 GL_STATIC_DRAW);
                     
                     glVertexAttribPointer(VSV_ATTRIBUTE_POSITION,
                                         3,
                                         GL_FLOAT,
                                         GL_FALSE,
                                         0,
                                         NULL);
                     
                     glEnableVertexAttribArray(VSV_ATTRIBUTE_POSITION);
                 
                 glBindBuffer(GL_ARRAY_BUFFER,0);

                 glGenBuffers(1,&vbo_smileyTexcoord);
                 
                 glBindBuffer(GL_ARRAY_BUFFER,vbo_smileyTexcoord);

                     glBufferData(GL_ARRAY_BUFFER,
                                 4*2*sizeof(GLfloat),
                                 NULL,
                                 GL_DYNAMIC_DRAW);

                     glVertexAttribPointer(VSV_ATTRIBUTE_TEXCOORD,
                                             2,
                                             GL_FLOAT,
                                             GL_FALSE,
                                             0,
                                             NULL);

                     glEnableVertexAttribArray(VSV_ATTRIBUTE_TEXCOORD);

                 glBindBuffer(GL_ARRAY_BUFFER,0);

                 glGenBuffers(1 , &vbo_smileysWhitelColor);

                 glBindBuffer(GL_ARRAY_BUFFER,vbo_smileysWhitelColor);
                 
                     glBufferData(GL_ARRAY_BUFFER,
                                 sizeof(rectangleColor),
                                 rectangleColor,
                                 GL_STATIC_DRAW);
                 
                     glVertexAttribPointer(VSV_ATTRIBUTE_COLOR,
                                             3,
                                             GL_FLOAT,
                                             GL_FALSE,
                                             0,
                                             NULL);
                 
                     glEnableVertexAttribArray(VSV_ATTRIBUTE_COLOR);
                 
                 glBindBuffer(GL_ARRAY_BUFFER,0);

             glBindVertexArray(0);


             //SetColor
             glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
             
             //Enable Depth steps
             glClearDepthf(1.0f);
             glEnable(GL_DEPTH_TEST);
             glDepthFunc(GL_LEQUAL);

             //Loading Texture
                smileyTextureID=[self loadTextureFromBMPFile:@"smiley" :@"bmp"];
                 if (smileyTextureID==0)
                 {
                     printf("\nERROR:smileyTextureID failed to load:");
                     [self uninitialize];
                     return nil;
                 }
             //Warmup resize call
             perspectiveProjectionMatrix=mat4::identity();

        //***********shader initialize end***************************************

        //register events/Gestures
        //1-register function
        //2-set Number Of Taps Required
        //3-set Number Of Touches Required
        //4-set Delegate
        //5-register/add Gesture Recognizer/event
        
        //Single tap event
        UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onSingleTap:)];
        [singleTapGestureRecognizer setNumberOfTapsRequired:1];
        [singleTapGestureRecognizer setNumberOfTouchesRequired:1];
        [singleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:singleTapGestureRecognizer];
        
        //double tap event
        UITapGestureRecognizer *doubleTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onDoubleTap:)];
        [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
        [doubleTapGestureRecognizer setNumberOfTouchesRequired:1];
        [doubleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:doubleTapGestureRecognizer];
        
        //this will block double tap to recognize as two single tap
        [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
        
        //Swipe event
        UISwipeGestureRecognizer *swipeGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(onSwipe:)];
        [swipeGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:swipeGestureRecognizer];
        
        //Long press
        UILongPressGestureRecognizer *longPressGestureRecognizer=[[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
        [longPressGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:longPressGestureRecognizer];
        
    }

    return  self;
}


-(GLuint)loadTextureFromBMPFile:(NSString *)imageFileName :(NSString *)extension
{
    ///code
    NSString *imageFileNameWithPath = [[NSBundle mainBundle]pathForResource:imageFileName ofType:extension];

    ///get UIImage representation from our image file.
    UIImage *bmpImage=[[UIImage alloc] initWithContentsOfFile:imageFileNameWithPath];
    if(!bmpImage)
    {
        NSLog(@"\nERROR:Failed to load image: %@.%@",imageFileName,extension );
        return 0;
    }

    CGImageRef cgImage = [bmpImage CGImage];
    
    ///get width and height of CGImage
    int imageWidth =(int)CGImageGetWidth(cgImage);
    int imageHeight =(int)CGImageGetHeight(cgImage);

    ///get core foundation data representation of our image
    CFDataRef imageData = CGDataProviderCopyData(CGImageGetDataProvider(cgImage)) ;

    ///convert CFDataRef formated data into (void*) ie generic data
    ///(Note will get pointer of imagedata's buffer)

    void* pixel =(void*) CFDataGetBytePtr(imageData);

    GLuint texture;
    glPixelStorei( GL_UNPACK_ALIGNMENT,
                    1
                );

    glGenTextures( 1,
                &texture
                );

    glBindTexture( GL_TEXTURE_2D,
                    texture
                );

    glTexParameteri( GL_TEXTURE_2D,
                    GL_TEXTURE_MAG_FILTER,
                    GL_LINEAR
                    );
    glTexParameteri( GL_TEXTURE_2D,
                    GL_TEXTURE_MIN_FILTER,
                    GL_LINEAR_MIPMAP_LINEAR
                   );

    glTexImage2D(GL_TEXTURE_2D,
                0,
                GL_RGBA,
                imageWidth,
                imageHeight,
                0,
                GL_RGBA,
                GL_UNSIGNED_BYTE,
                pixel);

    glGenerateMipmap(GL_TEXTURE_2D);

    //freeing the buffer
    CFRelease(imageData);

    return texture;

}

//this method is complursory to override
+(Class)layerClass
{
    //code
    return ([CAEAGLLayer class]);
}

/*
 Only override drawRect: if you perform custom drawing.
 An empty implementation adversely affects performance during animation.
//drawRect is only called for immediate mode
 - (void)drawRect:(CGRect)rect {
     // code
}
 */

//will override
//its resize()
-(void)layoutSubviews
{
    //code
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);

    //bind color Render buffer
        glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    
        //Provide storage to color buffer using Apple/driver/native API from resized drawable layer
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer *)[self layer]];
    
        //set attachment of render buffer ie for color buffer attachment is set to 0 th location of color attachment
        
        //get backing width and height of render buffer
        //getting width and height from display layer(ie eaglLayer)
        //because renderbuffer storage was provided by display layer
        GLint width;
        GLint height;
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
    
    //bind depth Render buffer
        glGenRenderbuffers(1, &depthRenderbuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
        
        //Provide storage to depth buffer using opengl API with resized drawable layer width and height
        //IOS default 16 bit support for depth render buffer
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
        
        //set attachment of render buffer ie for depth buffer attachment of depth attachment
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
        
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        {
            printf("\nGL_FRAMEBUFFER is not COMPLETE");
           
        }
    
    if(height < 0)
    {
        height = 1;
    }
        
    glViewport(0, 0, (GLsizei)width, (GLsizei)height);
    
    //************resize code for openGL start******************
    perspectiveProjectionMatrix=vmath::perspective(45.0f,
                                                        (GLfloat)width/(GLfloat)height,
                                                        0.1f,
                                                        100.0f);
    //************resize code for openGL end********************
    
    [self drawView:nil];
    
}

-(void)drawView:(id)sender
{
    //code
    [EAGLContext setCurrentContext:eaglContext];
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
    
    //************shader Draw start******************************
    
    GLfloat rectangleTexCoordPress1[]=
              {
                      0.0f,0.0f,
                      1.0f,0.0f,
                      1.0f,1.0f,
                      0.0f,1.0f
              };

              GLfloat rectangleTexCoordPress2[]=
              {
                      0.0f,0.0f,
                      0.5f,0.0f,
                      0.5f,0.5f,
                      0.0f,0.5f
              };

              GLfloat rectangleTexCoordPress3[]=
              {
                      0.0f,0.0f,
                      2.0f,0.0f,
                      2.0f,2.0f,
                      0.0f,2.0f
              };

              GLfloat rectangleTexCoordPress4[]=
              {
                      0.5f,0.5f,
                      0.5f,0.5f,
                      0.5f,0.5f,
                      0.5f,0.5f
              };

              GLfloat temp[1]={0.0f};

              //code
              mat4 modelViewMatrix;
              mat4 modelViewProjectionMatrix;
              mat4 translateMatrix;
              GLfloat *rectangleTexCoord;

              //code
              glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

              glUseProgram(gShaderProgramObject);

                  translateMatrix = vmath::translate(0.0f,0.0f,-6.0f);

                  modelViewMatrix = translateMatrix;

                  modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

                  glUniformMatrix4fv(mvpMatrixUniform,
                                  1,
                                  GL_FALSE,
                                  modelViewProjectionMatrix);

                  switch (PressedDigit)
                  {
                      case 1:
                      case 2:
                      case 3:
                      case 4:
                          glActiveTexture(GL_TEXTURE0);
                          glBindTexture(GL_TEXTURE_2D , smileyTextureID);
                          glUniform1i(textureSamplerUniform , 0);
                          break;
                      case 0:
                      default:
                              glBindTexture(GL_TEXTURE_2D , 0);
                              break;
                  }
                  
                  glBindVertexArray(vao_staticSmiley);
                      switch (PressedDigit)
                      {
                          case 1:
                          rectangleTexCoord = rectangleTexCoordPress1;
                          break;
                          
                          case 2:
                          rectangleTexCoord = rectangleTexCoordPress2;
                          break;
                          
                          case 3:
                          rectangleTexCoord = rectangleTexCoordPress3;
                          break;
                          
                          case 4:
                          rectangleTexCoord = rectangleTexCoordPress4;
                          break;

                          case 0:
                          default:
                          rectangleTexCoord=temp;
                          break;
                      }

                      glBindBuffer(GL_ARRAY_BUFFER,vbo_smileyTexcoord);

                          glBufferData(GL_ARRAY_BUFFER,
                                          4*2*sizeof(GLfloat),
                                          rectangleTexCoord,
                                          GL_DYNAMIC_DRAW);

                          glVertexAttribPointer(VSV_ATTRIBUTE_TEXCOORD,
                                                  2,
                                                  GL_FLOAT,
                                                  GL_FALSE,
                                                  0,
                                                  NULL);

                          glEnableVertexAttribArray(VSV_ATTRIBUTE_TEXCOORD);

                      glBindBuffer(GL_ARRAY_BUFFER,0);

                      glDrawArrays(GL_TRIANGLE_FAN , 0 , 4);
                  glBindVertexArray(0);
              
              glUseProgram(0);
    //************shader Draw end******************************
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];//swap buffer
    
    
}
//display link - is timer object which matches refresh rate of screen with drawable layer to avoid screen/image terring
-(void)startAnimation
{
    //code
    if(isAnimating == NO)
    {
        //Set dynamically displayLink based on underlying GL technology and set/register callback function for drawView()
        displayLink = [NSClassFromString(@"CADisplayLink") displayLinkWithTarget:self selector:@selector(drawView:)];
        
        //set FPS
        [displayLink setPreferredFramesPerSecond:animationFrameInterval];
        
        //run loop
        //add displaylink to runloop and set display mode
        [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        
        isAnimating = YES;
    }
}

-(void)stopAnimation
{
    //code
    if(isAnimating == YES)
    {
        [displayLink invalidate];
        displayLink = nil;
        isAnimating = NO;
    }
    
}

-(void)onSingleTap:(UITapGestureRecognizer *)gr
{
    //code
    PressedDigit +=1;
    if(PressedDigit > 4)
    {
        PressedDigit=0;
    }
    
    if(PressedDigit == 0)
    {
        glDisable(GL_TEXTURE_2D);
    }else{
        glEnable(GL_TEXTURE_2D);
    }
    
}

-(void)onDoubleTap:(UITapGestureRecognizer *)gr
{
    //code

}

-(void)onSwipe:(UISwipeGestureRecognizer *)gr
{
    //code
    //exit code
    [self uninitialize];
    [self release];
    exit(0);

}


-(void)onLongPress:(UILongPressGestureRecognizer *)gr
{
    //code


}

-(void)uninitialize
{
    if(vao_staticSmiley)
     {
         glDeleteVertexArrays(1,
                             &vao_staticSmiley);
     }
     vao_staticSmiley=0;

     if(vbo_smileyPosition)
     {
         glDeleteBuffers(1,
                     &vbo_smileyPosition);
     }
     vbo_smileyPosition=0;

     if(vbo_smileyTexcoord)
     {
         glDeleteBuffers(1,
                     &vbo_smileyTexcoord);
     }
     vbo_smileyTexcoord=0;

     if(vbo_smileysWhitelColor)
     {
         glDeleteBuffers(1,
                     &vbo_smileysWhitelColor);
     }
     vbo_smileysWhitelColor=0;

     if(gShaderProgramObject)
     {
         glUseProgram(gShaderProgramObject);

         GLsizei shaderCount;
         GLsizei tempShaderCount;
         GLuint *pShader=NULL;

         glGetProgramiv(gShaderProgramObject,GL_ATTACHED_SHADERS,&shaderCount);

         pShader=(GLuint *)malloc(sizeof(GLuint)*shaderCount);
         if(pShader==NULL)
         {
             printf("\nFailed to allocate memory for pShader");
         }else{
             glGetAttachedShaders(gShaderProgramObject,shaderCount,&tempShaderCount,pShader);
             
             printf("\nShader program has actual attached shader count is:%d",tempShaderCount);
             
             for(GLsizei i=0;i<shaderCount;i++)
             {
                 glDetachShader(gShaderProgramObject,pShader[i]);
                 glDeleteShader(pShader[i]);
                 pShader[i]=0;
             }
             free(pShader);
             
             glDeleteProgram(gShaderProgramObject);
             gShaderProgramObject=0;
             glUseProgram(0);
         }
     }

     glDeleteTextures(1,&smileyTextureID);
    
    //code
    if(depthRenderbuffer)
    {
        glDeleteRenderbuffers(1, &depthRenderbuffer);
    }
    depthRenderbuffer=0;
    
    if(colorRenderbuffer)
    {
        glDeleteRenderbuffers(1, &colorRenderbuffer);
    }
    colorRenderbuffer=0;
    
    if(defaultFramebuffer)
    {
        glDeleteFramebuffers(1, &defaultFramebuffer);
    }
    defaultFramebuffer=0;
    
    if(eaglContext)
    {
        if([EAGLContext currentContext] == eaglContext)
        {
            [EAGLContext setCurrentContext:nil];
        }
        [eaglContext release];
    }
    eaglContext =nil;

}

///like destructor
-(void)dealloc
{
    ///code
    [self uninitialize];
    [super dealloc];
}


@end
