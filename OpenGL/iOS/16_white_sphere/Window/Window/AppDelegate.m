//
//  AppDelegate.m
//  Window
//
//  Created by user205082 on 11/6/21.
//
#import "AppDelegate.h"
#import "ViewController.h"
#import "GLESView.h"

@implementation AppDelegate
{
    @private
    UIWindow *window;
    ViewController *viewController;
    GLESView *view;
    
}
//didFinishLaunchingWithOptions -to allow to lauch via third party
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    //get IOS device main screen and then get screen bounds rect as my window rect
    CGRect win_rect = [[UIScreen mainScreen] bounds];
    
    //MVC
    
    //model is created
    //creted window based of win_rect
    window = [[UIWindow alloc] initWithFrame:win_rect];
    
    //controller is created
    viewController=[[ViewController alloc] init];
    
    //controller is set for model
    [window setRootViewController:viewController];
    
    //view created
    view =[[GLESView alloc]initWithFrame:win_rect];
    
    //view is set for controller
    [viewController setView:view];
    [view release];
    
    ///its like setFocus() and setForgroundWindow() or MB_TOPMOST
    [window makeKeyAndVisible];
    
    [view startAnimation];
    
    // Override point for customization after application launch.
    return (YES);
}

//Interface method for when app is inactive
-(void)applicationWillResignActive:(UIApplication *)application
{
    //code
    [view stopAnimation];
}

//Interface method for when app is in background
-(void)applicationDidEnterBackground:(UIApplication *)application
{
    //code
    
}

//Interface method for when app is in foreground
-(void)applicationWillEnterForeground:(UIApplication *)application
{
    //code
}

//Interface method for when app is active
-(void)applicationDidBecomeActive:(UIApplication *)application
{
    //code
    [view startAnimation];
}

//Interface method for terminating app
-(void)applicationWillTerminate:(UIApplication *)application
{
    //code
    [view stopAnimation];
}

-(void)dealloc
{
    //code
    [view release];
    [viewController release];
    [window release];
    [super dealloc];
}


@end
