//
//  MyView.m
//  Window
//
//  Created by user205082 on 11/6/21.
//
#import<OpenGLES/ES3/gl.h>
#import<OpenGLES/ES3/glext.h>

#import "GLESView.h"
#import"vmath.h"
using namespace vmath;

enum{
    VSV_ATTRIBUTE_POSITION=0,
    VSV_ATTRIBUTE_COLOR,
    VSV_ATTRIBUTE_TEXCOORD,
    VSV_ATTRIBUTE_NORMAL,
};

#define CHECK_IMAGE_WIDTH 64
#define CHECK_IMAGE_HEIGHT 64

@implementation GLESView
{
    @private
    //Embedded Apple GL
    EAGLContext *eaglContext;
    
    GLuint defaultFramebuffer;
    GLuint colorRenderbuffer;
    GLuint depthRenderbuffer;
    
    //core animation
    //CADisplayLink --- underlying GL technology might change in future,so we are not directly going to use CADisplayLink pointer
    //thats why `id` is used, it will provide underlying GL driver dynamically
    id displayLink;
    NSInteger animationFrameInterval;
    bool isAnimating;
    
    GLuint gVertexShaderObject;
      GLuint gFragmentShaderObject;
      GLuint gShaderProgramObject;

      GLuint vao_checkerboard;
      GLuint vbo_checkerboardPosition;
      GLuint vbo_checkerboardTexcoord;

      GLuint mvpMatrixUniform;
      GLuint textureSamplerUniform;

      mat4 perspectiveProjectionMatrix;

      GLubyte checkImage[CHECK_IMAGE_HEIGHT][CHECK_IMAGE_WIDTH][4];
      GLuint CheckerboardTextureID;
    
}

-(id)initWithFrame:(CGRect)frame
{
    // code
    self = [super initWithFrame:frame];
    if (self)
    {
        //getting render mode display layer
        CAEAGLLayer *eaglLayer =(CAEAGLLayer *)[super layer];
        
        //set Opaque ie make display layer transparent
        [eaglLayer setOpaque:YES];
        
        //set Drawable Properties,define color format:RGBA8 and allow to store window in backing:YES
        [eaglLayer setDrawableProperties:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:NO],kEAGLDrawablePropertyRetainedBacking,
                                          kEAGLColorFormatRGBA8,kEAGLDrawablePropertyColorFormat,nil]];
        
        //create context of OpenFLES 3.0
        eaglContext = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES3];
        if(eaglContext == nil)
        {
            printf("\nOpenGLES Context creation failed");
            return (nil);
        }
    
        //set current context
        [EAGLContext setCurrentContext:eaglContext];
        
        //Create our framebuffer
        glGenFramebuffers(1, &defaultFramebuffer);
        glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
        //Create color Render buffer
            glGenRenderbuffers(1, &colorRenderbuffer);
            glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
        
            //Provide storage to color buffer using Apple/driver/native API
            [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
        
            //set attachment of render buffer ie for color buffer attachment is set to 0 th location of color attachment
            glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderbuffer);
            
            //get backing width and height of render buffer
            //getting width and height from display layer(ie eaglLayer)
            //because renderbuffer storage was provided by display layer
            GLint backingWidth;
            GLint backingHeight;
            glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
            glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
        
        //Create depth Render buffer
            glGenRenderbuffers(1, &depthRenderbuffer);
            glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
            
            //Provide storage to depth buffer using opengl API with width and height
            //IOS default 16 bit support for depth render buffer
            glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
            
            //set attachment of render buffer ie for depth buffer attachment of depth attachment
            glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
            
            if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
            {
                printf("\nGL_FRAMEBUFFER is not COMPLETE");
                [self uninitialize];
                return (nil);
            }
    
        animationFrameInterval = 60;//60 FPS //actually its default to 60 from IOS 8.2
        isAnimating = NO;
        
        printf("\nOpenGL Vender:%s",glGetString(GL_VENDOR));
        printf("\nOpenGL Render:%s",glGetString(GL_RENDERER));
        printf("\nOpenGL Version:%s",glGetString(GL_VERSION));
        printf("\nOpenGL GLSL Version:%s",glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        //***********shader initialize start***************************************
        
        
        GLint infoLogLength;
        GLint shaderCompileStatus;
        GLint shaderProgramLinkStatus;
        GLchar *szBuffer;
        GLsizei written;

        //Shader Code Start--------------------
            gVertexShaderObject=glCreateShader(GL_VERTEX_SHADER);

            const GLchar *vertexShaderSrc=
            "#version 300 es"\
            "\n"\
            "in vec4 vPosition;"\
            "in vec2 vTexCoord;"\
            "out vec2 out_TexCoord;"\

            "uniform mat4 u_mvpMatrix;"\

            "void main(void)"\
            "{"\
                "out_TexCoord=vTexCoord;"\
                "gl_Position=u_mvpMatrix * vPosition;"\
            "}";

            glShaderSource(gVertexShaderObject,
                            1,
                            (const GLchar **)&vertexShaderSrc,
                            NULL);

            glCompileShader(gVertexShaderObject);

            glGetShaderiv(gVertexShaderObject,
                            GL_COMPILE_STATUS,
                            &shaderCompileStatus);

            if(shaderCompileStatus == GL_FALSE)
            {
                glGetShaderiv(gVertexShaderObject,
                            GL_INFO_LOG_LENGTH,
                            &infoLogLength);

                if(infoLogLength > 0)
                {
                    szBuffer=(GLchar *)malloc(sizeof(GLchar) * infoLogLength);

                    if(szBuffer!=NULL)
                    {
                        glGetShaderInfoLog(gVertexShaderObject,
                                            infoLogLength,
                                            &written,
                                            szBuffer);
                        printf(
                                "\nERROR | Vertex Shader Compilation Failed Log : %s",
                                szBuffer);
                        free(szBuffer);
                        [self uninitialize];
                        return nil;
                        
                    }
                }
            }

            infoLogLength=0;
            shaderCompileStatus=0;
            shaderProgramLinkStatus=0;
            szBuffer=NULL;
            written=0;

            gFragmentShaderObject=glCreateShader(GL_FRAGMENT_SHADER);

            const GLchar *fragmentShaderSrc=
            "#version 300 es"\
            "\n"\
        "precision highp float;" \
        "precision mediump int;" \
            "in vec2 out_TexCoord;"\
            "out vec4 fragColor;"\

            "uniform sampler2D u_texture_sampler;"\

            "void main(void)"\
            "{"\
                "fragColor=texture(u_texture_sampler , out_TexCoord);"\
            "}";

            glShaderSource(gFragmentShaderObject,
                            1,
                            (const GLchar**)&fragmentShaderSrc,
                            NULL);
            
            glCompileShader(gFragmentShaderObject);

            glGetShaderiv(gFragmentShaderObject,
                            GL_COMPILE_STATUS,
                            &shaderCompileStatus);

            if(shaderCompileStatus==GL_FALSE)
            {
                glGetShaderiv(gFragmentShaderObject,
                            GL_INFO_LOG_LENGTH,
                            &infoLogLength);

                if(infoLogLength > 0 )
                {
                    szBuffer=(GLchar*)malloc(sizeof(GLchar) * infoLogLength);

                    glGetShaderInfoLog(gFragmentShaderObject,
                                            infoLogLength,
                                            &written,
                                            szBuffer);

                    printf(
                            "\nERROR | Fragment Shader Compilation failed log %s",
                            szBuffer);

                    free(szBuffer);
                    [self uninitialize];
                    return nil;
                    
                }
            }

            infoLogLength=0;
            shaderCompileStatus=0;
            shaderProgramLinkStatus=0;
            szBuffer=NULL;
            written=0;

            gShaderProgramObject=glCreateProgram();

            glAttachShader(gShaderProgramObject , gVertexShaderObject);
            glAttachShader(gShaderProgramObject , gFragmentShaderObject);

            //Shader Attribute Binding Before Pre Linking of attached Shader
                glBindAttribLocation(gShaderProgramObject , VSV_ATTRIBUTE_POSITION ,  "vPosition");
                glBindAttribLocation(gShaderProgramObject , VSV_ATTRIBUTE_TEXCOORD , "vTexCoord");

            glLinkProgram(gShaderProgramObject);

            glGetProgramiv(gShaderProgramObject,
                            GL_LINK_STATUS,
                            &shaderProgramLinkStatus);

            if(shaderProgramLinkStatus==GL_FALSE)
            {
                glGetProgramiv(gShaderProgramObject,
                                GL_INFO_LOG_LENGTH,
                                &infoLogLength);
                
                if(infoLogLength > 0)
                {
                    szBuffer=(GLchar *)malloc(sizeof(GLchar) * infoLogLength);
                    if(szBuffer!=NULL)
                    {
                        glGetProgramInfoLog(gShaderProgramObject,
                                            infoLogLength,
                                            &written,
                                            szBuffer);
                        printf(
                                "\nERROR | Shader Program Link Failed Log %s",
                                szBuffer);
                        free(szBuffer);
                        [self uninitialize];
                        return nil;
                        
                    }
                }
            }
            
            //Uniform variable Binding after post linking of attached shader
                mvpMatrixUniform=glGetUniformLocation(gShaderProgramObject,
                                                        "u_mvpMatrix");
                textureSamplerUniform=glGetUniformLocation(gShaderProgramObject,
                                                            "u_texture_sampler");

        //Shader Code End--------------------

        //VAO and its respective VBO preparation
        const GLfloat CheckerboardTexCoord[]=
        {
            0.0f,0.0f,
            0.0f,1.0f,
            1.0f,1.0f,
            1.0f,0.0f
        };

        glGenVertexArrays(1,&vao_checkerboard);
        glBindVertexArray(vao_checkerboard);

            glGenBuffers(1,&vbo_checkerboardPosition);
            glBindBuffer(GL_ARRAY_BUFFER,vbo_checkerboardPosition);

                glBufferData(GL_ARRAY_BUFFER,
                            3 * 4 * sizeof(GLfloat),
                            NULL,
                            GL_DYNAMIC_DRAW);

                glVertexAttribPointer(VSV_ATTRIBUTE_POSITION,
                                    3,
                                    GL_FLOAT,
                                    GL_FALSE,
                                    0,
                                    NULL);

                glEnableVertexAttribArray(VSV_ATTRIBUTE_POSITION);
            glBindBuffer(GL_ARRAY_BUFFER,0);

            glGenBuffers(1,&vbo_checkerboardTexcoord);
            glBindBuffer(GL_ARRAY_BUFFER,vbo_checkerboardTexcoord);

                glBufferData(GL_ARRAY_BUFFER,
                            sizeof(CheckerboardTexCoord),
                            CheckerboardTexCoord,
                            GL_STATIC_DRAW);
                
                glVertexAttribPointer(VSV_ATTRIBUTE_TEXCOORD,
                                    2,
                                    GL_FLOAT,
                                    GL_FALSE,
                                    0,
                                    NULL);

                glEnableVertexAttribArray(VSV_ATTRIBUTE_TEXCOORD);
            glBindBuffer(GL_ARRAY_BUFFER,0);

        glBindVertexArray(0);


        //SetColor
        glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        
        glClearDepthf(1.0f);
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);

        glEnable(GL_TEXTURE_2D);
        [self LoadGLTexture];

        perspectiveProjectionMatrix = mat4 :: identity();
        //***********shader initialize end***************************************

        //register events/Gestures
        //1-register function
        //2-set Number Of Taps Required
        //3-set Number Of Touches Required
        //4-set Delegate
        //5-register/add Gesture Recognizer/event
        
        //Single tap event
        UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onSingleTap:)];
        [singleTapGestureRecognizer setNumberOfTapsRequired:1];
        [singleTapGestureRecognizer setNumberOfTouchesRequired:1];
        [singleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:singleTapGestureRecognizer];
        
        //double tap event
        UITapGestureRecognizer *doubleTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onDoubleTap:)];
        [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
        [doubleTapGestureRecognizer setNumberOfTouchesRequired:1];
        [doubleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:doubleTapGestureRecognizer];
        
        //this will block double tap to recognize as two single tap
        [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
        
        //Swipe event
        UISwipeGestureRecognizer *swipeGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(onSwipe:)];
        [swipeGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:swipeGestureRecognizer];
        
        //Long press
        UILongPressGestureRecognizer *longPressGestureRecognizer=[[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
        [longPressGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:longPressGestureRecognizer];
        
    }

    return  self;
}

//this method is complursory to override
+(Class)layerClass
{
    //code
    return ([CAEAGLLayer class]);
}

/*
 Only override drawRect: if you perform custom drawing.
 An empty implementation adversely affects performance during animation.
//drawRect is only called for immediate mode
 - (void)drawRect:(CGRect)rect {
     // code
}
 */

//will override
//its resize()
-(void)layoutSubviews
{
    //code
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);

    //bind color Render buffer
        glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    
        //Provide storage to color buffer using Apple/driver/native API from resized drawable layer
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer *)[self layer]];
    
        //set attachment of render buffer ie for color buffer attachment is set to 0 th location of color attachment
        
        //get backing width and height of render buffer
        //getting width and height from display layer(ie eaglLayer)
        //because renderbuffer storage was provided by display layer
        GLint width;
        GLint height;
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
    
    //bind depth Render buffer
        glGenRenderbuffers(1, &depthRenderbuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
        
        //Provide storage to depth buffer using opengl API with resized drawable layer width and height
        //IOS default 16 bit support for depth render buffer
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
        
        //set attachment of render buffer ie for depth buffer attachment of depth attachment
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
        
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        {
            printf("\nGL_FRAMEBUFFER is not COMPLETE");
           
        }
    
    if(height < 0)
    {
        height = 1;
    }
        
    glViewport(0, 0, (GLsizei)width, (GLsizei)height);
    
    //************resize code for openGL start******************
    
    perspectiveProjectionMatrix = vmath::perspective(45.0f,
                                                    (GLfloat)width/(GLfloat)height,
                                                    0.1f,
                                                    100.0f);
    //************resize code for openGL end********************
    
    [self drawView:nil];
    
}

-(void)drawView:(id)sender
{
    //code
    [EAGLContext setCurrentContext:eaglContext];
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
    
    //************shader Draw start******************************
    
    mat4 modelViewMatrix;
      mat4 modelViewProjectionMatrix;
      mat4 translateMatrix;
      GLfloat  *rectPosition;

       GLfloat CheckerboardRectangle1[]=
      {
          -2.0f, -1.0f, 0.0f,
          -2.0f, 1.0f, 0.0f,
          0.0f, 1.0f, 0.0f,
          0.0f, -1.0f, 0.0f
      };
      GLfloat CheckerboardRectangle2[]=
      {
          1.0f, -1.0f, 0.0f,
          1.0f, 1.0f, 0.0f,
          2.41421f, 1.0f, -1.41421f,
          2.41421f, -1.0f, -1.41421f
      };
      //code
      glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

      glUseProgram(gShaderProgramObject);

          translateMatrix = vmath :: translate(0.0f,0.0f,-3.6f);
          modelViewMatrix = translateMatrix;
          modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

          glUniformMatrix4fv(mvpMatrixUniform,
                              1,
                              GL_FALSE,
                              modelViewProjectionMatrix);

          
          glActiveTexture(GL_TEXTURE0);
          glBindTexture(GL_TEXTURE_2D, CheckerboardTextureID);
          glUniform1i(textureSamplerUniform,0);

          rectPosition=CheckerboardRectangle1;

          glBindVertexArray(vao_checkerboard);

              glBindBuffer(GL_ARRAY_BUFFER,vbo_checkerboardPosition);
                  glBufferData(GL_ARRAY_BUFFER,
                              3 * 4 *sizeof(GLfloat),
                              rectPosition,
                              GL_DYNAMIC_DRAW);
                  glVertexAttribPointer(VSV_ATTRIBUTE_POSITION,
                                          3,
                                          GL_FLOAT,
                                          GL_FALSE,
                                          0,
                                          NULL);
                  glEnableVertexAttribArray(VSV_ATTRIBUTE_POSITION);
              glBindBuffer(GL_ARRAY_BUFFER,0);

              glDrawArrays(GL_TRIANGLE_FAN, 0, 4);

          glBindVertexArray(0);

          rectPosition=CheckerboardRectangle2;

          glBindVertexArray(vao_checkerboard);
              
              glBindBuffer(GL_ARRAY_BUFFER,vbo_checkerboardPosition);
                  glBufferData(GL_ARRAY_BUFFER,
                              3 * 4 *sizeof(GLfloat),
                              rectPosition,
                              GL_DYNAMIC_DRAW);
                  glVertexAttribPointer(VSV_ATTRIBUTE_POSITION,
                                          3,
                                          GL_FLOAT,
                                          GL_FALSE,
                                          0,
                                          NULL);
                  glEnableVertexAttribArray(VSV_ATTRIBUTE_POSITION);
              glBindBuffer(GL_ARRAY_BUFFER,0);

              glDrawArrays(GL_TRIANGLE_FAN, 0, 4);

          glBindVertexArray(0);

      glUseProgram(0);
    //************shader Draw end******************************
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];//swap buffer
    
    
}


-(void)LoadGLTexture
{
    [self MakeCheckImage];
    
    glPixelStorei(GL_UNPACK_ALIGNMENT,
                1);
    
    glGenTextures(1 ,
                &CheckerboardTextureID);

    glBindTexture( GL_TEXTURE_2D,
                    CheckerboardTextureID);
    
    glTexParameteri( GL_TEXTURE_2D,
                    GL_TEXTURE_MAG_FILTER,
                    GL_NEAREST
                    );
    glTexParameteri( GL_TEXTURE_2D,
                    GL_TEXTURE_MIN_FILTER,
                    GL_NEAREST
                   );
    glTexParameteri( GL_TEXTURE_2D,
                    GL_TEXTURE_WRAP_S,
                    GL_REPEAT
                    );
    glTexParameteri( GL_TEXTURE_2D,
                    GL_TEXTURE_WRAP_T,
                    GL_REPEAT
                   );


    glTexImage2D(GL_TEXTURE_2D,
                0,
                GL_RGBA,
                CHECK_IMAGE_WIDTH,
                CHECK_IMAGE_HEIGHT,
                0,
                GL_RGBA,
                GL_UNSIGNED_BYTE,
                checkImage);

    glGenerateMipmap(GL_TEXTURE_2D);
}

-(void)MakeCheckImage
{
    int vsv_i,vsv_j,vsv_c;

    for(vsv_i=0;vsv_i<CHECK_IMAGE_HEIGHT;vsv_i++){

        for(vsv_j=0;vsv_j<CHECK_IMAGE_WIDTH;vsv_j++){

            vsv_c=( (vsv_i & 0x8 )==0)^( (vsv_j & 0x8 )==0);
            vsv_c*=255;
            checkImage[vsv_i][vsv_j][0]=(GLubyte)vsv_c;
            checkImage[vsv_i][vsv_j][1]=(GLubyte)vsv_c;
            checkImage[vsv_i][vsv_j][2]=(GLubyte)vsv_c;
            checkImage[vsv_i][vsv_j][3]=255;
        }
    }
}

//display link - is timer object which matches refresh rate of screen with drawable layer to avoid screen/image terring
-(void)startAnimation
{
    //code
    if(isAnimating == NO)
    {
        //Set dynamically displayLink based on underlying GL technology and set/register callback function for drawView()
        displayLink = [NSClassFromString(@"CADisplayLink") displayLinkWithTarget:self selector:@selector(drawView:)];
        
        //set FPS
        [displayLink setPreferredFramesPerSecond:animationFrameInterval];
        
        //run loop
        //add displaylink to runloop and set display mode
        [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        
        isAnimating = YES;
    }
}

-(void)stopAnimation
{
    //code
    if(isAnimating == YES)
    {
        [displayLink invalidate];
        displayLink = nil;
        isAnimating = NO;
    }
    
}

-(void)onSingleTap:(UITapGestureRecognizer *)gr
{
    //code
}

-(void)onDoubleTap:(UITapGestureRecognizer *)gr
{
    //code

}

-(void)onSwipe:(UISwipeGestureRecognizer *)gr
{
    //code
    //exit code
    [self uninitialize];
    [self release];
    exit(0);

}


-(void)onLongPress:(UILongPressGestureRecognizer *)gr
{
    //code


}

-(void)uninitialize
{
    if(vao_checkerboard)
    {
        glDeleteVertexArrays(1,
                            &vao_checkerboard);
    }
    vao_checkerboard=0;

    if(vbo_checkerboardPosition)
    {
        glDeleteBuffers(1,
                    &vbo_checkerboardPosition);
    }
    vbo_checkerboardPosition=0;

    if(vbo_checkerboardTexcoord)
    {
        glDeleteBuffers(1,
                    &vbo_checkerboardTexcoord);
    }
    vbo_checkerboardTexcoord=0;

    if(gShaderProgramObject)
    {
        glUseProgram(gShaderProgramObject);

        GLsizei shaderCount;
        GLsizei tempShaderCount;
        GLuint *pShader=NULL;

        glGetProgramiv(gShaderProgramObject,GL_ATTACHED_SHADERS,&shaderCount);

        pShader=(GLuint *)malloc(sizeof(GLuint)*shaderCount);
        if(pShader==NULL)
        {
            printf("\nFailed to allocate memory for pShader");
        }else{
            glGetAttachedShaders(gShaderProgramObject,shaderCount,&tempShaderCount,pShader);
            
            printf("\nShader program has actual attached shader count is:%d",tempShaderCount);
            
            for(GLsizei i=0;i<shaderCount;i++)
            {
                glDetachShader(gShaderProgramObject,pShader[i]);
                glDeleteShader(pShader[i]);
                pShader[i]=0;
            }
            free(pShader);
            
            glDeleteProgram(gShaderProgramObject);
            gShaderProgramObject=0;
            glUseProgram(0);
        }
    }

    glDeleteTextures(1,&CheckerboardTextureID);
    
    //code
    if(depthRenderbuffer)
    {
        glDeleteRenderbuffers(1, &depthRenderbuffer);
    }
    depthRenderbuffer=0;
    
    if(colorRenderbuffer)
    {
        glDeleteRenderbuffers(1, &colorRenderbuffer);
    }
    colorRenderbuffer=0;
    
    if(defaultFramebuffer)
    {
        glDeleteFramebuffers(1, &defaultFramebuffer);
    }
    defaultFramebuffer=0;
    
    if(eaglContext)
    {
        if([EAGLContext currentContext] == eaglContext)
        {
            [EAGLContext setCurrentContext:nil];
        }
        [eaglContext release];
    }
    eaglContext =nil;

}

///like destructor
-(void)dealloc
{
    ///code
    [self uninitialize];
    [super dealloc];
}


@end
