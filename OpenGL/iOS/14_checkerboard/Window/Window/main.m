//
//  main.m
//  Window
//
//  Created by user205082 on 11/6/21.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    //code
    NSString *appDelegateClassName;
    
    // NSAutoreleasePool will hold all alocated memory
    NSAutoreleasePool *pool=[[NSAutoreleasePool alloc] init];

    // Setup code that might create autoreleased objects goes here.
    appDelegateClassName = NSStringFromClass([AppDelegate class]);
    int ret = UIApplicationMain(argc, argv, nil, appDelegateClassName);
    
    // releaseing every obj memory from pool
    [pool release];
    return (ret);
}
