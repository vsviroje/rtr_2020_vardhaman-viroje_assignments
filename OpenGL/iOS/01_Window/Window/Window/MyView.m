//
//  MyView.m
//  Window
//
//  Created by user205082 on 11/6/21.
//

#import "MyView.h"

@implementation MyView
{
    @private
    NSString *centeralText;
}

-(id)initWithFrame:(CGRect)frame
{
    // code
    self = [super initWithFrame:frame];
    if (self)
    {
        centeralText = @"Hello World";
        
        //register events
        //1-register function
        //2-set Number Of Taps Required
        //3-set Number Of Touches Required
        //4-set Delegate
        //5-register/add Gesture Recognizer/event
        
        //Single tap event
        UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onSingleTap:)];
        [singleTapGestureRecognizer setNumberOfTapsRequired:1];
        [singleTapGestureRecognizer setNumberOfTouchesRequired:1];
        [singleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:singleTapGestureRecognizer];
        
        //double tap event
        UITapGestureRecognizer *doubleTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onDoubleTap:)];
        [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
        [doubleTapGestureRecognizer setNumberOfTouchesRequired:1];
        [doubleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:doubleTapGestureRecognizer];
        
        //this will block double tap to recognize as two single tap
        [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
        
        //Swipe event
        UISwipeGestureRecognizer *swipeGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(onSwipe:)];
        [swipeGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:swipeGestureRecognizer];
        
        //Long press
        UILongPressGestureRecognizer *longPressGestureRecognizer=[[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
        [longPressGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:longPressGestureRecognizer];
        
    }

    return  self;
}

/*
 Only override drawRect: if you perform custom drawing.
 An empty implementation adversely affects performance during animation.
 */
 - (void)drawRect:(CGRect)rect {
     // code
    UIColor *backgroundColor = [UIColor blackColor];
    [backgroundColor set];

    UIRectFill(rect);
     
     NSDictionary *dictionaryForTextAttribute = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"Helvetica" size:32],NSFontAttributeName,
                                                                                           [UIColor greenColor],NSForegroundColorAttributeName,
                                                                                           nil];
     
     
     CGSize textSize = [centeralText sizeWithAttributes:dictionaryForTextAttribute];
     
     CGPoint point;
     point.x = (rect.size.width/2)-(textSize.width/2);
     point.y = (rect.size.height/2)-(textSize.height/2);
     
     [centeralText drawAtPoint:point withAttributes:dictionaryForTextAttribute];

}

-(void)onSingleTap:(UITapGestureRecognizer *)gr
{
    //code
    centeralText = @"Singl Tap";
    [self setNeedsDisplay];
}

-(void)onDoubleTap:(UITapGestureRecognizer *)gr
{
    //code
    centeralText = @"Double Tap";
    [self setNeedsDisplay];

}

-(void)onSwipe:(UISwipeGestureRecognizer *)gr
{
    //code
    //centeralText = @"Swipe";
    //[self setNeedsDisplay];
    
    [self release];
    exit(0);

}


-(void)onLongPress:(UILongPressGestureRecognizer *)gr
{
    //code
    centeralText = @"Long Press";
    [self setNeedsDisplay];

}

///like destructor
-(void)dealloc
{
    ///code
    [super dealloc];
}


@end
