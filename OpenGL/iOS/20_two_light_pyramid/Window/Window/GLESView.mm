//
//  MyView.m
//  Window
//
//  Created by user205082 on 11/6/21.
//
#import<OpenGLES/ES3/gl.h>
#import<OpenGLES/ES3/glext.h>

#import "GLESView.h"
#include"vmath.h"
using namespace vmath;

enum{
    VSV_ATTRIBUTE_POSITION=0,
    VSV_ATTRIBUTE_COLOR,
    VSV_ATTRIBUTE_TEXCOORD,
    VSV_ATTRIBUTE_NORMAL,
};

typedef struct Light {
    vec4 LightAmbient;
    vec4 LightDefuse;
    vec4 LightSpecular;
    vec4 LightPosition;
}LightStruct;

LightStruct light[2] = {
                        {
                            vec4(0.1f,0.1f,0.1f,1.0f),
                            vec4(1.0f,0.0f,0.0f,1.0f),
                            vec4(1.0f,0.0f,0.0f,1.0f),
                            vec4(2.0f,0.0f,0.0f,1.0f)
                        },
                        {
                            vec4(0.1f,0.1f,0.1f,1.0f),
                            vec4(0.0f,0.0f,1.0f,1.0f),
                            vec4(0.0f,0.0f,1.0f,1.0f),
                            vec4(-2.0f,0.0f,0.0f,1.0f)
                        }
                    };


@implementation GLESView
{
    @private
    //Embedded Apple GL
    EAGLContext *eaglContext;
    
    GLuint defaultFramebuffer;
    GLuint colorRenderbuffer;
    GLuint depthRenderbuffer;
    
    //core animation
    //CADisplayLink --- underlying GL technology might change in future,so we are not directly going to use CADisplayLink pointer
    //thats why `id` is used, it will provide underlying GL driver dynamically
    id displayLink;
    NSInteger animationFrameInterval;
    bool isAnimating;
    
    GLuint gVertexShaderObject;
    GLuint gFragmentShaderObject;
    GLuint gShaderProgramObject;

    GLuint vao_pyramid;
    GLuint vbo_pyramidPosition;
    GLuint vbo_pyramidNormal;

    GLuint modelMatrixUniform;
    GLuint viewMatrixUniform;
    GLuint projectionlMatrixUniform;

    GLuint lightAmbientUniform[2];
    GLuint lightDiffuseUniform[2];
    GLuint lightSpecularUniform[2];
    GLuint lightPositionUniform[2];

    GLuint materialAmbientUniform;
    GLuint materialDiffuseUniform;
    GLuint materialSpecularUniform;
    GLuint materialShininessUniform;

    GLuint LKeyPressedUniform;

    bool bLights;

    mat4 perspectiveProjectionMatrix;

    GLfloat pyramidRotate;
    
}

-(id)initWithFrame:(CGRect)frame
{
    // code
    self = [super initWithFrame:frame];
    if (self)
    {
        //getting render mode display layer
        CAEAGLLayer *eaglLayer =(CAEAGLLayer *)[super layer];
        
        //set Opaque ie make display layer transparent
        [eaglLayer setOpaque:YES];
        
        //set Drawable Properties,define color format:RGBA8 and allow to store window in backing:YES
        [eaglLayer setDrawableProperties:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:NO],kEAGLDrawablePropertyRetainedBacking,
                                          kEAGLColorFormatRGBA8,kEAGLDrawablePropertyColorFormat,nil]];
        
        //create context of OpenFLES 3.0
        eaglContext = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES3];
        if(eaglContext == nil)
        {
            printf("\nOpenGLES Context creation failed");
            return (nil);
        }
    
        //set current context
        [EAGLContext setCurrentContext:eaglContext];
        
        //Create our framebuffer
        glGenFramebuffers(1, &defaultFramebuffer);
        glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
        //Create color Render buffer
            glGenRenderbuffers(1, &colorRenderbuffer);
            glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
        
            //Provide storage to color buffer using Apple/driver/native API
            [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
        
            //set attachment of render buffer ie for color buffer attachment is set to 0 th location of color attachment
            glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderbuffer);
            
            //get backing width and height of render buffer
            //getting width and height from display layer(ie eaglLayer)
            //because renderbuffer storage was provided by display layer
            GLint backingWidth;
            GLint backingHeight;
            glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
            glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
        
        //Create depth Render buffer
            glGenRenderbuffers(1, &depthRenderbuffer);
            glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
            
            //Provide storage to depth buffer using opengl API with width and height
            //IOS default 16 bit support for depth render buffer
            glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
            
            //set attachment of render buffer ie for depth buffer attachment of depth attachment
            glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
            
            if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
            {
                printf("\nGL_FRAMEBUFFER is not COMPLETE");
                [self uninitialize];
                return (nil);
            }
    
        animationFrameInterval = 60;//60 FPS //actually its default to 60 from IOS 8.2
        isAnimating = NO;
        
        printf("\nOpenGL Vender:%s",glGetString(GL_VENDOR));
        printf("\nOpenGL Render:%s",glGetString(GL_RENDERER));
        printf("\nOpenGL Version:%s",glGetString(GL_VERSION));
        printf("\nOpenGL GLSL Version:%s",glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        //***********shader initialize start***************************************
        
        
        GLint infoLogLength;
         GLint shaderCompileStatus;
         GLint shaderProgramLinkStatus;
         GLchar *szBuffer;
         GLsizei written;
         

         //Shader Code Start--------------------
         gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

         const GLchar* vertexShaderSourceCode =
             "#version 300 es\n"\
             "\n"\
        "precision highp float;" \
        "precision mediump int;" \
             "in vec4 vPosition;\n"\
             "in vec3 vNormal;\n"\

             "uniform mat4 u_modelMatrix;\n"\
             "uniform mat4 u_viewMatrix;\n"\
             "uniform mat4 u_projectionMatrix;\n"\

             "uniform vec3 u_lightAmbient[2];\n"\
             "uniform vec3 u_lightDiffuse[2];\n"\
             "uniform vec3 u_lightSpecular[2];\n"\
             "uniform vec4 u_lightPosition[2];\n"\

             "uniform vec3 u_materialAmbient;\n"\
             "uniform vec3 u_materialDiffuse;\n"\
             "uniform vec3 u_materialSpecular;\n"\
             "uniform float u_materialShininess;\n"\

             "uniform int u_lKeyPressed;\n"\

             "out vec3 out_phongADS_Light;\n"\

             "void main(void)\n"\
             "{\n"\
                 "vec3 temp_light;\n"\
                 
                 "vec3 ambient[2];\n"\
                 "vec3 diffuse[2];\n"\
                 "vec3 specular[2];\n"\


                 "if (u_lKeyPressed == 1)\n"\
                 "{\n"\

                     "vec4 eyeCoord = u_viewMatrix * u_modelMatrix * vPosition;\n"\
                     "vec3 viewVector = normalize( -eyeCoord.xyz);\n"\
                     "vec3 transformedNormal = normalize( mat3(u_viewMatrix * u_modelMatrix) * vNormal );\n"\
                 
                     "vec3 lightDirection[2];\n"\
                     "vec3 reflectionVector[2];\n"\

                     "for (int i = 0; i < 2; i++)\n"\
                     "{\n"\

                         "lightDirection[i] = normalize( vec3( u_lightPosition[i] - eyeCoord ) );\n"\

                         "reflectionVector[i] = vec3(reflect( -lightDirection[i], transformedNormal ));\n"\

                         "ambient[i] = u_lightAmbient[i] * u_materialAmbient;\n"\

                         "diffuse[i] = u_lightDiffuse[i] * u_materialDiffuse * max( dot(lightDirection[i] , transformedNormal), 0.0);\n"\

                         "specular[i] = u_lightSpecular[i] * u_materialSpecular * pow( max( dot(reflectionVector[i] , viewVector), 0.0f) , u_materialShininess);\n"\
                     "}\n"\

                 "}\n"\

                 "if (u_lKeyPressed == 1)\n"\
                 "{\n"\
                     "temp_light = (ambient[0]+ambient[1]) + (diffuse[0]+diffuse[1]) + (specular[0]+specular[1]);\n"\
                 "}\n"\
                 "else\n"\
                 "{\n"\

                     "temp_light = vec3(1.0f);\n"\

                 "}\n"\
                 
                 "out_phongADS_Light=temp_light;\n"\

                 "gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix * vPosition;\n"\

             "}";

         glShaderSource(gVertexShaderObject,
             1,
             (const GLchar**)&vertexShaderSourceCode,
             NULL);

         glCompileShader(gVertexShaderObject);

         glGetShaderiv(gVertexShaderObject,
             GL_COMPILE_STATUS,
             &shaderCompileStatus);

         if (shaderCompileStatus == GL_FALSE)
         {
             glGetShaderiv(gVertexShaderObject,
                 GL_INFO_LOG_LENGTH,
                 &infoLogLength);

             if (infoLogLength > 0)
             {
                 szBuffer = (GLchar*)malloc(sizeof(GLchar) * infoLogLength);
                 if (szBuffer != NULL)
                 {
                     glGetShaderInfoLog(gVertexShaderObject,
                         infoLogLength,
                         &written,
                         szBuffer);
                     printf( "\nERROR:Vertex Shader Object Compilation Failed log : %s", szBuffer);
                     free(szBuffer);
                     [self uninitialize];
                     return nil;
                 }
             }
         }

         infoLogLength = 0;
         shaderCompileStatus = 0;
         shaderProgramLinkStatus = 0;
         szBuffer = NULL;
         written = 0;

         gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

         const GLchar* fragmentShaderSourceCode =
             "#version 300 es"\
             "\n"\
        "precision highp float;" \
        "precision mediump int;" \
             "in vec3 out_phongADS_Light;\n"\
             "out vec4 fragColor;"\
             "void main(void)"\
             "{"\
                 "fragColor = vec4( out_phongADS_Light, 1.0f );"\
             "}";

         glShaderSource(gFragmentShaderObject,
             1,
             (const GLchar**)&fragmentShaderSourceCode,
             NULL);

         glCompileShader(gFragmentShaderObject);

         glGetShaderiv(gFragmentShaderObject,
             GL_COMPILE_STATUS,
             &shaderCompileStatus);

         if (shaderCompileStatus == GL_FALSE)
         {
             glGetShaderiv(gFragmentShaderObject,
                 GL_INFO_LOG_LENGTH,
                 &infoLogLength);

             if (infoLogLength > 0)
             {
                 szBuffer = (GLchar*)malloc(sizeof(GLchar) * infoLogLength);
                 if (szBuffer != NULL)
                 {
                     glGetShaderInfoLog(gFragmentShaderObject,
                         infoLogLength,
                         &written,
                         szBuffer);
                     printf( "\nERROR:Fragment Shader Object Compilation Failed log : %s", szBuffer);
                     free(szBuffer);
                     [self uninitialize];
                     return nil;
                 }
             }
         }

         infoLogLength = 0;
         shaderCompileStatus = 0;
         shaderProgramLinkStatus = 0;
         szBuffer = NULL;
         written = 0;

         gShaderProgramObject = glCreateProgram();

         glAttachShader(gShaderProgramObject, gVertexShaderObject);
         glAttachShader(gShaderProgramObject, gFragmentShaderObject);

         //Shader Attribute Binding Before Pre Linking of attached Shader
         glBindAttribLocation(gShaderProgramObject,
             VSV_ATTRIBUTE_POSITION,
             "vPosition");
         glBindAttribLocation(gShaderProgramObject,
             VSV_ATTRIBUTE_NORMAL,
             "vNormal");

         glLinkProgram(gShaderProgramObject);

         glGetProgramiv(gShaderProgramObject,
             GL_LINK_STATUS,
             &shaderProgramLinkStatus);

         if (shaderProgramLinkStatus == GL_FALSE)
         {
             glGetProgramiv(gShaderProgramObject,
                 GL_INFO_LOG_LENGTH,
                 &infoLogLength);
             if (infoLogLength > 0)
             {
                 szBuffer = (GLchar*)malloc(sizeof(GLchar) * infoLogLength);

                 if (szBuffer != NULL)
                 {
                     glGetProgramInfoLog(gShaderProgramObject,
                         infoLogLength,
                         &written,
                         szBuffer);

                     printf( "\nERROR:Shader program Link failed log : %s", szBuffer);
                     free(szBuffer);
                     [self uninitialize];
                     return nil;
                 }
             }
         }

         //Shader Code End--------------------


         //Uniform variable Binding after post linking of attached shader
         modelMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_modelMatrix");
         viewMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_viewMatrix");
         projectionlMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_projectionMatrix");

         
         lightAmbientUniform[0] = glGetUniformLocation(gShaderProgramObject, "u_lightAmbient[0]");
         lightDiffuseUniform[0] = glGetUniformLocation(gShaderProgramObject, "u_lightDiffuse[0]");
         lightSpecularUniform[0] = glGetUniformLocation(gShaderProgramObject, "u_lightSpecular[0]");
         lightPositionUniform[0] = glGetUniformLocation(gShaderProgramObject, "u_lightPosition[0]");
         
         lightAmbientUniform[1] = glGetUniformLocation(gShaderProgramObject, "u_lightAmbient[1]");
         lightDiffuseUniform[1] = glGetUniformLocation(gShaderProgramObject, "u_lightDiffuse[1]");
         lightSpecularUniform[1] = glGetUniformLocation(gShaderProgramObject, "u_lightSpecular[1]");
         lightPositionUniform[1] = glGetUniformLocation(gShaderProgramObject, "u_lightPosition[1]");

         materialSpecularUniform = glGetUniformLocation(gShaderProgramObject, "u_materialSpecular");
         materialAmbientUniform = glGetUniformLocation(gShaderProgramObject, "u_materialAmbient");
         materialDiffuseUniform = glGetUniformLocation(gShaderProgramObject, "u_materialDiffuse");
         materialShininessUniform = glGetUniformLocation(gShaderProgramObject, "u_materialShininess");

         LKeyPressedUniform = glGetUniformLocation(gShaderProgramObject, "u_lKeyPressed");

         //VAO and its respective VBO preparation
             const GLfloat pyramidVertices[]=
             {
                 0.0f, 1.0f, 0.0f,
                 -1.0f, -1.0f, 1.0f,
                 1.0f, -1.0f, 1.0f,

                 0.0f, 1.0f, 0.0f,
                 1.0f, -1.0f, 1.0f,
                 1.0f, -1.0f, -1.0f,

                 0.0f, 1.0f, 0.0f,
                 1.0f, -1.0f, -1.0f,
                 -1.0f, -1.0f, -1.0f,

                 0.0f, 1.0f, 0.0f,
                 -1.0f, -1.0f, -1.0f,
                 -1.0f, -1.0f, 1.0f
             };

             const GLfloat pyramidNormals[] =
             {
                 0.0f, 0.447214f, 0.894427f,
                 0.0f, 0.447214f, 0.894427f,
                 0.0f, 0.447214f, 0.894427f,

                 0.894427f, 0.447214f, 0.0f,
                 0.894427f, 0.447214f, 0.0f,
                 0.894427f, 0.447214f, 0.0f,

                 0.0f, 0.447214f, -0.894427f,
                 0.0f, 0.447214f, -0.894427f,
                 0.0f, 0.447214f, -0.894427f,

                 -0.894427f, 0.447214f, 0.0f,
                 -0.894427f, 0.447214f, 0.0f,
                 -0.894427f, 0.447214f, 0.0f
             };

             glGenVertexArrays(1,&vao_pyramid);
             glBindVertexArray(vao_pyramid);

                 glGenBuffers(1,&vbo_pyramidPosition);
                 glBindBuffer(GL_ARRAY_BUFFER,vbo_pyramidPosition);

                     glBufferData(GL_ARRAY_BUFFER,
                                 sizeof(pyramidVertices),
                                 pyramidVertices,
                                 GL_STATIC_DRAW);

                     glVertexAttribPointer(VSV_ATTRIBUTE_POSITION,
                                         3,
                                         GL_FLOAT,
                                         GL_FALSE,
                                         0,
                                         NULL);

                     glEnableVertexAttribArray(VSV_ATTRIBUTE_POSITION);
                 
                 glBindBuffer(GL_ARRAY_BUFFER,0);

                 glGenBuffers(1, &vbo_pyramidNormal);
                 glBindBuffer(GL_ARRAY_BUFFER, vbo_pyramidNormal);

                 glBufferData(GL_ARRAY_BUFFER,
                     sizeof(pyramidNormals),
                     pyramidNormals,
                     GL_STATIC_DRAW);

                 glVertexAttribPointer(VSV_ATTRIBUTE_NORMAL,
                     3,
                     GL_FLOAT,
                     GL_FALSE,
                     0,
                     NULL);

                 glEnableVertexAttribArray(VSV_ATTRIBUTE_NORMAL);

                 glBindBuffer(GL_ARRAY_BUFFER, 0);

             glBindVertexArray(0);

         glClearColor(0.0,0.0,0.0f,1.0f);

         glClearDepthf(1.0f);
         glEnable(GL_DEPTH_TEST);
         glDepthFunc(GL_LEQUAL);

         perspectiveProjectionMatrix=mat4::identity();
        //***********shader initialize end***************************************

        //register events/Gestures
        //1-register function
        //2-set Number Of Taps Required
        //3-set Number Of Touches Required
        //4-set Delegate
        //5-register/add Gesture Recognizer/event
        
        //Single tap event
        UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onSingleTap:)];
        [singleTapGestureRecognizer setNumberOfTapsRequired:1];
        [singleTapGestureRecognizer setNumberOfTouchesRequired:1];
        [singleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:singleTapGestureRecognizer];
        
        //double tap event
        UITapGestureRecognizer *doubleTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onDoubleTap:)];
        [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
        [doubleTapGestureRecognizer setNumberOfTouchesRequired:1];
        [doubleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:doubleTapGestureRecognizer];
        
        //this will block double tap to recognize as two single tap
        [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
        
        //Swipe event
        UISwipeGestureRecognizer *swipeGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(onSwipe:)];
        [swipeGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:swipeGestureRecognizer];
        
        //Long press
        UILongPressGestureRecognizer *longPressGestureRecognizer=[[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
        [longPressGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:longPressGestureRecognizer];
        
    }

    return  self;
}

//this method is complursory to override
+(Class)layerClass
{
    //code
    return ([CAEAGLLayer class]);
}

/*
 Only override drawRect: if you perform custom drawing.
 An empty implementation adversely affects performance during animation.
//drawRect is only called for immediate mode
 - (void)drawRect:(CGRect)rect {
     // code
}
 */

//will override
//its resize()
-(void)layoutSubviews
{
    //code
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);

    //bind color Render buffer
        glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    
        //Provide storage to color buffer using Apple/driver/native API from resized drawable layer
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer *)[self layer]];
    
        //set attachment of render buffer ie for color buffer attachment is set to 0 th location of color attachment
        
        //get backing width and height of render buffer
        //getting width and height from display layer(ie eaglLayer)
        //because renderbuffer storage was provided by display layer
        GLint width;
        GLint height;
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
    
    //bind depth Render buffer
        glGenRenderbuffers(1, &depthRenderbuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
        
        //Provide storage to depth buffer using opengl API with resized drawable layer width and height
        //IOS default 16 bit support for depth render buffer
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
        
        //set attachment of render buffer ie for depth buffer attachment of depth attachment
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
        
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        {
            printf("\nGL_FRAMEBUFFER is not COMPLETE");
           
        }
    
    if(height < 0)
    {
        height = 1;
    }
        
    glViewport(0, 0, (GLsizei)width, (GLsizei)height);
    
    //************resize code for openGL start******************
    perspectiveProjectionMatrix=vmath::perspective(45.0f,
                                             (GLfloat)width/(GLfloat)height,
                                             0.1f,
                                             100.0f);
    //************resize code for openGL end********************
    
    [self drawView:nil];
    
}

-(void)drawView:(id)sender
{
    //code
    [EAGLContext setCurrentContext:eaglContext];
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
    
    //************shader Draw start******************************
    
    //code
GLfloat materialAmbient[] = { 0.1f,0.1f,0.1f,1.0f };
GLfloat materilaDefuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat materialSpecular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat materialShininess = 50.0f;

GLfloat temp[] = {0.0f,0.0f,0.0f,0.0f};
glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

glUseProgram(gShaderProgramObject);
    mat4 translateMatrix;
    mat4 rotateMatrix;

    mat4 modelMatrix;
    mat4 viewMatrix = mat4::identity();

    rotateMatrix = vmath::rotate(pyramidRotate, 0.0f, 1.0f, 0.0f);
    translateMatrix = vmath::translate(0.0f, 0.0f, -4.0f);
    modelMatrix = translateMatrix * rotateMatrix;

    glUniformMatrix4fv(modelMatrixUniform,
        1,
        GL_FALSE,
        modelMatrix);

    glUniformMatrix4fv(viewMatrixUniform,
        1,
        GL_FALSE,
        viewMatrix);

    glUniformMatrix4fv(projectionlMatrixUniform,
        1,
        GL_FALSE,
        perspectiveProjectionMatrix);

    if (bLights == true)
    {
        glUniform1i(LKeyPressedUniform, 1);

        for (int i = 0; i < 2; i++)
        {

            temp[0] = light[i].LightAmbient[0];
            temp[1] = light[i].LightAmbient[1];
            temp[2] = light[i].LightAmbient[2];
            temp[3] = light[i].LightAmbient[3];
            glUniform3fv(lightAmbientUniform[i], 1,temp);

            temp[0] = light[i].LightDefuse[0];
            temp[1] = light[i].LightDefuse[1];
            temp[2] = light[i].LightDefuse[2];
            temp[3] = light[i].LightDefuse[3];
            glUniform3fv(lightDiffuseUniform[i], 1, temp);

            temp[0] = light[i].LightSpecular[0];
            temp[1] = light[i].LightSpecular[1];
            temp[2] = light[i].LightSpecular[2];
            temp[3] = light[i].LightSpecular[3];
            glUniform3fv(lightSpecularUniform[i], 1, temp);

            temp[0] = light[i].LightPosition[0];
            temp[1] = light[i].LightPosition[1];
            temp[2] = light[i].LightPosition[2];
            temp[3] = light[i].LightPosition[3];
            glUniform4fv(lightPositionUniform[i], 1,temp);//posiitional light
            //fprintf(gpFile, "\nlightPositionUniform[i]:%d\tlight[i].LightPosition:%f,%f,%f,%f", lightPositionUniform[i], temp[0],temp[1],temp[2],temp[3]);

        }
        
        glUniform3fv(materialAmbientUniform, 1, materialAmbient);

        glUniform3fv(materialDiffuseUniform, 1, materilaDefuse);

        glUniform3fv(materialSpecularUniform, 1, materialSpecular);

        glUniform1f(materialShininessUniform, materialShininess);

    }
    else
    {
        glUniform1i(LKeyPressedUniform, 0);
    }



    glBindVertexArray(vao_pyramid);
        glDrawArrays(GL_TRIANGLES,
                        0,
                        12);
    glBindVertexArray(0);

glUseProgram(0);

pyramidRotate+=1.0f;


    //************shader Draw end******************************
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];//swap buffer
    
    
}
//display link - is timer object which matches refresh rate of screen with drawable layer to avoid screen/image terring
-(void)startAnimation
{
    //code
    if(isAnimating == NO)
    {
        //Set dynamically displayLink based on underlying GL technology and set/register callback function for drawView()
        displayLink = [NSClassFromString(@"CADisplayLink") displayLinkWithTarget:self selector:@selector(drawView:)];
        
        //set FPS
        [displayLink setPreferredFramesPerSecond:animationFrameInterval];
        
        //run loop
        //add displaylink to runloop and set display mode
        [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        
        isAnimating = YES;
    }
}

-(void)stopAnimation
{
    //code
    if(isAnimating == YES)
    {
        [displayLink invalidate];
        displayLink = nil;
        isAnimating = NO;
    }
    
}

-(void)onSingleTap:(UITapGestureRecognizer *)gr
{
    //code
    bLights = !bLights;

}

-(void)onDoubleTap:(UITapGestureRecognizer *)gr
{
    //code

}

-(void)onSwipe:(UISwipeGestureRecognizer *)gr
{
    //code
    //exit code
    [self uninitialize];
    [self release];
    exit(0);

}


-(void)onLongPress:(UILongPressGestureRecognizer *)gr
{
    //code


}

-(void)uninitialize
{
    if(vao_pyramid)
    {
        glDeleteVertexArrays(1,
                            &vao_pyramid);
    }
    vao_pyramid=0;

    if(vbo_pyramidPosition)
    {
        glDeleteBuffers(1,
                    &vbo_pyramidPosition);
    }
    vbo_pyramidPosition=0;


    if(gShaderProgramObject)
    {
        glUseProgram(gShaderProgramObject);

        GLsizei shaderCount;
        GLsizei tempShaderCount;
        GLuint *pShader=NULL;

        glGetProgramiv(gShaderProgramObject,GL_ATTACHED_SHADERS,&shaderCount);

        pShader=(GLuint *)malloc(sizeof(GLuint)*shaderCount);
        if(pShader==NULL)
        {
            printf("\nFailed to allocate memory for pShader");
        }else{
            glGetAttachedShaders(gShaderProgramObject,shaderCount,&tempShaderCount,pShader);
            
            printf("\nShader program has actual attached shader count is:%d",tempShaderCount);
            
            for(GLsizei i=0;i<shaderCount;i++)
            {
                glDetachShader(gShaderProgramObject,pShader[i]);
                glDeleteShader(pShader[i]);
                pShader[i]=0;
            }
            free(pShader);
            
            glDeleteProgram(gShaderProgramObject);
            gShaderProgramObject=0;
            glUseProgram(0);
        }
    }
    
    //code
    if(depthRenderbuffer)
    {
        glDeleteRenderbuffers(1, &depthRenderbuffer);
    }
    depthRenderbuffer=0;
    
    if(colorRenderbuffer)
    {
        glDeleteRenderbuffers(1, &colorRenderbuffer);
    }
    colorRenderbuffer=0;
    
    if(defaultFramebuffer)
    {
        glDeleteFramebuffers(1, &defaultFramebuffer);
    }
    defaultFramebuffer=0;
    
    if(eaglContext)
    {
        if([EAGLContext currentContext] == eaglContext)
        {
            [EAGLContext setCurrentContext:nil];
        }
        [eaglContext release];
    }
    eaglContext =nil;

}

///like destructor
-(void)dealloc
{
    ///code
    [self uninitialize];
    [super dealloc];
}


@end
