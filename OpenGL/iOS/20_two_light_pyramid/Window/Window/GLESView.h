//
//  MyView.h
//  Window
//
//  Created by user205082 on 11/6/21.
//

#import <UIKit/UIKit.h>


@interface GLESView : UIView <UIGestureRecognizerDelegate>
-(void)startAnimation;
-(void)stopAnimation;
@end

