//
//  MyView.m
//  Window
//
//  Created by user205082 on 11/6/21.
//
#import<OpenGLES/ES3/gl.h>
#import<OpenGLES/ES3/glext.h>

#import "GLESView.h"

#include "Sphere.h"
#include "Stack_LinkedList.h"

#include"vmath.h"
using namespace vmath;

enum{
    VSV_ATTRIBUTE_POSITION=0,
    VSV_ATTRIBUTE_COLOR,
    VSV_ATTRIBUTE_TEXCOORD,
    VSV_ATTRIBUTE_NORMAL,
};

struct STACK *MatrixStack = NULL;

GLfloat LightAmbient[] = { 0.1f,0.1f,0.1f,1.0f };
GLfloat LightDefuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat LightSpecular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat LightPosition[] = { 100.0f,100.0f,100.0f,1.0f };

GLfloat materialAmbient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat materilaDefuse[] = { 0.5f,0.2f,0.7f,1.0f };
GLfloat materialSpecular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat materialShininess = 128.0f;

void glpushMatrix(datatype data);
datatype glpopMatrix(void);

@implementation GLESView
{
    @private
    //Embedded Apple GL
    EAGLContext *eaglContext;
    
    GLuint defaultFramebuffer;
    GLuint colorRenderbuffer;
    GLuint depthRenderbuffer;
    
    //core animation
    //CADisplayLink --- underlying GL technology might change in future,so we are not directly going to use CADisplayLink pointer
    //thats why `id` is used, it will provide underlying GL driver dynamically
    id displayLink;
    NSInteger animationFrameInterval;
    bool isAnimating;
    
    
    GLuint gVertexShaderObject;
    GLuint gFragmentShaderObject;
    GLuint gShaderProgramObject;

    GLuint vaoSphere;
    GLuint vboSphere_position;
    GLuint vboSphere_normal;
    GLuint vboSphere_elements;

    GLuint modelMatrixUniform;
    GLuint viewMatrixUniform;
    GLuint projectionlMatrixUniform;

    GLuint lightAmbientUniform;
    GLuint lightDiffuseUniform;
    GLuint lightSpecularUniform;
    GLuint lightPositionUniform;

    GLuint materialAmbientUniform;
    GLuint materialDiffuseUniform;
    GLuint materialSpecularUniform;
    GLuint materialShininessUniform;

    GLuint LKeyPressedUniform;

    mat4 perspectiveProjectionMatrix;


    bool bLights;


    int shoulder ;
    int elbow ;

    float spherePosition[1146];
    float sphereNormals[1146];
    float sphereTexcoord[764];
    short sphereElement[2280];//Face Indices

    GLuint gNumSphereVertices, gNumSphereElements;
    
    
}

-(id)initWithFrame:(CGRect)frame
{
    // code
    self = [super initWithFrame:frame];
    if (self)
    {
        //getting render mode display layer
        CAEAGLLayer *eaglLayer =(CAEAGLLayer *)[super layer];
        
        //set Opaque ie make display layer transparent
        [eaglLayer setOpaque:YES];
        
        //set Drawable Properties,define color format:RGBA8 and allow to store window in backing:YES
        [eaglLayer setDrawableProperties:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:NO],kEAGLDrawablePropertyRetainedBacking,
                                          kEAGLColorFormatRGBA8,kEAGLDrawablePropertyColorFormat,nil]];
        
        //create context of OpenFLES 3.0
        eaglContext = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES3];
        if(eaglContext == nil)
        {
            printf("\nOpenGLES Context creation failed");
            return (nil);
        }
    
        //set current context
        [EAGLContext setCurrentContext:eaglContext];
        
        //Create our framebuffer
        glGenFramebuffers(1, &defaultFramebuffer);
        glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
        //Create color Render buffer
            glGenRenderbuffers(1, &colorRenderbuffer);
            glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
        
            //Provide storage to color buffer using Apple/driver/native API
            [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
        
            //set attachment of render buffer ie for color buffer attachment is set to 0 th location of color attachment
            glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderbuffer);
            
            //get backing width and height of render buffer
            //getting width and height from display layer(ie eaglLayer)
            //because renderbuffer storage was provided by display layer
            GLint backingWidth;
            GLint backingHeight;
            glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
            glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
        
        //Create depth Render buffer
            glGenRenderbuffers(1, &depthRenderbuffer);
            glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
            
            //Provide storage to depth buffer using opengl API with width and height
            //IOS default 16 bit support for depth render buffer
            glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
            
            //set attachment of render buffer ie for depth buffer attachment of depth attachment
            glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
            
            if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
            {
                printf("\nGL_FRAMEBUFFER is not COMPLETE");
                [self uninitialize];
                return (nil);
            }
    
        animationFrameInterval = 60;//60 FPS //actually its default to 60 from IOS 8.2
        isAnimating = NO;
        
        printf("\nOpenGL Vender:%s",glGetString(GL_VENDOR));
        printf("\nOpenGL Render:%s",glGetString(GL_RENDERER));
        printf("\nOpenGL Version:%s",glGetString(GL_VERSION));
        printf("\nOpenGL GLSL Version:%s",glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        //***********shader initialize start***************************************
        

        GLint infoLogLength;
        GLint shaderCompileStatus;
        GLint shaderProgramLinkStatus;
        GLchar * szBuffer=NULL;
        GLsizei written;

        //Shader Code Start--------------------
            gVertexShaderObject=glCreateShader(GL_VERTEX_SHADER);

            const GLchar *vertexShaderSourceCode=
            "#version 300 es\n"\
            "\n"\
        "precision highp float;" \
        "precision mediump int;" \
            "in vec4 vPosition;\n"\
            "in vec3 vNormal;\n"\

            "uniform mat4 u_modelMatrix;\n"\
            "uniform mat4 u_viewMatrix;\n"\
            "uniform mat4 u_projectionMatrix;\n"\

            "uniform vec4 u_lightPosition;\n"\
            
            "uniform int u_lKeyPressed;\n"\
            
            "out vec3 out_phongADS_Light;\n"\
            
            "out vec3 out_transformedNormal;\n"\
            "out vec3 out_lightDirection;\n"\
            "out vec3 out_viewVector;\n"\

            "void main(void)\n"\

            "{\n"\
                "if (u_lKeyPressed == 1)\n"\
                "{\n"\

                    "vec4 eyeCoord = u_viewMatrix * u_modelMatrix * vPosition;\n"\
                    
                    "vec3 transformedNormal = mat3(u_viewMatrix * u_modelMatrix) * vNormal ;\n"\
                    "out_transformedNormal = transformedNormal;\n"\

                    "vec3 lightDirection = vec3( u_lightPosition - eyeCoord ) ;\n"\
                    "out_lightDirection = lightDirection;\n"\
                    
                    "vec3 viewVector =  -eyeCoord.xyz ;\n"\
                    "out_viewVector = viewVector;\n"\

                "}\n"\
                
                "gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix * vPosition;\n"\
            
            "}";

            glShaderSource(gVertexShaderObject,
                            1,
                            (const GLchar**)&vertexShaderSourceCode,
                            NULL);
            
            glCompileShader(gVertexShaderObject);

            glGetShaderiv(gVertexShaderObject,
                            GL_COMPILE_STATUS,
                            &shaderCompileStatus);

            if(shaderCompileStatus==GL_FALSE)
            {
                glGetShaderiv(gVertexShaderObject,
                            GL_INFO_LOG_LENGTH,
                            &infoLogLength);
                
                if(infoLogLength > 0)
                {
                    szBuffer=(GLchar*)malloc(sizeof(GLchar)*infoLogLength);
                    if(szBuffer!=NULL)
                    {
                        glGetShaderInfoLog(gVertexShaderObject,
                                            infoLogLength,
                                            &written,
                                            szBuffer);
                        printf("\nERROR:Vertex Shader Object Compilation Failed log : %s",szBuffer);
                        free(szBuffer);
                       [self uninitialize];
                        return(nil);
                    }
                }
            }

            infoLogLength=0;
            shaderCompileStatus=0;
            shaderProgramLinkStatus=0;
            szBuffer=NULL;
            written=0;

            gFragmentShaderObject=glCreateShader(GL_FRAGMENT_SHADER);

            const GLchar *fragmentShaderSourceCode=
            "#version 300 es\n"\
            "\n"\
        "precision highp float;" \
        "precision mediump int;" \
            "vec3 phongADS_Light;\n"
            "out vec4 fragColor;\n"\

            "in vec3 out_transformedNormal;\n"\
            "in vec3 out_lightDirection;\n"\
            "in vec3 out_viewVector;\n"\

            "uniform vec3 u_lightAmbient;\n"\
            "uniform vec3 u_lightDiffuse;\n"\
            "uniform vec3 u_lightSpecular;\n"\

            "uniform vec3 u_materialAmbient;\n"\
            "uniform vec3 u_materialDiffuse;\n"\
            "uniform vec3 u_materialSpecular;\n"\
            "uniform float u_materialShininess;\n"\

            "uniform int u_lKeyPressed;\n"\

            "void main(void)\n"\
            "{"\
                "if (u_lKeyPressed == 1)\n"\
                "{\n"\
                    "vec3 normalizedTransformedNormal = normalize( out_transformedNormal );\n"\
                    "vec3 normalizedLightDirection = normalize( out_lightDirection );\n"\
                    "vec3 normalizedViewVector = normalize( out_viewVector );\n"\

                    "vec3 reflectionVector = reflect( -normalizedLightDirection, normalizedTransformedNormal );\n"\
                    
                    "vec3 ambient = u_lightAmbient * u_materialAmbient;\n"\

                    "vec3 diffuse = u_lightDiffuse * u_materialDiffuse * max( dot(normalizedLightDirection , normalizedTransformedNormal), 0.0);\n"\

                    "vec3 specular = u_lightSpecular * u_materialSpecular * pow( max( dot(reflectionVector , normalizedViewVector), 0.0f) , u_materialShininess);\n"\

                    "phongADS_Light= ambient+ diffuse + specular;\n"\
                    
                "}\n"\
                "else\n"\
                "{\n"\

                    "phongADS_Light=vec3(1.0f, 1.0f, 1.0f);\n"\
                
                "}\n"\
                
                "fragColor = vec4( phongADS_Light, 1.0f );\n"\

            "}";

            glShaderSource(gFragmentShaderObject,
                            1,
                            (const GLchar**)&fragmentShaderSourceCode,
                            NULL);
            
            glCompileShader(gFragmentShaderObject);

            glGetShaderiv(gFragmentShaderObject,
                        GL_COMPILE_STATUS,
                        &shaderCompileStatus);

            if(shaderCompileStatus==GL_FALSE)
            {
                glGetShaderiv(gFragmentShaderObject,
                            GL_INFO_LOG_LENGTH,
                            &infoLogLength);
                
                if(infoLogLength > 0)
                {
                    szBuffer=(GLchar*)malloc(sizeof(GLchar)*infoLogLength);
                    if(szBuffer!=NULL)
                    {
                        glGetShaderInfoLog(gFragmentShaderObject,
                                            infoLogLength,
                                            &written,
                                            szBuffer);
                        printf("\nERROR:Fragment Shader Object Compilation Failed log : %s",szBuffer);
                        free(szBuffer);
                        [self uninitialize];
                         return(nil);
                    }
                }
            }

            infoLogLength=0;
            shaderCompileStatus=0;
            shaderProgramLinkStatus=0;
            szBuffer=NULL;
            written=0;

            gShaderProgramObject=glCreateProgram();

            glAttachShader(gShaderProgramObject,gVertexShaderObject);
            glAttachShader(gShaderProgramObject,gFragmentShaderObject);

            //Shader Attribute Binding Before Pre Linking of attached Shader
                glBindAttribLocation(gShaderProgramObject,
                                    VSV_ATTRIBUTE_POSITION,
                                    "vPosition");
                glBindAttribLocation(gShaderProgramObject,
                                    VSV_ATTRIBUTE_NORMAL,
                                    "vNormal");

            glLinkProgram(gShaderProgramObject);

            glGetProgramiv(gShaderProgramObject,
                            GL_LINK_STATUS,
                            &shaderProgramLinkStatus);
            
            if(shaderProgramLinkStatus==GL_FALSE)
            {
                glGetProgramiv(gShaderProgramObject,
                                GL_INFO_LOG_LENGTH,
                                &infoLogLength);
                if(infoLogLength > 0)
                {
                    szBuffer=(GLchar *)malloc(sizeof(GLchar)*infoLogLength);

                    if(szBuffer!=NULL)
                    {
                        glGetProgramInfoLog(gShaderProgramObject,
                                            infoLogLength,
                                            &written,
                                            szBuffer);

                        printf("\nERROR:Shader program Link failed log : %s",szBuffer);
                        free(szBuffer);
                        [self uninitialize];
                         return(nil);
                    }
                }
            }

        //Shader Code End--------------------

        //Uniform variable Binding after post linking of attached shader
            modelMatrixUniform = glGetUniformLocation(gShaderProgramObject,"u_modelMatrix");
            viewMatrixUniform = glGetUniformLocation(gShaderProgramObject,"u_viewMatrix");
            projectionlMatrixUniform = glGetUniformLocation(gShaderProgramObject,"u_projectionMatrix");

            lightAmbientUniform = glGetUniformLocation(gShaderProgramObject,"u_lightAmbient");
            lightDiffuseUniform = glGetUniformLocation(gShaderProgramObject,"u_lightDiffuse");
            lightSpecularUniform = glGetUniformLocation(gShaderProgramObject,"u_lightSpecular");
            lightPositionUniform = glGetUniformLocation(gShaderProgramObject,"u_lightPosition");

            materialAmbientUniform = glGetUniformLocation(gShaderProgramObject,"u_materialAmbient");
            materialDiffuseUniform = glGetUniformLocation(gShaderProgramObject,"u_materialDiffuse");
            materialSpecularUniform = glGetUniformLocation(gShaderProgramObject,"u_materialSpecular");
            materialShininessUniform = glGetUniformLocation(gShaderProgramObject,"u_materialShininess");

            LKeyPressedUniform = glGetUniformLocation(gShaderProgramObject,"u_lKeyPressed");

        //VAO and its respective VBO preparation

             Sphere *sphere=new Sphere();
            sphere->sphereInitialize();
            
            sphere->getSphereVertexData(spherePosition, sphereNormals, sphereTexcoord, sphereElement);
            gNumSphereVertices = sphere->getNumberOfSphereVertices();
            gNumSphereElements = sphere->getNumberOfSphereElements();

            glGenVertexArrays(1,
                            &vaoSphere);

            glBindVertexArray(vaoSphere);

                glGenBuffers(1,
                            &vboSphere_position);

                glBindBuffer(GL_ARRAY_BUFFER,
                            vboSphere_position);

                    glBufferData(GL_ARRAY_BUFFER,
                                sizeof(spherePosition),
                                spherePosition,
                                GL_STATIC_DRAW);
                    
                    glVertexAttribPointer(VSV_ATTRIBUTE_POSITION,
                                        3,
                                        GL_FLOAT,
                                        GL_FALSE,
                                        0,
                                        NULL);

                    glEnableVertexAttribArray(VSV_ATTRIBUTE_POSITION);
                
                glBindBuffer(GL_ARRAY_BUFFER,
                            0);
                
                glGenBuffers(1,
                            &vboSphere_normal);

                glBindBuffer(GL_ARRAY_BUFFER,
                            vboSphere_normal);

                    glBufferData(GL_ARRAY_BUFFER,
                                    sizeof(sphereNormals),
                                    sphereNormals,
                                    GL_STATIC_DRAW);

                    glVertexAttribPointer(VSV_ATTRIBUTE_NORMAL,
                        3,
                        GL_FLOAT,
                        GL_FALSE,
                        0,
                        NULL);

                    glEnableVertexAttribArray(VSV_ATTRIBUTE_NORMAL);

                glBindBuffer(GL_ARRAY_BUFFER,
                    0);

                glGenBuffers(1,
                    &vboSphere_normal);

                glBindBuffer(GL_ARRAY_BUFFER,
                    vboSphere_normal);

                    glBufferData(GL_ARRAY_BUFFER,
                        sizeof(sphereNormals),
                        sphereNormals,
                        GL_STATIC_DRAW);

                    glVertexAttribPointer(VSV_ATTRIBUTE_NORMAL,
                        3,
                        GL_FLOAT,
                        GL_FALSE,
                        0,
                        NULL);

                    glEnableVertexAttribArray(VSV_ATTRIBUTE_NORMAL);

                glBindBuffer(GL_ARRAY_BUFFER,
                    0);


                glGenBuffers(1,
                    &vboSphere_elements);

                glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,
                    vboSphere_elements);

                    glBufferData(GL_ELEMENT_ARRAY_BUFFER,
                        sizeof(sphereElement),
                        sphereElement,
                        GL_STATIC_DRAW);

                glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,
                    0);

            glBindVertexArray(0);


        //SetColor
        glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        
        glClearDepthf(1.0f);
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);

        bLights =true;
        
        //Warmup resize call
        perspectiveProjectionMatrix=mat4::identity();
        
        //***********shader initialize end***************************************

        //register events/Gestures
        //1-register function
        //2-set Number Of Taps Required
        //3-set Number Of Touches Required
        //4-set Delegate
        //5-register/add Gesture Recognizer/event
        
        //Single tap event
        UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onSingleTap:)];
        [singleTapGestureRecognizer setNumberOfTapsRequired:1];
        [singleTapGestureRecognizer setNumberOfTouchesRequired:1];
        [singleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:singleTapGestureRecognizer];
        
        //double tap event
        UITapGestureRecognizer *doubleTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onDoubleTap:)];
        [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
        [doubleTapGestureRecognizer setNumberOfTouchesRequired:1];
        [doubleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:doubleTapGestureRecognizer];
        
        //this will block double tap to recognize as two single tap
        [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
        
        //Swipe event
        UISwipeGestureRecognizer *swipeGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(onSwipe:)];
        [swipeGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:swipeGestureRecognizer];
        
        //Long press
        UILongPressGestureRecognizer *longPressGestureRecognizer=[[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
        [longPressGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:longPressGestureRecognizer];
        
    }

    return  self;
}

//this method is complursory to override
+(Class)layerClass
{
    //code
    return ([CAEAGLLayer class]);
}

/*
 Only override drawRect: if you perform custom drawing.
 An empty implementation adversely affects performance during animation.
//drawRect is only called for immediate mode
 - (void)drawRect:(CGRect)rect {
     // code
}
 */

//will override
//its resize()
-(void)layoutSubviews
{
    //code
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);

    //bind color Render buffer
        glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    
        //Provide storage to color buffer using Apple/driver/native API from resized drawable layer
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer *)[self layer]];
    
        //set attachment of render buffer ie for color buffer attachment is set to 0 th location of color attachment
        
        //get backing width and height of render buffer
        //getting width and height from display layer(ie eaglLayer)
        //because renderbuffer storage was provided by display layer
        GLint width;
        GLint height;
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
    
    //bind depth Render buffer
        glGenRenderbuffers(1, &depthRenderbuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
        
        //Provide storage to depth buffer using opengl API with resized drawable layer width and height
        //IOS default 16 bit support for depth render buffer
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
        
        //set attachment of render buffer ie for depth buffer attachment of depth attachment
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
        
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        {
            printf("\nGL_FRAMEBUFFER is not COMPLETE");
           
        }
    
    if(height < 0)
    {
        height = 1;
    }
        
    glViewport(0, 0, (GLsizei)width, (GLsizei)height);
    
    //************resize code for openGL start******************
    perspectiveProjectionMatrix=vmath::perspective(45.0f,
                                                (GLfloat)width/(GLfloat)height,
                                                0.1f,
                                                100.0f);
    //************resize code for openGL end********************
    
    [self drawView:nil];
    
}

-(void)drawView:(id)sender
{
    //code
    [EAGLContext setCurrentContext:eaglContext];
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
    
    //************shader Draw start******************************
    
    mat4 translateMatrix;
    mat4 rotateMatrix;
    mat4 scaleMatrix;

    mat4 modelMatrix;
    mat4 viewMatrix = mat4::identity();

    //code
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

    glUseProgram(gShaderProgramObject);
    
        if (bLights == true)
        {
            glUniform1i(LKeyPressedUniform, 1);

            glUniform3fv(lightAmbientUniform, 1, LightAmbient);
            glUniform3fv(lightDiffuseUniform, 1, LightDefuse);
            glUniform3fv(lightSpecularUniform, 1, LightSpecular);
            glUniform4fv(lightPositionUniform, 1, LightPosition);//posiitional light

            glUniform3fv(materialAmbientUniform, 1, materialAmbient);
            glUniform3fv(materialDiffuseUniform, 1, materilaDefuse);
            glUniform3fv(materialSpecularUniform, 1, materialSpecular);

            glUniform1f(materialShininessUniform, materialShininess);
        }
        else
        {
            glUniform1i(LKeyPressedUniform, 0);
        }

        translateMatrix = vmath::translate(0.0f,0.0f,-12.0f);
        modelMatrix = translateMatrix;

        glpushMatrix(modelMatrix);
            
            rotateMatrix = vmath::rotate((GLfloat)shoulder, 0.0f, 0.0f, 1.0f);
            translateMatrix = vmath::translate(1.0f, 0.0f, 0.0f);

            modelMatrix = modelMatrix  * rotateMatrix * translateMatrix;

            glpushMatrix(modelMatrix);

                scaleMatrix = vmath::scale(2.0f, 0.5f, 1.0f);

                modelMatrix = modelMatrix * scaleMatrix;

                glUniformMatrix4fv(modelMatrixUniform,1,GL_FALSE,modelMatrix);

                glUniformMatrix4fv(viewMatrixUniform,1,GL_FALSE,viewMatrix);
        
                glUniformMatrix4fv(projectionlMatrixUniform,1,GL_FALSE,perspectiveProjectionMatrix);

                glBindVertexArray(vaoSphere);
                    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboSphere_elements);
                        glDrawElements(GL_TRIANGLES, gNumSphereElements, GL_UNSIGNED_SHORT, 0);
                    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
                glBindVertexArray(0);

            modelMatrix = glpopMatrix();
            
            translateMatrix = vmath::translate(1.0f, 0.0f, 0.0f);
            rotateMatrix = vmath::rotate((GLfloat)elbow, 0.0f, 0.0f, 1.0f);

            modelMatrix = modelMatrix * translateMatrix * rotateMatrix;

            translateMatrix = vmath::translate(0.9f, 0.0f, 0.0f);

            modelMatrix = modelMatrix * translateMatrix;

            glpushMatrix(modelMatrix);

                scaleMatrix = vmath::scale(2.0f, 0.5f, 1.0f);
                modelMatrix = modelMatrix * scaleMatrix;

                glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix);

                glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);

                glUniformMatrix4fv(projectionlMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

                glBindVertexArray(vaoSphere);
                    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboSphere_elements);
                        glDrawElements(GL_TRIANGLES, gNumSphereElements, GL_UNSIGNED_SHORT, 0);
                    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
                glBindVertexArray(0);

            modelMatrix = glpopMatrix();
        modelMatrix = glpopMatrix();
    glUseProgram(0);
    //************shader Draw end******************************
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];//swap buffer
    
    
}
//display link - is timer object which matches refresh rate of screen with drawable layer to avoid screen/image terring
-(void)startAnimation
{
    //code
    if(isAnimating == NO)
    {
        //Set dynamically displayLink based on underlying GL technology and set/register callback function for drawView()
        displayLink = [NSClassFromString(@"CADisplayLink") displayLinkWithTarget:self selector:@selector(drawView:)];
        
        //set FPS
        [displayLink setPreferredFramesPerSecond:animationFrameInterval];
        
        //run loop
        //add displaylink to runloop and set display mode
        [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        
        isAnimating = YES;
    }
}

-(void)stopAnimation
{
    //code
    if(isAnimating == YES)
    {
        [displayLink invalidate];
        displayLink = nil;
        isAnimating = NO;
    }
    
}

-(void)onSingleTap:(UITapGestureRecognizer *)gr
{
    //code
    shoulder = (shoulder + 3) % 360;
    elbow = (elbow + 3) % 360;

}

-(void)onDoubleTap:(UITapGestureRecognizer *)gr
{
    //code
    shoulder = (shoulder - 3) % 360;
    elbow = (elbow - 3) % 360;

}

-(void)onSwipe:(UISwipeGestureRecognizer *)gr
{
    //code
    //exit code
    [self uninitialize];
    [self release];
    exit(0);

}


-(void)onLongPress:(UILongPressGestureRecognizer *)gr
{
    //code


}

-(void)uninitialize
{
    //code
    if(depthRenderbuffer)
    {
        glDeleteRenderbuffers(1, &depthRenderbuffer);
    }
    depthRenderbuffer=0;
    
    if(colorRenderbuffer)
    {
        glDeleteRenderbuffers(1, &colorRenderbuffer);
    }
    colorRenderbuffer=0;
    
    if(defaultFramebuffer)
    {
        glDeleteFramebuffers(1, &defaultFramebuffer);
    }
    defaultFramebuffer=0;
    
    if(eaglContext)
    {
        if([EAGLContext currentContext] == eaglContext)
        {
            [EAGLContext setCurrentContext:nil];
        }
        [eaglContext release];
    }
    eaglContext =nil;

}

///like destructor
-(void)dealloc
{
    ///code
    [self uninitialize];
    [super dealloc];
}


@end

void glpushMatrix(datatype data)
{
    PushIntoStack(&MatrixStack, CreateStackNode(data));
}

datatype glpopMatrix(void)
{
    return PopFromStack(&MatrixStack);
}
