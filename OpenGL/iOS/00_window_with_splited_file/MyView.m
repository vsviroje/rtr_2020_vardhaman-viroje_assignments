#import<Foundation/Foundation.h> ///get foundation header file from foundation framework (similar to standard library like stdio.h)
#import<Cocoa/Cocoa.h>///analogus to window.h/xlib.h

#import"MyView.h"

//implmentating method for MyView
@implementation MyView
{
    @private ///private variable
    NSString *centeralText;
}

///id is a object of class
-(id)initWithFrame:(NSRect)frame
{
    ///code
    self = [super initWithFrame:frame];
    if (self)
    {
        centeralText = @"Hello World";
    }

    return self;
}

//InvalidateRect is similar to dirtyRect
-(void)drawRect:(NSRect)dirtyRect
{
    ///code
    NSColor *backgroundColor = [NSColor blackColor];
    [backgroundColor set];

    NSRectFill(dirtyRect);

    NSDictionary *dictionaryForTextAttribute = [NSDictionary dictionaryWithObjectsAndKeys:[NSFont fontWithName:@"Helvetica" size:32],NSFontAttributeName,
                                                                                          [NSColor greenColor],NSForegroundColorAttributeName,
                                                                                          nil];

    NSSize textSize = [centeralText sizeWithAttributes:dictionaryForTextAttribute];

    NSPoint point;
    point.x = (dirtyRect.size.width/2)-(textSize.width/2);
    point.y = (dirtyRect.size.height/2)-(textSize.height/2) + 12;

    [centeralText drawAtPoint:point withAttributes:dictionaryForTextAttribute];
}

///to shift keyboard and mouse controll from NSwindow to NSview
///NSReponder->NSobject method
-(BOOL)acceptsFirstResponder
{
    ///code
    [[self window] makeFirstResponder:self];

    return YES;
}

-(void)keyDown:(NSEvent*)theEvent
{
    ///code
    int key = [[theEvent characters] characterAtIndex:0];
    switch(key)
    {
        case 27://escape
            [self release];
            [NSApp terminate:self];
            break;
        case 'f':
        case 'F':
            [[self window] toggleFullScreen:self];
            break;
    }
}

-(void)mouseDown:(NSEvent*)theEvent
{
    ///code
    centeralText = @"left mouse button is click";
    //invalidateRect ---refresh
    [self setNeedsDisplay:YES];

}

-(void)rightMouseDown:(NSEvent*)theEvent
{
    ///code
    centeralText = @"right mouse button is click";
    //invalidateRect ---refresh
    [self setNeedsDisplay:YES];
    
}

-(void)otherMouseDown:(NSEvent*)theEvent
{
    ///code
    centeralText = @"Hello World";
    //invalidateRect ---refresh
    [self setNeedsDisplay:YES];
}

///like destructor
-(void)dealloc
{
    ///code
    [super dealloc];
}


@end
