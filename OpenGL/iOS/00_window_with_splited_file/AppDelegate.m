#import<Foundation/Foundation.h> ///get foundation header file from foundation framework (similar to standard library like stdio.h)
#import<Cocoa/Cocoa.h>///analogus to window.h/xlib.h

#import"AppDelegate.h"
#import"MyView.h"

//implmentating method for AppDelegate
@implementation AppDelegate
{
    @private ///private variable
    NSWindow *window;
    MyView *view;
}

///when application is getting started, then applicationDidFinishLaunching() a method of NSApplication is called
///WM_Create and applicationDidFinishLaunching() are analogus
-(void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    ///code
    ///NSRect is internally CGRect -core graphic ..its c based library
    ///CGPoint/NSPoint-x,y and CGSize/NSSize-width,height
    NSRect winRect = NSMakeRect(0.0,0.0,800.0,600.0);

    ///styleMask is similer to WM_overlappedWindow
    ///backing- how to store window in graphics
    ///defer - to halt showing of window or show window immediately
    window = [[NSWindow alloc] initWithContentRect:winRect
                                                styleMask:NSWindowStyleMaskTitled |
                                                        NSWindowStyleMaskClosable |
                                                        NSWindowStyleMaskMiniaturizable |
                                                        NSWindowStyleMaskResizable
                                                        backing:NSBackingStoreBuffered
                                                        defer:NO];
    
    [window setTitle:@"VSV : MacOS Window"];
    [window center];

    ///FrameSize will be same as windowSize
    view = [[MyView alloc] initWithFrame:winRect];
    ///assigning Frame to our window
    [window setContentView:view];

    ///NSWindow method will be implemented by itself
    ///if not use this line then app will not removed from taskbar
    [window setDelegate:self];

    ///its like setFocus() and setForgroundWindow() or MB_TOPMOST
    [window makeKeyAndOrderFront:self];
}

///when application is gets terminated, then applicationWillTerminate() a method of NSApplication is called
///WM_Destroy and applicationWillTerminate() are analogus
-(void)applicationWillTerminate:(NSNotification *)aNotification
{
    ///code
}

///NSWindowDelegate method
-(void)windowWillClose:(NSNotification *)aNotification
{
    ///code
    [NSApp terminate:self];
}

///like destructor
-(void)dealloc
{
    ///code
    [view release];
    [window release];
    [super dealloc];
}

@end
