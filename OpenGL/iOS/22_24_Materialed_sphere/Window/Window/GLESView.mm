//
//  MyView.m
//  Window
//
//  Created by user205082 on 11/6/21.
//
#import<OpenGLES/ES3/gl.h>
#import<OpenGLES/ES3/glext.h>

#import "GLESView.h"

#import"Sphere.h"
#import"vmath.h"
using namespace vmath;

enum{
    VSV_ATTRIBUTE_POSITION=0,
    VSV_ATTRIBUTE_COLOR,
    VSV_ATTRIBUTE_TEXCOORD,
    VSV_ATTRIBUTE_NORMAL,
};



GLfloat LightAmbient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat LightDefuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat LightSpecular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat LightPosition[] = { 0.0f,0.0f,0.0f,1.0f };

typedef struct Material {
    vec4 materialAmbient;
    vec4 materilaDefuse;
    vec4 materialSpecular;
    GLfloat materialShininess;
}MaterialStruct;

MaterialStruct Materials[24] = {
                        {
                            vec4(0.0215,0.1745,0.0215,1.0),
                            vec4(0.07568,0.61424,0.07568,1.0),
                            vec4(0.633,0.727811,0.633,1.0),
                            0.6 * 128
                        },
                        {
                            vec4(0.135,0.2225,0.1575,1.0),
                            vec4(0.54,0.89 ,0.63,1.0),
                            vec4(0.316228,0.316228,0.316228,1.0),
                            0.1 * 128
                        },
                        {
                            vec4(0.05375,0.05,0.06625,1.0),
                            vec4(0.18275,0.17,0.22525,1.0),
                            vec4(0.332741,0.328634,0.346435,1.0),
                            0.3 * 128
                        },{
                            vec4(0.25,0.20725,0.20725,1.0),
                            vec4(1.0,0.829,0.829,1.0),
                            vec4(0.296648,0.296648,0.296648,1.0),
                            0.088 * 128
                        },{
                            vec4(0.1745,0.01175,0.01175,1.0),
                            vec4(0.61424,0.04136,0.04136,1.0),
                            vec4(0.727811,0.626959,0.626959,1.0),
                            0.6 * 128
                        },{
                            vec4(0.1,0.18725,0.1745,1.0f),
                            vec4(0.396,0.74151,0.69102,1.0),
                            vec4(0.297254,0.30829,0.306678,1.0),
                            0.1 * 128
                        },{
                            vec4(0.329412,0.223529,0.027451,1.0),
                            vec4(0.780392,0.568627,0.113725,1.0),
                            vec4(0.992157,0.941176,0.807843,1.0),
                            0.21794872 * 128
                        },{
                            vec4(0.2125,0.1275,0.054,1.0f),
                            vec4(0.714,0.4284,0.18144,1.0f),
                            vec4(0.393548,0.271906,0.166721,1.0),
                            0.2 * 128
                        },{
                            vec4(0.25,0.25,0.25,1.0),
                            vec4(0.4,0.4,0.4,1.0),
                            vec4(0.774597,0.774597,0.774597,1.0),
                            0.6 * 128
                        },{
                            vec4(0.19125,0.0735,0.0225,1.0),
                            vec4(0.7038,0.27048,0.0828,1.0),
                            vec4(0.256777,0.137622,0.086014,1.0),
                            0.1 * 128
                        },{
                            vec4(0.24725,0.1995,0.0745,1.0),
                            vec4(0.75164,0.60648,0.22648,1.0),
                            vec4(0.628281,0.555802,0.366065,1.0),
                            0.4 * 128
                        },{
                            vec4(0.19225,0.19225,0.19225,1.0),
                            vec4(0.50754,0.50754,0.50754,1.0),
                            vec4(0.508273,0.508273,0.508273,1.0),
                            0.4 * 128
                        },{
                            vec4(0.0,0.0,0.0,1.0),
                            vec4(0.01,0.01,0.01,1.0),
                            vec4(0.50,0.50,0.50,1.0),
                            0.25 * 128
                        },{
                            vec4(0.0,0.1,0.06,1.0),
                            vec4(0.0,0.50980392,0.50980392,1.0),
                            vec4(0.50196078,0.50196078,0.50196078,1.0),
                            0.25 * 128
                        },{
                            vec4(0.0,0.0,0.0,1.0),
                            vec4(0.1,0.35,0.1,1.0),
                            vec4(0.45,0.55,0.45,1.0),
                            0.25 * 128
                        },{
                            vec4(0.0,0.0,0.0,1.0),
                            vec4(0.5,0.0,0.0,1.0),
                            vec4(0.7,0.6,0.6,1.0),
                            0.25 * 128
                        },{
                            vec4(0.0,0.0,0.0,1.0),
                            vec4(0.55,0.55,0.55,1.0),
                            vec4(0.70,0.70,0.70,1.0),
                            0.25 * 128
                        },{
                            vec4(0.0,0.0,0.0,1.0),
                            vec4(0.5,0.5,0.0,1.0),
                            vec4(0.60,0.60,0.60,1.0),
                            0.25 * 128
                        },{
                            vec4(0.02,0.02,0.02,1.0),
                            vec4(0.01,0.01,0.01,1.0),
                            vec4(0.4,0.4,0.4,1.0),
                            0.078125 * 128
                        },{
                            vec4(0.0,0.05,0.05,1.0),
                            vec4(0.4,0.5,0.5,1.0),
                            vec4(0.04,0.7,0.7,1.0),
                            0.078125 * 128
                        }, {
                            vec4(0.0,0.05,0.0,1.0),
                            vec4(0.4,0.5,0.4,1.0),
                            vec4(0.04,0.7,0.04,1.0f),
                            0.078125 * 128
                        }, {
                            vec4(0.05,0.0,0.0,1.0),
                            vec4(0.5,0.4,0.4,1.0),
                            vec4(0.7,0.04,0.04,1.0),
                            0.078125 * 128
                        }, {
                            vec4(0.05,0.05,0.05,1.0),
                            vec4(0.5,0.5,0.5,1.0),
                            vec4(0.7,0.7,0.7,1.0),
                            0.078125 * 128
                        }, {
                            vec4(0.05,0.05,0.0,1.0),
                            vec4(0.5,0.5,0.4,1.0),
                            vec4(0.7,0.7,0.04,1.0),
                            0.078125 * 128
                        }
                            };



@implementation GLESView
{
    @private
    //Embedded Apple GL
    EAGLContext *eaglContext;
    
    GLuint defaultFramebuffer;
    GLuint colorRenderbuffer;
    GLuint depthRenderbuffer;
    
    //core animation
    //CADisplayLink --- underlying GL technology might change in future,so we are not directly going to use CADisplayLink pointer
    //thats why `id` is used, it will provide underlying GL driver dynamically
    id displayLink;
    NSInteger animationFrameInterval;
    bool isAnimating;
    
    int RotationType;//x-0,y-1,z-2

           GLsizei orignal_width;
           GLsizei orignal_height;


           //-------------PF---------------
           GLuint gShaderProgramObject;

           GLuint modelMatrixUniform;
           GLuint viewMatrixUniform;
           GLuint projectionlMatrixUniform;

           GLuint lightAmbientUniform;
           GLuint lightDiffuseUniform;
           GLuint lightSpecularUniform;
           GLuint lightPositionUniform;

           GLuint materialAmbientUniform;
           GLuint materialDiffuseUniform;
           GLuint materialSpecularUniform;
           GLuint materialShininessUniform;

           GLuint LKeyPressedUniform;

           //-----------comman------------
           GLuint gVertexShaderObject;
           GLuint gFragmentShaderObject;

           GLuint vaoSphere;
           GLuint vboSphere_position;
           GLuint vboSphere_normal;
           GLuint vboSphere_elements;

           bool bLights;

           mat4 perspectiveProjectionMatrix;

           float spherePosition[1146];
           float sphereNormals[1146];
           float sphereTexcoord[764];
           short sphereElement[2280];//Face Indices

           GLuint gNumSphereVertices, gNumSphereElements;

           GLfloat Angle;
}

-(id)initWithFrame:(CGRect)frame
{
    // code
    self = [super initWithFrame:frame];
    if (self)
    {
        //getting render mode display layer
        CAEAGLLayer *eaglLayer =(CAEAGLLayer *)[super layer];
        
        //set Opaque ie make display layer transparent
        [eaglLayer setOpaque:YES];
        
        //set Drawable Properties,define color format:RGBA8 and allow to store window in backing:YES
        [eaglLayer setDrawableProperties:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:NO],kEAGLDrawablePropertyRetainedBacking,
                                          kEAGLColorFormatRGBA8,kEAGLDrawablePropertyColorFormat,nil]];
        
        //create context of OpenFLES 3.0
        eaglContext = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES3];
        if(eaglContext == nil)
        {
            printf("\nOpenGLES Context creation failed");
            return (nil);
        }
    
        //set current context
        [EAGLContext setCurrentContext:eaglContext];
        
        //Create our framebuffer
        glGenFramebuffers(1, &defaultFramebuffer);
        glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
        //Create color Render buffer
            glGenRenderbuffers(1, &colorRenderbuffer);
            glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
        
            //Provide storage to color buffer using Apple/driver/native API
            [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
        
            //set attachment of render buffer ie for color buffer attachment is set to 0 th location of color attachment
            glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderbuffer);
            
            //get backing width and height of render buffer
            //getting width and height from display layer(ie eaglLayer)
            //because renderbuffer storage was provided by display layer
            GLint backingWidth;
            GLint backingHeight;
            glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
            glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
        
        //Create depth Render buffer
            glGenRenderbuffers(1, &depthRenderbuffer);
            glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
            
            //Provide storage to depth buffer using opengl API with width and height
            //IOS default 16 bit support for depth render buffer
            glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
            
            //set attachment of render buffer ie for depth buffer attachment of depth attachment
            glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
            
            if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
            {
                printf("\nGL_FRAMEBUFFER is not COMPLETE");
                [self uninitialize];
                return (nil);
            }
    
        animationFrameInterval = 60;//60 FPS //actually its default to 60 from IOS 8.2
        isAnimating = NO;
        
        printf("\nOpenGL Vender:%s",glGetString(GL_VENDOR));
        printf("\nOpenGL Render:%s",glGetString(GL_RENDERER));
        printf("\nOpenGL Version:%s",glGetString(GL_VERSION));
        printf("\nOpenGL GLSL Version:%s",glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        //***********shader initialize start***************************************
        
        GLint infoLogLength;
        GLint shaderCompileStatus;
        GLint shaderProgramLinkStatus;
        GLchar *szBuffer;
        GLsizei written;

        //Per Fragment Shader Code Start--------------------
        gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

        const GLchar* vertexShaderSourceCode_PF =
            "#version 300 es\n"\
            "\n"\
        "precision highp float;" \
        "precision mediump int;" \
            "in vec4 vPosition;\n"\
            "in vec3 vNormal;\n"\

            "uniform mat4 u_modelMatrix;\n"\
            "uniform mat4 u_viewMatrix;\n"\
            "uniform mat4 u_projectionMatrix;\n"\

            "uniform vec4 u_lightPosition;\n"\

            "uniform int u_lKeyPressed;\n"\

            "out vec3 out_transformedNormal;\n"\
            "out vec3 out_lightDirection;\n"\
            "out vec3 out_viewVector;\n"\

            "void main(void)\n"\

            "{\n"\
                "if (u_lKeyPressed == 1)\n"\
                "{\n"\

                    "vec4 eyeCoord = u_viewMatrix * u_modelMatrix * vPosition;\n"\

                    "out_viewVector = -eyeCoord.xyz;\n"\

                    "out_transformedNormal = mat3(u_viewMatrix * u_modelMatrix) * vNormal ;\n"\

                    "out_lightDirection =  vec3( u_lightPosition - eyeCoord ) ;\n"\

                "}\n"\
            
                "gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix * vPosition;\n"\

            "}";

        glShaderSource(gVertexShaderObject,
            1,
            (const GLchar**)&vertexShaderSourceCode_PF,
            NULL);

        glCompileShader(gVertexShaderObject);

        glGetShaderiv(gVertexShaderObject,
            GL_COMPILE_STATUS,
            &shaderCompileStatus);

        if (shaderCompileStatus == GL_FALSE)
        {
            glGetShaderiv(gVertexShaderObject,
                GL_INFO_LOG_LENGTH,
                &infoLogLength);

            if (infoLogLength > 0)
            {
                szBuffer = (GLchar*)malloc(sizeof(GLchar) * infoLogLength);
                if (szBuffer != NULL)
                {
                    glGetShaderInfoLog(gVertexShaderObject,
                        infoLogLength,
                        &written,
                        szBuffer);
                    printf( "\nERROR:Vertex Shader Object Compilation Failed log : %s", szBuffer);
                    free(szBuffer);
                    [self uninitialize];
                    return(nil);
                }
            }
        }

        infoLogLength = 0;
        shaderCompileStatus = 0;
        shaderProgramLinkStatus = 0;
        szBuffer = NULL;
        written = 0;

        gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

        const GLchar* fragmentShaderSourceCode_PF =
            "#version 300 es"\
            "\n"\
        "precision highp float;" \
        "precision mediump int;" \
            "uniform vec3 u_lightAmbient;\n"\
            "uniform vec3 u_lightDiffuse;\n"\
            "uniform vec3 u_lightSpecular;\n"\

            "uniform vec3 u_materialAmbient;\n"\
            "uniform vec3 u_materialDiffuse;\n"\
            "uniform vec3 u_materialSpecular;\n"\
            "uniform float u_materialShininess;\n"\

            "uniform int u_lKeyPressed;\n"\

            "in vec3 out_transformedNormal;\n"\
            "in vec3 out_lightDirection;\n"\
            "in vec3 out_viewVector;\n"\

            "vec3 phongADS_Light;\n"
            "out vec4 fragColor;"\

            "void main(void)"\
            "{"\

                "if (u_lKeyPressed == 1)\n"\
                "{\n"\

                    "vec3 normalizedTransformedNormal = normalize( out_transformedNormal );\n"\
                    "vec3 normalizedLightDirection = normalize( out_lightDirection );\n"\
                    "vec3 normalizedViewVector = normalize( out_viewVector );\n"\

                    "vec3 reflectionVector = reflect( -normalizedLightDirection, normalizedTransformedNormal );\n"\

                    "vec3 ambient= u_lightAmbient * u_materialAmbient;\n"\

                    "vec3 diffuse = u_lightDiffuse * u_materialDiffuse * max( dot(normalizedLightDirection , normalizedTransformedNormal), 0.0);\n"\

                    "vec3 specular = u_lightSpecular * u_materialSpecular * pow( max( dot(reflectionVector , normalizedViewVector), 0.0f) , u_materialShininess);\n"\

                    "phongADS_Light = ambient + diffuse + specular;\n"\

                "}\n"\
                "else\n"\
                "{\n"\
                    "phongADS_Light=vec3(1.0f,1.0f,1.0f);\n"\
                "}\n"\

                "fragColor = vec4( phongADS_Light, 1.0f );"\
            "}";

        glShaderSource(gFragmentShaderObject,
            1,
            (const GLchar**)&fragmentShaderSourceCode_PF,
            NULL);

        glCompileShader(gFragmentShaderObject);

        glGetShaderiv(gFragmentShaderObject,
            GL_COMPILE_STATUS,
            &shaderCompileStatus);

        if (shaderCompileStatus == GL_FALSE)
        {
            glGetShaderiv(gFragmentShaderObject,
                GL_INFO_LOG_LENGTH,
                &infoLogLength);

            if (infoLogLength > 0)
            {
                szBuffer = (GLchar*)malloc(sizeof(GLchar) * infoLogLength);
                if (szBuffer != NULL)
                {
                    glGetShaderInfoLog(gFragmentShaderObject,
                        infoLogLength,
                        &written,
                        szBuffer);
                    printf( "\nERROR:Fragment Shader Object Compilation Failed log : %s", szBuffer);
                    free(szBuffer);
                    [self uninitialize];
                    return(nil);
                }
            }
        }

        infoLogLength = 0;
        shaderCompileStatus = 0;
        shaderProgramLinkStatus = 0;
        szBuffer = NULL;
        written = 0;

        gShaderProgramObject = glCreateProgram();

        glAttachShader(gShaderProgramObject, gVertexShaderObject);
        glAttachShader(gShaderProgramObject, gFragmentShaderObject);

        //Shader Attribute Binding Before Pre Linking of attached Shader
        glBindAttribLocation(gShaderProgramObject,
            VSV_ATTRIBUTE_POSITION,
            "vPosition");
        glBindAttribLocation(gShaderProgramObject,
            VSV_ATTRIBUTE_NORMAL,
            "vNormal");

        glLinkProgram(gShaderProgramObject);

        glGetProgramiv(gShaderProgramObject,
            GL_LINK_STATUS,
            &shaderProgramLinkStatus);

        if (shaderProgramLinkStatus == GL_FALSE)
        {
            glGetProgramiv(gShaderProgramObject,
                GL_INFO_LOG_LENGTH,
                &infoLogLength);
            if (infoLogLength > 0)
            {
                szBuffer = (GLchar*)malloc(sizeof(GLchar) * infoLogLength);

                if (szBuffer != NULL)
                {
                    glGetProgramInfoLog(gShaderProgramObject,
                        infoLogLength,
                        &written,
                        szBuffer);

                    printf( "\nERROR:Shader program Link failed log : %s", szBuffer);
                    free(szBuffer);
                    [self uninitialize];
                    return(nil);
                }
            }
        }

        //Shader Code End--------------------


        //Uniform variable Binding after post linking of attached shader
        modelMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_modelMatrix");
        viewMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_viewMatrix");
        projectionlMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_projectionMatrix");

        
        lightAmbientUniform = glGetUniformLocation(gShaderProgramObject, "u_lightAmbient");
        lightDiffuseUniform = glGetUniformLocation(gShaderProgramObject, "u_lightDiffuse");
        lightSpecularUniform = glGetUniformLocation(gShaderProgramObject, "u_lightSpecular");
        lightPositionUniform = glGetUniformLocation(gShaderProgramObject, "u_lightPosition");
        
        materialAmbientUniform = glGetUniformLocation(gShaderProgramObject, "u_materialAmbient");
        materialDiffuseUniform = glGetUniformLocation(gShaderProgramObject, "u_materialDiffuse");
        materialSpecularUniform = glGetUniformLocation(gShaderProgramObject, "u_materialSpecular");
        materialShininessUniform = glGetUniformLocation(gShaderProgramObject, "u_materialShininess");

        LKeyPressedUniform = glGetUniformLocation(gShaderProgramObject, "u_lKeyPressed");

        //VAO and its respective VBO preparation

            Sphere *sphere=new Sphere();
            sphere->sphereInitialize();
            
            sphere->getSphereVertexData(spherePosition, sphereNormals, sphereTexcoord, sphereElement);
            gNumSphereVertices = sphere->getNumberOfSphereVertices();
            gNumSphereElements = sphere->getNumberOfSphereElements();


            glGenVertexArrays(1,&vaoSphere);
            glBindVertexArray(vaoSphere);

                glGenBuffers(1,&vboSphere_position);
                glBindBuffer(GL_ARRAY_BUFFER, vboSphere_position);

                    glBufferData(GL_ARRAY_BUFFER,
                                sizeof(spherePosition),
                                spherePosition,
                                GL_STATIC_DRAW);

                    glVertexAttribPointer(VSV_ATTRIBUTE_POSITION,
                                        3,
                                        GL_FLOAT,
                                        GL_FALSE,
                                        0,
                                        NULL);

                    glEnableVertexAttribArray(VSV_ATTRIBUTE_POSITION);
                
                glBindBuffer(GL_ARRAY_BUFFER,0);

                glGenBuffers(1, &vboSphere_normal);
                glBindBuffer(GL_ARRAY_BUFFER, vboSphere_normal);

                glBufferData(GL_ARRAY_BUFFER,
                    sizeof(sphereNormals),
                    sphereNormals,
                    GL_STATIC_DRAW);

                glVertexAttribPointer(VSV_ATTRIBUTE_NORMAL,
                    3,
                    GL_FLOAT,
                    GL_FALSE,
                    0,
                    NULL);

                glEnableVertexAttribArray(VSV_ATTRIBUTE_NORMAL);

                glBindBuffer(GL_ARRAY_BUFFER, 0);

                glGenBuffers(1,
                    &vboSphere_elements);

                glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,
                    vboSphere_elements);

                    glBufferData(GL_ELEMENT_ARRAY_BUFFER,
                        sizeof(sphereElement),
                        sphereElement,
                        GL_STATIC_DRAW);

                glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,
                    0);

            glBindVertexArray(0);

        glClearColor(0.5,0.5,0.5f,0.0f);

        glClearDepthf(1.0f);
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);

        perspectiveProjectionMatrix=mat4::identity();
                
        //***********shader initialize end***************************************

        //register events/Gestures
        //1-register function
        //2-set Number Of Taps Required
        //3-set Number Of Touches Required
        //4-set Delegate
        //5-register/add Gesture Recognizer/event
        
        //Single tap event
        UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onSingleTap:)];
        [singleTapGestureRecognizer setNumberOfTapsRequired:1];
        [singleTapGestureRecognizer setNumberOfTouchesRequired:1];
        [singleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:singleTapGestureRecognizer];
        
        //double tap event
        UITapGestureRecognizer *doubleTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onDoubleTap:)];
        [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
        [doubleTapGestureRecognizer setNumberOfTouchesRequired:1];
        [doubleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:doubleTapGestureRecognizer];
        
        //this will block double tap to recognize as two single tap
        [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
        
        //Swipe event
        UISwipeGestureRecognizer *swipeGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(onSwipe:)];
        [swipeGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:swipeGestureRecognizer];
        
        //Long press
        UILongPressGestureRecognizer *longPressGestureRecognizer=[[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
        [longPressGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:longPressGestureRecognizer];
        
    }

    return  self;
}

//this method is complursory to override
+(Class)layerClass
{
    //code
    return ([CAEAGLLayer class]);
}

/*
 Only override drawRect: if you perform custom drawing.
 An empty implementation adversely affects performance during animation.
//drawRect is only called for immediate mode
 - (void)drawRect:(CGRect)rect {
     // code
}
 */

//will override
//its resize()
-(void)layoutSubviews
{
    //code
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);

    //bind color Render buffer
        glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    
        //Provide storage to color buffer using Apple/driver/native API from resized drawable layer
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer *)[self layer]];
    
        //set attachment of render buffer ie for color buffer attachment is set to 0 th location of color attachment
        
        //get backing width and height of render buffer
        //getting width and height from display layer(ie eaglLayer)
        //because renderbuffer storage was provided by display layer
        GLint width;
        GLint height;
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
    
    //bind depth Render buffer
        glGenRenderbuffers(1, &depthRenderbuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
        
        //Provide storage to depth buffer using opengl API with resized drawable layer width and height
        //IOS default 16 bit support for depth render buffer
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
        
        //set attachment of render buffer ie for depth buffer attachment of depth attachment
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
        
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        {
            printf("\nGL_FRAMEBUFFER is not COMPLETE");
           
        }
    
    if(height < 0)
    {
        height = 1;
    }
        
    glViewport(0, 0, (GLsizei)width, (GLsizei)height);
    
    //************resize code for openGL start******************
    
    orignal_width = (GLsizei)width;
    orignal_height = (GLsizei)height;
    
    perspectiveProjectionMatrix=vmath::perspective(45.0f,
                                                    (GLfloat)width/(GLfloat)height,
                                                    0.1f,
                                                    100.0f);
    
    //************resize code for openGL end********************
    
    [self drawView:nil];
    
}

-(void)drawView:(id)sender
{
    //code
    [EAGLContext setCurrentContext:eaglContext];
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
    
    //************shader Draw start******************************
    

    GLfloat radius = 3.0f;
        //code
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


    glUseProgram(gShaderProgramObject);
        mat4 translateMatrix;
        mat4 rotateMatrix;

        mat4 modelMatrix;
        mat4 viewMatrix = mat4::identity();

        GLfloat translate[3] = { 0.0f,0.0f,-1.5f };
        int k = 0;

        GLsizei unit_width = orignal_width / 6;
        GLsizei unit_height = orignal_height / 4;

        for (int i = 0; i < 4; i++)
        {

            for (int j = 0; j < 6; j++)
            {

                glViewport(j * unit_width, i * unit_height, unit_width, orignal_height / 6);
                //glViewport(0, 0, orignal_width, orignal_height);

                translateMatrix = vmath::translate(translate[0], translate[1], translate[2]);
                modelMatrix = translateMatrix;

                glUniformMatrix4fv(modelMatrixUniform,
                    1,
                    GL_FALSE,
                    modelMatrix);

                glUniformMatrix4fv(viewMatrixUniform,
                    1,
                    GL_FALSE,
                    viewMatrix);

                glUniformMatrix4fv(projectionlMatrixUniform,
                    1,
                    GL_FALSE,
                    perspectiveProjectionMatrix);

                if (bLights == true)
                {
                    glUniform1i(LKeyPressedUniform, 1);

                    glUniform3fv(lightAmbientUniform, 1, LightAmbient);
                    glUniform3fv(lightDiffuseUniform, 1, LightDefuse);
                    glUniform3fv(lightSpecularUniform, 1, LightSpecular);

                    if (RotationType == 1) {

                        LightPosition[0] = 0.0f;
                        LightPosition[1] = radius * sin(Angle);
                        LightPosition[2] = radius * cos(Angle);
                        LightPosition[3] = 1.0f;

                    }
                    else if (RotationType == 2) {

                        LightPosition[0] = radius * sin(Angle);
                        LightPosition[1] = 0.0f;
                        LightPosition[2] = radius * cos(Angle);
                        LightPosition[3] = 1.0f;

                    }
                    else if (RotationType == 3) {

                        LightPosition[0] = radius * sin(Angle);
                        LightPosition[1] = radius * cos(Angle);
                        LightPosition[2] = 0.0f;
                        LightPosition[3] = 1.0f;

                    }
                    else if (RotationType == 0)
                    {
                        LightPosition[0] = 3.0f;
                        LightPosition[1] = 3.0f;
                        LightPosition[2] = 3.0f;
                        LightPosition[3] = 1.0f;
                    }

                    glUniform4fv(lightPositionUniform, 1, LightPosition);//posiitional light


                    glUniform3fv(materialAmbientUniform, 1, Materials[k].materialAmbient);
                    glUniform3fv(materialDiffuseUniform, 1, Materials[k].materilaDefuse);
                    glUniform3fv(materialSpecularUniform, 1, Materials[k].materialSpecular);

                    glUniform1f(materialShininessUniform, Materials[k].materialShininess);
                }
                else
                {
                    glUniform1i(LKeyPressedUniform, 0);
                }

                glBindVertexArray(vaoSphere);
                    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboSphere_elements);
                    glDrawElements(GL_TRIANGLES, gNumSphereElements, GL_UNSIGNED_SHORT, 0);
                    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
                glBindVertexArray(0);

                k++;
            }

        }
    
    glUseProgram(0);

    Angle += 0.05f;

    //************shader Draw end******************************
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];//swap buffer
    
    
}
//display link - is timer object which matches refresh rate of screen with drawable layer to avoid screen/image terring
-(void)startAnimation
{
    //code
    if(isAnimating == NO)
    {
        //Set dynamically displayLink based on underlying GL technology and set/register callback function for drawView()
        displayLink = [NSClassFromString(@"CADisplayLink") displayLinkWithTarget:self selector:@selector(drawView:)];
        
        //set FPS
        [displayLink setPreferredFramesPerSecond:animationFrameInterval];
        
        //run loop
        //add displaylink to runloop and set display mode
        [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        
        isAnimating = YES;
    }
}

-(void)stopAnimation
{
    //code
    if(isAnimating == YES)
    {
        [displayLink invalidate];
        displayLink = nil;
        isAnimating = NO;
    }
    
}

-(void)onSingleTap:(UITapGestureRecognizer *)gr
{
    //code
    RotationType +=1;
    if (RotationType >3)
    {
        RotationType =0;
    }

}

-(void)onDoubleTap:(UITapGestureRecognizer *)gr
{
    //code
    
    bLights = !bLights;

}

-(void)onSwipe:(UISwipeGestureRecognizer *)gr
{
    //code
    //exit code
    [self uninitialize];
    [self release];
    exit(0);

}


-(void)onLongPress:(UILongPressGestureRecognizer *)gr
{
    //code


}

-(void)uninitialize
{
    if(vaoSphere)
   {
       glDeleteVertexArrays(1,
                           &vaoSphere);
   }
   vaoSphere = 0;

   if(vboSphere_position)
   {
       glDeleteBuffers(1,
                   &vboSphere_position);
   }
   vboSphere_position = 0;
   
   if (vboSphere_normal)
   {
       glDeleteBuffers(1,
           &vboSphere_normal);
   }
   vboSphere_normal = 0;

   if (vboSphere_elements)
   {
       glDeleteBuffers(1,
           &vboSphere_elements);
   }
   vboSphere_elements = 0;

   if(gShaderProgramObject)
   {
       glUseProgram(gShaderProgramObject);

       GLsizei shaderCount;
       GLsizei tempShaderCount;
       GLuint *pShader=NULL;

       glGetProgramiv(gShaderProgramObject,GL_ATTACHED_SHADERS,&shaderCount);

       pShader=(GLuint *)malloc(sizeof(GLuint)*shaderCount);
       if(pShader==NULL)
       {
           printf("\nFailed to allocate memory for pShader");
       }else{
           glGetAttachedShaders(gShaderProgramObject,shaderCount,&tempShaderCount,pShader);
           
           printf("\nShader program has actual attached shader count is:%d",tempShaderCount);
           
           for(GLsizei i=0;i<shaderCount;i++)
           {
               glDetachShader(gShaderProgramObject,pShader[i]);
               glDeleteShader(pShader[i]);
               pShader[i]=0;
           }
           free(pShader);
           
           glDeleteProgram(gShaderProgramObject);
           gShaderProgramObject =0;
       }
       glUseProgram(0);
   }

    
    //code
    if(depthRenderbuffer)
    {
        glDeleteRenderbuffers(1, &depthRenderbuffer);
    }
    depthRenderbuffer=0;
    
    if(colorRenderbuffer)
    {
        glDeleteRenderbuffers(1, &colorRenderbuffer);
    }
    colorRenderbuffer=0;
    
    if(defaultFramebuffer)
    {
        glDeleteFramebuffers(1, &defaultFramebuffer);
    }
    defaultFramebuffer=0;
    
    if(eaglContext)
    {
        if([EAGLContext currentContext] == eaglContext)
        {
            [EAGLContext setCurrentContext:nil];
        }
        [eaglContext release];
    }
    eaglContext =nil;

}

///like destructor
-(void)dealloc
{
    ///code
    [self uninitialize];
    [super dealloc];
}


@end
