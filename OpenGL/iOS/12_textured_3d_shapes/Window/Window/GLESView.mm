//
//  MyView.m
//  Window
//
//  Created by user205082 on 11/6/21.
//
#import<OpenGLES/ES3/gl.h>
#import<OpenGLES/ES3/glext.h>

#import "GLESView.h"
#import"vmath.h"
using namespace vmath;

enum{
    VSV_ATTRIBUTE_POSITION=0,
    VSV_ATTRIBUTE_COLOR,
    VSV_ATTRIBUTE_TEXCOORD,
    VSV_ATTRIBUTE_NORMAL,
};

@implementation GLESView
{
    @private
    //Embedded Apple GL
    EAGLContext *eaglContext;
    
    GLuint defaultFramebuffer;
    GLuint colorRenderbuffer;
    GLuint depthRenderbuffer;
    
    //core animation
    //CADisplayLink --- underlying GL technology might change in future,so we are not directly going to use CADisplayLink pointer
    //thats why `id` is used, it will provide underlying GL driver dynamically
    id displayLink;
    NSInteger animationFrameInterval;
    bool isAnimating;
    
    GLuint gVertexShaderObject;
    GLuint gFragmentShaderObject;
    GLuint gShaderProgramObject;

    GLuint vao_pyramid;
    GLuint vao_cube;

    GLuint vbo_pyramidTexture;
    GLuint vbo_pyramidPosition;

    GLuint vbo_cubePosition;
    GLuint vbo_cubeTexture;

    GLuint mvpMatrixUniform;
    GLuint textureSamplerUniform;

    mat4 perspectiveProjectionMatrix;

    GLuint Kundali_Texture,Stone_Texture;

    GLfloat pyramidRotate;
    GLfloat cubeRotate;
    
}

-(id)initWithFrame:(CGRect)frame
{
    // code
    self = [super initWithFrame:frame];
    if (self)
    {
        //getting render mode display layer
        CAEAGLLayer *eaglLayer =(CAEAGLLayer *)[super layer];
        
        //set Opaque ie make display layer transparent
        [eaglLayer setOpaque:YES];
        
        //set Drawable Properties,define color format:RGBA8 and allow to store window in backing:YES
        [eaglLayer setDrawableProperties:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:NO],kEAGLDrawablePropertyRetainedBacking,
                                          kEAGLColorFormatRGBA8,kEAGLDrawablePropertyColorFormat,nil]];
        
        //create context of OpenFLES 3.0
        eaglContext = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES3];
        if(eaglContext == nil)
        {
            printf("\nOpenGLES Context creation failed");
            return (nil);
        }
    
        //set current context
        [EAGLContext setCurrentContext:eaglContext];
        
        //Create our framebuffer
        glGenFramebuffers(1, &defaultFramebuffer);
        glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
        //Create color Render buffer
            glGenRenderbuffers(1, &colorRenderbuffer);
            glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
        
            //Provide storage to color buffer using Apple/driver/native API
            [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
        
            //set attachment of render buffer ie for color buffer attachment is set to 0 th location of color attachment
            glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderbuffer);
            
            //get backing width and height of render buffer
            //getting width and height from display layer(ie eaglLayer)
            //because renderbuffer storage was provided by display layer
            GLint backingWidth;
            GLint backingHeight;
            glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
            glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
        
        //Create depth Render buffer
            glGenRenderbuffers(1, &depthRenderbuffer);
            glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
            
            //Provide storage to depth buffer using opengl API with width and height
            //IOS default 16 bit support for depth render buffer
            glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
            
            //set attachment of render buffer ie for depth buffer attachment of depth attachment
            glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
            
            if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
            {
                printf("\nGL_FRAMEBUFFER is not COMPLETE");
                [self uninitialize];
                return (nil);
            }
    
        animationFrameInterval = 60;//60 FPS //actually its default to 60 from IOS 8.2
        isAnimating = NO;
        
        printf("\nOpenGL Vender:%s",glGetString(GL_VENDOR));
        printf("\nOpenGL Render:%s",glGetString(GL_RENDERER));
        printf("\nOpenGL Version:%s",glGetString(GL_VERSION));
        printf("\nOpenGL GLSL Version:%s",glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        //***********shader initialize start***************************************
        
        
        GLint infoLogLength;
        GLint shaderCompileStatus;
        GLint shaderProgramLinkStatus;
        GLchar *szBuffer;
        GLsizei written;
        
        //Shader Code Start--------------------
            gVertexShaderObject=glCreateShader(GL_VERTEX_SHADER);

            const GLchar *vertexShaderSourceCode=
                "#version 300 es" \
                "\n" \
                "in vec4 vPosition;"\
                "in vec2 vTexCoord;"\
                "out vec2 out_TexCoord;"\
                "uniform mat4 u_mvpMatrix;"\
                "void main(void)" \
                "{" \
                    "out_TexCoord=vTexCoord;"\
                    "gl_Position=u_mvpMatrix * vPosition;"\
                "}";

            glShaderSource(gVertexShaderObject,
                            1,
                            (const GLchar **)&vertexShaderSourceCode,
                            NULL);

            glCompileShader(gVertexShaderObject);

            glGetShaderiv(gVertexShaderObject,
                            GL_COMPILE_STATUS,
                            &shaderCompileStatus);

                            
            if(shaderCompileStatus==GL_FALSE)
            {
                glGetShaderiv(gVertexShaderObject,
                                GL_INFO_LOG_LENGTH,
                                &infoLogLength);
                if(infoLogLength>0)
                {
                    szBuffer=(GLchar*)malloc(sizeof(GLchar) * infoLogLength);

                    if(szBuffer!=NULL)
                    {
                        glGetShaderInfoLog(gVertexShaderObject,
                                            infoLogLength,
                                            &written,
                                            szBuffer);
                        
                        printf("\nERROR:Vertex shader compilation failed log: %s",szBuffer);
                        free(szBuffer);
                        [self uninitialize];
                        return(nil);
                    }
                }
            }

            infoLogLength=0;
            shaderCompileStatus=0;
            shaderProgramLinkStatus=0;
            szBuffer=NULL;
            written=0;

            gFragmentShaderObject=glCreateShader(GL_FRAGMENT_SHADER);

            const GLchar *fragmentShaderSourceCode=
                "#version 300 es" \
                "\n" \
        "precision highp float;" \
        "precision mediump int;" \
                "in vec2 out_TexCoord;"\
                "out vec4 fragColor;"\
                "uniform sampler2D u_texture_sampler;"\
                "void main(void)" \
                "{"\
                    "fragColor=texture(u_texture_sampler,out_TexCoord);"\
                "}";

            glShaderSource(gFragmentShaderObject,
                            1,
                            (const GLchar**)&fragmentShaderSourceCode,
                            NULL);

            glCompileShader(gFragmentShaderObject);

            glGetShaderiv(gFragmentShaderObject,
                        GL_COMPILE_STATUS,
                        &shaderCompileStatus);

            
            if(shaderCompileStatus==GL_FALSE)
            {
                glGetShaderiv(gVertexShaderObject,
                                GL_INFO_LOG_LENGTH,
                                &infoLogLength);

                if(infoLogLength>0)
                {
                    szBuffer=(GLchar*)malloc(sizeof(GLchar) * infoLogLength);

                    if(szBuffer!=NULL)
                    {
                        glGetShaderInfoLog(gFragmentShaderObject,
                                            infoLogLength,
                                            &written,
                                            szBuffer);
                        
                        printf("\nERROR:Fragment shader compilation failed log: %s",szBuffer);
                        free(szBuffer);
                        [self uninitialize];
                        return(nil);

                    }
                }
            }
            
            infoLogLength=0;
            shaderCompileStatus=0;
            shaderProgramLinkStatus=0;
            szBuffer=NULL;
            written=0;

            gShaderProgramObject=glCreateProgram();

            glAttachShader(gShaderProgramObject,gVertexShaderObject);
            glAttachShader(gShaderProgramObject,gFragmentShaderObject);
            //Shader Attribute Binding Before Pre Linking of attached Shader
                glBindAttribLocation(gShaderProgramObject,VSV_ATTRIBUTE_POSITION,"vPosition");
                glBindAttribLocation(gShaderProgramObject,VSV_ATTRIBUTE_TEXCOORD,"vTexCoord");

            glLinkProgram(gShaderProgramObject);

            glGetProgramiv(gShaderProgramObject,
                            GL_LINK_STATUS,
                            &shaderProgramLinkStatus);
            if(shaderProgramLinkStatus==GL_FALSE)
            {
                glGetProgramiv(gShaderProgramObject,
                            GL_INFO_LOG_LENGTH,
                            &infoLogLength);
                if(infoLogLength>0)
                {
                    szBuffer=(GLchar*)malloc(sizeof(GLchar)*infoLogLength);
                    if(szBuffer!=NULL)
                    {
                        glGetProgramInfoLog(gShaderProgramObject,
                                            infoLogLength,
                                            &written,
                                            szBuffer);

                        printf("\nERROR:Shader Program Link failed log: %s",szBuffer);
                        free(szBuffer);
                        [self uninitialize];
                        return(nil);                    }
                }
            }
            //Uniform variable Binding after post linking of attached shader
                mvpMatrixUniform=glGetUniformLocation(gShaderProgramObject,
                                                    "u_mvpMatrix");

                textureSamplerUniform=glGetUniformLocation(gShaderProgramObject,
                                                    "u_texture_sampler");

        //Shader Code End--------------------

        //VAO and its respective VBO preparation
            const GLfloat pyramidVertices[]=
            {
                0.0f, 1.0f, 0.0f,
                -1.0f, -1.0f, 1.0f,
                1.0f, -1.0f, 1.0f,

                0.0f, 1.0f, 0.0f,
                1.0f, -1.0f, 1.0f,
                1.0f, -1.0f, -1.0f,

                0.0f, 1.0f, 0.0f,
                1.0f, -1.0f, -1.0f,
                -1.0f, -1.0f, -1.0f,

                0.0f, 1.0f, 0.0f,
                -1.0f, -1.0f, -1.0f,
                -1.0f, -1.0f, 1.0f
            };

            const GLfloat pyramidTexCoord[]=
            {
                0.5f,1.0f,
                0.0f,0.0f,
                1.0f,0.0f,

                0.5f,1.0f,
                1.0f,0.0f,
                0.0f,0.0f,

                0.5f,1.0f,
                1.0f,0.0f,
                0.0f,0.0f,

                0.5f,1.0f,
                0.0f,0.0f,
                1.0f,0.0f
            };

            const GLfloat cubeVertices[]=
            {
                1.0f, 1.0f, 1.0f,
                -1.0f, 1.0f, 1.0f,
                -1.0f, -1.0f, 1.0f,
                1.0f, -1.0f, 1.0f,

                1.0f, 1.0f, -1.0f,
                1.0f, 1.0f, 1.0f,
                1.0f, -1.0f, 1.0f,
                1.0f, -1.0f, -1.0f,

                -1.0f, 1.0f, -1.0f,
                1.0f, 1.0f, -1.0f,
                1.0f, -1.0f, -1.0f,
                -1.0f, -1.0f, -1.0f,

                -1.0f, 1.0f, 1.0f,
                -1.0f, 1.0f, -1.0f,
                -1.0f, -1.0f, -1.0f,
                -1.0f, -1.0f, 1.0f,

                1.0f, 1.0f, -1.0f,
                -1.0f, 1.0f, -1.0f,
                -1.0f, 1.0f, 1.0f,
                1.0f, 1.0f, 1.0f,

                1.0f, -1.0f, -1.0f,
                -1.0f, -1.0f, -1.0f,
                -1.0f, -1.0f, 1.0f,
                1.0f, -1.0f, 1.0f
            };

            const GLfloat cubeTexCoord[]=
            {
                0.0f,0.0f,
                1.0f,0.0f,
                1.0f,1.0f,
                0.0f,1.0f,

                1.0f,0.0f,
                1.0f,1.0f,
                0.0f,1.0f,
                0.0f,0.0f,

                1.0f,0.0f,
                1.0f,1.0f,
                0.0f,1.0f,
                0.0f,0.0f,

                0.0f,0.0f,
                1.0f,0.0f,
                1.0f,1.0f,
                0.0f,1.0f,

                0.0f,1.0f,
                0.0f,0.0f,
                1.0f,0.0f,
                1.0f,1.0f,

                1.0f,1.0f,
                0.0f,1.0f,
                0.0f,0.0f,
                1.0f,0.0f

            };

            glGenVertexArrays(1,&vao_pyramid);
            glBindVertexArray(vao_pyramid);

                glGenBuffers(1,&vbo_pyramidPosition);
                glBindBuffer(GL_ARRAY_BUFFER,vbo_pyramidPosition);

                    glBufferData(GL_ARRAY_BUFFER,
                                sizeof(pyramidVertices),
                                pyramidVertices,
                                GL_STATIC_DRAW);

                    glVertexAttribPointer(VSV_ATTRIBUTE_POSITION,
                                        3,
                                        GL_FLOAT,
                                        GL_FALSE,
                                        0,
                                        NULL);

                    glEnableVertexAttribArray(VSV_ATTRIBUTE_POSITION);
                
                glBindBuffer(GL_ARRAY_BUFFER,0);
                
                glGenBuffers(1,&vbo_pyramidTexture);
                glBindBuffer(GL_ARRAY_BUFFER,vbo_pyramidTexture);
                    
                    glBufferData(GL_ARRAY_BUFFER,
                                sizeof(pyramidTexCoord),
                                pyramidTexCoord,
                                GL_STATIC_DRAW);
                    
                    glVertexAttribPointer(VSV_ATTRIBUTE_TEXCOORD,
                                        2,
                                        GL_FLOAT,
                                        GL_FALSE,
                                        0,
                                        NULL);

                    glEnableVertexAttribArray(VSV_ATTRIBUTE_TEXCOORD);
                
                glBindBuffer(GL_ARRAY_BUFFER,0);

            glBindVertexArray(0);

            glGenVertexArrays(1,&vao_cube);
            glBindVertexArray(vao_cube);
                glGenBuffers(1,&vbo_cubePosition);
                glBindBuffer(GL_ARRAY_BUFFER,vbo_cubePosition);

                    glBufferData(GL_ARRAY_BUFFER,
                                sizeof(cubeVertices),
                                cubeVertices,
                                GL_STATIC_DRAW);

                    glVertexAttribPointer(VSV_ATTRIBUTE_POSITION,
                                        3,
                                        GL_FLOAT,
                                        GL_FALSE,
                                        0,
                                        NULL);

                    glEnableVertexAttribArray(VSV_ATTRIBUTE_POSITION);
                glBindBuffer(GL_ARRAY_BUFFER,0);

                glGenBuffers(1,&vbo_cubeTexture);
                glBindBuffer(GL_ARRAY_BUFFER,vbo_cubeTexture);
                    glBufferData(GL_ARRAY_BUFFER,
                                sizeof(cubeTexCoord),
                                cubeTexCoord,
                                GL_STATIC_DRAW);
                    glVertexAttribPointer(VSV_ATTRIBUTE_TEXCOORD,
                                            2,
                                            GL_FLOAT,
                                            GL_FALSE,
                                            0,
                                            NULL);
                    glEnableVertexAttribArray(VSV_ATTRIBUTE_TEXCOORD);
                glBindBuffer(GL_ARRAY_BUFFER,0);
            glBindVertexArray(0);


        glClearColor(0.0,0.0,0.0f,0.0f);
        glClearDepthf(1.0f);
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);

        //---loading texture
        glEnable(GL_TEXTURE_2D);

        Stone_Texture=[self loadTextureFromBMPFile:@"stone" :@"bmp"];
        if (Stone_Texture==0)
        {
            printf("\nERROR:Stone_Texture failed to load:");
            free(szBuffer);
            [self uninitialize];
            return(nil);
        }
        Kundali_Texture=[self loadTextureFromBMPFile:@"kundali" :@"bmp"];
        if (Kundali_Texture==0)
        {
            printf("\nERROR:Kundali_Texture failed to load:");
            free(szBuffer);
            [self uninitialize];
            return(nil);
        }
     
        perspectiveProjectionMatrix=mat4::identity();
        //***********shader initialize end***************************************

        //register events/Gestures
        //1-register function
        //2-set Number Of Taps Required
        //3-set Number Of Touches Required
        //4-set Delegate
        //5-register/add Gesture Recognizer/event
        
        //Single tap event
        UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onSingleTap:)];
        [singleTapGestureRecognizer setNumberOfTapsRequired:1];
        [singleTapGestureRecognizer setNumberOfTouchesRequired:1];
        [singleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:singleTapGestureRecognizer];
        
        //double tap event
        UITapGestureRecognizer *doubleTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onDoubleTap:)];
        [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
        [doubleTapGestureRecognizer setNumberOfTouchesRequired:1];
        [doubleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:doubleTapGestureRecognizer];
        
        //this will block double tap to recognize as two single tap
        [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
        
        //Swipe event
        UISwipeGestureRecognizer *swipeGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(onSwipe:)];
        [swipeGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:swipeGestureRecognizer];
        
        //Long press
        UILongPressGestureRecognizer *longPressGestureRecognizer=[[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
        [longPressGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:longPressGestureRecognizer];
        
    }

    return  self;
}


-(GLuint)loadTextureFromBMPFile:(NSString *)imageFileName :(NSString *)extension
{
    ///code
    NSString *imageFileNameWithPath = [[NSBundle mainBundle]pathForResource:imageFileName ofType:extension];

    ///get UIImage representation from our image file.
    UIImage *bmpImage=[[UIImage alloc] initWithContentsOfFile:imageFileNameWithPath];
    if(!bmpImage)
    {
        NSLog(@"\nERROR:Failed to load image: %@.%@",imageFileName,extension );
        return 0;
    }

    CGImageRef cgImage = [bmpImage CGImage];
    
    ///get width and height of CGImage
    int imageWidth =(int)CGImageGetWidth(cgImage);
    int imageHeight =(int)CGImageGetHeight(cgImage);

    ///get core foundation data representation of our image
    CFDataRef imageData = CGDataProviderCopyData(CGImageGetDataProvider(cgImage)) ;

    ///convert CFDataRef formated data into (void*) ie generic data
    ///(Note will get pointer of imagedata's buffer)

    void* pixel =(void*) CFDataGetBytePtr(imageData);

    GLuint texture;
    glPixelStorei( GL_UNPACK_ALIGNMENT,
                    1
                );

    glGenTextures( 1,
                &texture
                );

    glBindTexture( GL_TEXTURE_2D,
                    texture
                );

    glTexParameteri( GL_TEXTURE_2D,
                    GL_TEXTURE_MAG_FILTER,
                    GL_LINEAR
                    );
    glTexParameteri( GL_TEXTURE_2D,
                    GL_TEXTURE_MIN_FILTER,
                    GL_LINEAR_MIPMAP_LINEAR
                   );

    glTexImage2D(GL_TEXTURE_2D,
                0,
                GL_RGBA,
                imageWidth,
                imageHeight,
                0,
                GL_RGBA,
                GL_UNSIGNED_BYTE,
                pixel);

    glGenerateMipmap(GL_TEXTURE_2D);

    //freeing the buffer
    CFRelease(imageData);

    return texture;

}

//this method is complursory to override
+(Class)layerClass
{
    //code
    return ([CAEAGLLayer class]);
}

/*
 Only override drawRect: if you perform custom drawing.
 An empty implementation adversely affects performance during animation.
//drawRect is only called for immediate mode
 - (void)drawRect:(CGRect)rect {
     // code
}
 */

//will override
//its resize()
-(void)layoutSubviews
{
    //code
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);

    //bind color Render buffer
        glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    
        //Provide storage to color buffer using Apple/driver/native API from resized drawable layer
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer *)[self layer]];
    
        //set attachment of render buffer ie for color buffer attachment is set to 0 th location of color attachment
        
        //get backing width and height of render buffer
        //getting width and height from display layer(ie eaglLayer)
        //because renderbuffer storage was provided by display layer
        GLint width;
        GLint height;
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
    
    //bind depth Render buffer
        glGenRenderbuffers(1, &depthRenderbuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
        
        //Provide storage to depth buffer using opengl API with resized drawable layer width and height
        //IOS default 16 bit support for depth render buffer
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
        
        //set attachment of render buffer ie for depth buffer attachment of depth attachment
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
        
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        {
            printf("\nGL_FRAMEBUFFER is not COMPLETE");
           
        }
    
    if(height < 0)
    {
        height = 1;
    }
        
    glViewport(0, 0, (GLsizei)width, (GLsizei)height);
    
    //************resize code for openGL start******************
    perspectiveProjectionMatrix=vmath::perspective(45.0f,
                                                        (GLfloat)width/(GLfloat)height,
                                                        0.1f,
                                                        100.0f);

    //************resize code for openGL end********************
    
    [self drawView:nil];
    
}

-(void)drawView:(id)sender
{
    //code
    [EAGLContext setCurrentContext:eaglContext];
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
    
    //************shader Draw start******************************
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
       glUseProgram(gShaderProgramObject);
           mat4 modelViewMatrix;
           mat4 modelViewProjectionMatrix;
           mat4 translateMatrix;
           mat4 rotateMatrix;
           mat4 scaleMatrix;



           rotateMatrix=vmath::rotate(pyramidRotate,0.0f,1.0f,0.0f);
           translateMatrix=vmath::translate(-1.5f,0.0f,-6.0f);
           
           modelViewMatrix=translateMatrix * rotateMatrix;

           modelViewProjectionMatrix=perspectiveProjectionMatrix * modelViewMatrix;

           glUniformMatrix4fv(mvpMatrixUniform,
                           1,
                           GL_FALSE,
                           modelViewProjectionMatrix);

           glActiveTexture(GL_TEXTURE0);
           glBindTexture( GL_TEXTURE_2D,Stone_Texture);
           glUniform1i(textureSamplerUniform,
                           0);

           glBindVertexArray(vao_pyramid);
                   glDrawArrays(GL_TRIANGLES,
                                   0,
                                   12);
           glBindVertexArray(0);

           rotateMatrix=vmath::rotate(cubeRotate,1.0f,0.0f,0.0f);
           rotateMatrix*=vmath::rotate(cubeRotate,0.0f,1.0f,0.0f);
           rotateMatrix*=vmath::rotate(cubeRotate,0.0f,0.0f,1.0f);

           translateMatrix=vmath::translate(1.5f,0.0f,-6.0f);
           scaleMatrix=vmath::scale(0.75f,0.75f,0.75f);

           modelViewMatrix=translateMatrix * scaleMatrix  * rotateMatrix  ;
           modelViewProjectionMatrix=perspectiveProjectionMatrix * modelViewMatrix;

           glUniformMatrix4fv(mvpMatrixUniform,
                           1,
                           GL_FALSE,
                           modelViewProjectionMatrix);

           glActiveTexture(GL_TEXTURE0);
           glBindTexture( GL_TEXTURE_2D,Kundali_Texture);
           glUniform1i(textureSamplerUniform,
                           0);
               
           glBindVertexArray(vao_cube);
               glDrawArrays(GL_TRIANGLE_FAN,0,4);
               glDrawArrays(GL_TRIANGLE_FAN,4,4);
               glDrawArrays(GL_TRIANGLE_FAN,8,4);
               glDrawArrays(GL_TRIANGLE_FAN,12,4);
               glDrawArrays(GL_TRIANGLE_FAN,16,4);
               glDrawArrays(GL_TRIANGLE_FAN,20,4);
           glBindVertexArray(0);

       glUseProgram(0);

       pyramidRotate+=1.0f;
       cubeRotate+=1.0f;
    //************shader Draw end******************************
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];//swap buffer
    
    
}
//display link - is timer object which matches refresh rate of screen with drawable layer to avoid screen/image terring
-(void)startAnimation
{
    //code
    if(isAnimating == NO)
    {
        //Set dynamically displayLink based on underlying GL technology and set/register callback function for drawView()
        displayLink = [NSClassFromString(@"CADisplayLink") displayLinkWithTarget:self selector:@selector(drawView:)];
        
        //set FPS
        [displayLink setPreferredFramesPerSecond:animationFrameInterval];
        
        //run loop
        //add displaylink to runloop and set display mode
        [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        
        isAnimating = YES;
    }
}

-(void)stopAnimation
{
    //code
    if(isAnimating == YES)
    {
        [displayLink invalidate];
        displayLink = nil;
        isAnimating = NO;
    }
    
}

-(void)onSingleTap:(UITapGestureRecognizer *)gr
{
    //code
}

-(void)onDoubleTap:(UITapGestureRecognizer *)gr
{
    //code

}

-(void)onSwipe:(UISwipeGestureRecognizer *)gr
{
    //code
    //exit code
    [self uninitialize];
    [self release];
    exit(0);

}


-(void)onLongPress:(UILongPressGestureRecognizer *)gr
{
    //code


}

-(void)uninitialize
{
    if(vao_cube)
    {
        glDeleteVertexArrays(1,
                            &vao_cube);
    }
    vao_cube=0;

    if(vao_pyramid)
    {
        glDeleteVertexArrays(1,
                            &vao_pyramid);
    }
    vao_pyramid=0;

    if(vbo_pyramidTexture){
        glDeleteBuffers(1,
                        &vbo_pyramidTexture);
    }
    vbo_pyramidTexture=0;

    if(vbo_pyramidPosition)
    {
        glDeleteBuffers(1,
                    &vbo_pyramidPosition);
    }
    vbo_pyramidPosition=0;

    if(vbo_cubePosition)
    {
        glDeleteBuffers(1,
                    &vbo_cubePosition);
    }
    vbo_cubePosition=0;

    if(vbo_cubeTexture)
    {
        glDeleteBuffers(1,
                    &vbo_cubeTexture);
    }
    vbo_cubeTexture=0;


    if(gShaderProgramObject)
    {
        glUseProgram(gShaderProgramObject);

        GLsizei shaderCount;
        GLsizei tempShaderCount;
        GLuint *pShader=NULL;

        glGetProgramiv(gShaderProgramObject,GL_ATTACHED_SHADERS,&shaderCount);

        pShader=(GLuint *)malloc(sizeof(GLuint)*shaderCount);
        if(pShader==NULL)
        {
            printf("\nFailed to allocate memory for pShader");
        }else{
            glGetAttachedShaders(gShaderProgramObject,shaderCount,&tempShaderCount,pShader);
            
            printf("\nShader program has actual attached shader count is:%d",tempShaderCount);
            
            for(GLsizei i=0;i<shaderCount;i++)
            {
                glDetachShader(gShaderProgramObject,pShader[i]);
                glDeleteShader(pShader[i]);
                pShader[i]=0;
            }
            free(pShader);
            
            glDeleteProgram(gShaderProgramObject);
            gShaderProgramObject=0;
            glUseProgram(0);
        }
    }

    glDeleteTextures(1,&Stone_Texture);
    glDeleteTextures(1,&Kundali_Texture);
    
    
    //code
    if(depthRenderbuffer)
    {
        glDeleteRenderbuffers(1, &depthRenderbuffer);
    }
    depthRenderbuffer=0;
    
    if(colorRenderbuffer)
    {
        glDeleteRenderbuffers(1, &colorRenderbuffer);
    }
    colorRenderbuffer=0;
    
    if(defaultFramebuffer)
    {
        glDeleteFramebuffers(1, &defaultFramebuffer);
    }
    defaultFramebuffer=0;
    
    if(eaglContext)
    {
        if([EAGLContext currentContext] == eaglContext)
        {
            [EAGLContext setCurrentContext:nil];
        }
        [eaglContext release];
    }
    eaglContext =nil;

}

///like destructor
-(void)dealloc
{
    ///code
    [self uninitialize];
    [super dealloc];
}


@end
