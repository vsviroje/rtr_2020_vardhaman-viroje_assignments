//
//  MyView.m
//  Window
//
//  Created by user205082 on 11/6/21.
//
#import<OpenGLES/ES3/gl.h>
#import<OpenGLES/ES3/glext.h>

#import "GLESView.h"

#include"Sphere.h"
#include"vmath.h"
using namespace vmath;

enum{
    VSV_ATTRIBUTE_POSITION=0,
    VSV_ATTRIBUTE_COLOR,
    VSV_ATTRIBUTE_TEXCOORD,
    VSV_ATTRIBUTE_NORMAL,
};


typedef struct Light {
    vec4 LightAmbient;
    vec4 LightDefuse;
    vec4 LightSpecular;
    vec4 LightPosition;
}LightStruct;

LightStruct light[3] = {
                        {
                            vec4(0.0f,0.0f,0.0f,1.0f),
                            vec4(1.0f,0.0f,0.0f,1.0f),
                            vec4(1.0f,0.0f,0.0f,1.0f),
                            vec4(2.0f,0.0f,0.0f,1.0f)
                        },
                        {
                            vec4(0.0f,0.0f,0.0f,1.0f),
                            vec4(0.0f,1.0f,0.0f,1.0f),
                            vec4(0.0f,1.0f,0.0f,1.0f),
                            vec4(-2.0f,0.0f,0.0f,1.0f)
                        },
                        {
                            vec4(0.0f,0.0f,0.0f,1.0f),
                            vec4(0.0f,0.0f,1.0f,1.0f),
                            vec4(0.0f,0.0f,1.0f,1.0f),
                            vec4(0.0f,0.0f,0.0f,1.0f)
                        }
                    };


GLfloat materialAmbient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat materilaDefuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat materialSpecular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat materialShininess = 128.0f;

@implementation GLESView
{
    @private
    //Embedded Apple GL
    EAGLContext *eaglContext;
    
    GLuint defaultFramebuffer;
    GLuint colorRenderbuffer;
    GLuint depthRenderbuffer;
    
    //core animation
    //CADisplayLink --- underlying GL technology might change in future,so we are not directly going to use CADisplayLink pointer
    //thats why `id` is used, it will provide underlying GL driver dynamically
    id displayLink;
    NSInteger animationFrameInterval;
    bool isAnimating;
    
    GLuint gShaderProgramObject_PF;

      GLuint modelMatrixUniform_PF;
      GLuint viewMatrixUniform_PF;
      GLuint projectionlMatrixUniform_PF;

      GLuint lightAmbientUniform_PF[3];
      GLuint lightDiffuseUniform_PF[3];
      GLuint lightSpecularUniform_PF[3];
      GLuint lightPositionUniform_PF[3];

      GLuint materialAmbientUniform_PF;
      GLuint materialDiffuseUniform_PF;
      GLuint materialSpecularUniform_PF;
      GLuint materialShininessUniform_PF;

      GLuint LKeyPressedUniform_PF;

      //-----------PV-------------------------------------------
      GLuint gShaderProgramObject_PV;

      GLuint modelMatrixUniform_PV;
      GLuint viewMatrixUniform_PV;
      GLuint projectionlMatrixUniform_PV;

      GLuint lightAmbientUniform_PV[3];
      GLuint lightDiffuseUniform_PV[3];
      GLuint lightSpecularUniform_PV[3];
      GLuint lightPositionUniform_PV[3];

      GLuint materialAmbientUniform_PV;
      GLuint materialDiffuseUniform_PV;
      GLuint materialSpecularUniform_PV;
      GLuint materialShininessUniform_PV;

      GLuint LKeyPressedUniform_PV;

      //-----------comman------------
      GLuint gTempShaderProgramObject;

      GLuint gVertexShaderObject;
      GLuint gFragmentShaderObject;

      GLuint vaoSphere;
      GLuint vboSphere_position;
      GLuint vboSphere_normal;
      GLuint vboSphere_elements;

      bool bLights;
      bool bTogglePVxOrPF;//True-Per Fragment and False-Per Vertex

      mat4 perspectiveProjectionMatrix;
      GLfloat Angle;


      float spherePosition[1146];
      float sphereNormals[1146];
      float sphereTexcoord[764];
      short sphereElement[2280];//Face Indices

      GLuint gNumSphereVertices, gNumSphereElements;
    
}

-(id)initWithFrame:(CGRect)frame
{
    // code
    self = [super initWithFrame:frame];
    if (self)
    {
        //getting render mode display layer
        CAEAGLLayer *eaglLayer =(CAEAGLLayer *)[super layer];
        
        //set Opaque ie make display layer transparent
        [eaglLayer setOpaque:YES];
        
        //set Drawable Properties,define color format:RGBA8 and allow to store window in backing:YES
        [eaglLayer setDrawableProperties:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:NO],kEAGLDrawablePropertyRetainedBacking,
                                          kEAGLColorFormatRGBA8,kEAGLDrawablePropertyColorFormat,nil]];
        
        //create context of OpenFLES 3.0
        eaglContext = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES3];
        if(eaglContext == nil)
        {
            printf("\nOpenGLES Context creation failed");
            return (nil);
        }
    
        //set current context
        [EAGLContext setCurrentContext:eaglContext];
        
        //Create our framebuffer
        glGenFramebuffers(1, &defaultFramebuffer);
        glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
        //Create color Render buffer
            glGenRenderbuffers(1, &colorRenderbuffer);
            glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
        
            //Provide storage to color buffer using Apple/driver/native API
            [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
        
            //set attachment of render buffer ie for color buffer attachment is set to 0 th location of color attachment
            glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderbuffer);
            
            //get backing width and height of render buffer
            //getting width and height from display layer(ie eaglLayer)
            //because renderbuffer storage was provided by display layer
            GLint backingWidth;
            GLint backingHeight;
            glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
            glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
        
        //Create depth Render buffer
            glGenRenderbuffers(1, &depthRenderbuffer);
            glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
            
            //Provide storage to depth buffer using opengl API with width and height
            //IOS default 16 bit support for depth render buffer
            glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
            
            //set attachment of render buffer ie for depth buffer attachment of depth attachment
            glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
            
            if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
            {
                printf("\nGL_FRAMEBUFFER is not COMPLETE");
                [self uninitialize];
                return (nil);
            }
    
        animationFrameInterval = 60;//60 FPS //actually its default to 60 from IOS 8.2
        isAnimating = NO;
        
        printf("\nOpenGL Vender:%s",glGetString(GL_VENDOR));
        printf("\nOpenGL Render:%s",glGetString(GL_RENDERER));
        printf("\nOpenGL Version:%s",glGetString(GL_VERSION));
        printf("\nOpenGL GLSL Version:%s",glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        //***********shader initialize start***************************************
        
        GLint infoLogLength;
        GLint shaderCompileStatus;
        GLint shaderProgramLinkStatus;
        GLchar *szBuffer;
        GLsizei written;


        //Per Vertex Shader Code Start--------------------
        gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

        const GLchar* vertexShaderSourceCode_PV =
            "#version 300 es\n"\
            "\n"\
        "precision highp float;" \
        "precision mediump int;" \
            "in vec4 vPosition;\n"\
            "in vec3 vNormal;\n"\

            "uniform mat4 u_modelMatrix;\n"\
            "uniform mat4 u_viewMatrix;\n"\
            "uniform mat4 u_projectionMatrix;\n"\

            "uniform vec3 u_lightAmbient[3];\n"\
            "uniform vec3 u_lightDiffuse[3];\n"\
            "uniform vec3 u_lightSpecular[3];\n"\
            "uniform vec4 u_lightPosition[3];\n"\

            "uniform vec3 u_materialAmbient;\n"\
            "uniform vec3 u_materialDiffuse;\n"\
            "uniform vec3 u_materialSpecular;\n"\
            "uniform float u_materialShininess;\n"\

            "uniform int u_lKeyPressed;\n"\

            "out vec3 out_phongADS_Light;\n"\

            "void main(void)\n"\
            "{\n"\
                
                "vec3 ambient[3];\n"\
                "vec3 diffuse[3];\n"\
                "vec3 specular[3];\n"\

                "if (u_lKeyPressed == 1)\n"\
                "{\n"\

                    "vec4 eyeCoord = u_viewMatrix * u_modelMatrix * vPosition;\n"\
                    "vec3 viewVector = normalize( -eyeCoord.xyz);\n"\
                    "vec3 transformedNormal = normalize( mat3(u_viewMatrix * u_modelMatrix) * vNormal );\n"\

                    "vec3 lightDirection[3];\n"\
                    "vec3 reflectionVector[3];\n"\

                    "for (int i = 0; i < 3; i++)\n"\
                    "{\n"\

                        "lightDirection[i] = normalize( vec3( u_lightPosition[i] - eyeCoord ) );\n"\

                        "reflectionVector[i] = reflect( -lightDirection[i], transformedNormal );\n"\

                        "ambient[i]= u_lightAmbient[i] * u_materialAmbient;\n"\

                        "diffuse[i] = u_lightDiffuse[i] * u_materialDiffuse * max( dot(lightDirection[i] , transformedNormal), 0.0);\n"\

                        "specular[i] = u_lightSpecular[i] * u_materialSpecular * pow( max( dot(reflectionVector[i] , viewVector), 0.0f) , u_materialShininess);\n"\

                    "}\n"\

                "}\n"\

                "if (u_lKeyPressed == 1)\n"\
                "{\n"\
                    "out_phongADS_Light = (ambient[0]+ambient[1]+ambient[2]) + (diffuse[0]+diffuse[1]+diffuse[2]) + (specular[0]+specular[1]+specular[2]);\n"\
                 "}\n"\
                "else\n"\
                "{\n"\

                    "out_phongADS_Light = vec3(1.0f, 1.0f, 1.0f);\n"\

                "}\n"\

                "gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix * vPosition;\n"\

            "}";

        glShaderSource(gVertexShaderObject,
            1,
            (const GLchar**)&vertexShaderSourceCode_PV,
            NULL);

        glCompileShader(gVertexShaderObject);

        glGetShaderiv(gVertexShaderObject,
            GL_COMPILE_STATUS,
            &shaderCompileStatus);

        if (shaderCompileStatus == GL_FALSE)
        {
            glGetShaderiv(gVertexShaderObject,
                GL_INFO_LOG_LENGTH,
                &infoLogLength);

            if (infoLogLength > 0)
            {
                szBuffer = (GLchar*)malloc(sizeof(GLchar) * infoLogLength);
                if (szBuffer != NULL)
                {
                    glGetShaderInfoLog(gVertexShaderObject,
                        infoLogLength,
                        &written,
                        szBuffer);
                    printf( "\nERROR:Vertex Shader Object Compilation Failed log : %s", szBuffer);
                    free(szBuffer);
                     [self uninitialize];
                    return nil;
                }
            }
        }

        infoLogLength = 0;
        shaderCompileStatus = 0;
        shaderProgramLinkStatus = 0;
        szBuffer = NULL;
        written = 0;

        gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

        const GLchar* fragmentShaderSourceCode_PV =
            "#version 300 es"\
            "\n"\
        "precision highp float;" \
        "precision mediump int;" \
            "in vec3 out_phongADS_Light;\n"
            "out vec4 fragColor;"\
            "void main(void)"\
            "{"\
                "fragColor = vec4( out_phongADS_Light, 1.0f );"\
            "}";

        glShaderSource(gFragmentShaderObject,
            1,
            (const GLchar**)&fragmentShaderSourceCode_PV,
            NULL);

        glCompileShader(gFragmentShaderObject);

        glGetShaderiv(gFragmentShaderObject,
            GL_COMPILE_STATUS,
            &shaderCompileStatus);

        if (shaderCompileStatus == GL_FALSE)
        {
            glGetShaderiv(gFragmentShaderObject,
                GL_INFO_LOG_LENGTH,
                &infoLogLength);

            if (infoLogLength > 0)
            {
                szBuffer = (GLchar*)malloc(sizeof(GLchar) * infoLogLength);
                if (szBuffer != NULL)
                {
                    glGetShaderInfoLog(gFragmentShaderObject,
                        infoLogLength,
                        &written,
                        szBuffer);
                    printf( "\nERROR:Fragment Shader Object Compilation Failed log : %s", szBuffer);
                    free(szBuffer);
                    [self uninitialize];
                    return nil;
                }
            }
        }

        infoLogLength = 0;
        shaderCompileStatus = 0;
        shaderProgramLinkStatus = 0;
        szBuffer = NULL;
        written = 0;

        gShaderProgramObject_PV = glCreateProgram();

        glAttachShader(gShaderProgramObject_PV, gVertexShaderObject);
        glAttachShader(gShaderProgramObject_PV, gFragmentShaderObject);

        //Shader Attribute Binding Before Pre Linking of attached Shader
        glBindAttribLocation(gShaderProgramObject_PV,
            VSV_ATTRIBUTE_POSITION,
            "vPosition");
        glBindAttribLocation(gShaderProgramObject_PV,
            VSV_ATTRIBUTE_NORMAL,
            "vNormal");

        glLinkProgram(gShaderProgramObject_PV);

        glGetProgramiv(gShaderProgramObject_PV,
            GL_LINK_STATUS,
            &shaderProgramLinkStatus);

        if (shaderProgramLinkStatus == GL_FALSE)
        {
            glGetProgramiv(gShaderProgramObject_PV,
                GL_INFO_LOG_LENGTH,
                &infoLogLength);
            if (infoLogLength > 0)
            {
                szBuffer = (GLchar*)malloc(sizeof(GLchar) * infoLogLength);

                if (szBuffer != NULL)
                {
                    glGetProgramInfoLog(gShaderProgramObject_PV,
                        infoLogLength,
                        &written,
                        szBuffer);

                    printf( "\nERROR:Shader program Link failed log : %s", szBuffer);
                    free(szBuffer);
                    [self uninitialize];
                   return nil;
                }
            }
        }

        //Shader Code End--------------------


        //Uniform variable Binding after post linking of attached shader
        modelMatrixUniform_PV = glGetUniformLocation(gShaderProgramObject_PV, "u_modelMatrix");
        viewMatrixUniform_PV = glGetUniformLocation(gShaderProgramObject_PV, "u_viewMatrix");
        projectionlMatrixUniform_PV = glGetUniformLocation(gShaderProgramObject_PV, "u_projectionMatrix");


        lightAmbientUniform_PV[0] = glGetUniformLocation(gShaderProgramObject_PV, "u_lightAmbient[0]");
        lightDiffuseUniform_PV[0] = glGetUniformLocation(gShaderProgramObject_PV, "u_lightDiffuse[0]");
        lightSpecularUniform_PV[0] = glGetUniformLocation(gShaderProgramObject_PV, "u_lightSpecular[0]");
        lightPositionUniform_PV[0] = glGetUniformLocation(gShaderProgramObject_PV, "u_lightPosition[0]");

        lightAmbientUniform_PV[1] = glGetUniformLocation(gShaderProgramObject_PV, "u_lightAmbient[1]");
        lightDiffuseUniform_PV[1] = glGetUniformLocation(gShaderProgramObject_PV, "u_lightDiffuse[1]");
        lightSpecularUniform_PV[1] = glGetUniformLocation(gShaderProgramObject_PV, "u_lightSpecular[1]");
        lightPositionUniform_PV[1] = glGetUniformLocation(gShaderProgramObject_PV, "u_lightPosition[1]");

        lightAmbientUniform_PV[2] = glGetUniformLocation(gShaderProgramObject_PV, "u_lightAmbient[2]");
        lightDiffuseUniform_PV[2] = glGetUniformLocation(gShaderProgramObject_PV, "u_lightDiffuse[2]");
        lightSpecularUniform_PV[2] = glGetUniformLocation(gShaderProgramObject_PV, "u_lightSpecular[2]");
        lightPositionUniform_PV[2] = glGetUniformLocation(gShaderProgramObject_PV, "u_lightPosition[2]");

        materialAmbientUniform_PV = glGetUniformLocation(gShaderProgramObject_PV, "u_materialAmbient");
        materialDiffuseUniform_PV = glGetUniformLocation(gShaderProgramObject_PV, "u_materialDiffuse");
        materialSpecularUniform_PV = glGetUniformLocation(gShaderProgramObject_PV, "u_materialSpecular");
        materialShininessUniform_PV = glGetUniformLocation(gShaderProgramObject_PV, "u_materialShininess");

        LKeyPressedUniform_PV = glGetUniformLocation(gShaderProgramObject_PV, "u_lKeyPressed");



    //############################################################################################
    //############################################################################################

        //Per Fragment Shader Code Start--------------------
        gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

        const GLchar* vertexShaderSourceCode_PF =
            "#version 300 es\n"\
            "\n"\
        "precision highp float;" \
        "precision mediump int;" \
            "in vec4 vPosition;\n"\
            "in vec3 vNormal;\n"\

            "uniform mat4 u_modelMatrix;\n"\
            "uniform mat4 u_viewMatrix;\n"\
            "uniform mat4 u_projectionMatrix;\n"\

            "uniform vec4 u_lightPosition[3];\n"\

            "uniform int u_lKeyPressed;\n"\

            "out vec3 out_transformedNormal;\n"\
            "out vec3 out_lightDirection[3];\n"\
            "out vec3 out_viewVector;\n"\

            "void main(void)\n"\

            "{\n"\
                "if (u_lKeyPressed == 1)\n"\
                "{\n"\

                    "vec4 eyeCoord = u_viewMatrix * u_modelMatrix * vPosition;\n"\

                    "out_viewVector = -eyeCoord.xyz;\n"\

                    "out_transformedNormal = mat3(u_viewMatrix * u_modelMatrix) * vNormal ;\n"\

                    "for (int i = 0; i < 3; i++)\n"\
                    "{\n"\
                        "out_lightDirection[i] =  vec3( u_lightPosition[i] - eyeCoord ) ;\n"\
                    "}\n"\

                "}\n"\
            

                "gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix * vPosition;\n"\

            "}";

        glShaderSource(gVertexShaderObject,
            1,
            (const GLchar**)&vertexShaderSourceCode_PF,
            NULL);

        glCompileShader(gVertexShaderObject);

        glGetShaderiv(gVertexShaderObject,
            GL_COMPILE_STATUS,
            &shaderCompileStatus);

        if (shaderCompileStatus == GL_FALSE)
        {
            glGetShaderiv(gVertexShaderObject,
                GL_INFO_LOG_LENGTH,
                &infoLogLength);

            if (infoLogLength > 0)
            {
                szBuffer = (GLchar*)malloc(sizeof(GLchar) * infoLogLength);
                if (szBuffer != NULL)
                {
                    glGetShaderInfoLog(gVertexShaderObject,
                        infoLogLength,
                        &written,
                        szBuffer);
                    printf( "\nERROR:Vertex Shader Object Compilation Failed log : %s", szBuffer);
                    free(szBuffer);
                    [self uninitialize];
                   return nil;
                    
                }
            }
        }

        infoLogLength = 0;
        shaderCompileStatus = 0;
        shaderProgramLinkStatus = 0;
        szBuffer = NULL;
        written = 0;

        gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

        const GLchar* fragmentShaderSourceCode_PF =
            "#version 300 es"\
            "\n"\
        "precision highp float;" \
        "precision mediump int;" \

            "uniform vec3 u_lightAmbient[3];\n"\
            "uniform vec3 u_lightDiffuse[3];\n"\
            "uniform vec3 u_lightSpecular[3];\n"\

            "uniform vec3 u_materialAmbient;\n"\
            "uniform vec3 u_materialDiffuse;\n"\
            "uniform vec3 u_materialSpecular;\n"\
            "uniform float u_materialShininess;\n"\

            "uniform int u_lKeyPressed;\n"\

            "in vec3 out_transformedNormal;\n"\
            "in vec3 out_lightDirection[3];\n"\
            "in vec3 out_viewVector;\n"\

            "vec3 phongADS_Light;\n"
            "out vec4 fragColor;"\

            "void main(void)"\
            "{"\
                "vec3 ambient[3];\n"\
                "vec3 diffuse[3];\n"\
                "vec3 specular[3];\n"\

                "if (u_lKeyPressed == 1)\n"\
                "{\n"\

                    "vec3 normalizedTransformedNormal = normalize( out_transformedNormal );\n"\

                    "vec3 normalizedLightDirection[3];\n"\

                    "for (int i = 0; i < 3; i++)\n"\
                    "{\n"\
                        " normalizedLightDirection[i] = normalize( out_lightDirection[i] );\n"\
                    "}\n"\
                
                    "vec3 normalizedViewVector = normalize( out_viewVector );\n"\

                    "vec3 reflectionVector[3];\n"\
            

                    "for (int i = 0; i < 3; i++)\n"\
                    "{\n"\
                    
                        "reflectionVector[i] = reflect( -normalizedLightDirection[i], normalizedTransformedNormal );\n"\

                        "ambient[i]= u_lightAmbient[i] * u_materialAmbient;\n"\

                        "diffuse[i] = u_lightDiffuse[i] * u_materialDiffuse * max( dot(normalizedLightDirection[i] , normalizedTransformedNormal), 0.0);\n"\

                        "specular[i] = u_lightSpecular[i] * u_materialSpecular * pow( max( dot(reflectionVector[i] , normalizedViewVector), 0.0f) , u_materialShininess);\n"\

                    "}\n"\

                "}\n"\

                 "if (u_lKeyPressed == 1)\n"\
                "{\n"\
                    "phongADS_Light = (ambient[0]+ambient[1]+ambient[2]) + (diffuse[0]+diffuse[1]+diffuse[2]) + (specular[0]+specular[1]+specular[2]);\n"\
                 "}\n"\
                "else\n"\
                "{\n"\
                    "phongADS_Light=vec3(1.0f,1.0f,1.0f);\n"\
                "}\n"\

                "fragColor = vec4( phongADS_Light, 1.0f );"\
            "}";

        glShaderSource(gFragmentShaderObject,
            1,
            (const GLchar**)&fragmentShaderSourceCode_PF,
            NULL);

        glCompileShader(gFragmentShaderObject);

        glGetShaderiv(gFragmentShaderObject,
            GL_COMPILE_STATUS,
            &shaderCompileStatus);

        if (shaderCompileStatus == GL_FALSE)
        {
            glGetShaderiv(gFragmentShaderObject,
                GL_INFO_LOG_LENGTH,
                &infoLogLength);

            if (infoLogLength > 0)
            {
                szBuffer = (GLchar*)malloc(sizeof(GLchar) * infoLogLength);
                if (szBuffer != NULL)
                {
                    glGetShaderInfoLog(gFragmentShaderObject,
                        infoLogLength,
                        &written,
                        szBuffer);
                    printf( "\nERROR:Fragment Shader Object Compilation Failed log : %s", szBuffer);
                    free(szBuffer);
                    [self uninitialize];
                   return nil;
                }
            }
        }

        infoLogLength = 0;
        shaderCompileStatus = 0;
        shaderProgramLinkStatus = 0;
        szBuffer = NULL;
        written = 0;

        gShaderProgramObject_PF = glCreateProgram();

        glAttachShader(gShaderProgramObject_PF, gVertexShaderObject);
        glAttachShader(gShaderProgramObject_PF, gFragmentShaderObject);

        //Shader Attribute Binding Before Pre Linking of attached Shader
        glBindAttribLocation(gShaderProgramObject_PF,
            VSV_ATTRIBUTE_POSITION,
            "vPosition");
        glBindAttribLocation(gShaderProgramObject_PF,
            VSV_ATTRIBUTE_NORMAL,
            "vNormal");

        glLinkProgram(gShaderProgramObject_PF);

        glGetProgramiv(gShaderProgramObject_PF,
            GL_LINK_STATUS,
            &shaderProgramLinkStatus);

        if (shaderProgramLinkStatus == GL_FALSE)
        {
            glGetProgramiv(gShaderProgramObject_PF,
                GL_INFO_LOG_LENGTH,
                &infoLogLength);
            if (infoLogLength > 0)
            {
                szBuffer = (GLchar*)malloc(sizeof(GLchar) * infoLogLength);

                if (szBuffer != NULL)
                {
                    glGetProgramInfoLog(gShaderProgramObject_PF,
                        infoLogLength,
                        &written,
                        szBuffer);

                    printf( "\nERROR:Shader program Link failed log : %s", szBuffer);
                    free(szBuffer);
                    [self uninitialize];
                   return nil;
                }
            }
        }

        //Shader Code End--------------------


        //Uniform variable Binding after post linking of attached shader
        modelMatrixUniform_PF = glGetUniformLocation(gShaderProgramObject_PF, "u_modelMatrix");
        viewMatrixUniform_PF = glGetUniformLocation(gShaderProgramObject_PF, "u_viewMatrix");
        projectionlMatrixUniform_PF = glGetUniformLocation(gShaderProgramObject_PF, "u_projectionMatrix");

        
        lightAmbientUniform_PF[0] = glGetUniformLocation(gShaderProgramObject_PF, "u_lightAmbient[0]");
        lightDiffuseUniform_PF[0] = glGetUniformLocation(gShaderProgramObject_PF, "u_lightDiffuse[0]");
        lightSpecularUniform_PF[0] = glGetUniformLocation(gShaderProgramObject_PF, "u_lightSpecular[0]");
        lightPositionUniform_PF[0] = glGetUniformLocation(gShaderProgramObject_PF, "u_lightPosition[0]");
        
        lightAmbientUniform_PF[1] = glGetUniformLocation(gShaderProgramObject_PF, "u_lightAmbient[1]");
        lightDiffuseUniform_PF[1] = glGetUniformLocation(gShaderProgramObject_PF, "u_lightDiffuse[1]");
        lightSpecularUniform_PF[1] = glGetUniformLocation(gShaderProgramObject_PF, "u_lightSpecular[1]");
        lightPositionUniform_PF[1] = glGetUniformLocation(gShaderProgramObject_PF, "u_lightPosition[1]");

        lightAmbientUniform_PF[2] = glGetUniformLocation(gShaderProgramObject_PF, "u_lightAmbient[2]");
        lightDiffuseUniform_PF[2] = glGetUniformLocation(gShaderProgramObject_PF, "u_lightDiffuse[2]");
        lightSpecularUniform_PF[2] = glGetUniformLocation(gShaderProgramObject_PF, "u_lightSpecular[2]");
        lightPositionUniform_PF[2] = glGetUniformLocation(gShaderProgramObject_PF, "u_lightPosition[2]");

        materialAmbientUniform_PF = glGetUniformLocation(gShaderProgramObject_PF, "u_materialAmbient");
        materialDiffuseUniform_PF = glGetUniformLocation(gShaderProgramObject_PF, "u_materialDiffuse");
        materialSpecularUniform_PF = glGetUniformLocation(gShaderProgramObject_PF, "u_materialSpecular");
        materialShininessUniform_PF = glGetUniformLocation(gShaderProgramObject_PF, "u_materialShininess");

        LKeyPressedUniform_PF = glGetUniformLocation(gShaderProgramObject_PF, "u_lKeyPressed");

        //VAO and its respective VBO preparation

            Sphere *sphere=new Sphere();
            sphere->sphereInitialize();
            
            sphere->getSphereVertexData(spherePosition, sphereNormals, sphereTexcoord, sphereElement);
            gNumSphereVertices = sphere->getNumberOfSphereVertices();
            gNumSphereElements = sphere->getNumberOfSphereElements();


            glGenVertexArrays(1,&vaoSphere);
            glBindVertexArray(vaoSphere);

                glGenBuffers(1,&vboSphere_position);
                glBindBuffer(GL_ARRAY_BUFFER, vboSphere_position);

                    glBufferData(GL_ARRAY_BUFFER,
                                sizeof(spherePosition),
                                spherePosition,
                                GL_STATIC_DRAW);

                    glVertexAttribPointer(VSV_ATTRIBUTE_POSITION,
                                        3,
                                        GL_FLOAT,
                                        GL_FALSE,
                                        0,
                                        NULL);

                    glEnableVertexAttribArray(VSV_ATTRIBUTE_POSITION);
                
                glBindBuffer(GL_ARRAY_BUFFER,0);

                glGenBuffers(1, &vboSphere_normal);
                glBindBuffer(GL_ARRAY_BUFFER, vboSphere_normal);

                glBufferData(GL_ARRAY_BUFFER,
                    sizeof(sphereNormals),
                    sphereNormals,
                    GL_STATIC_DRAW);

                glVertexAttribPointer(VSV_ATTRIBUTE_NORMAL,
                    3,
                    GL_FLOAT,
                    GL_FALSE,
                    0,
                    NULL);

                glEnableVertexAttribArray(VSV_ATTRIBUTE_NORMAL);

                glBindBuffer(GL_ARRAY_BUFFER, 0);

                glGenBuffers(1,
                    &vboSphere_elements);

                glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,
                    vboSphere_elements);

                    glBufferData(GL_ELEMENT_ARRAY_BUFFER,
                        sizeof(sphereElement),
                        sphereElement,
                        GL_STATIC_DRAW);

                glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,
                    0);

            glBindVertexArray(0);

        glClearColor(0.0,0.0,0.0f,0.0f);

        glClearDepthf(1.0f);
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);

        perspectiveProjectionMatrix=mat4::identity();
        //***********shader initialize end***************************************

        //register events/Gestures
        //1-register function
        //2-set Number Of Taps Required
        //3-set Number Of Touches Required
        //4-set Delegate
        //5-register/add Gesture Recognizer/event
        
        //Single tap event
        UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onSingleTap:)];
        [singleTapGestureRecognizer setNumberOfTapsRequired:1];
        [singleTapGestureRecognizer setNumberOfTouchesRequired:1];
        [singleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:singleTapGestureRecognizer];
        
        //double tap event
        UITapGestureRecognizer *doubleTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onDoubleTap:)];
        [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
        [doubleTapGestureRecognizer setNumberOfTouchesRequired:1];
        [doubleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:doubleTapGestureRecognizer];
        
        //this will block double tap to recognize as two single tap
        [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
        
        //Swipe event
        UISwipeGestureRecognizer *swipeGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(onSwipe:)];
        [swipeGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:swipeGestureRecognizer];
        
        //Long press
        UILongPressGestureRecognizer *longPressGestureRecognizer=[[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
        [longPressGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:longPressGestureRecognizer];
        
    }

    return  self;
}

//this method is complursory to override
+(Class)layerClass
{
    //code
    return ([CAEAGLLayer class]);
}

/*
 Only override drawRect: if you perform custom drawing.
 An empty implementation adversely affects performance during animation.
//drawRect is only called for immediate mode
 - (void)drawRect:(CGRect)rect {
     // code
}
 */

//will override
//its resize()
-(void)layoutSubviews
{
    //code
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);

    //bind color Render buffer
        glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    
        //Provide storage to color buffer using Apple/driver/native API from resized drawable layer
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer *)[self layer]];
    
        //set attachment of render buffer ie for color buffer attachment is set to 0 th location of color attachment
        
        //get backing width and height of render buffer
        //getting width and height from display layer(ie eaglLayer)
        //because renderbuffer storage was provided by display layer
        GLint width;
        GLint height;
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
    
    //bind depth Render buffer
        glGenRenderbuffers(1, &depthRenderbuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
        
        //Provide storage to depth buffer using opengl API with resized drawable layer width and height
        //IOS default 16 bit support for depth render buffer
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
        
        //set attachment of render buffer ie for depth buffer attachment of depth attachment
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
        
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        {
            printf("\nGL_FRAMEBUFFER is not COMPLETE");
           
        }
    
    if(height < 0)
    {
        height = 1;
    }
        
    glViewport(0, 0, (GLsizei)width, (GLsizei)height);
    
    //************resize code for openGL start******************
    perspectiveProjectionMatrix=vmath::perspective(45.0f,
                                                (GLfloat)width/(GLfloat)height,
                                                0.1f,
                                                100.0f);
    //************resize code for openGL end********************
    
    [self drawView:nil];
    
}

-(void)drawView:(id)sender
{
    //code
    [EAGLContext setCurrentContext:eaglContext];
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
    
    //************shader Draw start******************************
    
    GLfloat radius = 3.0f;
        //code
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    if (bTogglePVxOrPF)
    {
        glUseProgram(gShaderProgramObject_PF);
        mat4 translateMatrix;
        mat4 rotateMatrix;

        mat4 modelMatrix;
        mat4 viewMatrix = mat4::identity();

        translateMatrix = vmath::translate(0.0f, 0.0f, -1.5f);
        modelMatrix = translateMatrix ;

        glUniformMatrix4fv(modelMatrixUniform_PF,
            1,
            GL_FALSE,
            modelMatrix);

        glUniformMatrix4fv(viewMatrixUniform_PF,
            1,
            GL_FALSE,
            viewMatrix);

        glUniformMatrix4fv(projectionlMatrixUniform_PF,
            1,
            GL_FALSE,
            perspectiveProjectionMatrix);

        if (bLights == true)
        {
            glUniform1i(LKeyPressedUniform_PF, 1);

            for (int i = 0; i < 3; i++)
            {
                glUniform3fv(lightAmbientUniform_PF[i], 1, light[i].LightAmbient);
                glUniform3fv(lightDiffuseUniform_PF[i], 1, light[i].LightDefuse);
                glUniform3fv(lightSpecularUniform_PF[i], 1, light[i].LightSpecular);

                if (i == 0){
                    light[i].LightPosition = vec4(0.0f, radius * sin(Angle), radius * cos(Angle), 1.0f);
                }
                else if (i == 1){
                    light[i].LightPosition = vec4(radius * sin(Angle), 0.0f, radius * cos(Angle), 1.0f);
                }
                else if (i == 2){
                    light[i].LightPosition = vec4(radius * sin(Angle), radius * cos(Angle), 0.0f, 1.0f);
                }

                glUniform4fv(lightPositionUniform_PF[i], 1, light[i].LightPosition);//posiitional light
            }
            
            glUniform3fv(materialAmbientUniform_PF, 1, materialAmbient);
            glUniform3fv(materialDiffuseUniform_PF, 1, materilaDefuse);
            glUniform3fv(materialSpecularUniform_PF, 1, materialSpecular);

            glUniform1f(materialShininessUniform_PF, materialShininess);
        }
        else
        {
            glUniform1i(LKeyPressedUniform_PF, 0);
        }

    }
    else {
        glUseProgram(gShaderProgramObject_PV);
        mat4 translateMatrix;
        mat4 rotateMatrix;

        mat4 modelMatrix;
        mat4 viewMatrix = mat4::identity();

        translateMatrix = vmath::translate(0.0f, 0.0f, -1.5f);
        modelMatrix = translateMatrix;

        glUniformMatrix4fv(modelMatrixUniform_PV,
            1,
            GL_FALSE,
            modelMatrix);

        glUniformMatrix4fv(viewMatrixUniform_PV,
            1,
            GL_FALSE,
            viewMatrix);

        glUniformMatrix4fv(projectionlMatrixUniform_PV,
            1,
            GL_FALSE,
            perspectiveProjectionMatrix);

        if (bLights == true)
        {
            glUniform1i(LKeyPressedUniform_PV, 1);

            for (int i = 0; i < 3; i++)
            {
                glUniform3fv(lightAmbientUniform_PV[i], 1, light[i].LightAmbient);
                glUniform3fv(lightDiffuseUniform_PV[i], 1, light[i].LightDefuse);
                glUniform3fv(lightSpecularUniform_PV[i], 1, light[i].LightSpecular);

                if (i == 0) {
                    light[i].LightPosition = vec4(0.0f, radius * sin(Angle), radius * cos(Angle), 1.0f);
                }
                else if (i == 1) {
                    light[i].LightPosition = vec4(radius * sin(Angle), 0.0f, radius * cos(Angle), 1.0f);
                }
                else if (i == 2) {
                    light[i].LightPosition = vec4(radius * sin(Angle), radius * cos(Angle), 0.0f, 1.0f);
                }

                glUniform4fv(lightPositionUniform_PV[i], 1, light[i].LightPosition);//posiitional light
            }

            glUniform3fv(materialAmbientUniform_PV, 1, materialAmbient);
            glUniform3fv(materialDiffuseUniform_PV, 1, materilaDefuse);
            glUniform3fv(materialSpecularUniform_PV, 1, materialSpecular);

            glUniform1f(materialShininessUniform_PV, materialShininess);
        }
        else
        {
            glUniform1i(LKeyPressedUniform_PV, 0);
        }
    }


        glBindVertexArray(vaoSphere);
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboSphere_elements);
                glDrawElements(GL_TRIANGLES, gNumSphereElements, GL_UNSIGNED_SHORT, 0);
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
        glBindVertexArray(0);

    glUseProgram(0);
    Angle += 0.05f;

    //************shader Draw end******************************
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];//swap buffer
    
    
}
//display link - is timer object which matches refresh rate of screen with drawable layer to avoid screen/image terring
-(void)startAnimation
{
    //code
    if(isAnimating == NO)
    {
        //Set dynamically displayLink based on underlying GL technology and set/register callback function for drawView()
        displayLink = [NSClassFromString(@"CADisplayLink") displayLinkWithTarget:self selector:@selector(drawView:)];
        
        //set FPS
        [displayLink setPreferredFramesPerSecond:animationFrameInterval];
        
        //run loop
        //add displaylink to runloop and set display mode
        [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        
        isAnimating = YES;
    }
}

-(void)stopAnimation
{
    //code
    if(isAnimating == YES)
    {
        [displayLink invalidate];
        displayLink = nil;
        isAnimating = NO;
    }
    
}

-(void)onSingleTap:(UITapGestureRecognizer *)gr
{
    //code
    bTogglePVxOrPF = !bTogglePVxOrPF;

}

-(void)onDoubleTap:(UITapGestureRecognizer *)gr
{
    //code
    bLights = !bLights;
    if (bLights)
    {
        bTogglePVxOrPF = false;
    }

}

-(void)onSwipe:(UISwipeGestureRecognizer *)gr
{
    //code
    //exit code
    [self uninitialize];
    [self release];
    exit(0);

}


-(void)onLongPress:(UILongPressGestureRecognizer *)gr
{
    //code


}

-(void)uninitialize
{
    if(vaoSphere)
    {
        glDeleteVertexArrays(1,
                            &vaoSphere);
    }
    vaoSphere = 0;

    if(vboSphere_position)
    {
        glDeleteBuffers(1,
                    &vboSphere_position);
    }
    vboSphere_position = 0;
    
    if (vboSphere_normal)
    {
        glDeleteBuffers(1,
            &vboSphere_normal);
    }
    vboSphere_normal = 0;

    if (vboSphere_elements)
    {
        glDeleteBuffers(1,
            &vboSphere_elements);
    }
    vboSphere_elements = 0;

    if(gShaderProgramObject_PF)
    {
        glUseProgram(gShaderProgramObject_PF);

        GLsizei shaderCount;
        GLsizei tempShaderCount;
        GLuint *pShader=NULL;

        glGetProgramiv(gShaderProgramObject_PF,GL_ATTACHED_SHADERS,&shaderCount);

        pShader=(GLuint *)malloc(sizeof(GLuint)*shaderCount);
        if(pShader==NULL)
        {
            printf("\nFailed to allocate memory for pShader");
        }else{
            glGetAttachedShaders(gShaderProgramObject_PF,shaderCount,&tempShaderCount,pShader);
            
            printf("\nShader program has actual attached shader count is:%d",tempShaderCount);
            
            for(GLsizei i=0;i<shaderCount;i++)
            {
                glDetachShader(gShaderProgramObject_PF,pShader[i]);
                glDeleteShader(pShader[i]);
                pShader[i]=0;
            }
            free(pShader);
            
            glDeleteProgram(gShaderProgramObject_PF);
            gShaderProgramObject_PF =0;
        }
        glUseProgram(0);
    }


    if (gShaderProgramObject_PV)
    {
        glUseProgram(gShaderProgramObject_PV);

        GLsizei shaderCount;
        GLsizei tempShaderCount;
        GLuint* pShader = NULL;

        glGetProgramiv(gShaderProgramObject_PV, GL_ATTACHED_SHADERS, &shaderCount);

        pShader = (GLuint*)malloc(sizeof(GLuint) * shaderCount);
        if (pShader == NULL)
        {
            printf( "\nFailed to allocate memory for pShader");
        }
        else {
            glGetAttachedShaders(gShaderProgramObject_PV, shaderCount, &tempShaderCount, pShader);

            printf( "\nShader program has actual attached shader count is:%d", tempShaderCount);

            for (GLsizei i = 0; i < shaderCount; i++)
            {
                glDetachShader(gShaderProgramObject_PV, pShader[i]);
                glDeleteShader(pShader[i]);
                pShader[i] = 0;
            }
            free(pShader);

            glDeleteProgram(gShaderProgramObject_PV);
            gShaderProgramObject_PV = 0;
        }
        glUseProgram(0);
    }

    
    //code
    if(depthRenderbuffer)
    {
        glDeleteRenderbuffers(1, &depthRenderbuffer);
    }
    depthRenderbuffer=0;
    
    if(colorRenderbuffer)
    {
        glDeleteRenderbuffers(1, &colorRenderbuffer);
    }
    colorRenderbuffer=0;
    
    if(defaultFramebuffer)
    {
        glDeleteFramebuffers(1, &defaultFramebuffer);
    }
    defaultFramebuffer=0;
    
    if(eaglContext)
    {
        if([EAGLContext currentContext] == eaglContext)
        {
            [EAGLContext setCurrentContext:nil];
        }
        [eaglContext release];
    }
    eaglContext =nil;

}

///like destructor
-(void)dealloc
{
    ///code
    [self uninitialize];
    [super dealloc];
}


@end
