#include<stdio.h>
#include<stdlib.h>

#include "vmath.h"
using namespace vmath;

typedef mat4 datatype;

struct STACK{
    datatype data;
    struct STACK *next;
};


/*
int main()
{
    struct STACK *  CreateStackNode(datatype);
    void PushIntoStack(struct STACK ** ,struct STACK * );
    void DisplayStack(struct STACK ** );
    datatype TopData(struct STACK ** );
    datatype PopFromStack(struct STACK ** );
    void DeleteAllStackNode(struct STACK ** );

    int NumElement,i;
    datatype ElementValue;

    struct STACK *  temp=NULL;
    struct STACK *  TOP=NULL;

    printf("\nEnter Number of element to add in stack:\t");
    scanf("%d",&NumElement);
    
    for(i=0;i<NumElement;i++)
    {
        printf("\nEnter Data:%d:\t",i+1);
        scanf("%d",&ElementValue);
        temp=CreateStackNode(ElementValue);
        PushIntoStack(&TOP,temp);
    }

    DisplayStack(&TOP);

    ElementValue=TopData(&TOP);
    printf("\nTop Data:%d",ElementValue);

    ElementValue=PopFromStack(&TOP);
    printf("\nPoped Data:%d",ElementValue);

    DisplayStack(&TOP);

    DeleteAllStackNode(&TOP);

    DisplayStack(&TOP);


    return 0;
}

*/

struct STACK *  CreateStackNode(datatype data)
{

    struct STACK *  temp=NULL;
    temp=(struct STACK * )malloc(sizeof(struct STACK));

    if(temp==NULL)
    {
        return NULL;
    }

    temp->data=data;
    temp->next=NULL;

    return temp;
}

void PushIntoStack(struct STACK **  top,struct STACK *  currentNode)
{
    if (currentNode==NULL)
    {
        return;
    }
    currentNode->next=*top;
    *top=currentNode;
}

void DisplayStack(struct STACK **  top)
{
    int isEmptyStack(struct STACK ** );

    if(isEmptyStack(top))
    {
        return;
    }

    struct STACK *  temp=NULL;
    temp=*top;
    
    while(temp!=NULL)
    {
        temp=temp->next;
    }
}

int isEmptyStack(struct STACK **  top)
{
    if(top==NULL || *top==NULL)
    {
        return 1;
    }
    return 0;
}

datatype TopData(struct STACK **  top)
{
    int isEmptyStack(struct STACK ** );

    if(isEmptyStack(top))
    {
        return mat4::identity();
    }
    return (*top)->data;
}

datatype PopFromStack(struct STACK **  top)
{
    int isEmptyStack(struct STACK ** );

    if(isEmptyStack(top))
    {
        return mat4::identity();
    }

    datatype data;
    struct STACK *  temp=NULL;

    temp=*top;
   
    *top=(*top)->next;
    data=temp->data;
    
    free(temp);

    return data;

}

void DeleteAllStackNode(struct STACK **  top)
{
    int isEmptyStack(struct STACK ** );

    struct STACK *  walk=NULL;
    struct STACK *  temp=NULL;
   
    if(isEmptyStack(top))
    {
        return;
    }

    walk=*top;

    while(walk->next!=NULL)
    {
        temp=walk->next;
        walk->next=temp->next;
        free(temp);
    }
    
    free(walk);
    
    *top=NULL;

}
