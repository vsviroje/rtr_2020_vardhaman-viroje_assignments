#include<windows.h>
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
int WINAPI WinMain(HINSTANCE hInstance,HINSTANCE HPrevInstance,LPSTR lpszCmdLine,int iCmdShow)
{
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("MyCenteredWindow");
	
	RECT WindowSize;
	BOOL IsSystemParametersInfoOk;
	
	IsSystemParametersInfoOk =SystemParametersInfo(SPI_GETWORKAREA, 0, &WindowSize, 0);
	if (!IsSystemParametersInfoOk)
	{
		MessageBox(NULL, TEXT("Failed To Get INFO"), TEXT("FAILED"), MB_OK | MB_ICONERROR);
	}

	int X1 =( WindowSize.right - WindowSize.left)/2;
	int Y1 =( WindowSize.bottom - WindowSize.top)/2;
	

	/*
	int X1=GetSystemMetrics(SM_CXSCREEN)/2;
	int Y1= GetSystemMetrics(SM_CYSCREEN)/2;
	*/
	int X2 = 800/2;
	int Y2 = 600/2;


	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	RegisterClassEx(&wndclass);

	hwnd = CreateWindow(szAppName,
		TEXT("Centered Window"),
		WS_OVERLAPPEDWINDOW,
		X1 - X2,
		Y1 - Y2,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ShowWindow(hwnd, iCmdShow);
	UpdateWindow(hwnd);

	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return ((int)msg.wParam);
}
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	switch (iMsg) 
	{
		case WM_DESTROY:
			PostQuitMessage(0);
			break;
	}

	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}