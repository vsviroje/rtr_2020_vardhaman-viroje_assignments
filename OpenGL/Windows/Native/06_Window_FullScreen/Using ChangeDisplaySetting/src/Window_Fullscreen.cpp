#include<windows.h>

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
bool gbFullscreen = false;
HWND ghwnd = NULL;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE HPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("WindowFullScreen");

	RECT WindowSize;
	BOOL IsSystemParametersInfoOk;
	int nWidth = 800;
	int nHeight = 600;
	IsSystemParametersInfoOk = SystemParametersInfo(SPI_GETWORKAREA, 0, &WindowSize, 0);
	if (!IsSystemParametersInfoOk)
	{
		MessageBox(NULL, TEXT("Failed to get system parameter info"), TEXT("FAILED"), MB_OK | MB_ICONERROR);
	}

	int X1 = (WindowSize.right - WindowSize.left) / 2;
	int Y1 = (WindowSize.bottom - WindowSize.top) / 2;

	int X2 = nWidth / 2;
	int Y2 = nHeight / 2;


	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	RegisterClassEx(&wndclass);

	hwnd = CreateWindow(szAppName,
		TEXT("Fullscreen Window"),
		WS_OVERLAPPEDWINDOW,
		X1 - X2,
		Y1 - Y2,
		nWidth,
		nHeight,
		NULL,
		NULL,
		hInstance,
		NULL);
	ghwnd = hwnd;
	ShowWindow(hwnd, iCmdShow);
	UpdateWindow(hwnd);

	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return ((int)msg.wParam);
}
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void ToggleFullscreen();
	void ToggleFullscreen2();
	void ToggleFullscreen3();

	switch (iMsg)
	{
	case WM_KEYDOWN:
		switch(wParam)
		{
		case 0x46:
		case 0x66:
			//ToggleFullscreen();//GetMonitorInfo
			ToggleFullscreen2();//ChangeDisplaySetting +SetWindowPlacement+ SetWindowPos
			//ToggleFullscreen3();//ChangeDisplaySetting (Blinks)
			break;
		default:
			break;
		}
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}

	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}


void ToggleFullscreen() {
	//code
	MONITORINFO mi = { sizeof(MONITORINFO) };
	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd,MONITORINFOF_PRIMARY),&mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, (dwStyle & ~WS_OVERLAPPEDWINDOW));
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
		gbFullscreen = true;
	}
	else 
	{
		SetWindowLong(ghwnd, GWL_STYLE, (dwStyle | WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
		gbFullscreen = false;

	}
}

void ToggleFullscreen2() {
	DEVMODE dm = { sizeof(DEVMODE) };
	dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
	EnumDisplaySettings(NULL, ENUM_CURRENT_SETTINGS, &dm);

	if (gbFullscreen == false)
	{
		
		if (!GetWindowPlacement(ghwnd, &wpPrev))
		{
			return;
		}
		
		if ((ChangeDisplaySettings(&dm, CDS_FULLSCREEN) == DISP_CHANGE_SUCCESSFUL)&& (dwStyle & WS_OVERLAPPEDWINDOW))
		{
			SetWindowLong(ghwnd, GWL_STYLE, (dwStyle & ~WS_OVERLAPPEDWINDOW));
			SetWindowPos(ghwnd, HWND_TOP, 0,0,dm.dmPelsWidth,dm.dmPelsHeight, SWP_NOZORDER | SWP_FRAMECHANGED);
		}
		ShowCursor(FALSE);
		gbFullscreen = true;
	}
	else
	{
		if (ChangeDisplaySettings(NULL, CDS_RESET) == DISP_CHANGE_SUCCESSFUL)
		{
			SetWindowLong(ghwnd, GWL_STYLE, (dwStyle | WS_OVERLAPPEDWINDOW));
			SetWindowPlacement(ghwnd, &wpPrev);
			SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		}
		ShowCursor(TRUE);
		gbFullscreen = false;
	}

}


//Fullscreen Blinks for toggle back to normal window.
void ToggleFullscreen3() {
	DEVMODE dm = { sizeof(DEVMODE) };
	dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

	EnumDisplaySettings(NULL, ENUM_CURRENT_SETTINGS, &dm);

	if (gbFullscreen == false)
	{
		if ((ChangeDisplaySettings(&dm, CDS_FULLSCREEN) == DISP_CHANGE_SUCCESSFUL) && (dwStyle & WS_OVERLAPPEDWINDOW))
		{
			SetWindowLong(ghwnd, GWL_STYLE, (dwStyle & ~WS_OVERLAPPEDWINDOW));
		}
		ShowCursor(FALSE);
		gbFullscreen = true;
	}
	else
	{
		if (ChangeDisplaySettings(NULL, CDS_RESET) == DISP_CHANGE_SUCCESSFUL)
		{
			SetWindowLong(ghwnd, GWL_STYLE, (dwStyle | WS_OVERLAPPEDWINDOW));
		}
		ShowCursor(TRUE);
		gbFullscreen = false;
	}

}