#include<windows.h>
#include<stdio.h>
#include"tweaked_Smiley.h"

#include<GL/GLEW.h>
#include<GL/GL.h>
#include"vmath.h"

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

#pragma comment(lib,"GLEW32.lib")
#pragma comment(lib,"OpenGL32.lib")

using namespace vmath;

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

FILE* gpFile = NULL;
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
bool gbFullscreen = false;
bool gbActiveWindow = false;
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

enum{
	VSV_ATTRIBUTE_POSITION=0,
	VSV_ATTRIBUTE_COLOR,
	VSV_ATTRIBUTE_NORMAL,
	VSV_ATTRIBUTE_TEXCOORD,
};

GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

GLuint vao_staticSmiley;
GLuint vbo_smileyTexcoord;
GLuint vbo_smileyPosition;
GLuint vbo_smileysWhitelColor;


GLuint mvpMatrixUniform;
GLuint textureSamplerUniform;

mat4 perspectiveProjectionMatrix;

GLuint smileyTextureID;

unsigned int PressedDigit;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	void Initialize(void);
	void Display(void);

	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("PPtweakedSmiley");
	int X1, X2, Y1, Y2;
	bool bDone = false;

	if (fopen_s(&gpFile, "log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Failed to Create log file!"), TEXT("Error"), MB_OK | MB_ICONERROR);
		exit(0);
	}
	
	X1 = GetSystemMetrics(SM_CXSCREEN) / 2;
	Y1 = GetSystemMetrics(SM_CYSCREEN) / 2;

	X2 = WIN_WIDTH / 2;
	Y2 = WIN_HEIGHT / 2;

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style =  CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(GRAY_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));

	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("OpenGL Programmable Pipeline Tweaked Smiley"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		X1 - X2,//100,
		Y1 - Y2,//100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;
	Initialize();

	ShowWindow(hwnd, iCmdShow);

	SetForegroundWindow(hwnd);
	SetFocus(hwnd);
	


	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
		
			if (gbActiveWindow == true)
			{
				Display();
			}
		}
	}

	return ((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void ToggleFullscreen(void);
	void Resize(int, int);
	void UnInitialize(void);

	switch (iMsg)
	{
	case WM_CREATE:
		fprintf(gpFile, "\nlog is Started!");
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
			case VK_ESCAPE:
				DestroyWindow(hwnd);
				break;
			case 0x46:
			case 0x66:
				ToggleFullscreen();
				break;
			case 49://1
				PressedDigit=1;
				glEnable(GL_TEXTURE_2D);
				break;
			case 50://2
				PressedDigit=2;
				glEnable(GL_TEXTURE_2D);
				break;
			case 51://3
				PressedDigit=3;
				glEnable(GL_TEXTURE_2D);
				break;
			case 52://4
				PressedDigit=4;
				glEnable(GL_TEXTURE_2D);
				break;												
			default:
				PressedDigit=0;
				glDisable(GL_TEXTURE_2D);
				break;
		}
		break;
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;
	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;

	case WM_ERASEBKGND:
		return 0;
	case WM_SIZE:
		Resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;
	case WM_DESTROY:
		UnInitialize();
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}
void ToggleFullscreen(void)
{
	MONITORINFO mi = { sizeof(MONITORINFO) };
	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW) 
		{
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				fprintf(gpFile, "\nFullscreen Enabled");
				SetWindowLong(ghwnd, GWL_STYLE, (dwStyle & ~WS_OVERLAPPEDWINDOW));
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
		gbFullscreen = true;
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, (dwStyle | WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		fprintf(gpFile, "\nNormal Window");
		ShowCursor(TRUE);
		gbFullscreen = false;
	}
}

void Initialize(void)
{
	void Resize(int, int);
	bool LoadGLTexture(GLuint *,TCHAR[]);

	//Local variable initialization
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	ghdc = GetDC(ghwnd);

	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER ;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits= 32 ;


	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (iPixelFormatIndex == 0 )
	{
		fprintf(gpFile, "\nChoosePixelFormat() Failed to get pixel format index");
		DestroyWindow(ghwnd);
	}

	if ( SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE )
	{
		fprintf(gpFile, "\nSetPixelFormat() Failed to set pixel format for window");
		DestroyWindow(ghwnd);
	}

	ghrc = wglCreateContext(ghdc);
	if ( ghrc == NULL)
	{
		fprintf(gpFile, "\nwglCreateContext() Failed to get the rendering context");
		DestroyWindow(ghwnd);
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE )
	{
		fprintf(gpFile, "\nwglMakeCurrent() Failed to switch current context from device to rendering context");
		DestroyWindow(ghwnd);
	}

	GLenum glew_error=glewInit();
	if(glew_error != GLEW_OK)
	{
		fprintf(gpFile, "\nglewInit failed to enable extension for Programmable Pipeline");
		DestroyWindow(ghwnd);

	}
	fprintf(gpFile, "\nOpenGL Information are availble below:-");
	fprintf(gpFile, "\n*****************************************************");

	fprintf(gpFile, "\nOpenGL Vender:%s",glGetString(GL_VENDOR));
	fprintf(gpFile, "\nOpenGL Renderer:%s",glGetString(GL_RENDERER));
	fprintf(gpFile, "\nOpenGL Version:%s",glGetString(GL_VERSION));
	fprintf(gpFile, "\nOpenGL GLSL Version:%s",glGetString(GL_SHADING_LANGUAGE_VERSION));
	
	fprintf(gpFile, "\nOpenGL Extension's list:\n");
	GLint numExt;
	glGetIntegerv(GL_NUM_EXTENSIONS,&numExt);

	for(int i=0;i<numExt;i++)
	{
		fprintf(gpFile, "\n%s",glGetStringi(GL_EXTENSIONS,i));
	}
	fprintf(gpFile, "\n*****************************************************");


	GLint infoLogLength;
	GLint shaderCompileStatus;
	GLint shaderProgramLinkStatus;
	GLchar *szBuffer;
	GLsizei written;

	//Shader Code Start--------------------
		gVertexShaderObject=glCreateShader(GL_VERTEX_SHADER);

		const GLchar *vertexShaderSrc=
		"#version 460 core"\
		"\n"\
		"in vec4 vPosition;"\

		"in vec2 vTexCoord;"\
		"out vec2 out_TexCoord;"\
		
		"in vec4 vColor;"\
		"out vec4 out_color;"\
		
		"uniform mat4 u_mvpMatrix;"\

		"void main(void)"\
		"{"\
			"out_TexCoord=vTexCoord;"\
			"out_color=vColor;"\
			"gl_Position=u_mvpMatrix * vPosition;"\
		"}";

		glShaderSource(gVertexShaderObject,
						1,
						(const GLchar **)&vertexShaderSrc,
						NULL);

		glCompileShader(gVertexShaderObject);

		glGetShaderiv(gVertexShaderObject,
						GL_COMPILE_STATUS,
						&shaderCompileStatus);

		if(shaderCompileStatus ==GL_FALSE)
		{
			glGetShaderiv(gVertexShaderObject,
						GL_INFO_LOG_LENGTH,
						&infoLogLength);

			if(infoLogLength > 0)
			{
				szBuffer=(GLchar *)malloc(sizeof(GLchar) * infoLogLength);

				if(szBuffer!=NULL)
				{
					glGetShaderInfoLog(gVertexShaderObject,
										infoLogLength,
										&written,
										szBuffer);
					fprintf(gpFile,
							"\nERROR | Vertex Shader Compilation Failed Log : %s",
							szBuffer);
					free(szBuffer);
					DestroyWindow(ghwnd);
				}
			}
		}

		infoLogLength=0;
		shaderCompileStatus=0;
		shaderProgramLinkStatus=0;
		szBuffer=NULL;
		written=0;

		gFragmentShaderObject=glCreateShader(GL_FRAGMENT_SHADER);

		const GLchar *fragmentShaderSrc=
		"#version 460 core"\
		"\n"\
		
		"in vec2 out_TexCoord;"\
		
		"in vec4 out_color;"\
		
		"out vec4 fragColor;"\
		
		"uniform sampler2D u_texture_sampler;"\
		
		"void main(void)"\
		"{"\
			"if(out_TexCoord[0] != 0.0f){"\
				"fragColor=texture(u_texture_sampler , out_TexCoord) ;"\
			"}else{"\
				"fragColor=out_color;"\
			"}"\
		"}";

		glShaderSource(gFragmentShaderObject,
						1,
						(const GLchar**)&fragmentShaderSrc,
						NULL);
		
		glCompileShader(gFragmentShaderObject);

		glGetShaderiv(gFragmentShaderObject,
						GL_COMPILE_STATUS,
						&shaderCompileStatus);

		if(shaderCompileStatus==GL_FALSE)
		{
			glGetShaderiv(gFragmentShaderObject,
						GL_INFO_LOG_LENGTH,
						&infoLogLength);

			if(infoLogLength > 0 )
			{
				szBuffer=(GLchar*)malloc(sizeof(GLchar) * infoLogLength);

				glGetShaderInfoLog(gFragmentShaderObject,
										infoLogLength,
										&written,
										szBuffer);

				fprintf(gpFile,
						"\nERROR | Fragment Shader Compilation failed log %s",
						szBuffer);

				free(szBuffer);

				DestroyWindow(ghwnd);
			}
		}

		infoLogLength=0;
		shaderCompileStatus=0;
		shaderProgramLinkStatus=0;
		szBuffer=NULL;
		written=0;

		gShaderProgramObject=glCreateProgram();
		glAttachShader(gShaderProgramObject,gVertexShaderObject);
		glAttachShader(gShaderProgramObject,gFragmentShaderObject);

		//Shader Attribute Binding Before Pre Linking of attached Shader
			glBindAttribLocation(gShaderProgramObject,VSV_ATTRIBUTE_POSITION,"vPosition");
			glBindAttribLocation(gShaderProgramObject,VSV_ATTRIBUTE_TEXCOORD,"vTexCoord");
			glBindAttribLocation(gShaderProgramObject,VSV_ATTRIBUTE_COLOR,"vColor");

		glLinkProgram(gShaderProgramObject);

		glGetProgramiv(gShaderProgramObject,
						GL_LINK_STATUS,
						&shaderProgramLinkStatus);

		if(shaderProgramLinkStatus==GL_FALSE)
		{
			glGetProgramiv(gShaderProgramObject,
							GL_INFO_LOG_LENGTH,
							&infoLogLength);
			
			if(infoLogLength > 0)
			{
				szBuffer=(GLchar *)malloc(sizeof(GLchar) * infoLogLength);
				if(szBuffer!=NULL)
				{
					glGetProgramInfoLog(gShaderProgramObject,
										infoLogLength,
										&written,
										szBuffer);
					fprintf(gpFile,
							"\nERROR | Shader Program Link Failed Log %s",
							szBuffer);
					free(szBuffer);
					DestroyWindow(ghwnd);
				}
			}
		}
		
		//Uniform variable Binding after post linking of attached shader
		mvpMatrixUniform = glGetUniformLocation(gShaderProgramObject,
												"u_mvpMatrix");
		textureSamplerUniform = glGetUniformLocation(gShaderProgramObject,
													"u_texture_sampler");

	//Shader Code End--------------------

		
	//VAO and its respective VBO preparation
	const GLfloat rectangleVertices[]=
	{
		-1.0f, -1.0f, 0.0f,
		1.0f, -1.0f, 0.0f,
		1.0f, 1.0f, 0.0f,
		-1.0f, 1.0f, 0.0f
	};

	const GLfloat rectangleColor[]=
	{
			1.0f,1.0f,1.0f,
			1.0f,1.0f,1.0f,
			1.0f,1.0f,1.0f,
			1.0f,1.0f,1.0f
	};

	glGenVertexArrays(1 , &vao_staticSmiley);
	
	glBindVertexArray(vao_staticSmiley);

		glGenBuffers(1 , &vbo_smileyPosition);
		
		glBindBuffer(GL_ARRAY_BUFFER , vbo_smileyPosition);

			glBufferData(GL_ARRAY_BUFFER,
						sizeof(rectangleVertices),
						rectangleVertices,
						GL_STATIC_DRAW);
			
			glVertexAttribPointer(VSV_ATTRIBUTE_POSITION,
								3,
								GL_FLOAT,
								GL_FALSE,
								0,
								NULL);
			
			glEnableVertexAttribArray(VSV_ATTRIBUTE_POSITION);
		
		glBindBuffer(GL_ARRAY_BUFFER,0);

		glGenBuffers(1,&vbo_smileyTexcoord);
		
		glBindBuffer(GL_ARRAY_BUFFER,vbo_smileyTexcoord);

			glBufferData(GL_ARRAY_BUFFER,
						4*2*sizeof(GLfloat),
						NULL,
						GL_DYNAMIC_DRAW);

			glVertexAttribPointer(VSV_ATTRIBUTE_TEXCOORD,
									2,
									GL_FLOAT,
									GL_FALSE,
									0,
									NULL);

			glEnableVertexAttribArray(VSV_ATTRIBUTE_TEXCOORD);

		glBindBuffer(GL_ARRAY_BUFFER,0);

		glGenBuffers(1 , &vbo_smileysWhitelColor);

		glBindBuffer(GL_ARRAY_BUFFER,vbo_smileysWhitelColor);
		
			glBufferData(GL_ARRAY_BUFFER,
						sizeof(rectangleColor),
						rectangleColor,
						GL_STATIC_DRAW);
		
			glVertexAttribPointer(VSV_ATTRIBUTE_COLOR,
									3,
									GL_FLOAT,
									GL_FALSE,
									0,
									NULL);
		
			glEnableVertexAttribArray(VSV_ATTRIBUTE_COLOR);
		
		glBindBuffer(GL_ARRAY_BUFFER,0);		

	glBindVertexArray(0);


	//SetColor
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	
	//Enable Depth steps
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	//Loading Texture
	LoadGLTexture(&smileyTextureID,MAKEINTRESOURCE(SMILEY_BITMAP));

	//Warmup resize call
	perspectiveProjectionMatrix=mat4::identity();
	Resize(WIN_WIDTH, WIN_HEIGHT);
	
}

void Resize(int width, int height)
{
	if (height == 0)
	{
		height = 1;
	}
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	perspectiveProjectionMatrix=vmath::perspective(45.0f,
													(GLfloat)width/(GLfloat)height,
													0.1f,
													100.0f);
}

	 GLfloat rectangleTexCoordPress1[]=
	{
			0.0f,0.0f,
			1.0f,0.0f,
			1.0f,1.0f,
			0.0f,1.0f
	};

	 GLfloat rectangleTexCoordPress2[]=
	{
			0.0f,0.0f,
			0.5f,0.0f,
			0.5f,0.5f,
			0.0f,0.5f
	};

	 GLfloat rectangleTexCoordPress3[]=
	{
			0.0f,0.0f,
			2.0f,0.0f,
			2.0f,2.0f,
			0.0f,2.0f
	};

	 GLfloat rectangleTexCoordPress4[]=
	{
			0.5f,0.5f,
			0.5f,0.5f,
			0.5f,0.5f,
			0.5f,0.5f	
	};

	GLfloat temp[1]={0.0f};

void Display(void)
{
	mat4 modelViewMatrix;
	mat4 modelViewProjectionMatrix;
	mat4 translateMatrix;
	GLfloat *rectangleTexCoord;

	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

	glUseProgram(gShaderProgramObject);

		translateMatrix = vmath::translate(0.0f,0.0f,-6.0f);

		modelViewMatrix = translateMatrix;

		modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

		glUniformMatrix4fv(mvpMatrixUniform,
						1,
						GL_FALSE,
						modelViewProjectionMatrix);

		switch (PressedDigit)
		{
			case 1:
			case 2:
			case 3:
			case 4:
				glActiveTexture(GL_TEXTURE0);
				glBindTexture(GL_TEXTURE_2D , smileyTextureID);
				glUniform1i(textureSamplerUniform , 0);
				break;
			case 0:
			default:
					glBindTexture(GL_TEXTURE_2D , 0);
					break;
		}
		
		glBindVertexArray(vao_staticSmiley);
			switch (PressedDigit)
			{
				case 1:
				rectangleTexCoord = rectangleTexCoordPress1;
				break;
				
				case 2:
				rectangleTexCoord = rectangleTexCoordPress2;
				break;
				
				case 3:
				rectangleTexCoord = rectangleTexCoordPress3;
				break;
				
				case 4:
				rectangleTexCoord = rectangleTexCoordPress4;
				break;

				case 0:
				default:
				rectangleTexCoord=temp;
				break;
			}

			glBindBuffer(GL_ARRAY_BUFFER,vbo_smileyTexcoord);

				glBufferData(GL_ARRAY_BUFFER,
								4*2*sizeof(GLfloat),
								rectangleTexCoord,
								GL_DYNAMIC_DRAW);

				glVertexAttribPointer(VSV_ATTRIBUTE_TEXCOORD,
										2,
										GL_FLOAT,
										GL_FALSE,
										0,
										NULL);

				glEnableVertexAttribArray(VSV_ATTRIBUTE_TEXCOORD);

			glBindBuffer(GL_ARRAY_BUFFER,0);

			glDrawArrays(GL_TRIANGLE_FAN , 0 , 4);
		glBindVertexArray(0);
	
	glUseProgram(0);
	
	SwapBuffers(ghdc);
	
}
void UnInitialize(void)
{
	if (gbFullscreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, (dwStyle | WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}
	
	if(vao_staticSmiley)
	{
		glDeleteVertexArrays(1,
							&vao_staticSmiley);
	}
	vao_staticSmiley=0;

	if(vbo_smileyPosition)
	{
		glDeleteBuffers(1,
					&vbo_smileyPosition);
	}
	vbo_smileyPosition=0;

	if(vbo_smileyTexcoord)
	{
		glDeleteBuffers(1,
					&vbo_smileyTexcoord);
	}
	vbo_smileyTexcoord=0;

	if(vbo_smileysWhitelColor)
	{
		glDeleteBuffers(1,
					&vbo_smileysWhitelColor);
	}
	vbo_smileysWhitelColor=0;

	if(gShaderProgramObject)
	{
		glUseProgram(gShaderProgramObject);

		GLsizei shaderCount;
		GLsizei tempShaderCount;
		GLuint *pShader=NULL;

		glGetProgramiv(gShaderProgramObject,GL_ATTACHED_SHADERS,&shaderCount);

		pShader=(GLuint *)malloc(sizeof(GLuint)*shaderCount);
		if(pShader==NULL)
		{
			fprintf(gpFile,"\nFailed to allocate memory for pShader");
		}else{
			glGetAttachedShaders(gShaderProgramObject,shaderCount,&tempShaderCount,pShader);
			
			fprintf(gpFile,"\nShader program has actual attached shader count is:%d",tempShaderCount);
			
			for(GLsizei i=0;i<shaderCount;i++)
			{
				glDetachShader(gShaderProgramObject,pShader[i]);
				glDeleteShader(pShader[i]);
				pShader[i]=0;
			}
			free(pShader);
			
			glDeleteProgram(gShaderProgramObject);
			gShaderProgramObject=0;
			glUseProgram(0);
		}
	}

	glDeleteTextures(1,&smileyTextureID);


	if (wglGetCurrentContext() == ghrc ) {
		wglMakeCurrent(NULL, NULL);
	}


	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (gpFile)
	{
		fprintf(gpFile, "\nlog is Ended!");
		fclose(gpFile);
		gpFile = NULL;
	}
}

bool LoadGLTexture(GLuint *texture,TCHAR resourceID[])
{
	bool bResult=false;
	HBITMAP hbitmap=NULL;
	BITMAP bmp;

	hbitmap=(HBITMAP) LoadImage( GetModuleHandle(NULL),
								resourceID,
								IMAGE_BITMAP,
								0,
								0,
								LR_CREATEDIBSECTION);
	
	if(hbitmap!=NULL)
	{
		bResult=true;
		GetObject( hbitmap,
					sizeof(BITMAP),
					&bmp);
		
		glPixelStorei(GL_UNPACK_ALIGNMENT,
					1);
		
		glGenTextures(1 ,  
					texture);

		glBindTexture( GL_TEXTURE_2D,
						*texture);
		

		glTexParameteri(GL_TEXTURE_2D,
						GL_TEXTURE_MAG_FILTER,
						GL_LINEAR);

		glTexParameteri(GL_TEXTURE_2D,
						GL_TEXTURE_MIN_FILTER,
						GL_LINEAR_MIPMAP_LINEAR);


		glTexImage2D(GL_TEXTURE_2D,
					0,
					GL_RGB,
					bmp.bmWidth,
					bmp.bmHeight,
					0,
					GL_BGR,
					GL_UNSIGNED_BYTE,
					bmp.bmBits);

		glGenerateMipmap(GL_TEXTURE_2D);

		DeleteObject(hbitmap);
	}
	return bResult;
}


/*
cl /c /EHsc  /I C:\glew-2.1.0-win32\glew-2.1.0\include  tweaked_Smiley.cpp 
rc tweaked_Smiley.rc 
link tweaked_Smiley.obj tweaked_Smiley.res /LIBPATH:C:\glew-2.1.0-win32\glew-2.1.0\lib\Release\x64 user32.lib gdi32.lib kernel32.lib /SUBSYSTEM:WINDOWS
tweaked_Smiley.exe
*/




