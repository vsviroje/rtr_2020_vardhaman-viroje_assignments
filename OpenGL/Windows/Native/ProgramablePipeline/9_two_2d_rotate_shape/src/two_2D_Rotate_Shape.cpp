#include<windows.h>
#include<stdio.h>
#include"two_2D_Rotate_Shape.h"
#include "vmath.h"
#include<GL/GLEW.h>
#include<GL/GL.h>

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

#pragma comment(lib,"GLEW32.lib")
#pragma comment(lib,"OpenGL32.lib")

using namespace vmath;

LRESULT CALLBACK WndProc(HWND,UINT,WPARAM,LPARAM);

FILE* gpFile=NULL;
DWORD dwStyle;
WINDOWPLACEMENT wpPrev={sizeof(WINDOWPLACEMENT)};
bool gbFullscreen=false;
bool gbActiveWindow=false;
HWND ghwnd=NULL;
HDC ghdc=NULL;
HGLRC ghrc=NULL;

enum{
	VSV_ATTRIBUTE_POSITION=0,
	VSV_ATTRIBUTE_COLOR,
	VSV_ATTRIBUTE_TEXCOORD,
	VSV_ATTRIBUTE_NORMAL,
};

GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

GLuint vao_triangle;
GLuint vao_square;

GLuint vbo_triangleColor;
GLuint vbo_trianglePosition;

GLuint vbo_squarePosition;

GLuint mvpMatrixUniform;
mat4 perspectiveProjectionMatrix;


int WINAPI WinMain(HINSTANCE hInstance,HINSTANCE hPrevInstance,LPSTR lpszCmdline,int iCmdShow)
{
	void Initialize(void);
	void Display(void);

	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[]=TEXT("two2DRotateShape");
	int X1,X2,Y1,Y2;
	bool bDone=false;

	if(fopen_s(&gpFile,"log.txt","w")!=0)
	{
		MessageBox(NULL,TEXT("Failed to create log file!"),TEXT("ERROR"),MB_OK | MB_ICONERROR);
		exit(0);
	}

	X1=GetSystemMetrics(SM_CXSCREEN)/2;
	Y1=GetSystemMetrics(SM_CYSCREEN)/2;

	X2=WIN_WIDTH/2;
	Y2=WIN_HEIGHT/2;

	wndclass.cbSize=sizeof(WNDCLASSEX);
	wndclass.style=CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra=0;
	wndclass.cbWndExtra=0;
	wndclass.lpfnWndProc=WndProc;
	wndclass.hInstance=hInstance;
	wndclass.hIcon=LoadIcon(hInstance,MAKEINTRESOURCE(MYICON));
	wndclass.hCursor=LoadCursor(NULL,IDC_ARROW);
	wndclass.hbrBackground=(HBRUSH)GetStockObject(GRAY_BRUSH);
	wndclass.lpszClassName=szAppName;
	wndclass.lpszMenuName=NULL;
	wndclass.hIconSm=LoadIcon(hInstance,MAKEINTRESOURCE(MYICON));

	RegisterClassEx(&wndclass);

	hwnd=CreateWindowEx(WS_EX_APPWINDOW,
						szAppName,
						TEXT("Two 2D Rotate Shape"),
						WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
						X1-X2,
						Y1-Y2,
						WIN_WIDTH,
						WIN_HEIGHT,
						NULL,
						NULL,
						hInstance,
						NULL);

	ghwnd=hwnd;
	Initialize();

	ShowWindow(hwnd,iCmdShow);
	SetForegroundWindow(hwnd);

	SetFocus(hwnd);

	while(bDone==false)
	{
		if(PeekMessage(&msg,NULL,0,0,PM_REMOVE))
		{
			if(msg.message==WM_QUIT)
			{
				bDone=true;
			}else{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}else{
			if(gbActiveWindow==true)
			{
				Display();
			}
		}
	}

	return ((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd,UINT iMsg,WPARAM wParam,LPARAM lParam)
{
	void ToggleFullscreen(void);
	void Resize(int,int);
	void UnInitialize(void);

	switch(iMsg)
	{
		case WM_CREATE:
			fprintf(gpFile,"\nlog is started!");
			break;
		case WM_KEYDOWN:
			switch(wParam)
			{
				case VK_ESCAPE:
					DestroyWindow(hwnd);
					break;
				case 0x46:
				case 0x66:
					ToggleFullscreen();
					break;
				default:
					break;
			}
			break;
		case WM_SETFOCUS:
			gbActiveWindow=true;
			break;
		case WM_KILLFOCUS:
			gbActiveWindow=false;
			break;

		case WM_ERASEBKGND:
			return 0;
		case WM_SIZE:
			Resize(LOWORD(lParam),HIWORD(lParam));
			break;
		case WM_CLOSE:
			DestroyWindow(hwnd);
			break;
		case WM_DESTROY:
			UnInitialize();
			PostQuitMessage(0);
			break;
		default:
			break;
	}

	return (DefWindowProc(hwnd,iMsg,wParam,lParam));
}

void ToggleFullscreen(void)
{
	MONITORINFO mi={sizeof(MONITORINFO)};
	if(gbFullscreen==false)
	{
		dwStyle=GetWindowLong(ghwnd,GWL_STYLE);
		if(dwStyle & WS_OVERLAPPEDWINDOW)
		{
			if(GetWindowPlacement(ghwnd,&wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd,MONITORINFOF_PRIMARY),&mi))
			{
				fprintf(gpFile,"\nFullscreen Enabled");
				SetWindowLong(ghwnd,GWL_STYLE,(dwStyle & ~WS_OVERLAPPEDWINDOW));
				SetWindowPos(ghwnd,HWND_TOP,mi.rcMonitor.left,mi.rcMonitor.top,mi.rcMonitor.right-mi.rcMonitor.left,mi.rcMonitor.bottom-mi.rcMonitor.top,SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		gbFullscreen=true;
	}
	else{
		SetWindowLong(ghwnd,GWL_STYLE,(dwStyle | WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(ghwnd,&wpPrev);
		SetWindowPos(ghwnd,HWND_TOP,0,0,0,0,SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED );
		fprintf(gpFile,"\nNormal Window");
		gbFullscreen=false;
	}
}

void Initialize(void)
{
	void Resize(int,int);

	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	ghdc=GetDC(ghwnd);

	ZeroMemory(&pfd,sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize=sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion=1;
	pfd.dwFlags=PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType=PFD_TYPE_RGBA;
	pfd.cColorBits=32;
	pfd.cRedBits=8;
	pfd.cGreenBits=8;
	pfd.cBlueBits=8;
	pfd.cAlphaBits=8;
	pfd.cDepthBits=32;

	iPixelFormatIndex=ChoosePixelFormat(ghdc,&pfd);

	if(iPixelFormatIndex==0)
	{
		fprintf(gpFile,"\nChoosePixelFormat() failed to get pixel format index");
		DestroyWindow(ghwnd);
	}

	if(SetPixelFormat(ghdc,iPixelFormatIndex,&pfd)==FALSE)
	{
		fprintf(gpFile,"\nSetPixelFormat() failed to set pixel format for window");
		DestroyWindow(ghwnd);
	}

	ghrc=wglCreateContext(ghdc);
	if(ghrc==NULL)
	{
		fprintf(gpFile,"\nwglCreateContext() failed to get the rendering context");
		DestroyWindow(ghwnd);
	}

	if(wglMakeCurrent(ghdc,ghrc)==FALSE)
	{
		fprintf(gpFile,"\nwglMakeCurrent() failed to swith current context from device to rendering context");
		DestroyWindow(ghwnd);
	}

	GLenum glew_error=glewInit();
	if(glew_error != GLEW_OK)
	{
		fprintf(gpFile,"\nglewInit() failed to enable extension for programmable pipeline");
		DestroyWindow(ghwnd);
	}

	fprintf(gpFile,"\nOpenGL Information are available below:-");
	fprintf(gpFile,"\n*********************************************************************************");

	fprintf(gpFile,"\nOpenGL Vender:%s",glGetString(GL_VENDOR));
	fprintf(gpFile,"\nOpenGL Render:%s",glGetString(GL_RENDERER));
	fprintf(gpFile,"\nOpenGL Version:%s",glGetString(GL_VERSION));
	fprintf(gpFile,"\nOpenGL GLSL Version:%s",glGetString(GL_SHADING_LANGUAGE_VERSION));

	fprintf(gpFile,"\nOpenGL Extension's List:\n");

	GLint numExt;
	glGetIntegerv(GL_NUM_EXTENSIONS,&numExt);

	for(int i=0;i<numExt;i++)
	{
		fprintf(gpFile,"\n%s",glGetStringi(GL_EXTENSIONS,i));
	}
	fprintf(gpFile,"\n*********************************************************************************");
		
	GLint infoLogLength;
	GLint shaderCompileStatus;
	GLint shaderProgramLinkStatus;
	GLchar *szBuffer;
	GLsizei written;
	
	//Shader Code Start--------------------
		gVertexShaderObject=glCreateShader(GL_VERTEX_SHADER);

		const GLchar *vertexShaderSourceCode=
			"#version 460 core" \
			"\n" \
			"in vec4 vPosition;"\
			"in vec4 vColor;"\
			"out vec4 out_color;"\
			"uniform mat4 u_mvpMatrix;"\
			"void main(void)" \
			"{" \
				"out_color=vColor;"\
				"gl_Position=u_mvpMatrix * vPosition;"\
			"}";

		glShaderSource(gVertexShaderObject,
						1,
						(const GLchar **)&vertexShaderSourceCode,
						NULL);

		glCompileShader(gVertexShaderObject);

		glGetShaderiv(gVertexShaderObject,
						GL_COMPILE_STATUS,
						&shaderCompileStatus);

						
		if(shaderCompileStatus==GL_FALSE)
		{
			glGetShaderiv(gVertexShaderObject,
							GL_INFO_LOG_LENGTH,
							&infoLogLength);
			if(infoLogLength>0)
			{
				szBuffer=(GLchar*)malloc(sizeof(GLchar) * infoLogLength);

				if(szBuffer!=NULL)
				{
					glGetShaderInfoLog(gVertexShaderObject,
										infoLogLength,
										&written,
										szBuffer);
					
					fprintf(gpFile,"\nERROR:Vertex shader compilation failed log: %s",szBuffer);
					free(szBuffer);
					DestroyWindow(ghwnd);
				}
			}
		}

		infoLogLength=0;
		shaderCompileStatus=0;
		shaderProgramLinkStatus=0;
		szBuffer=NULL;
		written=0;

		gFragmentShaderObject=glCreateShader(GL_FRAGMENT_SHADER);

		const GLchar *fragmentShaderSourceCode=
			"#version 450" \
			"\n" \
			"in vec4 out_color;"\
			"out vec4 fragColor;"\
			"void main(void)" \
			"{"\
				"fragColor=out_color;"\
			"}";

		glShaderSource(gFragmentShaderObject,
						1,
						(const GLchar**)&fragmentShaderSourceCode,
						NULL);

		glCompileShader(gFragmentShaderObject);

		glGetShaderiv(gFragmentShaderObject,
					GL_COMPILE_STATUS,
					&shaderCompileStatus);

		
		if(shaderCompileStatus==GL_FALSE)
		{
			glGetShaderiv(gVertexShaderObject,
							GL_INFO_LOG_LENGTH,
							&infoLogLength);

			if(infoLogLength>0)
			{
				szBuffer=(GLchar*)malloc(sizeof(GLchar) * infoLogLength);

				if(szBuffer!=NULL)
				{
					glGetShaderInfoLog(gFragmentShaderObject,
										infoLogLength,
										&written,
										szBuffer);
					
					fprintf(gpFile,"\nERROR:Fragment shader compilation failed log: %s",szBuffer);
					free(szBuffer);
					DestroyWindow(ghwnd);
				}
			}
		}
		
		infoLogLength=0;
		shaderCompileStatus=0;
		shaderProgramLinkStatus=0;
		szBuffer=NULL;
		written=0;

		gShaderProgramObject=glCreateProgram();

		glAttachShader(gShaderProgramObject,gVertexShaderObject);
		glAttachShader(gShaderProgramObject,gFragmentShaderObject);
		//Shader Attribute Binding Before Pre Linking of attached Shader
			glBindAttribLocation(gShaderProgramObject,VSV_ATTRIBUTE_POSITION,"vPosition");
			glBindAttribLocation(gShaderProgramObject,VSV_ATTRIBUTE_COLOR,"vColor");

		glLinkProgram(gShaderProgramObject);

		glGetProgramiv(gShaderProgramObject,
						GL_LINK_STATUS,
						&shaderProgramLinkStatus);
		if(shaderProgramLinkStatus==GL_FALSE)
		{
			glGetProgramiv(gShaderProgramObject,
						GL_INFO_LOG_LENGTH,
						&infoLogLength);
			if(infoLogLength>0)
			{
				szBuffer=(GLchar*)malloc(sizeof(GLchar)*infoLogLength);
				if(szBuffer!=NULL)
				{
					glGetProgramInfoLog(gShaderProgramObject,
										infoLogLength,
										&written,
										szBuffer);

					fprintf(gpFile,"\nERROR:Shader Program Link failed log: %s",szBuffer);
					free(szBuffer);
					DestroyWindow(ghwnd);
				}
			}
		}
		//Uniform variable Binding after post linking of attached shader
			mvpMatrixUniform=glGetUniformLocation(gShaderProgramObject,
												"u_mvpMatrix");

	//Shader Code End--------------------
	//VAO and its respective VBO preparation
		const GLfloat triangleVertices[]=
		{
			0.0f,1.0f,0.0f,
			-1.0f,-1.0f,0.0f,
			1.0f,-1.0f,0.0f
		};

		const GLfloat triangleColor[]=
		{
			1.0f, 0.0f, 0.0f,
			0.0f, 1.0f, 0.0f,
			0.0f, 0.0f, 1.0f
		};

		const GLfloat squareVertices[]=
		{
			1.0f, 1.0f, 0.0f,
			-1.0f, 1.0f, 0.0f,
			-1.0f, -1.0f, 0.0f,
			1.0f, -1.0f, 0.0f
		};
	
		glGenVertexArrays(1,&vao_triangle);
		glBindVertexArray(vao_triangle);

			glGenBuffers(1,&vbo_trianglePosition);
			glBindBuffer(GL_ARRAY_BUFFER,vbo_trianglePosition);

				glBufferData(GL_ARRAY_BUFFER,
							sizeof(triangleVertices),
							triangleVertices,
							GL_STATIC_DRAW);

				glVertexAttribPointer(VSV_ATTRIBUTE_POSITION,
									3,
									GL_FLOAT,
									GL_FALSE,
									0,
									NULL);

				glEnableVertexAttribArray(VSV_ATTRIBUTE_POSITION);
			
			glBindBuffer(GL_ARRAY_BUFFER,0);
			
			glGenBuffers(1,&vbo_triangleColor);
			glBindBuffer(GL_ARRAY_BUFFER,vbo_triangleColor);
				
				glBufferData(GL_ARRAY_BUFFER,
							sizeof(triangleColor),
							triangleColor,
							GL_STATIC_DRAW);
				
				glVertexAttribPointer(VSV_ATTRIBUTE_COLOR,
									3,
									GL_FLOAT,
									GL_FALSE,
									0,
									NULL);

				glEnableVertexAttribArray(VSV_ATTRIBUTE_COLOR);
			
			glBindBuffer(GL_ARRAY_BUFFER,0);

		glBindVertexArray(0);

		glGenVertexArrays(1,&vao_square);
		glBindVertexArray(vao_square);
			glGenBuffers(1,&vbo_squarePosition);
			glBindBuffer(GL_ARRAY_BUFFER,vbo_squarePosition);

				glBufferData(GL_ARRAY_BUFFER,
							sizeof(squareVertices),
							squareVertices,
							GL_STATIC_DRAW);

				glVertexAttribPointer(VSV_ATTRIBUTE_POSITION,
									3,
									GL_FLOAT,
									GL_FALSE,
									0,
									NULL);

				glEnableVertexAttribArray(VSV_ATTRIBUTE_POSITION);
			glBindBuffer(GL_ARRAY_BUFFER,0);

			glVertexAttrib3f(VSV_ATTRIBUTE_COLOR,0.0f,0.0f,1.0f);

		glBindVertexArray(0);


	glClearColor(0.0,0.0,0.0f,0.0f);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	perspectiveProjectionMatrix=mat4::identity();
	Resize(WIN_WIDTH,WIN_HEIGHT);

}

void Resize(int width,int height)
{
	if(height==0)
	{
		height=1;
	}
	glViewport(0,0,(GLsizei)width,(GLsizei)height);

	perspectiveProjectionMatrix=vmath::perspective(45.0f,
													(GLfloat)width/(GLfloat)height,
													0.1f,
													100.0f);
}

GLfloat triangleRotate=0.0f;
GLfloat squareRotate=0.0f;

void Display(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glUseProgram(gShaderProgramObject);
		mat4 modelViewMatrix;
		mat4 modelViewProjectionMatrix;
		mat4 translateMatrix;
		mat4 rotateMatrix;
		
		rotateMatrix=vmath::rotate(triangleRotate,0.0f,1.0f,0.0f);
		translateMatrix=vmath::translate(-1.5f,0.0f,-6.0f);
		
		modelViewMatrix=translateMatrix * rotateMatrix;

		modelViewProjectionMatrix=perspectiveProjectionMatrix * modelViewMatrix;

		glUniformMatrix4fv(mvpMatrixUniform,
						1,
						GL_FALSE,
						modelViewProjectionMatrix);

		glBindVertexArray(vao_triangle);
				glDrawArrays(GL_TRIANGLES,
								0,
								3);
		glBindVertexArray(0);

		rotateMatrix=vmath::rotate(squareRotate,1.0f,0.0f,0.0f);
		translateMatrix=vmath::translate(1.5f,0.0f,-6.0f);

		modelViewMatrix=translateMatrix * rotateMatrix;
		modelViewProjectionMatrix=perspectiveProjectionMatrix * modelViewMatrix;

		glUniformMatrix4fv(mvpMatrixUniform,
						1,
						GL_FALSE,
						modelViewProjectionMatrix);
		glBindVertexArray(vao_square);
			glDrawArrays(GL_TRIANGLE_FAN,
						0,
						4);
		glBindVertexArray(0);

	glUseProgram(0);

	SwapBuffers(ghdc);

	triangleRotate+=1.0f;
	squareRotate+=1.0f;
}

void UnInitialize(void)
{
	if(gbFullscreen==true)
	{
		SetWindowLong(ghwnd,GWL_STYLE,(dwStyle | WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(ghwnd,&wpPrev);
		SetWindowPos(ghwnd,HWND_TOP,0,0,0,0,SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED );
		fprintf(gpFile,"\nNormal Window");
		gbFullscreen=false;
	}

	if(vao_square)
	{
		glDeleteVertexArrays(1,
							&vao_square);
	}
	vao_square=0;

	if(vao_triangle)
	{
		glDeleteVertexArrays(1,
							&vao_triangle);
	}
	vao_triangle=0;

	if(vbo_triangleColor){
		glDeleteBuffers(1,
						&vbo_triangleColor);
	}
	vbo_triangleColor=0;

	if(vbo_trianglePosition)
	{
		glDeleteBuffers(1,
					&vbo_trianglePosition);
	}
	vbo_trianglePosition=0;

	if(vbo_squarePosition)
	{
		glDeleteBuffers(1,
					&vbo_squarePosition);
	}
	vbo_squarePosition=0;


	if(gShaderProgramObject)
	{
		glUseProgram(gShaderProgramObject);

		GLsizei shaderCount;
		GLsizei tempShaderCount;
		GLuint *pShader=NULL;

		glGetProgramiv(gShaderProgramObject,GL_ATTACHED_SHADERS,&shaderCount);

		pShader=(GLuint *)malloc(sizeof(GLuint)*shaderCount);
		if(pShader==NULL)
		{
			fprintf(gpFile,"\nFailed to allocate memory for pShader");
		}else{
			glGetAttachedShaders(gShaderProgramObject,shaderCount,&tempShaderCount,pShader);
			
			fprintf(gpFile,"\nShader program has actual attached shader count is:%d",tempShaderCount);
			
			for(GLsizei i=0;i<shaderCount;i++)
			{
				glDetachShader(gShaderProgramObject,pShader[i]);
				glDeleteShader(pShader[i]);
				pShader[i]=0;
			}
			free(pShader);
			
			glDeleteProgram(gShaderProgramObject);
			gShaderProgramObject=0;
			glUseProgram(0);
		}
	}

	if(wglGetCurrentContext()==ghrc)
	{
		wglMakeCurrent(NULL,NULL);
	}

	if(ghrc)
	{
		wglDeleteContext(ghrc);
	}
		ghrc=NULL;

	if(ghdc)
	{
		ReleaseDC(ghwnd,ghdc);
	}
		ghdc=NULL;
	
	if(gpFile)
	{
		fprintf(gpFile,"\nlog is ended!");
		fclose(gpFile);
	}
		gpFile=NULL;
}