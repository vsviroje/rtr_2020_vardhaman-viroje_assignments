#include<windows.h>
#include<stdio.h>
#include"toggle_perVertex_perFragment.h"
#include "Sphere.h"

#include<GL/GLEW.h>
#include<GL/GL.h>

#include"vmath.h"

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

#pragma comment(lib,"GLEW32.lib")
#pragma comment(lib,"OpenGL32.lib")
#pragma comment(lib,"Sphere.lib")

using namespace vmath;

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

FILE* gpFile = NULL;
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
bool gbFullscreen = false;
bool gbActiveWindow = false;
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

enum{
	VSV_ATTRIBUTE_POSITION=0,
	VSV_ATTRIBUTE_COLOR,
	VSV_ATTRIBUTE_NORMAL,
	VSV_ATTRIBUTE_TEXCOORD,
};

GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;

GLuint gPV_ShaderProgramObject;
GLuint gPF_ShaderProgramObject;

GLuint vaoSphere;
GLuint vboSphere_position;
GLuint vboSphere_normal;
GLuint vboSphere_elements;

//-------------------------------------
GLuint PV_modelMatrixUniform;
GLuint PV_viewMatrixUniform;
GLuint PV_projectionlMatrixUniform;

GLuint PV_lightAmbientUniform;
GLuint PV_lightDiffuseUniform;
GLuint PV_lightSpecularUniform;
GLuint PV_lightPositionUniform;

GLuint PV_materialAmbientUniform;
GLuint PV_materialDiffuseUniform;
GLuint PV_materialSpecularUniform;
GLuint PV_materialShininessUniform;

GLuint PV_LKeyPressedUniform;

//------------------------------
GLuint PF_modelMatrixUniform;
GLuint PF_viewMatrixUniform;
GLuint PF_projectionlMatrixUniform;

GLuint PF_lightAmbientUniform;
GLuint PF_lightDiffuseUniform;
GLuint PF_lightSpecularUniform;
GLuint PF_lightPositionUniform;

GLuint PF_materialAmbientUniform;
GLuint PF_materialDiffuseUniform;
GLuint PF_materialSpecularUniform;
GLuint PF_materialShininessUniform;

GLuint PF_LKeyPressedUniform;

//-------------------

mat4 perspectiveProjectionMatrix;

bool bLights;
bool bTogglePVxOrPF;//True-Per Fragment and False-Per Vertex

float spherePosition[1146];
float sphereNormals[1146];
float sphereTexcoord[764];
unsigned short sphereElement[2280];//Face Indices

GLfloat LightAmbient[] = { 0.1f,0.1f,0.1f,1.0f };
GLfloat LightDefuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat LightSpecular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat LightPosition[] = { 100.0f,100.0f,100.0f,1.0f };

GLfloat materialAmbient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat materilaDefuse[] = { 0.5f,0.2f,0.7f,1.0f };
GLfloat materialSpecular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat materialShininess = 128.0f;

/*
GLfloat LightAmbient[]={0.0f,0.0f,0.0f,1.0f};//White Light
GLfloat LightDefuse[]={1.0f,1.0f,1.0f,1.0f};
GLfloat LightSpecular[]={1.0f,1.0f,1.0f,1.0f};
GLfloat LightPosition[]={100.0f,100.0f,100.0f,1.0f};

GLfloat materialAmbient[]={0.0f,0.0f,0.0f,1.0f};
GLfloat materilaDefuse[]={1.0f,1.0f,1.0f,1.0f};
GLfloat materialSpecular[]={1.0f,1.0f,1.0f,1.0f};
GLfloat materialShininess=50.0f;
*/

GLuint gNumSphereVertices, gNumSphereElements;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	void Initialize(void);
	void Display(void);

	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("ToggleLightingSphere");
	int X1, X2, Y1, Y2;
	bool bDone = false;

	if (fopen_s(&gpFile, "log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Failed to Create log file!"), TEXT("Error"), MB_OK | MB_ICONERROR);
		exit(0);
	}
	
	X1 = GetSystemMetrics(SM_CXSCREEN) / 2;
	Y1 = GetSystemMetrics(SM_CYSCREEN) / 2;

	X2 = WIN_WIDTH / 2;
	Y2 = WIN_HEIGHT / 2;

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style =  CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(GRAY_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));

	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("Toggle Lighting type Sphere(PerVertex/PerFragment)"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		X1 - X2,//100,
		Y1 - Y2,//100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;
	Initialize();

	ShowWindow(hwnd, iCmdShow);

	SetForegroundWindow(hwnd);
	SetFocus(hwnd);
	


	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
		
			if (gbActiveWindow == true)
			{
				Display();
			}
		}
	}

	return ((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void ToggleFullscreen(void);
	void Resize(int, int);
	void UnInitialize(void);

	switch (iMsg)
	{
	case WM_CREATE:
		fprintf(gpFile, "\nlog is Started!");
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
			case VK_ESCAPE:
				ToggleFullscreen();
				break;
			case 0x46://per Fragment 
			case 0x66://f,F
				bTogglePVxOrPF = true;
				break;
			case 'v':
			case 'V':
				bTogglePVxOrPF = false;
				break;
			case 'l':
			case 'L':
				bLights = !bLights;
				if (bLights)
				{
					bTogglePVxOrPF = false;
				}
				break;
			case 'q':
			case 'Q':
				DestroyWindow(hwnd);
				break;
			default:
				break;
		}
		break;
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;
	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;

	case WM_ERASEBKGND:
		return 0;
	case WM_SIZE:
		Resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;
	case WM_DESTROY:
		UnInitialize();
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}
void ToggleFullscreen(void)
{
	MONITORINFO mi = { sizeof(MONITORINFO) };
	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW) 
		{
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				fprintf(gpFile, "\nFullscreen Enabled");
				SetWindowLong(ghwnd, GWL_STYLE, (dwStyle & ~WS_OVERLAPPEDWINDOW));
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
		gbFullscreen = true;
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, (dwStyle | WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		fprintf(gpFile, "\nNormal Window Enabled");
		ShowCursor(TRUE);
		gbFullscreen = false;
	}
}

void Initialize(void)
{
	void Resize(int, int);
	
	//Local variable initialization
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	ghdc = GetDC(ghwnd);

	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER ;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits= 32 ;


	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (iPixelFormatIndex == 0 )
	{
		fprintf(gpFile, "\nChoosePixelFormat() Failed to get pixel format index");
		DestroyWindow(ghwnd);
	}

	if ( SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE )
	{
		fprintf(gpFile, "\nSetPixelFormat() Failed to set pixel format for window");
		DestroyWindow(ghwnd);
	}

	ghrc = wglCreateContext(ghdc);
	if ( ghrc == NULL)
	{
		fprintf(gpFile, "\nwglCreateContext() Failed to get the rendering context");
		DestroyWindow(ghwnd);
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE )
	{
		fprintf(gpFile, "\nwglMakeCurrent() Failed to switch current context from device to rendering context");
		DestroyWindow(ghwnd);
	}

	GLenum glew_error=glewInit();
	if(glew_error != GLEW_OK)
	{
		fprintf(gpFile, "\nglewInit() failed to enable extension for Programmable Pipeline");
		DestroyWindow(ghwnd);

	}
	fprintf(gpFile, "\nOpenGL Information are availble below:-");
	fprintf(gpFile, "\n*****************************************************");

	fprintf(gpFile, "\nOpenGL Vender:%s",glGetString(GL_VENDOR));
	fprintf(gpFile, "\nOpenGL Renderer:%s",glGetString(GL_RENDERER));
	fprintf(gpFile, "\nOpenGL Version:%s",glGetString(GL_VERSION));
	fprintf(gpFile, "\nOpenGL GLSL Version:%s",glGetString(GL_SHADING_LANGUAGE_VERSION));
	
	fprintf(gpFile, "\nOpenGL Extension's list:\n");
	GLint numExt;
	glGetIntegerv(GL_NUM_EXTENSIONS,&numExt);

	for(int i=0;i<numExt;i++)
	{
		fprintf(gpFile, "\n%s",glGetStringi(GL_EXTENSIONS,i));
	}
	fprintf(gpFile, "\n*****************************************************");

	GLint infoLogLength;
	GLint shaderCompileStatus;
	GLint shaderProgramLinkStatus;
	GLchar * szBuffer=NULL;
	GLsizei written;


	//Per Vertex Shader Code Start--------------------
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	const GLchar* vertexShaderSourceCode_PV =
		"#version 460 core\n"\
		"\n"\
		"in vec4 vPosition;\n"\
		"in vec3 vNormal;\n"\

		"uniform mat4 u_modelMatrix;\n"\
		"uniform mat4 u_viewMatrix;\n"\
		"uniform mat4 u_projectionMatrix;\n"\

		"uniform vec3 u_lightAmbient;\n"\
		"uniform vec3 u_lightDiffuse;\n"\
		"uniform vec3 u_lightSpecular;\n"\
		"uniform vec4 u_lightPosition;\n"\

		"uniform vec3 u_materialAmbient;\n"\
		"uniform vec3 u_materialDiffuse;\n"\
		"uniform vec3 u_materialSpecular;\n"\
		"uniform float u_materialShininess;\n"\

		"uniform int u_lKeyPressed;\n"\

		"out vec3 out_phongADS_Light;\n"\

		"void main(void)\n"\

		"{\n"\
		"if (u_lKeyPressed == 1)\n"\
		"{\n"\

		"vec4 eyeCoord = u_viewMatrix * u_modelMatrix * vPosition;\n"\

		"vec3 transformedNormal = normalize( mat3(u_viewMatrix * u_modelMatrix) * vNormal );\n"\

		"vec3 lightDirection = normalize( vec3( u_lightPosition - eyeCoord ) );\n"\

		"vec3 reflectionVector = reflect( -lightDirection, transformedNormal );\n"\

		"vec3 viewVector = normalize( -eyeCoord.xyz);\n"\

		"vec3 ambient = u_lightAmbient * u_materialAmbient;\n"\

		"vec3 diffuse = u_lightDiffuse * u_materialDiffuse * max( dot(lightDirection , transformedNormal), 0.0);\n"\

		"vec3 specular = u_lightSpecular * u_materialSpecular * pow( max( dot(reflectionVector , viewVector), 0.0f) , u_materialShininess);\n"\

		"out_phongADS_Light= ambient+ diffuse + specular;\n"\

		"}\n"\
		"else\n"\
		"{\n"\

		"out_phongADS_Light = vec3(1.0f, 1.0f, 1.0f);\n"\

		"}\n"\

		"gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix * vPosition;\n"\

		"}";

	glShaderSource(gVertexShaderObject,
		1,
		(const GLchar**)&vertexShaderSourceCode_PV,
		NULL);

	glCompileShader(gVertexShaderObject);

	glGetShaderiv(gVertexShaderObject,
		GL_COMPILE_STATUS,
		&shaderCompileStatus);

	if (shaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject,
			GL_INFO_LOG_LENGTH,
			&infoLogLength);

		if (infoLogLength > 0)
		{
			szBuffer = (GLchar*)malloc(sizeof(GLchar) * infoLogLength);
			if (szBuffer != NULL)
			{
				glGetShaderInfoLog(gVertexShaderObject,
					infoLogLength,
					&written,
					szBuffer);
				fprintf(gpFile, "\nERROR:Vertex Shader Object Compilation Failed log : %s", szBuffer);
				free(szBuffer);
				DestroyWindow(ghwnd);
			}
		}
	}

	infoLogLength = 0;
	shaderCompileStatus = 0;
	shaderProgramLinkStatus = 0;
	szBuffer = NULL;
	written = 0;

	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	const GLchar* fragmentShaderSourceCode_PV =
		"#version 460 core"\
		"\n"\
		"in vec3 out_phongADS_Light;\n"
		"out vec4 fragColor;"\
		"void main(void)"\
		"{"\
		"fragColor = vec4( out_phongADS_Light, 1.0f );"\
		"}";

	glShaderSource(gFragmentShaderObject,
		1,
		(const GLchar**)&fragmentShaderSourceCode_PV,
		NULL);

	glCompileShader(gFragmentShaderObject);

	glGetShaderiv(gFragmentShaderObject,
		GL_COMPILE_STATUS,
		&shaderCompileStatus);

	if (shaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject,
			GL_INFO_LOG_LENGTH,
			&infoLogLength);

		if (infoLogLength > 0)
		{
			szBuffer = (GLchar*)malloc(sizeof(GLchar) * infoLogLength);
			if (szBuffer != NULL)
			{
				glGetShaderInfoLog(gFragmentShaderObject,
					infoLogLength,
					&written,
					szBuffer);
				fprintf(gpFile, "\nERROR:Fragment Shader Object Compilation Failed log : %s", szBuffer);
				free(szBuffer);
				DestroyWindow(ghwnd);
			}
		}
	}

	infoLogLength = 0;
	shaderCompileStatus = 0;
	shaderProgramLinkStatus = 0;
	szBuffer = NULL;
	written = 0;

	gPV_ShaderProgramObject = glCreateProgram();

	glAttachShader(gPV_ShaderProgramObject, gVertexShaderObject);
	glAttachShader(gPV_ShaderProgramObject, gFragmentShaderObject);

	//Shader Attribute Binding Before Pre Linking of attached Shader
	glBindAttribLocation(gPV_ShaderProgramObject,
		VSV_ATTRIBUTE_POSITION,
		"vPosition");
	glBindAttribLocation(gPV_ShaderProgramObject,
		VSV_ATTRIBUTE_NORMAL,
		"vNormal");

	glLinkProgram(gPV_ShaderProgramObject);

	glGetProgramiv(gPV_ShaderProgramObject,
		GL_LINK_STATUS,
		&shaderProgramLinkStatus);

	if (shaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gPV_ShaderProgramObject,
			GL_INFO_LOG_LENGTH,
			&infoLogLength);
		if (infoLogLength > 0)
		{
			szBuffer = (GLchar*)malloc(sizeof(GLchar) * infoLogLength);

			if (szBuffer != NULL)
			{
				glGetProgramInfoLog(gPV_ShaderProgramObject,
					infoLogLength,
					&written,
					szBuffer);

				fprintf(gpFile, "\nERROR:Shader program Link failed log : %s", szBuffer);
				free(szBuffer);
				DestroyWindow(ghwnd);
			}
		}
	}

	//Shader Code End--------------------

	//Uniform variable Binding after post linking of attached shader 
	PV_modelMatrixUniform = glGetUniformLocation(gPV_ShaderProgramObject, "u_modelMatrix");
	PV_viewMatrixUniform = glGetUniformLocation(gPV_ShaderProgramObject, "u_viewMatrix");
	PV_projectionlMatrixUniform = glGetUniformLocation(gPV_ShaderProgramObject, "u_projectionMatrix");

	PV_lightAmbientUniform = glGetUniformLocation(gPV_ShaderProgramObject, "u_lightAmbient");
	PV_lightDiffuseUniform = glGetUniformLocation(gPV_ShaderProgramObject, "u_lightDiffuse");
	PV_lightSpecularUniform = glGetUniformLocation(gPV_ShaderProgramObject, "u_lightSpecular");
	PV_lightPositionUniform = glGetUniformLocation(gPV_ShaderProgramObject, "u_lightPosition");

	PV_materialAmbientUniform = glGetUniformLocation(gPV_ShaderProgramObject, "u_materialAmbient");
	PV_materialDiffuseUniform = glGetUniformLocation(gPV_ShaderProgramObject, "u_materialDiffuse");
	PV_materialSpecularUniform = glGetUniformLocation(gPV_ShaderProgramObject, "u_materialSpecular");
	PV_materialShininessUniform = glGetUniformLocation(gPV_ShaderProgramObject, "u_materialShininess");

	PV_LKeyPressedUniform = glGetUniformLocation(gPV_ShaderProgramObject, "u_lKeyPressed");


	//###########################################################################################
	//###########################################################################################

	//Per Fragment Shader Code Start--------------------
		gVertexShaderObject=glCreateShader(GL_VERTEX_SHADER);

		const GLchar *vertexShaderSourceCode_PF=
		"#version 460 core\n"\
		"\n"\
		"in vec4 vPosition;\n"\
		"in vec3 vNormal;\n"\

		"uniform mat4 u_modelMatrix;\n"\
		"uniform mat4 u_viewMatrix;\n"\
		"uniform mat4 u_projectionMatrix;\n"\

		"uniform vec4 u_lightPosition;\n"\
		
		"uniform int u_lKeyPressed;\n"\
		
		"out vec3 out_phongADS_Light;\n"\
		
		"out vec3 out_transformedNormal;\n"\
		"out vec3 out_lightDirection;\n"\
		"out vec3 out_viewVector;\n"\

		"void main(void)\n"\

		"{\n"\
			"if (u_lKeyPressed == 1)\n"\
			"{\n"\

				"vec4 eyeCoord = u_viewMatrix * u_modelMatrix * vPosition;\n"\
				
				"vec3 transformedNormal = mat3(u_viewMatrix * u_modelMatrix) * vNormal ;\n"\
				"out_transformedNormal = transformedNormal;\n"\

				"vec3 lightDirection = vec3( u_lightPosition - eyeCoord ) ;\n"\
				"out_lightDirection = lightDirection;\n"\
				
				"vec3 viewVector =  -eyeCoord.xyz ;\n"\
				"out_viewVector = viewVector;\n"\

			"}\n"\
			
			"gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix * vPosition;\n"\
		
		"}";

		glShaderSource(gVertexShaderObject,
						1,
						(const GLchar**)&vertexShaderSourceCode_PF,
						NULL);
		
		glCompileShader(gVertexShaderObject);

		glGetShaderiv(gVertexShaderObject,
						GL_COMPILE_STATUS,
						&shaderCompileStatus);

		if(shaderCompileStatus==GL_FALSE)
		{
			glGetShaderiv(gVertexShaderObject,
						GL_INFO_LOG_LENGTH,
						&infoLogLength);
			
			if(infoLogLength > 0)
			{
				szBuffer=(GLchar*)malloc(sizeof(GLchar)*infoLogLength);
				if(szBuffer!=NULL)
				{
					glGetShaderInfoLog(gVertexShaderObject,
										infoLogLength,
										&written,
										szBuffer);
					fprintf(gpFile,"\nERROR:Vertex Shader Object Compilation Failed log : %s",szBuffer);
					free(szBuffer);
					DestroyWindow(ghwnd);
				}
			}
		}

		infoLogLength=0;
		shaderCompileStatus=0;
		shaderProgramLinkStatus=0;
		szBuffer=NULL;
		written=0;

		gFragmentShaderObject=glCreateShader(GL_FRAGMENT_SHADER);

		const GLchar *fragmentShaderSourceCode_PF=
		"#version 460 core\n"\
		"\n"\
		"vec3 phongADS_Light;\n"
		"out vec4 fragColor;\n"\

		"in vec3 out_transformedNormal;\n"\
		"in vec3 out_lightDirection;\n"\
		"in vec3 out_viewVector;\n"\

		"uniform vec3 u_lightAmbient;\n"\
		"uniform vec3 u_lightDiffuse;\n"\
		"uniform vec3 u_lightSpecular;\n"\

		"uniform vec3 u_materialAmbient;\n"\
		"uniform vec3 u_materialDiffuse;\n"\
		"uniform vec3 u_materialSpecular;\n"\
		"uniform float u_materialShininess;\n"\

		"uniform int u_lKeyPressed;\n"\

		"void main(void)\n"\
		"{"\
			"if (u_lKeyPressed == 1)\n"\
			"{\n"\
				"vec3 normalizedTransformedNormal = normalize( out_transformedNormal );\n"\
				"vec3 normalizedLightDirection = normalize( out_lightDirection );\n"\
				"vec3 normalizedViewVector = normalize( out_viewVector );\n"\

				"vec3 reflectionVector = reflect( -normalizedLightDirection, normalizedTransformedNormal );\n"\
				
				"vec3 ambient = u_lightAmbient * u_materialAmbient;\n"\

				"vec3 diffuse = u_lightDiffuse * u_materialDiffuse * max( dot(normalizedLightDirection , normalizedTransformedNormal), 0.0);\n"\

				"vec3 specular = u_lightSpecular * u_materialSpecular * pow( max( dot(reflectionVector , normalizedViewVector), 0.0f) , u_materialShininess);\n"\

				"phongADS_Light= ambient+ diffuse + specular;\n"\
				
			"}\n"\
			"else\n"\
			"{\n"\

				"phongADS_Light=vec3(1.0f, 1.0f, 1.0f);\n"\
			
			"}\n"\
			
			"fragColor = vec4( phongADS_Light, 1.0f );\n"\

		"}";

		glShaderSource(gFragmentShaderObject,
						1,
						(const GLchar**)&fragmentShaderSourceCode_PF,
						NULL);
		
		glCompileShader(gFragmentShaderObject);

		glGetShaderiv(gFragmentShaderObject,
					GL_COMPILE_STATUS,
					&shaderCompileStatus);

		if(shaderCompileStatus==GL_FALSE)
		{
			glGetShaderiv(gFragmentShaderObject,
						GL_INFO_LOG_LENGTH,
						&infoLogLength);
			
			if(infoLogLength > 0)
			{
				szBuffer=(GLchar*)malloc(sizeof(GLchar)*infoLogLength);
				if(szBuffer!=NULL)
				{
					glGetShaderInfoLog(gFragmentShaderObject,
										infoLogLength,
										&written,
										szBuffer);
					fprintf(gpFile,"\nERROR:Fragment Shader Object Compilation Failed log : %s",szBuffer);
					free(szBuffer);
					DestroyWindow(ghwnd);
				}
			}
		}

		infoLogLength=0;
		shaderCompileStatus=0;
		shaderProgramLinkStatus=0;
		szBuffer=NULL;
		written=0;

		gPF_ShaderProgramObject =glCreateProgram();

		glAttachShader(gPF_ShaderProgramObject,gVertexShaderObject);
		glAttachShader(gPF_ShaderProgramObject,gFragmentShaderObject);

		//Shader Attribute Binding Before Pre Linking of attached Shader
			glBindAttribLocation(gPF_ShaderProgramObject,
								VSV_ATTRIBUTE_POSITION,
								"vPosition");
			glBindAttribLocation(gPF_ShaderProgramObject,
								VSV_ATTRIBUTE_NORMAL,
								"vNormal");

		glLinkProgram(gPF_ShaderProgramObject);

		glGetProgramiv(gPF_ShaderProgramObject,
						GL_LINK_STATUS,
						&shaderProgramLinkStatus);
		
		if(shaderProgramLinkStatus==GL_FALSE)
		{
			glGetProgramiv(gPF_ShaderProgramObject,
							GL_INFO_LOG_LENGTH,
							&infoLogLength);
			if(infoLogLength > 0)
			{
				szBuffer=(GLchar *)malloc(sizeof(GLchar)*infoLogLength);

				if(szBuffer!=NULL)
				{
					glGetProgramInfoLog(gPF_ShaderProgramObject,
										infoLogLength,
										&written,
										szBuffer);

					fprintf(gpFile,"\nERROR:Shader program Link failed log : %s",szBuffer);
					free(szBuffer);
					DestroyWindow(ghwnd);
				}
			}
		}

	//Shader Code End--------------------

	//Uniform variable Binding after post linking of attached shader 
		PF_modelMatrixUniform = glGetUniformLocation(gPF_ShaderProgramObject,"u_modelMatrix");
		PF_viewMatrixUniform = glGetUniformLocation(gPF_ShaderProgramObject,"u_viewMatrix");
		PF_projectionlMatrixUniform = glGetUniformLocation(gPF_ShaderProgramObject,"u_projectionMatrix");

		PF_lightAmbientUniform = glGetUniformLocation(gPF_ShaderProgramObject,"u_lightAmbient");
		PF_lightDiffuseUniform = glGetUniformLocation(gPF_ShaderProgramObject,"u_lightDiffuse");
		PF_lightSpecularUniform = glGetUniformLocation(gPF_ShaderProgramObject,"u_lightSpecular");
		PF_lightPositionUniform = glGetUniformLocation(gPF_ShaderProgramObject,"u_lightPosition");

		PF_materialAmbientUniform = glGetUniformLocation(gPF_ShaderProgramObject,"u_materialAmbient");
		PF_materialDiffuseUniform = glGetUniformLocation(gPF_ShaderProgramObject,"u_materialDiffuse");
		PF_materialSpecularUniform = glGetUniformLocation(gPF_ShaderProgramObject,"u_materialSpecular");
		PF_materialShininessUniform = glGetUniformLocation(gPF_ShaderProgramObject,"u_materialShininess");

		PF_LKeyPressedUniform = glGetUniformLocation(gPF_ShaderProgramObject,"u_lKeyPressed");

	//VAO and its respective VBO preparation	

		getSphereVertexData(spherePosition, sphereNormals, sphereTexcoord, sphereElement);
		gNumSphereVertices = getNumberOfSphereVertices();
		gNumSphereElements = getNumberOfSphereElements();

		glGenVertexArrays(1,
						&vaoSphere);

		glBindVertexArray(vaoSphere);

			glGenBuffers(1,
						&vboSphere_position);

			glBindBuffer(GL_ARRAY_BUFFER,
						vboSphere_position);

				glBufferData(GL_ARRAY_BUFFER,
							sizeof(spherePosition),
							spherePosition,
							GL_STATIC_DRAW);
				
				glVertexAttribPointer(VSV_ATTRIBUTE_POSITION,
									3,
									GL_FLOAT,
									GL_FALSE,
									0,
									NULL);

				glEnableVertexAttribArray(VSV_ATTRIBUTE_POSITION);
			
			glBindBuffer(GL_ARRAY_BUFFER,
						0);
			
			glGenBuffers(1,
						&vboSphere_normal);

			glBindBuffer(GL_ARRAY_BUFFER,
						vboSphere_normal);

				glBufferData(GL_ARRAY_BUFFER,
								sizeof(sphereNormals),
								sphereNormals,
								GL_STATIC_DRAW);

				glVertexAttribPointer(VSV_ATTRIBUTE_NORMAL,
					3,
					GL_FLOAT,
					GL_FALSE,
					0,
					NULL);

				glEnableVertexAttribArray(VSV_ATTRIBUTE_NORMAL);

			glBindBuffer(GL_ARRAY_BUFFER,
				0);

			glGenBuffers(1,
				&vboSphere_normal);

			glBindBuffer(GL_ARRAY_BUFFER,
				vboSphere_normal);

				glBufferData(GL_ARRAY_BUFFER,
					sizeof(sphereNormals),
					sphereNormals,
					GL_STATIC_DRAW);

				glVertexAttribPointer(VSV_ATTRIBUTE_NORMAL,
					3,
					GL_FLOAT,
					GL_FALSE,
					0,
					NULL);

				glEnableVertexAttribArray(VSV_ATTRIBUTE_NORMAL);

			glBindBuffer(GL_ARRAY_BUFFER,
				0);


			glGenBuffers(1,
				&vboSphere_elements);

			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,
				vboSphere_elements);

				glBufferData(GL_ELEMENT_ARRAY_BUFFER,
					sizeof(sphereElement),
					sphereElement,
					GL_STATIC_DRAW);

			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,
				0);

		glBindVertexArray(0);


	//SetColor
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	glShadeModel(GL_SMOOTH);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT,GL_NICEST);

	//Warmup resize call
	perspectiveProjectionMatrix=mat4::identity(); 
	
	Resize(WIN_WIDTH, WIN_HEIGHT);
	
}

void Resize(int width, int height)
{
	if (height == 0)
	{
		height = 1;
	}
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	
	perspectiveProjectionMatrix=vmath::perspective(45.0f,
													(GLfloat)width/(GLfloat)height,
													0.1f,
													100.0f);

}


void Display(void)
{
	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

	mat4 translateMatrix;

	mat4 modelMatrix;
	mat4 viewMatrix = mat4::identity();

	translateMatrix = vmath::translate(0.0f, 0.0f, -1.5f);
	modelMatrix = translateMatrix;

	if (bTogglePVxOrPF)
	{
		glUseProgram(gPF_ShaderProgramObject);


		glUniformMatrix4fv(PF_modelMatrixUniform,
			1,
			GL_FALSE,
			modelMatrix);

		glUniformMatrix4fv(PF_viewMatrixUniform,
			1,
			GL_FALSE,
			viewMatrix);

		glUniformMatrix4fv(PF_projectionlMatrixUniform,
			1,
			GL_FALSE,
			perspectiveProjectionMatrix);

		if (bLights == true)
		{
			glUniform1i(PF_LKeyPressedUniform, 1);

			glUniform3fv(PF_lightAmbientUniform, 1, LightAmbient);
			glUniform3fv(PF_lightDiffuseUniform, 1, LightDefuse);
			glUniform3fv(PF_lightSpecularUniform, 1, LightSpecular);
			glUniform4fv(PF_lightPositionUniform, 1, LightPosition);//posiitional light

			glUniform3fv(PF_materialAmbientUniform, 1, materialAmbient);
			glUniform3fv(PF_materialDiffuseUniform, 1, materilaDefuse);
			glUniform3fv(PF_materialSpecularUniform, 1, materialSpecular);

			glUniform1f(PF_materialShininessUniform, materialShininess);
		}
		else
		{
			glUniform1i(PF_LKeyPressedUniform, 0);
		}

	}
	else 
	{
		glUseProgram(gPV_ShaderProgramObject);


		glUniformMatrix4fv(PV_modelMatrixUniform,
			1,
			GL_FALSE,
			modelMatrix);

		glUniformMatrix4fv(PV_viewMatrixUniform,
			1,
			GL_FALSE,
			viewMatrix);

		glUniformMatrix4fv(PV_projectionlMatrixUniform,
			1,
			GL_FALSE,
			perspectiveProjectionMatrix);

		if (bLights == true)
		{
			glUniform1i(PV_LKeyPressedUniform, 1);

			glUniform3fv(PV_lightAmbientUniform, 1, LightAmbient);
			glUniform3fv(PV_lightDiffuseUniform, 1, LightDefuse);
			glUniform3fv(PV_lightSpecularUniform, 1, LightSpecular);
			glUniform4fv(PV_lightPositionUniform, 1, LightPosition);//posiitional light

			glUniform3fv(PV_materialAmbientUniform, 1, materialAmbient);
			glUniform3fv(PV_materialDiffuseUniform, 1, materilaDefuse);
			glUniform3fv(PV_materialSpecularUniform, 1, materialSpecular);

			glUniform1f(PV_materialShininessUniform, materialShininess);
		}
		else
		{
			glUniform1i(PV_LKeyPressedUniform, 0);
		}

	}

	glBindVertexArray(vaoSphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboSphere_elements);
	glDrawElements(GL_TRIANGLES, gNumSphereElements, GL_UNSIGNED_SHORT, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	glUseProgram(0);

	
	SwapBuffers(ghdc);
	
}
void UnInitialize(void)
{
	if (gbFullscreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, (dwStyle | WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}
	
	if(vaoSphere)
	{
		glDeleteVertexArrays(1,
							&vaoSphere);
	}
	vaoSphere =0;

	if(vboSphere_position){
		glDeleteBuffers(1,
						&vboSphere_position);
	}
	vboSphere_position =0;

	if(vboSphere_normal)
	{
		glDeleteBuffers(1,
					&vboSphere_normal);
	}
	vboSphere_normal =0;

	if (vboSphere_elements)
	{
		glDeleteBuffers(1,
			&vboSphere_elements);
	}
	vboSphere_elements = 0;

	if(gPF_ShaderProgramObject)
	{
		glUseProgram(gPF_ShaderProgramObject);

			GLsizei shaderCount;
			GLsizei tempShaderCount;
			GLuint *pShader=NULL;

			glGetProgramiv(gPF_ShaderProgramObject,
							GL_ATTACHED_SHADERS,
							&shaderCount);

			pShader=(GLuint *)malloc(sizeof(GLuint)*shaderCount);
			if(pShader==NULL)
			{
				fprintf(gpFile,"\nFailed to allocate memory for pShader");
			}else{
				glGetAttachedShaders(gPF_ShaderProgramObject,
									shaderCount,
									&tempShaderCount,
									pShader);
				
				fprintf(gpFile,"\nShader program has actual attached shader count is:%d",tempShaderCount);
				
				for(GLsizei i=0;i<shaderCount;i++)
				{
					glDetachShader(gPF_ShaderProgramObject,
									pShader[i]);

					glDeleteShader(pShader[i]);
					
					pShader[i]=0;
				}
				free(pShader);
				
				glDeleteProgram(gPF_ShaderProgramObject);
				
				gPF_ShaderProgramObject = 0;
			}
		glUseProgram(0);
	}

	if (gPV_ShaderProgramObject)
	{
		glUseProgram(gPV_ShaderProgramObject);

		GLsizei shaderCount;
		GLsizei tempShaderCount;
		GLuint* pShader = NULL;

		glGetProgramiv(gPV_ShaderProgramObject,
			GL_ATTACHED_SHADERS,
			&shaderCount);

		pShader = (GLuint*)malloc(sizeof(GLuint) * shaderCount);
		if (pShader == NULL)
		{
			fprintf(gpFile, "\nFailed to allocate memory for pShader");
		}
		else {
			glGetAttachedShaders(gPV_ShaderProgramObject,
				shaderCount,
				&tempShaderCount,
				pShader);

			fprintf(gpFile, "\nShader program has actual attached shader count is:%d", tempShaderCount);

			for (GLsizei i = 0; i < shaderCount; i++)
			{
				glDetachShader(gPV_ShaderProgramObject,
					pShader[i]);

				glDeleteShader(pShader[i]);

				pShader[i] = 0;
			}
			free(pShader);

			glDeleteProgram(gPV_ShaderProgramObject);

			gPV_ShaderProgramObject = 0;
		}
		glUseProgram(0);
	}


	if (wglGetCurrentContext() == ghrc ) {
		wglMakeCurrent(NULL, NULL);
	}


	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (gpFile)
	{
		fprintf(gpFile, "\nlog is Ended!");
		fclose(gpFile);
		gpFile = NULL;
	}
}



















