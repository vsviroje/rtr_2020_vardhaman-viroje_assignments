#include<windows.h>
#include<stdio.h>
#include"Light_24_sphere.h"
#include "vmath.h"
#include<GL/GLEW.h>
#include<GL/GL.h>
#include<math.h>
#include "Sphere.h"

#define WIN_WIDTH 800
#define WIN_HEIGHT 800

#pragma comment(lib,"GLEW32.lib")
#pragma comment(lib,"OpenGL32.lib")
#pragma comment(lib,"Sphere.lib")

using namespace vmath;

LRESULT CALLBACK WndProc(HWND,UINT,WPARAM,LPARAM);

FILE* gpFile=NULL;
DWORD dwStyle;
WINDOWPLACEMENT wpPrev={sizeof(WINDOWPLACEMENT)};
bool gbFullscreen=false;
bool gbActiveWindow=false;
HWND ghwnd=NULL;
HDC ghdc=NULL;
HGLRC ghrc=NULL;

enum{
	VSV_ATTRIBUTE_POSITION=0,
	VSV_ATTRIBUTE_COLOR,
	VSV_ATTRIBUTE_TEXCOORD,
	VSV_ATTRIBUTE_NORMAL,
};

int RotationType;//x-0,y-1,z-2

GLsizei orignal_width;
GLsizei orignal_height;


//-------------PF---------------
GLuint gShaderProgramObject;

GLuint modelMatrixUniform;
GLuint viewMatrixUniform;
GLuint projectionlMatrixUniform;

GLuint lightAmbientUniform;
GLuint lightDiffuseUniform;
GLuint lightSpecularUniform;
GLuint lightPositionUniform;

GLuint materialAmbientUniform;
GLuint materialDiffuseUniform;
GLuint materialSpecularUniform;
GLuint materialShininessUniform;

GLuint LKeyPressedUniform;

//-----------comman------------
GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;

GLuint vaoSphere;
GLuint vboSphere_position;
GLuint vboSphere_normal;
GLuint vboSphere_elements;

bool bLights;

mat4 perspectiveProjectionMatrix;

GLfloat LightAmbient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat LightDefuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat LightSpecular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat LightPosition[] = { 0.0f,0.0f,0.0f,1.0f };

typedef struct Material {
	vec4 materialAmbient;
	vec4 materilaDefuse;
	vec4 materialSpecular;
	GLfloat materialShininess;
}MaterialStruct;

MaterialStruct Materials[24] = {
						{
							vec4(0.0215,0.1745,0.0215,1.0),
							vec4(0.07568,0.61424,0.07568,1.0),
							vec4(0.633,0.727811,0.633,1.0),
							0.6 * 128
						},
						{
							vec4(0.135,0.2225,0.1575,1.0),
							vec4(0.54,0.89 ,0.63,1.0),
							vec4(0.316228,0.316228,0.316228,1.0),
							0.1 * 128
						},
						{
							vec4(0.05375,0.05,0.06625,1.0),
							vec4(0.18275,0.17,0.22525,1.0),
							vec4(0.332741,0.328634,0.346435,1.0),
							0.3 * 128
						},{
							vec4(0.25,0.20725,0.20725,1.0),
							vec4(1.0,0.829,0.829,1.0),
							vec4(0.296648,0.296648,0.296648,1.0),
							0.088 * 128
						},{
							vec4(0.1745,0.01175,0.01175,1.0),
							vec4(0.61424,0.04136,0.04136,1.0),
							vec4(0.727811,0.626959,0.626959,1.0),
							0.6 * 128
						},{
							vec4(0.1,0.18725,0.1745,1.0f),
							vec4(0.396,0.74151,0.69102,1.0),
							vec4(0.297254,0.30829,0.306678,1.0),
							0.1 * 128
						},{
							vec4(0.329412,0.223529,0.027451,1.0),
							vec4(0.780392,0.568627,0.113725,1.0),
							vec4(0.992157,0.941176,0.807843,1.0),
							0.21794872 * 128
						},{
							vec4(0.2125,0.1275,0.054,1.0f),
							vec4(0.714,0.4284,0.18144,1.0f),
							vec4(0.393548,0.271906,0.166721,1.0),
							0.2 * 128
						},{
							vec4(0.25,0.25,0.25,1.0),
							vec4(0.4,0.4,0.4,1.0),
							vec4(0.774597,0.774597,0.774597,1.0),
							0.6 * 128
						},{
							vec4(0.19125,0.0735,0.0225,1.0),
							vec4(0.7038,0.27048,0.0828,1.0),
							vec4(0.256777,0.137622,0.086014,1.0),
							0.1 * 128
						},{
							vec4(0.24725,0.1995,0.0745,1.0),
							vec4(0.75164,0.60648,0.22648,1.0),
							vec4(0.628281,0.555802,0.366065,1.0),
							0.4 * 128
						},{
							vec4(0.19225,0.19225,0.19225,1.0),
							vec4(0.50754,0.50754,0.50754,1.0),
							vec4(0.508273,0.508273,0.508273,1.0),
							0.4 * 128
						},{
							vec4(0.0,0.0,0.0,1.0),
							vec4(0.01,0.01,0.01,1.0),
							vec4(0.50,0.50,0.50,1.0),
							0.25 * 128
						},{
							vec4(0.0,0.1,0.06,1.0),
							vec4(0.0,0.50980392,0.50980392,1.0),
							vec4(0.50196078,0.50196078,0.50196078,1.0),
							0.25 * 128
						},{
							vec4(0.0,0.0,0.0,1.0),
							vec4(0.1,0.35,0.1,1.0),
							vec4(0.45,0.55,0.45,1.0),
							0.25 * 128
						},{
							vec4(0.0,0.0,0.0,1.0),
							vec4(0.5,0.0,0.0,1.0),
							vec4(0.7,0.6,0.6,1.0),
							0.25 * 128
						},{
							vec4(0.0,0.0,0.0,1.0),
							vec4(0.55,0.55,0.55,1.0),
							vec4(0.70,0.70,0.70,1.0),
							0.25 * 128
						},{
							vec4(0.0,0.0,0.0,1.0),
							vec4(0.5,0.5,0.0,1.0),
							vec4(0.60,0.60,0.60,1.0),
							0.25 * 128
						},{
							vec4(0.02,0.02,0.02,1.0),
							vec4(0.01,0.01,0.01,1.0),
							vec4(0.4,0.4,0.4,1.0),
							0.078125 * 128
						},{
							vec4(0.0,0.05,0.05,1.0),
							vec4(0.4,0.5,0.5,1.0),
							vec4(0.04,0.7,0.7,1.0),
							0.078125 * 128
						}, {
							vec4(0.0,0.05,0.0,1.0),
							vec4(0.4,0.5,0.4,1.0),
							vec4(0.04,0.7,0.04,1.0f),
							0.078125 * 128
						}, {
							vec4(0.05,0.0,0.0,1.0),
							vec4(0.5,0.4,0.4,1.0),
							vec4(0.7,0.04,0.04,1.0),
							0.078125 * 128
						}, {
							vec4(0.05,0.05,0.05,1.0),
							vec4(0.5,0.5,0.5,1.0),
							vec4(0.7,0.7,0.7,1.0),
							0.078125 * 128
						}, {
							vec4(0.05,0.05,0.0,1.0),
							vec4(0.5,0.5,0.4,1.0),
							vec4(0.7,0.7,0.04,1.0),
							0.078125 * 128
						}
							};

float spherePosition[1146];
float sphereNormals[1146];
float sphereTexcoord[764];
unsigned short sphereElement[2280];//Face Indices

GLuint gNumSphereVertices, gNumSphereElements;


int WINAPI WinMain(HINSTANCE hInstance,HINSTANCE hPrevInstance,LPSTR lpszCmdline,int iCmdShow)
{
	void Initialize(void);
	void Display(void);

	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[]=TEXT("PyramidWithNoColor");
	int X1,X2,Y1,Y2;
	bool bDone=false;

	if(fopen_s(&gpFile,"log.txt","w")!=0)
	{
		MessageBox(NULL,TEXT("Failed to create log file!"),TEXT("ERROR"),MB_OK | MB_ICONERROR);
		exit(0);
	}

	X1=GetSystemMetrics(SM_CXSCREEN)/2;
	Y1=GetSystemMetrics(SM_CYSCREEN)/2;

	X2=WIN_WIDTH/2;
	Y2=WIN_HEIGHT/2;

	wndclass.cbSize=sizeof(WNDCLASSEX);
	wndclass.style=CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra=0;
	wndclass.cbWndExtra=0;
	wndclass.lpfnWndProc=WndProc;
	wndclass.hInstance=hInstance;
	wndclass.hIcon=LoadIcon(hInstance,MAKEINTRESOURCE(MYICON));
	wndclass.hCursor=LoadCursor(NULL,IDC_ARROW);
	wndclass.hbrBackground=(HBRUSH)GetStockObject(GRAY_BRUSH);
	wndclass.lpszClassName=szAppName;
	wndclass.lpszMenuName=NULL;
	wndclass.hIconSm=LoadIcon(hInstance,MAKEINTRESOURCE(MYICON));

	RegisterClassEx(&wndclass);

	hwnd=CreateWindowEx(WS_EX_APPWINDOW,
						szAppName,
						TEXT("Pyramid without color"),
						WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
						X1-X2,
						Y1-Y2,
						WIN_WIDTH,
						WIN_HEIGHT,
						NULL,
						NULL,
						hInstance,
						NULL);

	ghwnd=hwnd;
	Initialize();

	ShowWindow(hwnd,iCmdShow);
	SetForegroundWindow(hwnd);

	SetFocus(hwnd);

	while(bDone==false)
	{
		if(PeekMessage(&msg,NULL,0,0,PM_REMOVE))
		{
			if(msg.message==WM_QUIT)
			{
				bDone=true;
			}else{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}else{
			
				Display();
			
		}
	}

	return ((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd,UINT iMsg,WPARAM wParam,LPARAM lParam)
{
	void ToggleFullscreen(void);
	void Resize(int,int);
	void UnInitialize(void);

	switch(iMsg)
	{
		case WM_CREATE:
			fprintf(gpFile,"\nlog is started!");
			break;
		case WM_KEYDOWN:
			switch(wParam)
			{
				case VK_ESCAPE:
					DestroyWindow(hwnd);
					break;
				case 0x46:
				case 0x66:
					ToggleFullscreen();
					break;
				case 'l':
				case 'L':
					bLights = !bLights;
					break;
				case 'x':
				case 'X':
					RotationType = 1;
					break;
				case 'y':
				case 'Y':
					RotationType = 2;
					break;
				case 'z':
				case 'Z':
					RotationType = 3;
					break;
				default:
					break;
			}
			break;
		case WM_SETFOCUS:
			gbActiveWindow=true;
			break;
		case WM_KILLFOCUS:
			gbActiveWindow=false;
			break;

		case WM_ERASEBKGND:
			return 0;
		case WM_SIZE:
			Resize(LOWORD(lParam),HIWORD(lParam));
			break;
		case WM_CLOSE:
			DestroyWindow(hwnd);
			break;
		case WM_DESTROY:
			UnInitialize();
			PostQuitMessage(0);
			break;
		default:
			break;
	}

	return (DefWindowProc(hwnd,iMsg,wParam,lParam));
}

void ToggleFullscreen(void)
{
	MONITORINFO mi={sizeof(MONITORINFO)};
	if(gbFullscreen==false)
	{
		dwStyle=GetWindowLong(ghwnd,GWL_STYLE);
		if(dwStyle & WS_OVERLAPPEDWINDOW)
		{
			if(GetWindowPlacement(ghwnd,&wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd,MONITORINFOF_PRIMARY),&mi))
			{
				fprintf(gpFile,"\nFullscreen Enabled");
				SetWindowLong(ghwnd,GWL_STYLE,(dwStyle & ~WS_OVERLAPPEDWINDOW));
				SetWindowPos(ghwnd,HWND_TOP,mi.rcMonitor.left,mi.rcMonitor.top,mi.rcMonitor.right-mi.rcMonitor.left,mi.rcMonitor.bottom-mi.rcMonitor.top,SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		gbFullscreen=true;
	}
	else{
		SetWindowLong(ghwnd,GWL_STYLE,(dwStyle | WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(ghwnd,&wpPrev);
		SetWindowPos(ghwnd,HWND_TOP,0,0,0,0,SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED );
		fprintf(gpFile,"\nNormal Window");
		gbFullscreen=false;
	}
}

void Initialize(void)
{
	void Resize(int,int);

	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	ghdc=GetDC(ghwnd);

	ZeroMemory(&pfd,sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize=sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion=1;
	pfd.dwFlags=PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType=PFD_TYPE_RGBA;
	pfd.cColorBits=32;
	pfd.cRedBits=8;
	pfd.cGreenBits=8;
	pfd.cBlueBits=8;
	pfd.cAlphaBits=8;
	pfd.cDepthBits=32;

	iPixelFormatIndex=ChoosePixelFormat(ghdc,&pfd);

	if(iPixelFormatIndex==0)
	{
		fprintf(gpFile,"\nChoosePixelFormat() failed to get pixel format index");
		DestroyWindow(ghwnd);
	}

	if(SetPixelFormat(ghdc,iPixelFormatIndex,&pfd)==FALSE)
	{
		fprintf(gpFile,"\nSetPixelFormat() failed to set pixel format for window");
		DestroyWindow(ghwnd);
	}

	ghrc=wglCreateContext(ghdc);
	if(ghrc==NULL)
	{
		fprintf(gpFile,"\nwglCreateContext() failed to get the rendering context");
		DestroyWindow(ghwnd);
	}

	if(wglMakeCurrent(ghdc,ghrc)==FALSE)
	{
		fprintf(gpFile,"\nwglMakeCurrent() failed to swith current context from device to rendering context");
		DestroyWindow(ghwnd);
	}

	GLenum glew_error=glewInit();
	if(glew_error != GLEW_OK)
	{
		fprintf(gpFile,"\nglewInit() failed to enable extension for programmable pipeline");
		DestroyWindow(ghwnd);
	}

	fprintf(gpFile,"\nOpenGL Information are available below:-");
	fprintf(gpFile,"\n*********************************************************************************");

	fprintf(gpFile,"\nOpenGL Vender:%s",glGetString(GL_VENDOR));
	fprintf(gpFile,"\nOpenGL Render:%s",glGetString(GL_RENDERER));
	fprintf(gpFile,"\nOpenGL Version:%s",glGetString(GL_VERSION));
	fprintf(gpFile,"\nOpenGL GLSL Version:%s",glGetString(GL_SHADING_LANGUAGE_VERSION));

	fprintf(gpFile,"\nOpenGL Extension's List:\n");

	GLint numExt;
	glGetIntegerv(GL_NUM_EXTENSIONS,&numExt);

	for(int i=0;i<numExt;i++)
	{
		fprintf(gpFile,"\n%s",glGetStringi(GL_EXTENSIONS,i));
	}
	fprintf(gpFile,"\n*********************************************************************************");
		
	GLint infoLogLength;
	GLint shaderCompileStatus;
	GLint shaderProgramLinkStatus;
	GLchar *szBuffer;
	GLsizei written;

	//Per Fragment Shader Code Start--------------------
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	const GLchar* vertexShaderSourceCode_PF =
		"#version 460 core\n"\
		"\n"\
		"in vec4 vPosition;\n"\
		"in vec3 vNormal;\n"\

		"uniform mat4 u_modelMatrix;\n"\
		"uniform mat4 u_viewMatrix;\n"\
		"uniform mat4 u_projectionMatrix;\n"\

		"uniform vec4 u_lightPosition;\n"\

		"uniform int u_lKeyPressed;\n"\

		"out vec3 out_transformedNormal;\n"\
		"out vec3 out_lightDirection;\n"\
		"out vec3 out_viewVector;\n"\

		"void main(void)\n"\

		"{\n"\
			"if (u_lKeyPressed == 1)\n"\
			"{\n"\

				"vec4 eyeCoord = u_viewMatrix * u_modelMatrix * vPosition;\n"\

				"out_viewVector = -eyeCoord.xyz;\n"\

				"out_transformedNormal = mat3(u_viewMatrix * u_modelMatrix) * vNormal ;\n"\

				"out_lightDirection =  vec3( u_lightPosition - eyeCoord ) ;\n"\

			"}\n"\
		
			"gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix * vPosition;\n"\

		"}";

	glShaderSource(gVertexShaderObject,
		1,
		(const GLchar**)&vertexShaderSourceCode_PF,
		NULL);

	glCompileShader(gVertexShaderObject);

	glGetShaderiv(gVertexShaderObject,
		GL_COMPILE_STATUS,
		&shaderCompileStatus);

	if (shaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject,
			GL_INFO_LOG_LENGTH,
			&infoLogLength);

		if (infoLogLength > 0)
		{
			szBuffer = (GLchar*)malloc(sizeof(GLchar) * infoLogLength);
			if (szBuffer != NULL)
			{
				glGetShaderInfoLog(gVertexShaderObject,
					infoLogLength,
					&written,
					szBuffer);
				fprintf(gpFile, "\nERROR:Vertex Shader Object Compilation Failed log : %s", szBuffer);
				free(szBuffer);
				DestroyWindow(ghwnd);
			}
		}
	}

	infoLogLength = 0;
	shaderCompileStatus = 0;
	shaderProgramLinkStatus = 0;
	szBuffer = NULL;
	written = 0;

	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	const GLchar* fragmentShaderSourceCode_PF =
		"#version 460 core"\
		"\n"\

		"uniform vec3 u_lightAmbient;\n"\
		"uniform vec3 u_lightDiffuse;\n"\
		"uniform vec3 u_lightSpecular;\n"\

		"uniform vec3 u_materialAmbient;\n"\
		"uniform vec3 u_materialDiffuse;\n"\
		"uniform vec3 u_materialSpecular;\n"\
		"uniform float u_materialShininess;\n"\

		"uniform int u_lKeyPressed;\n"\

		"in vec3 out_transformedNormal;\n"\
		"in vec3 out_lightDirection;\n"\
		"in vec3 out_viewVector;\n"\

		"vec3 phongADS_Light;\n"
		"out vec4 fragColor;"\

		"void main(void)"\
		"{"\

			"if (u_lKeyPressed == 1)\n"\
			"{\n"\

				"vec3 normalizedTransformedNormal = normalize( out_transformedNormal );\n"\
				"vec3 normalizedLightDirection = normalize( out_lightDirection );\n"\
				"vec3 normalizedViewVector = normalize( out_viewVector );\n"\

				"vec3 reflectionVector = reflect( -normalizedLightDirection, normalizedTransformedNormal );\n"\

				"vec3 ambient= u_lightAmbient * u_materialAmbient;\n"\

				"vec3 diffuse = u_lightDiffuse * u_materialDiffuse * max( dot(normalizedLightDirection , normalizedTransformedNormal), 0.0);\n"\

				"vec3 specular = u_lightSpecular * u_materialSpecular * pow( max( dot(reflectionVector , normalizedViewVector), 0.0f) , u_materialShininess);\n"\

				"phongADS_Light = ambient + diffuse + specular;\n"\

			"}\n"\
			"else\n"\
			"{\n"\
				"phongADS_Light=vec3(1.0f,1.0f,1.0f);\n"\
			"}\n"\

			"fragColor = vec4( phongADS_Light, 1.0f );"\
		"}";

	glShaderSource(gFragmentShaderObject,
		1,
		(const GLchar**)&fragmentShaderSourceCode_PF,
		NULL);

	glCompileShader(gFragmentShaderObject);

	glGetShaderiv(gFragmentShaderObject,
		GL_COMPILE_STATUS,
		&shaderCompileStatus);

	if (shaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject,
			GL_INFO_LOG_LENGTH,
			&infoLogLength);

		if (infoLogLength > 0)
		{
			szBuffer = (GLchar*)malloc(sizeof(GLchar) * infoLogLength);
			if (szBuffer != NULL)
			{
				glGetShaderInfoLog(gFragmentShaderObject,
					infoLogLength,
					&written,
					szBuffer);
				fprintf(gpFile, "\nERROR:Fragment Shader Object Compilation Failed log : %s", szBuffer);
				free(szBuffer);
				DestroyWindow(ghwnd);
			}
		}
	}

	infoLogLength = 0;
	shaderCompileStatus = 0;
	shaderProgramLinkStatus = 0;
	szBuffer = NULL;
	written = 0;

	gShaderProgramObject = glCreateProgram();

	glAttachShader(gShaderProgramObject, gVertexShaderObject);
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	//Shader Attribute Binding Before Pre Linking of attached Shader
	glBindAttribLocation(gShaderProgramObject,
		VSV_ATTRIBUTE_POSITION,
		"vPosition");
	glBindAttribLocation(gShaderProgramObject,
		VSV_ATTRIBUTE_NORMAL,
		"vNormal");

	glLinkProgram(gShaderProgramObject);

	glGetProgramiv(gShaderProgramObject,
		GL_LINK_STATUS,
		&shaderProgramLinkStatus);

	if (shaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject,
			GL_INFO_LOG_LENGTH,
			&infoLogLength);
		if (infoLogLength > 0)
		{
			szBuffer = (GLchar*)malloc(sizeof(GLchar) * infoLogLength);

			if (szBuffer != NULL)
			{
				glGetProgramInfoLog(gShaderProgramObject,
					infoLogLength,
					&written,
					szBuffer);

				fprintf(gpFile, "\nERROR:Shader program Link failed log : %s", szBuffer);
				free(szBuffer);
				DestroyWindow(ghwnd);
			}
		}
	}

	//Shader Code End--------------------


	//Uniform variable Binding after post linking of attached shader 
	modelMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_modelMatrix");
	viewMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_viewMatrix");
	projectionlMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_projectionMatrix");

	
	lightAmbientUniform = glGetUniformLocation(gShaderProgramObject, "u_lightAmbient");
	lightDiffuseUniform = glGetUniformLocation(gShaderProgramObject, "u_lightDiffuse");
	lightSpecularUniform = glGetUniformLocation(gShaderProgramObject, "u_lightSpecular");
	lightPositionUniform = glGetUniformLocation(gShaderProgramObject, "u_lightPosition");
	
	materialAmbientUniform = glGetUniformLocation(gShaderProgramObject, "u_materialAmbient");
	materialDiffuseUniform = glGetUniformLocation(gShaderProgramObject, "u_materialDiffuse");
	materialSpecularUniform = glGetUniformLocation(gShaderProgramObject, "u_materialSpecular");
	materialShininessUniform = glGetUniformLocation(gShaderProgramObject, "u_materialShininess");

	LKeyPressedUniform = glGetUniformLocation(gShaderProgramObject, "u_lKeyPressed");

	//VAO and its respective VBO preparation

	getSphereVertexData(spherePosition, sphereNormals, sphereTexcoord, sphereElement);
	gNumSphereVertices = getNumberOfSphereVertices();
	gNumSphereElements = getNumberOfSphereElements();


		glGenVertexArrays(1,&vaoSphere);
		glBindVertexArray(vaoSphere);

			glGenBuffers(1,&vboSphere_position);
			glBindBuffer(GL_ARRAY_BUFFER, vboSphere_position);

				glBufferData(GL_ARRAY_BUFFER,
							sizeof(spherePosition),
							spherePosition,
							GL_STATIC_DRAW);

				glVertexAttribPointer(VSV_ATTRIBUTE_POSITION,
									3,
									GL_FLOAT,
									GL_FALSE,
									0,
									NULL);

				glEnableVertexAttribArray(VSV_ATTRIBUTE_POSITION);
			
			glBindBuffer(GL_ARRAY_BUFFER,0);

			glGenBuffers(1, &vboSphere_normal);
			glBindBuffer(GL_ARRAY_BUFFER, vboSphere_normal);

			glBufferData(GL_ARRAY_BUFFER,
				sizeof(sphereNormals),
				sphereNormals,
				GL_STATIC_DRAW);

			glVertexAttribPointer(VSV_ATTRIBUTE_NORMAL,
				3,
				GL_FLOAT,
				GL_FALSE,
				0,
				NULL);

			glEnableVertexAttribArray(VSV_ATTRIBUTE_NORMAL);

			glBindBuffer(GL_ARRAY_BUFFER, 0);

			glGenBuffers(1,
				&vboSphere_elements);

			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,
				vboSphere_elements);

				glBufferData(GL_ELEMENT_ARRAY_BUFFER,
					sizeof(sphereElement),
					sphereElement,
					GL_STATIC_DRAW);

			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,
				0);

		glBindVertexArray(0);

	glClearColor(0.5,0.5,0.5f,0.0f);

	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	perspectiveProjectionMatrix=mat4::identity();
	Resize(WIN_WIDTH,WIN_HEIGHT);

}

void Resize(int width,int height)
{
	if(height==0)
	{
		height=1;
	}
	glViewport(0,0,(GLsizei)width,(GLsizei)height);

	orignal_width = (GLsizei)width;
	orignal_height = (GLsizei)height;

	perspectiveProjectionMatrix=vmath::perspective(45.0f,
													(GLfloat)width/(GLfloat)height,
													0.1f,
													100.0f);
}

GLfloat Angle = 0.0f;

GLfloat radius = 3.0f;

void Display(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


		glUseProgram(gShaderProgramObject);
		mat4 translateMatrix;
		mat4 rotateMatrix;

		mat4 modelMatrix;
		mat4 viewMatrix = mat4::identity();

		GLfloat translate[3] = { 0.0f,0.0f,-1.5f };
		int k = 0;

		GLsizei unit_width = orignal_width / 6;
		GLsizei unit_height = orignal_height / 4;

		for (int i = 0; i < 4; i++)
		{

			for (int j = 0; j < 6; j++)
			{				

				glViewport(j * unit_width, i * unit_height, unit_width, orignal_height / 6);
				//glViewport(0, 0, orignal_width, orignal_height);

				translateMatrix = vmath::translate(translate[0], translate[1], translate[2]);
				modelMatrix = translateMatrix;

				glUniformMatrix4fv(modelMatrixUniform,
					1,
					GL_FALSE,
					modelMatrix);

				glUniformMatrix4fv(viewMatrixUniform,
					1,
					GL_FALSE,
					viewMatrix);

				glUniformMatrix4fv(projectionlMatrixUniform,
					1,
					GL_FALSE,
					perspectiveProjectionMatrix);

				if (bLights == true)
				{
					glUniform1i(LKeyPressedUniform, 1);

					glUniform3fv(lightAmbientUniform, 1, LightAmbient);
					glUniform3fv(lightDiffuseUniform, 1, LightDefuse);
					glUniform3fv(lightSpecularUniform, 1, LightSpecular);

					if (RotationType == 1) {

						LightPosition[0] = 0.0f;
						LightPosition[1] = radius * sin(Angle);
						LightPosition[2] = radius * cos(Angle);
						LightPosition[3] = 1.0f;

					}
					else if (RotationType == 2) {

						LightPosition[0] = radius * sin(Angle);
						LightPosition[1] = 0.0f;
						LightPosition[2] = radius * cos(Angle);
						LightPosition[3] = 1.0f;

					}
					else if (RotationType == 3) {

						LightPosition[0] = radius * sin(Angle);
						LightPosition[1] = radius * cos(Angle);
						LightPosition[2] = 0.0f,
						LightPosition[3] = 1.0f;

					}
					else if (RotationType == 0)
					{
						LightPosition[0] = 3.0f;
						LightPosition[1] = 3.0f;
						LightPosition[2] = 3.0f,
						LightPosition[3] = 1.0f;
					}

					glUniform4fv(lightPositionUniform, 1, LightPosition);//posiitional light


					glUniform3fv(materialAmbientUniform, 1, Materials[k].materialAmbient);
					glUniform3fv(materialDiffuseUniform, 1, Materials[k].materilaDefuse);
					glUniform3fv(materialSpecularUniform, 1, Materials[k].materialSpecular);

					glUniform1f(materialShininessUniform, Materials[k].materialShininess);
				}
				else
				{
					glUniform1i(LKeyPressedUniform, 0);
				}

				glBindVertexArray(vaoSphere);
					glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboSphere_elements);
					glDrawElements(GL_TRIANGLES, gNumSphereElements, GL_UNSIGNED_SHORT, 0);
					glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
				glBindVertexArray(0);

				k++;
			}

		}

	
	glUseProgram(0);


	SwapBuffers(ghdc);

	Angle += 0.05f;
}

void UnInitialize(void)
{
	if(gbFullscreen==true)
	{
		SetWindowLong(ghwnd,GWL_STYLE,(dwStyle | WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(ghwnd,&wpPrev);
		SetWindowPos(ghwnd,HWND_TOP,0,0,0,0,SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED );
		fprintf(gpFile,"\nNormal Window");
		gbFullscreen=false;
	}

	if(vaoSphere)
	{
		glDeleteVertexArrays(1,
							&vaoSphere);
	}
	vaoSphere = 0;

	if(vboSphere_position)
	{
		glDeleteBuffers(1,
					&vboSphere_position);
	}
	vboSphere_position = 0;
	
	if (vboSphere_normal)
	{
		glDeleteBuffers(1,
			&vboSphere_normal);
	}
	vboSphere_normal = 0;

	if (vboSphere_elements)
	{
		glDeleteBuffers(1,
			&vboSphere_elements);
	}
	vboSphere_elements = 0;

	if(gShaderProgramObject)
	{
		glUseProgram(gShaderProgramObject);

		GLsizei shaderCount;
		GLsizei tempShaderCount;
		GLuint *pShader=NULL;

		glGetProgramiv(gShaderProgramObject,GL_ATTACHED_SHADERS,&shaderCount);

		pShader=(GLuint *)malloc(sizeof(GLuint)*shaderCount);
		if(pShader==NULL)
		{
			fprintf(gpFile,"\nFailed to allocate memory for pShader");
		}else{
			glGetAttachedShaders(gShaderProgramObject,shaderCount,&tempShaderCount,pShader);
			
			fprintf(gpFile,"\nShader program has actual attached shader count is:%d",tempShaderCount);
			
			for(GLsizei i=0;i<shaderCount;i++)
			{
				glDetachShader(gShaderProgramObject,pShader[i]);
				glDeleteShader(pShader[i]);
				pShader[i]=0;
			}
			free(pShader);
			
			glDeleteProgram(gShaderProgramObject);
			gShaderProgramObject =0;
		}
		glUseProgram(0);
	}


	if(wglGetCurrentContext()==ghrc)
	{
		wglMakeCurrent(NULL,NULL);
	}

	if(ghrc)
	{
		wglDeleteContext(ghrc);
	}
		ghrc=NULL;

	if(ghdc)
	{
		ReleaseDC(ghwnd,ghdc);
	}
		ghdc=NULL;
	
	if(gpFile)
	{
		fprintf(gpFile,"\nlog is ended!");
		fclose(gpFile);
	}
		gpFile=NULL;
}
