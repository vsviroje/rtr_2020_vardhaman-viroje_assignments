#include<windows.h>
#include<stdio.h>
#include"two_2D_color_shape.h"

#include<GL/GLEW.h>
#include<GL/GL.h>
#include "vmath.h"

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

#pragma comment(lib,"GLEW32.lib")
#pragma comment(lib,"OpenGL32.lib")

using namespace vmath;

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

FILE* gpFile = NULL;
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
bool gbFullscreen = false;
bool gbActiveWindow = false;
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

enum{
	VSV_ATTRIBUTE_POSITION,
	VSV_ATTRIBUTE_COLOR,
	VSV_ATTRIBUTE_TEXCOORD,
	VSV_ATTRIBUTE_NORMAL,
};

GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

GLuint vao_triangle;
GLuint vao_square;

GLuint vbo_positionTriangle;
GLuint vbo_colorTriangle;

GLuint vbo_positionSquare;
GLuint vbo_colorSquare;

GLuint mvpMatrixUniform;
mat4 perspectiveProjectionMatrix;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	void Initialize(void);
	void Display(void);

	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("two2DColorShape");
	int X1, X2, Y1, Y2;
	bool bDone = false;

	if (fopen_s(&gpFile, "log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Failed to Create log file!"), TEXT("Error"), MB_OK | MB_ICONERROR);
		exit(0);
	}
	
	X1 = GetSystemMetrics(SM_CXSCREEN) / 2;
	Y1 = GetSystemMetrics(SM_CYSCREEN) / 2;

	X2 = WIN_WIDTH / 2;
	Y2 = WIN_HEIGHT / 2;

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style =  CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(GRAY_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));

	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("Two 2D Color Shape"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		X1 - X2,//100,
		Y1 - Y2,//100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;
	Initialize();

	ShowWindow(hwnd, iCmdShow);

	SetForegroundWindow(hwnd);
	SetFocus(hwnd);
	


	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
		
			if (gbActiveWindow == true)
			{
				Display();
			}
		}
	}

	return ((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void ToggleFullscreen(void);
	void Resize(int, int);
	void UnInitialize(void);

	switch (iMsg)
	{
	case WM_CREATE:
		fprintf(gpFile, "\nLog is Started!");
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
			case VK_ESCAPE:
				DestroyWindow(hwnd);
				break;
			case 0x46:
			case 0x66:
				ToggleFullscreen();
				break;
			default:
				break;
		}
		break;
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;
	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;

	case WM_ERASEBKGND:
		return 0;
	case WM_SIZE:
		Resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;
	case WM_DESTROY:
	
		UnInitialize();
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}
void ToggleFullscreen(void)
{
	MONITORINFO mi = { sizeof(MONITORINFO) };
	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW) 
		{
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				fprintf(gpFile, "\nFullscreen Enabled");
				SetWindowLong(ghwnd, GWL_STYLE, (dwStyle & ~WS_OVERLAPPEDWINDOW));
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
		gbFullscreen = true;
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, (dwStyle | WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		fprintf(gpFile, "\nNormal Window");
		ShowCursor(TRUE);
		gbFullscreen = false;
	}
}

void Initialize(void)
{
	void Resize(int, int);
	
	//Local variable initialization
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	ghdc = GetDC(ghwnd);

	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER ;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits= 32 ;


	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (iPixelFormatIndex == 0 )
	{
		fprintf(gpFile, "\nChoosePixelFormat() Failed to get pixel format index");
		DestroyWindow(ghwnd);
	}

	if ( SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE )
	{
		fprintf(gpFile, "\nSetPixelFormat() Failed to set pixel format for window");
		DestroyWindow(ghwnd);
	}

	ghrc = wglCreateContext(ghdc);
	if ( ghrc == NULL)
	{
		fprintf(gpFile, "\nwglCreateContext() Failed to get the rendering context");
		DestroyWindow(ghwnd);
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE )
	{
		fprintf(gpFile, "\nwglMakeCurrent() Failed to switch current context from device to rendering context");
		DestroyWindow(ghwnd);
	}

	GLenum glew_error=glewInit();
	if(glew_error != GLEW_OK)
	{
		fprintf(gpFile, "\nglewInit failed to enable extension for Programmable Pipeline");
		DestroyWindow(ghwnd);

	}
	fprintf(gpFile, "\nOpenGL Information are availble below:-");
	fprintf(gpFile, "\n*****************************************************");

	fprintf(gpFile, "\nOpenGL Vender:%s",glGetString(GL_VENDOR));
	fprintf(gpFile, "\nOpenGL Renderer:%s",glGetString(GL_RENDERER));
	fprintf(gpFile, "\nOpenGL Version:%s",glGetString(GL_VERSION));
	fprintf(gpFile, "\nOpenGL GLSL Version:%s",glGetString(GL_SHADING_LANGUAGE_VERSION));
	
	fprintf(gpFile, "\nOpenGL Extension's list:\n");
	GLint numExt;
	glGetIntegerv(GL_NUM_EXTENSIONS,&numExt);

	for(int i=0;i<numExt;i++)
	{
		fprintf(gpFile, "\n%s",glGetStringi(GL_EXTENSIONS,i));
	}
	fprintf(gpFile, "\n*****************************************************");
	
	GLint infoLogLength;
	GLint shaderCompileStatus;
	GLint shaderProgramLinkStatus;
	GLchar *szBuffer;
	GLsizei written;

	//Shader Code Start--------------------
		gVertexShaderObject=glCreateShader(GL_VERTEX_SHADER);

		const GLchar* vertexShaderSourceCode=
		"#version 460 core"\
		"\n"\
		"in vec4 vPosition;"\
		"in vec4 vColor;"\
		"out vec4 out_color;"\
		"uniform mat4 u_mvpMatrix;"\
		"void main(void)"\
		"{"\
			"gl_Position=u_mvpMatrix * vPosition;"\
			"out_color=vColor;"\
		"}";

		glShaderSource(gVertexShaderObject,
						1,
						(const GLchar **)&vertexShaderSourceCode,
						NULL);

		glCompileShader(gVertexShaderObject);

		glGetShaderiv(gVertexShaderObject,
						GL_COMPILE_STATUS,
						&shaderCompileStatus);
		if(shaderCompileStatus==GL_FALSE)
		{
			glGetShaderiv(gVertexShaderObject,
						GL_INFO_LOG_LENGTH,
						&infoLogLength);
			
			if(infoLogLength>0)
			{
				szBuffer=(GLchar *)malloc(sizeof(GLchar) * infoLogLength);
				if(szBuffer!=NULL)
				{
					glGetShaderInfoLog(gVertexShaderObject,
										infoLogLength,
										&written,
										szBuffer);
					
					fprintf(gpFile,"\nERROR:Vertex Shader Compilation failed log : %s",szBuffer);
					free(szBuffer);
					DestroyWindow(ghwnd);
				}
			}
		}

		infoLogLength=0;
		shaderCompileStatus=0;
		shaderProgramLinkStatus=0;
		szBuffer=NULL;
		written=0;

		gFragmentShaderObject=glCreateShader(GL_FRAGMENT_SHADER);

		const GLchar * fragmentShaderSourceCode=
		"#version 460 core"\
		"\n"\
		"in vec4 out_color;"\
		"out vec4 fragColor;"\
		"void main(void)"\
		"{"\
			"fragColor=out_color;"\
		"}";

		glShaderSource(gFragmentShaderObject,
						1,
						(const GLchar**)&fragmentShaderSourceCode,
						NULL );
		
		glCompileShader(gFragmentShaderObject);

		glGetShaderiv(gFragmentShaderObject,
						GL_COMPILE_STATUS,
						&shaderCompileStatus);
		
		if(shaderCompileStatus==GL_FALSE)
		{
			glGetShaderiv(gFragmentShaderObject,
						GL_INFO_LOG_LENGTH,
						&infoLogLength);
			
			if(infoLogLength > 0)
			{
				szBuffer=(GLchar *)malloc(sizeof(GLchar) * infoLogLength);
				if(szBuffer != NULL)
				{
					glGetShaderInfoLog(gFragmentShaderObject,
										infoLogLength,
										&written,
										szBuffer);
					
					fprintf(gpFile,
							"\nERROR Fragment Shader Compilation failed log : %s",
							szBuffer);

					free(szBuffer);
					DestroyWindow(ghwnd);	
				}
			}
		}

		infoLogLength=0;
		shaderCompileStatus=0;
		shaderProgramLinkStatus=0;
		szBuffer=NULL;
		written=0;

		gShaderProgramObject=glCreateProgram();

		glAttachShader(gShaderProgramObject,gVertexShaderObject);
		glAttachShader(gShaderProgramObject,gFragmentShaderObject);

		//Shader Attribute Binding Before Pre Linking of attached Shader
			glBindAttribLocation(gShaderProgramObject,
								VSV_ATTRIBUTE_POSITION,
								"vPosition");
			glBindAttribLocation(gShaderProgramObject,
								VSV_ATTRIBUTE_COLOR,
								"vColor");
		
		glLinkProgram(gShaderProgramObject);

		glGetProgramiv(gShaderProgramObject,
					GL_LINK_STATUS,
					&shaderProgramLinkStatus);
		
		if(shaderProgramLinkStatus==GL_FALSE)
		{
			glGetProgramiv(gShaderProgramObject,
							GL_INFO_LOG_LENGTH,
							&infoLogLength);

			if(infoLogLength > 0)
			{
				szBuffer=(GLchar *)malloc(sizeof(GLchar) * infoLogLength);
				if(szBuffer!=NULL)
				{
					glGetProgramInfoLog(gShaderProgramObject,
										infoLogLength,
										&written,
										szBuffer);
					
					fprintf(gpFile,"\nERROR Shader Program Link Failed log : %s",szBuffer);

					free(szBuffer);
					DestroyWindow(ghwnd);
				}
			}	
		}

	//Shader Code End--------------------

	//Uniform variable Binding after post linking of attached shader
		mvpMatrixUniform=glGetUniformLocation(gShaderProgramObject,
											"u_mvpMatrix");

	//VAO and its respective VBO preparation
		const GLfloat triangleVertices[]=
		{
			0.0f,1.0f,0.0f,
			-1.0f,-1.0f,0.0f,
			1.0f,-1.0f,0.0f
		};

		const GLfloat triangleColor[]=
		{
			1.0f, 0.0f, 0.0f,
			0.0f, 1.0f, 0.0f,
			0.0f, 0.0f, 1.0f
		};

		const GLfloat squareVertices[]=
		{
			1.0f, 1.0f, 0.0f,
			-1.0f, 1.0f, 0.0f,
			-1.0f, -1.0f, 0.0f,
			1.0f, -1.0f, 0.0f
		};

		const GLfloat squareColor[]=
		{
			0.0f, 0.0f, 1.0f,
			0.0f, 0.0f, 1.0f,
			0.0f, 0.0f, 1.0f,
			0.0f, 0.0f, 1.0f
		};

		glGenVertexArrays(1,
						&vao_triangle);

		glBindVertexArray(vao_triangle);
			glGenBuffers(1,
						&vbo_positionTriangle);

			glBindBuffer(GL_ARRAY_BUFFER,
						vbo_positionTriangle);

				glBufferData(GL_ARRAY_BUFFER,
							sizeof(triangleVertices),
							triangleVertices,
							GL_STATIC_DRAW);

				glVertexAttribPointer(VSV_ATTRIBUTE_POSITION,
									3,
									GL_FLOAT,
									GL_FALSE,
									0,
									NULL);

				glEnableVertexAttribArray(VSV_ATTRIBUTE_POSITION);
			
			glBindBuffer(GL_ARRAY_BUFFER,0);
			
			glGenBuffers(1,
						&vbo_colorTriangle);

			glBindBuffer(GL_ARRAY_BUFFER,
						vbo_colorTriangle);

				glBufferData(GL_ARRAY_BUFFER,
							sizeof(triangleColor),
							triangleColor,
							GL_STATIC_DRAW);

				glVertexAttribPointer(VSV_ATTRIBUTE_COLOR,
										3,
										GL_FLOAT,
										GL_FALSE,
										0,
										NULL);

				glEnableVertexAttribArray(VSV_ATTRIBUTE_COLOR);
				
			glBindBuffer(GL_ARRAY_BUFFER,0);
		glBindVertexArray(0);

		glGenVertexArrays(1,
						&vao_square);

		glBindVertexArray(vao_square);

			glGenBuffers(1,
						&vbo_positionSquare);

			glBindBuffer(GL_ARRAY_BUFFER,
						vbo_positionSquare);

				glBufferData(GL_ARRAY_BUFFER,
							sizeof(squareVertices),
							squareVertices,
							GL_STATIC_DRAW);

				glVertexAttribPointer(VSV_ATTRIBUTE_POSITION,
									3,
									GL_FLOAT,
									GL_FALSE,
									0,
									NULL);

				glEnableVertexAttribArray(VSV_ATTRIBUTE_POSITION);
			
			glBindBuffer(GL_ARRAY_BUFFER,0);

			glGenBuffers(1,
						&vbo_colorSquare);

			glBindBuffer(GL_ARRAY_BUFFER,
						vbo_colorSquare);
				
				glBufferData(GL_ARRAY_BUFFER,
							sizeof(squareColor),
							squareColor,
							GL_STATIC_DRAW);

				glVertexAttribPointer(VSV_ATTRIBUTE_COLOR,
										3,
										GL_FLOAT,
										GL_FALSE,
										0,
										NULL);

				glEnableVertexAttribArray(VSV_ATTRIBUTE_COLOR);

			glBindBuffer(GL_ARRAY_BUFFER,0);	
		
		glBindVertexArray(0);
	//SetColor
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	//Warmup resize call

	perspectiveProjectionMatrix=mat4::identity();
	Resize(WIN_WIDTH, WIN_HEIGHT);
	
}

void Resize(int width, int height)
{
	if (height == 0)
	{
		height = 1;
	}
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	perspectiveProjectionMatrix=vmath::perspective(45.0f,
													(GLfloat)width/(GLfloat)height,
													0.1f,
													100.0f);
}


void Display(void)
{
	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
	glUseProgram(gShaderProgramObject);	
		mat4 modelViewMatrix=mat4::identity();
		mat4 modelViewProjectionMatrix=mat4::identity();
		mat4 translateMatrix;

		translateMatrix=vmath::translate(-1.5f,0.0f,-6.0f);
		modelViewMatrix=translateMatrix;

		modelViewProjectionMatrix=perspectiveProjectionMatrix * modelViewMatrix;

		glUniformMatrix4fv(mvpMatrixUniform,
							1,
							GL_FALSE,
							modelViewProjectionMatrix);
		glBindVertexArray(vao_triangle);
			glDrawArrays(GL_TRIANGLES,
						0,
						3);
		glBindVertexArray(0);
 		
		modelViewMatrix=mat4::identity();
		modelViewProjectionMatrix=mat4::identity();
		translateMatrix=mat4::identity();

		translateMatrix=vmath::translate(1.5f,0.0f,-6.0f);
		modelViewMatrix=translateMatrix;

		modelViewProjectionMatrix=perspectiveProjectionMatrix * modelViewMatrix;
		
		glUniformMatrix4fv(mvpMatrixUniform,
							1,
							GL_FALSE,
							modelViewProjectionMatrix);
		
		glBindVertexArray(vao_square);
			glDrawArrays(GL_TRIANGLE_FAN,
						0,
						4);
		glBindVertexArray(0);

	glUseProgram(0);	



	SwapBuffers(ghdc);
	
}
void UnInitialize(void)
{
	if (gbFullscreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, (dwStyle | WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}
	
	if(vao_square)
	{
		glDeleteVertexArrays(1,
							&vao_square);
	}
	vao_square=0;

	if(vao_triangle)
	{
		glDeleteVertexArrays(1,
							&vao_triangle);
	}
	vao_triangle=0;

	if(vbo_colorTriangle){
		glDeleteBuffers(1,
						&vbo_colorTriangle);
	}
	vbo_colorTriangle=0;

	if(vbo_positionTriangle)
	{
		glDeleteBuffers(1,
					&vbo_positionTriangle);
	}
	vbo_positionTriangle=0;

	if(vbo_colorSquare){
		glDeleteBuffers(1,
						&vbo_colorSquare);
	}
	vbo_colorSquare=0;

	if(vbo_positionSquare)
	{
		glDeleteBuffers(1,
					&vbo_positionSquare);
	}
	vbo_positionSquare=0;

	if(gShaderProgramObject)
	{
		glUseProgram(gShaderProgramObject);

			GLsizei shaderCount;
			GLsizei tempShaderCount;
			GLuint *pShader=NULL;

			glGetProgramiv(gShaderProgramObject,
							GL_ATTACHED_SHADERS,
							&shaderCount);

			pShader=(GLuint *)malloc(sizeof(GLuint)*shaderCount);
			if(pShader==NULL)
			{
				fprintf(gpFile,"\nFailed to allocate memory for pShader");
			}else{
				glGetAttachedShaders(gShaderProgramObject,
									shaderCount,
									&tempShaderCount,
									pShader);
				
				fprintf(gpFile,"\nShader program has actual attached shader count is:%d",tempShaderCount);
				
				for(GLsizei i=0;i<shaderCount;i++)
				{
					glDetachShader(gShaderProgramObject,
									pShader[i]);

					glDeleteShader(pShader[i]);
					
					pShader[i]=0;
				}
				free(pShader);
				
				glDeleteProgram(gShaderProgramObject);
				
				gShaderProgramObject=0;
			}
		glUseProgram(0);
	}

	if (wglGetCurrentContext() == ghrc ) {
		wglMakeCurrent(NULL, NULL);
	}


	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (gpFile)
	{
		fprintf(gpFile, "\nLog is Ended!");
		fclose(gpFile);
		gpFile = NULL;
	}
}



/*
cl /c /EHsc /I C:\glew-2.1.0-win32\glew-2.1.0\include two_2D_color_shape.cpp
rc two_2D_color_shape.rc
link two_2D_color_shape.obj two_2D_color_shape.res /LIBPATH:C:\glew-2.1.0-win32\glew-2.1.0\lib\Release\x64 user32.lib gdi32.lib kernel32.lib /SUBSYSTEM:WINDOWS
two_2D_color_shape.exe
*/
