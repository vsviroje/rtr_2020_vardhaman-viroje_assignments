#include<windows.h>
#include<stdio.h>
#include"Cube_Diffuse_Light.h"
#include "vmath.h"
#include<GL/GLEW.h>
#include<GL/GL.h>

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

#pragma comment(lib,"GLEW32.lib")
#pragma comment(lib,"OpenGL32.lib")

using namespace vmath;

LRESULT CALLBACK WndProc(HWND,UINT,WPARAM,LPARAM);

FILE* gpFile=NULL;
DWORD dwStyle;
WINDOWPLACEMENT wpPrev={sizeof(WINDOWPLACEMENT)};
bool gbFullscreen=false;
bool gbActiveWindow=false;
HWND ghwnd=NULL;
HDC ghdc=NULL;
HGLRC ghrc=NULL;

enum{
	VSV_ATTRIBUTE_POSITION=0,
	VSV_ATTRIBUTE_COLOR,
	VSV_ATTRIBUTE_NORMAL,
	VSV_ATTRIBUTE_TEXCOORD,
};

GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

GLuint vao_cube;

GLuint vbo_cubePosition;
GLuint vbo_cubeNormal;

GLuint modelViewMatrixUniform;
GLuint projectionMatrixUniform;
GLuint LKeyPressedUniform;

GLuint LDUniform;//Light diffuse
GLuint KDUniform;//Material diffuse
GLuint lightPositionUniform;

mat4 perspectiveMatrixUniform;

bool bAnimate;
bool bLights;

int WINAPI WinMain(HINSTANCE hInstance,HINSTANCE hPrevInstance,LPSTR lpszCmdline,int iCmdShow)
{
	void Initialize(void);
	void Display(void);

	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[]=TEXT("DiffuseLightCube");
	int X1,X2,Y1,Y2;
	bool bDone=false;

	if(fopen_s(&gpFile,"log.txt","w")!=0)
	{
		MessageBox(NULL,TEXT("Failed to create log file!"),TEXT("ERROR"),MB_OK | MB_ICONERROR);
		exit(0);
	}

	X1=GetSystemMetrics(SM_CXSCREEN)/2;
	Y1=GetSystemMetrics(SM_CYSCREEN)/2;

	X2=WIN_WIDTH/2;
	Y2=WIN_HEIGHT/2;

	wndclass.cbSize=sizeof(WNDCLASSEX);
	wndclass.style=CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra=0;
	wndclass.cbWndExtra=0;
	wndclass.lpfnWndProc=WndProc;
	wndclass.hInstance=hInstance;
	wndclass.hIcon=LoadIcon(hInstance,MAKEINTRESOURCE(MYICON));
	wndclass.hCursor=LoadCursor(NULL,IDC_ARROW);
	wndclass.hbrBackground=(HBRUSH)GetStockObject(GRAY_BRUSH);
	wndclass.lpszClassName=szAppName;
	wndclass.lpszMenuName=NULL;
	wndclass.hIconSm=LoadIcon(hInstance,MAKEINTRESOURCE(MYICON));

	RegisterClassEx(&wndclass);

	hwnd=CreateWindowEx(WS_EX_APPWINDOW,
						szAppName,
						TEXT("Cube with Diffuse Light"),
						WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
						X1-X2,
						Y1-Y2,
						WIN_WIDTH,
						WIN_HEIGHT,
						NULL,
						NULL,
						hInstance,
						NULL);

	ghwnd=hwnd;
	Initialize();

	ShowWindow(hwnd,iCmdShow);
	SetForegroundWindow(hwnd);

	SetFocus(hwnd);

	while(bDone==false)
	{
		if(PeekMessage(&msg,NULL,0,0,PM_REMOVE))
		{
			if(msg.message==WM_QUIT)
			{
				bDone=true;
			}else{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}else{
			if(gbActiveWindow==true)
			{
				Display();
				
			}
		}
	}

	return ((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd,UINT iMsg,WPARAM wParam,LPARAM lParam)
{
	void ToggleFullscreen(void);
	void Resize(int,int);
	void UnInitialize(void);

	switch(iMsg)
	{
		case WM_CREATE:
			fprintf(gpFile,"\nlog is started!");
			break;
		case WM_KEYDOWN:
			switch(wParam)
			{
				case VK_ESCAPE:
					DestroyWindow(hwnd);
					break;
				case 0x46:
				case 0x66:
					ToggleFullscreen();
					break;
				case 'a':
				case 'A':
					bAnimate = !bAnimate;
					break;
				case 'l':
				case 'L':
					bLights = !bLights;
					break;
				default:
					break;
			}
			break;
		case WM_SETFOCUS:
			gbActiveWindow=true;
			break;
		case WM_KILLFOCUS:
			gbActiveWindow=false;
			break;

		case WM_ERASEBKGND:
			return 0;
		case WM_SIZE:
			Resize(LOWORD(lParam),HIWORD(lParam));
			break;
		case WM_CLOSE:
			DestroyWindow(hwnd);
			break;
		case WM_DESTROY:
			UnInitialize();
			PostQuitMessage(0);
			break;
		default:
			break;
	}

	return (DefWindowProc(hwnd,iMsg,wParam,lParam));
}

void ToggleFullscreen(void)
{
	MONITORINFO mi={sizeof(MONITORINFO)};
	if(gbFullscreen==false)
	{
		dwStyle=GetWindowLong(ghwnd,GWL_STYLE);
		if(dwStyle & WS_OVERLAPPEDWINDOW)
		{
			if(GetWindowPlacement(ghwnd,&wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd,MONITORINFOF_PRIMARY),&mi))
			{
				fprintf(gpFile,"\nFullscreen Enabled");
				SetWindowLong(ghwnd,GWL_STYLE,(dwStyle & ~WS_OVERLAPPEDWINDOW));
				SetWindowPos(ghwnd,HWND_TOP,mi.rcMonitor.left,mi.rcMonitor.top,mi.rcMonitor.right-mi.rcMonitor.left,mi.rcMonitor.bottom-mi.rcMonitor.top,SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		gbFullscreen=true;
	}
	else{
		SetWindowLong(ghwnd,GWL_STYLE,(dwStyle | WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(ghwnd,&wpPrev);
		SetWindowPos(ghwnd,HWND_TOP,0,0,0,0,SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED );
		fprintf(gpFile,"\nNormal Window");
		gbFullscreen=false;
	}
}

void Initialize(void)
{
	void Resize(int,int);

	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	ghdc=GetDC(ghwnd);

	ZeroMemory(&pfd,sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize=sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion=1;
	pfd.dwFlags=PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType=PFD_TYPE_RGBA;
	pfd.cColorBits=32;
	pfd.cRedBits=8;
	pfd.cGreenBits=8;
	pfd.cBlueBits=8;
	pfd.cAlphaBits=8;
	pfd.cDepthBits=32;

	iPixelFormatIndex=ChoosePixelFormat(ghdc,&pfd);

	if(iPixelFormatIndex==0)
	{
		fprintf(gpFile,"\nChoosePixelFormat() failed to get pixel format index");
		DestroyWindow(ghwnd);
	}

	if(SetPixelFormat(ghdc,iPixelFormatIndex,&pfd)==FALSE)
	{
		fprintf(gpFile,"\nSetPixelFormat() failed to set pixel format for window");
		DestroyWindow(ghwnd);
	}

	ghrc=wglCreateContext(ghdc);
	if(ghrc==NULL)
	{
		fprintf(gpFile,"\nwglCreateContext() failed to get the rendering context");
		DestroyWindow(ghwnd);
	}

	if(wglMakeCurrent(ghdc,ghrc)==FALSE)
	{
		fprintf(gpFile,"\nwglMakeCurrent() failed to swith current context from device to rendering context");
		DestroyWindow(ghwnd);
	}

	GLenum glew_error=glewInit();
	if(glew_error != GLEW_OK)
	{
		fprintf(gpFile,"\nglewInit() failed to enable extension for programmable pipeline");
		DestroyWindow(ghwnd);
	}

	fprintf(gpFile,"\nOpenGL Information are available below:-");
	fprintf(gpFile,"\n*********************************************************************************");

	fprintf(gpFile,"\nOpenGL Vender:%s",glGetString(GL_VENDOR));
	fprintf(gpFile,"\nOpenGL Render:%s",glGetString(GL_RENDERER));
	fprintf(gpFile,"\nOpenGL Version:%s",glGetString(GL_VERSION));
	fprintf(gpFile,"\nOpenGL GLSL Version:%s",glGetString(GL_SHADING_LANGUAGE_VERSION));

	fprintf(gpFile,"\nOpenGL Extension's List:\n");

	GLint numExt;
	glGetIntegerv(GL_NUM_EXTENSIONS,&numExt);

	for(int i=0;i<numExt;i++)
	{
		fprintf(gpFile,"\n%s",glGetStringi(GL_EXTENSIONS,i));
	}
	fprintf(gpFile,"\n*********************************************************************************");
		
	GLint infoLogLength;
	GLint shaderCompileStatus;
	GLint shaderProgramLinkStatus;
	GLchar *szBuffer;
	GLsizei written;
	
	//Shader Code Start--------------------
		gVertexShaderObject=glCreateShader(GL_VERTEX_SHADER);

		const GLchar* vertexShaderSourceCode =
			"#version 460 core"\
			"\n"\
			"in vec4 vPosition;\n" \
			"in vec3 vNormal;\n" \
			"uniform mat4 u_modelViewMatrix;\n" \
			"uniform mat4 u_projectionMatrix;\n" \
			"uniform int u_LKeyPressed;\n" \
			"uniform vec3 u_LD;\n" \
			"uniform vec3 u_KD;\n" \
			"uniform vec4 u_lightPosition;\n" \
			"out vec3 out_diffuseLight;\n" \
			"void main(void)\n" \
			"{\n" \
				"if (u_LKeyPressed == 1)\n" \
				"{\n" \
					"vec4 eyeCoord = u_modelViewMatrix * vPosition;\n" \
					"mat3 normalMatrix = mat3( transpose( inverse( u_modelViewMatrix )));\n" \
					"vec3 transformedNormal = normalize( normalMatrix * vNormal );\n" \
					"vec3 srcLight = normalize( vec3( u_lightPosition - eyeCoord ));\n" \
					"out_diffuseLight = u_LD * u_KD * max( dot(srcLight, transformedNormal), 0.0);\n" \
				"}\n" \
				"gl_Position = u_projectionMatrix * u_modelViewMatrix * vPosition;\n" \
			"}\n";

		glShaderSource(gVertexShaderObject,
						1,
						(const GLchar **)&vertexShaderSourceCode,
						NULL);

		glCompileShader(gVertexShaderObject);

		glGetShaderiv(gVertexShaderObject,
						GL_COMPILE_STATUS,
						&shaderCompileStatus);

						
		if(shaderCompileStatus==GL_FALSE)
		{
			glGetShaderiv(gVertexShaderObject,
							GL_INFO_LOG_LENGTH,
							&infoLogLength);
			if(infoLogLength>0)
			{
				szBuffer=(GLchar*)malloc(sizeof(GLchar) * infoLogLength);

				if(szBuffer!=NULL)
				{
					glGetShaderInfoLog(gVertexShaderObject,
										infoLogLength,
										&written,
										szBuffer);
					
					fprintf(gpFile,"\nERROR:Vertex shader compilation failed log: %s",szBuffer);
					free(szBuffer);
					DestroyWindow(ghwnd);
				}
			}
		}

		infoLogLength=0;
		shaderCompileStatus=0;
		shaderProgramLinkStatus=0;
		szBuffer=NULL;
		written=0;

		gFragmentShaderObject=glCreateShader(GL_FRAGMENT_SHADER);

		const GLchar* fragmentShaderSourceCode =
			"#version 460 core" \
			"\n"\

			"in vec3 out_diffuseLight;"\
			"out vec4 fragColor;"\
			"uniform int u_LKeyPressed;"\

			"void main(void)" \
			"{"\
				"vec4 color;"\
				"if (u_LKeyPressed == 1)"\
				"{"\
					"color=vec4( out_diffuseLight, 1.0 );"\
				"}"\
				"else"\
				"{"\
					"color=vec4(1.0f, 1.0f, 1.0f, 1.0f);"\
				"}"\

				"fragColor=color;"\
			"}";

		glShaderSource(gFragmentShaderObject,
						1,
						(const GLchar**)&fragmentShaderSourceCode,
						NULL);

		glCompileShader(gFragmentShaderObject);

		glGetShaderiv(gFragmentShaderObject,
					GL_COMPILE_STATUS,
					&shaderCompileStatus);

		
		if(shaderCompileStatus==GL_FALSE)
		{
			glGetShaderiv(gVertexShaderObject,
							GL_INFO_LOG_LENGTH,
							&infoLogLength);

			if(infoLogLength>0)
			{
				szBuffer=(GLchar*)malloc(sizeof(GLchar) * infoLogLength);

				if(szBuffer!=NULL)
				{
					glGetShaderInfoLog(gFragmentShaderObject,
										infoLogLength,
										&written,
										szBuffer);
					
					fprintf(gpFile,"\nERROR:Fragment shader compilation failed log: %s",szBuffer);
					free(szBuffer);
					DestroyWindow(ghwnd);
				}
			}
		}
		
		infoLogLength=0;
		shaderCompileStatus=0;
		shaderProgramLinkStatus=0;
		szBuffer=NULL;
		written=0;

		gShaderProgramObject=glCreateProgram();

		glAttachShader(gShaderProgramObject,gVertexShaderObject);
		glAttachShader(gShaderProgramObject,gFragmentShaderObject);

		//Shader Attribute Binding Before Pre Linking of attached Shader
			glBindAttribLocation(gShaderProgramObject,VSV_ATTRIBUTE_POSITION,"vPosition");
			glBindAttribLocation(gShaderProgramObject, VSV_ATTRIBUTE_NORMAL, "vNormal");

		glLinkProgram(gShaderProgramObject);

		glGetProgramiv(gShaderProgramObject,
						GL_LINK_STATUS,
						&shaderProgramLinkStatus);
		if(shaderProgramLinkStatus==GL_FALSE)
		{
			glGetProgramiv(gShaderProgramObject,
						GL_INFO_LOG_LENGTH,
						&infoLogLength);
			if(infoLogLength>0)
			{
				szBuffer=(GLchar*)malloc(sizeof(GLchar)*infoLogLength);
				if(szBuffer!=NULL)
				{
					glGetProgramInfoLog(gShaderProgramObject,
										infoLogLength,
										&written,
										szBuffer);

					fprintf(gpFile,"\nERROR:Shader Program Link failed log: %s",szBuffer);
					free(szBuffer);
					DestroyWindow(ghwnd);
				}
			}
		}
		//Uniform variable Binding after post linking of attached shader
		 
		 modelViewMatrixUniform =glGetUniformLocation(gShaderProgramObject,
												"u_modelViewMatrix");

		 projectionMatrixUniform = glGetUniformLocation(gShaderProgramObject,
														"u_projectionMatrix");

		 LKeyPressedUniform = glGetUniformLocation(gShaderProgramObject,
													"u_LKeyPressed");

		  LDUniform = glGetUniformLocation(gShaderProgramObject,
															"u_LD");

		  KDUniform = glGetUniformLocation(gShaderProgramObject, 
															"u_KD");

		  lightPositionUniform = glGetUniformLocation(gShaderProgramObject,
															"u_lightPosition");

	//Shader Code End--------------------

	//VAO and its respective VBO preparation

		const GLfloat cubeVertices[]=
		{
			1.0f, 1.0f, 1.0f,
			-1.0f, 1.0f, 1.0f,
			-1.0f, -1.0f, 1.0f,
			1.0f, -1.0f, 1.0f,

			1.0f, 1.0f, -1.0f,
			1.0f, 1.0f, 1.0f,
			1.0f, -1.0f, 1.0f,
			1.0f, -1.0f, -1.0f,

			-1.0f, 1.0f, -1.0f,
			1.0f, 1.0f, -1.0f,
			1.0f, -1.0f, -1.0f,
			-1.0f, -1.0f, -1.0f,

			-1.0f, 1.0f, 1.0f,
			-1.0f, 1.0f, -1.0f,
			-1.0f, -1.0f, -1.0f,
			-1.0f, -1.0f, 1.0f,

			1.0f, 1.0f, -1.0f,
			-1.0f, 1.0f, -1.0f,
			-1.0f, 1.0f, 1.0f,
			1.0f, 1.0f, 1.0f,

			1.0f, -1.0f, -1.0f,
			-1.0f, -1.0f, -1.0f,
			-1.0f, -1.0f, 1.0f,
			1.0f, -1.0f, 1.0f
		};

		const GLfloat cubeNormals[] =
		{
			0.0f, 0.0f, 1.0f,
			0.0f, 0.0f, 1.0f,
			0.0f, 0.0f, 1.0f,
			0.0f, 0.0f, 1.0f,

			1.0f, 0.0f, 0.0f,
			1.0f, 0.0f, 0.0f,
			1.0f, 0.0f, 0.0f,
			1.0f, 0.0f, 0.0f,

			0.0f, 0.0f, -1.0f,
			0.0f, 0.0f, -1.0f,
			0.0f, 0.0f, -1.0f,
			0.0f, 0.0f, -1.0f,

			-1.0f, 0.0f, 0.0f,
			-1.0f, 0.0f, 0.0f,
			-1.0f, 0.0f, 0.0f,
			-1.0f, 0.0f, 0.0f,

			0.0f, 1.0f, 0.0f,
			0.0f, 1.0f, 0.0f,
			0.0f, 1.0f, 0.0f,
			0.0f, 1.0f, 0.0f,

			0.0f, -1.0f, 0.0f,
			0.0f, -1.0f, 0.0f,
			0.0f, -1.0f, 0.0f,
			0.0f, -1.0f, 0.0f
		};



		glGenVertexArrays(1,&vao_cube);
		glBindVertexArray(vao_cube);
			glGenBuffers(1,&vbo_cubePosition);
			glBindBuffer(GL_ARRAY_BUFFER,vbo_cubePosition);

				glBufferData(GL_ARRAY_BUFFER,
							sizeof(cubeVertices),
							cubeVertices,
							GL_STATIC_DRAW);

				glVertexAttribPointer(VSV_ATTRIBUTE_POSITION,
									3,
									GL_FLOAT,
									GL_FALSE,
									0,
									NULL);

				glEnableVertexAttribArray(VSV_ATTRIBUTE_POSITION);
			glBindBuffer(GL_ARRAY_BUFFER,0);

			glGenBuffers(1, &vbo_cubeNormal);
			glBindBuffer(GL_ARRAY_BUFFER, vbo_cubeNormal);

			glBufferData(GL_ARRAY_BUFFER,
				sizeof(cubeNormals),
				cubeNormals,
				GL_STATIC_DRAW);

			glVertexAttribPointer(VSV_ATTRIBUTE_NORMAL,
				3,
				GL_FLOAT,
				GL_FALSE,
				0,
				NULL);

			glEnableVertexAttribArray(VSV_ATTRIBUTE_NORMAL);
			glBindBuffer(GL_ARRAY_BUFFER, 0);

		glBindVertexArray(0);


	glClearColor(0.0,0.0,0.0f,0.0f);

	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
		
	bLights = false;
	bAnimate = false;

	Resize(WIN_WIDTH,WIN_HEIGHT);

}

void Resize(int width,int height)
{
	if(height==0)
	{
		height=1;
	}
	glViewport(0,0,(GLsizei)width,(GLsizei)height);

	perspectiveMatrixUniform =vmath::perspective(45.0f,
													(GLfloat)width/(GLfloat)height,
													0.1f,
													100.0f);
}

GLfloat cubeRotate=0.0f;

void Display(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUseProgram(gShaderProgramObject);
			
	if (bLights == true)
	{
		glUniform1i(LKeyPressedUniform, 1);

		glUniform3f(LDUniform, 1.0f, 1.0f, 1.0f);

		glUniform3f(KDUniform, 0.5f, 0.5f, 0.5f);

		GLfloat lightPosition[] = { 0.0f,0.0f,2.0f , 1.0f};//4 th element defines Positional light

		glUniform4fv(lightPositionUniform,
					1,//Number of array 
					(GLfloat *)lightPosition);
	}
	else 
	{
		glUniform1i(LKeyPressedUniform, 0);
	}

		mat4 modelViewMatrix;
		mat4 translateMatrix;
		mat4 rotateMatrix;
	

		rotateMatrix=vmath::rotate(cubeRotate,1.0f,0.0f,0.0f);
		rotateMatrix*=vmath::rotate(cubeRotate,0.0f,1.0f,0.0f);
		rotateMatrix*=vmath::rotate(cubeRotate,0.0f,0.0f,1.0f);

		translateMatrix=vmath::translate(0.0f,0.0f,-6.0f);

		modelViewMatrix=translateMatrix * rotateMatrix  ;

		glUniformMatrix4fv(modelViewMatrixUniform,
						1,
						GL_FALSE,
						modelViewMatrix);

		glUniformMatrix4fv(projectionMatrixUniform,
							1,
							GL_FALSE,
							perspectiveMatrixUniform);

		glBindVertexArray(vao_cube);
			glDrawArrays(GL_TRIANGLE_FAN,0,4);
			glDrawArrays(GL_TRIANGLE_FAN,4,4);
			glDrawArrays(GL_TRIANGLE_FAN,8,4);
			glDrawArrays(GL_TRIANGLE_FAN,12,4);
			glDrawArrays(GL_TRIANGLE_FAN,16,4);
			glDrawArrays(GL_TRIANGLE_FAN,20,4);
		glBindVertexArray(0);

	glUseProgram(0);

	SwapBuffers(ghdc);
	if (bAnimate == true)
	{
		cubeRotate += 1.0f;
	}
	
}

void UnInitialize(void)
{
	if(gbFullscreen==true)
	{
		SetWindowLong(ghwnd,GWL_STYLE,(dwStyle | WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(ghwnd,&wpPrev);
		SetWindowPos(ghwnd,HWND_TOP,0,0,0,0,SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED );
		fprintf(gpFile,"\nNormal Window");
		gbFullscreen=false;
	}

	if(vao_cube)
	{
		glDeleteVertexArrays(1,
							&vao_cube);
	}
	vao_cube=0;

	if(vbo_cubePosition)
	{
		glDeleteBuffers(1,
					&vbo_cubePosition);
	}
	vbo_cubePosition=0;


	if (vbo_cubeNormal)
	{
		glDeleteBuffers(1,
			&vbo_cubeNormal);
	}
	vbo_cubeNormal = 0;

	if(gShaderProgramObject)
	{
		glUseProgram(gShaderProgramObject);

		GLsizei shaderCount;
		GLsizei tempShaderCount;
		GLuint *pShader=NULL;

		glGetProgramiv(gShaderProgramObject,GL_ATTACHED_SHADERS,&shaderCount);

		pShader=(GLuint *)malloc(sizeof(GLuint)*shaderCount);
		if(pShader==NULL)
		{
			fprintf(gpFile,"\nFailed to allocate memory for pShader");
		}else{
			glGetAttachedShaders(gShaderProgramObject,shaderCount,&tempShaderCount,pShader);
			
			fprintf(gpFile,"\nShader program has actual attached shader count is:%d",tempShaderCount);
			
			for(GLsizei i=0;i<shaderCount;i++)
			{
				glDetachShader(gShaderProgramObject,pShader[i]);
				glDeleteShader(pShader[i]);
				pShader[i]=0;
			}
			free(pShader);
			
			glDeleteProgram(gShaderProgramObject);
			gShaderProgramObject=0;
			glUseProgram(0);
		}
	}

	if(wglGetCurrentContext()==ghrc)
	{
		wglMakeCurrent(NULL,NULL);
	}

	if(ghrc)
	{
		wglDeleteContext(ghrc);
	}
		ghrc=NULL;

	if(ghdc)
	{
		ReleaseDC(ghwnd,ghdc);
	}
		ghdc=NULL;
	
	if(gpFile)
	{
		fprintf(gpFile,"\nlog is ended!");
		fclose(gpFile);
	}
		gpFile=NULL;
}
