#include<windows.h>
#include<stdio.h>
#include"Checkerboard.h"

#include<GL/GLEW.h>
#include<GL/GL.h>
#include"vmath.h"

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

#define CHECK_IMAGE_WIDTH 64
#define CHECK_IMAGE_HEIGHT 64

#pragma comment(lib,"GLEW32.lib")
#pragma comment(lib,"OpenGL32.lib")

using namespace vmath;

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

FILE* gpFile = NULL;
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
bool gbFullscreen = false;
bool gbActiveWindow = false;
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

enum{
	VSV_ATTRIBUTE_POSITION,
	VSV_ATTRIBUTE_COLOR,
	VSV_ATTRIBUTE_NORMAL,
	VSV_ATTRIBUTE_TEXCOORD,
};

GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

GLuint vao_checkerboard;
GLuint vbo_checkerboardPosition;
GLuint vbo_checkerboardTexcoord;

GLuint mvpMatrixUniform;
GLuint textureSamplerUniform;

mat4 perspectiveProjectionMatrix;

GLubyte checkImage[CHECK_IMAGE_HEIGHT][CHECK_IMAGE_WIDTH][4];
GLuint CheckerboardTextureID;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	void Initialize(void);
	void Display(void);

	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("OpenGLPPCheckerBoard");
	int X1, X2, Y1, Y2;
	bool bDone = false;

	if (fopen_s(&gpFile, "log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Failed to Create log file!"), TEXT("Error"), MB_OK | MB_ICONERROR);
		exit(0);
	}
	
	X1 = GetSystemMetrics(SM_CXSCREEN) / 2;
	Y1 = GetSystemMetrics(SM_CYSCREEN) / 2;

	X2 = WIN_WIDTH / 2;
	Y2 = WIN_HEIGHT / 2;

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style =  CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(GRAY_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));

	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("OpenGL Programmable Pipeline Texture Checkerboard"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		X1 - X2,//100,
		Y1 - Y2,//100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;
	Initialize();

	ShowWindow(hwnd, iCmdShow);

	SetForegroundWindow(hwnd);
	SetFocus(hwnd);
	


	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
		
			if (gbActiveWindow == true)
			{
				Display();
			}
		}
	}

	return ((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void ToggleFullscreen(void);
	void Resize(int, int);
	void UnInitialize(void);

	switch (iMsg)
	{
	case WM_CREATE:
		fprintf(gpFile, "\nlog is Started!");
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
			case VK_ESCAPE:
				DestroyWindow(hwnd);
				break;
			case 0x46:
			case 0x66:
				ToggleFullscreen();
				break;
			default:
				break;
		}
		break;
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;
	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;

	case WM_ERASEBKGND:
		return 0;
	case WM_SIZE:
		Resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;
	case WM_DESTROY:
		UnInitialize();
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}
void ToggleFullscreen(void)
{
	MONITORINFO mi = { sizeof(MONITORINFO) };
	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW) 
		{
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				fprintf(gpFile, "\nFullscreen Enabled");
				SetWindowLong(ghwnd, GWL_STYLE, (dwStyle & ~WS_OVERLAPPEDWINDOW));
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
		gbFullscreen = true;
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, (dwStyle | WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		fprintf(gpFile, "\nNormal Window");
		ShowCursor(TRUE);
		gbFullscreen = false;
	}
}

void Initialize(void)
{
	void Resize(int, int);
	void LoadGLTexture(void);
	
	//Local variable initialization
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	ghdc = GetDC(ghwnd);

	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER ;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits= 32 ;


	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (iPixelFormatIndex == 0 )
	{
		fprintf(gpFile, "\nChoosePixelFormat() Failed to get pixel format index");
		DestroyWindow(ghwnd);
	}

	if ( SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE )
	{
		fprintf(gpFile, "\nSetPixelFormat() Failed to set pixel format for window");
		DestroyWindow(ghwnd);
	}

	ghrc = wglCreateContext(ghdc);
	if ( ghrc == NULL)
	{
		fprintf(gpFile, "\nwglCreateContext() Failed to get the rendering context");
		DestroyWindow(ghwnd);
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE )
	{
		fprintf(gpFile, "\nwglMakeCurrent() Failed to switch current context from device to rendering context");
		DestroyWindow(ghwnd);
	}

	GLenum glew_error=glewInit();
	if(glew_error != GLEW_OK)
	{
		fprintf(gpFile, "\nglewInit failed to enable extension for Programmable Pipeline");
		DestroyWindow(ghwnd);

	}
	fprintf(gpFile, "\nOpenGL Information are availble below:-");
	fprintf(gpFile, "\n*****************************************************");

	fprintf(gpFile, "\nOpenGL Vender:%s",glGetString(GL_VENDOR));
	fprintf(gpFile, "\nOpenGL Renderer:%s",glGetString(GL_RENDERER));
	fprintf(gpFile, "\nOpenGL Version:%s",glGetString(GL_VERSION));
	fprintf(gpFile, "\nOpenGL GLSL Version:%s",glGetString(GL_SHADING_LANGUAGE_VERSION));
	
	fprintf(gpFile, "\nOpenGL Extension's list:\n");
	GLint numExt;
	glGetIntegerv(GL_NUM_EXTENSIONS,&numExt);

	for(int i=0;i<numExt;i++)
	{
		fprintf(gpFile, "\n%s",glGetStringi(GL_EXTENSIONS,i));
	}
	fprintf(gpFile, "\n*****************************************************");

	GLint infoLogLength;
	GLint shaderCompileStatus;
	GLint shaderProgramLinkStatus;
	GLchar *szBuffer;
	GLsizei written;

	//Shader Code Start--------------------
		gVertexShaderObject=glCreateShader(GL_VERTEX_SHADER);

		const GLchar *vertexShaderSrc=
		"#version 460 core"\
		"\n"\
		"in vec4 vPosition;"\
		"in vec2 vTexCoord;"\
		"out vec2 out_TexCoord;"\

		"uniform mat4 u_mvpMatrix;"\

		"void main(void)"\
		"{"\
			"out_TexCoord=vTexCoord;"\
			"gl_Position=u_mvpMatrix * vPosition;"\
		"}";

		glShaderSource(gVertexShaderObject,
						1,
						(const GLchar **)&vertexShaderSrc,
						NULL);

		glCompileShader(gVertexShaderObject);

		glGetShaderiv(gVertexShaderObject,
						GL_COMPILE_STATUS,
						&shaderCompileStatus);

		if(shaderCompileStatus == GL_FALSE)
		{
			glGetShaderiv(gVertexShaderObject,
						GL_INFO_LOG_LENGTH,
						&infoLogLength);

			if(infoLogLength > 0)
			{
				szBuffer=(GLchar *)malloc(sizeof(GLchar) * infoLogLength);

				if(szBuffer!=NULL)
				{
					glGetShaderInfoLog(gVertexShaderObject,
										infoLogLength,
										&written,
										szBuffer);
					fprintf(gpFile,
							"\nERROR | Vertex Shader Compilation Failed Log : %s",
							szBuffer);
					free(szBuffer);
					DestroyWindow(ghwnd);
				}
			}
		}

		infoLogLength=0;
		shaderCompileStatus=0;
		shaderProgramLinkStatus=0;
		szBuffer=NULL;
		written=0;

		gFragmentShaderObject=glCreateShader(GL_FRAGMENT_SHADER);

		const GLchar *fragmentShaderSrc=
		"#version 460 core"\
		"\n"\
		
		"in vec2 out_TexCoord;"\
		"out vec4 fragColor;"\

		"uniform sampler2D u_texture_sampler;"\

		"void main(void)"\
		"{"\
			"fragColor=texture(u_texture_sampler , out_TexCoord);"\
		"}";

		glShaderSource(gFragmentShaderObject,
						1,
						(const GLchar**)&fragmentShaderSrc,
						NULL);
		
		glCompileShader(gFragmentShaderObject);

		glGetShaderiv(gFragmentShaderObject,
						GL_COMPILE_STATUS,
						&shaderCompileStatus);

		if(shaderCompileStatus==GL_FALSE)
		{
			glGetShaderiv(gFragmentShaderObject,
						GL_INFO_LOG_LENGTH,
						&infoLogLength);

			if(infoLogLength > 0 )
			{
				szBuffer=(GLchar*)malloc(sizeof(GLchar) * infoLogLength);

				glGetShaderInfoLog(gFragmentShaderObject,
										infoLogLength,
										&written,
										szBuffer);

				fprintf(gpFile,
						"\nERROR | Fragment Shader Compilation failed log %s",
						szBuffer);

				free(szBuffer);

				DestroyWindow(ghwnd);
			}
		}

		infoLogLength=0;
		shaderCompileStatus=0;
		shaderProgramLinkStatus=0;
		szBuffer=NULL;
		written=0;

		gShaderProgramObject=glCreateProgram();

		glAttachShader(gShaderProgramObject , gVertexShaderObject);
		glAttachShader(gShaderProgramObject , gFragmentShaderObject);

		//Shader Attribute Binding Before Pre Linking of attached Shader
			glBindAttribLocation(gShaderProgramObject , VSV_ATTRIBUTE_POSITION ,  "vPosition");
			glBindAttribLocation(gShaderProgramObject , VSV_ATTRIBUTE_TEXCOORD , "vTexCoord");

		glLinkProgram(gShaderProgramObject);

		glGetProgramiv(gShaderProgramObject,
						GL_LINK_STATUS,
						&shaderProgramLinkStatus);

		if(shaderProgramLinkStatus==GL_FALSE)
		{
			glGetProgramiv(gShaderProgramObject,
							GL_INFO_LOG_LENGTH,
							&infoLogLength);
			
			if(infoLogLength > 0)
			{
				szBuffer=(GLchar *)malloc(sizeof(GLchar) * infoLogLength);
				if(szBuffer!=NULL)
				{
					glGetProgramInfoLog(gShaderProgramObject,
										infoLogLength,
										&written,
										szBuffer);
					fprintf(gpFile,
							"\nERROR | Shader Program Link Failed Log %s",
							szBuffer);
					free(szBuffer);
					DestroyWindow(ghwnd);
				}
			}
		}
		
		//Uniform variable Binding after post linking of attached shader
			mvpMatrixUniform=glGetUniformLocation(gShaderProgramObject,
													"u_mvpMatrix");
			textureSamplerUniform=glGetUniformLocation(gShaderProgramObject,
														"u_texture_sampler");

	//Shader Code End--------------------

	//VAO and its respective VBO preparation
	const GLfloat CheckerboardTexCoord[]=
	{
		0.0f,0.0f,
		0.0f,1.0f,
		1.0f,1.0f,
		1.0f,0.0f
	};

	glGenVertexArrays(1,&vao_checkerboard);
	glBindVertexArray(vao_checkerboard);

		glGenBuffers(1,&vbo_checkerboardPosition);
		glBindBuffer(GL_ARRAY_BUFFER,vbo_checkerboardPosition);

			glBufferData(GL_ARRAY_BUFFER,
						3 * 4 * sizeof(GLfloat),
						NULL,
						GL_DYNAMIC_DRAW);	

			glVertexAttribPointer(VSV_ATTRIBUTE_POSITION,
								3,
								GL_FLOAT,
								GL_FALSE,
								0,
								NULL);

			glEnableVertexAttribArray(VSV_ATTRIBUTE_POSITION);
		glBindBuffer(GL_ARRAY_BUFFER,0);

		glGenBuffers(1,&vbo_checkerboardTexcoord);
		glBindBuffer(GL_ARRAY_BUFFER,vbo_checkerboardTexcoord);

			glBufferData(GL_ARRAY_BUFFER,
						sizeof(CheckerboardTexCoord),
						CheckerboardTexCoord,
						GL_STATIC_DRAW);
			
			glVertexAttribPointer(VSV_ATTRIBUTE_TEXCOORD,
								2,
								GL_FLOAT,
								GL_FALSE,
								0,
								NULL);

			glEnableVertexAttribArray(VSV_ATTRIBUTE_TEXCOORD);
		glBindBuffer(GL_ARRAY_BUFFER,0);

	glBindVertexArray(0);


	//SetColor
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	glEnable(GL_TEXTURE_2D);
 	LoadGLTexture();

	//Warmup resize call
	perspectiveProjectionMatrix = mat4 :: identity();
	Resize(WIN_WIDTH, WIN_HEIGHT);
	
}

void Resize(int width, int height)
{
	if (height == 0)
	{
		height = 1;
	}
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	perspectiveProjectionMatrix = vmath :: perspective(45.0f,
														(GLfloat)width/(GLfloat)height,
														0.1f,
														100.0f); 
}

 GLfloat CheckerboardRectangle1[]=
{
	-2.0f, -1.0f, 0.0f,
	-2.0f, 1.0f, 0.0f,
	0.0f, 1.0f, 0.0f,
	0.0f, -1.0f, 0.0f
};
 GLfloat CheckerboardRectangle2[]=
{
	1.0f, -1.0f, 0.0f,
	1.0f, 1.0f, 0.0f,
	2.41421f, 1.0f, -1.41421f,
	2.41421f, -1.0f, -1.41421f
};

void Display(void)
{
	mat4 modelViewMatrix;
	mat4 modelViewProjectionMatrix;
	mat4 translateMatrix;
	GLfloat  *rectPosition;
	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

	glUseProgram(gShaderProgramObject);

		translateMatrix = vmath :: translate(0.0f,0.0f,-3.6f);
		modelViewMatrix = translateMatrix;
		modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

		glUniformMatrix4fv(mvpMatrixUniform,
							1,
							GL_FALSE,
							modelViewProjectionMatrix);

		
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, CheckerboardTextureID);
		glUniform1i(textureSamplerUniform,0);

		rectPosition=CheckerboardRectangle1;

		glBindVertexArray(vao_checkerboard);

			glBindBuffer(GL_ARRAY_BUFFER,vbo_checkerboardPosition);
				glBufferData(GL_ARRAY_BUFFER,
							3 * 4 *sizeof(GLfloat),
							rectPosition,
							GL_DYNAMIC_DRAW);
				glVertexAttribPointer(VSV_ATTRIBUTE_POSITION,
										3,
										GL_FLOAT,
										GL_FALSE,
										0,
										NULL);
				glEnableVertexAttribArray(VSV_ATTRIBUTE_POSITION);
			glBindBuffer(GL_ARRAY_BUFFER,0);

			glDrawArrays(GL_TRIANGLE_FAN, 0, 4);	

		glBindVertexArray(0);

		rectPosition=CheckerboardRectangle2;

		glBindVertexArray(vao_checkerboard);
			
			glBindBuffer(GL_ARRAY_BUFFER,vbo_checkerboardPosition);
				glBufferData(GL_ARRAY_BUFFER,
							3 * 4 *sizeof(GLfloat),
							rectPosition,
							GL_DYNAMIC_DRAW);
				glVertexAttribPointer(VSV_ATTRIBUTE_POSITION,
										3,
										GL_FLOAT,
										GL_FALSE,
										0,
										NULL);
				glEnableVertexAttribArray(VSV_ATTRIBUTE_POSITION);
			glBindBuffer(GL_ARRAY_BUFFER,0);

			glDrawArrays(GL_TRIANGLE_FAN, 0, 4);	

		glBindVertexArray(0);

	glUseProgram(0);


	SwapBuffers(ghdc);
	
}
void UnInitialize(void)
{
	if (gbFullscreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, (dwStyle | WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

	if(vao_checkerboard)
	{
		glDeleteVertexArrays(1,
							&vao_checkerboard);
	}
	vao_checkerboard=0;

	if(vbo_checkerboardPosition)
	{
		glDeleteBuffers(1,
					&vbo_checkerboardPosition);
	}
	vbo_checkerboardPosition=0;

	if(vbo_checkerboardTexcoord)
	{
		glDeleteBuffers(1,
					&vbo_checkerboardTexcoord);
	}
	vbo_checkerboardTexcoord=0;

	if(gShaderProgramObject)
	{
		glUseProgram(gShaderProgramObject);

		GLsizei shaderCount;
		GLsizei tempShaderCount;
		GLuint *pShader=NULL;

		glGetProgramiv(gShaderProgramObject,GL_ATTACHED_SHADERS,&shaderCount);

		pShader=(GLuint *)malloc(sizeof(GLuint)*shaderCount);
		if(pShader==NULL)
		{
			fprintf(gpFile,"\nFailed to allocate memory for pShader");
		}else{
			glGetAttachedShaders(gShaderProgramObject,shaderCount,&tempShaderCount,pShader);
			
			fprintf(gpFile,"\nShader program has actual attached shader count is:%d",tempShaderCount);
			
			for(GLsizei i=0;i<shaderCount;i++)
			{
				glDetachShader(gShaderProgramObject,pShader[i]);
				glDeleteShader(pShader[i]);
				pShader[i]=0;
			}
			free(pShader);
			
			glDeleteProgram(gShaderProgramObject);
			gShaderProgramObject=0;
			glUseProgram(0);
		}
	}

	glDeleteTextures(1,&CheckerboardTextureID);

	
	if (wglGetCurrentContext() == ghrc ) {
		wglMakeCurrent(NULL, NULL);
	}


	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (gpFile)
	{
		fprintf(gpFile, "\nlog is Ended!");
		fclose(gpFile);
		gpFile = NULL;
	}
}


void LoadGLTexture()
{	
	void MakeCheckImage(void);
	MakeCheckImage();
		
		glPixelStorei(GL_UNPACK_ALIGNMENT,
					1);
		
		glGenTextures(1 ,  
					&CheckerboardTextureID);

		glBindTexture( GL_TEXTURE_2D,
						CheckerboardTextureID);
		
		glTexParameteri( GL_TEXTURE_2D,
						GL_TEXTURE_MAG_FILTER,
						GL_NEAREST
						);
		glTexParameteri( GL_TEXTURE_2D,
						GL_TEXTURE_MIN_FILTER,
						GL_NEAREST
					   );
		glTexParameteri( GL_TEXTURE_2D,
						GL_TEXTURE_WRAP_S,
						GL_REPEAT
						);
		glTexParameteri( GL_TEXTURE_2D,
						GL_TEXTURE_WRAP_T,
						GL_REPEAT
					   );
	

		glTexImage2D(GL_TEXTURE_2D,
					0,
					GL_RGBA,
					CHECK_IMAGE_WIDTH,
					CHECK_IMAGE_HEIGHT,
					0,
					GL_RGBA,
					GL_UNSIGNED_BYTE,
					checkImage);

		glGenerateMipmap(GL_TEXTURE_2D);

}




void MakeCheckImage(void)
{
	int vsv_i,vsv_j,vsv_c;

    for(vsv_i=0;vsv_i<CHECK_IMAGE_HEIGHT;vsv_i++){

        for(vsv_j=0;vsv_j<CHECK_IMAGE_WIDTH;vsv_j++){

            vsv_c=( (vsv_i & 0x8 )==0)^( (vsv_j & 0x8 )==0);
			vsv_c*=255;
			checkImage[vsv_i][vsv_j][0]=(GLubyte)vsv_c;
			checkImage[vsv_i][vsv_j][1]=(GLubyte)vsv_c;
			checkImage[vsv_i][vsv_j][2]=(GLubyte)vsv_c;
			checkImage[vsv_i][vsv_j][3]=255;
        }
    }
}










/*
cl /c /EHsc /I C:\glew-2.1.0-win32\glew-2.1.0\include Checkerboard.cpp 
rc Checkerboard.rc 
link Checkerboard.obj Checkerboard.res user32.lib gdi32.lib kernel32.lib /LIBPATH:C:\glew-2.1.0-win32\glew-2.1.0\lib\Release\x64 /SUBSYSTEM:WINDOWS
Checkerboard.exe 
*/















