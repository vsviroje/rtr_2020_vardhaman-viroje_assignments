#include<windows.h>
#include<stdio.h>
#include"render_to_texture.h"
#include "vmath.h"
#include<GL/GLEW.h>
#include<GL/GL.h>

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

#pragma comment(lib,"GLEW32.lib")
#pragma comment(lib,"OpenGL32.lib")

using namespace vmath;

LRESULT CALLBACK WndProc(HWND,UINT,WPARAM,LPARAM);

FILE* gpFile=NULL;
DWORD dwStyle;
WINDOWPLACEMENT wpPrev={sizeof(WINDOWPLACEMENT)};
bool gbFullscreen=false;
bool gbActiveWindow=false;
HWND ghwnd=NULL;
HDC ghdc=NULL;
HGLRC ghrc=NULL;

enum{
	VSV_ATTRIBUTE_POSITION=0,
	VSV_ATTRIBUTE_COLOR,
	VSV_ATTRIBUTE_TEXCOORD,
	VSV_ATTRIBUTE_NORMAL,
};

GLuint gVertexShaderObject_simple;
GLuint gFragmentShaderObject_simple;
GLuint gShaderProgramObject_simple;

GLuint gVertexShaderObject_render_to_texture;
GLuint gFragmentShaderObject_render_to_texture;
GLuint gShaderProgramObject_render_to_texture;

GLuint vao_pyramid;
GLuint vao_cube;

GLuint vbo_pyramidColor;
GLuint vbo_pyramidPosition;
GLuint vbo_pyramidTexture;

GLuint vbo_cubePosition;
GLuint vbo_cubeColor;
GLuint vbo_cubeTexture;

GLuint mvpMatrixUniform_simple;
GLuint mvpMatrixUniform_render_to_texture;

GLuint textureSamplerUniform_simple;
GLuint textureSamplerUniform_render_to_texture;

//Frame_buffer changes
GLuint gFrameBuffers = 0;
GLuint gColorTexture = 0;
GLuint gDepthTexture = 0;

int gWidth, gHeight;


//common
mat4 perspectiveProjectionMatrix;

GLuint Kundali_Texture, Stone_Texture;//texture variable


int WINAPI WinMain(HINSTANCE hInstance,HINSTANCE hPrevInstance,LPSTR lpszCmdline,int iCmdShow)
{
	void Initialize(void);
	void Display(void);

	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[]=TEXT("rendertotexture");
	int X1,X2,Y1,Y2;
	bool bDone=false;

	if(fopen_s(&gpFile,"log.txt","w")!=0)
	{
		MessageBox(NULL,TEXT("Failed to create log file!"),TEXT("ERROR"),MB_OK | MB_ICONERROR);
		exit(0);
	}

	X1=GetSystemMetrics(SM_CXSCREEN)/2;
	Y1=GetSystemMetrics(SM_CYSCREEN)/2;

	X2=WIN_WIDTH/2;
	Y2=WIN_HEIGHT/2;

	wndclass.cbSize=sizeof(WNDCLASSEX);
	wndclass.style=CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra=0;
	wndclass.cbWndExtra=0;
	wndclass.lpfnWndProc=WndProc;
	wndclass.hInstance=hInstance;
	wndclass.hIcon=LoadIcon(hInstance,MAKEINTRESOURCE(MYICON));
	wndclass.hCursor=LoadCursor(NULL,IDC_ARROW);
	wndclass.hbrBackground=(HBRUSH)GetStockObject(GRAY_BRUSH);
	wndclass.lpszClassName=szAppName;
	wndclass.lpszMenuName=NULL;
	wndclass.hIconSm=LoadIcon(hInstance,MAKEINTRESOURCE(MYICON));

	RegisterClassEx(&wndclass);

	hwnd=CreateWindowEx(WS_EX_APPWINDOW,
						szAppName,
						TEXT("Render To Texture"),
						WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
						X1-X2,
						Y1-Y2,
						WIN_WIDTH,
						WIN_HEIGHT,
						NULL,
						NULL,
						hInstance,
						NULL);

	ghwnd=hwnd;
	Initialize();

	ShowWindow(hwnd,iCmdShow);
	SetForegroundWindow(hwnd);

	SetFocus(hwnd);

	while(bDone==false)
	{
		if(PeekMessage(&msg,NULL,0,0,PM_REMOVE))
		{
			if(msg.message==WM_QUIT)
			{
				bDone=true;
			}else{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}else{
			if(gbActiveWindow==true)
			{
				Display();
			}
		}
	}

	return ((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd,UINT iMsg,WPARAM wParam,LPARAM lParam)
{
	void ToggleFullscreen(void);
	void Resize(int,int);
	void UnInitialize(void);

	switch(iMsg)
	{
		case WM_CREATE:
			fprintf(gpFile,"\nlog is started!");
			break;
		case WM_KEYDOWN:
			switch(wParam)
			{
				case VK_ESCAPE:
					DestroyWindow(hwnd);
					break;
				case 0x46:
				case 0x66:
					ToggleFullscreen();
					break;
				default:
					break;
			}
			break;
		case WM_SETFOCUS:
			gbActiveWindow=true;
			break;
		case WM_KILLFOCUS:
			gbActiveWindow=false;
			break;

		case WM_ERASEBKGND:
			return 0;
		case WM_SIZE:
			Resize(LOWORD(lParam),HIWORD(lParam));
			break;
		case WM_CLOSE:
			DestroyWindow(hwnd);
			break;
		case WM_DESTROY:
			UnInitialize();
			PostQuitMessage(0);
			break;
		default:
			break;
	}

	return (DefWindowProc(hwnd,iMsg,wParam,lParam));
}

void ToggleFullscreen(void)
{
	MONITORINFO mi={sizeof(MONITORINFO)};
	if(gbFullscreen==false)
	{
		dwStyle=GetWindowLong(ghwnd,GWL_STYLE);
		if(dwStyle & WS_OVERLAPPEDWINDOW)
		{
			if(GetWindowPlacement(ghwnd,&wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd,MONITORINFOF_PRIMARY),&mi))
			{
				fprintf(gpFile,"\nFullscreen Enabled");
				SetWindowLong(ghwnd,GWL_STYLE,(dwStyle & ~WS_OVERLAPPEDWINDOW));
				SetWindowPos(ghwnd,HWND_TOP,mi.rcMonitor.left,mi.rcMonitor.top,mi.rcMonitor.right-mi.rcMonitor.left,mi.rcMonitor.bottom-mi.rcMonitor.top,SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		gbFullscreen=true;
	}
	else{
		SetWindowLong(ghwnd,GWL_STYLE,(dwStyle | WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(ghwnd,&wpPrev);
		SetWindowPos(ghwnd,HWND_TOP,0,0,0,0,SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED );
		fprintf(gpFile,"\nNormal Window");
		gbFullscreen=false;
	}
}

void Initialize(void)
{
	void Resize(int,int);
	bool LoadGLTexture(GLuint*, TCHAR[]);

	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	ghdc=GetDC(ghwnd);

	ZeroMemory(&pfd,sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize=sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion=1;
	pfd.dwFlags=PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType=PFD_TYPE_RGBA;
	pfd.cColorBits=32;
	pfd.cRedBits=8;
	pfd.cGreenBits=8;
	pfd.cBlueBits=8;
	pfd.cAlphaBits=8;
	pfd.cDepthBits=32;

	iPixelFormatIndex=ChoosePixelFormat(ghdc,&pfd);

	if(iPixelFormatIndex==0)
	{
		fprintf(gpFile,"\nChoosePixelFormat() failed to get pixel format index");
		DestroyWindow(ghwnd);
	}

	if(SetPixelFormat(ghdc,iPixelFormatIndex,&pfd)==FALSE)
	{
		fprintf(gpFile,"\nSetPixelFormat() failed to set pixel format for window");
		DestroyWindow(ghwnd);
	}

	ghrc=wglCreateContext(ghdc);
	if(ghrc==NULL)
	{
		fprintf(gpFile,"\nwglCreateContext() failed to get the rendering context");
		DestroyWindow(ghwnd);
	}

	if(wglMakeCurrent(ghdc,ghrc)==FALSE)
	{
		fprintf(gpFile,"\nwglMakeCurrent() failed to swith current context from device to rendering context");
		DestroyWindow(ghwnd);
	}

	GLenum glew_error=glewInit();
	if(glew_error != GLEW_OK)
	{
		fprintf(gpFile,"\nglewInit() failed to enable extension for programmable pipeline");
		DestroyWindow(ghwnd);
	}

	fprintf(gpFile,"\nOpenGL Information are available below:-");
	fprintf(gpFile,"\n*********************************************************************************");

	fprintf(gpFile,"\nOpenGL Vender:%s",glGetString(GL_VENDOR));
	fprintf(gpFile,"\nOpenGL Render:%s",glGetString(GL_RENDERER));
	fprintf(gpFile,"\nOpenGL Version:%s",glGetString(GL_VERSION));
	fprintf(gpFile,"\nOpenGL GLSL Version:%s",glGetString(GL_SHADING_LANGUAGE_VERSION));

	fprintf(gpFile,"\nOpenGL Extension's List:\n");

	GLint numExt;
	glGetIntegerv(GL_NUM_EXTENSIONS,&numExt);

	for(int i=0;i<numExt;i++)
	{
		fprintf(gpFile,"\n%s",glGetStringi(GL_EXTENSIONS,i));
	}
	fprintf(gpFile,"\n*********************************************************************************");
		
	GLint infoLogLength;
	GLint shaderCompileStatus;
	GLint shaderProgramLinkStatus;
	GLchar *szBuffer;
	GLsizei written;
	
	//Shader Code Start--------------------
	gVertexShaderObject_simple =glCreateShader(GL_VERTEX_SHADER);

		const GLchar *vertexShaderSourceCode=
			"#version 460 core" \
			"\n" \
			
			"in vec4 vPosition;"\
			"in vec4 vColor;"\
			"in vec2 vTexCoord;"\
			"out vec4 out_color;"\
			"out vec2 out_TexCoord;"\
			"uniform mat4 u_mvpMatrix;"\
			"void main(void)" \
			"{" \
				"out_color=vColor;"\
				"out_TexCoord=vTexCoord;"\
				"gl_Position=u_mvpMatrix * vPosition;"\
			"}";

		glShaderSource(gVertexShaderObject_simple,
						1,
						(const GLchar **)&vertexShaderSourceCode,
						NULL);

		glCompileShader(gVertexShaderObject_simple);

		glGetShaderiv(gVertexShaderObject_simple,
						GL_COMPILE_STATUS,
						&shaderCompileStatus);

						
		if(shaderCompileStatus==GL_FALSE)
		{
			glGetShaderiv(gVertexShaderObject_simple,
							GL_INFO_LOG_LENGTH,
							&infoLogLength);
			if(infoLogLength>0)
			{
				szBuffer=(GLchar*)malloc(sizeof(GLchar) * infoLogLength);

				if(szBuffer!=NULL)
				{
					glGetShaderInfoLog(gVertexShaderObject_simple,
										infoLogLength,
										&written,
										szBuffer);
					
					fprintf(gpFile,"\nERROR:Vertex shader compilation failed log: %s",szBuffer);
					free(szBuffer);
					DestroyWindow(ghwnd);
				}
			}
		}

		infoLogLength=0;
		shaderCompileStatus=0;
		shaderProgramLinkStatus=0;
		szBuffer=NULL;
		written=0;

		gFragmentShaderObject_simple =glCreateShader(GL_FRAGMENT_SHADER);

		const GLchar *fragmentShaderSourceCode=
			"#version 450" \
			"\n" \

			"in vec4 out_color;"\
			"in vec2 out_TexCoord;"\
			"uniform sampler2D u_texture_sampler;"\
			"out vec4 fragColor;"\
			"void main(void)" \
			"{"\
				"fragColor=out_color;"\
				"fragColor=texture(u_texture_sampler,out_TexCoord);"\
			"}";

		glShaderSource(gFragmentShaderObject_simple,
						1,
						(const GLchar**)&fragmentShaderSourceCode,
						NULL);

		glCompileShader(gFragmentShaderObject_simple);

		glGetShaderiv(gFragmentShaderObject_simple,
					GL_COMPILE_STATUS,
					&shaderCompileStatus);

		
		if(shaderCompileStatus==GL_FALSE)
		{
			glGetShaderiv(gFragmentShaderObject_simple,
							GL_INFO_LOG_LENGTH,
							&infoLogLength);

			if(infoLogLength>0)
			{
				szBuffer=(GLchar*)malloc(sizeof(GLchar) * infoLogLength);

				if(szBuffer!=NULL)
				{
					glGetShaderInfoLog(gFragmentShaderObject_simple,
										infoLogLength,
										&written,
										szBuffer);
					
					fprintf(gpFile,"\nERROR:Fragment shader compilation failed log: %s",szBuffer);
					free(szBuffer);
					DestroyWindow(ghwnd);
				}
			}
		}
		
		infoLogLength=0;
		shaderCompileStatus=0;
		shaderProgramLinkStatus=0;
		szBuffer=NULL;
		written=0;

		gShaderProgramObject_simple =glCreateProgram();

		glAttachShader(gShaderProgramObject_simple, gVertexShaderObject_simple);
		glAttachShader(gShaderProgramObject_simple, gFragmentShaderObject_simple);
		//Shader Attribute Binding Before Pre Linking of attached Shader
			glBindAttribLocation(gShaderProgramObject_simple,VSV_ATTRIBUTE_POSITION,"vPosition");
			glBindAttribLocation(gShaderProgramObject_simple,VSV_ATTRIBUTE_COLOR,"vColor");
			glBindAttribLocation(gShaderProgramObject_simple, VSV_ATTRIBUTE_TEXCOORD, "vTexCoord");

		glLinkProgram(gShaderProgramObject_simple);

		glGetProgramiv(gShaderProgramObject_simple,
						GL_LINK_STATUS,
						&shaderProgramLinkStatus);
		if(shaderProgramLinkStatus==GL_FALSE)
		{
			glGetProgramiv(gShaderProgramObject_simple,
						GL_INFO_LOG_LENGTH,
						&infoLogLength);
			if(infoLogLength>0)
			{
				szBuffer=(GLchar*)malloc(sizeof(GLchar)*infoLogLength);
				if(szBuffer!=NULL)
				{
					glGetProgramInfoLog(gShaderProgramObject_simple,
										infoLogLength,
										&written,
										szBuffer);

					fprintf(gpFile,"\nERROR:Shader Program Link failed log: %s",szBuffer);
					free(szBuffer);
					DestroyWindow(ghwnd);
				}
			}
		}
		//Uniform variable Binding after post linking of attached shader
		mvpMatrixUniform_simple =glGetUniformLocation(gShaderProgramObject_simple,
												"u_mvpMatrix");

		textureSamplerUniform_simple = glGetUniformLocation(gShaderProgramObject_simple,
							"u_texture_sampler");
		
	//Shader Code End--------------------


	//Shader Code Start--------------------
		gVertexShaderObject_render_to_texture = glCreateShader(GL_VERTEX_SHADER);

		const GLchar* vertexShaderSourceCode_render_to_texture =
			"#version 460 core" \
			"\n" \
			"in vec4 vPosition;"\
			"in vec4 vColor;"\
			"in vec2 vTexCoord;"\

			"out vec4 out_color;"\
			"out vec2 out_TexCoord;"\

			"uniform mat4 u_mvpMatrix;"\
			"void main(void)" \
			"{" \
				"out_color=vColor;"\
				"out_TexCoord=vTexCoord;"\

				"gl_Position=u_mvpMatrix * vPosition;"\
			"}";

		glShaderSource(gVertexShaderObject_render_to_texture,
			1,
			(const GLchar**)&vertexShaderSourceCode_render_to_texture,
			NULL);

		glCompileShader(gVertexShaderObject_render_to_texture);

		glGetShaderiv(gVertexShaderObject_render_to_texture,
			GL_COMPILE_STATUS,
			&shaderCompileStatus);


		if (shaderCompileStatus == GL_FALSE)
		{
			glGetShaderiv(gVertexShaderObject_render_to_texture,
				GL_INFO_LOG_LENGTH,
				&infoLogLength);
			if (infoLogLength > 0)
			{
				szBuffer = (GLchar*)malloc(sizeof(GLchar) * infoLogLength);

				if (szBuffer != NULL)
				{
					glGetShaderInfoLog(gVertexShaderObject_render_to_texture,
						infoLogLength,
						&written,
						szBuffer);

					fprintf(gpFile, "\nERROR:Vertex shader compilation failed log: %s", szBuffer);
					free(szBuffer);
					DestroyWindow(ghwnd);
				}
			}
		}

		infoLogLength = 0;
		shaderCompileStatus = 0;
		shaderProgramLinkStatus = 0;
		szBuffer = NULL;
		written = 0;

		gFragmentShaderObject_render_to_texture = glCreateShader(GL_FRAGMENT_SHADER);

		const GLchar* fragmentShaderSourceCode_render_to_texture =
			"#version 450" \
			"\n" \
			"in vec2 out_TexCoord;"\
			"in vec4 out_color;"\
			"out vec4 fragColor;"\
			"uniform sampler2D u_texture_sampler;"\
			"void main(void)" \
			"{"\
				"fragColor=out_color;"\
				"fragColor=texture(u_texture_sampler,out_TexCoord);"\
			"}";

		glShaderSource(gFragmentShaderObject_render_to_texture,
			1,
			(const GLchar**)&fragmentShaderSourceCode_render_to_texture,
			NULL);

		glCompileShader(gFragmentShaderObject_render_to_texture);

		glGetShaderiv(gFragmentShaderObject_render_to_texture,
			GL_COMPILE_STATUS,
			&shaderCompileStatus);


		if (shaderCompileStatus == GL_FALSE)
		{
			glGetShaderiv(gFragmentShaderObject_render_to_texture,
				GL_INFO_LOG_LENGTH,
				&infoLogLength);

			if (infoLogLength > 0)
			{
				szBuffer = (GLchar*)malloc(sizeof(GLchar) * infoLogLength);

				if (szBuffer != NULL)
				{
					glGetShaderInfoLog(gFragmentShaderObject_render_to_texture,
						infoLogLength,
						&written,
						szBuffer);

					fprintf(gpFile, "\nERROR:Fragment shader compilation failed log: %s", szBuffer);
					free(szBuffer);
					DestroyWindow(ghwnd);
				}
			}
		}

		infoLogLength = 0;
		shaderCompileStatus = 0;
		shaderProgramLinkStatus = 0;
		szBuffer = NULL;
		written = 0;

		gShaderProgramObject_render_to_texture = glCreateProgram();

		glAttachShader(gShaderProgramObject_render_to_texture, gVertexShaderObject_render_to_texture);
		glAttachShader(gShaderProgramObject_render_to_texture, gFragmentShaderObject_render_to_texture);
		//Shader Attribute Binding Before Pre Linking of attached Shader
		glBindAttribLocation(gShaderProgramObject_render_to_texture, VSV_ATTRIBUTE_POSITION, "vPosition");
		glBindAttribLocation(gShaderProgramObject_render_to_texture, VSV_ATTRIBUTE_COLOR, "vColor");
		glBindAttribLocation(gShaderProgramObject_render_to_texture, VSV_ATTRIBUTE_TEXCOORD, "vTexCoord");

		glLinkProgram(gShaderProgramObject_render_to_texture);

		glGetProgramiv(gShaderProgramObject_render_to_texture,
			GL_LINK_STATUS,
			&shaderProgramLinkStatus);
		if (shaderProgramLinkStatus == GL_FALSE)
		{
			glGetProgramiv(gShaderProgramObject_render_to_texture,
				GL_INFO_LOG_LENGTH,
				&infoLogLength);
			if (infoLogLength > 0)
			{
				szBuffer = (GLchar*)malloc(sizeof(GLchar) * infoLogLength);
				if (szBuffer != NULL)
				{
					glGetProgramInfoLog(gShaderProgramObject_render_to_texture,
						infoLogLength,
						&written,
						szBuffer);

					fprintf(gpFile, "\nERROR:Shader Program Link failed log: %s", szBuffer);
					free(szBuffer);
					DestroyWindow(ghwnd);
				}
			}
		}
		//Uniform variable Binding after post linking of attached shader
		mvpMatrixUniform_render_to_texture = glGetUniformLocation(gShaderProgramObject_render_to_texture,
			"u_mvpMatrix");
		textureSamplerUniform_render_to_texture = glGetUniformLocation(gShaderProgramObject_render_to_texture,
													"u_texture_sampler");

		//Shader Code End--------------------


	//VAO and its respective VBO preparation
		const GLfloat pyramidVertices[]=
		{
			0.0f, 1.0f, 0.0f,
			-1.0f, -1.0f, 1.0f,
			1.0f, -1.0f, 1.0f,

			0.0f, 1.0f, 0.0f,
			1.0f, -1.0f, 1.0f,
			1.0f, -1.0f, -1.0f,

			0.0f, 1.0f, 0.0f,
			1.0f, -1.0f, -1.0f,
			-1.0f, -1.0f, -1.0f,

			0.0f, 1.0f, 0.0f,
			-1.0f, -1.0f, -1.0f,
			-1.0f, -1.0f, 1.0f
		};

		const GLfloat pyramidColors[]=
		{
			1.0f, 0.0f, 0.0f,
			0.0f, 1.0f, 0.0f,
			0.0f, 0.0f, 1.0f,

			1.0f, 0.0f, 0.0f,
			0.0f, 0.0f, 1.0f,
			0.0f, 1.0f, 0.0f,

			1.0f, 0.0f, 0.0f,
			0.0f, 1.0f, 0.0f,
			0.0f, 0.0f, 1.0f,

			1.0f, 0.0f, 0.0f,
			0.0f, 0.0f, 1.0f,
			0.0f, 1.0f, 0.0f
		};

		const GLfloat pyramidTexCoord[] =
		{
			0.5f,1.0f,
			0.0f,0.0f,
			1.0f,0.0f,

			0.5f,1.0f,
			1.0f,0.0f,
			0.0f,0.0f,

			0.5f,1.0f,
			1.0f,0.0f,
			0.0f,0.0f,

			0.5f,1.0f,
			0.0f,0.0f,
			1.0f,0.0f
		};

		const GLfloat cubeVertices[]=
		{
			1.0f, 1.0f, 1.0f,
			-1.0f, 1.0f, 1.0f,
			-1.0f, -1.0f, 1.0f,
			1.0f, -1.0f, 1.0f,

			1.0f, 1.0f, -1.0f,
			1.0f, 1.0f, 1.0f,
			1.0f, -1.0f, 1.0f,
			1.0f, -1.0f, -1.0f,

			-1.0f, 1.0f, -1.0f,
			1.0f, 1.0f, -1.0f,
			1.0f, -1.0f, -1.0f,
			-1.0f, -1.0f, -1.0f,

			-1.0f, 1.0f, 1.0f,
			-1.0f, 1.0f, -1.0f,
			-1.0f, -1.0f, -1.0f,
			-1.0f, -1.0f, 1.0f,

			1.0f, 1.0f, -1.0f,
			-1.0f, 1.0f, -1.0f,
			-1.0f, 1.0f, 1.0f,
			1.0f, 1.0f, 1.0f,

			1.0f, -1.0f, -1.0f,
			-1.0f, -1.0f, -1.0f,
			-1.0f, -1.0f, 1.0f,
			1.0f, -1.0f, 1.0f
		};

		const GLfloat cubeColors[]=
		{
			1.0f, 0.0f, 0.0f,
			1.0f, 0.0f, 0.0f,
			1.0f, 0.0f, 0.0f,
			1.0f, 0.0f, 0.0f,
			
			0.0f, 1.0f, 0.0f,
			0.0f, 1.0f, 0.0f,
			0.0f, 1.0f, 0.0f,
			0.0f, 1.0f, 0.0f,

			0.0f, 0.0f, 1.0f,
			0.0f, 0.0f, 1.0f,
			0.0f, 0.0f, 1.0f,
			0.0f, 0.0f, 1.0f,

			0.0f, 1.0f, 1.0f,
			0.0f, 1.0f, 1.0f,
			0.0f, 1.0f, 1.0f,
			0.0f, 1.0f, 1.0f,

			1.0f, 0.0f, 1.0f,
			1.0f, 0.0f, 1.0f,
			1.0f, 0.0f, 1.0f,
			1.0f, 0.0f, 1.0f,

			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f
		};


		const GLfloat cubeTexCoord[] =
		{
			0.0f,0.0f,
			1.0f,0.0f,
			1.0f,1.0f,
			0.0f,1.0f,

			1.0f,0.0f,
			1.0f,1.0f,
			0.0f,1.0f,
			0.0f,0.0f,

			1.0f,0.0f,
			1.0f,1.0f,
			0.0f,1.0f,
			0.0f,0.0f,

			0.0f,0.0f,
			1.0f,0.0f,
			1.0f,1.0f,
			0.0f,1.0f,

			0.0f,1.0f,
			0.0f,0.0f,
			1.0f,0.0f,
			1.0f,1.0f,

			1.0f,1.0f,
			0.0f,1.0f,
			0.0f,0.0f,
			1.0f,0.0f

		};

		glGenVertexArrays(1,&vao_pyramid);
		glBindVertexArray(vao_pyramid);

			glGenBuffers(1,&vbo_pyramidPosition);
			glBindBuffer(GL_ARRAY_BUFFER,vbo_pyramidPosition);

				glBufferData(GL_ARRAY_BUFFER,
							sizeof(pyramidVertices),
							pyramidVertices,
							GL_STATIC_DRAW);

				glVertexAttribPointer(VSV_ATTRIBUTE_POSITION,
									3,
									GL_FLOAT,
									GL_FALSE,
									0,
									NULL);

				glEnableVertexAttribArray(VSV_ATTRIBUTE_POSITION);
			
			glBindBuffer(GL_ARRAY_BUFFER,0);
			
			glGenBuffers(1,&vbo_pyramidColor);
			glBindBuffer(GL_ARRAY_BUFFER,vbo_pyramidColor);
				
				glBufferData(GL_ARRAY_BUFFER,
							sizeof(pyramidColors),
							pyramidColors,
							GL_STATIC_DRAW);
				
				glVertexAttribPointer(VSV_ATTRIBUTE_COLOR,
									3,
									GL_FLOAT,
									GL_FALSE,
									0,
									NULL);

				glEnableVertexAttribArray(VSV_ATTRIBUTE_COLOR);
			
			glBindBuffer(GL_ARRAY_BUFFER,0);

			glGenBuffers(1, &vbo_pyramidTexture);
			glBindBuffer(GL_ARRAY_BUFFER, vbo_pyramidTexture);

				glBufferData(GL_ARRAY_BUFFER,
					sizeof(pyramidTexCoord),
					pyramidTexCoord,
					GL_STATIC_DRAW);

				glVertexAttribPointer(VSV_ATTRIBUTE_TEXCOORD,
					2,
					GL_FLOAT,
					GL_FALSE,
					0,
					NULL);

				glEnableVertexAttribArray(VSV_ATTRIBUTE_TEXCOORD);

			glBindBuffer(GL_ARRAY_BUFFER, 0);

		glBindVertexArray(0);

		glGenVertexArrays(1,&vao_cube);
		glBindVertexArray(vao_cube);
			glGenBuffers(1,&vbo_cubePosition);
			glBindBuffer(GL_ARRAY_BUFFER,vbo_cubePosition);

				glBufferData(GL_ARRAY_BUFFER,
							sizeof(cubeVertices),
							cubeVertices,
							GL_STATIC_DRAW);

				glVertexAttribPointer(VSV_ATTRIBUTE_POSITION,
									3,
									GL_FLOAT,
									GL_FALSE,
									0,
									NULL);

				glEnableVertexAttribArray(VSV_ATTRIBUTE_POSITION);
			glBindBuffer(GL_ARRAY_BUFFER,0);

			glGenBuffers(1,&vbo_cubeColor);
			glBindBuffer(GL_ARRAY_BUFFER,vbo_cubeColor);
				glBufferData(GL_ARRAY_BUFFER,
							sizeof(cubeColors),
							cubeColors,
							GL_STATIC_DRAW);
				glVertexAttribPointer(VSV_ATTRIBUTE_COLOR,
										3,
										GL_FLOAT,
										GL_FALSE,
										0,
										NULL);
				glEnableVertexAttribArray(VSV_ATTRIBUTE_COLOR);
			glBindBuffer(GL_ARRAY_BUFFER,0);

			glGenBuffers(1, &vbo_cubeTexture);
				glBindBuffer(GL_ARRAY_BUFFER, vbo_cubeTexture);
				glBufferData(GL_ARRAY_BUFFER,
					sizeof(cubeTexCoord),
					cubeTexCoord,
					GL_STATIC_DRAW);
				glVertexAttribPointer(VSV_ATTRIBUTE_TEXCOORD,
					2,
					GL_FLOAT,
					GL_FALSE,
					0,
					NULL);
				glEnableVertexAttribArray(VSV_ATTRIBUTE_TEXCOORD);
			glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindVertexArray(0);


	glGenFramebuffers(1, &gFrameBuffers);
	glBindFramebuffer(GL_FRAMEBUFFER,gFrameBuffers);
		//color 
		glGenTextures(1, &gColorTexture);
		glBindTexture(GL_TEXTURE_2D,gColorTexture);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexImage2D(GL_TEXTURE_2D,
						0,
						GL_RGB,
						800,
						600,
						0,
						GL_RGB,
						GL_UNSIGNED_BYTE,
						NULL);
		glBindTexture(GL_TEXTURE_2D, 0);

		//depth
		glGenTextures(1, &gDepthTexture);
		glBindTexture(GL_TEXTURE_2D, gDepthTexture);
		/*glTexStorage2D(GL_TEXTURE_2D,
						1,
						GL_DEPTH_COMPONENT32F,
						800,
						600);*/


			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_NONE);

			glTexImage2D(GL_TEXTURE_2D,
				0,
				GL_DEPTH_COMPONENT,
				800,
				600,
				0,
				GL_DEPTH_COMPONENT,
				GL_UNSIGNED_BYTE,
				NULL);
		glBindTexture(GL_TEXTURE_2D, 0);

		glFramebufferTexture(GL_FRAMEBUFFER, 
							GL_COLOR_ATTACHMENT0, 
							gColorTexture, 
							0);

		glFramebufferTexture(GL_FRAMEBUFFER,
						GL_DEPTH_ATTACHMENT,
						gDepthTexture,
						0);

	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	glClearColor(0.0,0.0,0.0f,0.0f);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	glEnable(GL_TEXTURE_2D);
	LoadGLTexture(&Stone_Texture, MAKEINTRESOURCE(STONE_BITMAP));
	LoadGLTexture(&Kundali_Texture, MAKEINTRESOURCE(KUNDALI_BITMAP));

	perspectiveProjectionMatrix=mat4::identity();
	Resize(WIN_WIDTH,WIN_HEIGHT);

}

void Resize(int width,int height)
{
	GLuint tempColorTexture = 0;
	GLuint tempgDepthTexture = 0;

	if(height==0)
	{
		height=1;
	}
	
	gWidth = width;
	gHeight = height;

	//glViewport(0,0,(GLsizei)width,(GLsizei)height);

	perspectiveProjectionMatrix=vmath::perspective(45.0f,
													(GLfloat)width/(GLfloat)height,
													0.1f,
													100.0f);

	if (gFrameBuffers == 0)
		return;

	glBindFramebuffer(GL_FRAMEBUFFER, gFrameBuffers);
		
		if (gColorTexture == 0)
			return;
		//color 
		//glGenTextures(1, &gColorTexture);
		glBindTexture(GL_TEXTURE_2D, gColorTexture);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTexImage2D(GL_TEXTURE_2D,
				0,
				GL_RGB,
				(GLsizei)width,
				(GLsizei)height,
				0,
				GL_RGB,
				GL_UNSIGNED_BYTE,
				NULL);
		glBindTexture(GL_TEXTURE_2D, 0);
		//gColorTexture = tempColorTexture;

		if (gDepthTexture == 0)
			return;
		//depth
		//glGenTextures(1, &gDepthTexture);
		glBindTexture(GL_TEXTURE_2D, gDepthTexture);
			/*glTexStorage2D(GL_TEXTURE_2D,
				1,
				GL_DEPTH_COMPONENT32F,
				(GLsizei)width,
				(GLsizei)height);*/
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_NONE);
			glTexImage2D(GL_TEXTURE_2D,
				0,
				GL_DEPTH_COMPONENT,
				(GLsizei)width,
				(GLsizei)height,
				0,
				GL_DEPTH_COMPONENT,
				GL_UNSIGNED_BYTE,
				NULL);
		glBindTexture(GL_TEXTURE_2D, 0);
		//gDepthTexture = tempgDepthTexture;

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

GLfloat pyramidRotate=0.0f;
GLfloat cubeRotate=0.0f;

void Display(void)
{

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	mat4 modelViewMatrix;
	mat4 modelViewProjectionMatrix;
	mat4 translateMatrix;
	mat4 rotateMatrix;
	mat4 scaleMatrix;


	glBindFramebuffer(GL_FRAMEBUFFER, gFrameBuffers);
		glViewport(0, 0, (GLsizei)gWidth, (GLsizei)gHeight);
		//glViewport(0, 0, 800, 600);

		glClearBufferfv(GL_COLOR, 0, vec4(0.0f, 1.0f, 0.0f, 1.0f));
		glClearBufferfv(GL_DEPTH, 0, vec4(1.0f, 1.0f, 1.0f, 1.0f));

		glUseProgram(gShaderProgramObject_simple);

			rotateMatrix = vmath::rotate(cubeRotate, 1.0f, 0.0f, 0.0f);
			rotateMatrix *= vmath::rotate(cubeRotate, 0.0f, 1.0f, 0.0f);
			rotateMatrix *= vmath::rotate(cubeRotate, 0.0f, 0.0f, 1.0f);

			translateMatrix = vmath::translate(0.0f, 0.0f, -6.0f);
			scaleMatrix = vmath::scale(0.75f, 0.75f, 0.75f);

			modelViewMatrix = translateMatrix * scaleMatrix * rotateMatrix;
			modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

			glUniformMatrix4fv(mvpMatrixUniform_simple,
				1,
				GL_FALSE,
				modelViewProjectionMatrix);

			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, Kundali_Texture);
			glUniform1i(textureSamplerUniform_simple,
							0);

			glBindVertexArray(vao_cube);
			glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
			glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
			glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
			glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
			glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
			glDrawArrays(GL_TRIANGLE_FAN, 20, 4);
			glBindVertexArray(0);

		glUseProgram(0);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	glUseProgram(gShaderProgramObject_render_to_texture);
	
		rotateMatrix=vmath::rotate(cubeRotate,1.0f,0.0f,0.0f);
		rotateMatrix*=vmath::rotate(cubeRotate,0.0f,1.0f,0.0f);
		rotateMatrix*=vmath::rotate(cubeRotate,0.0f,0.0f,1.0f);

		translateMatrix=vmath::translate(0.0f,0.0f,-6.0f);
		scaleMatrix=vmath::scale(0.75f,0.75f,0.75f);

		modelViewMatrix=translateMatrix * scaleMatrix  * rotateMatrix  ;
		modelViewProjectionMatrix=perspectiveProjectionMatrix * modelViewMatrix;

		glUniformMatrix4fv(mvpMatrixUniform_render_to_texture,
						1,
						GL_FALSE,
						modelViewProjectionMatrix);
		
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, gColorTexture);
		glUniform1i(glGetUniformLocation(gShaderProgramObject_render_to_texture,
			"u_texture_sampler"), 0);
		
		glBindVertexArray(vao_cube);
			glDrawArrays(GL_TRIANGLE_FAN,0,4);
			glDrawArrays(GL_TRIANGLE_FAN,4,4);
			glDrawArrays(GL_TRIANGLE_FAN,8,4);
			glDrawArrays(GL_TRIANGLE_FAN,12,4);
			glDrawArrays(GL_TRIANGLE_FAN,16,4);
			glDrawArrays(GL_TRIANGLE_FAN,20,4);
		glBindVertexArray(0);

	glUseProgram(0);

	SwapBuffers(ghdc);

	pyramidRotate+=1.0f;
	cubeRotate+=1.0f;
}

void UnInitialize(void)
{
	if(gbFullscreen==true)
	{
		SetWindowLong(ghwnd,GWL_STYLE,(dwStyle | WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(ghwnd,&wpPrev);
		SetWindowPos(ghwnd,HWND_TOP,0,0,0,0,SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED );
		fprintf(gpFile,"\nNormal Window");
		gbFullscreen=false;
	}

	if(vao_cube)
	{
		glDeleteVertexArrays(1,
							&vao_cube);
	}
	vao_cube=0;

	if(vao_pyramid)
	{
		glDeleteVertexArrays(1,
							&vao_pyramid);
	}
	vao_pyramid=0;

	if(vbo_pyramidColor){
		glDeleteBuffers(1,
						&vbo_pyramidColor);
	}
	vbo_pyramidColor=0;

	if(vbo_pyramidPosition)
	{
		glDeleteBuffers(1,
					&vbo_pyramidPosition);
	}
	vbo_pyramidPosition=0;

	if(vbo_cubePosition)
	{
		glDeleteBuffers(1,
					&vbo_cubePosition);
	}
	vbo_cubePosition=0;

	if(vbo_cubeColor)
	{
		glDeleteBuffers(1,
					&vbo_cubeColor);
	}
	vbo_cubeColor=0;

	if (gFrameBuffers)
	{
		glDeleteFramebuffers(1,
			&gFrameBuffers);
	}
	gFrameBuffers = 0;

	if (gFrameBuffers)
	{
		glDeleteFramebuffers(1,
			&gFrameBuffers);
	}
	gFrameBuffers = 0;

	glDeleteTextures(1, &gColorTexture);
	glDeleteTextures(1, &gDepthTexture);
	
	glDeleteTextures(1, &Kundali_Texture);
	glDeleteTextures(1, &Stone_Texture);

	if(gShaderProgramObject_simple)
	{
		glUseProgram(gShaderProgramObject_simple);

		GLsizei shaderCount;
		GLsizei tempShaderCount;
		GLuint *pShader=NULL;

		glGetProgramiv(gShaderProgramObject_simple,GL_ATTACHED_SHADERS,&shaderCount);

		pShader=(GLuint *)malloc(sizeof(GLuint)*shaderCount);
		if(pShader==NULL)
		{
			fprintf(gpFile,"\nFailed to allocate memory for pShader");
		}else{
			glGetAttachedShaders(gShaderProgramObject_simple,shaderCount,&tempShaderCount,pShader);
			
			fprintf(gpFile,"\nShader program has actual attached shader count is:%d",tempShaderCount);
			
			for(GLsizei i=0;i<shaderCount;i++)
			{
				glDetachShader(gShaderProgramObject_simple,pShader[i]);
				glDeleteShader(pShader[i]);
				pShader[i]=0;
			}
			free(pShader);
			
			glDeleteProgram(gShaderProgramObject_simple);
			gShaderProgramObject_simple =0;
			glUseProgram(0);
		}
	}

	if(wglGetCurrentContext()==ghrc)
	{
		wglMakeCurrent(NULL,NULL);
	}

	if(ghrc)
	{
		wglDeleteContext(ghrc);
	}
		ghrc=NULL;

	if(ghdc)
	{
		ReleaseDC(ghwnd,ghdc);
	}
		ghdc=NULL;
	
	if(gpFile)
	{
		fprintf(gpFile,"\nlog is ended!");
		fclose(gpFile);
	}
		gpFile=NULL;
}


bool LoadGLTexture(GLuint* texture, TCHAR resourceID[])
{
	bool bResult = false;
	HBITMAP hbitmap = NULL;
	BITMAP bmp;
	//code 
	hbitmap = (HBITMAP)LoadImage(GetModuleHandle(NULL),//get current HInstance of our process
		resourceID,//resorce id
		IMAGE_BITMAP,//image type
		0,//width
		0,//height
		LR_CREATEDIBSECTION//load resource as device independent bitmap
	);
	if (hbitmap != NULL)
	{
		bResult = true;
		GetObject(hbitmap,//to extract information  from handle of object 
			sizeof(BITMAP),//Size of object where to store info
			&bmp//Address of object to store information
		);

		glPixelStorei(GL_UNPACK_ALIGNMENT,
			1
		);

		glGenTextures(1,
			texture
		);

		glBindTexture(GL_TEXTURE_2D,
			*texture
		);

		glTexParameteri(GL_TEXTURE_2D,
			GL_TEXTURE_MAG_FILTER,
			GL_LINEAR
		);
		glTexParameteri(GL_TEXTURE_2D,
			GL_TEXTURE_MIN_FILTER,
			GL_LINEAR_MIPMAP_LINEAR
		);

		glTexImage2D(GL_TEXTURE_2D,
			0,
			GL_RGB,
			bmp.bmWidth,
			bmp.bmHeight,
			0,
			GL_BGR,
			GL_UNSIGNED_BYTE,
			bmp.bmBits);

		glGenerateMipmap(GL_TEXTURE_2D);

		DeleteObject(hbitmap);
	}

	return bResult;
}
