#include<windows.h>
#include<stdio.h>
#include"model_loading.h"

#include<GL/GLEW.h>
#include<GL/GL.h>

#include"vmath.h"

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

#pragma comment(lib,"GLEW32.lib")
#pragma comment(lib,"OpenGL32.lib")

using namespace vmath;

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

FILE* gpFile = NULL;
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
bool gbFullscreen = false;
bool gbActiveWindow = false;
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

enum{
	VSV_ATTRIBUTE_POSITION=0,
	VSV_ATTRIBUTE_COLOR,
	VSV_ATTRIBUTE_TEXCOORD,
	VSV_ATTRIBUTE_NORMAL,
};

GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

GLuint vao;
GLuint vbo_position;
GLuint vbo_index;
GLuint mvpMatrixUniform;

mat4 perspectiveProjectionMatrix;


//Yogeshwar sir's globals for Model Loading
#define BUFFER_SIZE 			1024
#define S_EQUAL 				0

typedef struct vec_int {
	int* pi;
	size_t size;
}vec_int_t;

typedef struct vec_float {
	float* pf;
	size_t size;
}vec_float_t;

vec_float_t* gp_vertices;
vec_float_t* gp_texture;
vec_float_t* gp_normals;

vec_float_t* gp_vertices_sorted;
vec_float_t* gp_texture_sorted;
vec_float_t* gp_normals_sorted;


vec_int_t* gp_face_tri;
vec_int_t* gp_face_texture;
vec_int_t* gp_face_normals;

FILE* g_fp_meshfile = NULL;
char g_line[BUFFER_SIZE];
//


int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	void Initialize(void);
	void Display(void);

	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("ProgrammablePipelinePerspective");
	int X1, X2, Y1, Y2;
	bool bDone = false;

	if (fopen_s(&gpFile, "log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Failed to Create log file!"), TEXT("Error"), MB_OK | MB_ICONERROR);
		exit(0);
	}
	
	X1 = GetSystemMetrics(SM_CXSCREEN) / 2;
	Y1 = GetSystemMetrics(SM_CYSCREEN) / 2;

	X2 = WIN_WIDTH / 2;
	Y2 = WIN_HEIGHT / 2;

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style =  CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(GRAY_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));

	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("Programmable Pipeline Triangle with Perspective Projection"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		X1 - X2,//100,
		Y1 - Y2,//100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;
	Initialize();

	ShowWindow(hwnd, iCmdShow);

	SetForegroundWindow(hwnd);
	SetFocus(hwnd);
	


	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
		
			if (gbActiveWindow == true)
			{
				Display();
			}
		}
	}

	return ((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void ToggleFullscreen(void);
	void Resize(int, int);
	void UnInitialize(void);

	switch (iMsg)
	{
	case WM_CREATE:
		fprintf(gpFile, "\nLog is Started!");
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
			case VK_ESCAPE:
				DestroyWindow(hwnd);
				break;
			case 0x46:
			case 0x66:
				ToggleFullscreen();
				break;
			default:
				break;
		}
		break;
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;
	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;

	case WM_ERASEBKGND:
		return 0;
	case WM_SIZE:
		Resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;
	case WM_DESTROY:
		UnInitialize();
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}
void ToggleFullscreen(void)
{
	MONITORINFO mi = { sizeof(MONITORINFO) };
	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW) 
		{
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				fprintf(gpFile, "\nFullscreen Enabled");
				SetWindowLong(ghwnd, GWL_STYLE, (dwStyle & ~WS_OVERLAPPEDWINDOW));
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
		gbFullscreen = true;
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, (dwStyle | WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		fprintf(gpFile, "\nNormal Window");
		ShowCursor(TRUE);
		gbFullscreen = false;
	}
}

void Initialize(void)
{
	void Resize(int, int);
	void LoadMeshData(void);

	//Local variable initialization
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	ghdc = GetDC(ghwnd);

	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER ;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits= 32 ;


	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (iPixelFormatIndex == 0 )
	{
		fprintf(gpFile, "\nChoosePixelFormat() Failed to get pixel format index");
		DestroyWindow(ghwnd);
	}

	if ( SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE )
	{
		fprintf(gpFile, "\nSetPixelFormat() Failed to set pixel format for window");
		DestroyWindow(ghwnd);
	}

	ghrc = wglCreateContext(ghdc);
	if ( ghrc == NULL)
	{
		fprintf(gpFile, "\nwglCreateContext() Failed to get the rendering context");
		DestroyWindow(ghwnd);
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE )
	{
		fprintf(gpFile, "\nwglMakeCurrent() Failed to switch current context from device to rendering context");
		DestroyWindow(ghwnd);
	}

	GLenum glew_error=glewInit();
	if(glew_error != GLEW_OK)
	{
		fprintf(gpFile, "\nglewInit failed to enable extension for Programmable Pipeline");
		DestroyWindow(ghwnd);
	}

	fprintf(gpFile, "\nOpenGL Information are availble below:-");
	fprintf(gpFile, "\n*****************************************************");

	fprintf(gpFile, "\nOpenGL Vender:%s",glGetString(GL_VENDOR));
	fprintf(gpFile, "\nOpenGL Renderer:%s",glGetString(GL_RENDERER));
	fprintf(gpFile, "\nOpenGL Version:%s",glGetString(GL_VERSION));
	fprintf(gpFile, "\nOpenGL GLSL Version:%s",glGetString(GL_SHADING_LANGUAGE_VERSION));
	
	fprintf(gpFile, "\nOpenGL Extension's list:\n");
	GLint numExt;
	glGetIntegerv(GL_NUM_EXTENSIONS,&numExt);

	for(int i=0;i<numExt;i++)
	{
		fprintf(gpFile, "\n%s",glGetStringi(GL_EXTENSIONS,i));
	}
	fprintf(gpFile, "\n*****************************************************");

	GLint infoLogLength=0;
	GLint shaderCompileStatus=0;
	GLint shaderProgramLinkStatus=0;
	GLchar *szBuffer=NULL;
	GLsizei written;

	//Shader Code Start--------------------
		gVertexShaderObject=glCreateShader(GL_VERTEX_SHADER);

		const GLchar *vertexShaderSourceCode=
		"#version 460 core"\
		"\n"\
		"in vec4 vPosition;"\
		"uniform mat4 u_mvpMatrix;"\
		"void main(void)"\
		"{"\
			"gl_Position=u_mvpMatrix * vPosition;"\
		"}";

		glShaderSource(gVertexShaderObject,
						1,
						(const GLchar **)&vertexShaderSourceCode,
						NULL);

		glCompileShader(gVertexShaderObject);

		glGetShaderiv(gVertexShaderObject,
						GL_COMPILE_STATUS,
						&shaderCompileStatus);
		
		if(shaderCompileStatus==GL_FALSE)
		{
			glGetShaderiv(gVertexShaderObject,
						GL_INFO_LOG_LENGTH,
						&infoLogLength);
			
			if(infoLogLength > 0)
			{
				szBuffer=(GLchar *)malloc(sizeof(GLchar)*infoLogLength);
				if(szBuffer!=NULL)
				{
					glGetShaderInfoLog(gVertexShaderObject,
										infoLogLength,
										&written,
										szBuffer);

					fprintf(gpFile,"\nERROR Vertex Shader Object compilation failed log : %s",szBuffer);

					free(szBuffer);
					DestroyWindow(ghwnd);
				}
			}
		}

		infoLogLength=0;
		shaderCompileStatus=0;
		shaderProgramLinkStatus=0;
		szBuffer=NULL;
		written=0;

		gFragmentShaderObject=glCreateShader(GL_FRAGMENT_SHADER);

		const GLchar *fragmentShaderSourceCode=
		"#version 460 core"\
		"\n" \
		"out vec4 fragColor;"\
		"void main(void)"\
		"{"\
			"fragColor=vec4(1.0,1.0,1.0,1.0);"\
		"}";

		glShaderSource(gFragmentShaderObject,
						1,
						(const GLchar **)&fragmentShaderSourceCode,
						NULL);
		
		glCompileShader(gFragmentShaderObject);

		glGetShaderiv(gFragmentShaderObject,
						GL_COMPILE_STATUS,
						&shaderCompileStatus);

		if(shaderCompileStatus==GL_FALSE)
		{
			glGetShaderiv(gFragmentShaderObject,
						GL_INFO_LOG_LENGTH,
						&infoLogLength);

			if(infoLogLength > 0)
			{
				szBuffer=(GLchar *)malloc(sizeof(GLchar) * infoLogLength);
				if(szBuffer!=NULL)
				{
					glGetShaderInfoLog(gFragmentShaderObject,
										infoLogLength,
										&written,
										szBuffer);
					fprintf(gpFile,"\nERROR Fragment shader object compilation failed log : %s",szBuffer);
					free(szBuffer);
					DestroyWindow(ghwnd);
				}
			}
		}

		infoLogLength=0;
		shaderCompileStatus=0;
		shaderProgramLinkStatus=0;
		szBuffer=NULL;
		written=0;

		gShaderProgramObject=glCreateProgram();

		glAttachShader(gShaderProgramObject,gVertexShaderObject);
		glAttachShader(gShaderProgramObject,gFragmentShaderObject);
		
		//Shader Attribute Binding Before Pre Linking of attached Shader
			glBindAttribLocation(gShaderProgramObject,
								VSV_ATTRIBUTE_POSITION,
								"vPosition");

		glLinkProgram(gShaderProgramObject);

		glGetProgramiv(gShaderProgramObject,
						GL_LINK_STATUS,
						&shaderProgramLinkStatus);

		if(shaderProgramLinkStatus == GL_FALSE)
		{
			glGetProgramiv(gShaderProgramObject,
						GL_INFO_LOG_LENGTH,
						&infoLogLength);
			
			if(infoLogLength > 0)
			{
				szBuffer=(GLchar *)malloc(sizeof(GLchar) * infoLogLength);
				if(szBuffer!=NULL)
				{
					glGetProgramInfoLog(gShaderProgramObject,
										infoLogLength,
										&written,
										szBuffer);
					
					fprintf(gpFile,
							"\nERROR Shader Program Object Link failed Log : %s",
							szBuffer);	
					
					free(szBuffer);
					DestroyWindow(ghwnd);
				}
			}
		}
		
	//Shader Code End--------------------

	//Uniform variable Binding after post linking of attached shader 

		mvpMatrixUniform=glGetUniformLocation(gShaderProgramObject,
												"u_mvpMatrix");

	LoadMeshData();

	//VAO and its respective VBO preparation	

		glGenVertexArrays(1,
						&vao);

		glBindVertexArray(vao);

			glGenBuffers(1,
						&vbo_position);
			
			glBindBuffer(GL_ARRAY_BUFFER,
						vbo_position);
				
				glBufferData(GL_ARRAY_BUFFER,
							gp_vertices_sorted->size * sizeof(GLfloat),
							gp_vertices_sorted->pf,
							GL_STATIC_DRAW);
				
				glVertexAttribPointer(VSV_ATTRIBUTE_POSITION,
										3,
										GL_FLOAT,
										GL_FALSE,
										0,
										NULL);

				glEnableVertexAttribArray(VSV_ATTRIBUTE_POSITION);

			glBindBuffer(GL_ARRAY_BUFFER,
						0);

			glGenBuffers(1,
				&vbo_index);

			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,
				vbo_index);

			glBufferData(GL_ELEMENT_ARRAY_BUFFER,
				gp_face_tri->size * sizeof(int),
				gp_face_tri->pi,
				GL_STATIC_DRAW);

		glBindVertexArray(0);

	//SetColor
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	//Warmup resize call

	perspectiveProjectionMatrix=mat4::identity();
	
	Resize(WIN_WIDTH, WIN_HEIGHT);
	
}

void Resize(int width, int height)
{
	if (height == 0)
	{
		height = 1;
	}

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	perspectiveProjectionMatrix=vmath::perspective(45.0f,
												(GLfloat)width/(GLfloat)height,
												0.1f,
												100.0f);
	
}


void Display(void)
{
	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
	
	glUseProgram(gShaderProgramObject);

		mat4 moodelViewMatrix=mat4::identity();
		mat4 translateMatrix;
		mat4 modelViewProjectionMatix=mat4::identity();

		translateMatrix=vmath::translate(0.0f,0.0f,-3.0f);
		
		moodelViewMatrix=translateMatrix;

		modelViewProjectionMatix=perspectiveProjectionMatrix * moodelViewMatrix;

		glUniformMatrix4fv(mvpMatrixUniform,
							1,
							GL_FALSE,
							modelViewProjectionMatix);

		glBindVertexArray(vao);

		glDrawElements(GL_TRIANGLES,
			gp_vertices_sorted->size,//size of array
			GL_UNSIGNED_INT,//data type
			0);//starting Index


		glBindVertexArray(0);	

	glUseProgram(0);

	SwapBuffers(ghdc);
	
}
void UnInitialize(void)
{
	if (gbFullscreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, (dwStyle | WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}
	
	if(vao)
	{
		glDeleteVertexArrays(1,
							&vao);
	}
	vao=0;

	if(vbo_position)
	{
		glDeleteBuffers(1,
						&vbo_position);
	}
	vbo_position=0;

	if(gShaderProgramObject)
	{
		glUseProgram(gShaderProgramObject);

		GLsizei shaderCount;
		GLsizei tempShaderCount;
		GLuint *pShader=NULL;

		glGetProgramiv(gShaderProgramObject,GL_ATTACHED_SHADERS,&shaderCount);

		pShader=(GLuint *)malloc(sizeof(GLuint)*shaderCount);
		if(pShader==NULL)
		{
			fprintf(gpFile,"\nFailed to allocate memory for pShader");
		}else{
			glGetAttachedShaders(gShaderProgramObject,shaderCount,&tempShaderCount,pShader);
			
			fprintf(gpFile,"\nShader program has actual attached shader count is:%d",tempShaderCount);
			
			for(GLsizei i=0;i<shaderCount;i++)
			{
				glDetachShader(gShaderProgramObject,pShader[i]);
				glDeleteShader(pShader[i]);
				pShader[i]=0;
			}
			free(pShader);
			
			glDeleteProgram(gShaderProgramObject);
			gShaderProgramObject=0;
			glUseProgram(0);
		}
	}

	if (wglGetCurrentContext() == ghrc ) {
		wglMakeCurrent(NULL, NULL);
	}


	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (gpFile)
	{
		fprintf(gpFile, "\nLog is Ended!");
		fclose(gpFile);
		gpFile = NULL;
	}
}


void LoadMeshData(void)
{
	void UnInitialize(void);

	vec_int_t* create_vec_int(void);
	vec_float_t* create_vec_float(void);

	void push_back_vec_int(vec_int_t*, int);
	void push_back_vec_float(vec_float_t*, float);

	void* xcalloc(int, size_t);

	const char* sep_space = " ";
	const char* sep_fslash = "/";

	char* first_token = NULL;
	char* token = NULL;
	char* next_token = NULL;

	char* f_enteries[3] = { NULL,NULL,NULL };

	int nr_pos_coords = 0, nr_tex_coords = 0, nr_normal_coords = 0, nr_faces = 0;


	if (fopen_s(&g_fp_meshfile,"MonkeyHead.obj", "r") !=0)
	{
		UnInitialize();
	}

	gp_vertices = create_vec_float();
	gp_texture = create_vec_float();
	gp_normals = create_vec_float();

	gp_face_tri = create_vec_int();
	gp_face_texture = create_vec_int();
	gp_face_normals = create_vec_int();

	gp_vertices_sorted = create_vec_float();
	gp_texture_sorted = create_vec_float();
	gp_normals_sorted = create_vec_float();

	while (fgets(g_line, BUFFER_SIZE, g_fp_meshfile) != NULL)
	{

		first_token = strtok_s(g_line, sep_space, &next_token);

		if (strcmp(first_token, "v") == S_EQUAL)
		{
			nr_pos_coords++;
			while ((token = strtok_s(NULL, sep_space, &next_token)) != NULL)
				push_back_vec_float(gp_vertices, atof(token));

		}
		else if (strcmp(first_token, "vt") == S_EQUAL)
		{
			nr_tex_coords++;
			while ((token = strtok_s(NULL, sep_space, &next_token)) != NULL)
				push_back_vec_float(gp_texture, atof(token));

		}
		else if (strcmp(first_token, "vn") == S_EQUAL)
		{
			nr_normal_coords++;
			while ((token = strtok_s(NULL, sep_space, &next_token)) != NULL)
				push_back_vec_float(gp_normals, atof(token));

		}
		else if (strcmp(first_token, "f") == S_EQUAL)
		{
			nr_faces++;

			for (int i = 0; i < 3; i++)
			{
				f_enteries[i] = strtok_s(NULL, sep_space, &next_token);
			}

			for (int i = 0; i < 3; i++)
			{
				token = strtok_s(f_enteries[i], sep_fslash, &next_token);
				push_back_vec_int(gp_face_tri, atoi(token) - 1);

				token = strtok_s(NULL, sep_fslash, &next_token);
					push_back_vec_int(gp_face_texture, atoi(token) - 1);

				token = strtok_s(NULL, sep_fslash, &next_token);
					push_back_vec_int(gp_face_normals, atoi(token) - 1);

			}
		}

		memset((void*)g_line, (int)'\0', BUFFER_SIZE);
	}

	//create data according to faces/index 
	for (int i = 0; i < gp_face_tri->size; i++)
		push_back_vec_float(gp_vertices_sorted, gp_vertices->pf[i]);

	for (int i = 0; i < gp_face_texture->size; i++)
		push_back_vec_float(gp_texture_sorted, gp_texture->pf[i]);

	for (int i = 0; i < gp_face_normals->size; i++)
		push_back_vec_float(gp_normals_sorted, gp_normals->pf[i]);

	fclose(g_fp_meshfile);
	g_fp_meshfile = NULL;
}

vec_int_t* create_vec_int(void)
{
	void* xcalloc(int, size_t);

	return (vec_int_t*)xcalloc(1, sizeof(vec_int_t));
}

vec_float_t* create_vec_float(void)
{
	void* xcalloc(int, size_t);

	return (vec_float_t*)xcalloc(1, sizeof(vec_float_t));
}

void push_back_vec_int(vec_int_t* p_vec, int data)
{
	void* xrealloc(void*, size_t);

	p_vec->pi = (int*)xrealloc(p_vec->pi, (p_vec->size + 1) * sizeof(int));
	p_vec->size++;
	p_vec->pi[p_vec->size - 1] = data;
}


void push_back_vec_float(vec_float_t* p_vec, float data)
{
	void* xrealloc(void*, size_t);

	p_vec->pf = (float*)xrealloc(p_vec->pf, (p_vec->size + 1) * sizeof(float));
	p_vec->size++;
	p_vec->pf[p_vec->size - 1] = data;
}

void* xcalloc(int nr_elements, size_t size_per_element)
{
	void UnInitialize(void);

	void* p = calloc(nr_elements, size_per_element);
	if (!p)
	{
		UnInitialize();
	}

	memset(p, 0, size_per_element);

	return (p);
}

void* xrealloc(void* p, size_t new_size)
{
	void UnInitialize(void);

	void* ptr = realloc(p, new_size);
	if (!ptr)
	{
		UnInitialize();
	}
	return (ptr);
}

void clean_vec_float(vec_float_t* p_vec)
{
	free(p_vec->pf);
	free(p_vec);
	p_vec = NULL;
}

void clean_vec_int(vec_int* p_vec)
{
	free(p_vec->pi);
	free(p_vec);
	p_vec = NULL;
}

/*
cl /c /EHsc /I C:\glew-2.1.0-win32\glew-2.1.0\include pp_perspective.cpp 
rc pp_perspective.rc
link pp_perspective.obj pp_perspective.res /LIBPATH:C:\glew-2.1.0-win32\glew-2.1.0\lib\Release\x64 user32.lib gdi32.lib kernel32.lib /SUBSYSTEM:WINDOWS
pp_perspective.exe
*/
