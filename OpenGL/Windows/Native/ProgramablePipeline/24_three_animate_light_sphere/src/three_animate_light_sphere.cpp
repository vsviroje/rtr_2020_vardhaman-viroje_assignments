#include<windows.h>
#include<stdio.h>
#include"three_animate_light_sphere.h"
#include "vmath.h"
#include<GL/GLEW.h>
#include<GL/GL.h>
#include<math.h>
#include "Sphere.h"


#define WIN_WIDTH 800
#define WIN_HEIGHT 600

#pragma comment(lib,"GLEW32.lib")
#pragma comment(lib,"OpenGL32.lib")
#pragma comment(lib,"Sphere.lib")

using namespace vmath;

LRESULT CALLBACK WndProc(HWND,UINT,WPARAM,LPARAM);

FILE* gpFile=NULL;
DWORD dwStyle;
WINDOWPLACEMENT wpPrev={sizeof(WINDOWPLACEMENT)};
bool gbFullscreen=false;
bool gbActiveWindow=false;
HWND ghwnd=NULL;
HDC ghdc=NULL;
HGLRC ghrc=NULL;

enum{
	VSV_ATTRIBUTE_POSITION=0,
	VSV_ATTRIBUTE_COLOR,
	VSV_ATTRIBUTE_TEXCOORD,
	VSV_ATTRIBUTE_NORMAL,
};

//-------------PF---------------
GLuint gShaderProgramObject_PF;

GLuint modelMatrixUniform_PF;
GLuint viewMatrixUniform_PF;
GLuint projectionlMatrixUniform_PF;

GLuint lightAmbientUniform_PF[3];
GLuint lightDiffuseUniform_PF[3];
GLuint lightSpecularUniform_PF[3];
GLuint lightPositionUniform_PF[3];

GLuint materialAmbientUniform_PF;
GLuint materialDiffuseUniform_PF;
GLuint materialSpecularUniform_PF;
GLuint materialShininessUniform_PF;

GLuint LKeyPressedUniform_PF;

//-----------PV-------------------------------------------
GLuint gShaderProgramObject_PV;

GLuint modelMatrixUniform_PV;
GLuint viewMatrixUniform_PV;
GLuint projectionlMatrixUniform_PV;

GLuint lightAmbientUniform_PV[3];
GLuint lightDiffuseUniform_PV[3];
GLuint lightSpecularUniform_PV[3];
GLuint lightPositionUniform_PV[3];

GLuint materialAmbientUniform_PV;
GLuint materialDiffuseUniform_PV;
GLuint materialSpecularUniform_PV;
GLuint materialShininessUniform_PV;

GLuint LKeyPressedUniform_PV;

//-----------comman------------
GLuint gTempShaderProgramObject;

GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;

GLuint vaoSphere;
GLuint vboSphere_position;
GLuint vboSphere_normal;
GLuint vboSphere_elements;

bool bLights;
bool bTogglePVxOrPF;//True-Per Fragment and False-Per Vertex

mat4 perspectiveProjectionMatrix;

typedef struct Light {
	vec4 LightAmbient;
	vec4 LightDefuse;
	vec4 LightSpecular;
	vec4 LightPosition;
}LightStruct;

LightStruct light[3] = { 
						{
							vec4(0.0f,0.0f,0.0f,1.0f),
							vec4(1.0f,0.0f,0.0f,1.0f),
							vec4(1.0f,0.0f,0.0f,1.0f),
							vec4(2.0f,0.0f,0.0f,1.0f)
						},
						{
							vec4(0.0f,0.0f,0.0f,1.0f),
							vec4(0.0f,1.0f,0.0f,1.0f),
							vec4(0.0f,1.0f,0.0f,1.0f),
							vec4(-2.0f,0.0f,0.0f,1.0f)
						},
						{
							vec4(0.0f,0.0f,0.0f,1.0f),
							vec4(0.0f,0.0f,1.0f,1.0f),
							vec4(0.0f,0.0f,1.0f,1.0f),
							vec4(0.0f,0.0f,0.0f,1.0f)
						}
					};


GLfloat materialAmbient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat materilaDefuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat materialSpecular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat materialShininess = 128.0f;

float spherePosition[1146];
float sphereNormals[1146];
float sphereTexcoord[764];
unsigned short sphereElement[2280];//Face Indices

GLuint gNumSphereVertices, gNumSphereElements;


int WINAPI WinMain(HINSTANCE hInstance,HINSTANCE hPrevInstance,LPSTR lpszCmdline,int iCmdShow)
{
	void Initialize(void);
	void Display(void);

	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[]=TEXT("SphereWith3Light");
	int X1,X2,Y1,Y2;
	bool bDone=false;

	if(fopen_s(&gpFile,"log.txt","w")!=0)
	{
		MessageBox(NULL,TEXT("Failed to create log file!"),TEXT("ERROR"),MB_OK | MB_ICONERROR);
		exit(0);
	}

	X1=GetSystemMetrics(SM_CXSCREEN)/2;
	Y1=GetSystemMetrics(SM_CYSCREEN)/2;

	X2=WIN_WIDTH/2;
	Y2=WIN_HEIGHT/2;

	wndclass.cbSize=sizeof(WNDCLASSEX);
	wndclass.style=CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra=0;
	wndclass.cbWndExtra=0;
	wndclass.lpfnWndProc=WndProc;
	wndclass.hInstance=hInstance;
	wndclass.hIcon=LoadIcon(hInstance,MAKEINTRESOURCE(MYICON));
	wndclass.hCursor=LoadCursor(NULL,IDC_ARROW);
	wndclass.hbrBackground=(HBRUSH)GetStockObject(GRAY_BRUSH);
	wndclass.lpszClassName=szAppName;
	wndclass.lpszMenuName=NULL;
	wndclass.hIconSm=LoadIcon(hInstance,MAKEINTRESOURCE(MYICON));

	RegisterClassEx(&wndclass);

	hwnd=CreateWindowEx(WS_EX_APPWINDOW,
						szAppName,
						TEXT("Sphere With 3 Light"),
						WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
						X1-X2,
						Y1-Y2,
						WIN_WIDTH,
						WIN_HEIGHT,
						NULL,
						NULL,
						hInstance,
						NULL);

	ghwnd=hwnd;
	Initialize();

	ShowWindow(hwnd,iCmdShow);
	SetForegroundWindow(hwnd);

	SetFocus(hwnd);

	while(bDone==false)
	{
		if(PeekMessage(&msg,NULL,0,0,PM_REMOVE))
		{
			if(msg.message==WM_QUIT)
			{
				bDone=true;
			}else{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}else{
			
				Display();
			
		}
	}

	return ((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd,UINT iMsg,WPARAM wParam,LPARAM lParam)
{
	void ToggleFullscreen(void);
	void Resize(int,int);
	void UnInitialize(void);

	switch(iMsg)
	{
		case WM_CREATE:
			fprintf(gpFile,"\nlog is started!");
			break;
		case WM_KEYDOWN:
			switch(wParam)
			{
				case VK_ESCAPE:
					ToggleFullscreen();
					break;
				case 0x46://per Fragment 
				case 0x66://f,F
					bTogglePVxOrPF = true;
					break;
				case 'v':
				case 'V':
					bTogglePVxOrPF = false;
					break;
				case 'l':
				case 'L':
					bLights = !bLights;
					if (bLights)
					{
						bTogglePVxOrPF = false;
					}
					break;
				case 'q':
				case 'Q':
					DestroyWindow(hwnd);
					break;
				default:
					break;
			}
			break;
		case WM_SETFOCUS:
			gbActiveWindow=true;
			break;
		case WM_KILLFOCUS:
			gbActiveWindow=false;
			break;

		case WM_ERASEBKGND:
			return 0;
		case WM_SIZE:
			Resize(LOWORD(lParam),HIWORD(lParam));
			break;
		case WM_CLOSE:
			DestroyWindow(hwnd);
			break;
		case WM_DESTROY:
			UnInitialize();
			PostQuitMessage(0);
			break;
		default:
			break;
	}

	return (DefWindowProc(hwnd,iMsg,wParam,lParam));
}

void ToggleFullscreen(void)
{
	MONITORINFO mi={sizeof(MONITORINFO)};
	if(gbFullscreen==false)
	{
		dwStyle=GetWindowLong(ghwnd,GWL_STYLE);
		if(dwStyle & WS_OVERLAPPEDWINDOW)
		{
			if(GetWindowPlacement(ghwnd,&wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd,MONITORINFOF_PRIMARY),&mi))
			{
				fprintf(gpFile,"\nFullscreen Enabled");
				SetWindowLong(ghwnd,GWL_STYLE,(dwStyle & ~WS_OVERLAPPEDWINDOW));
				SetWindowPos(ghwnd,HWND_TOP,mi.rcMonitor.left,mi.rcMonitor.top,mi.rcMonitor.right-mi.rcMonitor.left,mi.rcMonitor.bottom-mi.rcMonitor.top,SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		gbFullscreen=true;
	}
	else{
		SetWindowLong(ghwnd,GWL_STYLE,(dwStyle | WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(ghwnd,&wpPrev);
		SetWindowPos(ghwnd,HWND_TOP,0,0,0,0,SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED );
		fprintf(gpFile,"\nNormal Window");
		gbFullscreen=false;
	}
}

void Initialize(void)
{
	void Resize(int,int);

	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	ghdc=GetDC(ghwnd);

	ZeroMemory(&pfd,sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize=sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion=1;
	pfd.dwFlags=PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType=PFD_TYPE_RGBA;
	pfd.cColorBits=32;
	pfd.cRedBits=8;
	pfd.cGreenBits=8;
	pfd.cBlueBits=8;
	pfd.cAlphaBits=8;
	pfd.cDepthBits=32;

	iPixelFormatIndex=ChoosePixelFormat(ghdc,&pfd);

	if(iPixelFormatIndex==0)
	{
		fprintf(gpFile,"\nChoosePixelFormat() failed to get pixel format index");
		DestroyWindow(ghwnd);
	}

	if(SetPixelFormat(ghdc,iPixelFormatIndex,&pfd)==FALSE)
	{
		fprintf(gpFile,"\nSetPixelFormat() failed to set pixel format for window");
		DestroyWindow(ghwnd);
	}

	ghrc=wglCreateContext(ghdc);
	if(ghrc==NULL)
	{
		fprintf(gpFile,"\nwglCreateContext() failed to get the rendering context");
		DestroyWindow(ghwnd);
	}

	if(wglMakeCurrent(ghdc,ghrc)==FALSE)
	{
		fprintf(gpFile,"\nwglMakeCurrent() failed to swith current context from device to rendering context");
		DestroyWindow(ghwnd);
	}

	GLenum glew_error=glewInit();
	if(glew_error != GLEW_OK)
	{
		fprintf(gpFile,"\nglewInit() failed to enable extension for programmable pipeline");
		DestroyWindow(ghwnd);
	}

	fprintf(gpFile,"\nOpenGL Information are available below:-");
	fprintf(gpFile,"\n*********************************************************************************");

	fprintf(gpFile,"\nOpenGL Vender:%s",glGetString(GL_VENDOR));
	fprintf(gpFile,"\nOpenGL Render:%s",glGetString(GL_RENDERER));
	fprintf(gpFile,"\nOpenGL Version:%s",glGetString(GL_VERSION));
	fprintf(gpFile,"\nOpenGL GLSL Version:%s",glGetString(GL_SHADING_LANGUAGE_VERSION));

	fprintf(gpFile,"\nOpenGL Extension's List:\n");

	GLint numExt;
	glGetIntegerv(GL_NUM_EXTENSIONS,&numExt);

	for(int i=0;i<numExt;i++)
	{
		fprintf(gpFile,"\n%s",glGetStringi(GL_EXTENSIONS,i));
	}
	fprintf(gpFile,"\n*********************************************************************************");
		
	GLint infoLogLength;
	GLint shaderCompileStatus;
	GLint shaderProgramLinkStatus;
	GLchar *szBuffer;
	GLsizei written;


	//Per Vertex Shader Code Start--------------------
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	const GLchar* vertexShaderSourceCode_PV =
		"#version 460 core\n"\
		"\n"\
		"in vec4 vPosition;\n"\
		"in vec3 vNormal;\n"\

		"uniform mat4 u_modelMatrix;\n"\
		"uniform mat4 u_viewMatrix;\n"\
		"uniform mat4 u_projectionMatrix;\n"\

		"uniform vec3 u_lightAmbient[3];\n"\
		"uniform vec3 u_lightDiffuse[3];\n"\
		"uniform vec3 u_lightSpecular[3];\n"\
		"uniform vec4 u_lightPosition[3];\n"\

		"uniform vec3 u_materialAmbient;\n"\
		"uniform vec3 u_materialDiffuse;\n"\
		"uniform vec3 u_materialSpecular;\n"\
		"uniform float u_materialShininess;\n"\

		"uniform int u_lKeyPressed;\n"\

		"out vec3 out_phongADS_Light;\n"\

		"void main(void)\n"\

		"{\n"\
		"if (u_lKeyPressed == 1)\n"\
		"{\n"\

		"vec4 eyeCoord = u_viewMatrix * u_modelMatrix * vPosition;\n"\
		"vec3 viewVector = normalize( -eyeCoord.xyz);\n"\
		"vec3 transformedNormal = normalize( mat3(u_viewMatrix * u_modelMatrix) * vNormal );\n"\

		"vec3 lightDirection[3];\n"\
		"vec3 reflectionVector[3];\n"\

		"vec3 ambient[3];\n"\
		"vec3 diffuse[3];\n"\
		"vec3 specular[3];\n"\

		"for (int i = 0; i < 3; i++)\n"\
		"{\n"\

		"lightDirection[i] = normalize( vec3( u_lightPosition[i] - eyeCoord ) );\n"\

		"reflectionVector[i] = reflect( -lightDirection[i], transformedNormal );\n"\

		"ambient[i]= u_lightAmbient[i] * u_materialAmbient;\n"\

		"diffuse[i] = u_lightDiffuse[i] * u_materialDiffuse * max( dot(lightDirection[i] , transformedNormal), 0.0);\n"\

		"specular[i] = u_lightSpecular[i] * u_materialSpecular * pow( max( dot(reflectionVector[i] , viewVector), 0.0f) , u_materialShininess);\n"\

		"out_phongADS_Light += ambient[i] + diffuse[i] + specular[i];\n"\

		"}\n"\

		"}\n"\
		"else\n"\
		"{\n"\

		"out_phongADS_Light = vec3(1.0f, 1.0f, 1.0f);\n"\

		"}\n"\

		"gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix * vPosition;\n"\

		"}";

	glShaderSource(gVertexShaderObject,
		1,
		(const GLchar**)&vertexShaderSourceCode_PV,
		NULL);

	glCompileShader(gVertexShaderObject);

	glGetShaderiv(gVertexShaderObject,
		GL_COMPILE_STATUS,
		&shaderCompileStatus);

	if (shaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject,
			GL_INFO_LOG_LENGTH,
			&infoLogLength);

		if (infoLogLength > 0)
		{
			szBuffer = (GLchar*)malloc(sizeof(GLchar) * infoLogLength);
			if (szBuffer != NULL)
			{
				glGetShaderInfoLog(gVertexShaderObject,
					infoLogLength,
					&written,
					szBuffer);
				fprintf(gpFile, "\nERROR:Vertex Shader Object Compilation Failed log : %s", szBuffer);
				free(szBuffer);
				DestroyWindow(ghwnd);
			}
		}
	}

	infoLogLength = 0;
	shaderCompileStatus = 0;
	shaderProgramLinkStatus = 0;
	szBuffer = NULL;
	written = 0;

	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	const GLchar* fragmentShaderSourceCode_PV =
		"#version 460 core"\
		"\n"\
		"in vec3 out_phongADS_Light;\n"
		"out vec4 fragColor;"\
		"void main(void)"\
		"{"\
		"fragColor = vec4( out_phongADS_Light, 1.0f );"\
		"}";

	glShaderSource(gFragmentShaderObject,
		1,
		(const GLchar**)&fragmentShaderSourceCode_PV,
		NULL);

	glCompileShader(gFragmentShaderObject);

	glGetShaderiv(gFragmentShaderObject,
		GL_COMPILE_STATUS,
		&shaderCompileStatus);

	if (shaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject,
			GL_INFO_LOG_LENGTH,
			&infoLogLength);

		if (infoLogLength > 0)
		{
			szBuffer = (GLchar*)malloc(sizeof(GLchar) * infoLogLength);
			if (szBuffer != NULL)
			{
				glGetShaderInfoLog(gFragmentShaderObject,
					infoLogLength,
					&written,
					szBuffer);
				fprintf(gpFile, "\nERROR:Fragment Shader Object Compilation Failed log : %s", szBuffer);
				free(szBuffer);
				DestroyWindow(ghwnd);
			}
		}
	}

	infoLogLength = 0;
	shaderCompileStatus = 0;
	shaderProgramLinkStatus = 0;
	szBuffer = NULL;
	written = 0;

	gShaderProgramObject_PV = glCreateProgram();

	glAttachShader(gShaderProgramObject_PV, gVertexShaderObject);
	glAttachShader(gShaderProgramObject_PV, gFragmentShaderObject);

	//Shader Attribute Binding Before Pre Linking of attached Shader
	glBindAttribLocation(gShaderProgramObject_PV,
		VSV_ATTRIBUTE_POSITION,
		"vPosition");
	glBindAttribLocation(gShaderProgramObject_PV,
		VSV_ATTRIBUTE_NORMAL,
		"vNormal");

	glLinkProgram(gShaderProgramObject_PV);

	glGetProgramiv(gShaderProgramObject_PV,
		GL_LINK_STATUS,
		&shaderProgramLinkStatus);

	if (shaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject_PV,
			GL_INFO_LOG_LENGTH,
			&infoLogLength);
		if (infoLogLength > 0)
		{
			szBuffer = (GLchar*)malloc(sizeof(GLchar) * infoLogLength);

			if (szBuffer != NULL)
			{
				glGetProgramInfoLog(gShaderProgramObject_PV,
					infoLogLength,
					&written,
					szBuffer);

				fprintf(gpFile, "\nERROR:Shader program Link failed log : %s", szBuffer);
				free(szBuffer);
				DestroyWindow(ghwnd);
			}
		}
	}

	//Shader Code End--------------------


	//Uniform variable Binding after post linking of attached shader 
	modelMatrixUniform_PV = glGetUniformLocation(gShaderProgramObject_PV, "u_modelMatrix");
	viewMatrixUniform_PV = glGetUniformLocation(gShaderProgramObject_PV, "u_viewMatrix");
	projectionlMatrixUniform_PV = glGetUniformLocation(gShaderProgramObject_PV, "u_projectionMatrix");


	lightAmbientUniform_PV[0] = glGetUniformLocation(gShaderProgramObject_PV, "u_lightAmbient[0]");
	lightDiffuseUniform_PV[0] = glGetUniformLocation(gShaderProgramObject_PV, "u_lightDiffuse[0]");
	lightSpecularUniform_PV[0] = glGetUniformLocation(gShaderProgramObject_PV, "u_lightSpecular[0]");
	lightPositionUniform_PV[0] = glGetUniformLocation(gShaderProgramObject_PV, "u_lightPosition[0]");

	lightAmbientUniform_PV[1] = glGetUniformLocation(gShaderProgramObject_PV, "u_lightAmbient[1]");
	lightDiffuseUniform_PV[1] = glGetUniformLocation(gShaderProgramObject_PV, "u_lightDiffuse[1]");
	lightSpecularUniform_PV[1] = glGetUniformLocation(gShaderProgramObject_PV, "u_lightSpecular[1]");
	lightPositionUniform_PV[1] = glGetUniformLocation(gShaderProgramObject_PV, "u_lightPosition[1]");

	lightAmbientUniform_PV[2] = glGetUniformLocation(gShaderProgramObject_PV, "u_lightAmbient[2]");
	lightDiffuseUniform_PV[2] = glGetUniformLocation(gShaderProgramObject_PV, "u_lightDiffuse[2]");
	lightSpecularUniform_PV[2] = glGetUniformLocation(gShaderProgramObject_PV, "u_lightSpecular[2]");
	lightPositionUniform_PV[2] = glGetUniformLocation(gShaderProgramObject_PV, "u_lightPosition[2]");

	materialAmbientUniform_PV = glGetUniformLocation(gShaderProgramObject_PV, "u_materialAmbient");
	materialDiffuseUniform_PV = glGetUniformLocation(gShaderProgramObject_PV, "u_materialDiffuse");
	materialSpecularUniform_PV = glGetUniformLocation(gShaderProgramObject_PV, "u_materialSpecular");
	materialShininessUniform_PV = glGetUniformLocation(gShaderProgramObject_PV, "u_materialShininess");

	LKeyPressedUniform_PV = glGetUniformLocation(gShaderProgramObject_PV, "u_lKeyPressed");



//############################################################################################
//############################################################################################

	//Per Fragment Shader Code Start--------------------
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	const GLchar* vertexShaderSourceCode_PF =
		"#version 460 core\n"\
		"\n"\
		"in vec4 vPosition;\n"\
		"in vec3 vNormal;\n"\

		"uniform mat4 u_modelMatrix;\n"\
		"uniform mat4 u_viewMatrix;\n"\
		"uniform mat4 u_projectionMatrix;\n"\

		"uniform vec4 u_lightPosition[3];\n"\

		"uniform int u_lKeyPressed;\n"\

		"out vec3 out_transformedNormal;\n"\
		"out vec3 out_lightDirection[3];\n"\
		"out vec3 out_viewVector;\n"\

		"void main(void)\n"\

		"{\n"\
			"if (u_lKeyPressed == 1)\n"\
			"{\n"\

				"vec4 eyeCoord = u_viewMatrix * u_modelMatrix * vPosition;\n"\

				"out_viewVector = -eyeCoord.xyz;\n"\

				"out_transformedNormal = mat3(u_viewMatrix * u_modelMatrix) * vNormal ;\n"\

				"for (int i = 0; i < 3; i++)\n"\
				"{\n"\
					"out_lightDirection[i] =  vec3( u_lightPosition[i] - eyeCoord ) ;\n"\
				"}\n"\

			"}\n"\
		

			"gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix * vPosition;\n"\

		"}";

	glShaderSource(gVertexShaderObject,
		1,
		(const GLchar**)&vertexShaderSourceCode_PF,
		NULL);

	glCompileShader(gVertexShaderObject);

	glGetShaderiv(gVertexShaderObject,
		GL_COMPILE_STATUS,
		&shaderCompileStatus);

	if (shaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject,
			GL_INFO_LOG_LENGTH,
			&infoLogLength);

		if (infoLogLength > 0)
		{
			szBuffer = (GLchar*)malloc(sizeof(GLchar) * infoLogLength);
			if (szBuffer != NULL)
			{
				glGetShaderInfoLog(gVertexShaderObject,
					infoLogLength,
					&written,
					szBuffer);
				fprintf(gpFile, "\nERROR:Vertex Shader Object Compilation Failed log : %s", szBuffer);
				free(szBuffer);
				DestroyWindow(ghwnd);
			}
		}
	}

	infoLogLength = 0;
	shaderCompileStatus = 0;
	shaderProgramLinkStatus = 0;
	szBuffer = NULL;
	written = 0;

	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	const GLchar* fragmentShaderSourceCode_PF =
		"#version 460 core"\
		"\n"\

		"uniform vec3 u_lightAmbient[3];\n"\
		"uniform vec3 u_lightDiffuse[3];\n"\
		"uniform vec3 u_lightSpecular[3];\n"\

		"uniform vec3 u_materialAmbient;\n"\
		"uniform vec3 u_materialDiffuse;\n"\
		"uniform vec3 u_materialSpecular;\n"\
		"uniform float u_materialShininess;\n"\

		"uniform int u_lKeyPressed;\n"\

		"in vec3 out_transformedNormal;\n"\
		"in vec3 out_lightDirection[3];\n"\
		"in vec3 out_viewVector;\n"\

		"vec3 phongADS_Light;\n"
		"out vec4 fragColor;"\

		"void main(void)"\
		"{"\

			"if (u_lKeyPressed == 1)\n"\
			"{\n"\

				"vec3 normalizedTransformedNormal = normalize( out_transformedNormal );\n"\

				"vec3 normalizedLightDirection[3];\n"\

				"for (int i = 0; i < 3; i++)\n"\
				"{\n"\
					" normalizedLightDirection[i] = normalize( out_lightDirection[i] );\n"\
				"}\n"\
			
				"vec3 normalizedViewVector = normalize( out_viewVector );\n"\

				"vec3 reflectionVector[3];\n"\
				"vec3 ambient[3];\n"\
				"vec3 diffuse[3];\n"\
				"vec3 specular[3];\n"\

				"for (int i = 0; i < 3; i++)\n"\
				"{\n"\
				
					"reflectionVector[i] = reflect( -normalizedLightDirection[i], normalizedTransformedNormal );\n"\

					"ambient[i]= u_lightAmbient[i] * u_materialAmbient;\n"\

					"diffuse[i] = u_lightDiffuse[i] * u_materialDiffuse * max( dot(normalizedLightDirection[i] , normalizedTransformedNormal), 0.0);\n"\

					"specular[i] = u_lightSpecular[i] * u_materialSpecular * pow( max( dot(reflectionVector[i] , normalizedViewVector), 0.0f) , u_materialShininess);\n"\

					"phongADS_Light += ambient[i] + diffuse[i] + specular[i];\n"\

				"}\n"\

			"}\n"\
			"else\n"\
			"{\n"\
				"phongADS_Light=vec3(1.0f,1.0f,1.0f);\n"\
			"}\n"\

			"fragColor = vec4( phongADS_Light, 1.0f );"\
		"}";

	glShaderSource(gFragmentShaderObject,
		1,
		(const GLchar**)&fragmentShaderSourceCode_PF,
		NULL);

	glCompileShader(gFragmentShaderObject);

	glGetShaderiv(gFragmentShaderObject,
		GL_COMPILE_STATUS,
		&shaderCompileStatus);

	if (shaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject,
			GL_INFO_LOG_LENGTH,
			&infoLogLength);

		if (infoLogLength > 0)
		{
			szBuffer = (GLchar*)malloc(sizeof(GLchar) * infoLogLength);
			if (szBuffer != NULL)
			{
				glGetShaderInfoLog(gFragmentShaderObject,
					infoLogLength,
					&written,
					szBuffer);
				fprintf(gpFile, "\nERROR:Fragment Shader Object Compilation Failed log : %s", szBuffer);
				free(szBuffer);
				DestroyWindow(ghwnd);
			}
		}
	}

	infoLogLength = 0;
	shaderCompileStatus = 0;
	shaderProgramLinkStatus = 0;
	szBuffer = NULL;
	written = 0;

	gShaderProgramObject_PF = glCreateProgram();

	glAttachShader(gShaderProgramObject_PF, gVertexShaderObject);
	glAttachShader(gShaderProgramObject_PF, gFragmentShaderObject);

	//Shader Attribute Binding Before Pre Linking of attached Shader
	glBindAttribLocation(gShaderProgramObject_PF,
		VSV_ATTRIBUTE_POSITION,
		"vPosition");
	glBindAttribLocation(gShaderProgramObject_PF,
		VSV_ATTRIBUTE_NORMAL,
		"vNormal");

	glLinkProgram(gShaderProgramObject_PF);

	glGetProgramiv(gShaderProgramObject_PF,
		GL_LINK_STATUS,
		&shaderProgramLinkStatus);

	if (shaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject_PF,
			GL_INFO_LOG_LENGTH,
			&infoLogLength);
		if (infoLogLength > 0)
		{
			szBuffer = (GLchar*)malloc(sizeof(GLchar) * infoLogLength);

			if (szBuffer != NULL)
			{
				glGetProgramInfoLog(gShaderProgramObject_PF,
					infoLogLength,
					&written,
					szBuffer);

				fprintf(gpFile, "\nERROR:Shader program Link failed log : %s", szBuffer);
				free(szBuffer);
				DestroyWindow(ghwnd);
			}
		}
	}

	//Shader Code End--------------------


	//Uniform variable Binding after post linking of attached shader 
	modelMatrixUniform_PF = glGetUniformLocation(gShaderProgramObject_PF, "u_modelMatrix");
	viewMatrixUniform_PF = glGetUniformLocation(gShaderProgramObject_PF, "u_viewMatrix");
	projectionlMatrixUniform_PF = glGetUniformLocation(gShaderProgramObject_PF, "u_projectionMatrix");

	
	lightAmbientUniform_PF[0] = glGetUniformLocation(gShaderProgramObject_PF, "u_lightAmbient[0]");
	lightDiffuseUniform_PF[0] = glGetUniformLocation(gShaderProgramObject_PF, "u_lightDiffuse[0]");
	lightSpecularUniform_PF[0] = glGetUniformLocation(gShaderProgramObject_PF, "u_lightSpecular[0]");
	lightPositionUniform_PF[0] = glGetUniformLocation(gShaderProgramObject_PF, "u_lightPosition[0]");
	
	lightAmbientUniform_PF[1] = glGetUniformLocation(gShaderProgramObject_PF, "u_lightAmbient[1]");
	lightDiffuseUniform_PF[1] = glGetUniformLocation(gShaderProgramObject_PF, "u_lightDiffuse[1]");
	lightSpecularUniform_PF[1] = glGetUniformLocation(gShaderProgramObject_PF, "u_lightSpecular[1]");
	lightPositionUniform_PF[1] = glGetUniformLocation(gShaderProgramObject_PF, "u_lightPosition[1]");

	lightAmbientUniform_PF[2] = glGetUniformLocation(gShaderProgramObject_PF, "u_lightAmbient[2]");
	lightDiffuseUniform_PF[2] = glGetUniformLocation(gShaderProgramObject_PF, "u_lightDiffuse[2]");
	lightSpecularUniform_PF[2] = glGetUniformLocation(gShaderProgramObject_PF, "u_lightSpecular[2]");
	lightPositionUniform_PF[2] = glGetUniformLocation(gShaderProgramObject_PF, "u_lightPosition[2]");

	materialAmbientUniform_PF = glGetUniformLocation(gShaderProgramObject_PF, "u_materialAmbient");
	materialDiffuseUniform_PF = glGetUniformLocation(gShaderProgramObject_PF, "u_materialDiffuse");
	materialSpecularUniform_PF = glGetUniformLocation(gShaderProgramObject_PF, "u_materialSpecular");
	materialShininessUniform_PF = glGetUniformLocation(gShaderProgramObject_PF, "u_materialShininess");

	LKeyPressedUniform_PF = glGetUniformLocation(gShaderProgramObject_PF, "u_lKeyPressed");

	//VAO and its respective VBO preparation

	getSphereVertexData(spherePosition, sphereNormals, sphereTexcoord, sphereElement);
	gNumSphereVertices = getNumberOfSphereVertices();
	gNumSphereElements = getNumberOfSphereElements();


		glGenVertexArrays(1,&vaoSphere);
		glBindVertexArray(vaoSphere);

			glGenBuffers(1,&vboSphere_position);
			glBindBuffer(GL_ARRAY_BUFFER, vboSphere_position);

				glBufferData(GL_ARRAY_BUFFER,
							sizeof(spherePosition),
							spherePosition,
							GL_STATIC_DRAW);

				glVertexAttribPointer(VSV_ATTRIBUTE_POSITION,
									3,
									GL_FLOAT,
									GL_FALSE,
									0,
									NULL);

				glEnableVertexAttribArray(VSV_ATTRIBUTE_POSITION);
			
			glBindBuffer(GL_ARRAY_BUFFER,0);

			glGenBuffers(1, &vboSphere_normal);
			glBindBuffer(GL_ARRAY_BUFFER, vboSphere_normal);

			glBufferData(GL_ARRAY_BUFFER,
				sizeof(sphereNormals),
				sphereNormals,
				GL_STATIC_DRAW);

			glVertexAttribPointer(VSV_ATTRIBUTE_NORMAL,
				3,
				GL_FLOAT,
				GL_FALSE,
				0,
				NULL);

			glEnableVertexAttribArray(VSV_ATTRIBUTE_NORMAL);

			glBindBuffer(GL_ARRAY_BUFFER, 0);

			glGenBuffers(1,
				&vboSphere_elements);

			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,
				vboSphere_elements);

				glBufferData(GL_ELEMENT_ARRAY_BUFFER,
					sizeof(sphereElement),
					sphereElement,
					GL_STATIC_DRAW);

			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,
				0);

		glBindVertexArray(0);

	glClearColor(0.0,0.0,0.0f,0.0f);

	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	perspectiveProjectionMatrix=mat4::identity();
	Resize(WIN_WIDTH,WIN_HEIGHT);

}

void Resize(int width,int height)
{
	if(height==0)
	{
		height=1;
	}
	glViewport(0,0,(GLsizei)width,(GLsizei)height);

	perspectiveProjectionMatrix=vmath::perspective(45.0f,
													(GLfloat)width/(GLfloat)height,
													0.1f,
													100.0f);
}

GLfloat Angle = 0.0f;

GLfloat radius = 3.0f;

void Display(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	if (bTogglePVxOrPF)
	{
		glUseProgram(gShaderProgramObject_PF);
		mat4 translateMatrix;
		mat4 rotateMatrix;

		mat4 modelMatrix;
		mat4 viewMatrix = mat4::identity();

		translateMatrix = vmath::translate(0.0f, 0.0f, -1.5f);
		modelMatrix = translateMatrix ;

		glUniformMatrix4fv(modelMatrixUniform_PF,
			1,
			GL_FALSE,
			modelMatrix);

		glUniformMatrix4fv(viewMatrixUniform_PF,
			1,
			GL_FALSE,
			viewMatrix);

		glUniformMatrix4fv(projectionlMatrixUniform_PF,
			1,
			GL_FALSE,
			perspectiveProjectionMatrix);

		if (bLights == true)
		{
			glUniform1i(LKeyPressedUniform_PF, 1);

			for (int i = 0; i < 3; i++)
			{
				glUniform3fv(lightAmbientUniform_PF[i], 1, light[i].LightAmbient);
				glUniform3fv(lightDiffuseUniform_PF[i], 1, light[i].LightDefuse);
				glUniform3fv(lightSpecularUniform_PF[i], 1, light[i].LightSpecular);

				if (i == 0){
					light[i].LightPosition = vec4(0.0f, radius * sin(Angle), radius * cos(Angle), 1.0f);
				}
				else if (i == 1){
					light[i].LightPosition = vec4(radius * sin(Angle), 0.0f, radius * cos(Angle), 1.0f);
				}
				else if (i == 2){
					light[i].LightPosition = vec4(radius * sin(Angle), radius * cos(Angle), 0.0f, 1.0f);
				}

				glUniform4fv(lightPositionUniform_PF[i], 1, light[i].LightPosition);//posiitional light
			}
			
			glUniform3fv(materialAmbientUniform_PF, 1, materialAmbient);
			glUniform3fv(materialDiffuseUniform_PF, 1, materilaDefuse);
			glUniform3fv(materialSpecularUniform_PF, 1, materialSpecular);

			glUniform1f(materialShininessUniform_PF, materialShininess);
		}
		else
		{
			glUniform1i(LKeyPressedUniform_PF, 0);
		}

	}
	else {
		glUseProgram(gShaderProgramObject_PV);
		mat4 translateMatrix;
		mat4 rotateMatrix;

		mat4 modelMatrix;
		mat4 viewMatrix = mat4::identity();

		translateMatrix = vmath::translate(0.0f, 0.0f, -1.5f);
		modelMatrix = translateMatrix;

		glUniformMatrix4fv(modelMatrixUniform_PV,
			1,
			GL_FALSE,
			modelMatrix);

		glUniformMatrix4fv(viewMatrixUniform_PV,
			1,
			GL_FALSE,
			viewMatrix);

		glUniformMatrix4fv(projectionlMatrixUniform_PV,
			1,
			GL_FALSE,
			perspectiveProjectionMatrix);

		if (bLights == true)
		{
			glUniform1i(LKeyPressedUniform_PV, 1);

			for (int i = 0; i < 3; i++)
			{
				glUniform3fv(lightAmbientUniform_PV[i], 1, light[i].LightAmbient);
				glUniform3fv(lightDiffuseUniform_PV[i], 1, light[i].LightDefuse);
				glUniform3fv(lightSpecularUniform_PV[i], 1, light[i].LightSpecular);

				if (i == 0) {
					light[i].LightPosition = vec4(0.0f, radius * sin(Angle), radius * cos(Angle), 1.0f);
				}
				else if (i == 1) {
					light[i].LightPosition = vec4(radius * sin(Angle), 0.0f, radius * cos(Angle), 1.0f);
				}
				else if (i == 2) {
					light[i].LightPosition = vec4(radius * sin(Angle), radius * cos(Angle), 0.0f, 1.0f);
				}

				glUniform4fv(lightPositionUniform_PV[i], 1, light[i].LightPosition);//posiitional light
			}

			glUniform3fv(materialAmbientUniform_PV, 1, materialAmbient);
			glUniform3fv(materialDiffuseUniform_PV, 1, materilaDefuse);
			glUniform3fv(materialSpecularUniform_PV, 1, materialSpecular);

			glUniform1f(materialShininessUniform_PV, materialShininess);
		}
		else
		{
			glUniform1i(LKeyPressedUniform_PV, 0);
		}
	}


		glBindVertexArray(vaoSphere);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboSphere_elements);
				glDrawElements(GL_TRIANGLES, gNumSphereElements, GL_UNSIGNED_SHORT, 0);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		glBindVertexArray(0);

	glUseProgram(0);


	SwapBuffers(ghdc);

	Angle += 0.05f;
}

void UnInitialize(void)
{
	if(gbFullscreen==true)
	{
		SetWindowLong(ghwnd,GWL_STYLE,(dwStyle | WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(ghwnd,&wpPrev);
		SetWindowPos(ghwnd,HWND_TOP,0,0,0,0,SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED );
		fprintf(gpFile,"\nNormal Window");
		gbFullscreen=false;
	}

	if(vaoSphere)
	{
		glDeleteVertexArrays(1,
							&vaoSphere);
	}
	vaoSphere = 0;

	if(vboSphere_position)
	{
		glDeleteBuffers(1,
					&vboSphere_position);
	}
	vboSphere_position = 0;
	
	if (vboSphere_normal)
	{
		glDeleteBuffers(1,
			&vboSphere_normal);
	}
	vboSphere_normal = 0;

	if (vboSphere_elements)
	{
		glDeleteBuffers(1,
			&vboSphere_elements);
	}
	vboSphere_elements = 0;

	if(gShaderProgramObject_PF)
	{
		glUseProgram(gShaderProgramObject_PF);

		GLsizei shaderCount;
		GLsizei tempShaderCount;
		GLuint *pShader=NULL;

		glGetProgramiv(gShaderProgramObject_PF,GL_ATTACHED_SHADERS,&shaderCount);

		pShader=(GLuint *)malloc(sizeof(GLuint)*shaderCount);
		if(pShader==NULL)
		{
			fprintf(gpFile,"\nFailed to allocate memory for pShader");
		}else{
			glGetAttachedShaders(gShaderProgramObject_PF,shaderCount,&tempShaderCount,pShader);
			
			fprintf(gpFile,"\nShader program has actual attached shader count is:%d",tempShaderCount);
			
			for(GLsizei i=0;i<shaderCount;i++)
			{
				glDetachShader(gShaderProgramObject_PF,pShader[i]);
				glDeleteShader(pShader[i]);
				pShader[i]=0;
			}
			free(pShader);
			
			glDeleteProgram(gShaderProgramObject_PF);
			gShaderProgramObject_PF =0;
		}
		glUseProgram(0);
	}


	if (gShaderProgramObject_PV)
	{
		glUseProgram(gShaderProgramObject_PV);

		GLsizei shaderCount;
		GLsizei tempShaderCount;
		GLuint* pShader = NULL;

		glGetProgramiv(gShaderProgramObject_PV, GL_ATTACHED_SHADERS, &shaderCount);

		pShader = (GLuint*)malloc(sizeof(GLuint) * shaderCount);
		if (pShader == NULL)
		{
			fprintf(gpFile, "\nFailed to allocate memory for pShader");
		}
		else {
			glGetAttachedShaders(gShaderProgramObject_PV, shaderCount, &tempShaderCount, pShader);

			fprintf(gpFile, "\nShader program has actual attached shader count is:%d", tempShaderCount);

			for (GLsizei i = 0; i < shaderCount; i++)
			{
				glDetachShader(gShaderProgramObject_PV, pShader[i]);
				glDeleteShader(pShader[i]);
				pShader[i] = 0;
			}
			free(pShader);

			glDeleteProgram(gShaderProgramObject_PV);
			gShaderProgramObject_PV = 0;
		}
		glUseProgram(0);
	}

	if(wglGetCurrentContext()==ghrc)
	{
		wglMakeCurrent(NULL,NULL);
	}

	if(ghrc)
	{
		wglDeleteContext(ghrc);
	}
		ghrc=NULL;

	if(ghdc)
	{
		ReleaseDC(ghwnd,ghdc);
	}
		ghdc=NULL;
	
	if(gpFile)
	{
		fprintf(gpFile,"\nlog is ended!");
		fclose(gpFile);
	}
		gpFile=NULL;
}
