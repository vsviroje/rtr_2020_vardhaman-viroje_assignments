#include<windows.h>
#include<stdio.h>
#include"Light_24_Material_Sphere.h"
#include<GL/GL.h>
#include<GL/GLU.h>

#define WIN_WIDTH 800
#define WIN_HEIGHT 600
#pragma comment(lib,"OpenGL32.lib")
#pragma comment(lib,"GLU32.lib")

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

FILE* gpFile = NULL;
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
bool gbFullscreen = false;
bool gbActiveWindow = false;
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

bool gbLight=false;

GLfloat LightAmbient[]={0.0f,0.0f,0.0f,1.0f};//White Light
GLfloat LightDefuse[]={1.0f,1.0f,1.0f,1.0f};
GLfloat LightPosition[]={0.0f,3.0f,3.0f,0.0f};//directional light like sun

GLfloat LightModelAmbient[]={0.2f,0.2f,0.2f,1.0f};
GLfloat LightModelLocalViewer[]={0.0f};

GLfloat AngleXRotation=0.0f;
GLfloat AngleYRotation=0.0f;
GLfloat AngleZRotation=0.0f;



GLint KeyPressed=0;

GLUquadric *SphereQuadric[24];


int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	void Initialize(void);
	void Display(void);

	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("OpenGLLight24MaterialSphere");
	int X1, X2, Y1, Y2;
	bool bDone = false;

	if (fopen_s(&gpFile, "VSV_OWT_log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Failed to Create log file!"), TEXT("Error"), MB_OK | MB_ICONERROR);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "\nLog will be start from here...!");
	}

	X1 = GetSystemMetrics(SM_CXSCREEN) / 2;
	Y1 = GetSystemMetrics(SM_CYSCREEN) / 2;

	X2 = WIN_WIDTH / 2;
	Y2 = WIN_HEIGHT / 2;

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style =  CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(GRAY_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));

	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("OpenGL Light 24 Material Sphere"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		X1 - X2,//100,
		Y1 - Y2,//100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;
	Initialize();

	ShowWindow(hwnd, iCmdShow);

	SetForegroundWindow(hwnd);
	SetFocus(hwnd);
	


	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
		
			if (gbActiveWindow == true)
			{
				Display();
			}
		}
	}

	return ((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void ToggleFullscreen(void);
	void Resize(int, int);
	void UnInitialize(void);

	switch (iMsg)
	{
	case WM_CREATE:
		fprintf(gpFile, "\nOpenGL Light 24 Material Sphere Log is Started!");
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
			case VK_ESCAPE:
				DestroyWindow(hwnd);
				break;
			case 0x46:
			case 0x66:
				ToggleFullscreen();
				break;
			case 'L':
			case 'l':
				if (gbLight==true){
					glDisable(GL_LIGHTING );
					gbLight=false;
				}else{
					glEnable(GL_LIGHTING);
					gbLight=true;
				}
				break;
			case 'X':
			case 'x':
				KeyPressed=1;
				AngleXRotation=0.0f;
				break;
			case 'Y':
			case 'y':
				KeyPressed=2;
				AngleYRotation=0.0f;
				break;
			case 'Z':
			case 'z':
				KeyPressed=3;
				AngleZRotation=0.0f;
				break;
			default:
				break;
		}
		break;
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;
	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;

	case WM_ERASEBKGND:
		return 0;
	case WM_SIZE:
		Resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;
	case WM_DESTROY:
		fprintf(gpFile, "\nOpenGL Light 24 Material Sphere Log is Ended!");
		UnInitialize();
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}
void ToggleFullscreen(void)
{
	MONITORINFO mi = { sizeof(MONITORINFO) };
	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW) 
		{
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				fprintf(gpFile, "\nFullscreen Enabled");
				SetWindowLong(ghwnd, GWL_STYLE, (dwStyle & ~WS_OVERLAPPEDWINDOW));
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
		gbFullscreen = true;
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, (dwStyle | WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		fprintf(gpFile, "\nNormal Window");
		ShowCursor(TRUE);
		gbFullscreen = false;
	}
}

void Initialize(void)
{
	void Resize(int, int);
	
	//Local variable initialization
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	ghdc = GetDC(ghwnd);

	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER ;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits= 32 ;


	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (iPixelFormatIndex == 0 )
	{
		fprintf(gpFile, "\nChoosePixelFormat() Failed to get pixel format index");
		DestroyWindow(ghwnd);
	}
	else
	{
		fprintf(gpFile, "\nChoosePixelFormat() Successfully selected pixel format and recived its index");
	}

	if ( SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE )
	{
		fprintf(gpFile, "\nSetPixelFormat() Failed to set pixel format for window");
		DestroyWindow(ghwnd);
	}
	else
	{
		fprintf(gpFile, "\nSetPixelFormat() Successfully able to set pixel format");
	}

	ghrc = wglCreateContext(ghdc);
	if ( ghrc == NULL)
	{
		fprintf(gpFile, "\nwglCreateContext() Failed to get the rendering context");
		DestroyWindow(ghwnd);
	}
	else
	{
		fprintf(gpFile, "\nwglCreateContext() Successfully able to get rendering context");
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE )
	{
		fprintf(gpFile, "\nwglMakeCurrent() Failed to switch current context from device to rendering context");
		DestroyWindow(ghwnd);
	}
	else
	{
		fprintf(gpFile, "\nwglMakeCurrent() Successfully able to switch current context from device to rendering context");
	}
	//Depth
	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT,GL_NICEST);

	//Set Light Model
	glEnable(GL_AUTO_NORMAL);
	glEnable(GL_NORMALIZE);
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT,LightModelAmbient);
	glLightModelfv(GL_LIGHT_MODEL_LOCAL_VIEWER,LightModelLocalViewer);

	//Set Lighting
	glLightfv(GL_LIGHT0,GL_AMBIENT,LightAmbient);
	glLightfv(GL_LIGHT0,GL_DIFFUSE,LightDefuse);
	glEnable(GL_LIGHT0);

	//Set Material
	// glLightfv(GL_LIGHT1,GL_SPECULAR,LightSpecular);
	
	//Quadric Initailization
	for(int i=0;i<=24;i++)
	{
		SphereQuadric[i]=gluNewQuadric();
	}

	//SetColor
	glClearColor(0.25f, 0.25f, 0.25f, 1.0f);

	//Warmup resize call
	Resize(WIN_WIDTH, WIN_HEIGHT);
	

}

void Resize(int width, int height)
{
	if (height == 0)
	{
		height = 1;
	}
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	glMatrixMode(GL_PROJECTION);	
	glLoadIdentity();

	// gluPerspective(	45.0f,
	// 				(GLfloat)width/(GLfloat)height,
	// 				0.1f,
	// 				100.0f
	// 			);

	if(width<=height)
	{
		glOrtho(-10.0f,
				10.0f,
				-10.0f*((GLfloat)height/(GLfloat)width),
				10.0f*((GLfloat)height/(GLfloat)width),
				-100.0f,
				100.0f
				);
	}
	else
	{
		glOrtho(-10.0f*((GLfloat)width/(GLfloat)height),
				10.0f*((GLfloat)width/(GLfloat)height),
				-10.0f,
				10.0f,
				-100.0f,
				100.0f
				);
	}

	// if(width<=height)
	// {
	// 	glOrtho(0.0f,
	// 			15.5f,
	// 			0.0f,
	// 			15.5f*((GLfloat)height/(GLfloat)width),
	// 			-100.0f,
	// 			100.0f
	// 			);
	// }
	// else
	// {
	// 		glOrtho(0.0f,
	// 			15.5f*((GLfloat)width/(GLfloat)height),
	// 			0.0f,
	// 			15.5f,
	// 			-100.0f,
	// 			100.0f
	// 			);
	// }

}

void Display(void)
{
	void RenderGLUSphere(GLUquadric *,GLdouble,GLint,GLint);
	void Draw24Sphere(void);
	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	if (KeyPressed==1)
	{
		glRotatef(AngleXRotation,1.0f,0.0f,0.0f);
		LightPosition[1]=AngleXRotation;

		LightPosition[0]=0.0f;
		LightPosition[2]=0.0f;

	}
	
	if (KeyPressed==2)
	{
		glRotatef(AngleYRotation,0.0f,1.0f,0.0f);
		LightPosition[2]=AngleYRotation;

		LightPosition[0]=0.0f;
		LightPosition[1]=0.0f;
	}
	
	if (KeyPressed==3)
	{
		glRotatef(AngleZRotation,0.0f,0.0f,1.0f);
		LightPosition[0]=AngleZRotation;
		
		LightPosition[1]=0.0f;
		LightPosition[2]=0.0f;
	}

	glLightfv(GL_LIGHT0,GL_POSITION,LightPosition);

	Draw24Sphere();
		
	SwapBuffers(ghdc);

	AngleXRotation += 1.0f;
	AngleYRotation += 1.0f;
	AngleZRotation += 1.0f;
	
}
void UnInitialize(void)
{
	if (gbFullscreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, (dwStyle | WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}
	
	if (wglGetCurrentContext() == ghrc ) {
		wglMakeCurrent(NULL, NULL);
	}
	else
	{
		fprintf(gpFile, "\nwglGetCurrentContext current context is not rendering context so no need to switch.");
	}

	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}
	else
	{
		fprintf(gpFile, "\nRendering Context is already uninitialized or never created/initialized.");
	}

	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}
	else
	{
		fprintf(gpFile, "\nDevice context is already uninitialized or never created/initialized.");
	}


	if (gpFile)
	{
		fprintf(gpFile, "\nLog will be End from here...!");
		fclose(gpFile);
		gpFile = NULL;
	}

	for(int i=0;i<=24;i++)
	{
		gluDeleteQuadric(SphereQuadric[i]);
		SphereQuadric[i]=NULL;
	}
}



void RenderGLUSphere(GLUquadric *qobj,/*The quadric object*/
   GLdouble   radius,/*radius of sphere*/
   GLint      slices,/*horizontal//longitude*/
   GLint      stacks)/*vertical//latitude*/
{
	gluSphere(qobj,radius,slices,stacks);
} 


void Draw24Sphere(void){
	GLfloat materialAmbient[4];
	GLfloat materilaDefuse[4];
	GLfloat materialSpecular[4];
	GLfloat materialShininess=0.0f;

	//Polygon Mode
	glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);

	//Emerald Sphere
		materialAmbient[0]=0.0215f;
		materialAmbient[1]=0.1745f;
		materialAmbient[2]=0.0215f;
		materialAmbient[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_AMBIENT,materialAmbient);

		materilaDefuse[0]=0.07568f;
		materilaDefuse[1]=0.61424f;
		materilaDefuse[2]=0.07568f;
		materilaDefuse[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_DIFFUSE,materilaDefuse);

		materialSpecular[0]=0.633f;
		materialSpecular[1]=0.727811f;
		materialSpecular[2]=0.633f;
		materialSpecular[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_SPECULAR,materialSpecular);

		materialShininess=0.6f * 128.0f;
		glMaterialf(GL_FRONT,GL_SHININESS,materialShininess);

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(-10.0f,6.0f,-20.0f);

		RenderGLUSphere(SphereQuadric[0],1.0f,30,30);// Normal vector Are calculated by GLU sphere itself we dont need to specify Nprmal vector.

	//Jade Sphere
		materialAmbient[0]=0.135f;
		materialAmbient[1]=0.2225f;
		materialAmbient[2]=0.1575f;
		materialAmbient[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_AMBIENT,materialAmbient);

		materilaDefuse[0]=0.54f;
		materilaDefuse[1]=0.89f;
		materilaDefuse[2]=0.63f;
		materilaDefuse[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_DIFFUSE,materilaDefuse);

		materialSpecular[0]=0.316228f;
		materialSpecular[1]=0.316228f;
		materialSpecular[2]=0.316228f;
		materialSpecular[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_SPECULAR,materialSpecular);

		materialShininess=0.1f * 128.0f;
		glMaterialf(GL_FRONT,GL_SHININESS,materialShininess);

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(-10.0f,3.5f,-20.0f);

		RenderGLUSphere(SphereQuadric[0],1.0f,30,30);// Normal vector Are calculated by GLU sphere itself we dont need to specify Nprmal vector.

	//Obsidian  Sphere
		materialAmbient[0]=0.05375f;
		materialAmbient[1]=0.05f;
		materialAmbient[2]=0.06625f;
		materialAmbient[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_AMBIENT,materialAmbient);

		materilaDefuse[0]=0.18275f;
		materilaDefuse[1]=0.17f;
		materilaDefuse[2]=0.22525f;
		materilaDefuse[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_DIFFUSE,materilaDefuse);

		materialSpecular[0]=0.332741f;
		materialSpecular[1]=0.328634f;
		materialSpecular[2]=0.346435f;
		materialSpecular[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_SPECULAR,materialSpecular);

		materialShininess=0.3f * 128.0f;
		glMaterialf(GL_FRONT,GL_SHININESS,materialShininess);

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(-10.0f,1.0f,-20.0f);

		RenderGLUSphere(SphereQuadric[0],1.0f,30,30);// Normal vector Are calculated by GLU sphere itself we dont need to specify Nprmal vector.

	
	//Pearl Sphere
		materialAmbient[0]=0.25f;
		materialAmbient[1]=0.20725f;
		materialAmbient[2]=0.20725f;
		materialAmbient[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_AMBIENT,materialAmbient);

		materilaDefuse[0]=1.0f;
		materilaDefuse[1]=0.829f;
		materilaDefuse[2]=0.829f;
		materilaDefuse[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_DIFFUSE,materilaDefuse);

		materialSpecular[0]=0.296648f;
		materialSpecular[1]=0.296648f;
		materialSpecular[2]=0.296648f;
		materialSpecular[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_SPECULAR,materialSpecular);

		materialShininess=0.088f * 128.0f;
		glMaterialf(GL_FRONT,GL_SHININESS,materialShininess);

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(-10.0f,-1.5f,-20.0f);

		RenderGLUSphere(SphereQuadric[0],1.0f,30,30);// Normal vector Are calculated by GLU sphere itself we dont need to specify Nprmal vector.

	//Ruby  Sphere
		materialAmbient[0]=0.1745f;
		materialAmbient[1]=0.01175f;
		materialAmbient[2]=0.01175f;
		materialAmbient[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_AMBIENT,materialAmbient);

		materilaDefuse[0]=0.61424f;
		materilaDefuse[1]=0.04136f;
		materilaDefuse[2]=0.04136f;
		materilaDefuse[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_DIFFUSE,materilaDefuse);

		materialSpecular[0]=0.727811f;
		materialSpecular[1]=0.626959f;
		materialSpecular[2]=0.626959f;
		materialSpecular[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_SPECULAR,materialSpecular);

		materialShininess=0.6f * 128.0f;
		glMaterialf(GL_FRONT,GL_SHININESS,materialShininess);

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(-10.0f,-4.0f,-20.0f);

		RenderGLUSphere(SphereQuadric[0],1.0f,30,30);// Normal vector Are calculated by GLU sphere itself we dont need to specify Nprmal vector.

	//Turquoise   Sphere	
		materialAmbient[0]=0.1f;
		materialAmbient[1]=0.18725f;
		materialAmbient[2]=0.1745f;
		materialAmbient[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_AMBIENT,materialAmbient);

		materilaDefuse[0]=0.396f;
		materilaDefuse[1]=0.74151f;
		materilaDefuse[2]=0.69102f;
		materilaDefuse[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_DIFFUSE,materilaDefuse);

		materialSpecular[0]=0.297254f;
		materialSpecular[1]=0.30829f;
		materialSpecular[2]=0.306678f;
		materialSpecular[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_SPECULAR,materialSpecular);

		materialShininess=0.1f * 128.0f;
		glMaterialf(GL_FRONT,GL_SHININESS,materialShininess);

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(-10.0f,-6.5f,-20.0f);

		RenderGLUSphere(SphereQuadric[0],1.0f,30,30);// Normal vector Are calculated by GLU sphere itself we dont need to specify Nprmal vector.

	//Brass Sphere	
		materialAmbient[0]=0.329412f;
		materialAmbient[1]=0.223529f;
		materialAmbient[2]=0.027451f;
		materialAmbient[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_AMBIENT,materialAmbient);

		materilaDefuse[0]=0.780392f;
		materilaDefuse[1]=0.568627f;
		materilaDefuse[2]=0.113725f;
		materilaDefuse[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_DIFFUSE,materilaDefuse);

		materialSpecular[0]=0.992157f;
		materialSpecular[1]=0.941176f;
		materialSpecular[2]=0.807843f;
		materialSpecular[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_SPECULAR,materialSpecular);

		materialShininess=0.21794872f * 128.0f;
		glMaterialf(GL_FRONT,GL_SHININESS,materialShininess);

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(-4.0f,6.0f,-20.0f);

		RenderGLUSphere(SphereQuadric[0],1.0f,30,30);// Normal vector Are calculated by GLU sphere itself we dont need to specify Nprmal vector.

	//Bronze  Sphere
		materialAmbient[0]=0.2125f;
		materialAmbient[1]=0.1275f;
		materialAmbient[2]=0.054f;
		materialAmbient[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_AMBIENT,materialAmbient);

		materilaDefuse[0]=0.714f;
		materilaDefuse[1]=0.4284f;
		materilaDefuse[2]=0.18144f;
		materilaDefuse[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_DIFFUSE,materilaDefuse);

		materialSpecular[0]=0.393548f;
		materialSpecular[1]=0.271906f;
		materialSpecular[2]=0.166721f;
		materialSpecular[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_SPECULAR,materialSpecular);

		materialShininess=0.2f * 128.0f;
		glMaterialf(GL_FRONT,GL_SHININESS,materialShininess);

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(-4.0f,3.5f,-20.0f);

		RenderGLUSphere(SphereQuadric[0],1.0f,30,30);// Normal vector Are calculated by GLU sphere itself we dont need to specify Nprmal vector.
	
	//Chrome  Sphere
		materialAmbient[0]=0.25f;
		materialAmbient[1]=0.25f;
		materialAmbient[2]=0.25f;
		materialAmbient[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_AMBIENT,materialAmbient);

		materilaDefuse[0]=0.4f;
		materilaDefuse[1]=0.4f;
		materilaDefuse[2]=0.4f;
		materilaDefuse[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_DIFFUSE,materilaDefuse);

		materialSpecular[0]=0.774597f;
		materialSpecular[1]=0.774597f;
		materialSpecular[2]=0.774597f;
		materialSpecular[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_SPECULAR,materialSpecular);

		materialShininess=0.6f * 128.0f;
		glMaterialf(GL_FRONT,GL_SHININESS,materialShininess);

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(-4.0f,1.0f,-20.0f);

		RenderGLUSphere(SphereQuadric[0],1.0f,30,30);// Normal vector Are calculated by GLU sphere itself we dont need to specify Nprmal vector.

	//Copper Sphere
		materialAmbient[0]=0.19125f;
		materialAmbient[1]=0.0735f;
		materialAmbient[2]=0.0225f;
		materialAmbient[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_AMBIENT,materialAmbient);

		materilaDefuse[0]=0.7038f;
		materilaDefuse[1]=0.27048f;
		materilaDefuse[2]=0.0828f;
		materilaDefuse[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_DIFFUSE,materilaDefuse);

		materialSpecular[0]=0.256777f;
		materialSpecular[1]=0.137622f;
		materialSpecular[2]=0.086014f;
		materialSpecular[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_SPECULAR,materialSpecular);

		materialShininess=0.1f * 128.0f;
		glMaterialf(GL_FRONT,GL_SHININESS,materialShininess);

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(-4.0f,-1.5f,-20.0f);

		RenderGLUSphere(SphereQuadric[0],1.0f,30,30);// Normal vector Are calculated by GLU sphere itself we dont need to specify Nprmal vector.

	//Gold Sphere
		materialAmbient[0]=0.24725f;
		materialAmbient[1]=0.1995f;
		materialAmbient[2]=0.0745f;
		materialAmbient[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_AMBIENT,materialAmbient);

		materilaDefuse[0]=0.75164f;
		materilaDefuse[1]=0.60648f;
		materilaDefuse[2]=0.22648f;
		materilaDefuse[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_DIFFUSE,materilaDefuse);

		materialSpecular[0]=0.628281f;
		materialSpecular[1]=0.555802f;
		materialSpecular[2]=0.366065f;
		materialSpecular[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_SPECULAR,materialSpecular);

		materialShininess=0.4f * 128.0f;
		glMaterialf(GL_FRONT,GL_SHININESS,materialShininess);

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(-4.0f,-4.0f,-20.0f);

		RenderGLUSphere(SphereQuadric[0],1.0f,30,30);// Normal vector Are calculated by GLU sphere itself we dont need to specify Nprmal vector.

	//Silver Sphere
		materialAmbient[0]=0.19225f;
		materialAmbient[1]=0.19225f;
		materialAmbient[2]=0.19225f;
		materialAmbient[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_AMBIENT,materialAmbient);

		materilaDefuse[0]=0.50754f;
		materilaDefuse[1]=0.50754f;
		materilaDefuse[2]=0.50754f;
		materilaDefuse[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_DIFFUSE,materilaDefuse);

		materialSpecular[0]=0.508273f;
		materialSpecular[1]=0.508273f;
		materialSpecular[2]=0.508273f;
		materialSpecular[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_SPECULAR,materialSpecular);

		materialShininess=0.4f * 128.0f;
		glMaterialf(GL_FRONT,GL_SHININESS,materialShininess);

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(-4.0f,-6.5f,-20.0f);

		RenderGLUSphere(SphereQuadric[0],1.0f,30,30);// Normal vector Are calculated by GLU sphere itself we dont need to specify Nprmal vector.

	//Black  Sphere
		materialAmbient[0]=0.0f;
		materialAmbient[1]=0.0f;
		materialAmbient[2]=0.0f;
		materialAmbient[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_AMBIENT,materialAmbient);

		materilaDefuse[0]=0.01f;
		materilaDefuse[1]=0.01f;
		materilaDefuse[2]=0.01f;
		materilaDefuse[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_DIFFUSE,materilaDefuse);

		materialSpecular[0]=0.50f;
		materialSpecular[1]=0.50f;
		materialSpecular[2]=0.50f;
		materialSpecular[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_SPECULAR,materialSpecular);

		materialShininess=0.25f * 128.0f;
		glMaterialf(GL_FRONT,GL_SHININESS,materialShininess);

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(2.0f,6.0f,-20.0f);

		RenderGLUSphere(SphereQuadric[0],1.0f,30,30);// Normal vector Are calculated by GLU sphere itself we dont need to specify Nprmal vector.

	//Cyan Sphere
		materialAmbient[0]=0.0f;
		materialAmbient[1]=0.1f;
		materialAmbient[2]=0.06f;
		materialAmbient[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_AMBIENT,materialAmbient);

		materilaDefuse[0]=0.0f;
		materilaDefuse[1]=0.50980392f;
		materilaDefuse[2]=0.50980392f;
		materilaDefuse[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_DIFFUSE,materilaDefuse);

		materialSpecular[0]=0.50196078f;
		materialSpecular[1]=0.50196078f;
		materialSpecular[2]=0.50196078f;
		materialSpecular[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_SPECULAR,materialSpecular);

		materialShininess=0.25f * 128.0f;
		glMaterialf(GL_FRONT,GL_SHININESS,materialShininess);

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(2.0f,3.5f,-20.0f);

		RenderGLUSphere(SphereQuadric[0],1.0f,30,30);// Normal vector Are calculated by GLU sphere itself we dont need to specify Nprmal vector.

	//Green Sphere
		materialAmbient[0]=0.0f;
		materialAmbient[1]=0.0f;
		materialAmbient[2]=0.0f;
		materialAmbient[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_AMBIENT,materialAmbient);

		materilaDefuse[0]=0.1f;
		materilaDefuse[1]=0.35f;
		materilaDefuse[2]=0.1f;
		materilaDefuse[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_DIFFUSE,materilaDefuse);

		materialSpecular[0]=0.45f;
		materialSpecular[1]=0.55f;
		materialSpecular[2]=0.45f;
		materialSpecular[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_SPECULAR,materialSpecular);

		materialShininess=0.25f * 128.0f;
		glMaterialf(GL_FRONT,GL_SHININESS,materialShininess);

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(2.0f,1.0f,-20.0f);

		RenderGLUSphere(SphereQuadric[0],1.0f,30,30);// Normal vector Are calculated by GLU sphere itself we dont need to specify Nprmal vector.

	//Red Sphere
		materialAmbient[0]=0.0f;
		materialAmbient[1]=0.0f;
		materialAmbient[2]=0.0f;
		materialAmbient[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_AMBIENT,materialAmbient);

		materilaDefuse[0]=0.5f;
		materilaDefuse[1]=0.0f;
		materilaDefuse[2]=0.0f;
		materilaDefuse[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_DIFFUSE,materilaDefuse);

		materialSpecular[0]=0.7f;
		materialSpecular[1]=0.6f;
		materialSpecular[2]=0.6f;
		materialSpecular[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_SPECULAR,materialSpecular);

		materialShininess=0.25f * 128.0f;
		glMaterialf(GL_FRONT,GL_SHININESS,materialShininess);

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(2.0f,-1.5f,-20.0f);

		RenderGLUSphere(SphereQuadric[0],1.0f,30,30);// Normal vector Are calculated by GLU sphere itself we dont need to specify Nprmal vector.

	//White Sphere
		materialAmbient[0]=0.0f;
		materialAmbient[1]=0.0f;
		materialAmbient[2]=0.0f;
		materialAmbient[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_AMBIENT,materialAmbient);

		materilaDefuse[0]=0.55f;
		materilaDefuse[1]=0.55f;
		materilaDefuse[2]=0.55f;
		materilaDefuse[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_DIFFUSE,materilaDefuse);

		materialSpecular[0]=0.70f;
		materialSpecular[1]=0.70f;
		materialSpecular[2]=0.70f;
		materialSpecular[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_SPECULAR,materialSpecular);

		materialShininess=0.25f * 128.0f;
		glMaterialf(GL_FRONT,GL_SHININESS,materialShininess);

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(2.0f,-4.0f,-20.0f);

		RenderGLUSphere(SphereQuadric[0],1.0f,30,30);// Normal vector Are calculated by GLU sphere itself we dont need to specify Nprmal vector.

	//Yellow Plastic Sphere
		materialAmbient[0]=0.0f;
		materialAmbient[1]=0.0f;
		materialAmbient[2]=0.0f;
		materialAmbient[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_AMBIENT,materialAmbient);

		materilaDefuse[0]=0.5f;
		materilaDefuse[1]=0.5f;
		materilaDefuse[2]=0.0f;
		materilaDefuse[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_DIFFUSE,materilaDefuse);

		materialSpecular[0]=0.60f;
		materialSpecular[1]=0.60f;
		materialSpecular[2]=0.50f;
		materialSpecular[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_SPECULAR,materialSpecular);

		materialShininess=0.25f * 128.0f;
		glMaterialf(GL_FRONT,GL_SHININESS,materialShininess);

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(2.0f,-6.5f,-20.0f);

		RenderGLUSphere(SphereQuadric[0],1.0f,30,30);// Normal vector Are calculated by GLU sphere itself we dont need to specify Nprmal vector.

	//Black Sphere
		materialAmbient[0]=0.02f;
		materialAmbient[1]=0.02f;
		materialAmbient[2]=0.02f;
		materialAmbient[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_AMBIENT,materialAmbient);

		materilaDefuse[0]=0.01f;
		materilaDefuse[1]=0.01f;
		materilaDefuse[2]=0.01f;
		materilaDefuse[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_DIFFUSE,materilaDefuse);

		materialSpecular[0]=0.4f;
		materialSpecular[1]=0.4f;
		materialSpecular[2]=0.4f;
		materialSpecular[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_SPECULAR,materialSpecular);

		materialShininess=0.078125f * 128.0f;
		glMaterialf(GL_FRONT,GL_SHININESS,materialShininess);

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(8.0f,6.0f,-20.0f);

		RenderGLUSphere(SphereQuadric[0],1.0f,30,30);// Normal vector Are calculated by GLU sphere itself we dont need to specify Nprmal vector.

	//Cyan Sphere
		materialAmbient[0]=0.0f;
		materialAmbient[1]=0.05f;
		materialAmbient[2]=0.05f;
		materialAmbient[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_AMBIENT,materialAmbient);

		materilaDefuse[0]=0.4f;
		materilaDefuse[1]=0.5f;
		materilaDefuse[2]=0.5f;
		materilaDefuse[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_DIFFUSE,materilaDefuse);

		materialSpecular[0]=0.04f;
		materialSpecular[1]=0.7f;
		materialSpecular[2]=0.7f;
		materialSpecular[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_SPECULAR,materialSpecular);

		materialShininess=0.078125f * 128.0f;
		glMaterialf(GL_FRONT,GL_SHININESS,materialShininess);

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(8.0f,3.5f,-20.0f);

		RenderGLUSphere(SphereQuadric[0],1.0f,30,30);// Normal vector Are calculated by GLU sphere itself we dont need to specify Nprmal vector.

	//Green Sphere
		materialAmbient[0]=0.0f;
		materialAmbient[1]=0.05f;
		materialAmbient[2]=0.0f;
		materialAmbient[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_AMBIENT,materialAmbient);

		materilaDefuse[0]=0.4f;
		materilaDefuse[1]=0.5f;
		materilaDefuse[2]=0.4f;
		materilaDefuse[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_DIFFUSE,materilaDefuse);

		materialSpecular[0]=0.04f;
		materialSpecular[1]=0.7f;
		materialSpecular[2]=0.047f;
		materialSpecular[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_SPECULAR,materialSpecular);

		materialShininess=0.078125f * 128.0f;
		glMaterialf(GL_FRONT,GL_SHININESS,materialShininess);

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(8.0f,1.0f,-20.0f);

		RenderGLUSphere(SphereQuadric[0],1.0f,30,30);// Normal vector Are calculated by GLU sphere itself we dont need to specify Nprmal vector.

	//Red Sphere
		materialAmbient[0]=0.05f;
		materialAmbient[1]=0.0f;
		materialAmbient[2]=0.0f;
		materialAmbient[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_AMBIENT,materialAmbient);

		materilaDefuse[0]=0.5f;
		materilaDefuse[1]=0.4f;
		materilaDefuse[2]=0.4f;
		materilaDefuse[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_DIFFUSE,materilaDefuse);

		materialSpecular[0]=0.7f;
		materialSpecular[1]=0.04f;
		materialSpecular[2]=0.04f;
		materialSpecular[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_SPECULAR,materialSpecular);

		materialShininess=0.078125f * 128.0f;
		glMaterialf(GL_FRONT,GL_SHININESS,materialShininess);

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(8.0f,-1.5f,-20.0f);

		RenderGLUSphere(SphereQuadric[0],1.0f,30,30);// Normal vector Are calculated by GLU sphere itself we dont need to specify Nprmal vector.

	//White Sphere
		materialAmbient[0]=0.05f;
		materialAmbient[1]=0.05f;
		materialAmbient[2]=0.05f;
		materialAmbient[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_AMBIENT,materialAmbient);

		materilaDefuse[0]=0.5f;
		materilaDefuse[1]=0.5f;
		materilaDefuse[2]=0.5f;
		materilaDefuse[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_DIFFUSE,materilaDefuse);

		materialSpecular[0]=0.7f;
		materialSpecular[1]=0.7f;
		materialSpecular[2]=0.7f;
		materialSpecular[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_SPECULAR,materialSpecular);

		materialShininess=0.078125f * 128.0f;
		glMaterialf(GL_FRONT,GL_SHININESS,materialShininess);

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(8.0f,-4.0f,-20.0f);

		RenderGLUSphere(SphereQuadric[0],1.0f,30,30);// Normal vector Are calculated by GLU sphere itself we dont need to specify Nprmal vector.
	
	//Yellow Sphere
		materialAmbient[0]=0.05f;
		materialAmbient[1]=0.05f;
		materialAmbient[2]=0.0f;
		materialAmbient[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_AMBIENT,materialAmbient);

		materilaDefuse[0]=0.5f;
		materilaDefuse[1]=0.5f;
		materilaDefuse[2]=0.4f;
		materilaDefuse[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_DIFFUSE,materilaDefuse);

		materialSpecular[0]=0.7f;
		materialSpecular[1]=0.7f;
		materialSpecular[2]=0.04f;
		materialSpecular[3]=1.0f;
		glMaterialfv(GL_FRONT,GL_SPECULAR,materialSpecular);

		materialShininess=0.078125f * 128.0f;
		glMaterialf(GL_FRONT,GL_SHININESS,materialShininess);

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(8.0f,-6.5f,-20.0f);

		RenderGLUSphere(SphereQuadric[0],1.0f,30,30);// Normal vector Are calculated by GLU sphere itself we dont need to specify Nprmal vector.


}














