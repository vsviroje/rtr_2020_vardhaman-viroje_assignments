#include<windows.h>
#include<stdio.h>
#include"ModelObjLoadingFFP.h"
#include<GL/GL.h>
#include<GL/GLU.h>

#define WIN_WIDTH 800
#define WIN_HEIGHT 600
#pragma comment(lib,"OpenGL32.lib")
#pragma comment(lib,"GLU32.lib")

//Yogeshwar sir's globals for Model Loading
	#define BUFFER_SIZE 			256
	#define S_EQUAL 				0

	#define NR_POINT_COORDS 		3
	#define NR_TEXTURE_COORDS 		2
	#define NR_NORMAL_COORDS 		3
	#define NR_FACE_TOKENS 			3
	#define NR_TRIANGLE_VERTICES 	3

	typedef struct vec_2d_int{
		GLint **pp_arr;
		size_t size;
	}vec_2d_int_t;

	typedef struct vec_3d_float{
		GLfloat **pp_arr;
		size_t size;
	}vec_2d_float_t;

	vec_2d_float_t *gp_vertices;
	vec_2d_float_t *gp_texture;
	vec_2d_float_t *gp_normals;

	vec_2d_int_t *gp_face_tri;
	vec_2d_int_t *gp_face_texture;
	vec_2d_int_t *gp_face_normals;

	FILE *g_fp_meshfile=NULL;
	char g_line[BUFFER_SIZE];


//

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

FILE* gpFile = NULL;
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
bool gbFullscreen = false;
bool gbActiveWindow = false;
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

bool gbLight=false;
bool gbTexture=false;
bool gbAnimate=false;


GLfloat Light0Ambient[]={0.0f,0.0f,0.0f,1.0f};
GLfloat Light0Defuse[]={1.0f,1.0f,1.0f,1.0f};//White Light
GLfloat Light0Specular[]={1.0f,1.0f,1.0f,1.0f};
GLfloat Light0Position[]={100.0f,100.0f,100.0f,1.0f};//Positional Light

GLfloat materialAmbient[]={0.0f,0.0f,0.0f,1.0f};
GLfloat materilaDefuse[]={1.0f,1.0f,1.0f,1.0f};
GLfloat materialSpecular[]={1.0f,1.0f,1.0f,1.0f};
GLfloat materialShininess=128.0f;

GLuint Stone_Texture;//texture variable
GLfloat RotateAngle=0.0f;

GLfloat gdepth = -10;
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	void Initialize(void);
	void Display(void);

	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("TeapotModelLoading");
	int X1, X2, Y1, Y2;
	bool bDone = false;

	if (fopen_s(&gpFile, "log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Failed to Create log file!"), TEXT("Error"), MB_OK | MB_ICONERROR);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "\nLog will be start from here...!");
	}

	X1 = GetSystemMetrics(SM_CXSCREEN) / 2;
	Y1 = GetSystemMetrics(SM_CYSCREEN) / 2;

	X2 = WIN_WIDTH / 2;
	Y2 = WIN_HEIGHT / 2;

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style =  CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(GRAY_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));

	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("OpenGL Teapot Model Loading"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		X1 - X2,//100,
		Y1 - Y2,//100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;
	Initialize();

	ShowWindow(hwnd, iCmdShow);

	SetForegroundWindow(hwnd);
	SetFocus(hwnd);
	

	fflush(gpFile);

	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
		
			if (gbActiveWindow == true)
			{
				Display();
			}
		}
	}

	return ((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void ToggleFullscreen(void);
	void Resize(int, int);
	void UnInitialize(void);

	switch (iMsg)
	{
	case WM_CREATE:
		fprintf(gpFile, "\nOpenGL Teapot Model Loading Log is Started!");
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
			case VK_ESCAPE:
				DestroyWindow(hwnd);
				break;
			case 0x46:
			case 0x66:
				ToggleFullscreen();
				break;
			case 'L':
			case 'l':
				if (gbLight==true){
					glDisable(GL_LIGHTING );
					gbLight=false;
				}else{
					glEnable(GL_LIGHTING);
					gbLight=true;
				}
				break;
			case 'T':
			case 't':
				if (gbTexture==true){
					glDisable(GL_TEXTURE_2D);
					gbTexture=false;
				}else{
					glEnable(GL_TEXTURE_2D);
					gbTexture=true;
				}
				break;
			case 'A':
			case 'a':
				if (gbAnimate==true){
					gbAnimate=false;
				}else{
					gbAnimate=true;
				}
				break;
			case 'W':
			case 'w':
				gdepth += 1;
				break;
			case 'S':
			case 's':
				gdepth -= 1;
				break;
			default:
				break;
		}
		break;
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;
	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;

	case WM_ERASEBKGND:
		return 0;
	case WM_SIZE:
		Resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;
	case WM_DESTROY:
		fprintf(gpFile, "\nOpenGL Teapot Model Loading Log is Ended!");
		UnInitialize();
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}
void ToggleFullscreen(void)
{
	MONITORINFO mi = { sizeof(MONITORINFO) };
	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW) 
		{
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				fprintf(gpFile, "\nFullscreen Enabled");
				SetWindowLong(ghwnd, GWL_STYLE, (dwStyle & ~WS_OVERLAPPEDWINDOW));
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
		gbFullscreen = true;
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, (dwStyle | WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		fprintf(gpFile, "\nNormal Window");
		ShowCursor(TRUE);
		gbFullscreen = false;
	}
}

void Initialize(void)
{
	void Resize(int, int);
	bool LoadGLTexture(GLuint *,TCHAR[]);
	void LoadMeshData(void);

	//Local variable initialization
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	ghdc = GetDC(ghwnd);

	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER ;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits= 32 ;


	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (iPixelFormatIndex == 0 )
	{
		fprintf(gpFile, "\nChoosePixelFormat() Failed to get pixel format index");
		DestroyWindow(ghwnd);
	}
	else
	{
		fprintf(gpFile, "\nChoosePixelFormat() Successfully selected pixel format and recived its index");
	}

	if ( SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE )
	{
		fprintf(gpFile, "\nSetPixelFormat() Failed to set pixel format for window");
		DestroyWindow(ghwnd);
	}
	else
	{
		fprintf(gpFile, "\nSetPixelFormat() Successfully able to set pixel format");
	}

	ghrc = wglCreateContext(ghdc);
	if ( ghrc == NULL)
	{
		fprintf(gpFile, "\nwglCreateContext() Failed to get the rendering context");
		DestroyWindow(ghwnd);
	}
	else
	{
		fprintf(gpFile, "\nwglCreateContext() Successfully able to get rendering context");
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE )
	{
		fprintf(gpFile, "\nwglMakeCurrent() Failed to switch current context from device to rendering context");
		DestroyWindow(ghwnd);
	}
	else
	{
		fprintf(gpFile, "\nwglMakeCurrent() Successfully able to switch current context from device to rendering context");
	}

	
	//SetColor

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	glShadeModel(GL_SMOOTH);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT,GL_NICEST);

	//Set Light 0
	glLightfv(GL_LIGHT0,GL_AMBIENT,Light0Ambient);
	glLightfv(GL_LIGHT0,GL_DIFFUSE,Light0Defuse);
	glLightfv(GL_LIGHT0,GL_POSITION,Light0Position);
	glLightfv(GL_LIGHT0,GL_SPECULAR,Light0Specular);


	//Set Material
	glMaterialfv(GL_FRONT,GL_AMBIENT,materialAmbient);
	glMaterialfv(GL_FRONT,GL_DIFFUSE,materilaDefuse);
	glMaterialfv(GL_FRONT,GL_SPECULAR,materialSpecular);
	glMaterialf(GL_FRONT,GL_SHININESS,materialShininess);

	glEnable(GL_LIGHT0);

	glBindTexture( GL_TEXTURE_2D,
					Stone_Texture
				);

	//---loading texture
	// glEnable(GL_TEXTURE_2D);
	LoadGLTexture(&Stone_Texture,MAKEINTRESOURCE(STONE_BITMAP));

	fprintf(gpFile, "LoadMeshData Before Call!\n");
	fflush(gpFile);
	
	//LoadMeshData
	LoadMeshData();

	fprintf(gpFile, "LoadMeshData returned!\n");
	fflush(gpFile);
	
	fprintf(gpFile, "\nResize started!");
	fflush(gpFile);
	//Warmup resize call
	Resize(WIN_WIDTH, WIN_HEIGHT);
		fprintf(gpFile, "\nResize ended!");
	fflush(gpFile);
}

void Resize(int width, int height)
{


	if (height == 0)
	{
		height = 1;
	}
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	glMatrixMode(GL_PROJECTION);	
	glLoadIdentity();

	gluPerspective(	45.0f,
					(GLfloat)width/(GLfloat)height,
					0.1f,
					100.0f
				);

}


void Display(void)
{
	
	fprintf(gpFile, "\nDisplay started!");
	fflush(gpFile);
	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
	
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(0.0f,0.0f, -5.0f);

	glRotatef(RotateAngle, 0.0f, 1.0f, 0.0f);
	
	//glFrontFace(GL_CCW);

	glPolygonMode(GL_FRONT_AND_BACK,GL_LINE);
	


	for(int i=0;i!=gp_face_tri->size;i++)
	{
		glBegin(GL_TRIANGLES);
			for(int j=0;j!=NR_TRIANGLE_VERTICES;j++)
			{
				int vi=gp_face_tri->pp_arr[i][j]-1;
				glVertex3f(gp_vertices->pp_arr[vi][0],gp_vertices->pp_arr[vi][1],gp_vertices->pp_arr[vi][2]);
				
				int vti = gp_face_texture->pp_arr[i][j] - 1;
				glTexCoord2f(gp_texture->pp_arr[vti][0], gp_texture->pp_arr[vti][1]);
				
				int vni = gp_face_normals->pp_arr[i][j] - 1;
				glNormal3f(gp_normals->pp_arr[vni][0], gp_normals->pp_arr[vni][1], gp_normals->pp_arr[vni][2]);
			}
		glEnd();
	}


	SwapBuffers(ghdc);

	if (gbAnimate==GL_TRUE){

		RotateAngle += 1.0f;
	}
	
}
void UnInitialize(void)
{
	void clean_vec_2d_int(vec_2d_int_t **);
	void clean_vec_2d_float(vec_2d_float_t **);

	if (gbFullscreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, (dwStyle | WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}
	
	if (wglGetCurrentContext() == ghrc ) {
		wglMakeCurrent(NULL, NULL);
	}
	else
	{
		fprintf(gpFile, "\nwglGetCurrentContext current context is not rendering context so no need to switch.");
	}

	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}
	else
	{
		fprintf(gpFile, "\nRendering Context is already uninitialized or never created/initialized.");
	}

	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}
	else
	{
		fprintf(gpFile, "\nDevice context is already uninitialized or never created/initialized.");
	}

	//Deleting Texture
	glDeleteTextures(1,&Stone_Texture);

	//Freeing MeshData
	clean_vec_2d_float(&gp_vertices);
	clean_vec_2d_float(&gp_texture);
	clean_vec_2d_float(&gp_normals);

	clean_vec_2d_int(&gp_face_tri);
	clean_vec_2d_int(&gp_face_texture);
	clean_vec_2d_int(&gp_face_normals);

	if (gpFile)
	{
		fprintf(gpFile, "\nLog will be End from here...!");
		fclose(gpFile);
		gpFile = NULL;
	}

}

bool LoadGLTexture(GLuint *texture,TCHAR resourceID[])
{
	bool bResult=false;
	HBITMAP hbitmap=NULL;
	BITMAP bmp;
	//code 
	hbitmap=(HBITMAP)LoadImage( GetModuleHandle(NULL),//get current HInstance of our process
								resourceID,//resorce id
								IMAGE_BITMAP,//image type
								0,//width
								0,//height
								LR_CREATEDIBSECTION//load resource as device independent bitmap
								);
	if(hbitmap!=NULL)
	{
		bResult=true;
		GetObject( hbitmap,//to extract information  from handle of object 
				sizeof(BITMAP),//Size of object where to store info
				&bmp//Address of object to store information
				);

		glPixelStorei( GL_UNPACK_ALIGNMENT,
						4
					);

		glGenTextures( 1,
					texture
					);

		glBindTexture( GL_TEXTURE_2D,
						*texture
					);

		//Setting Texture Parameter
		glTexParameteri( GL_TEXTURE_2D,
						GL_TEXTURE_MAG_FILTER,
						GL_LINEAR
						);
		glTexParameteri( GL_TEXTURE_2D,
						GL_TEXTURE_MIN_FILTER,
						GL_LINEAR_MIPMAP_LINEAR
					   );
		//following call will push texture data in graphic memory with helper graphic driver
		gluBuild2DMipmaps(GL_TEXTURE_2D,
						3,//RGB
						bmp.bmWidth,
						bmp.bmHeight,
						GL_BGR_EXT,
						GL_UNSIGNED_BYTE,
						bmp.bmBits
						);

		DeleteObject(hbitmap);
	}

	return bResult;
}


void LoadMeshData(void)
{
	void UnInitialize(void);

	vec_2d_int_t *create_vec_2d_int(void);
	vec_2d_float_t *create_vec_2d_float(void);

	void push_back_vec_2d_int(vec_2d_int_t *,int *);
	void push_back_vec_2d_float(vec_2d_float_t *,float *);

	void *xcalloc(int,size_t);
	fprintf(gpFile, "LoadMeshData started!\n");
	fflush(gpFile);
	const char* ObjFileName = "MonkeyHead.obj";
	const char* FileReadMode = "r";

	fopen_s(&g_fp_meshfile,ObjFileName, FileReadMode);
	if (!g_fp_meshfile)
	{
		UnInitialize();
	}

	gp_vertices=create_vec_2d_float();
	gp_texture=create_vec_2d_float();
	gp_normals=create_vec_2d_float();

	gp_face_tri=create_vec_2d_int();
	gp_face_texture=create_vec_2d_int();
	gp_face_normals=create_vec_2d_int();

	const char *sep_space=" ";
	const char *sep_fslash="/";
	char* next_token = NULL;

	char *first_token=NULL;
	char *token=NULL;

	char *face_tokens[NR_FACE_TOKENS];
	int nr_tokens;

	char *token_vertex_index=NULL;
	char *token_texture_index=NULL;
	char *token_normal_index=NULL;

	while(fgets(g_line,BUFFER_SIZE,g_fp_meshfile)!=NULL)
	{
		//fprintf(gpFile, "LoadMeshData :%s",g_line);
		//fflush(gpFile);

		first_token=strtok_s(g_line,sep_space,&next_token);

		if(strcmp(first_token,"v")==S_EQUAL)
		{
			GLfloat *pvec_point_coord=(GLfloat*)xcalloc(NR_POINT_COORDS,sizeof(GLfloat));

			for(int i=0;i!=NR_POINT_COORDS;i++)
			{
				pvec_point_coord[i]=(GLfloat)atof(strtok_s(NULL,sep_space,&next_token));
			}
			push_back_vec_2d_float(gp_vertices,pvec_point_coord);

		}
		else if(strcmp(first_token,"vt")==S_EQUAL)
		{
			GLfloat *pvec_texture_coord=(GLfloat*)xcalloc(NR_TEXTURE_COORDS,sizeof(GLfloat));
			
			for(int i=0;i!=NR_TEXTURE_COORDS;i++)
			{
				pvec_texture_coord[i]=(GLfloat)atof(strtok_s(NULL,sep_space,&next_token));
			}
			push_back_vec_2d_float(gp_texture,pvec_texture_coord);

		}
		else if(strcmp(first_token,"vn")==S_EQUAL)
		{
			GLfloat *pvec_normal_coord=(GLfloat*)xcalloc(NR_NORMAL_COORDS,sizeof(GLfloat));
			for(int i=0;i!=NR_NORMAL_COORDS;i++)
			{
				pvec_normal_coord[i]=(GLfloat)atof(strtok_s(NULL,sep_space,&next_token));
			}
			push_back_vec_2d_float(gp_normals,pvec_normal_coord);

		}
		else if(strcmp(first_token,"f")==S_EQUAL)
		{
			GLint *pvec_vertex_indices=(GLint*)xcalloc(3,sizeof(GLint));
			GLint *pvec_texture_indices=(GLint*)xcalloc(3,sizeof(GLint));
			GLint *pvec_normal_indices=(GLint*)xcalloc(3,sizeof(GLint));

			memset((void*)face_tokens,0,NR_FACE_TOKENS);
			nr_tokens=0;

			while(token=strtok_s(NULL,sep_space,&next_token))
			{
				if(strlen(token)<3)
					break;
				face_tokens[nr_tokens]=token;
				nr_tokens++;
			}

			for(int i=0;i<NR_FACE_TOKENS;i++)
			{
				token_vertex_index=strtok_s(face_tokens[i],sep_fslash,&next_token);
				token_texture_index=strtok_s(NULL,sep_fslash, &next_token);
				token_normal_index=strtok_s(NULL,sep_fslash, &next_token);
				
				pvec_vertex_indices[i]=atoi(token_vertex_index);
				pvec_texture_indices[i]=atoi(token_texture_index);
				pvec_normal_indices[i]=atoi(token_normal_index);

			}

			push_back_vec_2d_int(gp_face_tri,pvec_vertex_indices);
			push_back_vec_2d_int(gp_face_texture,pvec_texture_indices);
			push_back_vec_2d_int(gp_face_normals,pvec_normal_indices);
		}

		memset((void*)g_line,(int)'\0',BUFFER_SIZE);
	}

	fclose(g_fp_meshfile);
	g_fp_meshfile=NULL;

	fprintf(gpFile,"MeshFile->Vertex_Size:%d\nMeshFile->Texture_Size:%d\nMeshFile->Normal_Size:%d\nMeshFile->Faces_Size:%d\n",
	(int)gp_vertices->size,(int)gp_texture->size,(int)gp_normals->size,(int)gp_face_tri->size);
	fflush(gpFile);

}

vec_2d_int_t *create_vec_2d_int(void)
{
	void *xcalloc(int ,size_t);

	return (vec_2d_int_t*)xcalloc(1,sizeof(vec_2d_int_t));
}

vec_2d_float_t *create_vec_2d_float(void)
{
	void *xcalloc(int ,size_t);

	return (vec_2d_float_t*)xcalloc(1,sizeof(vec_2d_float_t));
}

void push_back_vec_2d_int(vec_2d_int_t *p_vec,GLint *p_arr)
{
	void *xrealloc(void *,size_t);

	p_vec->pp_arr=(GLint**)xrealloc(p_vec->pp_arr,(p_vec->size+1)*sizeof(GLint*));
	p_vec->size++;
	p_vec->pp_arr[p_vec->size-1]=p_arr;
}


void push_back_vec_2d_float(vec_2d_float_t *p_vec,GLfloat *p_arr)
{
	void *xrealloc(void *,size_t);

	p_vec->pp_arr=(GLfloat**)xrealloc(p_vec->pp_arr,(p_vec->size+1)*sizeof(GLfloat*));
	p_vec->size++;
	p_vec->pp_arr[p_vec->size-1]=p_arr;
}

void clean_vec_2d_int(vec_2d_int_t **pp_vec)
{
	vec_2d_int_t *p_vec=*pp_vec;
	for(size_t i=0;i!=p_vec->size;i++)
		free(p_vec->pp_arr[i]);
	free(p_vec);
	*pp_vec=NULL;
}


void clean_vec_2d_float(vec_2d_float_t **pp_vec)
{
	vec_2d_float_t *p_vec=*pp_vec;
	for(size_t i=0;i!=p_vec->size;i++)
		free(p_vec->pp_arr[i]);
	free(p_vec);
	*pp_vec=NULL;
}

void *xcalloc(int nr_elements,size_t size_per_element)
{
	void UnInitialize(void);

	void *p=calloc(nr_elements,size_per_element);
	if(!p)
	{
		fprintf(gpFile,"calloc:fatal:out of memory\n");
		UnInitialize();
	}
	return (p);
}

void *xrealloc(void *p,size_t new_size)
{
	void UnInitialize(void);

	void *ptr=realloc(p,new_size);
	if(!ptr)
	{
		fprintf(gpFile,"realloc:fatal:out of memory\n");
		UnInitialize();
	}
	return (ptr);
}












