#include<windows.h>
#include<stdio.h>
#include"Dynamic_India.h"
#include<GL/GL.h>
#include<GL/GLU.h>
#include<math.h>

#define WIN_WIDTH 800
#define WIN_HEIGHT 600
#pragma comment(lib,"OpenGL32.lib")
#pragma comment(lib,"GLU32.lib")
#pragma comment(lib,"winmm.lib")

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

FILE* gpFile = NULL;
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
bool gbFullscreen = false;
bool gbActiveWindow = false;
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

LARGE_INTEGER iSysFrequency;
LARGE_INTEGER iStartTime;
LARGE_INTEGER iCurrentTime;

GLboolean PlaneSceen1stPart=GL_FALSE;
GLboolean PlaneSceen2ndPart=GL_FALSE;

GLboolean FirstISceenPart=GL_FALSE;
GLboolean NSceenPart=GL_FALSE;
GLboolean DSceenPart=GL_FALSE;
GLboolean SecondISceenPart=GL_FALSE;
GLboolean ASceenPart=GL_FALSE;
GLboolean PlaneSceenPart=GL_FALSE;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	void Initialize(void);
	void Display(void);
	void Update(void);


	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("StaticIndia");
	int X1, X2, Y1, Y2;
	bool bDone = false;

	if (fopen_s(&gpFile, "VSV_OWT_log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Failed to Create log file!"), TEXT("Error"), MB_OK | MB_ICONERROR);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "\nLog will be start from here...!");
	}

	X1 = GetSystemMetrics(SM_CXSCREEN) / 2;
	Y1 = GetSystemMetrics(SM_CYSCREEN) / 2;

	X2 = WIN_WIDTH / 2;
	Y2 = WIN_HEIGHT / 2;

	if(!PlaySound(MAKEINTRESOURCE(DYNAMIC_INDIA_AUDIO),hInstance, SND_ASYNC | SND_RESOURCE))
	{
		fprintf(gpFile, "\nFailed to load Wav file to play music!");
		exit(0);
	}

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style =  CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(GRAY_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));

	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("Static India"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		X1 - X2,//100,
		Y1 - Y2,//100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;
	Initialize();

	ShowWindow(hwnd, iCmdShow);

	SetForegroundWindow(hwnd);
	SetFocus(hwnd);
	


	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
		
			if (gbActiveWindow == true)
			{
				QueryPerformanceCounter((LARGE_INTEGER*)&iCurrentTime);
				if ( ( ( ((float)iCurrentTime.QuadPart) - ((float)iStartTime.QuadPart) ) / (float)iSysFrequency.QuadPart ) >= 1.0f / 60.0f)
				{
					fprintf(gpFile, "\n%f-INside",((float)iCurrentTime.QuadPart) - ((float)iStartTime.QuadPart));
					if (((float)iCurrentTime.QuadPart) - ((float)iStartTime.QuadPart)>=67723264.000000){
							FirstISceenPart=GL_TRUE;
					}
				}
				// FirstISceenPart=GL_TRUE;
				Display();
				Update();
			}
		}
	}

	return ((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void ToggleFullscreen(void);
	void Resize(int, int);
	void UnInitialize(void);

	switch (iMsg)
	{
	case WM_CREATE:
		fprintf(gpFile, "\nOpenGL Window Triangle Log is Started!");
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
			case VK_ESCAPE:
				DestroyWindow(hwnd);
				break;
			case 0x46:
			case 0x66:
				ToggleFullscreen();
				break;
			case 0x50:
				fprintf(gpFile, "\n%f-Start number",((float)iCurrentTime.QuadPart) - ((float)iStartTime.QuadPart));
				break;
			default:
				break;
		}
		break;
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;
	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;

	case WM_ERASEBKGND:
		return 0;
	case WM_SIZE:
		Resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;
	case WM_DESTROY:
		fprintf(gpFile, "\nOpenGL Window Triangle Log is Ended!");
		UnInitialize();
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}
void ToggleFullscreen(void)
{
	MONITORINFO mi = { sizeof(MONITORINFO) };
	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW) 
		{
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				fprintf(gpFile, "\nFullscreen Enabled");
				SetWindowLong(ghwnd, GWL_STYLE, (dwStyle & ~WS_OVERLAPPEDWINDOW));
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
		gbFullscreen = true;
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, (dwStyle | WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		fprintf(gpFile, "\nNormal Window");
		ShowCursor(TRUE);
		gbFullscreen = false;
	}
}

void Initialize(void)
{
	void Resize(int, int);
	
	//Local variable initialization
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	ghdc = GetDC(ghwnd);

	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER ;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;


	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (iPixelFormatIndex == 0 )
	{
		fprintf(gpFile, "\nChoosePixelFormat() Failed to get pixel format index");
		DestroyWindow(ghwnd);
	}
	else
	{
		fprintf(gpFile, "\nChoosePixelFormat() Successfully selected pixel format and recived its index");
	}

	if ( SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE )
	{
		fprintf(gpFile, "\nSetPixelFormat() Failed to set pixel format for window");
		DestroyWindow(ghwnd);
	}
	else
	{
		fprintf(gpFile, "\nSetPixelFormat() Successfully able to set pixel format");
	}

	ghrc = wglCreateContext(ghdc);
	if ( ghrc == NULL)
	{
		fprintf(gpFile, "\nwglCreateContext() Failed to get the rendering context");
		DestroyWindow(ghwnd);
	}
	else
	{
		fprintf(gpFile, "\nwglCreateContext() Successfully able to get rendering context");
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE )
	{
		fprintf(gpFile, "\nwglMakeCurrent() Failed to switch current context from device to rendering context");
		DestroyWindow(ghwnd);
	}
	else
	{
		fprintf(gpFile, "\nwglMakeCurrent() Successfully able to switch current context from device to rendering context");
	}

	if (!QueryPerformanceFrequency((LARGE_INTEGER*)&iSysFrequency))
	{
		fprintf(gpFile, "\nQueryPerformanceFrequency Failed to get frequency of the performance counter");
		DestroyWindow(ghwnd);
	}

	QueryPerformanceCounter((LARGE_INTEGER*)&iStartTime);
	//SetColor

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	

	//Warmup resize call

	Resize(WIN_WIDTH, WIN_HEIGHT);
	

}

void Resize(int width, int height)
{
	if (height == 0)
	{
		height = 1;
	}
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	glMatrixMode(GL_PROJECTION);	
	glLoadIdentity();

	gluPerspective(	45.0f,
					(GLfloat)width/(GLfloat)height,
					0.1f,
					100.0f
				);

}

GLfloat First_I_x=-6.5f;
GLfloat N_y=4.0f;
GLfloat Second_I_y=-4.0f;
GLfloat A_x=6.5f;

GLfloat AngleArcLeftTop=180.0f;
GLfloat AngleArcLeftBottom=180.0f;
GLfloat AngleArcRightTop=270.0f;
GLfloat AngleArcRightBottom=90.0f;

GLfloat Plane_Middle_x=-8.0f;

GLfloat Green[]={0.0f,0.0f,0.0f};
GLfloat Orange[]={0.0f,0.0f,0.0f};

void Display(void)
{
	void Display_I(void);
	void Display_N(void);
	void Display_D(void);
	void Display_A(void);
	void Plane2(void);
	void ArcLeftTop(void);
	void ArcLeftBottom(void);
	void ArcRightTop(void);
	void ArcRightBottom(void);

	//code
	glClear(GL_COLOR_BUFFER_BIT);
	
	glMatrixMode(GL_MODELVIEW);

	if (FirstISceenPart){
		glLoadIdentity();
		glTranslatef(0.0f,0.0f,-10.0f);

			glTranslatef(First_I_x,0.0f,0.0f);
			Display_I();
	}

	if (NSceenPart){
		glLoadIdentity();
		glTranslatef(0.0f,0.0f,-10.0f);

			glTranslatef(-1.25f,N_y,0.0f);
			Display_N();
	}

	if (DSceenPart){
		glLoadIdentity();
		glTranslatef(0.0f,0.0f,-10.0f);

			glTranslatef(0.0f,0.0f,0.0f);
			Display_D();
	}

	if (SecondISceenPart){	
		glLoadIdentity();
		glTranslatef(0.0f,0.0f,-10.0f);

			glTranslatef(1.25f,Second_I_y,0.0f);
			Display_I();
	}

	if (ASceenPart){	
		glLoadIdentity();
		glTranslatef(0.0f,0.0f,-10.0f);

			glTranslatef(A_x,0.0f,0.0f);
			Display_A();
	}
	
	if(PlaneSceenPart)
	{
		if(PlaneSceen1stPart){
			glLoadIdentity();
			glTranslatef(0.0f,0.0f,-10.0f);
				glTranslatef(-4.0f,3.5f,0.0f);
				ArcLeftTop();

			glLoadIdentity();
			glTranslatef(0.0f,0.0f,-10.0f);
				glTranslatef(-4.0f,-3.5f,0.0f);
				ArcLeftBottom();
		}

		if(PlaneSceen2ndPart){
			glLoadIdentity();
			glTranslatef(0.0f,0.0f,-10.0f);
				glTranslatef(4.0f,-3.5f,0.0f);
				ArcRightBottom();

			glLoadIdentity();
			glTranslatef(0.0f,0.0f,-10.0f);
				glTranslatef(4.0f,3.5f,0.0f);
				ArcRightTop();
		}
			
		glLoadIdentity();
		glTranslatef(0.0f,0.0f,-10.0f);
			glTranslatef(Plane_Middle_x,0.0f,0.0f);
			Plane2();
	}

	SwapBuffers(ghdc);
	
}

void Update(void){
	if (FirstISceenPart){
		if (First_I_x <= -2.5f)
		{
			First_I_x += 0.01f;
		}else{
			NSceenPart=GL_TRUE;
		}
	}	
	if(NSceenPart){
		if (N_y >=0.0f)
		{
			N_y -= 0.01f;
		}else{
			DSceenPart=GL_TRUE;
		}
	}

	if (SecondISceenPart){
		if (Second_I_y<=0.0f){
			Second_I_y+=0.01;
		}else{
			ASceenPart=GL_TRUE;
		}
	}

	if(ASceenPart)
	{
		if (A_x >= 2.5f)
		{
			A_x -= 0.01f;
		}else{
			PlaneSceenPart=GL_TRUE;
		}
	}

	//GREEN-0.0f, 1.0f, 0.0f
	//Orange-1.0f, 0.5f, 0.0f
	if(DSceenPart){
		if (Orange[0] <= 1.0f)
		{
			Orange[0]+=0.002;
		}
		if (Orange[1] <= 0.5f)
		{
			Orange[1]+=0.002;
		}
		if (Green[1] <= 1.0f)
		{
			Green[1] +=0.002;
		}else{
			SecondISceenPart=GL_TRUE;
		}
	}
	if(PlaneSceenPart)
	{
		if (AngleArcLeftTop<=270.0f)
		{
			AngleArcLeftTop+=0.1f;
		}

		if (AngleArcLeftBottom>=90.0f)
		{
			AngleArcLeftBottom-=0.1f;
		}

		if(Plane_Middle_x >= 4.0f) {
			
			PlaneSceen2ndPart=GL_TRUE;

			if (AngleArcRightTop<=360.0f)
			{
				AngleArcRightTop+=0.1f;
			}

			if (AngleArcRightBottom>=0.0f)
			{
				AngleArcRightBottom-=0.1f;
			}
		}
		
		if(Plane_Middle_x >= -4.0f) {
			PlaneSceen1stPart=GL_FALSE;	
		}else{
			PlaneSceen1stPart=GL_TRUE;
		}

		if (Plane_Middle_x<=8.0f)
		{
			Plane_Middle_x+=0.00446f;
		}
	}
}

void UnInitialize(void)
{
	if (gbFullscreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, (dwStyle | WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}
	
	if (wglGetCurrentContext() == ghrc ) {
		wglMakeCurrent(NULL, NULL);
	}
	else
	{
		fprintf(gpFile, "\nwglGetCurrentContext current context is not rendering context so no need to switch.");
	}

	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}
	else
	{
		fprintf(gpFile, "\nRendering Context is already uninitialized or never created/initialized.");
	}

	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}
	else
	{
		fprintf(gpFile, "\nDevice context is already uninitialized or never created/initialized.");
	}


	if (gpFile)
	{
		fprintf(gpFile, "\nLog will be End from here...!");
		fclose(gpFile);
		gpFile = NULL;
	}
}



void Display_I(void)
{
	glBegin(GL_QUADS);

		glColor3f(1.0f, 0.5f, 0.0f);
			glVertex3f(0.5f, 0.5f, 0.0f);
			glVertex3f(-0.5f, 0.5f, 0.0f);
			glVertex3f(-0.5f, 0.3f, 0.0f);
			glVertex3f(0.5f, 0.3f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

		glColor3f(1.0f, 0.5f, 0.0f);
			glVertex3f(0.1f, 0.3f, 0.0f);
			glVertex3f(-0.1f, 0.3f, 0.0f);
		glColor3f(0.0f, 1.0f, 0.0f);
			glVertex3f(-0.1f, -0.3f, 0.0f);
			glVertex3f(0.1f, -0.3f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

		glColor3f(0.0f, 1.0f, 0.0f);
			glVertex3f(0.5f, -0.3f, 0.0f);
			glVertex3f(-0.5f, -0.3f, 0.0f);
			glVertex3f(-0.5f, -0.5f, 0.0f);
			glVertex3f(0.5f, -0.5f, 0.0f);

	glEnd();
}

void Display_N(void)
{
	glBegin(GL_QUADS);

		glColor3f(1.0f, 0.5f, 0.0f);
			glVertex3f(-0.3f, 0.5f, 0.0f);
			glVertex3f(-0.5f, 0.5f, 0.0f);
		glColor3f(0.0f, 1.0f, 0.0f);		
			glVertex3f(-0.5f, -0.5f, 0.0f);
			glVertex3f(-0.3f, -0.5f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

		glColor3f(1.0f, 0.5f, 0.0f);
			glVertex3f(0.5f, 0.5f, 0.0f);
			glVertex3f(0.3f, 0.5f, 0.0f);
		glColor3f(0.0f, 1.0f, 0.0f);		
			glVertex3f(0.3f, -0.5f, 0.0f);
			glVertex3f(0.5f, -0.5f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

		glColor3f(0.0f, 1.0f, 0.0f);		
			glVertex3f(0.3f, -0.3f, 0.0f);
		glColor3f(1.0f, 0.5f, 0.0f);
			glVertex3f(-0.3f, 0.5f, 0.0f);
			glVertex3f(-0.3f, 0.3f, 0.0f);
		glColor3f(0.0f, 1.0f, 0.0f);		
			glVertex3f(0.3f, -0.5f, 0.0f);

	glEnd();
}

void Display_D(void)
{
	glBegin(GL_QUADS);

		glColor3fv(Orange);
			glVertex3f(-0.3f, 0.5f, 0.0f);
			glVertex3f(-0.5f, 0.5f, 0.0f);
		glColor3fv(Green);		
			glVertex3f(-0.5f, -0.5f, 0.0f);
			glVertex3f(-0.3f, -0.5f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

		glColor3fv(Orange);
			glVertex3f(0.2, 0.5f, 0.0f);
			glVertex3f(-0.3f, 0.5f, 0.0f);
			glVertex3f(-0.3f, 0.3f, 0.0f);
			glVertex3f(0.1f, 0.3f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

		glColor3fv(Green);		
			glVertex3f(0.1f, -0.3f, 0.0f);
			glVertex3f(-0.3f, -0.3f, 0.0f);
			glVertex3f(-0.3f, -0.5f, 0.0f);
			glVertex3f(0.2f, -0.5f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

		glColor3fv(Orange);
			glVertex3f(0.4f, 0.3f, 0.0f);
			glVertex3f(0.2f, 0.5f, 0.0f);
			glVertex3f(0.1f, 0.3f, 0.0f);
			glVertex3f(0.3f, 0.1f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

		glColor3fv(Green);		
			glVertex3f(0.4f, -0.3f, 0.0f);
			glVertex3f(0.3f, -0.1f, 0.0f);
			glVertex3f(0.1f, -0.3f, 0.0f);
			glVertex3f(0.2f, -0.5f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

		glColor3fv(Green);		
			glVertex3f(0.4f, -0.3f, 0.0f);
		glColor3fv(Orange);
			glVertex3f(0.4f, 0.3f, 0.0f);
			glVertex3f(0.3f, 0.1f, 0.0f);
		glColor3fv(Green);		
			glVertex3f(0.3f, -0.1f, 0.0f);

	glEnd();
}

void Display_A(void)
{
	glBegin(GL_QUADS);

		glColor3f(0.0f, 1.0f, 0.0f);		
			glVertex3f(0.5f, -0.5f, 0.0f);
		glColor3f(1.0f, 0.5f, 0.0f);
			glVertex3f(0.0f, 0.5f, 0.0f);
			glVertex3f(0.0f, 0.3f, 0.0f);
		glColor3f(0.0f, 1.0f, 0.0f);		
			glVertex3f(0.4f, -0.5f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

		glColor3f(0.0f, 1.0f, 0.0f);		
			glVertex3f(-0.4f, -0.5f, 0.0f);
		glColor3f(1.0f, 0.5f, 0.0f);
			glVertex3f(0.0f, 0.3f, 0.0f);
			glVertex3f(0.0f, 0.5f, 0.0f);
		glColor3f(0.0f, 1.0f, 0.0f);		
			glVertex3f(-0.5f, -0.5f, 0.0f);

	glEnd();
	//Flag
	if(Plane_Middle_x >= 2.7f) {
		glBegin(GL_QUADS);

			glColor3f(1.0f, 0.5f, 0.0f);
				glVertex3f(0.19f, -0.07f, 0.0f);
				glVertex3f(0.15f, 0.0f, 0.0f);
				glVertex3f(-0.15f, 0.0f, 0.0f);
				glVertex3f(-0.19f, -0.07f, 0.0f);

		glEnd();

		glBegin(GL_QUADS);

			glColor3f(1.0f, 1.0f, 1.0f);
				glVertex3f(0.22f, -0.13f, 0.0f);
				glVertex3f(0.19f, -0.07f, 0.0f);
				glVertex3f(-0.19f, -0.07f, 0.0f);
				glVertex3f(-0.22f, -0.13f, 0.0f);
		glEnd();

		glBegin(GL_QUADS);

			glColor3f(0.0f, 1.0f, 0.0f);		
				glVertex3f(0.22f, -0.13f, 0.0f);
				glVertex3f(-0.22f, -0.13f, 0.0f);
				glVertex3f(-0.25f, -0.2f, 0.0f);
				glVertex3f(0.25f, -0.2f, 0.0f);

		glEnd();
	}
	
}


void ArcLeftTop(void)
{
	GLfloat x,y,z,r;
	GLdouble pi;
	void Plane2(void);
	pi=3.14159265358;
	r=3.5f;
	
	z=0.0f;
	
	x=r* cos((AngleArcLeftTop/180)*pi);
	y=r* sin((AngleArcLeftTop/180)*pi);

	glTranslatef(x,y,z);
		Plane2();
	
}


void ArcLeftBottom(void)
{
	GLfloat x,y,z,r;
	GLdouble pi;
	void Plane2(void);
	pi=3.14159265358;
	r=3.5f;
	
	z=0.0f;
	
	
	x=r* cos((AngleArcLeftBottom/180)*pi);
	y=r* sin((AngleArcLeftBottom/180)*pi);

	glTranslatef(x,y,z);
		Plane2();
	
}


void ArcRightTop(void)
{
	GLfloat x,y,z,r;
	GLdouble pi;
	void Plane2(void);
	pi=3.14159265358;
	r=3.5f;
	
	z=0.0f;

	
	x=r* cos((AngleArcRightTop/180)*pi);
	y=r* sin((AngleArcRightTop/180)*pi);

	glTranslatef(x,y,z);
		Plane2();
	
}


void ArcRightBottom(void)
{
	GLfloat x,y,z,r;
	GLdouble pi;
	void Plane2(void);
	pi=3.14159265358;
	r=3.5f;
	
	z=0.0f;
	
	
	x=r* cos((AngleArcRightBottom/180)*pi);
	y=r* sin((AngleArcRightBottom/180)*pi);

	glTranslatef(x,y,z);
		Plane2();
	
}

void Plane2(void)
{
	glBegin(GL_TRIANGLES);
		glColor3f(0.0f, 0.0f, 1.0f);
			glVertex3f(0.9f, -0.1f, 0.0f);
			glVertex3f(0.7f, 0.0f, 0.0f);
			glVertex3f(0.7f, -0.2f, 0.0f);
	glEnd();
	glBegin(GL_QUADS);
		glColor3f(0.0f, 0.0f, 1.0f);
			glVertex3f(0.7f, 0.0f, 0.0f);
			glVertex3f(0.4f, 0.1f, 0.0f);
			glVertex3f(0.4f, -0.3f, 0.0f);
			glVertex3f(0.7f, -0.2f, 0.0f);
	glEnd();
	glBegin(GL_POLYGON);
		glColor3f(0.0f, 0.0f, 1.0f);
			glVertex3f(0.4f, -0.3f, 0.0f);
			glVertex3f(0.4f, 0.1f, 0.0f);
			glVertex3f(0.1f, 0.3f, 0.0f);
			glVertex3f(0.15f, -0.05f, 0.0f);
			glVertex3f(0.15f, -0.15f, 0.0f);
			glVertex3f(0.1f, -0.5f, 0.0f);
	glEnd();
	glBegin(GL_QUADS);
		glColor3f(0.0f, 0.0f, 1.0f);
			glVertex3f(0.15f,-0.05f, 0.0f);
			glVertex3f(0.1f, -0.05f, 0.0f);
			glVertex3f(0.1f, -0.15f, 0.0f);
			glVertex3f(0.15f, -0.15f, 0.0f);
	glEnd();

	glBegin(GL_QUADS);
		glColor3f(0.0f, 0.0f, 1.0f);
			glVertex3f(0.1f,-0.2f, 0.0f);
			glVertex3f(0.1f, 0.0f, 0.0f);
			glVertex3f(0.0f, 0.1f, 0.0f);
			glVertex3f(0.0f, -0.3f, 0.0f);
	glEnd();
	//Flag Start
	glBegin(GL_QUADS);
	//Orange
		glColor3f(1.0f, 0.5f, 0.0f);
			glVertex3f(-0.05f,-0.07f, 0.0f);
			glVertex3f(-0.05f, 0.0f, 0.0f);
			glVertex3f(-0.4f, -0.0f, 0.0f);
			glVertex3f(-0.4f, -0.07f, 0.0f);
	glEnd();
	glBegin(GL_QUADS);
	//White
		glColor3f(1.0f, 1.0f, 1.0f);
			glVertex3f(-0.05f,-0.13f, 0.0f);
			glVertex3f(-0.05f, -0.07f, 0.0f);
			glVertex3f(-0.4f, -0.07f, 0.0f);
			glVertex3f(-0.4f, -0.13f, 0.0f);
	glEnd();
	glBegin(GL_QUADS);
	//Green
		glColor3f(0.0f, 1.0f, 0.0f);
			glVertex3f(-0.05f,-0.2f, 0.0f);
			glVertex3f(-0.05f, -0.13f, 0.0f);
			glVertex3f(-0.4f, -0.13f, 0.0f);
			glVertex3f(-0.4f, -0.2f, 0.0f);
	glEnd();
	//Flag end
	glBegin(GL_LINES);
	glColor3f(1.0f, 1.0f, 1.0f);
    	glVertex3f(0.0f,-0.05f, 0.0f);
    	glVertex3f(-0.05f,-0.05f, 0.0f);
	glEnd();
	glBegin(GL_LINES);
	glColor3f(1.0f, 1.0f, 1.0f);
    	glVertex3f(0.0f,-0.15f, 0.0f);
    	glVertex3f(-0.05f,-0.15f, 0.0f);
	glEnd();

	//IAF START
	glBegin(GL_LINES);
	glColor3f(1.0f, 1.0f, 1.0f);
    	glVertex3f(0.35f,-0.05f, 0.0f);
    	glVertex3f(0.25f,-0.05f, 0.0f);

		glVertex3f(0.3f,-0.05f, 0.0f);
    	glVertex3f(0.3f,-0.15f, 0.0f);

		glVertex3f(0.35f,-0.15f, 0.0f);
    	glVertex3f(0.25f,-0.15f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glColor3f(1.0f, 1.0f, 1.0f);
    	glVertex3f(0.5f,-0.15f, 0.0f);
    	glVertex3f(0.45f,-0.05f, 0.0f);

		glVertex3f(0.4f,-0.15f, 0.0f);
    	glVertex3f(0.45f,-0.05f, 0.0f);

		glVertex3f(0.47f,-0.1f, 0.0f);
    	glVertex3f(0.43f,-0.1f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glColor3f(1.0f, 1.0f, 1.0f);
    	glVertex3f(0.65f,-0.05f, 0.0f);
    	glVertex3f(0.55f,-0.05f, 0.0f);

		glVertex3f(0.6f,-0.1f, 0.0f);
    	glVertex3f(0.55f,-0.1f, 0.0f);

		glVertex3f(0.55f,-0.15f, 0.0f);
    	glVertex3f(0.55f,-0.05f, 0.0f);
	glEnd();

	//End
}











