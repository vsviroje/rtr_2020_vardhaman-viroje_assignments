#include<windows.h>
#include<stdio.h>
#include"kundali.h"
#include<GL/GL.h>
#include<math.h>
#include<GL/GLU.h>

#define WIN_WIDTH 800
#define WIN_HEIGHT 800
#pragma comment(lib,"OpenGL32.lib")
#pragma comment(lib,"GLU32.lib")

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

FILE* gpFile = NULL;
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
bool gbFullscreen = false;
bool gbActiveWindow = false;
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	void Initialize(void);
	void Display(void);

	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("OpenGLkundali");
	int X1, X2, Y1, Y2;
	bool bDone = false;

	if (fopen_s(&gpFile, "VSV_OWT_log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Failed to Create log file!"), TEXT("Error"), MB_OK | MB_ICONERROR);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "\nLog will be start from here...!");
	}

	X1 = GetSystemMetrics(SM_CXSCREEN) / 2;
	Y1 = GetSystemMetrics(SM_CYSCREEN) / 2;

	X2 = WIN_WIDTH / 2;
	Y2 = WIN_HEIGHT / 2;

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style =  CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(GRAY_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));

	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("OpenGL kundali"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		X1 - X2,//100,
		Y1 - Y2,//100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;
	Initialize();

	ShowWindow(hwnd, iCmdShow);

	SetForegroundWindow(hwnd);
	SetFocus(hwnd);
	


	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
		
			if (gbActiveWindow == true)
			{
				Display();
			}
		}
	}

	return ((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void ToggleFullscreen(void);
	void Resize(int, int);
	void UnInitialize(void);

	switch (iMsg)
	{
	case WM_CREATE:
		fprintf(gpFile, "\nOpenGL Window Triangle Log is Started!");
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
			case VK_ESCAPE:
				DestroyWindow(hwnd);
				break;
			case 0x46:
			case 0x66:
				ToggleFullscreen();
				break;
			default:
				break;
		}
		break;
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;
	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;

	case WM_ERASEBKGND:
		return 0;
	case WM_SIZE:
		Resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;
	case WM_DESTROY:
		fprintf(gpFile, "\nOpenGL Window Triangle Log is Ended!");
		UnInitialize();
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}
void ToggleFullscreen(void)
{
	MONITORINFO mi = { sizeof(MONITORINFO) };
	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW) 
		{
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				fprintf(gpFile, "\nFullscreen Enabled");
				SetWindowLong(ghwnd, GWL_STYLE, (dwStyle & ~WS_OVERLAPPEDWINDOW));
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
		gbFullscreen = true;
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, (dwStyle | WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		fprintf(gpFile, "\nNormal Window");
		ShowCursor(TRUE);
		gbFullscreen = false;
	}
}

void Initialize(void)
{
	void Resize(int, int);
	
	//Local variable initialization
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	ghdc = GetDC(ghwnd);

	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER ;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;


	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (iPixelFormatIndex == 0 )
	{
		fprintf(gpFile, "\nChoosePixelFormat() Failed to get pixel format index");
		DestroyWindow(ghwnd);
	}
	else
	{
		fprintf(gpFile, "\nChoosePixelFormat() Successfully selected pixel format and recived its index");
	}

	if ( SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE )
	{
		fprintf(gpFile, "\nSetPixelFormat() Failed to set pixel format for window");
		DestroyWindow(ghwnd);
	}
	else
	{
		fprintf(gpFile, "\nSetPixelFormat() Successfully able to set pixel format");
	}

	ghrc = wglCreateContext(ghdc);
	if ( ghrc == NULL)
	{
		fprintf(gpFile, "\nwglCreateContext() Failed to get the rendering context");
		DestroyWindow(ghwnd);
	}
	else
	{
		fprintf(gpFile, "\nwglCreateContext() Successfully able to get rendering context");
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE )
	{
		fprintf(gpFile, "\nwglMakeCurrent() Failed to switch current context from device to rendering context");
		DestroyWindow(ghwnd);
	}
	else
	{
		fprintf(gpFile, "\nwglMakeCurrent() Successfully able to switch current context from device to rendering context");
	}

	
	//SetColor

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	//Warmup resize call

	Resize(WIN_WIDTH, WIN_HEIGHT);
	

}

void Resize(int width, int height)
{
	if (height == 0)
	{
		height = 1;
	}
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(	45.0f,
					(GLfloat)width/(GLfloat)height,
					0.0f,
					100.0f
				);


}
void Display(void)
{
	//code

	glClear(GL_COLOR_BUFFER_BIT);
	
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(0.0f,0.0f,-3.0f);

	glBegin(GL_LINES);

		glColor3f(1.0f, 1.0f, 1.0f);

			glVertex3f(-0.5f,-0.5f, 0.0f);
			glVertex3f(0.5f,-0.5f, 0.0f);

			glVertex3f(0.5f,-0.5f, 0.0f);
			glVertex3f(0.5f,0.5f, 0.0f);

			glVertex3f(0.5f,0.5f, 0.0f);
			glVertex3f(-0.5f,0.5f, 0.0f);

			glVertex3f(-0.5f,0.5f, 0.0f);
			glVertex3f(-0.5f,-0.5f, 0.0f);

	glEnd();

	glBegin(GL_LINES);

		glColor3f(1.0f, 1.0f, 1.0f);
		
			glVertex3f(0.0f,-0.5f, 0.0f);
			glVertex3f(0.5f,0.0f, 0.0f);

			glVertex3f(0.5f,0.0f, 0.0f);
			glVertex3f(0.0f,0.5f, 0.0f);

			glVertex3f(0.0f,0.5f, 0.0f);
			glVertex3f(-0.5f,0.0f, 0.0f);

			glVertex3f(-0.5f,0.0f, 0.0f);
			glVertex3f(0.0f,-0.5f, 0.0f);

	glEnd();
	
	glBegin(GL_LINES);

		glColor3f(1.0f, 1.0f, 1.0f);
		
			glVertex3f(-0.5f,-0.5f, 0.0f);
			glVertex3f(0.5f,0.5f, 0.0f);

			glVertex3f(0.5f,-0.5f, 0.0f);
			glVertex3f(-0.5f,0.5f, 0.0f);

	glEnd();
	
	SwapBuffers(ghdc);
	
	
}
void UnInitialize(void)
{
	if (gbFullscreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, (dwStyle | WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}
	
	if (wglGetCurrentContext() == ghrc ) {
		wglMakeCurrent(NULL, NULL);
	}
	else
	{
		fprintf(gpFile, "\nwglGetCurrentContext current context is not rendering context so no need to switch.");
	}

	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}
	else
	{
		fprintf(gpFile, "\nRendering Context is already uninitialized or never created/initialized.");
	}

	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}
	else
	{
		fprintf(gpFile, "\nDevice context is already uninitialized or never created/initialized.");
	}


	if (gpFile)
	{
		fprintf(gpFile, "\nLog will be End from here...!");
		fclose(gpFile);
		gpFile = NULL;
	}
}
