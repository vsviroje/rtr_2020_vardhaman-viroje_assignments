#include<windows.h>
#include<stdio.h>
#include "MatrixMultiplication.h"
#include<GL/GL.h>
#include<GL/GLU.h>

#define _USE_MATH_DEFINES
#include<math.h>

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

#pragma comment(lib,"OpenGL32.lib")
#pragma comment(lib,"GLU32.lib")
#pragma comment(lib,"user32.lib")
#pragma comment(lib,"gdi32.lib")

LRESULT CALLBACK WndProc(HWND,UINT,WPARAM,LPARAM);

FILE* gpFile=NULL;
DWORD dwStyle;
WINDOWPLACEMENT wpPrev={sizeof(WINDOWPLACEMENT)};
bool gbFullscreen=false;
bool gbActiveWindow=false;
HWND ghwnd=NULL;
HDC ghdc=NULL;
HGLRC ghrc=NULL;

GLfloat IdentityMatrix[16];
GLfloat TranslationMatrix[16];
GLfloat ScaleMatrix[16];
GLfloat RotationMatrix_X[16];
GLfloat RotationMatrix_Y[16];
GLfloat RotationMatrix_Z[16];


int WINAPI WinMain(HINSTANCE hInstance,HINSTANCE hPrevInstance,LPSTR lpszCmdLine,int iCmdShow)
{
    void Initialize(void);
    void Display(void);

    WNDCLASSEX wndclass;
    HWND hwnd;
    MSG msg;
    TCHAR szAppName[]=TEXT("MatrixMultiplication");
    int X1,X2,Y1,Y2;
    bool bDone=false;

    if(fopen_s(&gpFile,"Log.txt","w")!=0)
    {
        MessageBox(NULL,TEXT("Failed to create log file!"),TEXT("ERROR"),MB_OK|MB_ICONERROR);
        exit(0);
    }else{
        fprintf(gpFile,"\nLog Start..!");
    }

    X1=GetSystemMetrics(SM_CXSCREEN)/2;
    Y1=GetSystemMetrics(SM_CYSCREEN)/2;

    X2=WIN_WIDTH/2;
    Y2=WIN_HEIGHT/2;

    wndclass.cbSize=sizeof(WNDCLASSEX);
    wndclass.style=CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
    wndclass.cbClsExtra=0;
    wndclass.cbWndExtra=0;
    wndclass.lpfnWndProc=WndProc;
    wndclass.hInstance=hInstance;
    wndclass.hIcon=LoadIcon(hInstance,MAKEINTRESOURCE(MYICON));
    wndclass.hCursor=LoadCursor(NULL,IDC_ARROW);
    wndclass.hbrBackground=(HBRUSH)GetStockObject(GRAY_BRUSH);
    wndclass.lpszClassName=szAppName;
    wndclass.lpszMenuName=NULL;
    wndclass.hIconSm=LoadIcon(hInstance,MAKEINTRESOURCE(MYICON));

    RegisterClassEx(&wndclass);

    hwnd=CreateWindowEx(WS_EX_APPWINDOW,
        szAppName,
        TEXT("3D Cube using Matrix Multiplication"),
        WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
        X1-X2,
        Y1-Y2,
        WIN_WIDTH,
        WIN_HEIGHT,
        NULL,
        NULL,
        hInstance,
        NULL);

    ghwnd=hwnd;
    Initialize();

    ShowWindow(hwnd,iCmdShow);

    SetForegroundWindow(hwnd);
    SetFocus(hwnd);

    while(bDone==false)
    {
        
        if(PeekMessage(&msg,NULL,0,0,PM_REMOVE))
        {
            if(msg.message==WM_QUIT)
            {
                bDone=true;
            }else{
                TranslateMessage(&msg);
                DispatchMessage(&msg);
            }
        }else{
            if(gbActiveWindow==true)
            {
                Display();
            }
        }
    }    

    return ((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd,UINT iMsg,WPARAM wParam,LPARAM lParam)
{
    void ToggleFullscreen(void);
    void Resize(int,int);
    void UnInitialize(void);

    switch(iMsg)
    {
        case WM_CREATE:
            fprintf(gpFile,"\nWindow Created for 3d cube.");
            break;
        case WM_KEYDOWN:
            switch(wParam)
            {
                case VK_ESCAPE:
                    DestroyWindow(hwnd);
                    break;
                case 0x46:
                case 0x66:
                    ToggleFullscreen();
                    break;
                default:
                    break;
            }
            break;
        case WM_SETFOCUS:
            gbActiveWindow=true;
            break;
        case WM_KILLFOCUS:
            gbActiveWindow=false;
            break;
        case WM_ERASEBKGND:
            return 0;
        case WM_SIZE:
            Resize(LOWORD(lParam),HIWORD(lParam));
            break;
        case WM_CLOSE:
            DestroyWindow(hwnd);
            break;
        case WM_DESTROY:
            fprintf(gpFile,"\nLog is terminated.");
            UnInitialize();
            PostQuitMessage(0);
            break;
    }
    return (DefWindowProc(hwnd,iMsg,wParam,lParam));
}

void ToggleFullscreen(void)
{
    MONITORINFO mi={sizeof(MONITORINFO)};
    if(gbFullscreen==false)
    {
        dwStyle=GetWindowLong(ghwnd,GWL_STYLE);
        if(dwStyle & WS_OVERLAPPEDWINDOW)
        {
            if(GetWindowPlacement(ghwnd,&wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd,MONITORINFOF_PRIMARY),&mi))
            {
                fprintf(gpFile,"\nFullscreen Enable");
                SetWindowLong(ghwnd,GWL_STYLE,(dwStyle & ~WS_OVERLAPPEDWINDOW));
                SetWindowPos(ghwnd,HWND_TOP,mi.rcMonitor.left,mi.rcMonitor.top,mi.rcMonitor.right-mi.rcMonitor.left,mi.rcMonitor.bottom-mi.rcMonitor.top,SWP_NOZORDER|SWP_FRAMECHANGED);
            }
        }
        ShowCursor(FALSE);
        gbFullscreen=true;
    }
    else
    {
        SetWindowLong(ghwnd,GWL_STYLE,(dwStyle | WS_OVERLAPPEDWINDOW));
        SetWindowPlacement(ghwnd,&wpPrev);
        SetWindowPos(ghwnd,HWND_TOP,0,0,0,0,SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
        fprintf(gpFile,"\nNormal Window Enable");
        ShowCursor(TRUE);
        gbFullscreen=false;
    }
}

void Initialize(void)
{
    void Resize(int,int);

    PIXELFORMATDESCRIPTOR pfd;
    int iPixelFormatIndex;

    ghdc=GetDC(ghwnd);

    ZeroMemory(&pfd,sizeof(PIXELFORMATDESCRIPTOR));

    pfd.nSize=sizeof(PIXELFORMATDESCRIPTOR);
    pfd.nVersion=1;
    pfd.dwFlags=PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
    pfd.iPixelType=PFD_TYPE_RGBA;
    pfd.cColorBits=32;
    pfd.cRedBits=8;
    pfd.cGreenBits=8;
    pfd.cBlueBits=8;
    pfd.cAlphaBits=8;
    pfd.cDepthBits=32;

    iPixelFormatIndex=ChoosePixelFormat(ghdc,&pfd);
    if(iPixelFormatIndex==0)
    {
        fprintf(gpFile,"\nFailed to choose pixel format index");
        DestroyWindow(ghwnd);
    }

    if(SetPixelFormat(ghdc,iPixelFormatIndex,&pfd)==FALSE)
    {
        fprintf(gpFile,"\nFailed to set pixel format");
        DestroyWindow(ghwnd);
    }

    ghrc=wglCreateContext(ghdc);
    if(ghrc==NULL)
    {
        fprintf(gpFile,"\nFailed to create rendering context");
        DestroyWindow(ghwnd);
    }

    if(wglMakeCurrent(ghdc,ghrc)==FALSE)
    {
        fprintf(gpFile,"\nFailed to switch from device context to rendering context");
        DestroyWindow(ghwnd);
    }
    glClearColor(0.0f,0.0f,0.0f,0.0f);

    IdentityMatrix[0]=1.0f; IdentityMatrix[4]=0.0f; IdentityMatrix[8]=0.0f;     IdentityMatrix[12]=0.0f; 
    IdentityMatrix[1]=0.0f; IdentityMatrix[5]=1.0f; IdentityMatrix[9]=0.0f;     IdentityMatrix[13]=0.0f; 
    IdentityMatrix[2]=0.0f; IdentityMatrix[6]=0.0f; IdentityMatrix[10]=1.0f;    IdentityMatrix[14]=0.0f; 
    IdentityMatrix[3]=0.0f; IdentityMatrix[7]=0.0f; IdentityMatrix[11]=0.0f;    IdentityMatrix[15]=1.0f; 


    TranslationMatrix[0]=1.0f; TranslationMatrix[4]=0.0f; TranslationMatrix[8]=0.0f;     TranslationMatrix[12]=0.0f; 
    TranslationMatrix[1]=0.0f; TranslationMatrix[5]=1.0f; TranslationMatrix[9]=0.0f;     TranslationMatrix[13]=0.0f; 
    TranslationMatrix[2]=0.0f; TranslationMatrix[6]=0.0f; TranslationMatrix[10]=1.0f;     TranslationMatrix[14]=-6.0f; 
    TranslationMatrix[3]=0.0f; TranslationMatrix[7]=0.0f; TranslationMatrix[11]=0.0f;     TranslationMatrix[15]=1.0f; 

    ScaleMatrix[0]=0.75f; ScaleMatrix[4]=0.0f; ScaleMatrix[8]=0.0f;     ScaleMatrix[12]=0.0f; 
    ScaleMatrix[1]=0.0f; ScaleMatrix[5]=0.75f; ScaleMatrix[9]=0.0f;     ScaleMatrix[13]=0.0f; 
    ScaleMatrix[2]=0.0f; ScaleMatrix[6]=0.0f; ScaleMatrix[10]=0.75f;    ScaleMatrix[14]=0.0f; 
    ScaleMatrix[3]=0.0f; ScaleMatrix[7]=0.0f; ScaleMatrix[11]=0.0f;    ScaleMatrix[15]=1.0f; 



    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);

    glShadeModel(GL_SMOOTH);
    glHint(GL_PERSPECTIVE_CORRECTION_HINT,GL_NICEST);

    Resize(WIN_WIDTH,WIN_HEIGHT);
}

void Resize(int width, int height)
{
	if (height == 0)
	{
		height = 1;
	}
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	glMatrixMode(GL_PROJECTION);	
	glLoadIdentity();

	gluPerspective(	45.0f,
					(GLfloat)width/(GLfloat)height,
					0.1f,
					100.0f
				);

}

GLfloat RotateAngle=0.0f;

void Display(void)
{
	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
	
    static GLfloat AngleInRadient=0.0f;

	glMatrixMode(GL_MODELVIEW);

	// glLoadIdentity();
	// glTranslatef(0.0f,0.0f,-6.0f);
	// glScalef(0.75f,0.75f,0.75f);
	// glRotatef(RotateAngle, 1.0f, 0.0f, 0.0f);
	// glRotatef(RotateAngle, 0.0f, 1.0f, 0.0f);
	// glRotatef(RotateAngle, 0.0f, 0.0f, 1.0f);


    glLoadMatrixf(IdentityMatrix);
    glMultMatrixf(TranslationMatrix);
    glMultMatrixf(ScaleMatrix);

    AngleInRadient=RotateAngle*(M_PI/180.0f);

    RotationMatrix_X[0]=1.0f;                   RotationMatrix_X[4]=0.0f;                   RotationMatrix_X[8]=0.0f;                   RotationMatrix_X[12]=0.0f; 
    RotationMatrix_X[1]=0.0f;                   RotationMatrix_X[5]=cos(AngleInRadient);    RotationMatrix_X[9]=-sin(AngleInRadient);   RotationMatrix_X[13]=0.0f; 
    RotationMatrix_X[2]=0.0f;                   RotationMatrix_X[6]=sin(AngleInRadient);    RotationMatrix_X[10]=cos(AngleInRadient);   RotationMatrix_X[14]=0.0f; 
    RotationMatrix_X[3]=0.0f;                   RotationMatrix_X[7]=0.0f;                   RotationMatrix_X[11]=0.0f;                  RotationMatrix_X[15]=1.0f; 

    RotationMatrix_Y[0]=cos(AngleInRadient);    RotationMatrix_Y[4]=0.0f;                   RotationMatrix_Y[8]=sin(AngleInRadient);    RotationMatrix_Y[12]=0.0f; 
    RotationMatrix_Y[1]=0.0f;                   RotationMatrix_Y[5]=1.0f;                   RotationMatrix_Y[9]=0.0f;                   RotationMatrix_Y[13]=0.0f; 
    RotationMatrix_Y[2]=-sin(AngleInRadient);   RotationMatrix_Y[6]=0.0f;                   RotationMatrix_Y[10]=cos(AngleInRadient);   RotationMatrix_Y[14]=0.0f; 
    RotationMatrix_Y[3]=0.0f;                   RotationMatrix_Y[7]=0.0f;                   RotationMatrix_Y[11]=0.0f;                  RotationMatrix_Y[15]=1.0f; 

    RotationMatrix_Z[0]=cos(AngleInRadient);    RotationMatrix_Z[4]=-sin(AngleInRadient);   RotationMatrix_Z[8]=0.0f;                   RotationMatrix_Z[12]=0.0f; 
    RotationMatrix_Z[1]=sin(AngleInRadient);    RotationMatrix_Z[5]=cos(AngleInRadient);    RotationMatrix_Z[9]=0.0f;                   RotationMatrix_Z[13]=0.0f; 
    RotationMatrix_Z[2]=0.0f;                   RotationMatrix_Z[6]=0.0f;                   RotationMatrix_Z[10]=1.0f;                  RotationMatrix_Z[14]=0.0f; 
    RotationMatrix_Z[3]=0.0f;                   RotationMatrix_Z[7]=0.0f;                   RotationMatrix_Z[11]=0.0f;                  RotationMatrix_Z[15]=1.0f; 
    
    glMultMatrixf(RotationMatrix_X);
    glMultMatrixf(RotationMatrix_Y);
    glMultMatrixf(RotationMatrix_Z);



	glBegin(GL_QUADS);

		glColor3f(1.0f, 0.0f, 0.0f);
			glVertex3f(1.0f, 1.0f, 1.0f);
			glVertex3f(-1.0f, 1.0f, 1.0f);
			glVertex3f(-1.0f, -1.0f, 1.0f);
			glVertex3f(1.0f, -1.0f, 1.0f);
		
		glColor3f(0.0f, 1.0f, 0.0f);
			glVertex3f(1.0f, 1.0f, -1.0f);
			glVertex3f(1.0f, 1.0f, 1.0f);
			glVertex3f(1.0f, -1.0f, 1.0f);
			glVertex3f(1.0f, -1.0f, -1.0f);
		
		glColor3f(0.0f, 0.0f, 1.0f);
			glVertex3f(-1.0f, 1.0f, -1.0f);
			glVertex3f(1.0f, 1.0f, -1.0f);
			glVertex3f(1.0f, -1.0f, -1.0f);
			glVertex3f(-1.0f, -1.0f, -1.0f);
		
		glColor3f(0.0f, 1.0f, 1.0f);
			glVertex3f(-1.0f, 1.0f, 1.0f);
			glVertex3f(-1.0f, 1.0f, -1.0f);
			glVertex3f(-1.0f, -1.0f, -1.0f);
			glVertex3f(-1.0f, -1.0f, 1.0f);

		glColor3f(1.0f, 0.0f, 1.0f);
			glVertex3f(1.0f, 1.0f, -1.0f);
			glVertex3f(-1.0f, 1.0f, -1.0f);
			glVertex3f(-1.0f, 1.0f, 1.0f);
			glVertex3f(1.0f, 1.0f, 1.0f);

		glColor3f(1.0f, 1.0f, 0.0f);
			glVertex3f(1.0f, -1.0f, -1.0f);
			glVertex3f(-1.0f, -1.0f, -1.0f);
			glVertex3f(-1.0f, -1.0f, 1.0f);
			glVertex3f(1.0f, -1.0f, 1.0f);

	glEnd();

	SwapBuffers(ghdc);

	if(RotateAngle>360.0f)
		RotateAngle=0.0f;
	
	RotateAngle += 1.0f;
	
}

void UnInitialize(void)
{
    if(gbFullscreen==true)
    {
        dwStyle=GetWindowLong(ghwnd,GWL_STYLE);
        SetWindowLong(ghwnd,GWL_STYLE,(dwStyle | WS_OVERLAPPEDWINDOW));
        SetWindowPlacement(ghwnd,&wpPrev);
        SetWindowPos(ghwnd,HWND_TOP,0,0,0,0,SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
        ShowCursor(TRUE);
    }

    if(wglGetCurrentContext()==ghrc)
    {
        wglMakeCurrent(NULL,NULL);
    }
    if(ghrc)
    {
        wglDeleteContext(ghrc);
        ghrc=NULL;
    }

    if(ghdc)
    {
        ReleaseDC(ghwnd,ghdc);
        ghdc=NULL;
    }

    if(gpFile)
    {
        fprintf(gpFile,"\nLog Ended..!");
        fclose(gpFile);
        gpFile=NULL;
    }


}