// C/C++ Standard
#define _USE_MATH_DEFINES
#include<math.h>
#include<stdlib.h>
#include<stdio.h>

//Win32
#include<windows.h>

//Custom
#include"GameBaseLogic.h"

//OpenGL
#include<GL/GL.h>
#include<GL/GLU.h>

//OpenAL
#include<al.h>
#include<alc.h>
#include"C:\Program Files (x86)\OpenAL 1.1 SDK\samples\framework\Win32\CWaves.h" //Change the path

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

//OpenGL
#pragma comment(lib,"OpenGL32.lib")
#pragma comment(lib,"GLU32.lib")

//Win32
#pragma comment(lib,"user32.lib")
#pragma comment(lib,"gdi32.lib")
#pragma comment(lib,"winmm.lib")

//OpenAL
#pragma comment(lib,"C:\\Program Files (x86)\\OpenAL 1.1 SDK\\libs\\Win64\\OpenAL32.lib") //Change the path

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

FILE* gpFile = NULL;
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
bool gbFullscreen = false;
bool gbActiveWindow = false;
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;
HINSTANCE ghInstance=NULL;

/* Skeleton of Screen
	Bird Hunter Game Inputs
	1.Menu Input:-p/P
		-Start 	:s/S
		-About	:a/A
		-Help	:h/H
		-Exit	:q/Q or Esc
	2.Game Input:-
		-Game Music On/Off	:m/M 
		-Game View 2D/3D		:v/V
		-Rifle Angle Shift:
			-Left		:Left Arrow Key
			-Right		:Right Arrow Key
		-Firing Shot		:Spacebar
*/

//Temp Configuration
	int day=0;
	int year=0;
//

//OpenAL Configuration
	static ALCdevice *g_alcDevice=NULL;
	static const ALCchar *gDefaultALCDeviceName=NULL;
	static ALCcontext *g_alcContext=NULL;

	class Sound{
		public:
			CWaves cWaves;
			WAVEID waveID;
			ALuint Source,Buffer;
			ALCenum error;
			ALint	iDataSize, iFrequency;
			ALenum	eBufferFormat;
			ALchar	*pData;
			const char *szFileName;

		void Initialize(const char *sz_File_Name);
		void DeInitialize(void);
	};

	void Sound::Initialize(const char *sz_File_Name)
	{
		if (sz_File_Name==NULL)
		{
			fprintf(gpFile, "\nFailed to get sound file name");
			DestroyWindow(ghwnd);
		}

		szFileName=sz_File_Name;

		alGenSources((ALuint)1,&Source);
		alSourcef(Source,AL_GAIN,1);
		
		if ((error = alGetError()) != AL_NO_ERROR) 
		{
			fprintf(gpFile, "\nalSourcef() failed to Failed to set volume");
			DestroyWindow(ghwnd);
		}

		alSource3f(Source,AL_POSITION,0.0,0.0,0.0);
		
		alGenBuffers((ALuint)1,&Buffer);

		cWaves.LoadWaveFile(szFileName,&waveID);
		cWaves.GetWaveSize(waveID, (unsigned long*)&iDataSize);
		cWaves.GetWaveData(waveID, (void**)&pData);
		cWaves.GetWaveFrequency(waveID, (unsigned long*)&iFrequency);
		cWaves.GetWaveALBufferFormat(waveID, &alGetEnumValue, (unsigned long*)&eBufferFormat);
					
		alBufferData(Buffer,eBufferFormat,pData,iDataSize,iFrequency);

		alSourcei(Source,AL_BUFFER,Buffer);

		cWaves.DeleteWaveFile(waveID);
	}
	void Sound::DeInitialize(void)
	{
		alDeleteSources(1, &Source);
		alDeleteBuffers(1, &Buffer);
	}

	Sound Bird_Death_Sound,Stone_Blasting_Sound,Gun_Shot_Sound;

//	

//GLU Shapes Variables
	GLUquadric *SphereQuadric=NULL;
	GLUquadric *CylinderQuadric=NULL;
	GLUquadric *DiskQuadric=NULL;
	GLUquadric *PartialDiskQuadric=NULL;
//

//Model Configuration
	//Human
		//Leg
			GLfloat LegSpeed=0.0f;//Make it 1 to move
			GLfloat LegThaiRotationMax=290.0f;
			GLfloat LegThaiRotationMin=250.0f;
			GLfloat LegCalfRotationCenterAngle=270.0f;

			GLfloat RightLegThaiRotation=LegCalfRotationCenterAngle;//Should be LegThaiRotationMax
			GLfloat RightLegCalfRotation=0.0f;
			GLfloat RightLegThaiSpeed=LegSpeed;
			GLfloat RightLegCalfSpeed=LegSpeed;

			GLfloat LeftLegThaiRotation=LegCalfRotationCenterAngle;//Should be LegThaiRotationMin
			GLfloat LeftLegCalfRotation=0.0f;
			GLfloat LeftLegThaiSpeed=LegSpeed;
			GLfloat LeftLegCalfSpeed=LegSpeed;
		//
		//Hand
			GLfloat HandSpeed=0.0;//Make 1 to move
			GLfloat HandShoulderRotationMax=290;
			GLfloat HandShoulderRotationMin=250;
			GLfloat HandForearmRotationCenterAngle=270;

			GLfloat RightHandShoulderRotation=HandForearmRotationCenterAngle;//Should be HandShoulderRotationMin
			GLfloat RightHandForeArmRotation=90.0f;//Recomendation:Make 20
			GLfloat RightHandShoulderSpeed=HandSpeed;
			GLfloat RightHandForeArmSpeed=HandSpeed;

			GLfloat LeftHandShoulderRotation=HandForearmRotationCenterAngle;//Should be HandShoulderRotationMax
			GLfloat LeftHandForeArmRotation=90.0f;//Recomendation:Make 20
			GLfloat LeftHandShoulderSpeed=HandSpeed;
			GLfloat LeftHandForeArmSpeed=HandSpeed;
		//
	//
	//Bird
		GLfloat WingRotate=35.0f,WingRotateSpeed=1.0f;
	//
//

//Set Light and Material
	GLfloat LightAmbient[]={0.0f,0.0f,0.0f,1.0f};//White Light
	GLfloat LightDefuse[]={1.0f,1.0f,1.0f,1.0f};
	GLfloat LightPosition[]={0.0f,6.0f,3.0f,0.0f};//directional light like sun
	GLfloat LightSpecular[]={1.0f,1.0f,1.0f,1.0f};

	GLfloat LightModelAmbient[]={0.8f,0.8f,0.8f,1.0f};
	GLfloat LightModelLocalViewer[]={0.0f};

	GLfloat materialAmbient[4];
	GLfloat materilaDefuse[4];
	GLfloat materialSpecular[4];
	GLfloat materialEmission[4];
	GLfloat materialShininess=0.0f;
//

//DisplayList
	//Tree DisplayList
		GLuint TreeModel=0;

	//Bullet DisplayList
		GLuint BulletModel=0;
	
	//Rifle DisplayList
		GLuint RifleGrip=0;
		GLuint RifleBody=0;

	// Human Material DisplayList
		GLuint SkinHumanMaterial=0;
		GLuint HeadHumanMaterial=0;
		GLuint JointSphereHumanMaterial=0;
		GLuint CylinderHumanMaterial=0;
		GLuint ChestHumanMaterial=0;

	//Bird Material DisplayList
		GLuint MouthBirdMaterial=0;
		GLuint BodyBirdMaterial=0;
		GLuint LegBirdMaterial=0;
		GLuint FootBirdMaterial=0;
		GLuint EyeBirdMaterial=0;
//

//Queue+Singly LL for ShotQueue

	//FIreShot DS
		typedef struct SHOTDATAINFO
		{
			GLfloat x;
			GLfloat y;
			GLfloat angle;
			GLfloat angleInRadiant;
			GLboolean removeShot;
		}ShotDataNodeInfo;

		typedef struct SHOTLINKEDLIST
		{
			ShotDataNodeInfo data;
			struct SHOTLINKEDLIST *next;
		}Node,*pShotNode;

		typedef struct SHOTLISTQUEUE{
			struct SHOTLINKEDLIST *front;
			struct SHOTLINKEDLIST *rear;
		}QNode,*pQueueShotListNode;

		pQueueShotListNode ShotListQueue=NULL;
	//

	//Bird FLY DS
		typedef struct BIRDFLYDATAINFO
		{
			GLfloat x;
			GLfloat y;
			GLfloat xVel;
			GLfloat yVel;
			GLfloat angleInRadiant;
			GLboolean stoneDroppedYet;
			GLfloat stoneDropX;
			GLboolean removeBird;
		}BirdFlyDataNodeInfo;

		typedef struct BIRDFLYLINKEDLIST
		{
			BirdFlyDataNodeInfo data;
			struct BIRDFLYLINKEDLIST *next;
		}BirdFlyNode,*pBirdFlyNode;

		typedef struct BIRDFLYLISTQUEUE{
			struct BIRDFLYLINKEDLIST *front;
			struct BIRDFLYLINKEDLIST *rear;
		}QBirdFlyNode,*pQueueBirdFlyListNode;

		pQueueBirdFlyListNode BirdFlyListQueue=NULL;
	//

	//Falling Stone DS
		typedef struct STONE_FALLING_STRUCT
		{
			GLfloat x;
			GLfloat y;
			GLfloat xVel;
			GLfloat yVel;
			GLfloat angleInRadiant;
			GLboolean removeStone;
		}StoneFallingStructDataNode;

		typedef struct STONE_FALLING_LINKED_LIST
		{
			StoneFallingStructDataNode data;
			struct STONE_FALLING_LINKED_LIST *next;
		}StoneFallingLLNode,*pStoneFallingLLNode;

		typedef struct STONE_FALLING_LIST_QUEUE{
			struct STONE_FALLING_LINKED_LIST *front;
			struct STONE_FALLING_LINKED_LIST *rear;
		}StoneFallingQueueNode,*pStoneFallingQueueNode;

		pStoneFallingQueueNode StoneFallingListQueue=NULL;
	//
//

//Intervals,Frequency
	GLboolean fireShotTimeInterval=GL_FALSE,birdFlyTimeInterval=GL_FALSE;

	LARGE_INTEGER iSysFrequency;
	LARGE_INTEGER iFireShotStartTime;
	LARGE_INTEGER iBirdFlyStartTime;
	LARGE_INTEGER iCurrentTime;

	GLfloat FiringAngle=90.0f;
	GLfloat FiringAngleSpeed=1.0f;
//

//Global Key Pressed
	//Game Input
		GLboolean leftArrowKeyPressed=GL_FALSE,rightArrowKeyPressed=GL_FALSE,fireShotKeyPressed=GL_FALSE;
	//Menu Input
		GLboolean MenuMusic=GL_TRUE,GameMusic=GL_TRUE;
		GLboolean isMenuEnable=GL_FALSE,isAboutEnable=GL_FALSE,isHelpEnable=GL_FALSE,isStartEnable=GL_FALSE,isGameOverEnable=GL_FALSE,QuitGame=GL_FALSE;
//

//Global Box Configuration for Game 
	GLfloat SquareCubeLength=4.25f;
	GLfloat WidthStart=-SquareCubeLength-4.0f;
	GLfloat HeightStart=-SquareCubeLength;
	GLfloat WidthEnd=SquareCubeLength+4.0f;
	GLfloat HeightEnd=SquareCubeLength;

//

//Global Scale for Random number
	const float fBirdFlyXPositionScale=1.0f/(float)RAND_MAX;
	// const float fBirdFlyYPositionScale=(HeightEnd*2.0f)/(float)RAND_MAX;
//

//Object Collision Detection Configuration
	GLfloat ShotBirdRangeLength=0.3f;
	GLfloat ShotStoneRangeLength=0.1f;
//

//Font 
	unsigned int FontListBase;

	GLYPHMETRICSFLOAT gmf[256];
//

//Game Level Configuration
	GLint giMaxShotWasteLeft=50;
	GLint giMaxBirdAliveLeft=10;
	GLint giMaxStoneDropWasteLeft=7;

	GLint giMaxShotWasteLeftCount=giMaxShotWasteLeft;
	GLint giMaxBirdAliveLeftCount=giMaxBirdAliveLeft;
	GLint giMaxStoneDropWasteCount=giMaxStoneDropWasteLeft;
//

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	void Initialize(void);
	void Display(void);
	void Update(void);
	void RemoveQueueData(void);
	void ToggleFullscreen(void);

	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("GameBirdHunterActualGameV1");
	int X1, X2, Y1, Y2;
	bool bDone = false;

	double elapsedTimeFireShot=0.0f,elapsedTimeBirdFly=0.0f;

	ghInstance=hInstance;

	if (fopen_s(&gpFile, "Game-Bird_Hunter-log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Failed to Create log file!"), TEXT("Error"), MB_OK | MB_ICONERROR);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "\nLog will be start from here...!");
	}

	X1 = GetSystemMetrics(SM_CXSCREEN) / 2;
	Y1 = GetSystemMetrics(SM_CYSCREEN) / 2;

	X2 = WIN_WIDTH / 2;
	Y2 = WIN_HEIGHT / 2;

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style =  CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(GRAY_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));

	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("Bird Hunter(version 1)"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		X1 - X2,//100,
		Y1 - Y2,//100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;
	Initialize();

	ShowWindow(hwnd, iCmdShow);

	SetForegroundWindow(hwnd);
	SetFocus(hwnd);	
	
	gbFullscreen=false;
	ToggleFullscreen();

	isMenuEnable=GL_TRUE;


	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE ))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				if (isStartEnable==GL_TRUE)
				{	
					QueryPerformanceCounter((LARGE_INTEGER*)&iCurrentTime);
					elapsedTimeFireShot=( ( ((float)iCurrentTime.QuadPart) - ((float)iFireShotStartTime.QuadPart) ) / (float)iSysFrequency.QuadPart );
					elapsedTimeBirdFly=( ( ((float)iCurrentTime.QuadPart) - ((float)iBirdFlyStartTime.QuadPart) ) / (float)iSysFrequency.QuadPart );

					if ( elapsedTimeFireShot >= 0.0800f)//Shot time Interval // 800 milisec
					{
						fireShotTimeInterval=GL_TRUE;
						iFireShotStartTime.QuadPart=iCurrentTime.QuadPart;
					}else{
						fireShotTimeInterval=GL_FALSE;
					}

					if ( elapsedTimeBirdFly >= 3.0f)//Birds time Interval //5000 milisec
					{
						birdFlyTimeInterval=GL_TRUE;
						iBirdFlyStartTime.QuadPart=iCurrentTime.QuadPart;
					}else{
						birdFlyTimeInterval=GL_FALSE;
					}
				}

				Display();
				Update();
				RemoveQueueData();
			}
		}
	}

	return ((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void ToggleFullscreen(void);
	void Resize(int, int);
	void UnInitialize(void);

	switch (iMsg)
	{
	case WM_CREATE:
		fprintf(gpFile, "\nOpenGL+Win32 Game-Bird Hunter(Base Logic) Log is Started!");
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
			case VK_ESCAPE:
			case 'Q':
				DestroyWindow(hwnd);
				break;
			case VK_SPACE:
				fireShotKeyPressed=GL_TRUE;	
				break;
			case 0x46:
			case 0x66:
				ToggleFullscreen();
				break;
			case VK_RIGHT:
				rightArrowKeyPressed=GL_TRUE;
				break;
			case VK_LEFT:
				leftArrowKeyPressed=GL_TRUE;
				break;
			case 'M':
				GameMusic=!GameMusic;
				break;
			case 'P':
				isMenuEnable=GL_TRUE;
				//Disable other Screen
				isStartEnable=GL_FALSE;
				isHelpEnable=GL_FALSE;
				isAboutEnable=GL_FALSE;
				isGameOverEnable=GL_FALSE;
				break;
			case 'S':
				isStartEnable=GL_TRUE;
				//Disable other Screen
				isMenuEnable=GL_FALSE;
				isHelpEnable=GL_FALSE;
				isAboutEnable=GL_FALSE;
				isGameOverEnable=GL_FALSE;

				giMaxShotWasteLeftCount=giMaxShotWasteLeft;
				giMaxBirdAliveLeftCount=giMaxBirdAliveLeft;
				giMaxStoneDropWasteCount=giMaxStoneDropWasteLeft;

				break;
			case 'H':
				isHelpEnable=GL_TRUE;
				//Disable other Screen
				isStartEnable=GL_FALSE;
				isMenuEnable=GL_FALSE;
				isAboutEnable=GL_FALSE;
				isGameOverEnable=GL_FALSE;

				break;
			case 'A':
				isAboutEnable=GL_TRUE;
				//Disable other Screen
				isStartEnable=GL_FALSE;
				isMenuEnable=GL_FALSE;
				isHelpEnable=GL_FALSE;
				isGameOverEnable=GL_FALSE;
				break;
				
			default:
				break;
		}
		break;
	case WM_KEYUP:
		switch (wParam)
		{
			case VK_SPACE:
				fireShotKeyPressed=GL_FALSE;	
				break;
			case VK_RIGHT:
				rightArrowKeyPressed=GL_FALSE;
				break;
			case VK_LEFT:
				leftArrowKeyPressed=GL_FALSE;
				break;
			default:
				break;
		}
		break;
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;
	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;

	case WM_ERASEBKGND:
		return 0;
	case WM_SIZE:
		Resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;
	case WM_DESTROY:
		fprintf(gpFile, "\nOpenGL+Win32 Game-Bird Hunter(Base Logic) Log is Ended!");

		UnInitialize();
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}
void ToggleFullscreen(void)
{
	
	MONITORINFO mi = { sizeof(MONITORINFO) };
	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW) 
		{
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				fprintf(gpFile, "\nFullscreen Enabled");
				SetWindowLong(ghwnd, GWL_STYLE, (dwStyle & ~WS_OVERLAPPEDWINDOW));
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
		gbFullscreen = true;
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, (dwStyle | WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		fprintf(gpFile, "\nNormal Window");
		ShowCursor(TRUE);
		gbFullscreen = false;
	}
}

void Initialize(void)
{
	void Resize(int, int);

	void HumanMaterialDisplayList(void);
	void BirdMaterialDisplayList(void);
	void TreeModelDisplayList(void);
	void BulletModelDisplayList(void);
	void RifleMaterialDisplayList(void);
	
	pQueueShotListNode CreateShotListQueue(void);
	pQueueBirdFlyListNode CreateBirdFlyListQueue(void);
	pStoneFallingQueueNode CreateStoneFallingListQueue(void);

	ShotListQueue=CreateShotListQueue();
	BirdFlyListQueue=CreateBirdFlyListQueue();
	StoneFallingListQueue=CreateStoneFallingListQueue();

	unsigned int CreateOutlineFont(char *,int,float);

	//Local variable initialization
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	ghdc = GetDC(ghwnd);

	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER ;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits= 32 ;


	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (iPixelFormatIndex == 0 )
	{
		fprintf(gpFile, "\nChoosePixelFormat() Failed to get pixel format index");
		DestroyWindow(ghwnd);
	}

	if ( SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE )
	{
		fprintf(gpFile, "\nSetPixelFormat() Failed to set pixel format for window");
		DestroyWindow(ghwnd);
	}

	ghrc = wglCreateContext(ghdc);
	if ( ghrc == NULL)
	{
		fprintf(gpFile, "\nwglCreateContext() Failed to get the rendering context");
		DestroyWindow(ghwnd);
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE )
	{
		fprintf(gpFile, "\nwglMakeCurrent() Failed to switch current context from device to rendering context");
		DestroyWindow(ghwnd);
	}

	//Time Interval setup
	if (!QueryPerformanceFrequency((LARGE_INTEGER*)&iSysFrequency))
	{
		fprintf(gpFile, "\nQueryPerformanceFrequency Failed to get frequency of the performance counter");
		DestroyWindow(ghwnd);
	}
	//Query Performance Counter
		QueryPerformanceCounter((LARGE_INTEGER*)&iFireShotStartTime);
		QueryPerformanceCounter((LARGE_INTEGER*)&iBirdFlyStartTime);

	//SetColor
		glClearColor(0.25f, 0.25f, 0.25f, 1.0f);
		
	//Depth
		glClearDepth(1.0f);
		glEnable(GL_DEPTH_TEST);
		glDepthFunc(GL_LEQUAL);

		glShadeModel(GL_SMOOTH);
		glHint(GL_PERSPECTIVE_CORRECTION_HINT,GL_NICEST);

	//Set Lighting
		glEnable(GL_LIGHTING);
		glLightfv(GL_LIGHT0,GL_AMBIENT,LightAmbient);
		glLightfv(GL_LIGHT0,GL_DIFFUSE,LightDefuse);
		glLightfv(GL_LIGHT0,GL_POSITION,LightPosition);
		glLightfv(GL_LIGHT0,GL_SPECULAR,LightSpecular);
		glEnable(GL_LIGHT0);

	//Set Light Model
		glEnable(GL_AUTO_NORMAL);
		glEnable(GL_NORMALIZE);
		glLightModelfv(GL_LIGHT_MODEL_AMBIENT,LightModelAmbient);
		glLightModelfv(GL_LIGHT_MODEL_LOCAL_VIEWER,LightModelLocalViewer);

	//Quadric Initailization
		SphereQuadric=gluNewQuadric();
		CylinderQuadric=gluNewQuadric();
		DiskQuadric=gluNewQuadric();
		PartialDiskQuadric=gluNewQuadric();

	//DisplayList
		HumanMaterialDisplayList();
		BirdMaterialDisplayList();
		TreeModelDisplayList();
		BulletModelDisplayList();
		RifleMaterialDisplayList();

	//Font
		FontListBase=CreateOutlineFont("Comic Sans MS",10,0.25f);

	//Initailize OpenAL
		//Step1:Get Default Device Name
			gDefaultALCDeviceName=alcGetString(NULL,ALC_DEFAULT_DEVICE_SPECIFIER);
		//Step2:Open Default Device
			g_alcDevice=alcOpenDevice(gDefaultALCDeviceName);
			//Check Failed to get Device
			if(g_alcDevice==NULL)
			{
				fprintf(gpFile, "\nalcOpenDevice() failed to open the ALC device by name :%s",gDefaultALCDeviceName);
				DestroyWindow(ghwnd);
			}
		//Step3:Create Context to OpenGL
			g_alcContext=alcCreateContext(g_alcDevice,NULL);
			if(g_alcContext==NULL)
			{
				fprintf(gpFile, "\nalcCreateContext() failed to create the ALC context");
				DestroyWindow(ghwnd);
			}
		//Step4:Update the ALC context to current Context
			if(!alcMakeContextCurrent(g_alcContext))
			{
				fprintf(gpFile, "\nalcMakeContextCurrent() failed to update the ALC context to current context");
				DestroyWindow(ghwnd);
			}
		//Step5:Initialize all sound
			Bird_Death_Sound.Initialize("Bird_Death_Sound.wav");
			Stone_Blasting_Sound.Initialize("Stone_Blasting_Sound.wav");
			Gun_Shot_Sound.Initialize("Gun_Shot_Sound.wav");
	//

	//Warmup resize call
		Resize(WIN_WIDTH, WIN_HEIGHT);
	

}

void Resize(int width, int height)
{
	if (height == 0)
	{
		height = 1;
	}
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	glMatrixMode(GL_PROJECTION);	
	glLoadIdentity();

	gluPerspective(	45.0f,
					(GLfloat)width/(GLfloat)height,
					0.1f,
					100.0f
				);

}

void Display(void)
{
	//Screens
		void ActualGameScreens(void);
		void MenuScreen(void);
		void HelpScreen(void);
		void AboutScreen(void);
		void GameOverScreen(void);

	//code
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();

		glTranslatef(0.0f,0.0f,-11.5f);
		// glRotatef(year,0.0f,1.0f,0.0f);

		// glPolygonMode(GL_FRONT_AND_BACK,GL_LINE);//TODO:[Remove Later]
		// glBegin(GL_QUADS);
		// 	glVertex3f(WidthStart, HeightStart, 0.0f);
		// 	glVertex3f(WidthEnd, HeightStart, 0.0f);
		// 	glVertex3f(WidthEnd, HeightEnd, 0.0f);
		// 	glVertex3f(WidthStart, HeightEnd, 0.0f);	
		// glEnd();
		glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);//TODO:[Remove Later]

		if(isMenuEnable==GL_TRUE)
		{
			glPushMatrix();
				MenuScreen();
			glPopMatrix();
		}

		if(isHelpEnable==GL_TRUE)
		{
			glPushMatrix();
				HelpScreen();
			glPopMatrix();
		}
		
		if(isAboutEnable==GL_TRUE)
		{
			glPushMatrix();
				AboutScreen();
			glPopMatrix();
		}

		if(isStartEnable==GL_TRUE)
		{
			glPushMatrix();
				ActualGameScreens();
			glPopMatrix();
		}

		if(isGameOverEnable==GL_TRUE)
		{
			glPushMatrix();
				GameOverScreen();
			glPopMatrix();
		}

	SwapBuffers(ghdc);
	
}
void UnInitialize(void)
{
	void DeallocateAll_DS_Pointers(void);
	
	if (gbFullscreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, (dwStyle | WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}
	
	if (wglGetCurrentContext() == ghrc ) {
		wglMakeCurrent(NULL, NULL);
	}
	else
	{
		fprintf(gpFile, "\nwglGetCurrentContext current context is not rendering context so no need to switch.");
	}

	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}
	else
	{
		fprintf(gpFile, "\nRendering Context is already uninitialized or never created/initialized.");
	}


	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}
	else
	{
		fprintf(gpFile, "\nDevice context is already uninitialized or never created/initialized.");
	}

	//GLU Shape
		gluDeleteQuadric(SphereQuadric);
		gluDeleteQuadric(CylinderQuadric);
		gluDeleteQuadric(DiskQuadric);
		gluDeleteQuadric(PartialDiskQuadric);

	//disable Light
		glDisable(GL_LIGHTING );
	//Delete Display List
		glDeleteLists(SkinHumanMaterial,1);
		glDeleteLists(HeadHumanMaterial,1);
		glDeleteLists(JointSphereHumanMaterial,1);
		glDeleteLists(CylinderHumanMaterial,1);
		glDeleteLists(ChestHumanMaterial,1);

	//Delete Tree List
		glDeleteLists(TreeModel,1);

	//Delete Display List
		glDeleteLists(MouthBirdMaterial,1);
		glDeleteLists(BodyBirdMaterial,1);
		glDeleteLists(LegBirdMaterial,1);
		glDeleteLists(FootBirdMaterial,1);
		glDeleteLists(EyeBirdMaterial,1);

	//deallocating DS pointer
		QuitGame=GL_TRUE;
		DeallocateAll_DS_Pointers();
	//

	//OpenAL DeInitialize
		Bird_Death_Sound.DeInitialize();
		Stone_Blasting_Sound.DeInitialize();
		Gun_Shot_Sound.DeInitialize();

		alcMakeContextCurrent(NULL);
		alcDestroyContext(g_alcContext);
		alcCloseDevice(g_alcDevice);

	//Font List
		if(FontListBase!=0){
			glDeleteLists(FontListBase,256);
		}
	//

	//File
		if (gpFile)
		{
			fprintf(gpFile, "\nLog will be End from here...!");
			fclose(gpFile);
			gpFile = NULL;
		}
	//
}

void Update(void)
{
	pShotNode NewShotNode=NULL;
	pBirdFlyNode NewBirdFlyNode=NULL;

	//Game Over Checking up
		if(giMaxShotWasteLeftCount <=0 || giMaxBirdAliveLeftCount<=0 || giMaxStoneDropWasteCount<=0)
		{
			isGameOverEnable=GL_TRUE;

			isStartEnable=GL_FALSE;
			isMenuEnable=GL_FALSE;
			isHelpEnable=GL_FALSE;
			isAboutEnable=GL_FALSE;

			giMaxShotWasteLeftCount=giMaxShotWasteLeft;
			giMaxBirdAliveLeftCount=giMaxBirdAliveLeft;
			giMaxStoneDropWasteCount=giMaxStoneDropWasteLeft;
		}
	//

	//Firing Angle
		if (FiringAngle<30.0f)
		{
			FiringAngle=30.0f;
		}
		if (FiringAngle>150.0f)
		{
			FiringAngle=150.0f;
		}

	//Arrow Key Press
		if (leftArrowKeyPressed==GL_TRUE){
			FiringAngle += FiringAngleSpeed;
		}

		if (rightArrowKeyPressed==GL_TRUE){
			FiringAngle -= FiringAngleSpeed;
		}

	
	//Global update
		year+=1.0f;		

	//Birds Update
		if (WingRotate>70.0f)
		{
			WingRotateSpeed=-1.0f;
		}
		if (WingRotate<-70.0f)
		{
			WingRotateSpeed=1.0f;
		}
		WingRotate+=WingRotateSpeed;
	
	//Human Update
		//Right Leg
			if (RightLegThaiRotation > LegThaiRotationMax )
			{
				RightLegThaiSpeed=-LegSpeed;
			}
			if (RightLegThaiRotation < LegThaiRotationMin )
			{
				RightLegThaiSpeed=LegSpeed;
			}

			if (RightLegThaiRotation > LegCalfRotationCenterAngle){
				RightLegCalfSpeed=-LegSpeed;
			}
			if (RightLegThaiRotation < LegCalfRotationCenterAngle){
				RightLegCalfSpeed=LegSpeed;
			}

			RightLegCalfRotation += RightLegCalfSpeed;

			RightLegThaiRotation += RightLegThaiSpeed;
			
		//Left Leg
			if (LeftLegThaiRotation > LegThaiRotationMax)
			{	
				LeftLegThaiSpeed=-LegSpeed;
			}
			if (LeftLegThaiRotation < LegThaiRotationMin)
			{
				LeftLegThaiSpeed=LegSpeed;
			}

			if (LeftLegThaiRotation > LegCalfRotationCenterAngle){
				LeftLegCalfSpeed=-LegSpeed;
			}
			if (LeftLegThaiRotation < LegCalfRotationCenterAngle){
				LeftLegCalfSpeed=LegSpeed;
			}

			LeftLegCalfRotation += LeftLegCalfSpeed;
			LeftLegThaiRotation += LeftLegThaiSpeed;

		//Right Hand
			if (RightHandShoulderRotation > HandShoulderRotationMax )
			{
				RightHandShoulderSpeed=-HandSpeed;
			}
			if (RightHandShoulderRotation < HandShoulderRotationMin )
			{
				RightHandShoulderSpeed=HandSpeed;
			}

			// if (RightHandShoulderRotation > HandForearmRotationCenterAngle){
			// 	RightHandForeArmSpeed=-HandSpeed;
			// }
			// if (RightHandShoulderRotation < HandForearmRotationCenterAngle){
			// 	RightHandForeArmSpeed=HandSpeed;
			// }

			// RightHandForeArmRotation += RightHandForeArmSpeed;

			RightHandShoulderRotation += RightHandShoulderSpeed;

		//Left Hand
			if (LeftHandShoulderRotation > HandShoulderRotationMax )
			{
				LeftHandShoulderSpeed=-HandSpeed;
			}
			if (LeftHandShoulderRotation < HandShoulderRotationMin )
			{
				LeftHandShoulderSpeed=HandSpeed;
			}

			// if (LeftHandShoulderRotation > HandForearmRotationCenterAngle){
			// 	LeftHandForeArmSpeed=-HandSpeed;
			// }
			// if (LeftHandShoulderRotation < HandForearmRotationCenterAngle){
			// 	LeftHandForeArmSpeed=HandSpeed;
			// }

			// LeftHandForeArmRotation += LeftHandForeArmSpeed;

			LeftHandShoulderRotation += LeftHandShoulderSpeed;

	//create/Malloc fire Shot
		if (fireShotKeyPressed==GL_TRUE)
		{

			if(fireShotTimeInterval==GL_TRUE)
			{
				//Shot Node Data Preparation
				NewShotNode=(pShotNode)malloc(sizeof(Node));

				if(NewShotNode==NULL)
				{
					fprintf(gpFile, "\nFailed to create Fire Shot node...!");
					DestroyWindow(ghwnd);
				}

				NewShotNode->data.angle=FiringAngle;
				NewShotNode->data.angleInRadiant=FiringAngle*(M_PI/180.0f);
				//To Handle The Firing Angle+Distance Position	
					NewShotNode->data.x=cos(NewShotNode->data.angleInRadiant)*0.75f;
					NewShotNode->data.y=sin(NewShotNode->data.angleInRadiant)*0.75f;
				
				// NewShotNode->data.x=cos(NewShotNode->data.angleInRadiant);
				// NewShotNode->data.y=sin(NewShotNode->data.angleInRadiant);

				NewShotNode->data.removeShot=GL_FALSE;
				NewShotNode->next=NULL;
				if(GameMusic)
				{
					// if(!PlaySound(MAKEINTRESOURCE(GUN_SHOT_SOUND),ghInstance, SND_ASYNC | SND_RESOURCE))
					// {
					// 	fprintf(gpFile, "\nFailed to load GUN_SHOT_SOUND Wav file to play music!");
					// 	exit(0);
					// }
					alSourcePlay(Gun_Shot_Sound.Source);
				}
				//Shot Node Data pushed into ShotListQueue
					if(ShotListQueue->rear!=NULL)
					{
						ShotListQueue->rear->next=NewShotNode;
					}

					ShotListQueue->rear=NewShotNode;

					if(ShotListQueue->front==NULL)
					{
						ShotListQueue->front= ShotListQueue->rear;
					}
			}

		}

	//create/Malloc Bird fly
		if (birdFlyTimeInterval==GL_TRUE)
		{
			//Bird Fly Node Data Preparation
				NewBirdFlyNode=(pBirdFlyNode)malloc(sizeof(BirdFlyNode));

				if(NewBirdFlyNode==NULL)
				{
					fprintf(gpFile, "\nFailed to create Bird Fly Node...!");
					DestroyWindow(ghwnd);
				}

				if(fBirdFlyXPositionScale*rand()>0.5f)
				{
					NewBirdFlyNode->data.x=WidthStart-0.25f;
					NewBirdFlyNode->data.xVel=0.01f;
				}else{
					NewBirdFlyNode->data.x=WidthEnd+0.25f;
					NewBirdFlyNode->data.xVel=-0.01f;
				}
				//min+rand()/(RAND_MAX/(max-min))
				NewBirdFlyNode->data.y= (HeightEnd/2.0f)+ rand() / (RAND_MAX /(HeightEnd-(HeightEnd/2.0f)));
				NewBirdFlyNode->data.yVel=0;
				NewBirdFlyNode->data.removeBird=GL_FALSE;

				NewBirdFlyNode->data.stoneDroppedYet=GL_FALSE;
				NewBirdFlyNode->data.stoneDropX= (WidthStart + 1.0f) + rand() / (RAND_MAX/((WidthEnd-1.0f)-(WidthStart + 1.0f)));

				NewBirdFlyNode->next=NULL;

				//Bird Fly Node Data pushed into BirdFlyListQueue
					if(BirdFlyListQueue->rear!=NULL)
					{
						BirdFlyListQueue->rear->next=NewBirdFlyNode;
					}

					BirdFlyListQueue->rear=NewBirdFlyNode;

					if(BirdFlyListQueue->front==NULL)
					{
						BirdFlyListQueue->front= BirdFlyListQueue->rear;
					}
		}
}


void RemoveQueueData(void)
{	
	void DeallocateAll_DS_Pointers(void);

	pShotNode tempShotNode=NULL,deleteShotNode=NULL,previousShotNode=NULL;
	pBirdFlyNode tempBirdFlyNode=NULL,deleteBirdFlyNode=NULL,previousBirdFlyNode=NULL;
	pStoneFallingLLNode tempStoneFallingNode=NULL,deleteStoneFallingNode=NULL,previousStoneFallingNode=NULL;

	if(isStartEnable==GL_FALSE)
	{
		DeallocateAll_DS_Pointers();
	}else{

		//Removing Data From Queues
			//ShotListQueue
				tempShotNode=ShotListQueue->front;
				while(tempShotNode!=NULL)
				{
					// fprintf(gpFile, "\nRemove Queue Data->Shot Data:-ShotX:%f,ShotY:%f",tempShotNode->data.x ,tempShotNode->data.y);
					// fflush(gpFile);

					if(tempShotNode->data.removeShot==GL_TRUE)
					{
						if((tempShotNode==ShotListQueue->front)&&(tempShotNode==ShotListQueue->rear))
						{
							deleteShotNode=tempShotNode;
							free(deleteShotNode);

							deleteShotNode=NULL;
							tempShotNode=NULL;
							
							ShotListQueue->front=NULL;
							ShotListQueue->rear=NULL;
							continue;
						}
						else if (tempShotNode==ShotListQueue->front)
						{	
		
							deleteShotNode=tempShotNode;
							ShotListQueue->front=tempShotNode->next;
							tempShotNode=tempShotNode->next;
							free(deleteShotNode);
							deleteShotNode=NULL;

						}
						else if (tempShotNode==ShotListQueue->rear)
						{	
							if(previousShotNode!=NULL)
							{
								deleteShotNode=tempShotNode;
								previousShotNode->next=NULL;
								ShotListQueue->rear=previousShotNode;
								free(deleteShotNode);
								deleteShotNode=NULL;
								tempShotNode=NULL;
								continue;
							}
						}
						else
						{
							if(previousShotNode!=NULL)
							{
								deleteShotNode=tempShotNode;
								previousShotNode->next=deleteShotNode->next;
								free(deleteShotNode);
								deleteShotNode=NULL;
								
								tempShotNode=previousShotNode;
							}
						}
					}
					previousShotNode=tempShotNode;
					tempShotNode=tempShotNode->next;
				}
			//BirdFlyListQueue
				tempBirdFlyNode=BirdFlyListQueue->front;
				while(tempBirdFlyNode!=NULL)
				{	
					// fprintf(gpFile, "\nRemove Queue Data->Bird Fly Data:-BirdX:%f,BirdY:%f",tempBirdFlyNode->data.x ,tempBirdFlyNode->data.y);
					// fflush(gpFile);

					if(tempBirdFlyNode->data.removeBird==GL_TRUE)
					{

						if((tempBirdFlyNode==BirdFlyListQueue->front)&&(tempBirdFlyNode==BirdFlyListQueue->rear))
						{
							deleteBirdFlyNode=tempBirdFlyNode;
							free(deleteBirdFlyNode);
							deleteBirdFlyNode=NULL;
							tempBirdFlyNode=NULL;
							
							BirdFlyListQueue->front=NULL;
							BirdFlyListQueue->rear=NULL;
							continue;
						}
						else if (tempBirdFlyNode==BirdFlyListQueue->front)
						{	
							deleteBirdFlyNode=tempBirdFlyNode;
							BirdFlyListQueue->front=tempBirdFlyNode->next;
							tempBirdFlyNode=tempBirdFlyNode->next;
							free(deleteBirdFlyNode);
							deleteBirdFlyNode=NULL;
						}
						else if (tempBirdFlyNode==BirdFlyListQueue->rear)
						{	
							if(previousBirdFlyNode!=NULL)
							{
								deleteBirdFlyNode=tempBirdFlyNode;
								previousBirdFlyNode->next=NULL;
								BirdFlyListQueue->rear=previousBirdFlyNode;
								free(deleteBirdFlyNode);
								deleteBirdFlyNode=NULL;
								tempBirdFlyNode=NULL;
								continue;
							}
						}
						else
						{
							if(previousBirdFlyNode!=NULL)
							{
								deleteBirdFlyNode=tempBirdFlyNode;
								previousBirdFlyNode->next=deleteBirdFlyNode->next;
								free(deleteBirdFlyNode);
								deleteBirdFlyNode=NULL;
								
								tempBirdFlyNode=previousBirdFlyNode;
							}
						}

					}
					previousBirdFlyNode=tempBirdFlyNode;
					tempBirdFlyNode=tempBirdFlyNode->next;
				}

			//StoneFallingListQueue
				tempStoneFallingNode=NULL,deleteStoneFallingNode=NULL,previousStoneFallingNode=NULL;

				tempStoneFallingNode=StoneFallingListQueue->front;
				while(tempStoneFallingNode!=NULL)
				{
					// fprintf(gpFile, "\nRemove Queue Data->Stone Falling Data:-BirdX:%f,BirdY:%f",tempStoneFallingNode->data.x ,tempStoneFallingNode->data.y);
					// fflush(gpFile);

					if(tempStoneFallingNode->data.removeStone==GL_TRUE)
					{
						if((tempStoneFallingNode==StoneFallingListQueue->front)&&(tempStoneFallingNode==StoneFallingListQueue->rear))
						{
							deleteStoneFallingNode=tempStoneFallingNode;
							free(deleteStoneFallingNode);
							deleteStoneFallingNode=NULL;
							tempStoneFallingNode=NULL;
							
							StoneFallingListQueue->front=NULL;
							StoneFallingListQueue->rear=NULL;
							continue;
						}
						else if (tempStoneFallingNode==StoneFallingListQueue->front)
						{	
							deleteStoneFallingNode=tempStoneFallingNode;
							StoneFallingListQueue->front=tempStoneFallingNode->next;
							tempStoneFallingNode=tempStoneFallingNode->next;
							free(deleteStoneFallingNode);
							deleteStoneFallingNode=NULL;
						}
						else if (tempStoneFallingNode==StoneFallingListQueue->rear)
						{	
							if(previousStoneFallingNode!=NULL)
							{
								deleteStoneFallingNode=tempStoneFallingNode;
								previousStoneFallingNode->next=NULL;
								StoneFallingListQueue->rear=previousStoneFallingNode;
								free(deleteStoneFallingNode);
								deleteStoneFallingNode=NULL;
								tempStoneFallingNode=NULL;
								continue;
							}
						}
						else
						{
							if(previousStoneFallingNode!=NULL)
							{
								deleteStoneFallingNode=tempStoneFallingNode;
								previousStoneFallingNode->next=deleteStoneFallingNode->next;
								free(deleteStoneFallingNode);
								deleteStoneFallingNode=NULL;
								
								tempStoneFallingNode=previousStoneFallingNode;
							}
						}

					}
					previousStoneFallingNode=tempStoneFallingNode;
					tempStoneFallingNode=tempStoneFallingNode->next;
				}	
		//	
	}
	
}

//GLU Shape Start
	void RenderGLUSphere(GLUquadric *qobj,/*The quadric object*/
	GLdouble   radius,/*radius of sphere*/
	GLint      slices,/*horizontal//longitude*/
	GLint      stacks)/*vertical//latitude*/
	{
		gluSphere(qobj,radius,slices,stacks);
	} 

	void RenderGLUCylinder( GLUquadric *qobj,//The quadric object
	GLdouble   baseRadius,//Radius of bottom/base of cylinder
	GLdouble   topRadius,//Radius of top of cylinder//if 0 then Cone
	GLdouble   height,//cylinder height
	GLint      slices,//horizontal//longitude
	GLint      stacks)//vertical//latitude
	{
		gluCylinder(qobj,baseRadius,topRadius,height,slices,stacks);
	} 


	void RenderGLUDisk(GLUquadric *qobj,//The quadric object
	GLdouble   innerRadius,// inner radius of the disk
	GLdouble   outerRadius,//outer radius of the disk.
	GLint      slices,//horizontal//longitude
	GLint      loops)//number of concentric rings
	{
		gluDisk(qobj,innerRadius,outerRadius,slices,loops);
	} 

	void RenderGLUPartialDisk(GLUquadric *qobj,//The quadric object
	GLdouble   innerRadius,// inner radius of the disk
	GLdouble   outerRadius,//outer radius of the disk
	GLint      slices,//horizontal//longitude
	GLint      loops,//number of concentric rings
	GLdouble   startAngle,//starting arc angle, in degrees, of the disk portion
	GLdouble   sweepAngle//ending arc angle, in degrees, of the disk portion
	)
	{
		gluPartialDisk(qobj,innerRadius,outerRadius,slices,loops,startAngle,sweepAngle);
	} 
//GLU Shape End

//Common DS Part	
	pQueueShotListNode CreateShotListQueue(void)
	{
		pQueueShotListNode NewQueue=NULL;

		NewQueue=(pQueueShotListNode)malloc(sizeof(QNode));

		if(NewQueue==NULL)
		{
			fprintf(gpFile, "\nFailed to create Shot queue...!");
			DestroyWindow(ghwnd);
		}

		NewQueue->front=NewQueue->rear=NULL;

		return NewQueue;
	}

	pQueueBirdFlyListNode CreateBirdFlyListQueue(void)
	{
		pQueueBirdFlyListNode NewQueue=NULL;

		NewQueue=(pQueueBirdFlyListNode)malloc(sizeof(QBirdFlyNode));

		if(NewQueue==NULL)
		{
			fprintf(gpFile, "\nFailed to create Shot queue...!");
			DestroyWindow(ghwnd);
		}

		NewQueue->front=NewQueue->rear=NULL;

		return NewQueue;
	}

	pStoneFallingQueueNode CreateStoneFallingListQueue(void)
	{
		pStoneFallingQueueNode NewQueue=NULL;

		NewQueue=(pStoneFallingQueueNode)malloc(sizeof(StoneFallingQueueNode));

		if(NewQueue==NULL)
		{
			fprintf(gpFile, "\nFailed to create Stone Falling queue...!");
			DestroyWindow(ghwnd);
		}

		NewQueue->front=NewQueue->rear=NULL;

		return NewQueue;
	}

//

//Geometry Model Start

	void DrawCube(void)
	{
		glBegin(GL_QUADS);
		//front
			glNormal3f(0.0f,0.0f,1.0f);
				glVertex3f(1.0f, 1.0f, 1.0f);
				glVertex3f(-1.0f, 1.0f, 1.0f);
				glVertex3f(-1.0f, -1.0f, 1.0f);
				glVertex3f(1.0f, -1.0f, 1.0f);

			//Right
			glNormal3f(1.0f,0.0f,0.0f);
				glVertex3f(1.0f, 1.0f, -1.0f);
				glVertex3f(1.0f, 1.0f, 1.0f);
				glVertex3f(1.0f, -1.0f, 1.0f);
				glVertex3f(1.0f, -1.0f, -1.0f);

			//Back
			glNormal3f(0.0f,0.0f,-1.0f);
				glVertex3f(-1.0f, 1.0f, -1.0f);
				glVertex3f(1.0f, 1.0f, -1.0f);
				glVertex3f(1.0f, -1.0f, -1.0f);
				glVertex3f(-1.0f, -1.0f, -1.0f);
		
			//left
			glNormal3f(-1.0f,0.0f,0.0f);
				glVertex3f(-1.0f, 1.0f, 1.0f);
				glVertex3f(-1.0f, 1.0f, -1.0f);
				glVertex3f(-1.0f, -1.0f, -1.0f);
				glVertex3f(-1.0f, -1.0f, 1.0f);
			
			//top
			glNormal3f(0.0f,1.0f,0.0f);
				glVertex3f(1.0f, 1.0f, -1.0f);
				glVertex3f(-1.0f, 1.0f, -1.0f);
				glVertex3f(-1.0f, 1.0f, 1.0f);
				glVertex3f(1.0f, 1.0f, 1.0f);

			//Bottom
			glNormal3f(0.0f,-1.0f,0.0f);
				glVertex3f(1.0f, -1.0f, -1.0f);
				glVertex3f(-1.0f, -1.0f, -1.0f);
				glVertex3f(-1.0f, -1.0f, 1.0f);
				glVertex3f(1.0f, -1.0f, 1.0f);

		glEnd();
	}

	void BirdWing(void)
	{
		glBegin(GL_TRIANGLES);
			glNormal3f(1.0f, 1.0f, 0.0f);
			glVertex3f(1.0f, 1.0f, 0.0f);
			glNormal3f(-1.0f, -1.0f, 0.0f);
			glVertex3f(-1.0f, -1.0f, 0.0f);
			glNormal3f(1.0f, -1.0f, 0.0f);
			glVertex3f(1.0f, -1.0f, 0.0f);
		glEnd();
	}

	void RifleAim(void)
	{
		glBegin(GL_TRIANGLES);
			glNormal3f(1.0f, 1.0f, 1.0f);
			glVertex3f(1.0f, 1.0f, 0.0f);
			glNormal3f(1.0f, 1.0f, 1.0f);
			glVertex3f(-1.0f, -1.0f, 0.0f);
			glNormal3f(1.0f, 1.0f, 1.0f);
			glVertex3f(1.0f, -1.0f, 0.0f);
		glEnd();
	}

	void BirdFly(void)
	{
		//GLU Wrapper
		void RenderGLUSphere(GLUquadric *,GLdouble,GLint,GLint);
		void RenderGLUCylinder( GLUquadric *,GLdouble ,GLdouble ,GLdouble ,GLint ,GLint);
		void RenderGLUDisk(GLUquadric *,GLdouble ,GLdouble ,GLint ,GLint);
		void RenderGLUPartialDisk(GLUquadric *,GLdouble ,GLdouble ,GLint ,GLint ,GLdouble ,GLdouble);

		//Geometry
		void BirdWing(void);
		GLfloat GreenColor[]={97.0f/255.0f,179.0f/255.0f,59.0f/255.0f};
		GLfloat RedColor[]={174.0f/255.0f,3.0f/255.0f,21.0f/255.0f};
		GLfloat EyeColor[]={0.0f,0.0f,0.0f};
		GLfloat LegColor[]={92.0f/255.0f,79.0f/255.0f,73.0f/255.0f};

		glPushMatrix();
			//Head
				glPushMatrix();
					glTranslatef(-3.5f,3.2f,0.0f);
					glPushMatrix();
						glRotatef(90.0f,1.0f,0.0f,0.0f);
							glCallList(BodyBirdMaterial);
							RenderGLUSphere(SphereQuadric,1.3f,20,20);
					glPopMatrix();
					//Mouth ie Choch
						glPushMatrix();
							glTranslatef(-1.0f,0.0f,0.0f);
							glRotatef(90.0f,0.0f,-1.0f,0.0f);	
							glCallList(MouthBirdMaterial);
							RenderGLUCylinder(CylinderQuadric,0.5f,0.0f,1.5f,15,15);
						glPopMatrix();
					//right eye
						glPushMatrix();
							glTranslatef(0.0f,0.0f,1.3f);
							glCallList(EyeBirdMaterial);
							RenderGLUDisk(DiskQuadric,0.0f,0.2f,20,10);
						glPopMatrix();
					//left eye
						glPushMatrix();
							glTranslatef(0.0f,0.0f,-1.3f);
							glCallList(EyeBirdMaterial);
							RenderGLUDisk(DiskQuadric,0.0f,0.2f,20,10);
						glPopMatrix();
				glPopMatrix();

			//Chest
				glPushMatrix();
					glCallList(BodyBirdMaterial);
					glRotatef(-30.0f,0.0f,0.0f,1.0f);
					glScalef(2.0f,1.0f,1.0f);
					glRotatef(90.0f,1.0f,0.0f,0.0f);
					RenderGLUSphere(SphereQuadric,2.0f,20,20);
				glPopMatrix();
			
			//Right Leg
				glPushMatrix();
					glTranslatef(0.0f,-2.0f,-0.3f);
					glPushMatrix();
						glRotatef(90.0f,1.0f,0.0f,0.0f);
							glCallList(LegBirdMaterial);
							RenderGLUCylinder(CylinderQuadric,0.2f,0.1f,1.0f,20,20);
					glPopMatrix();

					glTranslatef(-0.1f,-1.0f,0.0f);

					glPushMatrix();
						glScalef(3.0f,1.0f,2.5f);
							glCallList(FootBirdMaterial);
							RenderGLUSphere(SphereQuadric,0.1f,10,10);
					glPopMatrix();
				glPopMatrix();

			//Left Leg
				glPushMatrix();
					glTranslatef(0.0f,-2.0f,0.3f);
					glPushMatrix();
						glRotatef(90.0f,1.0f,0.0f,0.0f);
							glCallList(LegBirdMaterial);
							RenderGLUCylinder(CylinderQuadric,0.2f,0.1f,1.0f,20,20);
					glPopMatrix();

					glTranslatef(-0.1f,-1.0f,0.0f);

					glPushMatrix();
						glScalef(3.0f,1.0f,2.5f);
						glCallList(FootBirdMaterial);
						RenderGLUSphere(SphereQuadric,0.1f,10,10);
					glPopMatrix();
				glPopMatrix();

			//Tail
				glPushMatrix();
					glTranslatef(2.75f,-1.5f,0.0f);
					glRotatef(-20.0f,0.0f,0.0f,1.0f);
					glScalef(1.0f,1.0f,1.5f);
					glRotatef(90.0f,0.0f,1.0f,0.0f);
					glCallList(BodyBirdMaterial);
					RenderGLUCylinder(CylinderQuadric,1.0f,0.0f,7.0f,20,20);
				glPopMatrix();

			//Right Wing
				glPushMatrix();
					glTranslatef(0.0f,0.0f,1.5f);
					glRotatef(WingRotate,1.0f,0.0f,0.0f);
						RenderGLUSphere(SphereQuadric,0.5f,20,20);
					glRotatef(90.0f,1.0f,0.0f,0.0f);
					glPushMatrix();
						glRotatef(180.0f,0.0f,-1.0f,0.0f);
						glTranslatef(0.0f,4.0f,0.0f);
						glScalef(2.0f,4.0f,1.0f);
						glCallList(BodyBirdMaterial);
						BirdWing();
					glPopMatrix();
				glPopMatrix();

			//Left Wing
				glPushMatrix();
					glTranslatef(0.0f,0.0f,-1.5f);
					glRotatef(WingRotate,-1.0f,0.0f,0.0f);
						RenderGLUSphere(SphereQuadric,0.5f,20,20);
					
					glRotatef(90.0f,-1.0f,0.0f,0.0f);
					
					glPushMatrix();
						glRotatef(180.0f,0.0f,1.0f,0.0f);
						glTranslatef(0.0f,4.0f,0.0f);
						glScalef(2.0f,4.0f,1.0f);
						glCallList(BodyBirdMaterial);
						BirdWing();
					glPopMatrix();

				glPopMatrix();
		glPopMatrix();
	}

	void HumanBody(void)
	{
		//GLU Wrapper
		void RenderGLUSphere(GLUquadric *,GLdouble,GLint,GLint);
		void RenderGLUCylinder( GLUquadric *,GLdouble ,GLdouble ,GLdouble ,GLint ,GLint);
		void RenderGLUDisk(GLUquadric *,GLdouble ,GLdouble ,GLint ,GLint);
		void RenderGLUPartialDisk(GLUquadric *,GLdouble ,GLdouble ,GLint ,GLint ,GLdouble ,GLdouble);
		void DrawCube(void);
		void RifleModel(void);


		glPushMatrix();
			//Head
				glPushMatrix();
					glTranslatef(0.0f,5.75f,0.0f);
					glRotatef(90.0f,1.0f,0.0f,0.0f);
						glCallList(SkinHumanMaterial);
						RenderGLUSphere(SphereQuadric,0.85f,20,20);
				glPopMatrix();
			//Neck
				glPushMatrix();
					glTranslatef(0.0f,5.0f,0.0f);
					glRotatef(90.0f,1.0f,0.0f,0.0f);
						glCallList(CylinderHumanMaterial);
						RenderGLUCylinder(CylinderQuadric,0.45f,0.45f,0.7f,15,15);
				glPopMatrix();
			//Chest
				glPushMatrix();
					glPushMatrix();
						glTranslatef(0.0f,3.0f,0.0f);
						glScalef(2.25f,1.5f,1.25f);				
							glCallList(ChestHumanMaterial);
							RenderGLUSphere(SphereQuadric,1.0f,9,9);
					glPopMatrix();
					
					glPushMatrix();
						glTranslatef(0.0f,2.0f,0.0f);
						glScalef(2.125f,3.0f,1.125f);
						// DrawCube();
							glCallList(ChestHumanMaterial);
							RenderGLUSphere(SphereQuadric,1.0f,5,5);
					glPopMatrix();
				glPopMatrix();
			
			
			//Right Hand
				glPushMatrix();
					glTranslatef(2.25f,3.5f,0.0f);
					//Shoulder
						glColor3f(1.0f,1.0f,1.0f);//TO DO:[Change Color]
						glRotatef(-90.0f,1.0f,0.0f,0.0f);
						glRotatef(RightHandShoulderRotation,1.0f,0.0f,0.0f);	//TO DO:[Change Angle]
							glCallList(JointSphereHumanMaterial);
							RenderGLUSphere(SphereQuadric,0.5f,10,10);
					//Bicep
						glPushMatrix();
							glRotatef(-90.0f,1.0f,0.0f,0.0f);//TO DO:[Change Angle]	
								glCallList(CylinderHumanMaterial);
								RenderGLUCylinder(CylinderQuadric,0.5f,0.4f,2.0f,15,15);
						glPopMatrix();

					//Forearm Rotation
						glTranslatef(0.0f,2.0f,0.0f);
						glRotatef(RightHandForeArmRotation,1.0f,0.0f,0.0f);	//TO DO:[Change Angle]
							glCallList(JointSphereHumanMaterial);
							RenderGLUSphere(SphereQuadric,0.4f,10,10);

					//Forearm
						glPushMatrix();
							glRotatef(-90.0f,1.0f,0.0f,0.0f);//TO DO:[Change Angle]	
								glCallList(CylinderHumanMaterial);
								RenderGLUCylinder(CylinderQuadric,0.4f,0.3f,1.75f,10,10);
						glPopMatrix();
					//Rifle Holding+Rotating
						glTranslatef(0.0f,1.75f,0.0f);
						glRotatef(-90.0f,0.0f,1.0f,0.0f);
						glRotatef(FiringAngle,0.0f,1.0f,0.0f);
							glCallList(JointSphereHumanMaterial);
							RenderGLUSphere(SphereQuadric,0.275f,10,10);
					//Rifle
						glPushMatrix();
							glTranslatef(0.0f,0.0f,3.0f);
							glRotatef(90.0f,1.0f,0.0f,0.0f);
							glRotatef(180.0f,0.0f,1.0f,0.0f);		
							glScalef(0.5,0.5,0.5);
							RifleModel();
						glPopMatrix();
				glPopMatrix();

			//Left Hand
				glPushMatrix();
					glTranslatef(-2.25f,3.5f,0.0f);
					//Shoulder
						glColor3f(1.0f,1.0f,1.0f);//TO DO:[Change Color]
						glRotatef(-90.0f,1.0f,0.0f,0.0f);
						glRotatef(LeftHandShoulderRotation,1.0f,0.0f,0.0f);	//TO DO:[Change Angle]
							glCallList(JointSphereHumanMaterial);
							RenderGLUSphere(SphereQuadric,0.5f,10,10);
					//Bicep
						glPushMatrix();
							glRotatef(-90.0f,1.0f,0.0f,0.0f);//TO DO:[Change Angle]	
								glCallList(CylinderHumanMaterial);
								RenderGLUCylinder(CylinderQuadric,0.5f,0.4f,2.0f,15,15);
						glPopMatrix();
						
					//Forearm Rotation
						glTranslatef(0.0f,2.0f,0.0f);
						glRotatef(LeftHandForeArmRotation,1.0f,0.0f,0.0f);	//TO DO:[Change Angle]
							glCallList(JointSphereHumanMaterial);
							RenderGLUSphere(SphereQuadric,0.4f,10,10);
					//Forearm
						glPushMatrix();
							glRotatef(-90.0f,1.0f,0.0f,0.0f);//TO DO:[Change Angle]	
								glCallList(CylinderHumanMaterial);
								RenderGLUCylinder(CylinderQuadric,0.4f,0.3f,1.75f,10,10);
						glPopMatrix();
				glPopMatrix();
			
			//Heap
				glPushMatrix();
					glTranslatef(0.0f,0.25f,0.0f);
					glRotatef(90.0f,1.0f,0.0f,0.0f);//TO DO:[Change Angle]
					glScalef(2.5f,1.5f,1.0f);
						glCallList(ChestHumanMaterial);
						RenderGLUCylinder(CylinderQuadric,0.6f,0.65f,1.25f,11,11);
				glPopMatrix();

			//Right Leg
				glPushMatrix();
					glTranslatef(0.75f,-1.0f,0.0f);
					//Thai Rotation
						glRotatef(-90.0f,1.0f,0.0f,0.0f);
						glRotatef(RightLegThaiRotation,1.0f,0.0f,0.0f);	//TO DO:[Change Angle]
							glCallList(JointSphereHumanMaterial);
							RenderGLUSphere(SphereQuadric,0.7f,10,10);

					//Thai
						glPushMatrix();
							glRotatef(-90.0f,1.0f,0.0f,0.0f);//TO DO:[Change Angle]	
								glCallList(HeadHumanMaterial);
								RenderGLUCylinder(CylinderQuadric,0.7f,0.5f,2.0f,15,15);
						glPopMatrix();

					//Calf Rotation
						glTranslatef(0.0f,2.0f,0.0f);
						glRotatef(RightLegCalfRotation,1.0f,0.0f,0.0f);	//TO DO:[Change Angle]
							glCallList(JointSphereHumanMaterial);
							RenderGLUSphere(SphereQuadric,0.5f,10,10);
						
					//Calf
						glPushMatrix();
							glRotatef(-90.0f,1.0f,0.0f,0.0f);//TO DO:[Change Angle]	
								glCallList(HeadHumanMaterial);
								RenderGLUCylinder(CylinderQuadric,0.5f,0.3f,2.0f,10,10);
						glPopMatrix();

					//foot
						glTranslatef(0.0f,2.0f,0.0f);
						glCallList(JointSphereHumanMaterial);
						RenderGLUSphere(SphereQuadric,0.3f,10,10);
									
						glTranslatef(0.0f,0.5f,0.25f);
						glPushMatrix();
							glScalef(1.5f,1.0f,2.0f);
								glCallList(HeadHumanMaterial);
								RenderGLUSphere(SphereQuadric,0.3f,10,10);
						glPopMatrix();

				glPopMatrix();

			//Left Leg
				glPushMatrix();
					glTranslatef(-0.75f,-1.0f,0.0f);
					//Thai Rotation
						glColor3f(1.0f,1.0f,1.0f);//TO DO:[Change Color]
						glRotatef(-90.0f,1.0f,0.0f,0.0f);
						glRotatef(LeftLegThaiRotation,1.0f,0.0f,0.0f);	//TO DO:[Change Angle]
							glCallList(JointSphereHumanMaterial);
							RenderGLUSphere(SphereQuadric,0.7f,10,10);
					
					//Thai
						glPushMatrix();
							glRotatef(-90.0f,1.0f,0.0f,0.0f);//TO DO:[Change Angle]	
								glCallList(HeadHumanMaterial);
								RenderGLUCylinder(CylinderQuadric,0.7f,0.5f,2.0f,15,15);
						glPopMatrix();

					//Calf Rotation
						glTranslatef(0.0f,2.0f,0.0f);
						glRotatef(LeftLegCalfRotation,1.0f,0.0f,0.0f);	//TO DO:[Change Angle]
							glCallList(JointSphereHumanMaterial);
							RenderGLUSphere(SphereQuadric,0.5f,10,10);

					//Calf
						glPushMatrix();
							glRotatef(-90.0f,1.0f,0.0f,0.0f);//TO DO:[Change Angle]	
								glCallList(HeadHumanMaterial);
								RenderGLUCylinder(CylinderQuadric,0.5f,0.3f,2.0f,10,10);
						glPopMatrix();

					//foot
						glTranslatef(0.0f,2.0f,0.0f);
							glCallList(JointSphereHumanMaterial);
							RenderGLUSphere(SphereQuadric,0.3f,10,10);
									
						glTranslatef(0.0f,0.5f,0.25f);
						glPushMatrix();
							glScalef(1.5f,1.0f,2.0f);
							glCallList(HeadHumanMaterial);
							RenderGLUSphere(SphereQuadric,0.3f,10,10);
						glPopMatrix();

				glPopMatrix();


		glPopMatrix();
	}

	
	void RifleModel(void)
	{
		//GLU Wrapper
			void RenderGLUSphere(GLUquadric *,GLdouble,GLint,GLint);
			void RenderGLUCylinder( GLUquadric *,GLdouble ,GLdouble ,GLdouble ,GLint ,GLint);
			void RenderGLUDisk(GLUquadric *,GLdouble ,GLdouble ,GLint ,GLint);
			void RenderGLUPartialDisk(GLUquadric *,GLdouble ,GLdouble ,GLint ,GLint ,GLdouble ,GLdouble);

		//Geometry
			void DrawCube(void);
			void RifleAim(void);

		//Entire Rifle Model
			glPushMatrix();
				//Silencer
					glPushMatrix();
						glTranslatef(0.0f,2.5f,0.0f);
						glRotatef(-90.0f,1.0f,0.0f,0.0f);	
							materialAmbient[0]=0.19225f;
							materialAmbient[1]=0.19225f;
							materialAmbient[2]=0.19225f;
							materialAmbient[3]=1.0f;
							glMaterialfv(GL_FRONT,GL_AMBIENT,materialAmbient);

							materilaDefuse[0]=0.50754f;
							materilaDefuse[1]=0.50754f;
							materilaDefuse[2]=0.50754f;
							materilaDefuse[3]=1.0f;
							glMaterialfv(GL_FRONT,GL_DIFFUSE,materilaDefuse);
							if(fireShotKeyPressed){
								materialEmission[0]=0.8;
								materialEmission[1]=0.0;
								materialEmission[2]=0.0;
								materialEmission[3]=1.0f;
								glMaterialfv(GL_FRONT,GL_EMISSION,materialEmission);
							}
						RenderGLUCylinder(CylinderQuadric,1.125f,1.125f,2.0f,20,20);
					glPopMatrix();
					
						materialEmission[0]=0.0;
						materialEmission[1]=0.0;
						materialEmission[2]=0.0;
						materialEmission[3]=1.0f;
						glMaterialfv(GL_FRONT,GL_EMISSION,materialEmission);

				//Main Pipe
					glPushMatrix();
						glTranslatef(0.0f,2.5f,0.0f);
						glRotatef(90.0f,1.0f,0.0f,0.0f);
						glCallList(RifleBody);
						RenderGLUCylinder(CylinderQuadric,1.0f,1.0f,5.0f,20,20);
					glPopMatrix();
				//Body of Rifle
					glTranslatef(0.0f,-4.0f,0.0f);
						glPushMatrix();
							glScalef(1.5,2.0f,1.5f);
							glCallList(RifleBody);
							DrawCube();
						glPopMatrix();
				//End of Rifle
						glPushMatrix();
							glTranslatef(0.0f,-5.0f,0.0f);
								glPushMatrix();
									glScalef(0.5,1.0f,0.5f);
									glRotatef(-90.0f,1.0f,0.0f,0.0f);
									glCallList(RifleBody);
									RenderGLUCylinder(CylinderQuadric,2.0f,1.5f,3.0f,4,4);
								glPopMatrix();
						glPopMatrix();
				//Grip of Rifle
						glPushMatrix();
							glTranslatef(0.0f,0.0f,1.5f);
							glRotatef(90.0f,0.0f,0.0f,1.0f);
							glScalef(1.0,1.0f,1.25f);
							glCallList(RifleGrip);
							RenderGLUCylinder(CylinderQuadric,0.75f,0.75f,2.0f,20,20);
						glPopMatrix();
				//Aim of Rifle
						glPushMatrix();
							glTranslatef(0.0f,0.0f,-2.5f);
							glRotatef(-90.0f,0.0f,1.0f,0.0f);
							glCallList(RifleBody);
							RifleAim();
						glPopMatrix();
			glPopMatrix();
		
	}
//Geometry Model End


//DisplayList Start

	void TreeModelDisplayList(void)
	{
		TreeModel=glGenLists(1);
		glNewList(TreeModel,GL_COMPILE);
			//Entire Tree
				glPushMatrix();
					//Tree Top
						glPushMatrix();
							glTranslatef(0.0f,5.0f,0.0f);
							glRotatef(-90.0f,1.0f,0.0f,0.0f);

							materialAmbient[0]=0.0f;
							materialAmbient[1]=0.0f;
							materialAmbient[2]=0.0f;
							materialAmbient[3]=1.0f;
							glMaterialfv(GL_FRONT,GL_AMBIENT,materialAmbient);

							materilaDefuse[0]=0.1f;
							materilaDefuse[1]=0.35f;
							materilaDefuse[2]=0.1f;
							materilaDefuse[3]=1.0f;
							glMaterialfv(GL_FRONT,GL_DIFFUSE,materilaDefuse);

							materialSpecular[0]=0.45f;
							materialSpecular[1]=0.55f;
							materialSpecular[2]=0.45f;
							materialSpecular[3]=1.0f;
							glMaterialfv(GL_FRONT,GL_SPECULAR,materialSpecular);

							materialShininess=0.25f * 128.0f;
							glMaterialf(GL_FRONT,GL_SHININESS,materialShininess);

							RenderGLUCylinder(CylinderQuadric,5.0f,0.0f,3.0f,20,20);
						glPopMatrix();
					//Tree Middle 1
						glPushMatrix();
							glTranslatef(0.0f,2.5f,0.0f);
							glRotatef(-90.0f,1.0f,0.0f,0.0f);
							RenderGLUCylinder(CylinderQuadric,5.0f,1.5f,3.0f,20,20);
						glPopMatrix();

					//Tree Middle 2
						glPushMatrix();
							glRotatef(-90.0f,1.0f,0.0f,0.0f);
							RenderGLUCylinder(CylinderQuadric,5.0f,1.5f,3.0f,20,20);
						glPopMatrix();
				
					//Tree Base
						glPushMatrix();
							glRotatef(90.0f,1.0f,0.0f,0.0f);
							
							materialAmbient[0]=0.2125f;
							materialAmbient[1]=0.1275f;
							materialAmbient[2]=0.054f;
							materialAmbient[3]=1.0f;
							glMaterialfv(GL_FRONT,GL_AMBIENT,materialAmbient);

							materilaDefuse[0]=0.714f;
							materilaDefuse[1]=0.4284f;
							materilaDefuse[2]=0.18144f;
							materilaDefuse[3]=1.0f;
							glMaterialfv(GL_FRONT,GL_DIFFUSE,materilaDefuse);

							materialSpecular[0]=0.393548f;
							materialSpecular[1]=0.271906f;
							materialSpecular[2]=0.166721f;
							materialSpecular[3]=1.0f;
							glMaterialfv(GL_FRONT,GL_SPECULAR,materialSpecular);

							materialShininess=0.2f * 128.0f;
							glMaterialf(GL_FRONT,GL_SHININESS,materialShininess);

							RenderGLUCylinder(CylinderQuadric,1.0f,1.0f,5.0f,20,20);
						glPopMatrix();
				glPopMatrix();
		glEndList();	
	}

	void BulletModelDisplayList(void)
	{
		BulletModel=glGenLists(1);
		glNewList(BulletModel,GL_COMPILE);
			//Entire Bullet Model
				glPushMatrix();
				//Top of Bullet
					glPushMatrix();
						materialAmbient[0]=0.1f;
						materialAmbient[1]=0.1f;
						materialAmbient[2]=0.1f;
						materialAmbient[3]=1.0f;
						glMaterialfv(GL_FRONT,GL_AMBIENT,materialAmbient);

						materilaDefuse[0]=183.0f/256.0f;
						materilaDefuse[1]=110.0f/256.0f;
						materilaDefuse[2]=75.0f/256.0f;
						materilaDefuse[3]=1.0f;
						glMaterialfv(GL_FRONT,GL_DIFFUSE,materilaDefuse);

						materialSpecular[0]=183.0f/256.0f;
						materialSpecular[1]=110.0f/256.0f;
						materialSpecular[2]=75.0f/256.0f;
						materialSpecular[3]=1.0f;
						glMaterialfv(GL_FRONT,GL_SPECULAR,materialSpecular);

						materialShininess=128.0f;
						glMaterialf(GL_FRONT,GL_SHININESS,materialShininess);
						RenderGLUSphere(SphereQuadric,1.5f,15,15);
					glPopMatrix();
				//Middle of Bullet
					glPushMatrix();
						glRotatef(90.0f,1.0f,0.0f,0.0f);
							materialAmbient[0]=0.19225f;
							materialAmbient[1]=0.19225f;
							materialAmbient[2]=0.19225f;
							materialAmbient[3]=1.0f;
							glMaterialfv(GL_FRONT,GL_AMBIENT,materialAmbient);

							materilaDefuse[0]=0.50754f;
							materilaDefuse[1]=0.50754f;
							materilaDefuse[2]=0.50754f;
							materilaDefuse[3]=1.0f;
							glMaterialfv(GL_FRONT,GL_DIFFUSE,materilaDefuse);

							materialSpecular[0]=0.508273f;
							materialSpecular[1]=0.508273f;
							materialSpecular[2]=0.508273f;
							materialSpecular[3]=1.0f;
							glMaterialfv(GL_FRONT,GL_SPECULAR,materialSpecular);

							materialShininess=0.4f * 128.0f;
							glMaterialf(GL_FRONT,GL_SHININESS,materialShininess);

							RenderGLUCylinder(CylinderQuadric,1.5f,1.5f,3.0f,20,20);
					glPopMatrix();
				//Base of Bullet
					glPushMatrix();
						glTranslatef(0.0f,-3.0f,0.0f);
						glRotatef(90.0f,1.0f,0.0f,0.0f);
							materialAmbient[0]=0.19225f;
							materialAmbient[1]=0.19225f;
							materialAmbient[2]=0.19225f;
							materialAmbient[3]=1.0f;
							glMaterialfv(GL_FRONT,GL_AMBIENT,materialAmbient);

							materilaDefuse[0]=0.50754f;
							materilaDefuse[1]=0.50754f;
							materilaDefuse[2]=0.50754f;
							materilaDefuse[3]=1.0f;
							glMaterialfv(GL_FRONT,GL_DIFFUSE,materilaDefuse);

							materialSpecular[0]=0.508273f;
							materialSpecular[1]=0.508273f;
							materialSpecular[2]=0.508273f;
							materialSpecular[3]=1.0f;
							glMaterialfv(GL_FRONT,GL_SPECULAR,materialSpecular);

							materialShininess=0.4f * 128.0f;
							glMaterialf(GL_FRONT,GL_SHININESS,materialShininess);
							RenderGLUDisk(DiskQuadric,0.5f,1.5f,20,20);
					glPopMatrix();
				glPopMatrix();
		glEndList();	
	}


	void HumanMaterialDisplayList(void)
	{	
		HeadHumanMaterial=glGenLists(1);
		glNewList(HeadHumanMaterial,GL_COMPILE);
			materialAmbient[0]=0.19225f;
			materialAmbient[1]=0.19225f;
			materialAmbient[2]=0.19225f;
			materialAmbient[3]=1.0f;
			glMaterialfv(GL_FRONT,GL_AMBIENT,materialAmbient);

			materilaDefuse[0]=0.50754f;
			materilaDefuse[1]=0.50754f;
			materilaDefuse[2]=0.50754f;
			materilaDefuse[3]=1.0f;
			glMaterialfv(GL_FRONT,GL_DIFFUSE,materilaDefuse);

			materialSpecular[0]=0.508273f;
			materialSpecular[1]=0.508273f;
			materialSpecular[2]=0.508273f;
			materialSpecular[3]=1.0f;
			glMaterialfv(GL_FRONT,GL_SPECULAR,materialSpecular);

			materialShininess=0.4f * 128.0f;
			glMaterialf(GL_FRONT,GL_SHININESS,materialShininess);	
		glEndList();	


		SkinHumanMaterial=glGenLists(1);
		glNewList(SkinHumanMaterial,GL_COMPILE);
			materialAmbient[0]=0.25f;
			materialAmbient[1]=0.20725f;
			materialAmbient[2]=0.20725f;
			materialAmbient[3]=1.0f;
			glMaterialfv(GL_FRONT,GL_AMBIENT,materialAmbient);

			materilaDefuse[0]=1.0f;
			materilaDefuse[1]=0.829f;
			materilaDefuse[2]=0.829f;
			materilaDefuse[3]=1.0f;
			glMaterialfv(GL_FRONT,GL_DIFFUSE,materilaDefuse);

			materialSpecular[0]=0.296648f;
			materialSpecular[1]=0.296648f;
			materialSpecular[2]=0.296648f;
			materialSpecular[3]=1.0f;
			glMaterialfv(GL_FRONT,GL_SPECULAR,materialSpecular);

			materialShininess=0.088f * 128.0f;
			glMaterialf(GL_FRONT,GL_SHININESS,materialShininess);
		glEndList();	


		JointSphereHumanMaterial=glGenLists(1);
		glNewList(JointSphereHumanMaterial,GL_COMPILE);
			materialAmbient[0]=0.0f;
			materialAmbient[1]=0.1f;
			materialAmbient[2]=0.06f;
			materialAmbient[3]=1.0f;
			glMaterialfv(GL_FRONT,GL_AMBIENT,materialAmbient);

			materilaDefuse[0]=0.0f;
			materilaDefuse[1]=0.50980392f;
			materilaDefuse[2]=0.50980392f;
			materilaDefuse[3]=1.0f;
			glMaterialfv(GL_FRONT,GL_DIFFUSE,materilaDefuse);

			materialSpecular[0]=0.50196078f;
			materialSpecular[1]=0.50196078f;
			materialSpecular[2]=0.50196078f;
			materialSpecular[3]=1.0f;
			glMaterialfv(GL_FRONT,GL_SPECULAR,materialSpecular);

			materialShininess=0.25f * 128.0f;
			glMaterialf(GL_FRONT,GL_SHININESS,materialShininess);
		glEndList();

		CylinderHumanMaterial=glGenLists(1);
		glNewList(CylinderHumanMaterial,GL_COMPILE);
			materialAmbient[0]=0.19125f;
			materialAmbient[1]=0.0735f;
			materialAmbient[2]=0.0225f;
			materialAmbient[3]=1.0f;
			glMaterialfv(GL_FRONT,GL_AMBIENT,materialAmbient);

			materilaDefuse[0]=0.7038f;
			materilaDefuse[1]=0.27048f;
			materilaDefuse[2]=0.0828f;
			materilaDefuse[3]=1.0f;
			glMaterialfv(GL_FRONT,GL_DIFFUSE,materilaDefuse);

			materialSpecular[0]=0.256777f;
			materialSpecular[1]=0.137622f;
			materialSpecular[2]=0.086014f;
			materialSpecular[3]=1.0f;
			glMaterialfv(GL_FRONT,GL_SPECULAR,materialSpecular);

			materialShininess=0.1f * 128.0f;
			glMaterialf(GL_FRONT,GL_SHININESS,materialShininess);
		glEndList();		

		ChestHumanMaterial=glGenLists(1);	
		glNewList(ChestHumanMaterial,GL_COMPILE);
			materialAmbient[0]=0.24725f;
			materialAmbient[1]=0.1995f;
			materialAmbient[2]=0.0745f;
			materialAmbient[3]=1.0f;
			glMaterialfv(GL_FRONT,GL_AMBIENT,materialAmbient);

			materilaDefuse[0]=0.75164f;
			materilaDefuse[1]=0.60648f;
			materilaDefuse[2]=0.22648f;
			materilaDefuse[3]=1.0f;
			glMaterialfv(GL_FRONT,GL_DIFFUSE,materilaDefuse);

			materialSpecular[0]=0.628281f;
			materialSpecular[1]=0.555802f;
			materialSpecular[2]=0.366065f;
			materialSpecular[3]=1.0f;
			glMaterialfv(GL_FRONT,GL_SPECULAR,materialSpecular);

			materialShininess=  128.0f;
			glMaterialf(GL_FRONT,GL_SHININESS,materialShininess);
		glEndList();		

	}


	void BirdMaterialDisplayList(void)
	{	
		MouthBirdMaterial=glGenLists(1);
		glNewList(MouthBirdMaterial,GL_COMPILE);
			materialAmbient[0]=0.1745f;
			materialAmbient[1]=0.01175f;
			materialAmbient[2]=0.01175f;
			materialAmbient[3]=1.0f;
			glMaterialfv(GL_FRONT,GL_AMBIENT,materialAmbient);

			materilaDefuse[0]=0.61424f;
			materilaDefuse[1]=0.04136f;
			materilaDefuse[2]=0.04136f;
			materilaDefuse[3]=1.0f;
			glMaterialfv(GL_FRONT,GL_DIFFUSE,materilaDefuse);

			materialSpecular[0]=0.727811f;
			materialSpecular[1]=0.626959f;
			materialSpecular[2]=0.626959f;
			materialSpecular[3]=1.0f;
			glMaterialfv(GL_FRONT,GL_SPECULAR,materialSpecular);

			materialShininess=0.6f * 128.0f;
			glMaterialf(GL_FRONT,GL_SHININESS,materialShininess);
		glEndList();	

		BodyBirdMaterial=glGenLists(1);
		glNewList(BodyBirdMaterial,GL_COMPILE);
			materialAmbient[0]=0.0215f;
			materialAmbient[1]=0.1745f;
			materialAmbient[2]=0.0215f;
			materialAmbient[3]=1.0f;
			glMaterialfv(GL_FRONT,GL_AMBIENT,materialAmbient);

			materilaDefuse[0]=0.07568f;
			materilaDefuse[1]=0.61424f;
			materilaDefuse[2]=0.07568f;
			materilaDefuse[3]=1.0f;
			glMaterialfv(GL_FRONT,GL_DIFFUSE,materilaDefuse);

			materialSpecular[0]=0.633f;
			materialSpecular[1]=0.727811f;
			materialSpecular[2]=0.633f;
			materialSpecular[3]=1.0f;
			glMaterialfv(GL_FRONT,GL_SPECULAR,materialSpecular);

			materialShininess=0.6f * 128.0f;
			glMaterialf(GL_FRONT,GL_SHININESS,materialShininess);
		glEndList();	

		LegBirdMaterial=glGenLists(1);
		glNewList(LegBirdMaterial,GL_COMPILE);
			materialAmbient[0]=0.2125f;
			materialAmbient[1]=0.1275f;
			materialAmbient[2]=0.054f;
			materialAmbient[3]=1.0f;
			glMaterialfv(GL_FRONT,GL_AMBIENT,materialAmbient);

			materilaDefuse[0]=0.714f;
			materilaDefuse[1]=0.4284f;
			materilaDefuse[2]=0.18144f;
			materilaDefuse[3]=1.0f;
			glMaterialfv(GL_FRONT,GL_DIFFUSE,materilaDefuse);

			materialSpecular[0]=0.393548f;
			materialSpecular[1]=0.271906f;
			materialSpecular[2]=0.166721f;
			materialSpecular[3]=1.0f;
			glMaterialfv(GL_FRONT,GL_SPECULAR,materialSpecular);

			materialShininess=0.2f * 128.0f;
			glMaterialf(GL_FRONT,GL_SHININESS,materialShininess);
		glEndList();	

		FootBirdMaterial=glGenLists(1);
		glNewList(FootBirdMaterial,GL_COMPILE);
			materialAmbient[0]=0.05f;
			materialAmbient[1]=0.05f;
			materialAmbient[2]=0.05f;
			materialAmbient[3]=1.0f;
			glMaterialfv(GL_FRONT,GL_AMBIENT,materialAmbient);

			materilaDefuse[0]=0.5f;
			materilaDefuse[1]=0.5f;
			materilaDefuse[2]=0.5f;
			materilaDefuse[3]=1.0f;
			glMaterialfv(GL_FRONT,GL_DIFFUSE,materilaDefuse);

			materialSpecular[0]=0.7f;
			materialSpecular[1]=0.7f;
			materialSpecular[2]=0.7f;
			materialSpecular[3]=1.0f;
			glMaterialfv(GL_FRONT,GL_SPECULAR,materialSpecular);

			materialShininess=0.078125f * 128.0f;
			glMaterialf(GL_FRONT,GL_SHININESS,materialShininess);
			
		glEndList();	

		EyeBirdMaterial=glGenLists(1);
		glNewList(EyeBirdMaterial,GL_COMPILE);
			materialAmbient[0]=0.0f;
			materialAmbient[1]=0.0f;
			materialAmbient[2]=0.0f;
			materialAmbient[3]=1.0f;
			glMaterialfv(GL_FRONT,GL_AMBIENT,materialAmbient);

			materilaDefuse[0]=0.01f;
			materilaDefuse[1]=0.01f;
			materilaDefuse[2]=0.01f;
			materilaDefuse[3]=1.0f;
			glMaterialfv(GL_FRONT,GL_DIFFUSE,materilaDefuse);

			materialSpecular[0]=0.50f;
			materialSpecular[1]=0.50f;
			materialSpecular[2]=0.50f;
			materialSpecular[3]=1.0f;
			glMaterialfv(GL_FRONT,GL_SPECULAR,materialSpecular);

			materialShininess=0.25f * 128.0f;
			glMaterialf(GL_FRONT,GL_SHININESS,materialShininess);
			
		glEndList();	
	}

	
	void RifleMaterialDisplayList(void)
	{	
		RifleGrip=glGenLists(1);
		glNewList(RifleGrip,GL_COMPILE);
			materialAmbient[0]=0.19225f;
			materialAmbient[1]=0.19225f;
			materialAmbient[2]=0.19225f;
			materialAmbient[3]=1.0f;
			glMaterialfv(GL_FRONT,GL_AMBIENT,materialAmbient);

			materilaDefuse[0]=0.50754f;
			materilaDefuse[1]=0.50754f;
			materilaDefuse[2]=0.50754f;
			materilaDefuse[3]=1.0f;
			glMaterialfv(GL_FRONT,GL_DIFFUSE,materilaDefuse);

		glEndList();	

		RifleBody=glGenLists(1);
		glNewList(RifleBody,GL_COMPILE);
			materialAmbient[0]=0.19225f;
			materialAmbient[1]=0.19225f;
			materialAmbient[2]=0.19225f;
			materialAmbient[3]=1.0f;
			glMaterialfv(GL_FRONT,GL_AMBIENT,materialAmbient);

			materilaDefuse[0]=0.50754f;
			materilaDefuse[1]=0.50754f;
			materilaDefuse[2]=0.50754f;
			materilaDefuse[3]=1.0f;
			glMaterialfv(GL_FRONT,GL_DIFFUSE,materilaDefuse);

			materialSpecular[0]=0.508273f;
			materialSpecular[1]=0.508273f;
			materialSpecular[2]=0.508273f;
			materialSpecular[3]=1.0f;
			glMaterialfv(GL_FRONT,GL_SPECULAR,materialSpecular);

			materialShininess=0.4f * 128.0f;
			glMaterialf(GL_FRONT,GL_SHININESS,materialShininess);

		glEndList();	
	}

//DisplayList End


//Game Screens
	//Actual Game
		void ActualGameScreens(void)
		{
			//GLU Wrapper
				void RenderGLUSphere(GLUquadric *,GLdouble,GLint,GLint);
				void RenderGLUCylinder( GLUquadric *,GLdouble ,GLdouble ,GLdouble ,GLint ,GLint);
				void RenderGLUDisk(GLUquadric *,GLdouble ,GLdouble ,GLint ,GLint);
				void RenderGLUPartialDisk(GLUquadric *,GLdouble ,GLdouble ,GLint ,GLint ,GLdouble ,GLdouble);

			//Geometry
				void DrawCube(void);
				void BirdWing(void);
				void BirdFly(void);
				void HumanBody(void);

			//Others
				void PrintString(unsigned int ,char *);

			//Struct Variable
				pShotNode tempShotNode=NULL;
				pBirdFlyNode tempBirdFlyNode=NULL;
				pStoneFallingLLNode NewStoneFallingNode=NULL;
				pStoneFallingLLNode tempStoneFallingNode=NULL;
			
			//Others		
				char TempStringBuffer[100];
		
					
			glPushMatrix();
				glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);//TODO:[Remove Later]
				//Tree Position On Border
					glPushMatrix();
						//Tree Left
						glPushMatrix();
							glTranslatef(WidthStart,HeightStart+2.0f,0.0f);	
							glScalef(0.4,0.4,0.4);
							glCallList(TreeModel);
						glPopMatrix();
						//Tree Right
						glPushMatrix();
							glTranslatef(WidthEnd,HeightStart+2.0f,0.0f);
							glScalef(0.4,0.4,0.4);
							glCallList(TreeModel);
						glPopMatrix();
					glPopMatrix();

				//Display ScoreBoard
					glPushMatrix();
						glTranslatef(0.0f,0.0f,-25.0f);	

						glPushMatrix();
							glTranslatef(-19.0f,12.0f,0.0f);
							sprintf(TempStringBuffer, "Max Fire Shot Waste Left Count :%d",giMaxShotWasteLeftCount);
							PrintString(FontListBase,TempStringBuffer);
						glPopMatrix();

						glPushMatrix();
							glTranslatef(-20.0f,11.0f,0.0f);	
							sprintf(TempStringBuffer, "Max Bird Alive Left Count :%d",giMaxBirdAliveLeftCount);
							PrintString(FontListBase,TempStringBuffer);
						glPopMatrix();

						glPushMatrix();
							glTranslatef(-18.0f,10.0f,0.0f);	
							sprintf(TempStringBuffer, "Max Stone Dropped Waste Left Count  :%d",giMaxStoneDropWasteCount);
							PrintString(FontListBase,TempStringBuffer);
						glPopMatrix();

						glPushMatrix();
							glTranslatef(-21.0f,8.0f,0.0f);	
							PrintString(FontListBase,"Menu(Resets the Game) :P");
						glPopMatrix();
							
					glPopMatrix();

					
				glTranslatef(0.0f,HeightStart,0.0f);	
				// glRotatef(FiringAngle,0.0f,0.0f,1.0f);//IMP
				glPushMatrix();
						glTranslatef(0.25f,0.0f,0.0f);
						// glRotatef(year,0.0f,1.0f,0.0f);	
						glRotatef(180.0f,0.0,1.0f,0.0f);
						glScalef(0.1f,0.1f,0.1f);
						HumanBody();
				glPopMatrix();

				// glRotatef(FiringAngle,0.0f,0.0f,-1.0f);//IMP

				//Render Shot Firing data
					tempShotNode=NULL;
					tempShotNode=ShotListQueue->front;
					while(tempShotNode!=NULL)
					{	
						// fprintf(gpFile, "\nDisplay->Shot Data:-ShotX:%f,ShotY:%f",tempShotNode->data.x ,tempShotNode->data.y);
						// fflush(gpFile);
						
						glPushMatrix();

							glTranslatef(tempShotNode->data.x,tempShotNode->data.y,0.0f);
							// RenderGLUSphere(SphereQuadric,0.1f,10,10);
							glScalef(0.025,0.025,0.025);
							glRotatef(-90.0f,0.0f,0.0f,1.0f);
							glRotatef(tempShotNode->data.angle,0.0f,0.0f,1.0f);
							glCallList(BulletModel);

							tempShotNode->data.x += 0.05f *cos(tempShotNode->data.angleInRadiant);
							tempShotNode->data.y += 0.05f *sin(tempShotNode->data.angleInRadiant);
						
							if(tempShotNode->data.removeShot==GL_FALSE)
							{	
								//Handling Boundry Check
									if( tempShotNode->data.x > WidthEnd || tempShotNode->data.x < WidthStart || tempShotNode->data.y > (HeightEnd * 2.0f) )
									{
										giMaxShotWasteLeftCount -= 1;
										tempShotNode->data.removeShot=GL_TRUE;
										goto nextShotNode; 
									}

								//Checking if Shot heats the Bird
									tempBirdFlyNode=NULL;
									tempBirdFlyNode=BirdFlyListQueue->front;
									while(tempBirdFlyNode!=NULL)
									{
										// fprintf(gpFile, "\nDisplay->BirdFly Data:-BirdX:%f,BirdY:%f",tempBirdFlyNode->data.x ,tempBirdFlyNode->data.y);
										// fflush(gpFile);

										if(	(tempShotNode->data.y-HeightEnd) 	>  (tempBirdFlyNode->data.y - ShotBirdRangeLength)
											&& (tempShotNode->data.y-HeightEnd) <  (tempBirdFlyNode->data.y + ShotBirdRangeLength)
											&& tempShotNode->data.x 			>  (tempBirdFlyNode->data.x - ShotBirdRangeLength)
											&& tempShotNode->data.x 			<  (tempBirdFlyNode->data.x + ShotBirdRangeLength) 
											&& tempBirdFlyNode->data.removeBird == GL_FALSE)
										{
											// fprintf(gpFile, "\nDisplay->BirdShot and Stone Data:-ShotX:%f,ShotY:%f,BirdX:%f,BirdY:%f,",tempShotNode->data.x ,tempShotNode->data.y,tempBirdFlyNode->data.x,tempBirdFlyNode->data.y);
											// fflush(gpFile);

											if(GameMusic)
											{					
												// if(!PlaySound(MAKEINTRESOURCE(BIRD_DEATH_SOUND),ghInstance, SND_ASYNC | SND_RESOURCE))
												// {
												// 	fprintf(gpFile, "\nFailed to load BIRD_DEATH_SOUND Wav file to play music!");
												// 	exit(0);
												// }
												alSourcePlay(Bird_Death_Sound.Source);
											}

											tempBirdFlyNode->data.removeBird=GL_TRUE;
											tempShotNode->data.removeShot=GL_TRUE;

											goto nextShotNode; 
										}
										tempBirdFlyNode=tempBirdFlyNode->next;
									}

								//Checking if Shot heats the Stone
									tempStoneFallingNode=NULL;
									tempStoneFallingNode=StoneFallingListQueue->front;
									while(tempStoneFallingNode!=NULL)
									{
										// fprintf(gpFile, "\nDisplay->StoneFalling Data:-StoneX:%f,StoneY:%f",tempStoneFallingNode->data.x ,tempStoneFallingNode->data.y);
										// fflush(gpFile);

										if(  (tempShotNode->data.y-HeightEnd) 	>  (tempStoneFallingNode->data.y - ShotStoneRangeLength)
											&& (tempShotNode->data.y-HeightEnd) <  (tempStoneFallingNode->data.y +  ShotStoneRangeLength) 
											&& tempShotNode->data.x 			>  (tempStoneFallingNode->data.x -  ShotStoneRangeLength)
											&& tempShotNode->data.x 			<  (tempStoneFallingNode->data.x + ShotStoneRangeLength) 
											&& tempStoneFallingNode->data.removeStone == GL_FALSE)
										{
											// fprintf(gpFile, "\nDisplay->StoneShot Data:-ShotX:%f,ShotY:%f,StoneX:%f,StoneY:%f,",tempShotNode->data.x ,tempShotNode->data.y,tempStoneFallingNode->data.x,tempStoneFallingNode->data.y);
											// fflush(gpFile);

											if(GameMusic)
											{	
												// if(!PlaySound(MAKEINTRESOURCE(STONE_BLASTING_SOUND),ghInstance, SND_ASYNC | SND_RESOURCE))
												// {
												// 	fprintf(gpFile, "\nFailed to load STONE_BLASTING_SOUND Wav file to play music!");
												// 	exit(0);
												// }
												alSourcePlay(Stone_Blasting_Sound.Source);
											}
											tempStoneFallingNode->data.removeStone=GL_TRUE; 
											tempShotNode->data.removeShot=GL_TRUE;
											goto nextShotNode; 
										}
										tempStoneFallingNode=tempStoneFallingNode->next;
									}
							}

							nextShotNode: 
							tempShotNode=tempShotNode->next;
						glPopMatrix();
					}

				//Render Bird Flying data
					tempBirdFlyNode=NULL;
					tempBirdFlyNode=BirdFlyListQueue->front;
					while(tempBirdFlyNode!=NULL)
					{	
						// fprintf(gpFile, "\nDisplay->Render BirdFly Data:-BirdX:%f,BirdY:%f",tempBirdFlyNode->data.x ,tempBirdFlyNode->data.y);
						// fflush(gpFile);

						glPushMatrix();
							glTranslatef(0.0f,HeightEnd,0.0f);	
							glTranslatef(tempBirdFlyNode->data.x,tempBirdFlyNode->data.y,0.0f);
							
							glScalef(0.05f,0.05f,0.05f);
							if (tempBirdFlyNode->data.xVel>0)
							{
								glRotatef(180.0f,0.0f,1.0f,0.0f);	
							}
							BirdFly();
							
							tempBirdFlyNode->data.x +=tempBirdFlyNode->data.xVel;
							tempBirdFlyNode->data.y +=tempBirdFlyNode->data.yVel;
							
							if(tempBirdFlyNode->data.stoneDroppedYet==GL_FALSE){
								if( 	(tempBirdFlyNode->data.xVel > 0 && tempBirdFlyNode->data.x > tempBirdFlyNode->data.stoneDropX) 
									|| 	(tempBirdFlyNode->data.xVel < 0 && tempBirdFlyNode->data.x < tempBirdFlyNode->data.stoneDropX) )
								{
									tempBirdFlyNode->data.stoneDroppedYet=GL_TRUE;
									//Stone Falling Node Data Preparation
										NewStoneFallingNode=NULL;
										NewStoneFallingNode=(pStoneFallingLLNode)malloc(sizeof(StoneFallingLLNode));

										if(NewStoneFallingNode==NULL)
										{
											fprintf(gpFile, "\nFailed to create Stone Falling Node...!");
											DestroyWindow(ghwnd);
										}

										NewStoneFallingNode->data.x=tempBirdFlyNode->data.x;
										NewStoneFallingNode->data.y=tempBirdFlyNode->data.y;
										NewStoneFallingNode->data.yVel=-0.01;
										NewStoneFallingNode->data.removeStone=GL_FALSE;
										NewStoneFallingNode->next=NULL;

										//Shot Node Data pushed into ShotListQueue
										if(StoneFallingListQueue->rear!=NULL)
										{
											StoneFallingListQueue->rear->next=NewStoneFallingNode;
										}

										StoneFallingListQueue->rear=NewStoneFallingNode;

										if(StoneFallingListQueue->front==NULL)
										{
											StoneFallingListQueue->front= StoneFallingListQueue->rear;
										}
								}	
							}
							//Wall handling
							if(tempBirdFlyNode->data.removeBird==GL_FALSE)
							{
								if( 	(tempBirdFlyNode->data.xVel > 0 && tempBirdFlyNode->data.x > WidthEnd) 
									|| 	(tempBirdFlyNode->data.xVel < 0 && tempBirdFlyNode->data.x < WidthStart) )
								{
									giMaxBirdAliveLeftCount -= 1;
									tempBirdFlyNode->data.removeBird=GL_TRUE;
								}
							}

							tempBirdFlyNode=tempBirdFlyNode->next;
						glPopMatrix();
					}

				//Render Stone Falling data
					tempStoneFallingNode=NULL;
					tempStoneFallingNode=StoneFallingListQueue->front;
					while(tempStoneFallingNode!=NULL)
					{

						// fprintf(gpFile, "\nDisplay->Render StoneFalling Data:-StoneX:%f,StoneY:%f",tempStoneFallingNode->data.x ,tempStoneFallingNode->data.y);
						// fflush(gpFile);

						glPushMatrix();
							glTranslatef(0.0f,HeightEnd,0.0f);	

							glTranslatef(tempStoneFallingNode->data.x,tempStoneFallingNode->data.y,0.0f);
							glCallList(JointSphereHumanMaterial);
							RenderGLUSphere(SphereQuadric,0.1f,10,10);
							
							tempStoneFallingNode->data.y +=tempStoneFallingNode->data.yVel;

							//Ground Handling
								if(tempStoneFallingNode->data.removeStone==GL_FALSE)
								{
									if( tempStoneFallingNode->data.y < HeightStart ) 
									{
										giMaxStoneDropWasteCount -= 1;
										tempStoneFallingNode->data.removeStone=GL_TRUE;
									}
								}

							tempStoneFallingNode=tempStoneFallingNode->next;
						glPopMatrix();
					}	
			glPopMatrix();

		}
	//

	//Menu
		void MenuScreen(void)
		{
			void PrintString(unsigned int ,char *);
			
			glCallList(SkinHumanMaterial);
			
			glTranslatef(0.0f,HeightEnd,0.0f);	

			glPushMatrix();
				glTranslatef(0.0f,-1.0f,0.0f);	
				PrintString(FontListBase,"Bird Hunter");
			glPopMatrix();
			glPushMatrix();
				glTranslatef(0.0f,-3.0f,0.0f);	
				PrintString(FontListBase,"Start Game : S");
			glPopMatrix();
			glPushMatrix();
				glTranslatef(0.0f,-4.0f,0.0f);	
				PrintString(FontListBase,"Help          : H");
			glPopMatrix();
			glPushMatrix();
				glTranslatef(0.0f,-5.0f,0.0f);	
				PrintString(FontListBase,"About        : A");
			glPopMatrix();
			glPushMatrix();
				glTranslatef(0.0f,-6.0f,0.0f);	
				PrintString(FontListBase,"Quit          : Q");
			glPopMatrix();
		}
	//

	//Help
		void HelpScreen(void)
		{
			void PrintString(unsigned int ,char *);
			
			glCallList(SkinHumanMaterial);
			
			glTranslatef(0.0f,HeightEnd,0.0f);	

			glPushMatrix();
				glTranslatef(0.0f,-1.0f,0.0f);	
				PrintString(FontListBase,"Help");
			glPopMatrix();

			glTranslatef(0.0f,0.0f,-3.0f);	
			
			glPushMatrix();
				glTranslatef(0.0f,-3.0f,0.0f);	
				PrintString(FontListBase,"Game Music On/Off       : M");
			glPopMatrix();
			glPushMatrix();
				glTranslatef(0.0f,-4.0f,0.0f);	
				PrintString(FontListBase,"Full Screen On/Off       : F");
			glPopMatrix();
			glPushMatrix();
				glTranslatef(-2.0f,-5.0f,0.0f);	
				PrintString(FontListBase,"Rifle Angle Shift:");
			glPopMatrix();
			glPushMatrix();
				glTranslatef(0.0f,-6.0f,0.0f);	
				PrintString(FontListBase,"Left         : <-");
			glPopMatrix();
			glPushMatrix();
				glTranslatef(0.0f,-7.0f,0.0f);	
				PrintString(FontListBase,"Right        : ->");
			glPopMatrix();
			glPushMatrix();
				glTranslatef(0.0f,-8.0f,0.0f);	
				PrintString(FontListBase,"Fire Shot : Spacebar");
			glPopMatrix();

			glPushMatrix();
				glTranslatef(-8.0f,-9.0f,0.0f);	
				PrintString(FontListBase,"Menu : P");
			glPopMatrix();

			glPushMatrix();
				glTranslatef(8.0f,-9.0f,0.0f);	
				PrintString(FontListBase,"Quit : Q");
			glPopMatrix();
		}
	//

	//About
		void AboutScreen(void)
		{
			void PrintString(unsigned int ,char *);
			
			glCallList(SkinHumanMaterial);
			
			glTranslatef(0.0f,HeightEnd,0.0f);	

			glPushMatrix();
				glTranslatef(0.0f,-1.0f,0.0f);	
				PrintString(FontListBase,"About");
			glPopMatrix();

			glPushMatrix();
				glTranslatef(0.0f,-2.0f,0.0f);	
				PrintString(FontListBase,"Bird Hunter");
			glPopMatrix();
				
			glPushMatrix();
			
				glTranslatef(0.0f,0.0f,-10.0f);
				
				glPushMatrix();
					glTranslatef(0.0f,-2.0f,-1.0f);	
					PrintString(FontListBase,"Implemented with following:");
				glPopMatrix();
			
				glPushMatrix();
					glTranslatef(0.0f,-4.0f,0.0f);	
					PrintString(FontListBase,"(Win32 + OpenGL + OpenAL) API + Single Linked List (Data Structure)");
				glPopMatrix();

				glPushMatrix();
					glTranslatef(0.0f,-6.0f,0.0f);	
					PrintString(FontListBase,"RTR2020 Batch,Member of Texture Group,AstroMediComp");
				glPopMatrix();
				
				glPushMatrix();
					glTranslatef(0.0f,-7.0f,0.0f);	
					PrintString(FontListBase,"Special Thanks:Dr. Vijay D. Gokhale(Guru)");
				glPopMatrix();

				glPushMatrix();
					glTranslatef(0.0f,-8.0f,0.0f);	
					PrintString(FontListBase,"Special Thanks:Archana Jethale(Texture Group Leader)");
				glPopMatrix();

				glPushMatrix();
					glTranslatef(0.0f,-10.0f,0.0f);	
					PrintString(FontListBase,"Developed By:Vardhaman Viroje");
				glPopMatrix();

				glPushMatrix();
					glTranslatef(-12.0f,-12.0f,0.0f);	
					PrintString(FontListBase,"Menu : P");
				glPopMatrix();

				glPushMatrix();
					glTranslatef(12.0f,-12.0f,0.0f);	
					PrintString(FontListBase,"Quit : Q");
				glPopMatrix();
			
			glPopMatrix();
		}
	//

	//Game Over
		void GameOverScreen(void)
		{
			void PrintString(unsigned int ,char *);
			
			glCallList(SkinHumanMaterial);
			
			glTranslatef(0.0f,HeightEnd,0.0f);	

			glPushMatrix();
				glTranslatef(0.0f,-1.0f,0.0f);	
				PrintString(FontListBase,"Game Over");
			glPopMatrix();

			glPushMatrix();
				glTranslatef(0.0f,-5.0f,0.0f);	
				PrintString(FontListBase,"Menu :P");
			glPopMatrix();
		
			glPushMatrix();
				glTranslatef(0.0f,-6.0f,0.0f);	
				PrintString(FontListBase,"Quit : Q");
			glPopMatrix();
		}
	//


// 

//Out Line Fonts Initialization
		
	unsigned int CreateOutlineFont(char *fontName,int fontSize,float depth)
	{
		HFONT hFont;
		unsigned int base;

		base=glGenLists(256);
		if (stricmp(fontName,"symbol")==0)
		{
			hFont=CreateFont(fontSize,0,0,0,
			FW_BOLD,FALSE,FALSE,FALSE,
			SYMBOL_CHARSET,OUT_TT_PRECIS,CLIP_DEFAULT_PRECIS,
			ANTIALIASED_QUALITY,FF_DONTCARE | DEFAULT_PITCH,fontName);
		}else{
			hFont=CreateFont(fontSize,0,0,0,
			FW_BOLD,FALSE,FALSE,FALSE,
			ANSI_CHARSET,OUT_TT_PRECIS,CLIP_DEFAULT_PRECIS,
			ANTIALIASED_QUALITY,FF_DONTCARE | DEFAULT_PITCH,fontName);
		}

		if(!hFont){
			return 0;
		}
		SelectObject(ghdc,hFont);
		wglUseFontOutlines(ghdc,0,255,base,0.0f,depth,WGL_FONT_POLYGONS,gmf);

		return base;
	}

	void PrintString(unsigned int base,char *str)
	{
		float length=0;
		if((base==0) || (str==NULL))
			return;

		for(int i=0;i<strlen(str);i++)
		{
			length += gmf[str[i]].gmfCellIncX;
		}

		glTranslatef(-length/2.0f,0.0f,0.0f);
		
		glPushAttrib(GL_LIST_BIT);
			glListBase(base);
			glCallLists(strlen(str),GL_UNSIGNED_BYTE,str);
		glPopAttrib();
	}
//


void DeallocateAll_DS_Pointers(void)
{
		//ShotListQueue
			pShotNode tempNode=NULL;
			while(ShotListQueue->front!=NULL)
			{
				tempNode=ShotListQueue->front;
				// fprintf(gpFile, "\nUnInitialize:-Shot-Data:angle-%f,x-%f,y-%f",tempNode->data.angle,tempNode->data.x,tempNode->data.y);
				// fflush(gpFile);

				ShotListQueue->front=tempNode->next;
				free(tempNode);
			}
			ShotListQueue->front=ShotListQueue->rear=NULL;
			tempNode=NULL;
			if(QuitGame==GL_TRUE)
			{
				free(ShotListQueue);
			}
		//BirdFlyListQueue
			pBirdFlyNode tempBirdFlyNode=NULL;
			while(BirdFlyListQueue->front!=NULL)
			{
				tempBirdFlyNode=BirdFlyListQueue->front;
				// fprintf(gpFile, "\nUnInitialize:-Bird-Data:x-%f,y-%f",tempBirdFlyNode->data.x,tempBirdFlyNode->data.y);
				// fflush(gpFile);

				BirdFlyListQueue->front=tempBirdFlyNode->next;
				free(tempBirdFlyNode);
			}
			BirdFlyListQueue->front=BirdFlyListQueue->rear=NULL;
			tempBirdFlyNode=NULL;
			if(QuitGame==GL_TRUE)
			{
				free(BirdFlyListQueue);
			}
		//StoneFallingListQueue
			pStoneFallingLLNode tempStoneFallingNode=NULL;
			while(StoneFallingListQueue->front!=NULL)
			{
				tempStoneFallingNode=StoneFallingListQueue->front;
				// fprintf(gpFile, "\nUnInitialize:-Stone-Data:x-%f,y-%f",tempStoneFallingNode->data.x,tempStoneFallingNode->data.y);
				// fflush(gpFile);

				StoneFallingListQueue->front=tempStoneFallingNode->next;
				free(tempStoneFallingNode);
			}
			StoneFallingListQueue->front=StoneFallingListQueue->rear=NULL;
			tempStoneFallingNode=NULL;
			if(QuitGame==GL_TRUE)
			{
				free(StoneFallingListQueue);
			}
}
