#include<windows.h>
#include<stdio.h>
#include"Texture_Checker_Board.h"
#include<GL/GL.h>
#include<GL/GLU.h>

#define WIN_WIDTH 800
#define WIN_HEIGHT 600
#pragma comment(lib,"OpenGL32.lib")
#pragma comment(lib,"GLU32.lib")

#define CHECK_IMAGE_WIDTH 64
#define CHECK_IMAGE_HEIGHT 64

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

FILE* gpFile = NULL;
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
bool gbFullscreen = false;
bool gbActiveWindow = false;
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

// GLuint Kundali_Texture;//texture variable
GLuint Tex_Image;//texture variable

GLubyte checkImage[CHECK_IMAGE_HEIGHT][CHECK_IMAGE_WIDTH][4];

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	void Initialize(void);
	void Display(void);

	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("TextureCube");
	int X1, X2, Y1, Y2;
	bool bDone = false;

	if (fopen_s(&gpFile, "log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Failed to Create log file!"), TEXT("Error"), MB_OK | MB_ICONERROR);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "\nLog will be start from here...!");
	}

	X1 = GetSystemMetrics(SM_CXSCREEN) / 2;
	Y1 = GetSystemMetrics(SM_CYSCREEN) / 2;

	X2 = WIN_WIDTH / 2;
	Y2 = WIN_HEIGHT / 2;

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style =  CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(GRAY_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));

	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("OpenGL Texture Cube"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		X1 - X2,//100,
		Y1 - Y2,//100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;
	Initialize();

	ShowWindow(hwnd, iCmdShow);

	SetForegroundWindow(hwnd);
	SetFocus(hwnd);
	


	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
		
			if (gbActiveWindow == true)
			{
				Display();
			}
		}
	}

	return ((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void ToggleFullscreen(void);
	void Resize(int, int);
	void UnInitialize(void);

	switch (iMsg)
	{
	case WM_CREATE:
		fprintf(gpFile, "\nOpenGL Texture Cube Log is Started!");
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
			case VK_ESCAPE:
				DestroyWindow(hwnd);
				break;
			case 0x46:
			case 0x66:
				ToggleFullscreen();
				break;
			default:
				break;
		}
		break;
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;
	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;

	case WM_ERASEBKGND:
		return 0;
	case WM_SIZE:
		Resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;
	case WM_DESTROY:
		fprintf(gpFile, "\nOpenGL Texture Cube Log is Ended!");
		UnInitialize();
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}
void ToggleFullscreen(void)
{
	MONITORINFO mi = { sizeof(MONITORINFO) };
	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW) 
		{
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				fprintf(gpFile, "\nFullscreen Enabled");
				SetWindowLong(ghwnd, GWL_STYLE, (dwStyle & ~WS_OVERLAPPEDWINDOW));
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
		gbFullscreen = true;
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, (dwStyle | WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		fprintf(gpFile, "\nNormal Window");
		ShowCursor(TRUE);
		gbFullscreen = false;
	}
}

void Initialize(void)
{
	void Resize(int, int);
	// bool LoadGLTexture(GLuint*,TCHAR[]);
	void LoadGLTexture(void);

	//Local variable initialization
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	ghdc = GetDC(ghwnd);

	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER ;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits= 32 ;


	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (iPixelFormatIndex == 0 )
	{
		fprintf(gpFile, "\nChoosePixelFormat() Failed to get pixel format index");
		DestroyWindow(ghwnd);
	}
	else
	{
		fprintf(gpFile, "\nChoosePixelFormat() Successfully selected pixel format and recived its index");
	}

	if ( SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE )
	{
		fprintf(gpFile, "\nSetPixelFormat() Failed to set pixel format for window");
		DestroyWindow(ghwnd);
	}
	else
	{
		fprintf(gpFile, "\nSetPixelFormat() Successfully able to set pixel format");
	}

	ghrc = wglCreateContext(ghdc);
	if ( ghrc == NULL)
	{
		fprintf(gpFile, "\nwglCreateContext() Failed to get the rendering context");
		DestroyWindow(ghwnd);
	}
	else
	{
		fprintf(gpFile, "\nwglCreateContext() Successfully able to get rendering context");
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE )
	{
		fprintf(gpFile, "\nwglMakeCurrent() Failed to switch current context from device to rendering context");
		DestroyWindow(ghwnd);
	}
	else
	{
		fprintf(gpFile, "\nwglMakeCurrent() Successfully able to switch current context from device to rendering context");
	}

	//---loading texture
	glEnable(GL_TEXTURE_2D);
	// LoadGLTexture(&Kundali_Texture,MAKEINTRESOURCE(KUNDALI_BITMAP));
	LoadGLTexture();

	//SetColor
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	//Set Depth
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	glShadeModel(GL_SMOOTH);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT,GL_NICEST);
	//Warmup resize call
	Resize(WIN_WIDTH, WIN_HEIGHT);
	
}

void Resize(int width, int height)
{
	if (height == 0)
	{
		height = 1;
	}
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	glMatrixMode(GL_PROJECTION);	
	glLoadIdentity();

	gluPerspective(	60.0f,
					(GLfloat)width/(GLfloat)height,
					0.1f,
					30.0f
				);

}

GLfloat RotateAngle=0.0f;

void Display(void)
{
	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
	
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(0.0f,0.0f,-3.6f);

	glBindTexture( GL_TEXTURE_2D,
						Tex_Image
					);

	glBegin(GL_QUADS);
		
		glTexCoord2f(0.0f,0.0f);
			glVertex3f(-2.0f, -1.0f, 0.0f);
		glTexCoord2f(0.0f,1.0f);
			glVertex3f(-2.0f, 1.0f, 0.0f);
		glTexCoord2f(1.0f,1.0f);
			glVertex3f(0.0f, 1.0f, 0.0f);	
		glTexCoord2f(1.0f,0.0f);
			glVertex3f(0.0f, -1.0f, 0.0f);
	
	glEnd();

	glBegin(GL_QUADS);
		
		glTexCoord2f(0.0f,0.0f);
			glVertex3f(1.0f, -1.0f, 0.0f);
		glTexCoord2f(0.0f,1.0f);
			glVertex3f(1.0f, 1.0f, 0.0f);
		glTexCoord2f(1.0f,1.0f);
			glVertex3f(2.41421f, 1.0f, -1.41421f);	
		glTexCoord2f(1.0f,0.0f);
			glVertex3f(2.41421f, -1.0f, -1.41421f);
	
	glEnd();

	SwapBuffers(ghdc);

	if(RotateAngle>360.0f)
		RotateAngle=0.0f;
	
	RotateAngle += 1.0f;
	
}
void UnInitialize(void)
{
	if (gbFullscreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, (dwStyle | WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}
	
	if (wglGetCurrentContext() == ghrc ) {
		wglMakeCurrent(NULL, NULL);
	}
	else
	{
		fprintf(gpFile, "\nwglGetCurrentContext current context is not rendering context so no need to switch.");
	}

	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}
	else
	{
		fprintf(gpFile, "\nRendering Context is already uninitialized or never created/initialized.");
	}

	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}
	else
	{
		fprintf(gpFile, "\nDevice context is already uninitialized or never created/initialized.");
	}

	glDeleteTextures(1,&Tex_Image);


	if (gpFile)
	{
		fprintf(gpFile, "\nLog will be End from here...!");
		fclose(gpFile);
		gpFile = NULL;
	}
}




bool LoadGLTexture(GLuint *texture,TCHAR resourceID[])
{
	bool bResult=false;
	HBITMAP hbitmap=NULL;
	BITMAP bmp;
	//code 
	hbitmap=(HBITMAP)LoadImage( GetModuleHandle(NULL),//get current HInstance of our process
								resourceID,//resorce id
								IMAGE_BITMAP,//image type
								0,//width
								0,//height
								LR_CREATEDIBSECTION//load resource as device independent bitmap
								);
	if(hbitmap!=NULL)
	{
		bResult=true;
		GetObject( hbitmap,//to extract information  from handle of object 
				sizeof(BITMAP),//Size of object where to store info
				&bmp//Address of object to store information
				);

		glPixelStorei( GL_UNPACK_ALIGNMENT,
						4
					);

		glGenTextures( 1,
					texture
					);

		glBindTexture( GL_TEXTURE_2D,
						*texture
					);

		//Setting Texture Parameter
		glTexParameteri( GL_TEXTURE_2D,
						GL_TEXTURE_MAG_FILTER,
						GL_LINEAR
						);
		glTexParameteri( GL_TEXTURE_2D,
						GL_TEXTURE_MIN_FILTER,
						GL_LINEAR_MIPMAP_LINEAR
					   );
		//following call will push texture data in graphic memory with helper graphic driver
		gluBuild2DMipmaps(GL_TEXTURE_2D,
						3,//RGB
						bmp.bmWidth,
						bmp.bmHeight,
						GL_BGR_EXT,
						GL_UNSIGNED_BYTE,
						bmp.bmBits
						);

		DeleteObject(hbitmap);
	}

	return bResult;
}

void LoadGLTexture(void)
{
	void MakeCheckImage(void);
	MakeCheckImage();

	// bool bResult=false;
	// HBITMAP hbitmap=NULL;
	// BITMAP bmp;
	// //code 
	// hbitmap=(HBITMAP)LoadImage(  GetModuleHandle(NULL),//get current HInstance of our process
	// 							resourceID,//resorce id
	// 							IMAGE_BITMAP,//image type
	// 							0,//width
	// 							0,//height
	// 							LR_CREATEDIBSECTION//load resource as device independent bitmap
	// 							);
	// if(hbitmap!=NULL)
	// {
	// 	bResult=true;
		// GetObject( hbitmap,//to extract information  from handle of object 
		// 		sizeof(BITMAP),//Size of object where to store info
		// 		&bmp//Address of object to store information
		// 		);

		glGenTextures( 1,
					&Tex_Image
					);

		glBindTexture( GL_TEXTURE_2D,
						Tex_Image
					);

		glPixelStorei( GL_UNPACK_ALIGNMENT,
						1
					);			

		//Setting Texture Parameter
		glTexParameteri( GL_TEXTURE_2D,
						GL_TEXTURE_MAG_FILTER,
						GL_NEAREST
						);
		glTexParameteri( GL_TEXTURE_2D,
						GL_TEXTURE_MIN_FILTER,
						GL_NEAREST
					   );
		glTexParameteri( GL_TEXTURE_2D,
						GL_TEXTURE_WRAP_S,//Wrap Image/geometry at end (Horizontal)
						GL_REPEAT
						);
		glTexParameteri( GL_TEXTURE_2D,
						GL_TEXTURE_WRAP_T,//Wrap Image/geometry at end (vertical)
						GL_REPEAT
					   );

		glTexImage2D(GL_TEXTURE_2D,
					0,//mipmap level-use default
					4,//for RGBA-Internal texture Format or also can use GL_RGBA 
					CHECK_IMAGE_WIDTH,//Image Width
					CHECK_IMAGE_HEIGHT,//Image Height
					0,//Boarder width if were to set a value then it should be in power of 2
					GL_RGBA,
					GL_UNSIGNED_BYTE,
					checkImage
					);

		//To set texture environment
		// glTexEnvf( GL_TEXTURE_ENV,
		// 		GL_TEXTURE_ENV_MODE,
		// 		GL_REPLACE
		// 		);

		// //following call will push texture data in graphic memory with helper graphic driver
		// gluBuild2DMipmaps(GL_TEXTURE_2D,
		// 				3,//RGB
		// 				bmp.bmWidth,
		// 				bmp.bmHeight,
		// 				GL_BGR_EXT,
		// 				GL_UNSIGNED_BYTE,
		// 				bmp.bmBits
		// 				);

		// DeleteObject(hbitmap);
	// }
}


void MakeCheckImage(void)
{
	int vsv_i,vsv_j,vsv_c;

    for(vsv_i=0;vsv_i<CHECK_IMAGE_HEIGHT;vsv_i++){

        for(vsv_j=0;vsv_j<CHECK_IMAGE_WIDTH;vsv_j++){

            vsv_c=( (vsv_i & 0x8 )==0)^( (vsv_j & 0x8 )==0);
			vsv_c*=255;
			checkImage[vsv_i][vsv_j][0]=(GLubyte)vsv_c;
			checkImage[vsv_i][vsv_j][1]=(GLubyte)vsv_c;
			checkImage[vsv_i][vsv_j][2]=(GLubyte)vsv_c;
			checkImage[vsv_i][vsv_j][3]=255;
        }
    }
}
















