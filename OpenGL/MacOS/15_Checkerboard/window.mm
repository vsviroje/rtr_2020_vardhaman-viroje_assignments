#import<Foundation/Foundation.h> ///get foundation header file from foundation framework (similar to standard library like stdio.h)
#import<Cocoa/Cocoa.h>///analogus to window.h/xlib.h
#import<QuartzCore/CVDisplayLink.h>///CVDisplayLink mean core video
#import<OpenGL/gl3.h>///gl.h....shaders were added from 3.2 so gl3
#include"vmath.h"
using namespace vmath;

enum{
    VSV_ATTRIBUTE_POSITION=0,
    VSV_ATTRIBUTE_COLOR,
    VSV_ATTRIBUTE_TEXCOORD,
    VSV_ATTRIBUTE_NORMAL,
};

#define CHECK_IMAGE_WIDTH 64
#define CHECK_IMAGE_HEIGHT 64

//callback
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef,const CVTimeStamp*,const CVTimeStamp*,CVOptionFlags,CVOptionFlags*,void *);

FILE *gpFile = NULL;

///mkdir -p Window.app/Contents/MacOS
///clang++ -Wno-deprecated-declarations -o Window.app/Contents/MacOS/window Window.mm -framework Cocoa -framework QuartzCore -framework OpenGL

///NeXT STEP (NS)

/// our class AppDelegate is inheritated from NSObject class and it will implement NSApplicationDelegate and NSWindowDelegate interface
/// : is extends and <> are implements
///we will define method later...its forward declaration
@interface AppDelegate:NSObject<NSApplicationDelegate, NSWindowDelegate>
@end

int main(int argc,char* argv[])
{
    ///code
    ///NSAutoreleasePool will hold all alocated memory
    NSAutoreleasePool *pool=[[NSAutoreleasePool alloc] init];
    ///NSApp is inbuild global NS application object
    ///we get to access shared NS Application object
    NSApp =[NSApplication sharedApplication];

    ///setting NSApp method implementation to our AppDelegate object
    [NSApp setDelegate:[[AppDelegate alloc] init] ];

    ///Message/run/event loop
    [NSApp run];

    ///releaseing every obj memory from pool
    [pool release];

    return 0;
}

///
@interface MyOpenGLView:NSOpenGLView
@end

//implmentating method for AppDelegate
@implementation AppDelegate
    {
        @private ///private variable
        NSWindow *window;
        MyOpenGLView *myOpenGLview;
    }

    ///when application is getting started, then applicationDidFinishLaunching() a method of NSApplication is called
    ///WM_Create and applicationDidFinishLaunching() are analogus
    -(void)applicationDidFinishLaunching:(NSNotification *)aNotification
    {
        ///code
        //window.app is same as NSBundle(its for recongnising window.app in foundation.h)
        NSBundle *appBundle = [NSBundle mainBundle];
        ///storing window.app dir path
        NSString *appDirPath = [appBundle bundlePath];

        /*
        eg . if appDirPath is /usr/viroje/Desktop/assignment/02LogFile/window.app
            then stringByDeletingLastPathComponent will perform
        then parentDirPath will be /usr/viroje/Desktop/assignment/02LogFile
        */
        NSString *parentDirPath = [appDirPath stringByDeletingLastPathComponent];

        NSString *logFileNameWithPath = [NSString stringWithFormat:@"%@/Log.txt",parentDirPath];

        const char *pSzLogFileNameWithPath =  [logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];

        gpFile = fopen(pSzLogFileNameWithPath,"w");
        if(gpFile==NULL)
        {
            [self release];
            [NSApp terminate:self];
        }

        fprintf(gpFile,"\nProgram Started Successfully");
        fflush(gpFile);

        ///NSRect is internally CGRect -core graphic ..its c based library
        ///CGPoint/NSPoint-x,y and CGSize/NSSize-width,height
        NSRect winRect = NSMakeRect(0.0,0.0,800.0,600.0);

        ///styleMask is similer to WM_overlappedWindow
        ///backing- how to store window in graphics
        ///defer - to halt showing of window or show window immediately
        window = [[NSWindow alloc] initWithContentRect:winRect
                                                    styleMask:NSWindowStyleMaskTitled |
                                                            NSWindowStyleMaskClosable |
                                                            NSWindowStyleMaskMiniaturizable |
                                                            NSWindowStyleMaskResizable
                                                            backing:NSBackingStoreBuffered
                                                            defer:NO];
        
        [window setTitle:@"OpenGL : Checkerboard"];
        [window center];

        ///FrameSize will be same as windowSize
        myOpenGLview = [[MyOpenGLView alloc] initWithFrame:winRect];
        ///assigning Frame to our window
        [window setContentView:myOpenGLview];

        ///NSWindow method will be implemented by itself
        ///if not use this line then app will not removed from taskbar
        [window setDelegate:self];

        ///its like setFocus() and setForgroundWindow() or MB_TOPMOST
        [window makeKeyAndOrderFront:self];
    }

    ///when application is gets terminated, then applicationWillTerminate() a method of NSApplication is called
    ///WM_Destroy and applicationWillTerminate() are analogus
    /// [NSApp terminate:self] is like WM_QUIT
    ///  when this line is called [NSApp terminate:self];  ...then applicationWillTerminate() is called ...then it exit Message/run/event loop
    -(void)applicationWillTerminate:(NSNotification *)aNotification
    {
        ///code

        if(gpFile)
        {
            fprintf(gpFile,"\nProgram terminated Successfully");
            fflush(gpFile);
            fclose(gpFile);
        }
        gpFile=NULL;
    }

    ///NSWindowDelegate method
    -(void)windowWillClose:(NSNotification *)aNotification
    {
        ///code
        [NSApp terminate:self];
    }

    ///like destructor
    -(void)dealloc
    {
        ///code
        [myOpenGLview release];
        [window release];
        [super dealloc];
    }

@end

//implmentating method for MyOpenGLView
@implementation MyOpenGLView
    {
        @private ///private variable
        CVDisplayLinkRef displayLinkRef;

        GLuint gVertexShaderObject;
        GLuint gFragmentShaderObject;
        GLuint gShaderProgramObject;

        GLuint vao_checkerboard;
        GLuint vbo_checkerboardPosition;
        GLuint vbo_checkerboardTexcoord;

        GLuint mvpMatrixUniform;
        GLuint textureSamplerUniform;

        mat4 perspectiveProjectionMatrix;

        GLubyte checkImage[CHECK_IMAGE_HEIGHT][CHECK_IMAGE_WIDTH][4];
        GLuint CheckerboardTextureID;
        
    }

    ///id is a object of class
    -(id)initWithFrame:(NSRect)frame
    {
        ///code
        self = [super initWithFrame:frame];
        if (self)
        {
            //array of NSOpenGLPixelFormatAttribute
            //its like pixelformatdiscriptor/framebufferattribute
            NSOpenGLPixelFormatAttribute attributes[] = {
                                                            NSOpenGLPFAOpenGLProfile,NSOpenGLProfileVersion4_1Core,
                                                            NSOpenGLPFAScreenMask,CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay), ///Display/monitor  id/type
                                                            NSOpenGLPFANoRecovery,/// canceling automatic shifting process from h/w rendering to s/w rendering ,if h/w rendering context not found
                                                            NSOpenGLPFAAccelerated,/// to use h/w rendering context
                                                            NSOpenGLPFAColorSize,24,//RGB
                                                            NSOpenGLPFADepthSize,24,
                                                            NSOpenGLPFAAlphaSize,8,
                                                            NSOpenGLPFADoubleBuffer,
                                                            0
                                                        };
            ///autorelease = release on your own in auto release pool
            ///pixelFormat its like choosePixelFormat
            NSOpenGLPixelFormat *pixelFormat = [[[NSOpenGLPixelFormat alloc] initWithAttributes:attributes] autorelease];
            if (pixelFormat==nil)
            {
                fprintf(gpFile,"\npixelFormat is nil.exited");
                fflush(gpFile);
                [self release];
                [NSApp terminate:self];
            }

            //shareContext = from linux
            NSOpenGLContext *glContext = [[[NSOpenGLContext alloc] initWithFormat:pixelFormat shareContext:nil] autorelease];

            //set pixelformat
            [self setPixelFormat:pixelFormat];

            //set content
            [self setOpenGLContext:glContext];
        }

        return self;
    }

    -(CVReturn)getFrameForTime:(const CVTimeStamp*)outputTime
    {
        ///code
        ///Apple OpenGL supports multi threading
        ///pool in main() is main thread and pool in getFrameForTime() are other thread
        ///this pool is renderings auto release pool
        NSAutoreleasePool *pool=[[NSAutoreleasePool alloc] init];

        ///to draw our shaders
        [self drawView];

        ///releasing renderings auto release pool
        [pool release];

        return(kCVReturnSuccess);
    }

    -(void)prepareOpenGL
    {
        ///code
        [super prepareOpenGL];

        ///Objective c does not add `get` as prefix for getter methods
        ///Objective c does add `set` as prefix for setter methods
        ///it gives opengl context and set to current context
        [[self openGLContext] makeCurrentContext];

        ///Swap interval code below
        ///CP=context parameter
        GLint swapInt = 1;
        [[self openGLContext] setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];

        fprintf(gpFile,"\nOpenGL Vender:%s",glGetString(GL_VENDOR));
        fprintf(gpFile,"\nOpenGL Render:%s",glGetString(GL_RENDERER));
        fprintf(gpFile,"\nOpenGL Version:%s",glGetString(GL_VERSION));
        fprintf(gpFile,"\nOpenGL GLSL Version:%s",glGetString(GL_SHADING_LANGUAGE_VERSION));

        ///****************shader codes start*****************************
        
        GLint infoLogLength;
        GLint shaderCompileStatus;
        GLint shaderProgramLinkStatus;
        GLchar *szBuffer;
        GLsizei written;

        //Shader Code Start--------------------
            gVertexShaderObject=glCreateShader(GL_VERTEX_SHADER);

            const GLchar *vertexShaderSrc=
            "#version 410 core"\
            "\n"\
            "in vec4 vPosition;"\
            "in vec2 vTexCoord;"\
            "out vec2 out_TexCoord;"\

            "uniform mat4 u_mvpMatrix;"\

            "void main(void)"\
            "{"\
                "out_TexCoord=vTexCoord;"\
                "gl_Position=u_mvpMatrix * vPosition;"\
            "}";

            glShaderSource(gVertexShaderObject,
                            1,
                            (const GLchar **)&vertexShaderSrc,
                            NULL);

            glCompileShader(gVertexShaderObject);

            glGetShaderiv(gVertexShaderObject,
                            GL_COMPILE_STATUS,
                            &shaderCompileStatus);

            if(shaderCompileStatus == GL_FALSE)
            {
                glGetShaderiv(gVertexShaderObject,
                            GL_INFO_LOG_LENGTH,
                            &infoLogLength);

                if(infoLogLength > 0)
                {
                    szBuffer=(GLchar *)malloc(sizeof(GLchar) * infoLogLength);

                    if(szBuffer!=NULL)
                    {
                        glGetShaderInfoLog(gVertexShaderObject,
                                            infoLogLength,
                                            &written,
                                            szBuffer);
                        fprintf(gpFile,
                                "\nERROR | Vertex Shader Compilation Failed Log : %s",
                                szBuffer);
                        free(szBuffer);
    [self release];
                [NSApp terminate:self];                    }
                }
            }

            infoLogLength=0;
            shaderCompileStatus=0;
            shaderProgramLinkStatus=0;
            szBuffer=NULL;
            written=0;

            gFragmentShaderObject=glCreateShader(GL_FRAGMENT_SHADER);

            const GLchar *fragmentShaderSrc=
            "#version 410 core"\
            "\n"\
            
            "in vec2 out_TexCoord;"\
            "out vec4 fragColor;"\

            "uniform sampler2D u_texture_sampler;"\

            "void main(void)"\
            "{"\
                "fragColor=texture(u_texture_sampler , out_TexCoord);"\
            "}";

            glShaderSource(gFragmentShaderObject,
                            1,
                            (const GLchar**)&fragmentShaderSrc,
                            NULL);
            
            glCompileShader(gFragmentShaderObject);

            glGetShaderiv(gFragmentShaderObject,
                            GL_COMPILE_STATUS,
                            &shaderCompileStatus);

            if(shaderCompileStatus==GL_FALSE)
            {
                glGetShaderiv(gFragmentShaderObject,
                            GL_INFO_LOG_LENGTH,
                            &infoLogLength);

                if(infoLogLength > 0 )
                {
                    szBuffer=(GLchar*)malloc(sizeof(GLchar) * infoLogLength);

                    glGetShaderInfoLog(gFragmentShaderObject,
                                            infoLogLength,
                                            &written,
                                            szBuffer);

                    fprintf(gpFile,
                            "\nERROR | Fragment Shader Compilation failed log %s",
                            szBuffer);

                    free(szBuffer);

[self release];
                [NSApp terminate:self];                }
            }

            infoLogLength=0;
            shaderCompileStatus=0;
            shaderProgramLinkStatus=0;
            szBuffer=NULL;
            written=0;

            gShaderProgramObject=glCreateProgram();

            glAttachShader(gShaderProgramObject , gVertexShaderObject);
            glAttachShader(gShaderProgramObject , gFragmentShaderObject);

            //Shader Attribute Binding Before Pre Linking of attached Shader
                glBindAttribLocation(gShaderProgramObject , VSV_ATTRIBUTE_POSITION ,  "vPosition");
                glBindAttribLocation(gShaderProgramObject , VSV_ATTRIBUTE_TEXCOORD , "vTexCoord");

            glLinkProgram(gShaderProgramObject);

            glGetProgramiv(gShaderProgramObject,
                            GL_LINK_STATUS,
                            &shaderProgramLinkStatus);

            if(shaderProgramLinkStatus==GL_FALSE)
            {
                glGetProgramiv(gShaderProgramObject,
                                GL_INFO_LOG_LENGTH,
                                &infoLogLength);
                
                if(infoLogLength > 0)
                {
                    szBuffer=(GLchar *)malloc(sizeof(GLchar) * infoLogLength);
                    if(szBuffer!=NULL)
                    {
                        glGetProgramInfoLog(gShaderProgramObject,
                                            infoLogLength,
                                            &written,
                                            szBuffer);
                        fprintf(gpFile,
                                "\nERROR | Shader Program Link Failed Log %s",
                                szBuffer);
                        free(szBuffer);
    [self release];
                [NSApp terminate:self];                    }
                }
            }
            
            //Uniform variable Binding after post linking of attached shader
                mvpMatrixUniform=glGetUniformLocation(gShaderProgramObject,
                                                        "u_mvpMatrix");
                textureSamplerUniform=glGetUniformLocation(gShaderProgramObject,
                                                            "u_texture_sampler");

        //Shader Code End--------------------

        //VAO and its respective VBO preparation
        const GLfloat CheckerboardTexCoord[]=
        {
            0.0f,0.0f,
            0.0f,1.0f,
            1.0f,1.0f,
            1.0f,0.0f
        };

        glGenVertexArrays(1,&vao_checkerboard);
        glBindVertexArray(vao_checkerboard);

            glGenBuffers(1,&vbo_checkerboardPosition);
            glBindBuffer(GL_ARRAY_BUFFER,vbo_checkerboardPosition);

                glBufferData(GL_ARRAY_BUFFER,
                            3 * 4 * sizeof(GLfloat),
                            NULL,
                            GL_DYNAMIC_DRAW);

                glVertexAttribPointer(VSV_ATTRIBUTE_POSITION,
                                    3,
                                    GL_FLOAT,
                                    GL_FALSE,
                                    0,
                                    NULL);

                glEnableVertexAttribArray(VSV_ATTRIBUTE_POSITION);
            glBindBuffer(GL_ARRAY_BUFFER,0);

            glGenBuffers(1,&vbo_checkerboardTexcoord);
            glBindBuffer(GL_ARRAY_BUFFER,vbo_checkerboardTexcoord);

                glBufferData(GL_ARRAY_BUFFER,
                            sizeof(CheckerboardTexCoord),
                            CheckerboardTexCoord,
                            GL_STATIC_DRAW);
                
                glVertexAttribPointer(VSV_ATTRIBUTE_TEXCOORD,
                                    2,
                                    GL_FLOAT,
                                    GL_FALSE,
                                    0,
                                    NULL);

                glEnableVertexAttribArray(VSV_ATTRIBUTE_TEXCOORD);
            glBindBuffer(GL_ARRAY_BUFFER,0);

        glBindVertexArray(0);


        //SetColor
        glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        
        glClearDepth(1.0f);
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);

        glEnable(GL_TEXTURE_2D);
        [self LoadGLTexture];

        perspectiveProjectionMatrix = mat4 :: identity();

        ///****************shader codes end*****************************

        ///-----Setup code for DisplayLink and its Callback (its CV and CG related code )----------------------------
        /* CGL-core graphics library
            1.Create DisplayLink
            2.set Callback function for DisplayLink
            3.Convert OpenGLContext to CGL context
            4.Convert OpenGLPixelFormat to CGL PixelFormat
            5.set current displays CGL context and CGL PixelFormat
            6.start DisplayLink
        */

        //1.Create DisplayLink
        CVDisplayLinkCreateWithActiveCGDisplays(&displayLinkRef);
        
        //2.set Callback function for DisplayLink
        //displayLinkContext is self
        CVDisplayLinkSetOutputCallback(displayLinkRef,&MyDisplayLinkCallback,self);
        
        //3.Convert OpenGLContext to CGL context
        CGLContextObj cglContext = (CGLContextObj)[[self openGLContext] CGLContextObj];

        //4.Convert OpenGLPixelFormat to CGL PixelFormat
        CGLPixelFormatObj cglPixelFormat = (CGLPixelFormatObj)[[self pixelFormat] CGLPixelFormatObj];

        // 5.set current displays CGL context and CGL PixelFormat
        CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLinkRef,cglContext,cglPixelFormat);

        //6.start DisplayLink
        CVDisplayLinkStart(displayLinkRef);

    }

    -(void)reshape
    {
        ///code
        [super reshape];

        CGLLockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);

        NSRect rect = [self bounds];

        if (rect.size.height < 0)
        {
            rect.size.height = 1;
        }

        glViewport(0,0,(GLsizei)rect.size.width,(GLsizei)rect.size.height);

        int width = rect.size.width;
        int height = rect.size.height;

        perspectiveProjectionMatrix = vmath::perspective(45.0f,
                                                        (GLfloat)width/(GLfloat)height,
                                                        0.1f,
                                                        100.0f);

        CGLUnlockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);
    }

    //InvalidateRect is similar to dirtyRect
    -(void)drawRect:(NSRect)dirtyRect
    {
        ///code
        ///due to multi threading support for OpenGL
        ///we have to call drawView() at two place to syncronised between main thread and other thread
        [self drawView];
    }

    -(void)drawView
    {
        ///code
        [[self openGLContext] makeCurrentContext];
        CGLLockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);

        //*************shader drawing code start********************
            mat4 modelViewMatrix;
            mat4 modelViewProjectionMatrix;
            mat4 translateMatrix;
            GLfloat  *rectPosition;

             GLfloat CheckerboardRectangle1[]=
            {
                -2.0f, -1.0f, 0.0f,
                -2.0f, 1.0f, 0.0f,
                0.0f, 1.0f, 0.0f,
                0.0f, -1.0f, 0.0f
            };
            GLfloat CheckerboardRectangle2[]=
            {
                1.0f, -1.0f, 0.0f,
                1.0f, 1.0f, 0.0f,
                2.41421f, 1.0f, -1.41421f,
                2.41421f, -1.0f, -1.41421f
            };
            //code
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

            glUseProgram(gShaderProgramObject);

                translateMatrix = vmath :: translate(0.0f,0.0f,-3.6f);
                modelViewMatrix = translateMatrix;
                modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

                glUniformMatrix4fv(mvpMatrixUniform,
                                    1,
                                    GL_FALSE,
                                    modelViewProjectionMatrix);

                
                glActiveTexture(GL_TEXTURE0);
                glBindTexture(GL_TEXTURE_2D, CheckerboardTextureID);
                glUniform1i(textureSamplerUniform,0);

                rectPosition=CheckerboardRectangle1;

                glBindVertexArray(vao_checkerboard);

                    glBindBuffer(GL_ARRAY_BUFFER,vbo_checkerboardPosition);
                        glBufferData(GL_ARRAY_BUFFER,
                                    3 * 4 *sizeof(GLfloat),
                                    rectPosition,
                                    GL_DYNAMIC_DRAW);
                        glVertexAttribPointer(VSV_ATTRIBUTE_POSITION,
                                                3,
                                                GL_FLOAT,
                                                GL_FALSE,
                                                0,
                                                NULL);
                        glEnableVertexAttribArray(VSV_ATTRIBUTE_POSITION);
                    glBindBuffer(GL_ARRAY_BUFFER,0);

                    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);

                glBindVertexArray(0);

                rectPosition=CheckerboardRectangle2;

                glBindVertexArray(vao_checkerboard);
                    
                    glBindBuffer(GL_ARRAY_BUFFER,vbo_checkerboardPosition);
                        glBufferData(GL_ARRAY_BUFFER,
                                    3 * 4 *sizeof(GLfloat),
                                    rectPosition,
                                    GL_DYNAMIC_DRAW);
                        glVertexAttribPointer(VSV_ATTRIBUTE_POSITION,
                                                3,
                                                GL_FLOAT,
                                                GL_FALSE,
                                                0,
                                                NULL);
                        glEnableVertexAttribArray(VSV_ATTRIBUTE_POSITION);
                    glBindBuffer(GL_ARRAY_BUFFER,0);

                    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);

                glBindVertexArray(0);

            glUseProgram(0);
        //*************shader drawing code end********************

        ///swapbuffer
        CGLFlushDrawable((CGLContextObj)[[self openGLContext] CGLContextObj]);

        CGLUnlockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);
    }

    ///to shift keyboard and mouse controll from NSwindow to NSOpenGLView
    ///NSReponder->NSobject method
    -(BOOL)acceptsFirstResponder
    {
        ///code
        [[self window] makeFirstResponder:self];

        return YES;
    }

    -(void)keyDown:(NSEvent*)theEvent
    {
        ///code
        int key = [[theEvent characters] characterAtIndex:0];
        switch(key)
        {
            case 27://escape
                [self release];
                [NSApp terminate:self];
                break;
            case 'f':
            case 'F':
                [[self window] toggleFullScreen:self];
                break;
        }
    }

    -(void)mouseDown:(NSEvent*)theEvent
    {
        ///code


    }

    -(void)rightMouseDown:(NSEvent*)theEvent
    {
        ///code

        
    }

    -(void)otherMouseDown:(NSEvent*)theEvent
    {
        ///code
    
    }

    -(void)LoadGLTexture
    {
        [self MakeCheckImage];
        
        glPixelStorei(GL_UNPACK_ALIGNMENT,
                    1);
        
        glGenTextures(1 ,
                    &CheckerboardTextureID);

        glBindTexture( GL_TEXTURE_2D,
                        CheckerboardTextureID);
        
        glTexParameteri( GL_TEXTURE_2D,
                        GL_TEXTURE_MAG_FILTER,
                        GL_NEAREST
                        );
        glTexParameteri( GL_TEXTURE_2D,
                        GL_TEXTURE_MIN_FILTER,
                        GL_NEAREST
                       );
        glTexParameteri( GL_TEXTURE_2D,
                        GL_TEXTURE_WRAP_S,
                        GL_REPEAT
                        );
        glTexParameteri( GL_TEXTURE_2D,
                        GL_TEXTURE_WRAP_T,
                        GL_REPEAT
                       );
    

        glTexImage2D(GL_TEXTURE_2D,
                    0,
                    GL_RGBA,
                    CHECK_IMAGE_WIDTH,
                    CHECK_IMAGE_HEIGHT,
                    0,
                    GL_RGBA,
                    GL_UNSIGNED_BYTE,
                    checkImage);

        glGenerateMipmap(GL_TEXTURE_2D);
    }

    -(void)MakeCheckImage
    {
        int vsv_i,vsv_j,vsv_c;

        for(vsv_i=0;vsv_i<CHECK_IMAGE_HEIGHT;vsv_i++){

            for(vsv_j=0;vsv_j<CHECK_IMAGE_WIDTH;vsv_j++){

                vsv_c=( (vsv_i & 0x8 )==0)^( (vsv_j & 0x8 )==0);
                vsv_c*=255;
                checkImage[vsv_i][vsv_j][0]=(GLubyte)vsv_c;
                checkImage[vsv_i][vsv_j][1]=(GLubyte)vsv_c;
                checkImage[vsv_i][vsv_j][2]=(GLubyte)vsv_c;
                checkImage[vsv_i][vsv_j][3]=255;
            }
        }
    }


    ///like destructor
    -(void)dealloc
    {
        ///*********Shader dealloc start******************************************************
        if(vao_checkerboard)
        {
            glDeleteVertexArrays(1,
                                &vao_checkerboard);
        }
        vao_checkerboard=0;

        if(vbo_checkerboardPosition)
        {
            glDeleteBuffers(1,
                        &vbo_checkerboardPosition);
        }
        vbo_checkerboardPosition=0;

        if(vbo_checkerboardTexcoord)
        {
            glDeleteBuffers(1,
                        &vbo_checkerboardTexcoord);
        }
        vbo_checkerboardTexcoord=0;

        if(gShaderProgramObject)
        {
            glUseProgram(gShaderProgramObject);

            GLsizei shaderCount;
            GLsizei tempShaderCount;
            GLuint *pShader=NULL;

            glGetProgramiv(gShaderProgramObject,GL_ATTACHED_SHADERS,&shaderCount);

            pShader=(GLuint *)malloc(sizeof(GLuint)*shaderCount);
            if(pShader==NULL)
            {
                fprintf(gpFile,"\nFailed to allocate memory for pShader");
            }else{
                glGetAttachedShaders(gShaderProgramObject,shaderCount,&tempShaderCount,pShader);
                
                fprintf(gpFile,"\nShader program has actual attached shader count is:%d",tempShaderCount);
                
                for(GLsizei i=0;i<shaderCount;i++)
                {
                    glDetachShader(gShaderProgramObject,pShader[i]);
                    glDeleteShader(pShader[i]);
                    pShader[i]=0;
                }
                free(pShader);
                
                glDeleteProgram(gShaderProgramObject);
                gShaderProgramObject=0;
                glUseProgram(0);
            }
        }

        glDeleteTextures(1,&CheckerboardTextureID);
        ///*********Shader dealloc end******************************************************

        CVDisplayLinkStop(displayLinkRef);
        CVDisplayLinkRelease(displayLinkRef);
        [super dealloc];
    }


@end


///As Callback function is C style func its should return in global space
///displayLinkContext will be MyOpenGLView obj which will be set by OS,when OS will call this function

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLinkRef,
                                const CVTimeStamp* now,
                                const CVTimeStamp* outputTime,
                                CVOptionFlags flagsIn,
                                CVOptionFlags* flagsOut,
                                void * displayLinkContext)
{
    CVReturn result = [(MyOpenGLView *)displayLinkContext  getFrameForTime:outputTime];
    return result;
}


