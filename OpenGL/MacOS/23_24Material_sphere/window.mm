#import<Foundation/Foundation.h> ///get foundation header file from foundation framework (similar to standard library like stdio.h)
#import<Cocoa/Cocoa.h>///analogus to window.h/xlib.h
#import<QuartzCore/CVDisplayLink.h>///CVDisplayLink mean core video
#import<OpenGL/gl3.h>///gl.h....shaders were added from 3.2 so gl3
#include"Sphere.h"
#include"vmath.h"
using namespace vmath;

enum{
    VSV_ATTRIBUTE_POSITION=0,
    VSV_ATTRIBUTE_COLOR,
    VSV_ATTRIBUTE_TEXCOORD,
    VSV_ATTRIBUTE_NORMAL,
};



GLfloat LightAmbient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat LightDefuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat LightSpecular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat LightPosition[] = { 0.0f,0.0f,0.0f,1.0f };

typedef struct Material {
    vec4 materialAmbient;
    vec4 materilaDefuse;
    vec4 materialSpecular;
    GLfloat materialShininess;
}MaterialStruct;

MaterialStruct Materials[24] = {
                        {
                            vec4(0.0215,0.1745,0.0215,1.0),
                            vec4(0.07568,0.61424,0.07568,1.0),
                            vec4(0.633,0.727811,0.633,1.0),
                            0.6 * 128
                        },
                        {
                            vec4(0.135,0.2225,0.1575,1.0),
                            vec4(0.54,0.89 ,0.63,1.0),
                            vec4(0.316228,0.316228,0.316228,1.0),
                            0.1 * 128
                        },
                        {
                            vec4(0.05375,0.05,0.06625,1.0),
                            vec4(0.18275,0.17,0.22525,1.0),
                            vec4(0.332741,0.328634,0.346435,1.0),
                            0.3 * 128
                        },{
                            vec4(0.25,0.20725,0.20725,1.0),
                            vec4(1.0,0.829,0.829,1.0),
                            vec4(0.296648,0.296648,0.296648,1.0),
                            0.088 * 128
                        },{
                            vec4(0.1745,0.01175,0.01175,1.0),
                            vec4(0.61424,0.04136,0.04136,1.0),
                            vec4(0.727811,0.626959,0.626959,1.0),
                            0.6 * 128
                        },{
                            vec4(0.1,0.18725,0.1745,1.0f),
                            vec4(0.396,0.74151,0.69102,1.0),
                            vec4(0.297254,0.30829,0.306678,1.0),
                            0.1 * 128
                        },{
                            vec4(0.329412,0.223529,0.027451,1.0),
                            vec4(0.780392,0.568627,0.113725,1.0),
                            vec4(0.992157,0.941176,0.807843,1.0),
                            0.21794872 * 128
                        },{
                            vec4(0.2125,0.1275,0.054,1.0f),
                            vec4(0.714,0.4284,0.18144,1.0f),
                            vec4(0.393548,0.271906,0.166721,1.0),
                            0.2 * 128
                        },{
                            vec4(0.25,0.25,0.25,1.0),
                            vec4(0.4,0.4,0.4,1.0),
                            vec4(0.774597,0.774597,0.774597,1.0),
                            0.6 * 128
                        },{
                            vec4(0.19125,0.0735,0.0225,1.0),
                            vec4(0.7038,0.27048,0.0828,1.0),
                            vec4(0.256777,0.137622,0.086014,1.0),
                            0.1 * 128
                        },{
                            vec4(0.24725,0.1995,0.0745,1.0),
                            vec4(0.75164,0.60648,0.22648,1.0),
                            vec4(0.628281,0.555802,0.366065,1.0),
                            0.4 * 128
                        },{
                            vec4(0.19225,0.19225,0.19225,1.0),
                            vec4(0.50754,0.50754,0.50754,1.0),
                            vec4(0.508273,0.508273,0.508273,1.0),
                            0.4 * 128
                        },{
                            vec4(0.0,0.0,0.0,1.0),
                            vec4(0.01,0.01,0.01,1.0),
                            vec4(0.50,0.50,0.50,1.0),
                            0.25 * 128
                        },{
                            vec4(0.0,0.1,0.06,1.0),
                            vec4(0.0,0.50980392,0.50980392,1.0),
                            vec4(0.50196078,0.50196078,0.50196078,1.0),
                            0.25 * 128
                        },{
                            vec4(0.0,0.0,0.0,1.0),
                            vec4(0.1,0.35,0.1,1.0),
                            vec4(0.45,0.55,0.45,1.0),
                            0.25 * 128
                        },{
                            vec4(0.0,0.0,0.0,1.0),
                            vec4(0.5,0.0,0.0,1.0),
                            vec4(0.7,0.6,0.6,1.0),
                            0.25 * 128
                        },{
                            vec4(0.0,0.0,0.0,1.0),
                            vec4(0.55,0.55,0.55,1.0),
                            vec4(0.70,0.70,0.70,1.0),
                            0.25 * 128
                        },{
                            vec4(0.0,0.0,0.0,1.0),
                            vec4(0.5,0.5,0.0,1.0),
                            vec4(0.60,0.60,0.60,1.0),
                            0.25 * 128
                        },{
                            vec4(0.02,0.02,0.02,1.0),
                            vec4(0.01,0.01,0.01,1.0),
                            vec4(0.4,0.4,0.4,1.0),
                            0.078125 * 128
                        },{
                            vec4(0.0,0.05,0.05,1.0),
                            vec4(0.4,0.5,0.5,1.0),
                            vec4(0.04,0.7,0.7,1.0),
                            0.078125 * 128
                        }, {
                            vec4(0.0,0.05,0.0,1.0),
                            vec4(0.4,0.5,0.4,1.0),
                            vec4(0.04,0.7,0.04,1.0f),
                            0.078125 * 128
                        }, {
                            vec4(0.05,0.0,0.0,1.0),
                            vec4(0.5,0.4,0.4,1.0),
                            vec4(0.7,0.04,0.04,1.0),
                            0.078125 * 128
                        }, {
                            vec4(0.05,0.05,0.05,1.0),
                            vec4(0.5,0.5,0.5,1.0),
                            vec4(0.7,0.7,0.7,1.0),
                            0.078125 * 128
                        }, {
                            vec4(0.05,0.05,0.0,1.0),
                            vec4(0.5,0.5,0.4,1.0),
                            vec4(0.7,0.7,0.04,1.0),
                            0.078125 * 128
                        }
                            };

//callback
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef,const CVTimeStamp*,const CVTimeStamp*,CVOptionFlags,CVOptionFlags*,void *);

FILE *gpFile = NULL;

///mkdir -p Window.app/Contents/MacOS
///clang++ -Wno-deprecated-declarations -o Window.app/Contents/MacOS/window Window.mm -framework Cocoa -framework QuartzCore -framework OpenGL

///NeXT STEP (NS)

/// our class AppDelegate is inheritated from NSObject class and it will implement NSApplicationDelegate and NSWindowDelegate interface
/// : is extends and <> are implements
///we will define method later...its forward declaration
@interface AppDelegate:NSObject<NSApplicationDelegate, NSWindowDelegate>
@end

int main(int argc,char* argv[])
{
    ///code
    ///NSAutoreleasePool will hold all alocated memory
    NSAutoreleasePool *pool=[[NSAutoreleasePool alloc] init];
    ///NSApp is inbuild global NS application object
    ///we get to access shared NS Application object
    NSApp =[NSApplication sharedApplication];

    ///setting NSApp method implementation to our AppDelegate object
    [NSApp setDelegate:[[AppDelegate alloc] init] ];

    ///Message/run/event loop
    [NSApp run];

    ///releaseing every obj memory from pool
    [pool release];

    return 0;
}

///
@interface MyOpenGLView:NSOpenGLView
@end

//implmentating method for AppDelegate
@implementation AppDelegate
    {
        @private ///private variable
        NSWindow *window;
        MyOpenGLView *myOpenGLview;
    }

    ///when application is getting started, then applicationDidFinishLaunching() a method of NSApplication is called
    ///WM_Create and applicationDidFinishLaunching() are analogus
    -(void)applicationDidFinishLaunching:(NSNotification *)aNotification
    {
        ///code
        //window.app is same as NSBundle(its for recongnising window.app in foundation.h)
        NSBundle *appBundle = [NSBundle mainBundle];
        ///storing window.app dir path
        NSString *appDirPath = [appBundle bundlePath];

        /*
        eg . if appDirPath is /usr/viroje/Desktop/assignment/02LogFile/window.app
            then stringByDeletingLastPathComponent will perform
        then parentDirPath will be /usr/viroje/Desktop/assignment/02LogFile
        */
        NSString *parentDirPath = [appDirPath stringByDeletingLastPathComponent];

        NSString *logFileNameWithPath = [NSString stringWithFormat:@"%@/Log.txt",parentDirPath];

        const char *pSzLogFileNameWithPath =  [logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];

        gpFile = fopen(pSzLogFileNameWithPath,"w");
        if(gpFile==NULL)
        {
            [self release];
            [NSApp terminate:self];
        }

        fprintf(gpFile,"\nProgram Started Successfully");
        fflush(gpFile);

        ///NSRect is internally CGRect -core graphic ..its c based library
        ///CGPoint/NSPoint-x,y and CGSize/NSSize-width,height
        NSRect winRect = NSMakeRect(0.0,0.0,800.0,600.0);

        ///styleMask is similer to WM_overlappedWindow
        ///backing- how to store window in graphics
        ///defer - to halt showing of window or show window immediately
        window = [[NSWindow alloc] initWithContentRect:winRect
                                                    styleMask:NSWindowStyleMaskTitled |
                                                            NSWindowStyleMaskClosable |
                                                            NSWindowStyleMaskMiniaturizable |
                                                            NSWindowStyleMaskResizable
                                                            backing:NSBackingStoreBuffered
                                                            defer:NO];
        
        [window setTitle:@"OpenGL : 24 Material Sphere"];
        [window center];

        ///FrameSize will be same as windowSize
        myOpenGLview = [[MyOpenGLView alloc] initWithFrame:winRect];
        ///assigning Frame to our window
        [window setContentView:myOpenGLview];

        ///NSWindow method will be implemented by itself
        ///if not use this line then app will not removed from taskbar
        [window setDelegate:self];

        ///its like setFocus() and setForgroundWindow() or MB_TOPMOST
        [window makeKeyAndOrderFront:self];
    }

    ///when application is gets terminated, then applicationWillTerminate() a method of NSApplication is called
    ///WM_Destroy and applicationWillTerminate() are analogus
    /// [NSApp terminate:self] is like WM_QUIT
    ///  when this line is called [NSApp terminate:self];  ...then applicationWillTerminate() is called ...then it exit Message/run/event loop
    -(void)applicationWillTerminate:(NSNotification *)aNotification
    {
        ///code

        if(gpFile)
        {
            fprintf(gpFile,"\nProgram terminated Successfully");
            fflush(gpFile);
            fclose(gpFile);
        }
        gpFile=NULL;
    }

    ///NSWindowDelegate method
    -(void)windowWillClose:(NSNotification *)aNotification
    {
        ///code
        [NSApp terminate:self];
    }

    ///like destructor
    -(void)dealloc
    {
        ///code
        [myOpenGLview release];
        [window release];
        [super dealloc];
    }

@end

//implmentating method for MyOpenGLView
@implementation MyOpenGLView
    {
        @private ///private variable
        CVDisplayLinkRef displayLinkRef;

        int RotationType;//x-0,y-1,z-2

        GLsizei orignal_width;
        GLsizei orignal_height;


        //-------------PF---------------
        GLuint gShaderProgramObject;

        GLuint modelMatrixUniform;
        GLuint viewMatrixUniform;
        GLuint projectionlMatrixUniform;

        GLuint lightAmbientUniform;
        GLuint lightDiffuseUniform;
        GLuint lightSpecularUniform;
        GLuint lightPositionUniform;

        GLuint materialAmbientUniform;
        GLuint materialDiffuseUniform;
        GLuint materialSpecularUniform;
        GLuint materialShininessUniform;

        GLuint LKeyPressedUniform;

        //-----------comman------------
        GLuint gVertexShaderObject;
        GLuint gFragmentShaderObject;

        GLuint vaoSphere;
        GLuint vboSphere_position;
        GLuint vboSphere_normal;
        GLuint vboSphere_elements;

        bool bLights;

        mat4 perspectiveProjectionMatrix;

        float spherePosition[1146];
        float sphereNormals[1146];
        float sphereTexcoord[764];
        short sphereElement[2280];//Face Indices

        GLuint gNumSphereVertices, gNumSphereElements;

        GLfloat Angle;

    }

    ///id is a object of class
    -(id)initWithFrame:(NSRect)frame
    {
        ///code
        self = [super initWithFrame:frame];
        if (self)
        {
            //array of NSOpenGLPixelFormatAttribute
            //its like pixelformatdiscriptor/framebufferattribute
            NSOpenGLPixelFormatAttribute attributes[] = {
                                                            NSOpenGLPFAOpenGLProfile,NSOpenGLProfileVersion4_1Core,
                                                            NSOpenGLPFAScreenMask,CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay), ///Display/monitor  id/type
                                                            NSOpenGLPFANoRecovery,/// canceling automatic shifting process from h/w rendering to s/w rendering ,if h/w rendering context not found
                                                            NSOpenGLPFAAccelerated,/// to use h/w rendering context
                                                            NSOpenGLPFAColorSize,24,//RGB
                                                            NSOpenGLPFADepthSize,24,
                                                            NSOpenGLPFAAlphaSize,8,
                                                            NSOpenGLPFADoubleBuffer,
                                                            0
                                                        };
            ///autorelease = release on your own in auto release pool
            ///pixelFormat its like choosePixelFormat
            NSOpenGLPixelFormat *pixelFormat = [[[NSOpenGLPixelFormat alloc] initWithAttributes:attributes] autorelease];
            if (pixelFormat==nil)
            {
                fprintf(gpFile,"\npixelFormat is nil.exited");
                fflush(gpFile);
                [self release];
                [NSApp terminate:self];
            }

            //shareContext = from linux
            NSOpenGLContext *glContext = [[[NSOpenGLContext alloc] initWithFormat:pixelFormat shareContext:nil] autorelease];

            //set pixelformat
            [self setPixelFormat:pixelFormat];

            //set content
            [self setOpenGLContext:glContext];
        }

        return self;
    }

    -(CVReturn)getFrameForTime:(const CVTimeStamp*)outputTime
    {
        ///code
        ///Apple OpenGL supports multi threading
        ///pool in main() is main thread and pool in getFrameForTime() are other thread
        ///this pool is renderings auto release pool
        NSAutoreleasePool *pool=[[NSAutoreleasePool alloc] init];

        ///to draw our shaders
        [self drawView];

        ///releasing renderings auto release pool
        [pool release];

        return(kCVReturnSuccess);
    }

    -(void)prepareOpenGL
    {
        ///code
        [super prepareOpenGL];

        ///Objective c does not add `get` as prefix for getter methods
        ///Objective c does add `set` as prefix for setter methods
        ///it gives opengl context and set to current context
        [[self openGLContext] makeCurrentContext];

        ///Swap interval code below
        ///CP=context parameter
        GLint swapInt = 1;
        [[self openGLContext] setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];

        fprintf(gpFile,"\nOpenGL Vender:%s",glGetString(GL_VENDOR));
        fprintf(gpFile,"\nOpenGL Render:%s",glGetString(GL_RENDERER));
        fprintf(gpFile,"\nOpenGL Version:%s",glGetString(GL_VERSION));
        fprintf(gpFile,"\nOpenGL GLSL Version:%s",glGetString(GL_SHADING_LANGUAGE_VERSION));

        ///****************shader codes start*****************************

              
        GLint infoLogLength;
        GLint shaderCompileStatus;
        GLint shaderProgramLinkStatus;
        GLchar *szBuffer;
        GLsizei written;

        //Per Fragment Shader Code Start--------------------
        gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

        const GLchar* vertexShaderSourceCode_PF =
            "#version 410 core\n"\
            "\n"\
            "in vec4 vPosition;\n"\
            "in vec3 vNormal;\n"\

            "uniform mat4 u_modelMatrix;\n"\
            "uniform mat4 u_viewMatrix;\n"\
            "uniform mat4 u_projectionMatrix;\n"\

            "uniform vec4 u_lightPosition;\n"\

            "uniform int u_lKeyPressed;\n"\

            "out vec3 out_transformedNormal;\n"\
            "out vec3 out_lightDirection;\n"\
            "out vec3 out_viewVector;\n"\

            "void main(void)\n"\

            "{\n"\
                "if (u_lKeyPressed == 1)\n"\
                "{\n"\

                    "vec4 eyeCoord = u_viewMatrix * u_modelMatrix * vPosition;\n"\

                    "out_viewVector = -eyeCoord.xyz;\n"\

                    "out_transformedNormal = mat3(u_viewMatrix * u_modelMatrix) * vNormal ;\n"\

                    "out_lightDirection =  vec3( u_lightPosition - eyeCoord ) ;\n"\

                "}\n"\
            
                "gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix * vPosition;\n"\

            "}";

        glShaderSource(gVertexShaderObject,
            1,
            (const GLchar**)&vertexShaderSourceCode_PF,
            NULL);

        glCompileShader(gVertexShaderObject);

        glGetShaderiv(gVertexShaderObject,
            GL_COMPILE_STATUS,
            &shaderCompileStatus);

        if (shaderCompileStatus == GL_FALSE)
        {
            glGetShaderiv(gVertexShaderObject,
                GL_INFO_LOG_LENGTH,
                &infoLogLength);

            if (infoLogLength > 0)
            {
                szBuffer = (GLchar*)malloc(sizeof(GLchar) * infoLogLength);
                if (szBuffer != NULL)
                {
                    glGetShaderInfoLog(gVertexShaderObject,
                        infoLogLength,
                        &written,
                        szBuffer);
                    fprintf(gpFile, "\nERROR:Vertex Shader Object Compilation Failed log : %s", szBuffer);
                    free(szBuffer);
                    [self release];
                [NSApp terminate:self];
                }
            }
        }

        infoLogLength = 0;
        shaderCompileStatus = 0;
        shaderProgramLinkStatus = 0;
        szBuffer = NULL;
        written = 0;

        gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

        const GLchar* fragmentShaderSourceCode_PF =
            "#version 410 core"\
            "\n"\

            "uniform vec3 u_lightAmbient;\n"\
            "uniform vec3 u_lightDiffuse;\n"\
            "uniform vec3 u_lightSpecular;\n"\

            "uniform vec3 u_materialAmbient;\n"\
            "uniform vec3 u_materialDiffuse;\n"\
            "uniform vec3 u_materialSpecular;\n"\
            "uniform float u_materialShininess;\n"\

            "uniform int u_lKeyPressed;\n"\

            "in vec3 out_transformedNormal;\n"\
            "in vec3 out_lightDirection;\n"\
            "in vec3 out_viewVector;\n"\

            "vec3 phongADS_Light;\n"
            "out vec4 fragColor;"\

            "void main(void)"\
            "{"\

                "if (u_lKeyPressed == 1)\n"\
                "{\n"\

                    "vec3 normalizedTransformedNormal = normalize( out_transformedNormal );\n"\
                    "vec3 normalizedLightDirection = normalize( out_lightDirection );\n"\
                    "vec3 normalizedViewVector = normalize( out_viewVector );\n"\

                    "vec3 reflectionVector = reflect( -normalizedLightDirection, normalizedTransformedNormal );\n"\

                    "vec3 ambient= u_lightAmbient * u_materialAmbient;\n"\

                    "vec3 diffuse = u_lightDiffuse * u_materialDiffuse * max( dot(normalizedLightDirection , normalizedTransformedNormal), 0.0);\n"\

                    "vec3 specular = u_lightSpecular * u_materialSpecular * pow( max( dot(reflectionVector , normalizedViewVector), 0.0f) , u_materialShininess);\n"\

                    "phongADS_Light = ambient + diffuse + specular;\n"\

                "}\n"\
                "else\n"\
                "{\n"\
                    "phongADS_Light=vec3(1.0f,1.0f,1.0f);\n"\
                "}\n"\

                "fragColor = vec4( phongADS_Light, 1.0f );"\
            "}";

        glShaderSource(gFragmentShaderObject,
            1,
            (const GLchar**)&fragmentShaderSourceCode_PF,
            NULL);

        glCompileShader(gFragmentShaderObject);

        glGetShaderiv(gFragmentShaderObject,
            GL_COMPILE_STATUS,
            &shaderCompileStatus);

        if (shaderCompileStatus == GL_FALSE)
        {
            glGetShaderiv(gFragmentShaderObject,
                GL_INFO_LOG_LENGTH,
                &infoLogLength);

            if (infoLogLength > 0)
            {
                szBuffer = (GLchar*)malloc(sizeof(GLchar) * infoLogLength);
                if (szBuffer != NULL)
                {
                    glGetShaderInfoLog(gFragmentShaderObject,
                        infoLogLength,
                        &written,
                        szBuffer);
                    fprintf(gpFile, "\nERROR:Fragment Shader Object Compilation Failed log : %s", szBuffer);
                    free(szBuffer);
                    [self release];
                [NSApp terminate:self];
                }
            }
        }

        infoLogLength = 0;
        shaderCompileStatus = 0;
        shaderProgramLinkStatus = 0;
        szBuffer = NULL;
        written = 0;

        gShaderProgramObject = glCreateProgram();

        glAttachShader(gShaderProgramObject, gVertexShaderObject);
        glAttachShader(gShaderProgramObject, gFragmentShaderObject);

        //Shader Attribute Binding Before Pre Linking of attached Shader
        glBindAttribLocation(gShaderProgramObject,
            VSV_ATTRIBUTE_POSITION,
            "vPosition");
        glBindAttribLocation(gShaderProgramObject,
            VSV_ATTRIBUTE_NORMAL,
            "vNormal");

        glLinkProgram(gShaderProgramObject);

        glGetProgramiv(gShaderProgramObject,
            GL_LINK_STATUS,
            &shaderProgramLinkStatus);

        if (shaderProgramLinkStatus == GL_FALSE)
        {
            glGetProgramiv(gShaderProgramObject,
                GL_INFO_LOG_LENGTH,
                &infoLogLength);
            if (infoLogLength > 0)
            {
                szBuffer = (GLchar*)malloc(sizeof(GLchar) * infoLogLength);

                if (szBuffer != NULL)
                {
                    glGetProgramInfoLog(gShaderProgramObject,
                        infoLogLength,
                        &written,
                        szBuffer);

                    fprintf(gpFile, "\nERROR:Shader program Link failed log : %s", szBuffer);
                    free(szBuffer);
                    [self release];
                [NSApp terminate:self];
                }
            }
        }

        //Shader Code End--------------------


        //Uniform variable Binding after post linking of attached shader
        modelMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_modelMatrix");
        viewMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_viewMatrix");
        projectionlMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_projectionMatrix");

        
        lightAmbientUniform = glGetUniformLocation(gShaderProgramObject, "u_lightAmbient");
        lightDiffuseUniform = glGetUniformLocation(gShaderProgramObject, "u_lightDiffuse");
        lightSpecularUniform = glGetUniformLocation(gShaderProgramObject, "u_lightSpecular");
        lightPositionUniform = glGetUniformLocation(gShaderProgramObject, "u_lightPosition");
        
        materialAmbientUniform = glGetUniformLocation(gShaderProgramObject, "u_materialAmbient");
        materialDiffuseUniform = glGetUniformLocation(gShaderProgramObject, "u_materialDiffuse");
        materialSpecularUniform = glGetUniformLocation(gShaderProgramObject, "u_materialSpecular");
        materialShininessUniform = glGetUniformLocation(gShaderProgramObject, "u_materialShininess");

        LKeyPressedUniform = glGetUniformLocation(gShaderProgramObject, "u_lKeyPressed");

        //VAO and its respective VBO preparation

            Sphere *sphere=new Sphere();
            sphere->sphereInitialize();
            
            sphere->getSphereVertexData(spherePosition, sphereNormals, sphereTexcoord, sphereElement);
            gNumSphereVertices = sphere->getNumberOfSphereVertices();
            gNumSphereElements = sphere->getNumberOfSphereElements();


            glGenVertexArrays(1,&vaoSphere);
            glBindVertexArray(vaoSphere);

                glGenBuffers(1,&vboSphere_position);
                glBindBuffer(GL_ARRAY_BUFFER, vboSphere_position);

                    glBufferData(GL_ARRAY_BUFFER,
                                sizeof(spherePosition),
                                spherePosition,
                                GL_STATIC_DRAW);

                    glVertexAttribPointer(VSV_ATTRIBUTE_POSITION,
                                        3,
                                        GL_FLOAT,
                                        GL_FALSE,
                                        0,
                                        NULL);

                    glEnableVertexAttribArray(VSV_ATTRIBUTE_POSITION);
                
                glBindBuffer(GL_ARRAY_BUFFER,0);

                glGenBuffers(1, &vboSphere_normal);
                glBindBuffer(GL_ARRAY_BUFFER, vboSphere_normal);

                glBufferData(GL_ARRAY_BUFFER,
                    sizeof(sphereNormals),
                    sphereNormals,
                    GL_STATIC_DRAW);

                glVertexAttribPointer(VSV_ATTRIBUTE_NORMAL,
                    3,
                    GL_FLOAT,
                    GL_FALSE,
                    0,
                    NULL);

                glEnableVertexAttribArray(VSV_ATTRIBUTE_NORMAL);

                glBindBuffer(GL_ARRAY_BUFFER, 0);

                glGenBuffers(1,
                    &vboSphere_elements);

                glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,
                    vboSphere_elements);

                    glBufferData(GL_ELEMENT_ARRAY_BUFFER,
                        sizeof(sphereElement),
                        sphereElement,
                        GL_STATIC_DRAW);

                glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,
                    0);

            glBindVertexArray(0);

        glClearColor(0.5,0.5,0.5f,0.0f);

        glClearDepth(1.0f);
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);

        perspectiveProjectionMatrix=mat4::identity();
        ///****************shader codes end*****************************

        ///-----Setup code for DisplayLink and its Callback (its CV and CG related code )----------------------------
        /* CGL-core graphics library
            1.Create DisplayLink
            2.set Callback function for DisplayLink
            3.Convert OpenGLContext to CGL context
            4.Convert OpenGLPixelFormat to CGL PixelFormat
            5.set current displays CGL context and CGL PixelFormat
            6.start DisplayLink
        */

        //1.Create DisplayLink
        CVDisplayLinkCreateWithActiveCGDisplays(&displayLinkRef);
        
        //2.set Callback function for DisplayLink
        //displayLinkContext is self
        CVDisplayLinkSetOutputCallback(displayLinkRef,&MyDisplayLinkCallback,self);
        
        //3.Convert OpenGLContext to CGL context
        CGLContextObj cglContext = (CGLContextObj)[[self openGLContext] CGLContextObj];

        //4.Convert OpenGLPixelFormat to CGL PixelFormat
        CGLPixelFormatObj cglPixelFormat = (CGLPixelFormatObj)[[self pixelFormat] CGLPixelFormatObj];

        // 5.set current displays CGL context and CGL PixelFormat
        CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLinkRef,cglContext,cglPixelFormat);

        //6.start DisplayLink
        CVDisplayLinkStart(displayLinkRef);

    }

    -(void)reshape
    {
        ///code
        [super reshape];

        CGLLockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);

        NSRect rect = [self bounds];

        if (rect.size.height < 0)
        {
            rect.size.height = 1;
        }

        glViewport(0,0,(GLsizei)rect.size.width,(GLsizei)rect.size.height);

        int width = rect.size.width;
        int height = rect.size.height;

        orignal_width = (GLsizei)width;
        orignal_height = (GLsizei)height;

        perspectiveProjectionMatrix=vmath::perspective(45.0f,
                                                        (GLfloat)width/(GLfloat)height,
                                                        0.1f,
                                                        100.0f);

        CGLUnlockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);
    }

    //InvalidateRect is similar to dirtyRect
    -(void)drawRect:(NSRect)dirtyRect
    {
        ///code
        ///due to multi threading support for OpenGL
        ///we have to call drawView() at two place to syncronised between main thread and other thread
        [self drawView];
    }

    -(void)drawView
    {
        ///code
        [[self openGLContext] makeCurrentContext];
        CGLLockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);

        //*************shader drawing code start********************

        GLfloat radius = 3.0f;
            //code
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


        glUseProgram(gShaderProgramObject);
            mat4 translateMatrix;
            mat4 rotateMatrix;

            mat4 modelMatrix;
            mat4 viewMatrix = mat4::identity();

            GLfloat translate[3] = { 0.0f,0.0f,-1.5f };
            int k = 0;

            GLsizei unit_width = orignal_width / 6;
            GLsizei unit_height = orignal_height / 4;

            for (int i = 0; i < 4; i++)
            {

                for (int j = 0; j < 6; j++)
                {

                    glViewport(j * unit_width, i * unit_height, unit_width, orignal_height / 6);
                    //glViewport(0, 0, orignal_width, orignal_height);

                    translateMatrix = vmath::translate(translate[0], translate[1], translate[2]);
                    modelMatrix = translateMatrix;

                    glUniformMatrix4fv(modelMatrixUniform,
                        1,
                        GL_FALSE,
                        modelMatrix);

                    glUniformMatrix4fv(viewMatrixUniform,
                        1,
                        GL_FALSE,
                        viewMatrix);

                    glUniformMatrix4fv(projectionlMatrixUniform,
                        1,
                        GL_FALSE,
                        perspectiveProjectionMatrix);

                    if (bLights == true)
                    {
                        glUniform1i(LKeyPressedUniform, 1);

                        glUniform3fv(lightAmbientUniform, 1, LightAmbient);
                        glUniform3fv(lightDiffuseUniform, 1, LightDefuse);
                        glUniform3fv(lightSpecularUniform, 1, LightSpecular);

                        if (RotationType == 1) {

                            LightPosition[0] = 0.0f;
                            LightPosition[1] = radius * sin(Angle);
                            LightPosition[2] = radius * cos(Angle);
                            LightPosition[3] = 1.0f;

                        }
                        else if (RotationType == 2) {

                            LightPosition[0] = radius * sin(Angle);
                            LightPosition[1] = 0.0f;
                            LightPosition[2] = radius * cos(Angle);
                            LightPosition[3] = 1.0f;

                        }
                        else if (RotationType == 3) {

                            LightPosition[0] = radius * sin(Angle);
                            LightPosition[1] = radius * cos(Angle);
                            LightPosition[2] = 0.0f,
                            LightPosition[3] = 1.0f;

                        }
                        else if (RotationType == 0)
                        {
                            LightPosition[0] = 3.0f;
                            LightPosition[1] = 3.0f;
                            LightPosition[2] = 3.0f,
                            LightPosition[3] = 1.0f;
                        }

                        glUniform4fv(lightPositionUniform, 1, LightPosition);//posiitional light


                        glUniform3fv(materialAmbientUniform, 1, Materials[k].materialAmbient);
                        glUniform3fv(materialDiffuseUniform, 1, Materials[k].materilaDefuse);
                        glUniform3fv(materialSpecularUniform, 1, Materials[k].materialSpecular);

                        glUniform1f(materialShininessUniform, Materials[k].materialShininess);
                    }
                    else
                    {
                        glUniform1i(LKeyPressedUniform, 0);
                    }

                    glBindVertexArray(vaoSphere);
                        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboSphere_elements);
                        glDrawElements(GL_TRIANGLES, gNumSphereElements, GL_UNSIGNED_SHORT, 0);
                        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
                    glBindVertexArray(0);

                    k++;
                }

            }
        
        glUseProgram(0);

        Angle += 0.05f;

        //*************shader drawing code end********************

        ///swapbuffer
        CGLFlushDrawable((CGLContextObj)[[self openGLContext] CGLContextObj]);

        CGLUnlockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);
    }

    ///to shift keyboard and mouse controll from NSwindow to NSOpenGLView
    ///NSReponder->NSobject method
    -(BOOL)acceptsFirstResponder
    {
        ///code
        [[self window] makeFirstResponder:self];

        return YES;
    }

    -(void)keyDown:(NSEvent*)theEvent
    {
        ///code
        int key = [[theEvent characters] characterAtIndex:0];
        switch(key)
        {
             case 27://escape
                [self release];
                [NSApp terminate:self];
                break;
            case 'f':
            case 'F':
                [[self window] toggleFullScreen:self];
                break;
            case 'l':
            case 'L':
                bLights = !bLights;
                break;
            case 'x':
            case 'X':
                RotationType = 1;
                break;
            case 'y':
            case 'Y':
                RotationType = 2;
                break;
            case 'z':
            case 'Z':
                RotationType = 3;
                break;
            default:
                RotationType = 0;
                break;
        }
    }

    -(void)mouseDown:(NSEvent*)theEvent
    {
        ///code


    }

    -(void)rightMouseDown:(NSEvent*)theEvent
    {
        ///code

        
    }

    -(void)otherMouseDown:(NSEvent*)theEvent
    {
        ///code
    
    }

    ///like destructor
    -(void)dealloc
    {
        ///*********Shader dealloc start******************************************************
        
        if(vaoSphere)
        {
            glDeleteVertexArrays(1,
                                &vaoSphere);
        }
        vaoSphere = 0;

        if(vboSphere_position)
        {
            glDeleteBuffers(1,
                        &vboSphere_position);
        }
        vboSphere_position = 0;
        
        if (vboSphere_normal)
        {
            glDeleteBuffers(1,
                &vboSphere_normal);
        }
        vboSphere_normal = 0;

        if (vboSphere_elements)
        {
            glDeleteBuffers(1,
                &vboSphere_elements);
        }
        vboSphere_elements = 0;

        if(gShaderProgramObject)
        {
            glUseProgram(gShaderProgramObject);

            GLsizei shaderCount;
            GLsizei tempShaderCount;
            GLuint *pShader=NULL;

            glGetProgramiv(gShaderProgramObject,GL_ATTACHED_SHADERS,&shaderCount);

            pShader=(GLuint *)malloc(sizeof(GLuint)*shaderCount);
            if(pShader==NULL)
            {
                fprintf(gpFile,"\nFailed to allocate memory for pShader");
            }else{
                glGetAttachedShaders(gShaderProgramObject,shaderCount,&tempShaderCount,pShader);
                
                fprintf(gpFile,"\nShader program has actual attached shader count is:%d",tempShaderCount);
                
                for(GLsizei i=0;i<shaderCount;i++)
                {
                    glDetachShader(gShaderProgramObject,pShader[i]);
                    glDeleteShader(pShader[i]);
                    pShader[i]=0;
                }
                free(pShader);
                
                glDeleteProgram(gShaderProgramObject);
                gShaderProgramObject =0;
            }
            glUseProgram(0);
        }


        ///*********Shader dealloc end******************************************************

        CVDisplayLinkStop(displayLinkRef);
        CVDisplayLinkRelease(displayLinkRef);
        [super dealloc];
    }


@end


///As Callback function is C style func its should return in global space
///displayLinkContext will be MyOpenGLView obj which will be set by OS,when OS will call this function

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLinkRef,
                                const CVTimeStamp* now,
                                const CVTimeStamp* outputTime,
                                CVOptionFlags flagsIn,
                                CVOptionFlags* flagsOut,
                                void * displayLinkContext)
{
    CVReturn result = [(MyOpenGLView *)displayLinkContext  getFrameForTime:outputTime];
    return result;
}
