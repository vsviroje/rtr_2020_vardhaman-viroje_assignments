#import<Foundation/Foundation.h> ///get foundation header file from foundation framework (similar to standard library like stdio.h)
#import<Cocoa/Cocoa.h>///analogus to window.h/xlib.h
#import<QuartzCore/CVDisplayLink.h>///CVDisplayLink mean core video
#import<OpenGL/gl3.h>///gl.h....shaders were added from 3.2 so gl3

#include "Sphere.h"
#include"vmath.h"
using namespace vmath;

enum{
    VSV_ATTRIBUTE_POSITION=0,
    VSV_ATTRIBUTE_COLOR,
    VSV_ATTRIBUTE_TEXCOORD,
    VSV_ATTRIBUTE_NORMAL,
};

//callback
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef,const CVTimeStamp*,const CVTimeStamp*,CVOptionFlags,CVOptionFlags*,void *);

FILE *gpFile = NULL;

///mkdir -p Window.app/Contents/MacOS
///clang++ -Wno-deprecated-declarations -o Window.app/Contents/MacOS/window Window.mm -framework Cocoa -framework QuartzCore -framework OpenGL

///NeXT STEP (NS)

/// our class AppDelegate is inheritated from NSObject class and it will implement NSApplicationDelegate and NSWindowDelegate interface
/// : is extends and <> are implements
///we will define method later...its forward declaration
@interface AppDelegate:NSObject<NSApplicationDelegate, NSWindowDelegate>
@end

int main(int argc,char* argv[])
{
    ///code
    ///NSAutoreleasePool will hold all alocated memory
    NSAutoreleasePool *pool=[[NSAutoreleasePool alloc] init];
    ///NSApp is inbuild global NS application object
    ///we get to access shared NS Application object
    NSApp =[NSApplication sharedApplication];

    ///setting NSApp method implementation to our AppDelegate object
    [NSApp setDelegate:[[AppDelegate alloc] init] ];

    ///Message/run/event loop
    [NSApp run];

    ///releaseing every obj memory from pool
    [pool release];

    return 0;
}

///
@interface MyOpenGLView:NSOpenGLView
@end

//implmentating method for AppDelegate
@implementation AppDelegate
    {
        @private ///private variable
        NSWindow *window;
        MyOpenGLView *myOpenGLview;
    }

    ///when application is getting started, then applicationDidFinishLaunching() a method of NSApplication is called
    ///WM_Create and applicationDidFinishLaunching() are analogus
    -(void)applicationDidFinishLaunching:(NSNotification *)aNotification
    {
        ///code
        //window.app is same as NSBundle(its for recongnising window.app in foundation.h)
        NSBundle *appBundle = [NSBundle mainBundle];
        ///storing window.app dir path
        NSString *appDirPath = [appBundle bundlePath];

        /*
        eg . if appDirPath is /usr/viroje/Desktop/assignment/02LogFile/window.app
            then stringByDeletingLastPathComponent will perform
        then parentDirPath will be /usr/viroje/Desktop/assignment/02LogFile
        */
        NSString *parentDirPath = [appDirPath stringByDeletingLastPathComponent];

        NSString *logFileNameWithPath = [NSString stringWithFormat:@"%@/Log.txt",parentDirPath];

        const char *pSzLogFileNameWithPath =  [logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];

        gpFile = fopen(pSzLogFileNameWithPath,"w");
        if(gpFile==NULL)
        {
            [self release];
            [NSApp terminate:self];
        }

        fprintf(gpFile,"\nProgram Started Successfully");
        fflush(gpFile);

        ///NSRect is internally CGRect -core graphic ..its c based library
        ///CGPoint/NSPoint-x,y and CGSize/NSSize-width,height
        NSRect winRect = NSMakeRect(0.0,0.0,800.0,600.0);

        ///styleMask is similer to WM_overlappedWindow
        ///backing- how to store window in graphics
        ///defer - to halt showing of window or show window immediately
        window = [[NSWindow alloc] initWithContentRect:winRect
                                                    styleMask:NSWindowStyleMaskTitled |
                                                            NSWindowStyleMaskClosable |
                                                            NSWindowStyleMaskMiniaturizable |
                                                            NSWindowStyleMaskResizable
                                                            backing:NSBackingStoreBuffered
                                                            defer:NO];
        
        [window setTitle:@"OpenGL : Toggle PerFragment/PerVertex based lighting sphere"];
        [window center];

        ///FrameSize will be same as windowSize
        myOpenGLview = [[MyOpenGLView alloc] initWithFrame:winRect];
        ///assigning Frame to our window
        [window setContentView:myOpenGLview];

        ///NSWindow method will be implemented by itself
        ///if not use this line then app will not removed from taskbar
        [window setDelegate:self];

        ///its like setFocus() and setForgroundWindow() or MB_TOPMOST
        [window makeKeyAndOrderFront:self];
    }

    ///when application is gets terminated, then applicationWillTerminate() a method of NSApplication is called
    ///WM_Destroy and applicationWillTerminate() are analogus
    /// [NSApp terminate:self] is like WM_QUIT
    ///  when this line is called [NSApp terminate:self];  ...then applicationWillTerminate() is called ...then it exit Message/run/event loop
    -(void)applicationWillTerminate:(NSNotification *)aNotification
    {
        ///code

        if(gpFile)
        {
            fprintf(gpFile,"\nProgram terminated Successfully");
            fflush(gpFile);
            fclose(gpFile);
        }
        gpFile=NULL;
    }

    ///NSWindowDelegate method
    -(void)windowWillClose:(NSNotification *)aNotification
    {
        ///code
        [NSApp terminate:self];
    }

    ///like destructor
    -(void)dealloc
    {
        ///code
        [myOpenGLview release];
        [window release];
        [super dealloc];
    }

@end

//implmentating method for MyOpenGLView
@implementation MyOpenGLView
    {
        @private ///private variable
        CVDisplayLinkRef displayLinkRef;

        GLuint gVertexShaderObject;
        GLuint gFragmentShaderObject;

        GLuint gPV_ShaderProgramObject;
        GLuint gPF_ShaderProgramObject;

        GLuint vaoSphere;
        GLuint vboSphere_position;
        GLuint vboSphere_normal;
        GLuint vboSphere_elements;

        //-------------------------------------
        GLuint PV_modelMatrixUniform;
        GLuint PV_viewMatrixUniform;
        GLuint PV_projectionlMatrixUniform;

        GLuint PV_lightAmbientUniform;
        GLuint PV_lightDiffuseUniform;
        GLuint PV_lightSpecularUniform;
        GLuint PV_lightPositionUniform;

        GLuint PV_materialAmbientUniform;
        GLuint PV_materialDiffuseUniform;
        GLuint PV_materialSpecularUniform;
        GLuint PV_materialShininessUniform;

        GLuint PV_LKeyPressedUniform;

        //------------------------------
        GLuint PF_modelMatrixUniform;
        GLuint PF_viewMatrixUniform;
        GLuint PF_projectionlMatrixUniform;

        GLuint PF_lightAmbientUniform;
        GLuint PF_lightDiffuseUniform;
        GLuint PF_lightSpecularUniform;
        GLuint PF_lightPositionUniform;

        GLuint PF_materialAmbientUniform;
        GLuint PF_materialDiffuseUniform;
        GLuint PF_materialSpecularUniform;
        GLuint PF_materialShininessUniform;

        GLuint PF_LKeyPressedUniform;

        //-------------------

        mat4 perspectiveProjectionMatrix;

        bool bLights;
        bool bTogglePVxOrPF;//True-Per Fragment and False-Per Vertex

        float spherePosition[1146];
        float sphereNormals[1146];
        float sphereTexcoord[764];
        short sphereElement[2280];//Face Indices

        GLuint gNumSphereVertices, gNumSphereElements;
    }

    ///id is a object of class
    -(id)initWithFrame:(NSRect)frame
    {
        ///code
        self = [super initWithFrame:frame];
        if (self)
        {
            //array of NSOpenGLPixelFormatAttribute
            //its like pixelformatdiscriptor/framebufferattribute
            NSOpenGLPixelFormatAttribute attributes[] = {
                                                            NSOpenGLPFAOpenGLProfile,NSOpenGLProfileVersion4_1Core,
                                                            NSOpenGLPFAScreenMask,CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay), ///Display/monitor  id/type
                                                            NSOpenGLPFANoRecovery,/// canceling automatic shifting process from h/w rendering to s/w rendering ,if h/w rendering context not found
                                                            NSOpenGLPFAAccelerated,/// to use h/w rendering context
                                                            NSOpenGLPFAColorSize,24,//RGB
                                                            NSOpenGLPFADepthSize,24,
                                                            NSOpenGLPFAAlphaSize,8,
                                                            NSOpenGLPFADoubleBuffer,
                                                            0
                                                        };
            ///autorelease = release on your own in auto release pool
            ///pixelFormat its like choosePixelFormat
            NSOpenGLPixelFormat *pixelFormat = [[[NSOpenGLPixelFormat alloc] initWithAttributes:attributes] autorelease];
            if (pixelFormat==nil)
            {
                fprintf(gpFile,"\npixelFormat is nil.exited");
                fflush(gpFile);
                [self release];
                [NSApp terminate:self];
            }

            //shareContext = from linux
            NSOpenGLContext *glContext = [[[NSOpenGLContext alloc] initWithFormat:pixelFormat shareContext:nil] autorelease];

            //set pixelformat
            [self setPixelFormat:pixelFormat];

            //set content
            [self setOpenGLContext:glContext];
        }

        return self;
    }

    -(CVReturn)getFrameForTime:(const CVTimeStamp*)outputTime
    {
        ///code
        ///Apple OpenGL supports multi threading
        ///pool in main() is main thread and pool in getFrameForTime() are other thread
        ///this pool is renderings auto release pool
        NSAutoreleasePool *pool=[[NSAutoreleasePool alloc] init];

        ///to draw our shaders
        [self drawView];

        ///releasing renderings auto release pool
        [pool release];

        return(kCVReturnSuccess);
    }

    -(void)prepareOpenGL
    {
        ///code
        [super prepareOpenGL];

        ///Objective c does not add `get` as prefix for getter methods
        ///Objective c does add `set` as prefix for setter methods
        ///it gives opengl context and set to current context
        [[self openGLContext] makeCurrentContext];

        ///Swap interval code below
        ///CP=context parameter
        GLint swapInt = 1;
        [[self openGLContext] setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];

        fprintf(gpFile,"\nOpenGL Vender:%s",glGetString(GL_VENDOR));
        fprintf(gpFile,"\nOpenGL Render:%s",glGetString(GL_RENDERER));
        fprintf(gpFile,"\nOpenGL Version:%s",glGetString(GL_VERSION));
        fprintf(gpFile,"\nOpenGL GLSL Version:%s",glGetString(GL_SHADING_LANGUAGE_VERSION));

        ///****************shader codes start*****************************
                    
            GLint infoLogLength;
            GLint shaderCompileStatus;
            GLint shaderProgramLinkStatus;
            GLchar * szBuffer=NULL;
            GLsizei written;


            //Per Vertex Shader Code Start--------------------
            gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

            const GLchar* vertexShaderSourceCode_PV =
                "#version 410 core\n"\
                "\n"\
                "in vec4 vPosition;\n"\
                "in vec3 vNormal;\n"\

                "uniform mat4 u_modelMatrix;\n"\
                "uniform mat4 u_viewMatrix;\n"\
                "uniform mat4 u_projectionMatrix;\n"\

                "uniform vec3 u_lightAmbient;\n"\
                "uniform vec3 u_lightDiffuse;\n"\
                "uniform vec3 u_lightSpecular;\n"\
                "uniform vec4 u_lightPosition;\n"\

                "uniform vec3 u_materialAmbient;\n"\
                "uniform vec3 u_materialDiffuse;\n"\
                "uniform vec3 u_materialSpecular;\n"\
                "uniform float u_materialShininess;\n"\

                "uniform int u_lKeyPressed;\n"\

                "out vec3 out_phongADS_Light;\n"\

                "void main(void)\n"\

                "{\n"\
                "if (u_lKeyPressed == 1)\n"\
                "{\n"\

                "vec4 eyeCoord = u_viewMatrix * u_modelMatrix * vPosition;\n"\

                "vec3 transformedNormal = normalize( mat3(u_viewMatrix * u_modelMatrix) * vNormal );\n"\

                "vec3 lightDirection = normalize( vec3( u_lightPosition - eyeCoord ) );\n"\

                "vec3 reflectionVector = reflect( -lightDirection, transformedNormal );\n"\

                "vec3 viewVector = normalize( -eyeCoord.xyz);\n"\

                "vec3 ambient = u_lightAmbient * u_materialAmbient;\n"\

                "vec3 diffuse = u_lightDiffuse * u_materialDiffuse * max( dot(lightDirection , transformedNormal), 0.0);\n"\

                "vec3 specular = u_lightSpecular * u_materialSpecular * pow( max( dot(reflectionVector , viewVector), 0.0f) , u_materialShininess);\n"\

                "out_phongADS_Light= ambient+ diffuse + specular;\n"\

                "}\n"\
                "else\n"\
                "{\n"\

                "out_phongADS_Light = vec3(1.0f, 1.0f, 1.0f);\n"\

                "}\n"\

                "gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix * vPosition;\n"\

                "}";

            glShaderSource(gVertexShaderObject,
                1,
                (const GLchar**)&vertexShaderSourceCode_PV,
                NULL);

            glCompileShader(gVertexShaderObject);

            glGetShaderiv(gVertexShaderObject,
                GL_COMPILE_STATUS,
                &shaderCompileStatus);

            if (shaderCompileStatus == GL_FALSE)
            {
                glGetShaderiv(gVertexShaderObject,
                    GL_INFO_LOG_LENGTH,
                    &infoLogLength);

                if (infoLogLength > 0)
                {
                    szBuffer = (GLchar*)malloc(sizeof(GLchar) * infoLogLength);
                    if (szBuffer != NULL)
                    {
                        glGetShaderInfoLog(gVertexShaderObject,
                            infoLogLength,
                            &written,
                            szBuffer);
                        fprintf(gpFile, "\nERROR:Vertex Shader Object Compilation Failed log : %s", szBuffer);
                        free(szBuffer);
                        [self release];
                [NSApp terminate:self];
                    }
                }
            }

            infoLogLength = 0;
            shaderCompileStatus = 0;
            shaderProgramLinkStatus = 0;
            szBuffer = NULL;
            written = 0;

            gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

            const GLchar* fragmentShaderSourceCode_PV =
                "#version 410 core"\
                "\n"\
                "in vec3 out_phongADS_Light;\n"
                "out vec4 fragColor;"\
                "void main(void)"\
                "{"\
                "fragColor = vec4( out_phongADS_Light, 1.0f );"\
                "}";

            glShaderSource(gFragmentShaderObject,
                1,
                (const GLchar**)&fragmentShaderSourceCode_PV,
                NULL);

            glCompileShader(gFragmentShaderObject);

            glGetShaderiv(gFragmentShaderObject,
                GL_COMPILE_STATUS,
                &shaderCompileStatus);

            if (shaderCompileStatus == GL_FALSE)
            {
                glGetShaderiv(gFragmentShaderObject,
                    GL_INFO_LOG_LENGTH,
                    &infoLogLength);

                if (infoLogLength > 0)
                {
                    szBuffer = (GLchar*)malloc(sizeof(GLchar) * infoLogLength);
                    if (szBuffer != NULL)
                    {
                        glGetShaderInfoLog(gFragmentShaderObject,
                            infoLogLength,
                            &written,
                            szBuffer);
                        fprintf(gpFile, "\nERROR:Fragment Shader Object Compilation Failed log : %s", szBuffer);
                        free(szBuffer);
                        [self release];
                [NSApp terminate:self];
                    }
                }
            }

            infoLogLength = 0;
            shaderCompileStatus = 0;
            shaderProgramLinkStatus = 0;
            szBuffer = NULL;
            written = 0;

            gPV_ShaderProgramObject = glCreateProgram();

            glAttachShader(gPV_ShaderProgramObject, gVertexShaderObject);
            glAttachShader(gPV_ShaderProgramObject, gFragmentShaderObject);

            //Shader Attribute Binding Before Pre Linking of attached Shader
            glBindAttribLocation(gPV_ShaderProgramObject,
                VSV_ATTRIBUTE_POSITION,
                "vPosition");
            glBindAttribLocation(gPV_ShaderProgramObject,
                VSV_ATTRIBUTE_NORMAL,
                "vNormal");

            glLinkProgram(gPV_ShaderProgramObject);

            glGetProgramiv(gPV_ShaderProgramObject,
                GL_LINK_STATUS,
                &shaderProgramLinkStatus);

            if (shaderProgramLinkStatus == GL_FALSE)
            {
                glGetProgramiv(gPV_ShaderProgramObject,
                    GL_INFO_LOG_LENGTH,
                    &infoLogLength);
                if (infoLogLength > 0)
                {
                    szBuffer = (GLchar*)malloc(sizeof(GLchar) * infoLogLength);

                    if (szBuffer != NULL)
                    {
                        glGetProgramInfoLog(gPV_ShaderProgramObject,
                            infoLogLength,
                            &written,
                            szBuffer);

                        fprintf(gpFile, "\nERROR:Shader program Link failed log : %s", szBuffer);
                        free(szBuffer);
                        [self release];
                [NSApp terminate:self];
                    }
                }
            }

            //Shader Code End--------------------

            //Uniform variable Binding after post linking of attached shader
            PV_modelMatrixUniform = glGetUniformLocation(gPV_ShaderProgramObject, "u_modelMatrix");
            PV_viewMatrixUniform = glGetUniformLocation(gPV_ShaderProgramObject, "u_viewMatrix");
            PV_projectionlMatrixUniform = glGetUniformLocation(gPV_ShaderProgramObject, "u_projectionMatrix");

            PV_lightAmbientUniform = glGetUniformLocation(gPV_ShaderProgramObject, "u_lightAmbient");
            PV_lightDiffuseUniform = glGetUniformLocation(gPV_ShaderProgramObject, "u_lightDiffuse");
            PV_lightSpecularUniform = glGetUniformLocation(gPV_ShaderProgramObject, "u_lightSpecular");
            PV_lightPositionUniform = glGetUniformLocation(gPV_ShaderProgramObject, "u_lightPosition");

            PV_materialAmbientUniform = glGetUniformLocation(gPV_ShaderProgramObject, "u_materialAmbient");
            PV_materialDiffuseUniform = glGetUniformLocation(gPV_ShaderProgramObject, "u_materialDiffuse");
            PV_materialSpecularUniform = glGetUniformLocation(gPV_ShaderProgramObject, "u_materialSpecular");
            PV_materialShininessUniform = glGetUniformLocation(gPV_ShaderProgramObject, "u_materialShininess");

            PV_LKeyPressedUniform = glGetUniformLocation(gPV_ShaderProgramObject, "u_lKeyPressed");


            //###########################################################################################
            //###########################################################################################

            //Per Fragment Shader Code Start--------------------
                gVertexShaderObject=glCreateShader(GL_VERTEX_SHADER);

                const GLchar *vertexShaderSourceCode_PF=
                "#version 410 core\n"\
                "\n"\
                "in vec4 vPosition;\n"\
                "in vec3 vNormal;\n"\

                "uniform mat4 u_modelMatrix;\n"\
                "uniform mat4 u_viewMatrix;\n"\
                "uniform mat4 u_projectionMatrix;\n"\

                "uniform vec4 u_lightPosition;\n"\
                
                "uniform int u_lKeyPressed;\n"\
                
                "out vec3 out_phongADS_Light;\n"\
                
                "out vec3 out_transformedNormal;\n"\
                "out vec3 out_lightDirection;\n"\
                "out vec3 out_viewVector;\n"\

                "void main(void)\n"\

                "{\n"\
                    "if (u_lKeyPressed == 1)\n"\
                    "{\n"\

                        "vec4 eyeCoord = u_viewMatrix * u_modelMatrix * vPosition;\n"\
                        
                        "vec3 transformedNormal = mat3(u_viewMatrix * u_modelMatrix) * vNormal ;\n"\
                        "out_transformedNormal = transformedNormal;\n"\

                        "vec3 lightDirection = vec3( u_lightPosition - eyeCoord ) ;\n"\
                        "out_lightDirection = lightDirection;\n"\
                        
                        "vec3 viewVector =  -eyeCoord.xyz ;\n"\
                        "out_viewVector = viewVector;\n"\

                    "}\n"\
                    
                    "gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix * vPosition;\n"\
                
                "}";

                glShaderSource(gVertexShaderObject,
                                1,
                                (const GLchar**)&vertexShaderSourceCode_PF,
                                NULL);
                
                glCompileShader(gVertexShaderObject);

                glGetShaderiv(gVertexShaderObject,
                                GL_COMPILE_STATUS,
                                &shaderCompileStatus);

                if(shaderCompileStatus==GL_FALSE)
                {
                    glGetShaderiv(gVertexShaderObject,
                                GL_INFO_LOG_LENGTH,
                                &infoLogLength);
                    
                    if(infoLogLength > 0)
                    {
                        szBuffer=(GLchar*)malloc(sizeof(GLchar)*infoLogLength);
                        if(szBuffer!=NULL)
                        {
                            glGetShaderInfoLog(gVertexShaderObject,
                                                infoLogLength,
                                                &written,
                                                szBuffer);
                            fprintf(gpFile,"\nERROR:Vertex Shader Object Compilation Failed log : %s",szBuffer);
                            free(szBuffer);
                            [self release];
                [NSApp terminate:self];
                        }
                    }
                }

                infoLogLength=0;
                shaderCompileStatus=0;
                shaderProgramLinkStatus=0;
                szBuffer=NULL;
                written=0;

                gFragmentShaderObject=glCreateShader(GL_FRAGMENT_SHADER);

                const GLchar *fragmentShaderSourceCode_PF=
                "#version 410 core\n"\
                "\n"\
                "vec3 phongADS_Light;\n"
                "out vec4 fragColor;\n"\

                "in vec3 out_transformedNormal;\n"\
                "in vec3 out_lightDirection;\n"\
                "in vec3 out_viewVector;\n"\

                "uniform vec3 u_lightAmbient;\n"\
                "uniform vec3 u_lightDiffuse;\n"\
                "uniform vec3 u_lightSpecular;\n"\

                "uniform vec3 u_materialAmbient;\n"\
                "uniform vec3 u_materialDiffuse;\n"\
                "uniform vec3 u_materialSpecular;\n"\
                "uniform float u_materialShininess;\n"\

                "uniform int u_lKeyPressed;\n"\

                "void main(void)\n"\
                "{"\
                    "if (u_lKeyPressed == 1)\n"\
                    "{\n"\
                        "vec3 normalizedTransformedNormal = normalize( out_transformedNormal );\n"\
                        "vec3 normalizedLightDirection = normalize( out_lightDirection );\n"\
                        "vec3 normalizedViewVector = normalize( out_viewVector );\n"\

                        "vec3 reflectionVector = reflect( -normalizedLightDirection, normalizedTransformedNormal );\n"\
                        
                        "vec3 ambient = u_lightAmbient * u_materialAmbient;\n"\

                        "vec3 diffuse = u_lightDiffuse * u_materialDiffuse * max( dot(normalizedLightDirection , normalizedTransformedNormal), 0.0);\n"\

                        "vec3 specular = u_lightSpecular * u_materialSpecular * pow( max( dot(reflectionVector , normalizedViewVector), 0.0f) , u_materialShininess);\n"\

                        "phongADS_Light= ambient+ diffuse + specular;\n"\
                        
                    "}\n"\
                    "else\n"\
                    "{\n"\

                        "phongADS_Light=vec3(1.0f, 1.0f, 1.0f);\n"\
                    
                    "}\n"\
                    
                    "fragColor = vec4( phongADS_Light, 1.0f );\n"\

                "}";

                glShaderSource(gFragmentShaderObject,
                                1,
                                (const GLchar**)&fragmentShaderSourceCode_PF,
                                NULL);
                
                glCompileShader(gFragmentShaderObject);

                glGetShaderiv(gFragmentShaderObject,
                            GL_COMPILE_STATUS,
                            &shaderCompileStatus);

                if(shaderCompileStatus==GL_FALSE)
                {
                    glGetShaderiv(gFragmentShaderObject,
                                GL_INFO_LOG_LENGTH,
                                &infoLogLength);
                    
                    if(infoLogLength > 0)
                    {
                        szBuffer=(GLchar*)malloc(sizeof(GLchar)*infoLogLength);
                        if(szBuffer!=NULL)
                        {
                            glGetShaderInfoLog(gFragmentShaderObject,
                                                infoLogLength,
                                                &written,
                                                szBuffer);
                            fprintf(gpFile,"\nERROR:Fragment Shader Object Compilation Failed log : %s",szBuffer);
                            free(szBuffer);
                            [self release];
                [NSApp terminate:self];
                        }
                    }
                }

                infoLogLength=0;
                shaderCompileStatus=0;
                shaderProgramLinkStatus=0;
                szBuffer=NULL;
                written=0;

                gPF_ShaderProgramObject =glCreateProgram();

                glAttachShader(gPF_ShaderProgramObject,gVertexShaderObject);
                glAttachShader(gPF_ShaderProgramObject,gFragmentShaderObject);

                //Shader Attribute Binding Before Pre Linking of attached Shader
                    glBindAttribLocation(gPF_ShaderProgramObject,
                                        VSV_ATTRIBUTE_POSITION,
                                        "vPosition");
                    glBindAttribLocation(gPF_ShaderProgramObject,
                                        VSV_ATTRIBUTE_NORMAL,
                                        "vNormal");

                glLinkProgram(gPF_ShaderProgramObject);

                glGetProgramiv(gPF_ShaderProgramObject,
                                GL_LINK_STATUS,
                                &shaderProgramLinkStatus);
                
                if(shaderProgramLinkStatus==GL_FALSE)
                {
                    glGetProgramiv(gPF_ShaderProgramObject,
                                    GL_INFO_LOG_LENGTH,
                                    &infoLogLength);
                    if(infoLogLength > 0)
                    {
                        szBuffer=(GLchar *)malloc(sizeof(GLchar)*infoLogLength);

                        if(szBuffer!=NULL)
                        {
                            glGetProgramInfoLog(gPF_ShaderProgramObject,
                                                infoLogLength,
                                                &written,
                                                szBuffer);

                            fprintf(gpFile,"\nERROR:Shader program Link failed log : %s",szBuffer);
                            free(szBuffer);
                            [self release];
                [NSApp terminate:self];
                        }
                    }
                }

            //Shader Code End--------------------

            //Uniform variable Binding after post linking of attached shader
                PF_modelMatrixUniform = glGetUniformLocation(gPF_ShaderProgramObject,"u_modelMatrix");
                PF_viewMatrixUniform = glGetUniformLocation(gPF_ShaderProgramObject,"u_viewMatrix");
                PF_projectionlMatrixUniform = glGetUniformLocation(gPF_ShaderProgramObject,"u_projectionMatrix");

                PF_lightAmbientUniform = glGetUniformLocation(gPF_ShaderProgramObject,"u_lightAmbient");
                PF_lightDiffuseUniform = glGetUniformLocation(gPF_ShaderProgramObject,"u_lightDiffuse");
                PF_lightSpecularUniform = glGetUniformLocation(gPF_ShaderProgramObject,"u_lightSpecular");
                PF_lightPositionUniform = glGetUniformLocation(gPF_ShaderProgramObject,"u_lightPosition");

                PF_materialAmbientUniform = glGetUniformLocation(gPF_ShaderProgramObject,"u_materialAmbient");
                PF_materialDiffuseUniform = glGetUniformLocation(gPF_ShaderProgramObject,"u_materialDiffuse");
                PF_materialSpecularUniform = glGetUniformLocation(gPF_ShaderProgramObject,"u_materialSpecular");
                PF_materialShininessUniform = glGetUniformLocation(gPF_ShaderProgramObject,"u_materialShininess");

                PF_LKeyPressedUniform = glGetUniformLocation(gPF_ShaderProgramObject,"u_lKeyPressed");

            //VAO and its respective VBO preparation

                Sphere *sphere=new Sphere();
                sphere->sphereInitialize();
                
                sphere->getSphereVertexData(spherePosition, sphereNormals, sphereTexcoord, sphereElement);
                gNumSphereVertices = sphere->getNumberOfSphereVertices();
                gNumSphereElements = sphere->getNumberOfSphereElements();

                glGenVertexArrays(1,
                                &vaoSphere);

                glBindVertexArray(vaoSphere);

                    glGenBuffers(1,
                                &vboSphere_position);

                    glBindBuffer(GL_ARRAY_BUFFER,
                                vboSphere_position);

                        glBufferData(GL_ARRAY_BUFFER,
                                    sizeof(spherePosition),
                                    spherePosition,
                                    GL_STATIC_DRAW);
                        
                        glVertexAttribPointer(VSV_ATTRIBUTE_POSITION,
                                            3,
                                            GL_FLOAT,
                                            GL_FALSE,
                                            0,
                                            NULL);

                        glEnableVertexAttribArray(VSV_ATTRIBUTE_POSITION);
                    
                    glBindBuffer(GL_ARRAY_BUFFER,
                                0);
                    
                    glGenBuffers(1,
                                &vboSphere_normal);

                    glBindBuffer(GL_ARRAY_BUFFER,
                                vboSphere_normal);

                        glBufferData(GL_ARRAY_BUFFER,
                                        sizeof(sphereNormals),
                                        sphereNormals,
                                        GL_STATIC_DRAW);

                        glVertexAttribPointer(VSV_ATTRIBUTE_NORMAL,
                            3,
                            GL_FLOAT,
                            GL_FALSE,
                            0,
                            NULL);

                        glEnableVertexAttribArray(VSV_ATTRIBUTE_NORMAL);

                    glBindBuffer(GL_ARRAY_BUFFER,
                        0);

                    glGenBuffers(1,
                        &vboSphere_normal);

                    glBindBuffer(GL_ARRAY_BUFFER,
                        vboSphere_normal);

                        glBufferData(GL_ARRAY_BUFFER,
                            sizeof(sphereNormals),
                            sphereNormals,
                            GL_STATIC_DRAW);

                        glVertexAttribPointer(VSV_ATTRIBUTE_NORMAL,
                            3,
                            GL_FLOAT,
                            GL_FALSE,
                            0,
                            NULL);

                        glEnableVertexAttribArray(VSV_ATTRIBUTE_NORMAL);

                    glBindBuffer(GL_ARRAY_BUFFER,
                        0);


                    glGenBuffers(1,
                        &vboSphere_elements);

                    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,
                        vboSphere_elements);

                        glBufferData(GL_ELEMENT_ARRAY_BUFFER,
                            sizeof(sphereElement),
                            sphereElement,
                            GL_STATIC_DRAW);

                    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,
                        0);

                glBindVertexArray(0);


            //SetColor
            glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
            
            glClearDepth(1.0f);
            glEnable(GL_DEPTH_TEST);
            glDepthFunc(GL_LEQUAL);

            //Warmup resize call
            perspectiveProjectionMatrix=mat4::identity();
    

        ///****************shader codes end*****************************

        ///-----Setup code for DisplayLink and its Callback (its CV and CG related code )----------------------------
        /* CGL-core graphics library
            1.Create DisplayLink
            2.set Callback function for DisplayLink
            3.Convert OpenGLContext to CGL context
            4.Convert OpenGLPixelFormat to CGL PixelFormat
            5.set current displays CGL context and CGL PixelFormat
            6.start DisplayLink
        */

        //1.Create DisplayLink
        CVDisplayLinkCreateWithActiveCGDisplays(&displayLinkRef);
        
        //2.set Callback function for DisplayLink
        //displayLinkContext is self
        CVDisplayLinkSetOutputCallback(displayLinkRef,&MyDisplayLinkCallback,self);
        
        //3.Convert OpenGLContext to CGL context
        CGLContextObj cglContext = (CGLContextObj)[[self openGLContext] CGLContextObj];

        //4.Convert OpenGLPixelFormat to CGL PixelFormat
        CGLPixelFormatObj cglPixelFormat = (CGLPixelFormatObj)[[self pixelFormat] CGLPixelFormatObj];

        // 5.set current displays CGL context and CGL PixelFormat
        CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLinkRef,cglContext,cglPixelFormat);

        //6.start DisplayLink
        CVDisplayLinkStart(displayLinkRef);

    }

    -(void)reshape
    {
        ///code
        [super reshape];

        CGLLockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);

        NSRect rect = [self bounds];

        if (rect.size.height < 0)
        {
            rect.size.height = 1;
        }

        glViewport(0,0,(GLsizei)rect.size.width,(GLsizei)rect.size.height);

        int width = rect.size.width;
        int height = rect.size.height;

        perspectiveProjectionMatrix=vmath::perspective(45.0f,
                                                    (GLfloat)width/(GLfloat)height,
                                                    0.1f,
                                                    100.0f);

        CGLUnlockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);
    }

    //InvalidateRect is similar to dirtyRect
    -(void)drawRect:(NSRect)dirtyRect
    {
        ///code
        ///due to multi threading support for OpenGL
        ///we have to call drawView() at two place to syncronised between main thread and other thread
        [self drawView];
    }

    -(void)drawView
    {
        ///code
        [[self openGLContext] makeCurrentContext];
        CGLLockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);

        //*************shader drawing code start********************
            GLfloat LightAmbient[] = { 0.1f,0.1f,0.1f,1.0f };
            GLfloat LightDefuse[] = { 1.0f,1.0f,1.0f,1.0f };
            GLfloat LightSpecular[] = { 1.0f,1.0f,1.0f,1.0f };
            GLfloat LightPosition[] = { 100.0f,100.0f,100.0f,1.0f };

            GLfloat materialAmbient[] = { 0.0f,0.0f,0.0f,1.0f };
            GLfloat materilaDefuse[] = { 0.5f,0.2f,0.7f,1.0f };
            GLfloat materialSpecular[] = { 1.0f,1.0f,1.0f,1.0f };
            GLfloat materialShininess = 128.0f;

            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

            mat4 translateMatrix;

            mat4 modelMatrix;
            mat4 viewMatrix = mat4::identity();

            translateMatrix = vmath::translate(0.0f, 0.0f, -1.5f);
            modelMatrix = translateMatrix;

            if (bTogglePVxOrPF)
            {
                glUseProgram(gPF_ShaderProgramObject);


                glUniformMatrix4fv(PF_modelMatrixUniform,
                    1,
                    GL_FALSE,
                    modelMatrix);

                glUniformMatrix4fv(PF_viewMatrixUniform,
                    1,
                    GL_FALSE,
                    viewMatrix);

                glUniformMatrix4fv(PF_projectionlMatrixUniform,
                    1,
                    GL_FALSE,
                    perspectiveProjectionMatrix);

                if (bLights == true)
                {
                    glUniform1i(PF_LKeyPressedUniform, 1);

                    glUniform3fv(PF_lightAmbientUniform, 1, LightAmbient);
                    glUniform3fv(PF_lightDiffuseUniform, 1, LightDefuse);
                    glUniform3fv(PF_lightSpecularUniform, 1, LightSpecular);
                    glUniform4fv(PF_lightPositionUniform, 1, LightPosition);//posiitional light

                    glUniform3fv(PF_materialAmbientUniform, 1, materialAmbient);
                    glUniform3fv(PF_materialDiffuseUniform, 1, materilaDefuse);
                    glUniform3fv(PF_materialSpecularUniform, 1, materialSpecular);

                    glUniform1f(PF_materialShininessUniform, materialShininess);
                }
                else
                {
                    glUniform1i(PF_LKeyPressedUniform, 0);
                }

            }
            else
            {
                glUseProgram(gPV_ShaderProgramObject);


                glUniformMatrix4fv(PV_modelMatrixUniform,
                    1,
                    GL_FALSE,
                    modelMatrix);

                glUniformMatrix4fv(PV_viewMatrixUniform,
                    1,
                    GL_FALSE,
                    viewMatrix);

                glUniformMatrix4fv(PV_projectionlMatrixUniform,
                    1,
                    GL_FALSE,
                    perspectiveProjectionMatrix);

                if (bLights == true)
                {
                    glUniform1i(PV_LKeyPressedUniform, 1);

                    glUniform3fv(PV_lightAmbientUniform, 1, LightAmbient);
                    glUniform3fv(PV_lightDiffuseUniform, 1, LightDefuse);
                    glUniform3fv(PV_lightSpecularUniform, 1, LightSpecular);
                    glUniform4fv(PV_lightPositionUniform, 1, LightPosition);//posiitional light

                    glUniform3fv(PV_materialAmbientUniform, 1, materialAmbient);
                    glUniform3fv(PV_materialDiffuseUniform, 1, materilaDefuse);
                    glUniform3fv(PV_materialSpecularUniform, 1, materialSpecular);

                    glUniform1f(PV_materialShininessUniform, materialShininess);
                }
                else
                {
                    glUniform1i(PV_LKeyPressedUniform, 0);
                }

            }

            glBindVertexArray(vaoSphere);
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboSphere_elements);
            glDrawElements(GL_TRIANGLES, gNumSphereElements, GL_UNSIGNED_SHORT, 0);
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
            glBindVertexArray(0);

            glUseProgram(0);

        //*************shader drawing code end********************

        ///swapbuffer
        CGLFlushDrawable((CGLContextObj)[[self openGLContext] CGLContextObj]);

        CGLUnlockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);
    }

    ///to shift keyboard and mouse controll from NSwindow to NSOpenGLView
    ///NSReponder->NSobject method
    -(BOOL)acceptsFirstResponder
    {
        ///code
        [[self window] makeFirstResponder:self];

        return YES;
    }

    -(void)keyDown:(NSEvent*)theEvent
    {
        ///code
        int key = [[theEvent characters] characterAtIndex:0];
        switch(key)
        {
            case 27://escape
                [[self window] toggleFullScreen:self];
                break;
            case 'f':
            case 'F':
                bTogglePVxOrPF = true;
                break;
            case 'v':
            case 'V':
                bTogglePVxOrPF = false;
                break;
            case 'l':
            case 'L':
                bLights = !bLights;
                if (bLights)
                {
                    bTogglePVxOrPF = false;
                }
                break;
            case 'q':
            case 'Q':
                [self release];
                [NSApp terminate:self];
                break;
        }
    }

    -(void)mouseDown:(NSEvent*)theEvent
    {
        ///code


    }

    -(void)rightMouseDown:(NSEvent*)theEvent
    {
        ///code

        
    }

    -(void)otherMouseDown:(NSEvent*)theEvent
    {
        ///code
    
    }

    ///like destructor
    -(void)dealloc
    {
        ///*********Shader dealloc start******************************************************
        
        if(vaoSphere)
        {
            glDeleteVertexArrays(1,
                                &vaoSphere);
        }
        vaoSphere =0;

        if(vboSphere_position){
            glDeleteBuffers(1,
                            &vboSphere_position);
        }
        vboSphere_position =0;

        if(vboSphere_normal)
        {
            glDeleteBuffers(1,
                        &vboSphere_normal);
        }
        vboSphere_normal =0;

        if (vboSphere_elements)
        {
            glDeleteBuffers(1,
                &vboSphere_elements);
        }
        vboSphere_elements = 0;

        if(gPF_ShaderProgramObject)
        {
            glUseProgram(gPF_ShaderProgramObject);

                GLsizei shaderCount;
                GLsizei tempShaderCount;
                GLuint *pShader=NULL;

                glGetProgramiv(gPF_ShaderProgramObject,
                                GL_ATTACHED_SHADERS,
                                &shaderCount);

                pShader=(GLuint *)malloc(sizeof(GLuint)*shaderCount);
                if(pShader==NULL)
                {
                    fprintf(gpFile,"\nFailed to allocate memory for pShader");
                }else{
                    glGetAttachedShaders(gPF_ShaderProgramObject,
                                        shaderCount,
                                        &tempShaderCount,
                                        pShader);
                    
                    fprintf(gpFile,"\nShader program has actual attached shader count is:%d",tempShaderCount);
                    
                    for(GLsizei i=0;i<shaderCount;i++)
                    {
                        glDetachShader(gPF_ShaderProgramObject,
                                        pShader[i]);

                        glDeleteShader(pShader[i]);
                        
                        pShader[i]=0;
                    }
                    free(pShader);
                    
                    glDeleteProgram(gPF_ShaderProgramObject);
                    
                    gPF_ShaderProgramObject = 0;
                }
            glUseProgram(0);
        }

        if (gPV_ShaderProgramObject)
        {
            glUseProgram(gPV_ShaderProgramObject);

            GLsizei shaderCount;
            GLsizei tempShaderCount;
            GLuint* pShader = NULL;

            glGetProgramiv(gPV_ShaderProgramObject,
                GL_ATTACHED_SHADERS,
                &shaderCount);

            pShader = (GLuint*)malloc(sizeof(GLuint) * shaderCount);
            if (pShader == NULL)
            {
                fprintf(gpFile, "\nFailed to allocate memory for pShader");
            }
            else {
                glGetAttachedShaders(gPV_ShaderProgramObject,
                    shaderCount,
                    &tempShaderCount,
                    pShader);

                fprintf(gpFile, "\nShader program has actual attached shader count is:%d", tempShaderCount);

                for (GLsizei i = 0; i < shaderCount; i++)
                {
                    glDetachShader(gPV_ShaderProgramObject,
                        pShader[i]);

                    glDeleteShader(pShader[i]);

                    pShader[i] = 0;
                }
                free(pShader);

                glDeleteProgram(gPV_ShaderProgramObject);

                gPV_ShaderProgramObject = 0;
            }
            glUseProgram(0);
        }
        ///*********Shader dealloc end******************************************************

        CVDisplayLinkStop(displayLinkRef);
        CVDisplayLinkRelease(displayLinkRef);
        [super dealloc];
    }


@end


///As Callback function is C style func its should return in global space
///displayLinkContext will be MyOpenGLView obj which will be set by OS,when OS will call this function

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLinkRef,
                                const CVTimeStamp* now,
                                const CVTimeStamp* outputTime,
                                CVOptionFlags flagsIn,
                                CVOptionFlags* flagsOut,
                                void * displayLinkContext)
{
    CVReturn result = [(MyOpenGLView *)displayLinkContext  getFrameForTime:outputTime];
    return result;
}


