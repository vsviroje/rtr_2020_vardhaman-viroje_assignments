#import<Foundation/Foundation.h> ///get foundation header file from foundation framework (similar to standard library like stdio.h)
#import<Cocoa/Cocoa.h>///analogus to window.h/xlib.h

///mkdir -p Window.app/Contents/MacOS
///clang -o Window.app/Contents/MacOS/window Window.m -framework Cocoa

///NeXT STEP (NS)

/// our class AppDelegate is inheritated from NSObject class and it will implement NSApplicationDelegate and NSWindowDelegate interface
/// : is extends and <> are implements
///we will define method later...its forward declaration
@interface AppDelegate:NSObject<NSApplicationDelegate, NSWindowDelegate>
@end

int main(int argc,char* argv[])
{
    ///code
    ///NSAutoreleasePool will hold all alocated memory
    NSAutoreleasePool *pool=[[NSAutoreleasePool alloc] init];
    ///NSApp is inbuild global NS application object
    ///we get to access shared NS Application object
    NSApp =[NSApplication sharedApplication];

    ///setting NSApp method implementation to our AppDelegate object
    [NSApp setDelegate:[[AppDelegate alloc] init] ];

    ///Message/run/event loop
    [NSApp run];

    ///releaseing every obj memory from pool
    [pool release];

    return 0;
}

///
@interface MyView:NSView
@end

//implmentating method for AppDelegate
@implementation AppDelegate
{
    @private ///private variable
    NSWindow *window;
    MyView *view;
}

///when application is getting started, then applicationDidFinishLaunching() a method of NSApplication is called
///WM_Create and applicationDidFinishLaunching() are analogus
-(void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    ///code
    ///NSRect is internally CGRect -core graphic ..its c based library
    ///CGPoint/NSPoint-x,y and CGSize/NSSize-width,height
    NSRect winRect = NSMakeRect(0.0,0.0,800.0,600.0);

    ///styleMask is similer to WM_overlappedWindow
    ///backing- how to store window in graphics
    ///defer - to halt showing of window or show window immediately
    window = [[NSWindow alloc] initWithContentRect:winRect
                                                styleMask:NSWindowStyleMaskTitled |
                                                        NSWindowStyleMaskClosable |
                                                        NSWindowStyleMaskMiniaturizable |
                                                        NSWindowStyleMaskResizable
                                                        backing:NSBackingStoreBuffered
                                                        defer:NO];
    
    [window setTitle:@"VSV : MacOS Window"];
    [window center];

    ///FrameSize will be same as windowSize
    view = [[MyView alloc] initWithFrame:winRect];
    ///assigning Frame to our window
    [window setContentView:view];

    ///NSWindow method will be implemented by itself
    ///if not use this line then app will not removed from taskbar
    [window setDelegate:self];

    ///its like setFocus() and setForgroundWindow() or MB_TOPMOST
    [window makeKeyAndOrderFront:self];
}

///when application is gets terminated, then applicationWillTerminate() a method of NSApplication is called
///WM_Destroy and applicationWillTerminate() are analogus
-(void)applicationWillTerminate:(NSNotification *)aNotification
{
    ///code
}

///NSWindowDelegate method
-(void)windowWillClose:(NSNotification *)aNotification
{
    ///code
    [NSApp terminate:self];
}

///like destructor
-(void)dealloc
{
    ///code
    [view release];
    [window release];
    [super dealloc];
}

@end

//implmentating method for MyView
@implementation MyView
{
    @private ///private variable
    NSString *centeralText;
}

///id is a object of class
-(id)initWithFrame:(NSRect)frame
{
    ///code
    self = [super initWithFrame:frame];
    if (self)
    {
        centeralText = @"Hello World";
    }

    return self;
}

//InvalidateRect is similar to dirtyRect
-(void)drawRect:(NSRect)dirtyRect
{
    ///code
    NSColor *backgroundColor = [NSColor blackColor];
    [backgroundColor set];

    NSRectFill(dirtyRect);

    NSDictionary *dictionaryForTextAttribute = [NSDictionary dictionaryWithObjectsAndKeys:[NSFont fontWithName:@"Helvetica" size:32],NSFontAttributeName,
                                                                                          [NSColor greenColor],NSForegroundColorAttributeName,
                                                                                          nil];

    NSSize textSize = [centeralText sizeWithAttributes:dictionaryForTextAttribute];

    NSPoint point;
    point.x = (dirtyRect.size.width/2)-(textSize.width/2);
    point.y = (dirtyRect.size.height/2)-(textSize.height/2) + 12;

    [centeralText drawAtPoint:point withAttributes:dictionaryForTextAttribute];
}

///to shift keyboard and mouse controll from NSwindow to NSview
///NSReponder->NSobject method
-(BOOL)acceptsFirstResponder
{
    ///code
    [[self window] makeFirstResponder:self];

    return YES;
}

-(void)keyDown:(NSEvent*)theEvent
{
    ///code
    int key = [[theEvent characters] characterAtIndex:0];
    switch(key)
    {
        case 27://escape
            [self release];
            [NSApp terminate:self];
            break;
        case 'f':
        case 'F':
            [[self window] toggleFullScreen:self];
            break;
    }
}

-(void)mouseDown:(NSEvent*)theEvent
{
    ///code
    centeralText = @"left mouse button is click";
    //invalidateRect ---refresh
    [self setNeedsDisplay:YES];

}

-(void)rightMouseDown:(NSEvent*)theEvent
{
    ///code
    centeralText = @"right mouse button is click";
    //invalidateRect ---refresh
    [self setNeedsDisplay:YES];
    
}

-(void)otherMouseDown:(NSEvent*)theEvent
{
    ///code
    centeralText = @"Hello World";
    //invalidateRect ---refresh
    [self setNeedsDisplay:YES];
}

///like destructor
-(void)dealloc
{
    ///code
    [super dealloc];
}


@end
