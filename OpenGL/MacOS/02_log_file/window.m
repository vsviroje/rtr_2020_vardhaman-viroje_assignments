#import<Foundation/Foundation.h> ///get foundation header file from foundation framework (similar to standard library like stdio.h)
#import<Cocoa/Cocoa.h>///analogus to window.h/xlib.h

FILE *gpFile = NULL;

///mkdir -p Window.app/Contents/MacOS
///clang -o Window.app/Contents/MacOS/window Window.m -framework Cocoa

///NeXT STEP (NS)

/// our class AppDelegate is inheritated from NSObject class and it will implement NSApplicationDelegate and NSWindowDelegate interface
/// : is extends and <> are implements
///we will define method later...its forward declaration
@interface AppDelegate:NSObject<NSApplicationDelegate, NSWindowDelegate>
@end

int main(int argc,char* argv[])
{
    ///code
    ///NSAutoreleasePool will hold all alocated memory
    NSAutoreleasePool *pool=[[NSAutoreleasePool alloc] init];
    ///NSApp is inbuild global NS application object
    ///we get to access shared NS Application object
    NSApp =[NSApplication sharedApplication];

    ///setting NSApp method implementation to our AppDelegate object
    [NSApp setDelegate:[[AppDelegate alloc] init] ];

    ///Message/run/event loop
    [NSApp run];

    ///releaseing every obj memory from pool
    [pool release];

    return 0;
}

///
@interface MyView:NSView
@end

//implmentating method for AppDelegate
@implementation AppDelegate
{
    @private ///private variable
    NSWindow *window;
    MyView *view;
}

///when application is getting started, then applicationDidFinishLaunching() a method of NSApplication is called
///WM_Create and applicationDidFinishLaunching() are analogus
-(void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    ///code
    //window.app is same as NSBundle(its for recongnising window.app in foundation.h)
    NSBundle *appBundle = [NSBundle mainBundle];
    ///storing window.app dir path
    NSString *appDirPath = [appBundle bundlePath];

    /*
    eg . if appDirPath is /usr/viroje/Desktop/assignment/02LogFile/window.app
        then stringByDeletingLastPathComponent will perform
       then parentDirPath will be /usr/viroje/Desktop/assignment/02LogFile
    */
    NSString *parentDirPath = [appDirPath stringByDeletingLastPathComponent];

    NSString *logFileNameWithPath = [NSString stringWithFormat:@"%@/Log.txt",parentDirPath];

    const char *pSzLogFileNameWithPath =  [logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];

    gpFile = fopen(pSzLogFileNameWithPath,"w");
    if(gpFile==NULL)
    {
        [self release];
        [NSApp terminate:self];
    }

    fprintf(gpFile,"\nProgram Started Successfully");
    fflush(gpFile);

    ///NSRect is internally CGRect -core graphic ..its c based library
    ///CGPoint/NSPoint-x,y and CGSize/NSSize-width,height
    NSRect winRect = NSMakeRect(0.0,0.0,800.0,600.0);

    ///styleMask is similer to WM_overlappedWindow
    ///backing- how to store window in graphics
    ///defer - to halt showing of window or show window immediately
    window = [[NSWindow alloc] initWithContentRect:winRect
                                                styleMask:NSWindowStyleMaskTitled |
                                                        NSWindowStyleMaskClosable |
                                                        NSWindowStyleMaskMiniaturizable |
                                                        NSWindowStyleMaskResizable
                                                        backing:NSBackingStoreBuffered
                                                        defer:NO];
    
    [window setTitle:@"VSV : MacOS log file"];
    [window center];

    ///FrameSize will be same as windowSize
    view = [[MyView alloc] initWithFrame:winRect];
    ///assigning Frame to our window
    [window setContentView:view];

    ///NSWindow method will be implemented by itself
    ///if not use this line then app will not removed from taskbar
    [window setDelegate:self];

    ///its like setFocus() and setForgroundWindow() or MB_TOPMOST
    [window makeKeyAndOrderFront:self];
}

///when application is gets terminated, then applicationWillTerminate() a method of NSApplication is called
///WM_Destroy and applicationWillTerminate() are analogus
/// [NSApp terminate:self] is like WM_QUIT
///  when this line is called [NSApp terminate:self];  ...then applicationWillTerminate() is called ...then it exit Message/run/event loop
-(void)applicationWillTerminate:(NSNotification *)aNotification
{
    ///code

    if(gpFile)
    {
        fprintf(gpFile,"\nProgram terminated Successfully");
        fflush(gpFile);
        fclose(gpFile);
    }
    gpFile=NULL;
}

///NSWindowDelegate method
-(void)windowWillClose:(NSNotification *)aNotification
{
    ///code
    [NSApp terminate:self];
}

///like destructor
-(void)dealloc
{
    ///code
    [view release];
    [window release];
    [super dealloc];
}

@end

//implmentating method for MyView
@implementation MyView
{
    @private ///private variable

}

///id is a object of class
-(id)initWithFrame:(NSRect)frame
{
    ///code
    self = [super initWithFrame:frame];
    if (self)
    {

    }

    return self;
}

//InvalidateRect is similar to dirtyRect
-(void)drawRect:(NSRect)dirtyRect
{
    ///code
    
}

///to shift keyboard and mouse controll from NSwindow to NSview
///NSReponder->NSobject method
-(BOOL)acceptsFirstResponder
{
    ///code
    [[self window] makeFirstResponder:self];

    return YES;
}

-(void)keyDown:(NSEvent*)theEvent
{
    ///code
    int key = [[theEvent characters] characterAtIndex:0];
    switch(key)
    {
        case 27://escape
            [self release];
            [NSApp terminate:self];
            break;
        case 'f':
        case 'F':
            [[self window] toggleFullScreen:self];
            break;
    }
}

-(void)mouseDown:(NSEvent*)theEvent
{
    ///code


}

-(void)rightMouseDown:(NSEvent*)theEvent
{
    ///code

    
}

-(void)otherMouseDown:(NSEvent*)theEvent
{
    ///code
 
}

///like destructor
-(void)dealloc
{
    ///code
    [super dealloc];
}


@end
