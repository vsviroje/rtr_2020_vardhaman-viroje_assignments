#import<Foundation/Foundation.h> ///get foundation header file from foundation framework (similar to standard library like stdio.h)
#import<Cocoa/Cocoa.h>///analogus to window.h/xlib.h
#import<QuartzCore/CVDisplayLink.h>///CVDisplayLink mean core video
#import<OpenGL/gl3.h>///gl.h....shaders were added from 3.2 so gl3
#include"vmath.h"
using namespace vmath;

enum{
    VSV_ATTRIBUTE_POSITION=0,
    VSV_ATTRIBUTE_COLOR,
    VSV_ATTRIBUTE_TEXCOORD,
    VSV_ATTRIBUTE_NORMAL,
};

typedef struct Light {
    vec4 LightAmbient;
    vec4 LightDefuse;
    vec4 LightSpecular;
    vec4 LightPosition;
}LightStruct;

LightStruct light[2] = {
                        {
                            vec4(0.1f,0.1f,0.1f,1.0f),
                            vec4(1.0f,0.0f,0.0f,1.0f),
                            vec4(1.0f,0.0f,0.0f,1.0f),
                            vec4(2.0f,0.0f,0.0f,1.0f)
                        },
                        {
                            vec4(0.1f,0.1f,0.1f,1.0f),
                            vec4(0.0f,0.0f,1.0f,1.0f),
                            vec4(0.0f,0.0f,1.0f,1.0f),
                            vec4(-2.0f,0.0f,0.0f,1.0f)
                        }
                    };

//callback
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef,const CVTimeStamp*,const CVTimeStamp*,CVOptionFlags,CVOptionFlags*,void *);

FILE *gpFile = NULL;

///mkdir -p Window.app/Contents/MacOS
///clang++ -Wno-deprecated-declarations -o Window.app/Contents/MacOS/window Window.mm -framework Cocoa -framework QuartzCore -framework OpenGL

///NeXT STEP (NS)

/// our class AppDelegate is inheritated from NSObject class and it will implement NSApplicationDelegate and NSWindowDelegate interface
/// : is extends and <> are implements
///we will define method later...its forward declaration
@interface AppDelegate:NSObject<NSApplicationDelegate, NSWindowDelegate>
@end

int main(int argc,char* argv[])
{
    ///code
    ///NSAutoreleasePool will hold all alocated memory
    NSAutoreleasePool *pool=[[NSAutoreleasePool alloc] init];
    ///NSApp is inbuild global NS application object
    ///we get to access shared NS Application object
    NSApp =[NSApplication sharedApplication];

    ///setting NSApp method implementation to our AppDelegate object
    [NSApp setDelegate:[[AppDelegate alloc] init] ];

    ///Message/run/event loop
    [NSApp run];

    ///releaseing every obj memory from pool
    [pool release];

    return 0;
}

///
@interface MyOpenGLView:NSOpenGLView
@end

//implmentating method for AppDelegate
@implementation AppDelegate
    {
        @private ///private variable
        NSWindow *window;
        MyOpenGLView *myOpenGLview;
    }

    ///when application is getting started, then applicationDidFinishLaunching() a method of NSApplication is called
    ///WM_Create and applicationDidFinishLaunching() are analogus
    -(void)applicationDidFinishLaunching:(NSNotification *)aNotification
    {
        ///code
        //window.app is same as NSBundle(its for recongnising window.app in foundation.h)
        NSBundle *appBundle = [NSBundle mainBundle];
        ///storing window.app dir path
        NSString *appDirPath = [appBundle bundlePath];

        /*
        eg . if appDirPath is /usr/viroje/Desktop/assignment/02LogFile/window.app
            then stringByDeletingLastPathComponent will perform
        then parentDirPath will be /usr/viroje/Desktop/assignment/02LogFile
        */
        NSString *parentDirPath = [appDirPath stringByDeletingLastPathComponent];

        NSString *logFileNameWithPath = [NSString stringWithFormat:@"%@/Log.txt",parentDirPath];

        const char *pSzLogFileNameWithPath =  [logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];

        gpFile = fopen(pSzLogFileNameWithPath,"w");
        if(gpFile==NULL)
        {
            [self release];
            [NSApp terminate:self];
        }

        fprintf(gpFile,"\nProgram Started Successfully");
        fflush(gpFile);

        ///NSRect is internally CGRect -core graphic ..its c based library
        ///CGPoint/NSPoint-x,y and CGSize/NSSize-width,height
        NSRect winRect = NSMakeRect(0.0,0.0,800.0,600.0);

        ///styleMask is similer to WM_overlappedWindow
        ///backing- how to store window in graphics
        ///defer - to halt showing of window or show window immediately
        window = [[NSWindow alloc] initWithContentRect:winRect
                                                    styleMask:NSWindowStyleMaskTitled |
                                                            NSWindowStyleMaskClosable |
                                                            NSWindowStyleMaskMiniaturizable |
                                                            NSWindowStyleMaskResizable
                                                            backing:NSBackingStoreBuffered
                                                            defer:NO];
        
        [window setTitle:@"OpenGL : Two rotating light on pyramid"];
        [window center];

        ///FrameSize will be same as windowSize
        myOpenGLview = [[MyOpenGLView alloc] initWithFrame:winRect];
        ///assigning Frame to our window
        [window setContentView:myOpenGLview];

        ///NSWindow method will be implemented by itself
        ///if not use this line then app will not removed from taskbar
        [window setDelegate:self];

        ///its like setFocus() and setForgroundWindow() or MB_TOPMOST
        [window makeKeyAndOrderFront:self];
    }

    ///when application is gets terminated, then applicationWillTerminate() a method of NSApplication is called
    ///WM_Destroy and applicationWillTerminate() are analogus
    /// [NSApp terminate:self] is like WM_QUIT
    ///  when this line is called [NSApp terminate:self];  ...then applicationWillTerminate() is called ...then it exit Message/run/event loop
    -(void)applicationWillTerminate:(NSNotification *)aNotification
    {
        ///code

        if(gpFile)
        {
            fprintf(gpFile,"\nProgram terminated Successfully");
            fflush(gpFile);
            fclose(gpFile);
        }
        gpFile=NULL;
    }

    ///NSWindowDelegate method
    -(void)windowWillClose:(NSNotification *)aNotification
    {
        ///code
        [NSApp terminate:self];
    }

    ///like destructor
    -(void)dealloc
    {
        ///code
        [myOpenGLview release];
        [window release];
        [super dealloc];
    }

@end

//implmentating method for MyOpenGLView
@implementation MyOpenGLView
    {
        @private ///private variable
        CVDisplayLinkRef displayLinkRef;

        GLuint gVertexShaderObject;
        GLuint gFragmentShaderObject;
        GLuint gShaderProgramObject;

        GLuint vao_pyramid;
        GLuint vbo_pyramidPosition;
        GLuint vbo_pyramidNormal;

        GLuint modelMatrixUniform;
        GLuint viewMatrixUniform;
        GLuint projectionlMatrixUniform;

        GLuint lightAmbientUniform[2];
        GLuint lightDiffuseUniform[2];
        GLuint lightSpecularUniform[2];
        GLuint lightPositionUniform[2];

        GLuint materialAmbientUniform;
        GLuint materialDiffuseUniform;
        GLuint materialSpecularUniform;
        GLuint materialShininessUniform;

        GLuint LKeyPressedUniform;

        bool bLights;

        mat4 perspectiveProjectionMatrix;

        GLfloat pyramidRotate;

    }

    ///id is a object of class
    -(id)initWithFrame:(NSRect)frame
    {
        ///code
        self = [super initWithFrame:frame];
        if (self)
        {
            //array of NSOpenGLPixelFormatAttribute
            //its like pixelformatdiscriptor/framebufferattribute
            NSOpenGLPixelFormatAttribute attributes[] = {
                                                            NSOpenGLPFAOpenGLProfile,NSOpenGLProfileVersion4_1Core,
                                                            NSOpenGLPFAScreenMask,CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay), ///Display/monitor  id/type
                                                            NSOpenGLPFANoRecovery,/// canceling automatic shifting process from h/w rendering to s/w rendering ,if h/w rendering context not found
                                                            NSOpenGLPFAAccelerated,/// to use h/w rendering context
                                                            NSOpenGLPFAColorSize,24,//RGB
                                                            NSOpenGLPFADepthSize,24,
                                                            NSOpenGLPFAAlphaSize,8,
                                                            NSOpenGLPFADoubleBuffer,
                                                            0
                                                        };
            ///autorelease = release on your own in auto release pool
            ///pixelFormat its like choosePixelFormat
            NSOpenGLPixelFormat *pixelFormat = [[[NSOpenGLPixelFormat alloc] initWithAttributes:attributes] autorelease];
            if (pixelFormat==nil)
            {
                fprintf(gpFile,"\npixelFormat is nil.exited");
                fflush(gpFile);
                [self release];
                [NSApp terminate:self];
            }

            //shareContext = from linux
            NSOpenGLContext *glContext = [[[NSOpenGLContext alloc] initWithFormat:pixelFormat shareContext:nil] autorelease];

            //set pixelformat
            [self setPixelFormat:pixelFormat];

            //set content
            [self setOpenGLContext:glContext];
        }

        return self;
    }

    -(CVReturn)getFrameForTime:(const CVTimeStamp*)outputTime
    {
        ///code
        ///Apple OpenGL supports multi threading
        ///pool in main() is main thread and pool in getFrameForTime() are other thread
        ///this pool is renderings auto release pool
        NSAutoreleasePool *pool=[[NSAutoreleasePool alloc] init];

        ///to draw our shaders
        [self drawView];

        ///releasing renderings auto release pool
        [pool release];

        return(kCVReturnSuccess);
    }

    -(void)prepareOpenGL
    {
        ///code
        [super prepareOpenGL];

        ///Objective c does not add `get` as prefix for getter methods
        ///Objective c does add `set` as prefix for setter methods
        ///it gives opengl context and set to current context
        [[self openGLContext] makeCurrentContext];

        ///Swap interval code below
        ///CP=context parameter
        GLint swapInt = 1;
        [[self openGLContext] setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];

        fprintf(gpFile,"\nOpenGL Vender:%s",glGetString(GL_VENDOR));
        fprintf(gpFile,"\nOpenGL Render:%s",glGetString(GL_RENDERER));
        fprintf(gpFile,"\nOpenGL Version:%s",glGetString(GL_VERSION));
        fprintf(gpFile,"\nOpenGL GLSL Version:%s",glGetString(GL_SHADING_LANGUAGE_VERSION));

        ///****************shader codes start*****************************
        GLint infoLogLength;
        GLint shaderCompileStatus;
        GLint shaderProgramLinkStatus;
        GLchar *szBuffer;
        GLsizei written;
        

        //Shader Code Start--------------------
        gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

        const GLchar* vertexShaderSourceCode =
            "#version 410 core\n"\
            "\n"\
            "in vec4 vPosition;\n"\
            "in vec3 vNormal;\n"\

            "uniform mat4 u_modelMatrix;\n"\
            "uniform mat4 u_viewMatrix;\n"\
            "uniform mat4 u_projectionMatrix;\n"\

            "uniform vec3 u_lightAmbient[2];\n"\
            "uniform vec3 u_lightDiffuse[2];\n"\
            "uniform vec3 u_lightSpecular[2];\n"\
            "uniform vec4 u_lightPosition[2];\n"\

            "uniform vec3 u_materialAmbient;\n"\
            "uniform vec3 u_materialDiffuse;\n"\
            "uniform vec3 u_materialSpecular;\n"\
            "uniform float u_materialShininess;\n"\

            "uniform int u_lKeyPressed;\n"\

            "out vec3 out_phongADS_Light;\n"\

            "void main(void)\n"\
            "{\n"\
                "vec3 temp_light;\n"\
                
                "vec3 ambient[2];\n"\
                "vec3 diffuse[2];\n"\
                "vec3 specular[2];\n"\


                "if (u_lKeyPressed == 1)\n"\
                "{\n"\

                    "vec4 eyeCoord = u_viewMatrix * u_modelMatrix * vPosition;\n"\
                    "vec3 viewVector = normalize( -eyeCoord.xyz);\n"\
                    "vec3 transformedNormal = normalize( mat3(u_viewMatrix * u_modelMatrix) * vNormal );\n"\
                
                    "vec3 lightDirection[2];\n"\
                    "vec3 reflectionVector[2];\n"\

                    "for (int i = 0; i < 2; i++)\n"\
                    "{\n"\

                        "lightDirection[i] = normalize( vec3( u_lightPosition[i] - eyeCoord ) );\n"\

                        "reflectionVector[i] = vec3(reflect( -lightDirection[i], transformedNormal ));\n"\

                        "ambient[i] = u_lightAmbient[i] * u_materialAmbient;\n"\

                        "diffuse[i] = u_lightDiffuse[i] * u_materialDiffuse * max( dot(lightDirection[i] , transformedNormal), 0.0);\n"\

                        "specular[i] = u_lightSpecular[i] * u_materialSpecular * pow( max( dot(reflectionVector[i] , viewVector), 0.0f) , u_materialShininess);\n"\
                    "}\n"\

                "}\n"\

                "if (u_lKeyPressed == 1)\n"\
                "{\n"\
                    "temp_light = (ambient[0]+ambient[1]) + (diffuse[0]+diffuse[1]) + (specular[0]+specular[1]);\n"\
                "}\n"\
                "else\n"\
                "{\n"\

                    "temp_light = vec3(1.0f);\n"\

                "}\n"\
                
                "out_phongADS_Light=temp_light;\n"\

                "gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix * vPosition;\n"\

            "}";

        glShaderSource(gVertexShaderObject,
            1,
            (const GLchar**)&vertexShaderSourceCode,
            NULL);

        glCompileShader(gVertexShaderObject);

        glGetShaderiv(gVertexShaderObject,
            GL_COMPILE_STATUS,
            &shaderCompileStatus);

        if (shaderCompileStatus == GL_FALSE)
        {
            glGetShaderiv(gVertexShaderObject,
                GL_INFO_LOG_LENGTH,
                &infoLogLength);

            if (infoLogLength > 0)
            {
                szBuffer = (GLchar*)malloc(sizeof(GLchar) * infoLogLength);
                if (szBuffer != NULL)
                {
                    glGetShaderInfoLog(gVertexShaderObject,
                        infoLogLength,
                        &written,
                        szBuffer);
                    fprintf(gpFile, "\nERROR:Vertex Shader Object Compilation Failed log : %s", szBuffer);
                    free(szBuffer);
                    [self release];
                [NSApp terminate:self];
                }
            }
        }

        infoLogLength = 0;
        shaderCompileStatus = 0;
        shaderProgramLinkStatus = 0;
        szBuffer = NULL;
        written = 0;

        gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

        const GLchar* fragmentShaderSourceCode =
            "#version 410 core"\
            "\n"\
            "in vec3 out_phongADS_Light;\n"\
            "out vec4 fragColor;"\
            "void main(void)"\
            "{"\
                "fragColor = vec4( out_phongADS_Light, 1.0f );"\
            "}";

        glShaderSource(gFragmentShaderObject,
            1,
            (const GLchar**)&fragmentShaderSourceCode,
            NULL);

        glCompileShader(gFragmentShaderObject);

        glGetShaderiv(gFragmentShaderObject,
            GL_COMPILE_STATUS,
            &shaderCompileStatus);

        if (shaderCompileStatus == GL_FALSE)
        {
            glGetShaderiv(gFragmentShaderObject,
                GL_INFO_LOG_LENGTH,
                &infoLogLength);

            if (infoLogLength > 0)
            {
                szBuffer = (GLchar*)malloc(sizeof(GLchar) * infoLogLength);
                if (szBuffer != NULL)
                {
                    glGetShaderInfoLog(gFragmentShaderObject,
                        infoLogLength,
                        &written,
                        szBuffer);
                    fprintf(gpFile, "\nERROR:Fragment Shader Object Compilation Failed log : %s", szBuffer);
                    free(szBuffer);
                    [self release];
                [NSApp terminate:self];
                }
            }
        }

        infoLogLength = 0;
        shaderCompileStatus = 0;
        shaderProgramLinkStatus = 0;
        szBuffer = NULL;
        written = 0;

        gShaderProgramObject = glCreateProgram();

        glAttachShader(gShaderProgramObject, gVertexShaderObject);
        glAttachShader(gShaderProgramObject, gFragmentShaderObject);

        //Shader Attribute Binding Before Pre Linking of attached Shader
        glBindAttribLocation(gShaderProgramObject,
            VSV_ATTRIBUTE_POSITION,
            "vPosition");
        glBindAttribLocation(gShaderProgramObject,
            VSV_ATTRIBUTE_NORMAL,
            "vNormal");

        glLinkProgram(gShaderProgramObject);

        glGetProgramiv(gShaderProgramObject,
            GL_LINK_STATUS,
            &shaderProgramLinkStatus);

        if (shaderProgramLinkStatus == GL_FALSE)
        {
            glGetProgramiv(gShaderProgramObject,
                GL_INFO_LOG_LENGTH,
                &infoLogLength);
            if (infoLogLength > 0)
            {
                szBuffer = (GLchar*)malloc(sizeof(GLchar) * infoLogLength);

                if (szBuffer != NULL)
                {
                    glGetProgramInfoLog(gShaderProgramObject,
                        infoLogLength,
                        &written,
                        szBuffer);

                    fprintf(gpFile, "\nERROR:Shader program Link failed log : %s", szBuffer);
                    free(szBuffer);
                    [self release];
                [NSApp terminate:self];
                }
            }
        }

        //Shader Code End--------------------


        //Uniform variable Binding after post linking of attached shader
        modelMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_modelMatrix");
        viewMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_viewMatrix");
        projectionlMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_projectionMatrix");

        
        lightAmbientUniform[0] = glGetUniformLocation(gShaderProgramObject, "u_lightAmbient[0]");
        lightDiffuseUniform[0] = glGetUniformLocation(gShaderProgramObject, "u_lightDiffuse[0]");
        lightSpecularUniform[0] = glGetUniformLocation(gShaderProgramObject, "u_lightSpecular[0]");
        lightPositionUniform[0] = glGetUniformLocation(gShaderProgramObject, "u_lightPosition[0]");
        
        lightAmbientUniform[1] = glGetUniformLocation(gShaderProgramObject, "u_lightAmbient[1]");
        lightDiffuseUniform[1] = glGetUniformLocation(gShaderProgramObject, "u_lightDiffuse[1]");
        lightSpecularUniform[1] = glGetUniformLocation(gShaderProgramObject, "u_lightSpecular[1]");
        lightPositionUniform[1] = glGetUniformLocation(gShaderProgramObject, "u_lightPosition[1]");

        materialSpecularUniform = glGetUniformLocation(gShaderProgramObject, "u_materialSpecular");
        materialAmbientUniform = glGetUniformLocation(gShaderProgramObject, "u_materialAmbient");
        materialDiffuseUniform = glGetUniformLocation(gShaderProgramObject, "u_materialDiffuse");
        materialShininessUniform = glGetUniformLocation(gShaderProgramObject, "u_materialShininess");

        LKeyPressedUniform = glGetUniformLocation(gShaderProgramObject, "u_lKeyPressed");

        //VAO and its respective VBO preparation
            const GLfloat pyramidVertices[]=
            {
                0.0f, 1.0f, 0.0f,
                -1.0f, -1.0f, 1.0f,
                1.0f, -1.0f, 1.0f,

                0.0f, 1.0f, 0.0f,
                1.0f, -1.0f, 1.0f,
                1.0f, -1.0f, -1.0f,

                0.0f, 1.0f, 0.0f,
                1.0f, -1.0f, -1.0f,
                -1.0f, -1.0f, -1.0f,

                0.0f, 1.0f, 0.0f,
                -1.0f, -1.0f, -1.0f,
                -1.0f, -1.0f, 1.0f
            };

            const GLfloat pyramidNormals[] =
            {
                0.0f, 0.447214f, 0.894427f,
                0.0f, 0.447214f, 0.894427f,
                0.0f, 0.447214f, 0.894427f,

                0.894427f, 0.447214f, 0.0f,
                0.894427f, 0.447214f, 0.0f,
                0.894427f, 0.447214f, 0.0f,

                0.0f, 0.447214f, -0.894427f,
                0.0f, 0.447214f, -0.894427f,
                0.0f, 0.447214f, -0.894427f,

                -0.894427f, 0.447214f, 0.0f,
                -0.894427f, 0.447214f, 0.0f,
                -0.894427f, 0.447214f, 0.0f
            };

            glGenVertexArrays(1,&vao_pyramid);
            glBindVertexArray(vao_pyramid);

                glGenBuffers(1,&vbo_pyramidPosition);
                glBindBuffer(GL_ARRAY_BUFFER,vbo_pyramidPosition);

                    glBufferData(GL_ARRAY_BUFFER,
                                sizeof(pyramidVertices),
                                pyramidVertices,
                                GL_STATIC_DRAW);

                    glVertexAttribPointer(VSV_ATTRIBUTE_POSITION,
                                        3,
                                        GL_FLOAT,
                                        GL_FALSE,
                                        0,
                                        NULL);

                    glEnableVertexAttribArray(VSV_ATTRIBUTE_POSITION);
                
                glBindBuffer(GL_ARRAY_BUFFER,0);

                glGenBuffers(1, &vbo_pyramidNormal);
                glBindBuffer(GL_ARRAY_BUFFER, vbo_pyramidNormal);

                glBufferData(GL_ARRAY_BUFFER,
                    sizeof(pyramidNormals),
                    pyramidNormals,
                    GL_STATIC_DRAW);

                glVertexAttribPointer(VSV_ATTRIBUTE_NORMAL,
                    3,
                    GL_FLOAT,
                    GL_FALSE,
                    0,
                    NULL);

                glEnableVertexAttribArray(VSV_ATTRIBUTE_NORMAL);

                glBindBuffer(GL_ARRAY_BUFFER, 0);

            glBindVertexArray(0);

        glClearColor(0.0,0.0,0.0f,1.0f);

        glClearDepth(1.0f);
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);

        perspectiveProjectionMatrix=mat4::identity();
    
        ///****************shader codes end*****************************

        ///-----Setup code for DisplayLink and its Callback (its CV and CG related code )----------------------------
        /* CGL-core graphics library
            1.Create DisplayLink
            2.set Callback function for DisplayLink
            3.Convert OpenGLContext to CGL context
            4.Convert OpenGLPixelFormat to CGL PixelFormat
            5.set current displays CGL context and CGL PixelFormat
            6.start DisplayLink
        */

        //1.Create DisplayLink
        CVDisplayLinkCreateWithActiveCGDisplays(&displayLinkRef);
        
        //2.set Callback function for DisplayLink
        //displayLinkContext is self
        CVDisplayLinkSetOutputCallback(displayLinkRef,&MyDisplayLinkCallback,self);
        
        //3.Convert OpenGLContext to CGL context
        CGLContextObj cglContext = (CGLContextObj)[[self openGLContext] CGLContextObj];

        //4.Convert OpenGLPixelFormat to CGL PixelFormat
        CGLPixelFormatObj cglPixelFormat = (CGLPixelFormatObj)[[self pixelFormat] CGLPixelFormatObj];

        // 5.set current displays CGL context and CGL PixelFormat
        CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLinkRef,cglContext,cglPixelFormat);

        //6.start DisplayLink
        CVDisplayLinkStart(displayLinkRef);

    }

    -(void)reshape
    {
        ///code
        [super reshape];

        CGLLockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);

        NSRect rect = [self bounds];

        if (rect.size.height < 0)
        {
            rect.size.height = 1;
        }

        glViewport(0,0,(GLsizei)rect.size.width,(GLsizei)rect.size.height);

        int width = rect.size.width;
        int height = rect.size.height;

           perspectiveProjectionMatrix=vmath::perspective(45.0f,
                                                    (GLfloat)width/(GLfloat)height,
                                                    0.1f,
                                                    100.0f);

        CGLUnlockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);
    }

    //InvalidateRect is similar to dirtyRect
    -(void)drawRect:(NSRect)dirtyRect
    {
        ///code
        ///due to multi threading support for OpenGL
        ///we have to call drawView() at two place to syncronised between main thread and other thread
        [self drawView];
    }

    -(void)drawView
    {
        ///code
        [[self openGLContext] makeCurrentContext];
        CGLLockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);

        //*************shader drawing code start********************
            //code
        GLfloat materialAmbient[] = { 0.1f,0.1f,0.1f,1.0f };
        GLfloat materilaDefuse[] = { 1.0f,1.0f,1.0f,1.0f };
        GLfloat materialSpecular[] = { 1.0f,1.0f,1.0f,1.0f };
        GLfloat materialShininess = 50.0f;

        GLfloat temp[] = {0.0f,0.0f,0.0f,0.0f};
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        glUseProgram(gShaderProgramObject);
            mat4 translateMatrix;
            mat4 rotateMatrix;

            mat4 modelMatrix;
            mat4 viewMatrix = mat4::identity();

            rotateMatrix = vmath::rotate(pyramidRotate, 0.0f, 1.0f, 0.0f);
            translateMatrix = vmath::translate(0.0f, 0.0f, -4.0f);
            modelMatrix = translateMatrix * rotateMatrix;

            glUniformMatrix4fv(modelMatrixUniform,
                1,
                GL_FALSE,
                modelMatrix);

            glUniformMatrix4fv(viewMatrixUniform,
                1,
                GL_FALSE,
                viewMatrix);

            glUniformMatrix4fv(projectionlMatrixUniform,
                1,
                GL_FALSE,
                perspectiveProjectionMatrix);

            if (bLights == true)
            {
                glUniform1i(LKeyPressedUniform, 1);

                for (int i = 0; i < 2; i++)
                {
                    fprintf(gpFile, "\nlight[%d]",i);

                    temp[0] = light[i].LightAmbient[0];
                    temp[1] = light[i].LightAmbient[1];
                    temp[2] = light[i].LightAmbient[2];
                    temp[3] = light[i].LightAmbient[3];
                    glUniform3fv(lightAmbientUniform[i], 1,temp);
                    fprintf(gpFile, "\nlightAmbientUniform[i]:%d\tlight[i].LightAmbient:%f,%f,%f,%f", lightAmbientUniform[i], temp[0],temp[1],temp[2],temp[3]);

                    temp[0] = light[i].LightDefuse[0];
                    temp[1] = light[i].LightDefuse[1];
                    temp[2] = light[i].LightDefuse[2];
                    temp[3] = light[i].LightDefuse[3];
                    glUniform3fv(lightDiffuseUniform[i], 1, temp);
                    fprintf(gpFile, "\nlightDiffuseUniform[i]:%d\tlight[i].lightDiffuseUniform:%f,%f,%f,%f", lightDiffuseUniform[i], temp[0],temp[1],temp[2],temp[3]);

                    temp[0] = light[i].LightSpecular[0];
                    temp[1] = light[i].LightSpecular[1];
                    temp[2] = light[i].LightSpecular[2];
                    temp[3] = light[i].LightSpecular[3];
                    glUniform3fv(lightSpecularUniform[i], 1, temp);
                    fprintf(gpFile, "\nlightSpecularUniform[i]:%d\tlight[i].LightSpecular:%f,%f,%f,%f", lightSpecularUniform[i], temp[0],temp[1],temp[2],temp[3]);

                    temp[0] = light[i].LightPosition[0];
                    temp[1] = light[i].LightPosition[1];
                    temp[2] = light[i].LightPosition[2];
                    temp[3] = light[i].LightPosition[3];
                    glUniform4fv(lightPositionUniform[i], 1,temp);//posiitional light
                    fprintf(gpFile, "\nlightPositionUniform[i]:%d\tlight[i].LightPosition:%f,%f,%f,%f", lightPositionUniform[i], temp[0],temp[1],temp[2],temp[3]);

                }
                
                glUniform3fv(materialAmbientUniform, 1, materialAmbient);
                fprintf(gpFile, "\nmaterialAmbientUniform:%d\tmaterialAmbient:%f,%f,%f,%f", materialAmbientUniform, materialAmbient[0],materialAmbient[1],materialAmbient[2],materialAmbient[3]);

                glUniform3fv(materialDiffuseUniform, 1, materilaDefuse);
                fprintf(gpFile, "\nmaterialDiffuseUniform:%d\tmaterilaDefuse:%f,%f,%f,%f", materialDiffuseUniform, materilaDefuse[0],materilaDefuse[1],materilaDefuse[2],materilaDefuse[3]);

                glUniform3fv(materialSpecularUniform, 1, materialSpecular);
                fprintf(gpFile, "\nmaterialSpecularUniform:%d\tmaterialSpecular:%f,%f,%f,%f", materialSpecularUniform, materialSpecular[0],materialSpecular[1],materialSpecular[2],materialSpecular[3]);

                glUniform1f(materialShininessUniform, materialShininess);
                fprintf(gpFile, "\nmaterialShininessUniform:%d\tmaterialShininess:%f", materialShininessUniform, materialShininess);

            }
            else
            {
                glUniform1i(LKeyPressedUniform, 0);
            }



            glBindVertexArray(vao_pyramid);
                glDrawArrays(GL_TRIANGLES,
                                0,
                                12);
            glBindVertexArray(0);

        glUseProgram(0);

        pyramidRotate+=1.0f;


        //*************shader drawing code end********************

        ///swapbuffer
        CGLFlushDrawable((CGLContextObj)[[self openGLContext] CGLContextObj]);

        CGLUnlockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);
    }

    ///to shift keyboard and mouse controll from NSwindow to NSOpenGLView
    ///NSReponder->NSobject method
    -(BOOL)acceptsFirstResponder
    {
        ///code
        [[self window] makeFirstResponder:self];

        return YES;
    }

    -(void)keyDown:(NSEvent*)theEvent
    {
        ///code
        int key = [[theEvent characters] characterAtIndex:0];
        switch(key)
        {
            case 27://escape
                [self release];
                [NSApp terminate:self];
                break;
            case 'f':
            case 'F':
                [[self window] toggleFullScreen:self];
                break;
            case 'l':
            case 'L':
                bLights = !bLights;
                break;
        }
    }

    -(void)mouseDown:(NSEvent*)theEvent
    {
        ///code


    }

    -(void)rightMouseDown:(NSEvent*)theEvent
    {
        ///code

        
    }

    -(void)otherMouseDown:(NSEvent*)theEvent
    {
        ///code
    
    }

    ///like destructor
    -(void)dealloc
    {
        ///*********Shader dealloc start******************************************************
       

        if(vao_pyramid)
        {
            glDeleteVertexArrays(1,
                                &vao_pyramid);
        }
        vao_pyramid=0;

        if(vbo_pyramidPosition)
        {
            glDeleteBuffers(1,
                        &vbo_pyramidPosition);
        }
        vbo_pyramidPosition=0;


        if(gShaderProgramObject)
        {
            glUseProgram(gShaderProgramObject);

            GLsizei shaderCount;
            GLsizei tempShaderCount;
            GLuint *pShader=NULL;

            glGetProgramiv(gShaderProgramObject,GL_ATTACHED_SHADERS,&shaderCount);

            pShader=(GLuint *)malloc(sizeof(GLuint)*shaderCount);
            if(pShader==NULL)
            {
                fprintf(gpFile,"\nFailed to allocate memory for pShader");
            }else{
                glGetAttachedShaders(gShaderProgramObject,shaderCount,&tempShaderCount,pShader);
                
                fprintf(gpFile,"\nShader program has actual attached shader count is:%d",tempShaderCount);
                
                for(GLsizei i=0;i<shaderCount;i++)
                {
                    glDetachShader(gShaderProgramObject,pShader[i]);
                    glDeleteShader(pShader[i]);
                    pShader[i]=0;
                }
                free(pShader);
                
                glDeleteProgram(gShaderProgramObject);
                gShaderProgramObject=0;
                glUseProgram(0);
            }
        }
        ///*********Shader dealloc end******************************************************

        CVDisplayLinkStop(displayLinkRef);
        CVDisplayLinkRelease(displayLinkRef);
        [super dealloc];
    }


@end


///As Callback function is C style func its should return in global space
///displayLinkContext will be MyOpenGLView obj which will be set by OS,when OS will call this function

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLinkRef,
                                const CVTimeStamp* now,
                                const CVTimeStamp* outputTime,
                                CVOptionFlags flagsIn,
                                CVOptionFlags* flagsOut,
                                void * displayLinkContext)
{
    CVReturn result = [(MyOpenGLView *)displayLinkContext  getFrameForTime:outputTime];
    return result;
}
