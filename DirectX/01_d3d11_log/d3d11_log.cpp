//Geting graphic card name and vram info in logs

#include<stdio.h>
#include<d3d11.h> //its like gl/gl.h
#include<math.h>

#pragma comment(lib,"d3d11.lib")
#pragma comment(lib,"dxgi.lib")// dxgi ie(DirectX graphics infrastructure) ...its infrastucture of abstraction for graphics card driver
								//it abstracts graphic cards drivers low level API.
								//Its analogus to WGL of OpenGL

//Father of interface in DirectX is IUnknown 
//Its like pure abstraction base class
/*
Enables clients to get pointers to other interfaces on a given object through the QueryInterface method, 
and manage the existence of the object through the AddRef and Release methods. All other COM interfaces are inherited, directly or indirectly, from IUnknown. 
Therefore, the three methods in IUnknown are the first entries in the vtable for every interface.
*/

int main(void)
{
	//Factory in COM: Factory provides the interfaces to be implemented
	//Adapter :all d3d11,d3d12,vulkan,metal technology represent physical devices as Adapter
	//Device :all d3d11,d3d12,vulkan,metal technology logical representation of physical devices so called as Device.(like DeviceContext in OpenGL)

	//hardware->dxgi(infrastructure for hw)->Adapter(physical representation from infratructure for hw)->Device(logical representation from physical adapter)

	//variable declaration
	IDXGIFactory* pIDXGIFactory = NULL;//An IDXGIFactory interface implements methods for generating DXGI objects
	IDXGIAdapter* pIDXGIAdapter = NULL;//The IDXGIAdapter interface represents a display subsystem (including one or more GPUs, DACs and video memory)

	DXGI_ADAPTER_DESC dxgiAdapterDesc;//provide description of dxgi adapter
	HRESULT hr;//result handler
	char str[255];

	//code

	//In COM,first we have to initialize the COM (ie CoInitialize()),then get instance of interface (ie CoCreateInstance()) and then after using functionality from interface, we have to release the interface and uninitialize the COM.
	//But in DirectX there is helper function which already has COM initialization and requested interface instance creation.
	
	//DirectX helper function CreateDXGIFactory for COM initialization and requested interface instance creation
	hr = CreateDXGIFactory(__uuidof(IDXGIFactory), //GUID for interface//GUID(globally unique identifier )- unique number in world to be recognize by OS//uuid(universal unique identifier)
							(void**)&pIDXGIFactory); 

	if(FAILED(hr))//Provides a generic test for failure on any status value.
	{
		printf("\nCreateDXGIFactory() failed");
		goto cleanUp;
	}

	//EnumAdapters():get physical device/graphic card/adapter from the list of adapters at specified index
	//EnumAdapters(0,			//index
	//			&pIDXGIAdapter) //to get instance of adapter at specified index

	if (pIDXGIFactory->EnumAdapters(0, &pIDXGIAdapter) == DXGI_ERROR_NOT_FOUND)//adapter not found
	{
		printf("\nEnumAdapters() failed | IDXGIAdapter can not be found");
		goto cleanUp;
	}

	ZeroMemory((void*)&dxgiAdapterDesc, sizeof(DXGI_ADAPTER_DESC));

	//GetDesc():get adapter description 
	hr = pIDXGIAdapter->GetDesc(&dxgiAdapterDesc);
	if (FAILED(hr))//Provides a generic test for failure on any status value.
	{
		printf("\nCreateDXGIFactory() failed");
		goto cleanUp;
	}

	//convert wide char to specified MultiByte 
	WideCharToMultiByte(CP_ACP,	//CP_ACP : code page for ANSI code page//code page for a language 
		0,//method of conversion//0-default
		dxgiAdapterDesc.Description,//specify src for conversion
		255,//len of char to be converted from src
		str,//dest
		255,//len of char of dest
		NULL,//Pointer to the character to use if a character cannot be represented in the specified code page. The application sets this parameter to NULL if the function is to use a system default value.
		NULL);//indicates if the function has used a default character in the conversion

	printf("\nGraphic Card Name | %s",str);
	//I64d:64bit integer in bytes
	printf("\nGraphic Card VRAM | %I64d Bytes | %d GB", (__int64)dxgiAdapterDesc.DedicatedVideoMemory, (int)ceil(dxgiAdapterDesc.DedicatedVideoMemory / 1024.0 / 1024.04 / 1024.04));

	cleanUp:
	
		ZeroMemory((void*)&dxgiAdapterDesc, sizeof(DXGI_ADAPTER_DESC));
	//destruct order clean up
		if (pIDXGIAdapter)
		{
			pIDXGIAdapter->Release();
		}

		if (pIDXGIFactory)
		{
			pIDXGIFactory->Release();
		}
		
	return(0);
}





