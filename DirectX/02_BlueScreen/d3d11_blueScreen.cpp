/*
	Main Server for DirectX are the 2 dll ie d3d11.dll,dxgi.dll and our code will be client application

	//In COM,first we have to initialize the COM (ie CoInitialize()),then get instance of interface (ie CoCreateInstance()) and then after using functionality from interface, we have to release the interface and uninitialize the COM.
	//But in DirectX there is helper function which hides complexity like COM initialization and requested interface instance creation steps.

	DirectX is multi threaded rendering techniq.
	From one DirectX helper function we are going to get the 3 interface which are swapchain,logical device,device context.
	
	swapchain: Muliple buffer are in queue like chain and ready to render like swap the next buffer.
				Creating swapchain is like creating pixelformatdescriptor in OpenGL

				Note:For learning purpose we are going to create 2 buffer for swapchain to simulate double buffer concept in OpenGL.
	
	RenderTargetView: Create targeted view window to render.(like surface view in Android)

	Step 1 : Create Swapchain by filling swapchains descriptor.
	Step 2 : Use Swapchain descriptor structor to get 3 interface ie swapchain,logical device,device context.(need to specify expected DirectX driver,DirectX Feature level )			
	Step 3 : Create RenderTargetView
	Step 4 : Set RenderTargetView into pipeline.
	Step 5 : Set your viewing place in pipeline of RenderTargetView.(like viewport)
	Step 6 : Start display/rendering.(render/game loop)
		step A:Clear RenderTargetView.(like clearcolor)
		step B:Create Frame.
		step C:Inform swapchain to present frame in RenderTargetView.
	Step 7: Safely release the interface 

	If window is resized ,OpenGL resize the buffer by itself.
	But from DirectX 11 ,developer has to explicitly resize the buffer.

	Note:
		RenderTargetView code is written in resize().
		Because If window size is changed,then need to resize the buffer also.
		Unlike OpenGL,in DirectX  explicitly need to resize the buffer.
		Thats why RenderTargetView code is not in initialize().
	
	For log ,flush at every statement.

	DirectX has inbuilt rasterizer called as WARP driver.It less powerfull than actual hardware driver but more powerful reference/software driver.
	WARP-(Windows Advanced Rasterization Presentation)

	UNORM:(unsigned normalized)
*/ 

#include<windows.h>
#include<d3d11.h> 

#include<math.h>
#include<stdio.h>

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

#pragma comment(lib,"user32.lib")
#pragma comment(lib,"gdi32.lib")
#pragma comment(lib,"d3d11.lib")
#pragma comment(lib,"dxgi.lib")

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
FILE* gpFile = NULL;
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
bool gbFullscreen = false;
bool gbActiveWindow = false;
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

IDXGISwapChain* gpIDXGISwapChain = NULL;
ID3D11Device* gpID3D11Device = NULL;
ID3D11DeviceContext* gpID3D11DeviceContext = NULL;
ID3D11RenderTargetView* gpID3D11RenderTargetView = NULL;
float gClearColor[4];

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	HRESULT Initialize(void);
	void Display(void);
	void UnInitialize(void);
	void PrintD3DInfo(void);

	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("Direct3D11");
	int X1, X2, Y1, Y2;
	bool bDone = false;

	HRESULT hr;

	if (fopen_s(&gpFile, "log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Failed to Create log file!"), TEXT("Error"), MB_OK | MB_ICONERROR);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "\nDirect3D 11 Application Started...!");
	}

	X1 = GetSystemMetrics(SM_CXSCREEN) / 2;
	Y1 = GetSystemMetrics(SM_CYSCREEN) / 2;

	X2 = WIN_WIDTH / 2;
	Y2 = WIN_HEIGHT / 2;

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(GRAY_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("D3D11 Blue Screen"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		X1 - X2,//100,
		Y1 - Y2,//100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	ShowWindow(hwnd, iCmdShow);

	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	PrintD3DInfo();

	hr = Initialize();
	if (FAILED(hr))
	{
		fprintf(gpFile, "\nError | WinMain()->Initialize() failed...!");
		fflush(gpFile);
		UnInitialize();
	}

	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{

			if (gbActiveWindow == true)
			{
				Display();
			}
		}
	}

	return ((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void ToggleFullscreen(void);
	HRESULT Resize(int, int);
	void UnInitialize(void);
	HRESULT hr;

	switch (iMsg)
	{
	case WM_CREATE:
		fprintf(gpFile, "\nLog is Started!");
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		case 0x46:
		case 0x66:
			ToggleFullscreen();
			break;
		default:
			break;
		}
		break;
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;
	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;

	case WM_ERASEBKGND:
		return 0;
	case WM_SIZE:
		if (gpID3D11DeviceContext)
		{
			hr=Resize(LOWORD(lParam), HIWORD(lParam));
			if (FAILED(hr))
			{
				fprintf(gpFile, "\nERROR | WndProc()->resize() failed");
				fflush(gpFile);
				return (hr);
			} 
		}
		break;
	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;
	case WM_DESTROY:
		fprintf(gpFile, "\nLog is Ended!");
		UnInitialize();
		PostQuitMessage(0);
		break;
	}
	
	fflush(gpFile);

	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void PrintD3DInfo(void)
{
	//variable declaration
	IDXGIFactory* pIDXGIFactory = NULL;
	IDXGIAdapter* pIDXGIAdapter = NULL;

	DXGI_ADAPTER_DESC dxgiAdapterDesc;
	HRESULT hr;
	char str[255];

	//code
	hr = CreateDXGIFactory(__uuidof(IDXGIFactory),
		(void**)&pIDXGIFactory);

	if (FAILED(hr))
	{
		fprintf(gpFile,"\nError | DisplayGCInfo()->CreateDXGIFactory() failed");
		goto cleanUp;
	}

	if (pIDXGIFactory->EnumAdapters(0, &pIDXGIAdapter) == DXGI_ERROR_NOT_FOUND)//adapter not found
	{
		fprintf(gpFile,"\nError | DisplayGCInfo()->EnumAdapters() failed | IDXGIAdapter can not be found");
		goto cleanUp;
	}

	ZeroMemory((void*)&dxgiAdapterDesc, sizeof(DXGI_ADAPTER_DESC));

	hr = pIDXGIAdapter->GetDesc(&dxgiAdapterDesc);
	if (FAILED(hr))
	{
		fprintf(gpFile,"\nERROR | DisplayGCInfo()->CreateDXGIFactory() failed");
		goto cleanUp;
	}

	WideCharToMultiByte(CP_ACP,	
		0,
		dxgiAdapterDesc.Description,
		255,
		str,
		255,
		NULL,
		NULL);

	fprintf(gpFile,"\nGraphic Card Name | %s", str);
	fprintf(gpFile,"\nGraphic Card VRAM | %I64d Bytes | %d GB", (__int64)dxgiAdapterDesc.DedicatedVideoMemory, (int)ceil(dxgiAdapterDesc.DedicatedVideoMemory / 1024.0 / 1024.04 / 1024.04));

	cleanUp:

		ZeroMemory((void*)&dxgiAdapterDesc, sizeof(DXGI_ADAPTER_DESC));
		//destruct order clean up
		if (pIDXGIAdapter)
		{
			pIDXGIAdapter->Release();
		}

		if (pIDXGIFactory)
		{
			pIDXGIFactory->Release();
		}
	
	fflush(gpFile);

	return;
}


void ToggleFullscreen(void)
{
	MONITORINFO mi = { sizeof(MONITORINFO) };
	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				fprintf(gpFile, "\nFullscreen Enabled");
				SetWindowLong(ghwnd, GWL_STYLE, (dwStyle & ~WS_OVERLAPPEDWINDOW));
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
		gbFullscreen = true;
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, (dwStyle | WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		fprintf(gpFile, "\nNormal Window");
		ShowCursor(TRUE);
		gbFullscreen = false;
	}
	fflush(gpFile);
}

HRESULT Initialize(void)
{
	HRESULT Resize(int, int);
	void UnInitialize(void);

	HRESULT hr;

	D3D_DRIVER_TYPE d3dDriverType;
	D3D_DRIVER_TYPE d3dDriverTypes[] = { D3D_DRIVER_TYPE_HARDWARE,
										D3D_DRIVER_TYPE_WARP,
										D3D_DRIVER_TYPE_REFERENCE };

	D3D_FEATURE_LEVEL d3dFeatureLevel_required = D3D_FEATURE_LEVEL_11_0;
	D3D_FEATURE_LEVEL d3dFeatureLevel_acquired = D3D_FEATURE_LEVEL_11_0;

	UINT createDeviceFlags = 0;
	UINT numDriverTypes = sizeof(d3dDriverTypes) / sizeof(d3dDriverTypes[0]);
	UINT numFeatureLevels = 1;

	DXGI_SWAP_CHAIN_DESC dxgiSwapChainDesc;
	ZeroMemory((void*)&dxgiSwapChainDesc, sizeof(DXGI_SWAP_CHAIN_DESC));

	dxgiSwapChainDesc.BufferCount = 1;
	dxgiSwapChainDesc.BufferDesc.Width = WIN_WIDTH;
	dxgiSwapChainDesc.BufferDesc.Height = WIN_HEIGHT;
	dxgiSwapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	dxgiSwapChainDesc.BufferDesc.RefreshRate.Numerator = 60; //60 FPS
	dxgiSwapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
	dxgiSwapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;//mentioning its a rendering buffer
	dxgiSwapChainDesc.OutputWindow = ghwnd; //show this rendering buffer in specified window handler
	//MSAA-(multi sampling anti aliasing):richness of quality | a technique used in computer graphics to remove jaggies
	dxgiSwapChainDesc.SampleDesc.Count = 1;//number of multisamples per pixel.
	dxgiSwapChainDesc.SampleDesc.Quality = 0;//The image quality level.
	dxgiSwapChainDesc.Windowed = TRUE;//if false it will be fullscreen

	for (UINT i = 0; i < numDriverTypes; i++)
	{
		d3dDriverType = d3dDriverTypes[i];

		//primary use of this helper function is to get 3 interface ie swapChain,logical device,device context
		hr = D3D11CreateDeviceAndSwapChain(
			NULL, //set adapter to default
			d3dDriverType, //Driver Type
			NULL, // set a handle to a DLL that implements a custom software rasterizer. 
			createDeviceFlags, // to specify which type of debugging needs to enable.
			&d3dFeatureLevel_required,//specify required feature level array
			numFeatureLevels,//number of Feature Levels in array
			D3D11_SDK_VERSION,// specify sdk version
			&dxgiSwapChainDesc, //specify Swap Chain Desc
			&gpIDXGISwapChain,//to get instance of SwapChain interface
			&gpID3D11Device,//to get instance of device interface
			&d3dFeatureLevel_acquired, //to get acquired feature level
			&gpID3D11DeviceContext//to get instance of device context interface
		);

		if (SUCCEEDED(hr))
		{
			break;
		}
	}

	if (FAILED(hr))
	{
		fprintf(gpFile, "\nError | Initialize()->D3D11CreateDeviceAndSwapChain() failed");
		fflush(gpFile);
		return(hr);
	}
	else {
		fprintf(gpFile, "\nInfo | Initialize()->D3D11CreateDeviceAndSwapChain() SUCCEEDED");
		
		fprintf(gpFile, "\nThe chosen Driver is of type ");
		if (d3dDriverType == D3D_DRIVER_TYPE_HARDWARE)
		{
			fprintf(gpFile, "Hardware.\n");
		}
		else if (d3dDriverType == D3D_DRIVER_TYPE_WARP)
		{
			fprintf(gpFile, "WARP.\n");
		}
		else if (d3dDriverType == D3D_DRIVER_TYPE_REFERENCE)
		{
			fprintf(gpFile, "Reference.\n");
		}
		else
		{
			fprintf(gpFile, "Unkown.\n");
		}
		fflush(gpFile);

		fprintf(gpFile, "\nThe Supported Highest Feature level is ");
		if (d3dFeatureLevel_acquired == D3D_FEATURE_LEVEL_11_0)
		{
			fprintf(gpFile, "11.0.\n");
		}
		else if (d3dFeatureLevel_acquired == D3D_FEATURE_LEVEL_10_1)
		{
			fprintf(gpFile, "10.1.\n");
		}
		else if (d3dFeatureLevel_acquired == D3D_FEATURE_LEVEL_10_0)
		{
			fprintf(gpFile, "10.0.\n");
		}
		else {
			fprintf(gpFile, "Unkown.\n");
		}
		fflush(gpFile);
	}

	gClearColor[0] = 0.0f;
	gClearColor[1] = 0.0f;
	gClearColor[2] = 1.0f;
	gClearColor[3] = 1.0f;

	//Warmup resize call
	hr=Resize(WIN_WIDTH, WIN_HEIGHT);
	if (FAILED(hr))
	{
		fprintf(gpFile, "\nError | Initialize()->Resize() failed");
		fflush(gpFile);
		return(hr);
	}

	return(S_OK);
}

HRESULT Resize(int width, int height)
{
	/*
		Resize() is called to update render target view with new width and height.
		step 1:release previous render target view because new width and height of window required new render target view.
		step 2:resize the buffers in swapchain
		step 3:Get a texture buffer from swapchain because texture has color and depth in it
		step 4:Create new render target view based on that buffer
		step 5:release the buffer
		step 6:set render target view to output merger stage
		step 7:describe viewport with new width and height and set viewport to rasterizer stage
	*/

	//code
	HRESULT hr = S_OK;
	ID3D11Texture2D* pID3D11Texture2D_BackBuffer;
	D3D11_VIEWPORT d3dViewPort;

	//free any size-dependant resource
	if (gpID3D11RenderTargetView)
	{
		gpID3D11RenderTargetView->Release();
		gpID3D11RenderTargetView = NULL;
	}

	//Changes the swap chain's back buffer size, format, and number of buffers. This should be called when the application window is resized.
	gpIDXGISwapChain->ResizeBuffers(1,//specify count of buffer to be resized
									width,
									height,
									DXGI_FORMAT_R8G8B8A8_UNORM,
									0);//flag to specifies options for swap-chain behavior.

	//Accesses one of the swap-chain's back buffers.
	gpIDXGISwapChain->GetBuffer(0,//index in swapchain buffers
								__uuidof(ID3D11Texture2D),//The type of interface used to manipulate the buffer.
								(LPVOID*)&pID3D11Texture2D_BackBuffer);

	//Creates a render-target view for accessing resource data.
	hr = gpID3D11Device->CreateRenderTargetView(pID3D11Texture2D_BackBuffer,//resource to refer render target from
												NULL,//set RenderTargetView through desc//null means get a default
												&gpID3D11RenderTargetView);

	pID3D11Texture2D_BackBuffer->Release();
	pID3D11Texture2D_BackBuffer = NULL;

	if (FAILED(hr))
	{
		fprintf(gpFile, "\nError | Resize()->CreateRenderTargetView() failed");
		fflush(gpFile);
		return(hr);
	}

	//Bind one or more render targets atomically and the depth-stencil buffer to the output-merger stage.
	gpID3D11DeviceContext->OMSetRenderTargets(1, //Number of render targets to bind 
											&gpID3D11RenderTargetView, //bind color RenderTargetView
											NULL);//bind depth stencil view

	d3dViewPort.TopLeftX = 0;
	d3dViewPort.TopLeftY = 0;
	d3dViewPort.Width = (float)width;
	d3dViewPort.Height = (float)height;
	d3dViewPort.MinDepth = 0.0f;//Minimum depth of the viewport. 
	d3dViewPort.MaxDepth = 1.0f;//Maximum depth of the viewport. 

	//Bind an array of viewports to the rasterizer stage of the pipeline.
	gpID3D11DeviceContext->RSSetViewports(1, //Number of viewports to bind.
										&d3dViewPort);//bind ViewPort

	return (hr);
}


void Display(void)
{
	//code
	gpID3D11DeviceContext->ClearRenderTargetView(gpID3D11RenderTargetView, gClearColor);



	//Presents a rendered image to the user.
	//switch between front and back buffers
	gpIDXGISwapChain->Present(0,//pecifies how to synchronize presentation of a frame with the vertical blank.
								0);//contains swap-chain presentation options

}

void UnInitialize(void)
{
	if (gbFullscreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, (dwStyle | WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

	if (gpID3D11RenderTargetView)
	{
		gpID3D11RenderTargetView->Release();
		gpID3D11RenderTargetView = NULL;
	}

	if (gpIDXGISwapChain)
	{
		gpIDXGISwapChain->Release();
		gpIDXGISwapChain = NULL;
	}

	if (gpID3D11DeviceContext)
	{
		gpID3D11DeviceContext->Release();
		gpID3D11DeviceContext = NULL;
	}

	if (gpID3D11Device)
	{
		gpID3D11Device->Release();
		gpID3D11Device = NULL;
	}

	if (gpFile)
	{
		fprintf(gpFile, "\nDirect3D 11 Application ended...!");
		fclose(gpFile);
		gpFile = NULL;
	}
}

// cl /EHsc file.cpp
// file.exe