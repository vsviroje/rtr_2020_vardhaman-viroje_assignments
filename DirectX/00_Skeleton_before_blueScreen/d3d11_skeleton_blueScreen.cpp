/*
	Main Server for DirectX are the 2 dll ie d3d11.dll,dxgi.dll and our code will be client application

	//In COM,first we have to initialize the COM (ie CoInitialize()),then get instance of interface (ie CoCreateInstance()) and then after using functionality from interface, we have to release the interface and uninitialize the COM.
	//But in DirectX there is helper function which hides complexity like COM initialization and requested interface instance creation steps.

	DirectX is multi threaded rendering techniq.
	From ` DirectX helper function we are going to get the 3 interface which are swapchain,logical device,device context.
	
	swapchain: Muliple buffer are in queue like chain and ready to render like swap the next buffer.
				Creating swapchain is like creating pixelformatdescriptor in OpenGL

				Note:For learning purpose we are going to create 2 buffer for swapchain to simulate double buffer concept in OpenGL.
	
	RenderTargetView: Create targeted view window to render.(like surface view in Android)

	Step 1 : Create Swapchain by filling swapchains descriptor.
	Step 2 : Use Swapchain descriptor structor to get 3 interface ie swapchain,logical device,device context.(need to specify expected DirectX driver,DirectX Feature level )			
	Step 3 : Create RenderTargetView
	Step 4 : Set RenderTargetView into pipeline.
	Step 5 : Set your viewing place in pipeline of RenderTargetView.(like viewport)
	Step 6 : Start display/rendering.(render/game loop)
		step A:Clear RenderTargetView.(like clearcolor)
		step B:Create Frame.
		step C:Inform swapchain to present frame in RenderTargetView.
	Step 7: Safely release the interface 

	If window is resized ,OpenGL resize the buffer by itself.
	But from DirectX 11 ,developer has to explicitly resize the buffer.

	Note:
		RenderTargetView code is written in resize().
		Because If window size is changed,then need to resize the buffer also.
		Unlike OpenGL,in DirectX  explicitly need to resize the buffer.
		Thats why RenderTargetView code is not in initialize().
	
	For log ,flush at every statement.



*/ 

#include<windows.h>
#include<d3d11.h> //its like gl/gl.h

#include<math.h>
#include<stdio.h>

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

#pragma comment(lib,"d3d11.lib")
#pragma comment(lib,"dxgi.lib")// dxgi ie(DirectX graphics infrastructure) ...its infrastucture of abstraction for graphics card driver
								//it abstracts graphic cards drivers low level API.
								//Its analogus to WGL of OpenGL

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
FILE* gpFile = NULL;
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
bool gbFullscreen = false;
bool gbActiveWindow = false;
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	void Initialize(void);
	void Display(void);

	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("D3D11_BlueScreen");
	int X1, X2, Y1, Y2;
	bool bDone = false;

	if (fopen_s(&gpFile, "log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Failed to Create log file!"), TEXT("Error"), MB_OK | MB_ICONERROR);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "\nDirect3D 11 Application Started...!");
	}

	X1 = GetSystemMetrics(SM_CXSCREEN) / 2;
	Y1 = GetSystemMetrics(SM_CYSCREEN) / 2;

	X2 = WIN_WIDTH / 2;
	Y2 = WIN_HEIGHT / 2;

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(GRAY_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("D3D11 Blue Screen"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		X1 - X2,//100,
		Y1 - Y2,//100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;
	Initialize();

	ShowWindow(hwnd, iCmdShow);

	SetForegroundWindow(hwnd);
	SetFocus(hwnd);



	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{

			if (gbActiveWindow == true)
			{
				Display();
			}
		}
	}

	return ((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void ToggleFullscreen(void);
	void Resize(int, int);
	void UnInitialize(void);

	switch (iMsg)
	{
	case WM_CREATE:
		fprintf(gpFile, "\nLog is Started!");
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		case 0x46:
		case 0x66:
			ToggleFullscreen();
			break;
		default:
			break;
		}
		break;
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;
	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;

	case WM_ERASEBKGND:
		return 0;
	case WM_SIZE:
		Resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;
	case WM_DESTROY:
		fprintf(gpFile, "\nLog is Ended!");
		UnInitialize();
		PostQuitMessage(0);
		break;
	}
	
	fflush(gpFile);

	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void DisplayGCInfo(void)
{
	//Factory in COM: Factory provides the interfaces to be implemented
	//Adapter :all d3d11,d3d12,vulkan,metal technology represent physical devices as Adapter
	//Device :all d3d11,d3d12,vulkan,metal technology logical representation of physical devices so called as Device.(like DeviceContext in OpenGL)

	//hardware->dxgi(infrastructure for hw)->Adapter(physical representation from infratructure for hw)->Device(logical representation from physical adapter)

	//variable declaration
	IDXGIFactory* pIDXGIFactory = NULL;//An IDXGIFactory interface implements methods for generating DXGI objects
	IDXGIAdapter* pIDXGIAdapter = NULL;//The IDXGIAdapter interface represents a display subsystem (including one or more GPUs, DACs and video memory)

	DXGI_ADAPTER_DESC dxgiAdapterDesc;//provide description of dxgi adapter
	HRESULT hr;//result handler
	char str[255];

	//code

	//In COM,first we have to initialize the COM (ie CoInitialize()),then get instance of interface (ie CoCreateInstance()) and then after using functionality from interface, we have to release the interface and uninitialize the COM.
	//But in DirectX there is helper function which already has COM initialization and requested interface instance creation.

	//DirectX helper function CreateDXGIFactory for COM initialization and requested interface instance creation
	hr = CreateDXGIFactory(__uuidof(IDXGIFactory), //GUID for interface//GUID(globally unique identifier )- unique number in world to be recognize by OS//uuid(universal unique identifier)
		(void**)&pIDXGIFactory);

	if (FAILED(hr))//Provides a generic test for failure on any status value.
	{
		fprintf(gpFile,"\nCreateDXGIFactory() failed");
		goto cleanUp;
	}

	//EnumAdapters():get physical device/graphic card/adapter from the list of adapters at specified index
	//EnumAdapters(0,			//index
	//			&pIDXGIAdapter) //to get instance of adapter at specified index

	if (pIDXGIFactory->EnumAdapters(0, &pIDXGIAdapter) == DXGI_ERROR_NOT_FOUND)//adapter not found
	{
		fprintf(gpFile,"\nEnumAdapters() failed | IDXGIAdapter can not be found");
		goto cleanUp;
	}

	ZeroMemory((void*)&dxgiAdapterDesc, sizeof(DXGI_ADAPTER_DESC));

	//GetDesc():get adapter description 
	hr = pIDXGIAdapter->GetDesc(&dxgiAdapterDesc);
	if (FAILED(hr))//Provides a generic test for failure on any status value.
	{
		fprintf(gpFile,"\nGetDesc() failed");
		goto cleanUp;
	}

	//convert wide char to specified MultiByte 
	WideCharToMultiByte(CP_ACP,	//CP_ACP : code page for ANSI code page//code page for a language 
		0,//method of conversion//0-default
		dxgiAdapterDesc.Description,//specify src for conversion
		255,//len of char to be converted from src
		str,//dest
		255,//len of char of dest
		NULL,//Pointer to the character to use if a character cannot be represented in the specified code page. The application sets this parameter to NULL if the function is to use a system default value.
		NULL);//indicates if the function has used a default character in the conversion

	fprintf(gpFile,"\nGraphic Card Name | %s", str);
	//I64d:64bit integer in bytes
	fprintf(gpFile,"\nGraphic Card VRAM | %I64d Bytes | %d GB", (__int64)dxgiAdapterDesc.DedicatedVideoMemory, (int)ceil(dxgiAdapterDesc.DedicatedVideoMemory / 1024.0 / 1024.04 / 1024.04));

	fflush(gpFile);
	cleanUp:

		ZeroMemory((void*)&dxgiAdapterDesc, sizeof(DXGI_ADAPTER_DESC));
		//destruct order clean up
		if (pIDXGIAdapter)
		{
			pIDXGIAdapter->Release();
		}

		if (pIDXGIFactory)
		{
			pIDXGIFactory->Release();
		}

	return(void);
}


void ToggleFullscreen(void)
{
	MONITORINFO mi = { sizeof(MONITORINFO) };
	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				fprintf(gpFile, "\nFullscreen Enabled");
				SetWindowLong(ghwnd, GWL_STYLE, (dwStyle & ~WS_OVERLAPPEDWINDOW));
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
		gbFullscreen = true;
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, (dwStyle | WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		fprintf(gpFile, "\nNormal Window");
		ShowCursor(TRUE);
		gbFullscreen = false;
	}
}

void Initialize(void)
{
	void Resize(int, int);



	//Warmup resize call
	Resize(WIN_WIDTH, WIN_HEIGHT);
}

void Resize(int width, int height)
{
	
}


void Display(void)
{
	//code
	

}
void UnInitialize(void)
{
	if (gbFullscreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, (dwStyle | WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}



	if (gpFile)
	{
		fprintf(gpFile, "\nDirect3D 11 Application ended...!");
		fclose(gpFile);
		gpFile = NULL;
	}
}






















