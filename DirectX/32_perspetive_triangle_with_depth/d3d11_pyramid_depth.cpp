/*
	In DirectX,Back face culling is by default on.

	Depth is size dependent.

	//just like need for rtv dsv also needs a texture buffer
	//We need to define the depth stencil for resize.
	//We dont define D3D11_TEXTURE2D_DESC for rtv because we are not creating the rtv by specifying config,we are creating rtv based on getting swap-chain's back buffers which alredy has the rtv config in it.
	//Swap chain handles resize of buffer and we are getting the back buffer from swap chain

	steps to create depth
	1:declaration global depthstensilview
	2:In resize()
		A:deinitialization of previous DSV
		B:initialization of D3D11_TEXTURE2D_DESC variable
		c:create texture2d as depth buffer using D3D11_TEXTURE2D_DESC variable
		d:initialize D3D11_DEPTH_STENCIL_VIEW_DESC variable
		e:using  D3D11_DEPTH_STENCIL_VIEW_DESC variable and depth buffer create DSV
		f:In OM stage set DSV along with RTV

*/ 

#include<windows.h>

#include<d3d11.h> 
#include<d3dcompiler.h>
#pragma warning(disable:4838)//to supress the waring related converting unsigned int to signed int.
#include"XNAMath\xnamath.h"

#include<math.h>
#include<stdio.h>

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

#pragma comment(lib,"user32.lib")
#pragma comment(lib,"gdi32.lib")

#pragma comment(lib,"d3d11.lib")
#pragma comment(lib,"dxgi.lib")
#pragma comment(lib,"d3dcompiler.lib")

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
FILE* gpFile = NULL;
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
bool gbFullscreen = false;
bool gbActiveWindow = false;
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

IDXGISwapChain* gpIDXGISwapChain = NULL;
ID3D11Device* gpID3D11Device = NULL;
ID3D11DeviceContext* gpID3D11DeviceContext = NULL;
ID3D11RenderTargetView* gpID3D11RenderTargetView = NULL;
ID3D11DepthStencilView* gpID3D11DepthStencilView = NULL;
float gClearColor[4];

ID3D11VertexShader* gpID3D11VertexShader = NULL;
ID3D11PixelShader* gpID3D11PixelShader = NULL;
ID3D11Buffer* gpID3D11Buffer_VertexBuffer_position = NULL;
ID3D11Buffer* gpID3D11Buffer_VertexBuffer_color = NULL;
ID3D11Buffer* gpID3D11Buffer_ConstantBuffer = NULL;
ID3D11RasterizerState* gpID3D11RasterizerState = NULL;
ID3D11InputLayout* gpID3D11InputLayout = NULL;

struct CBUFFER
{
	XMMATRIX WorldViewProjectionMatrix;
};

XMMATRIX gPerspectiveProjectionMatrix;

float angle=0.0f;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	HRESULT Initialize(void);
	void Display(void);
	void UnInitialize(void);
	void PrintD3DInfo(void);

	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("Direct3D11");
	int X1, X2, Y1, Y2;
	bool bDone = false;

	HRESULT hr;

	if (fopen_s(&gpFile, "log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Failed to Create log file!"), TEXT("Error"), MB_OK | MB_ICONERROR);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "\nDirect3D 11 Application Started...!");
	}

	X1 = GetSystemMetrics(SM_CXSCREEN) / 2;
	Y1 = GetSystemMetrics(SM_CYSCREEN) / 2;

	X2 = WIN_WIDTH / 2;
	Y2 = WIN_HEIGHT / 2;

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(GRAY_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("D3D11 Perspetive Triangle with Depth"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		X1 - X2,//100,
		Y1 - Y2,//100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	ShowWindow(hwnd, iCmdShow);

	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	PrintD3DInfo();

	hr = Initialize();
	if (FAILED(hr))
	{
		fprintf(gpFile, "\nError | WinMain()->Initialize() failed...!");
		fflush(gpFile);
		UnInitialize();
	}

	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{

			if (gbActiveWindow == true)
			{
				Display();
			}
		}
	}

	return ((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void ToggleFullscreen(void);
	HRESULT Resize(int, int);
	void UnInitialize(void);
	HRESULT hr;

	switch (iMsg)
	{
	case WM_CREATE:
		fprintf(gpFile, "\nLog is Started!");
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		case 0x46:
		case 0x66:
			ToggleFullscreen();
			break;
		default:
			break;
		}
		break;
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;
	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;

	case WM_ERASEBKGND:
		return 0;
	case WM_SIZE:
		if (gpID3D11DeviceContext)
		{
			hr=Resize(LOWORD(lParam), HIWORD(lParam));
			if (FAILED(hr))
			{
				fprintf(gpFile, "\nERROR | WndProc()->resize() failed");
				fflush(gpFile);
				return (hr);
			} 
		}
		break;
	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;
	case WM_DESTROY:
		fprintf(gpFile, "\nLog is Ended!");
		UnInitialize();
		PostQuitMessage(0);
		break;
	}
	
	fflush(gpFile);

	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void PrintD3DInfo(void)
{
	//variable declaration
	IDXGIFactory* pIDXGIFactory = NULL;
	IDXGIAdapter* pIDXGIAdapter = NULL;

	DXGI_ADAPTER_DESC dxgiAdapterDesc;
	HRESULT hr;
	char str[255];

	//code
	hr = CreateDXGIFactory(__uuidof(IDXGIFactory),
		(void**)&pIDXGIFactory);

	if (FAILED(hr))
	{
		fprintf(gpFile,"\nError | DisplayGCInfo()->CreateDXGIFactory() failed");
		goto cleanUp;
	}

	if (pIDXGIFactory->EnumAdapters(0, &pIDXGIAdapter) == DXGI_ERROR_NOT_FOUND)//adapter not found
	{
		fprintf(gpFile,"\nError | DisplayGCInfo()->EnumAdapters() failed | IDXGIAdapter can not be found");
		goto cleanUp;
	}

	ZeroMemory((void*)&dxgiAdapterDesc, sizeof(DXGI_ADAPTER_DESC));

	hr = pIDXGIAdapter->GetDesc(&dxgiAdapterDesc);
	if (FAILED(hr))
	{
		fprintf(gpFile,"\nERROR | DisplayGCInfo()->CreateDXGIFactory() failed");
		goto cleanUp;
	}

	WideCharToMultiByte(CP_ACP,	
		0,
		dxgiAdapterDesc.Description,
		255,
		str,
		255,
		NULL,
		NULL);

	fprintf(gpFile,"\nGraphic Card Name | %s", str);
	fprintf(gpFile,"\nGraphic Card VRAM | %I64d Bytes | %d GB", (__int64)dxgiAdapterDesc.DedicatedVideoMemory, (int)ceil(dxgiAdapterDesc.DedicatedVideoMemory / 1024.0 / 1024.04 / 1024.04));

	cleanUp:

		ZeroMemory((void*)&dxgiAdapterDesc, sizeof(DXGI_ADAPTER_DESC));
		//destruct order clean up
		if (pIDXGIAdapter)
		{
			pIDXGIAdapter->Release();
		}

		if (pIDXGIFactory)
		{
			pIDXGIFactory->Release();
		}
	
	fflush(gpFile);

	return;
}


void ToggleFullscreen(void)
{
	MONITORINFO mi = { sizeof(MONITORINFO) };
	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				fprintf(gpFile, "\nFullscreen Enabled");
				SetWindowLong(ghwnd, GWL_STYLE, (dwStyle & ~WS_OVERLAPPEDWINDOW));
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
		gbFullscreen = true;
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, (dwStyle | WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		fprintf(gpFile, "\nNormal Window");
		ShowCursor(TRUE);
		gbFullscreen = false;
	}
	fflush(gpFile);
}

HRESULT Initialize(void)
{
	HRESULT Resize(int, int);
	void UnInitialize(void);

	HRESULT hr;

	D3D_DRIVER_TYPE d3dDriverType;
	D3D_DRIVER_TYPE d3dDriverTypes[] = { D3D_DRIVER_TYPE_HARDWARE,
										D3D_DRIVER_TYPE_WARP,
										D3D_DRIVER_TYPE_REFERENCE };

	D3D_FEATURE_LEVEL d3dFeatureLevel_required = D3D_FEATURE_LEVEL_11_0;
	D3D_FEATURE_LEVEL d3dFeatureLevel_acquired = D3D_FEATURE_LEVEL_11_0;

	UINT createDeviceFlags = 0;
	UINT numDriverTypes = sizeof(d3dDriverTypes) / sizeof(d3dDriverTypes[0]);
	UINT numFeatureLevels = 1;

	DXGI_SWAP_CHAIN_DESC dxgiSwapChainDesc;
	ZeroMemory((void*)&dxgiSwapChainDesc, sizeof(DXGI_SWAP_CHAIN_DESC));

	dxgiSwapChainDesc.BufferCount = 1;
	dxgiSwapChainDesc.BufferDesc.Width = WIN_WIDTH;
	dxgiSwapChainDesc.BufferDesc.Height = WIN_HEIGHT;
	dxgiSwapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	dxgiSwapChainDesc.BufferDesc.RefreshRate.Numerator = 60; //60 FPS
	dxgiSwapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
	dxgiSwapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;//mentioning its a rendering buffer
	dxgiSwapChainDesc.OutputWindow = ghwnd; //show this rendering buffer in specified window handler
	//MSAA-(multi sampling anti aliasing):richness of quality | a technique used in computer graphics to remove jaggies
	dxgiSwapChainDesc.SampleDesc.Count = 1;//number of multisamples per pixel.
	dxgiSwapChainDesc.SampleDesc.Quality = 0;//The image quality level.
	dxgiSwapChainDesc.Windowed = TRUE;//if false it will be fullscreen

	for (UINT i = 0; i < numDriverTypes; i++)
	{
		d3dDriverType = d3dDriverTypes[i];

		//primary use of this helper function is to get 3 interface ie swapChain,logical device,device context
		hr = D3D11CreateDeviceAndSwapChain(
			NULL, //set adapter to default
			d3dDriverType, //Driver Type
			NULL, // set a handle to a DLL that implements a custom software rasterizer. 
			createDeviceFlags, // to specify which type of debugging needs to enable.
			&d3dFeatureLevel_required,//specify required feature level array
			numFeatureLevels,//number of Feature Levels in array
			D3D11_SDK_VERSION,// specify sdk version
			&dxgiSwapChainDesc, //specify Swap Chain Desc
			&gpIDXGISwapChain,//to get instance of SwapChain interface
			&gpID3D11Device,//to get instance of device interface
			&d3dFeatureLevel_acquired, //to get acquired feature level
			&gpID3D11DeviceContext//to get instance of device context interface
		);

		if (SUCCEEDED(hr))
		{
			break;
		}
	}

	if (FAILED(hr))
	{
		fprintf(gpFile, "\nError | Initialize()->D3D11CreateDeviceAndSwapChain() failed");
		fflush(gpFile);
		return(hr);
	}
	else {
		fprintf(gpFile, "\nThe chosen Driver is of type ");
		if (d3dDriverType == D3D_DRIVER_TYPE_HARDWARE)
		{
			fprintf(gpFile, "Hardware.");
		}
		else if (d3dDriverType == D3D_DRIVER_TYPE_WARP)
		{
			fprintf(gpFile, "WARP.");
		}
		else if (d3dDriverType == D3D_DRIVER_TYPE_REFERENCE)
		{
			fprintf(gpFile, "Reference.");
		}
		else
		{
			fprintf(gpFile, "Unkown.");
		}
		fflush(gpFile);

		fprintf(gpFile, "\nThe Supported Highest Feature level is ");
		if (d3dFeatureLevel_acquired == D3D_FEATURE_LEVEL_11_0)
		{
			fprintf(gpFile, "11.0.");
		}
		else if (d3dFeatureLevel_acquired == D3D_FEATURE_LEVEL_10_1)
		{
			fprintf(gpFile, "10.1.");
		}
		else if (d3dFeatureLevel_acquired == D3D_FEATURE_LEVEL_10_0)
		{
			fprintf(gpFile, "10.0.");
		}
		else {
			fprintf(gpFile, "Unkown.");
		}
		fflush(gpFile);
	}

	const char* vertexShaderSrc =
		"cbuffer ConstantBuffer" \
		"{"\
			"float4x4 worldViewProjectionMatrix;"\
		"}"\

		"struct vertex_output"\
		"{"\
			"float4 position : SV_POSITION;"\
			"float4 color : COLOR;"\
		"};"\

		/*	
			col-color
		*/
		"vertex_output main(float4 pos : POSITION,float4 col : COLOR)"\
		"{"\
			"vertex_output output;"\
			"output.position = mul(worldViewProjectionMatrix, pos);"\
			"output.color = col;"\
			"return(output);"\
		"}";

	ID3DBlob* pID3DBlob_VertexShaderCode = NULL;//to hold binary of compiled Vertex Shader src
	ID3DBlob* pID3DBlob_Error = NULL;//to hold error if any of compiled Vertex Shader src

	hr = D3DCompile(vertexShaderSrc, // src for shader code
		lstrlenA(vertexShaderSrc) + 1, //length of shader code + \n
		"VS",//Id in string to specify a type of compilation.
		NULL,//An optional array of D3D_SHADER_MACRO structures that define shader macros.
		D3D_COMPILE_STANDARD_FILE_INCLUDE,// handling include standard files.
		"main",//The name of the shader entry point function where shader execution begins.
		"vs_5_0",//A string that specifies the shader target or set of shader features to compile against. 
		0,//define flags for different compilation operation ie debugging,optimisation,row major,column major,etc
		0,//Flags defined by D3D compile effect constants,When you compile a shader and not an effect file, D3DCompile ignores Flags2; we recommend that you set Flags2 to zero because it is good programming practice to set a nonpointer parameter to zero if the called function will not use it.
		&pID3DBlob_VertexShaderCode,//to access the compiled code.
		&pID3DBlob_Error);// to access compiler error

	if (FAILED(hr))
	{
		if (pID3DBlob_Error != NULL)
		{
			fprintf(gpFile, "\nError | Initialize()->D3DCompile() failed for vertex shader | %s", (char*)pID3DBlob_Error->GetBufferPointer());
			fflush(gpFile);

			pID3DBlob_Error->Release();
			pID3DBlob_Error = NULL;
		}
		else {
			fprintf(gpFile, "\nError | Initialize()->D3DCompile() failed for vertex shader | some COM error");
			fflush(gpFile);
		}

		return(hr);
	}

	hr = gpID3D11Device->CreateVertexShader(pID3DBlob_VertexShaderCode->GetBufferPointer(),//set buffer of vertex shader
											pID3DBlob_VertexShaderCode->GetBufferSize(),//set size of buffer
											NULL,// to set a class linkage interface
											&gpID3D11VertexShader);//to get instance of created vertex shader interface

	if (FAILED(hr))
	{
		fprintf(gpFile, "\nError | Initialize()->CreateVertexShader() failed ");
		fflush(gpFile);
		return(hr);
	}

	gpID3D11DeviceContext->VSSetShader(gpID3D11VertexShader,
										NULL, //to set array of class instance
										0);//num of class instance in array

	const char* pixelShaderSrc =
		/*
			shader variable SV_TARGET its like fragColor in OpenGL.
			final output is mentioned as target from pixel shader
		*/
		"struct vertex_output"\
		"{"\
			"float4 position : SV_POSITION;"\
			"float4 color : COLOR;"\
		"};"\

		"float4 main(vertex_output input) : SV_TARGET"\
		"{"\
			"return(input.color);"\
		"}";

	ID3DBlob * pID3DBlob_PixelShaderCode = NULL;
	pID3DBlob_Error = NULL;

	hr = D3DCompile(pixelShaderSrc, // src for shader code
					lstrlenA(pixelShaderSrc) + 1, //length of shader code + \n
					"PS",//Id in string to specify a type of compilation.
					NULL,//An optional array of D3D_SHADER_MACRO structures that define shader macros.
					D3D_COMPILE_STANDARD_FILE_INCLUDE,// handling include standard files.
					"main",//The name of the shader entry point function where shader execution begins.
					"ps_5_0",//A string that specifies the shader target or set of shader features to compile against. 
					0,//define flags for different compilation operation ie debugging,optimisation,row major,column major,etc
					0,//Flags defined by D3D compile effect constants,When you compile a shader and not an effect file, D3DCompile ignores Flags2; we recommend that you set Flags2 to zero because it is good programming practice to set a nonpointer parameter to zero if the called function will not use it.
					&pID3DBlob_PixelShaderCode,//to access the compiled code.
					&pID3DBlob_Error);// to access compiler error

	if (FAILED(hr))
	{
		if (pID3DBlob_Error != NULL)
		{
			fprintf(gpFile, "\nError | Initialize()->D3DCompile() failed for pixel shader | %s",(char *)pID3DBlob_Error->GetBufferPointer());
			fflush(gpFile);

			pID3DBlob_Error->Release();
			pID3DBlob_Error = NULL;
		}
		else {
			fprintf(gpFile, "\nError | Initialize()->D3DCompile() failed for pixel shader | some COM error");
			fflush(gpFile);
		}

		return(hr);
	}

	hr = gpID3D11Device->CreatePixelShader(pID3DBlob_PixelShaderCode->GetBufferPointer(),//set buffer of vertex shader
											pID3DBlob_PixelShaderCode->GetBufferSize(),//set size of buffer
											NULL,// to set a class linkage interface
											&gpID3D11PixelShader);//to get instance of created vertex shader interface


	pID3DBlob_PixelShaderCode->Release();
	pID3DBlob_PixelShaderCode = NULL;

	if (FAILED(hr))
	{
		fprintf(gpFile, "\nError | Initialize()->CreatePixelShader() failed ");
		fflush(gpFile);
		return(hr);
	}

	gpID3D11DeviceContext->PSSetShader(gpID3D11PixelShader,
										NULL, //to set array of class instance
										0);//num of class instance in array


	D3D11_INPUT_ELEMENT_DESC d3d11InputElementDesc[2];

	d3d11InputElementDesc[0].SemanticName = "POSITION";//The HLSL semantic associated with this element in a shader input-signature.
	d3d11InputElementDesc[0].SemanticIndex = 0; //A semantic index is only needed in a case where there is more than one element with the same semantic
	d3d11InputElementDesc[0].Format = DXGI_FORMAT_R32G32B32_FLOAT;
	d3d11InputElementDesc[0].InputSlot = 0;//An integer value that identifies the input-assembler (see input slot). Valid values are between 0 and 15, defined in D3D11.h.
	d3d11InputElementDesc[0].AlignedByteOffset= 0;//Offset (in bytes) from the start of the vertex
	d3d11InputElementDesc[0].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;//Identifies the input data class for a single input slot //to specify its pervertex or instance data
	d3d11InputElementDesc[0].InstanceDataStepRate = 0;//The number of instances to draw using the same per-instance data before advancing in the buffer by one element. This value must be 0 for an element that contains per-vertex data 

	d3d11InputElementDesc[1].SemanticName = "COLOR";//The HLSL semantic associated with this element in a shader input-signature.
	d3d11InputElementDesc[1].SemanticIndex = 0; //A semantic index is only needed in a case where there is more than one element with the same semantic
	d3d11InputElementDesc[1].Format = DXGI_FORMAT_R32G32B32_FLOAT;
	d3d11InputElementDesc[1].InputSlot = 1;//An integer value that identifies the input-assembler (see input slot). Valid values are between 0 and 15, defined in D3D11.h.
	d3d11InputElementDesc[1].AlignedByteOffset = 0;//Offset (in bytes) from the start of the vertex
	d3d11InputElementDesc[1].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;//Identifies the input data class for a single input slot //to specify its pervertex or instance data
	d3d11InputElementDesc[1].InstanceDataStepRate = 0;//The number of instances to draw using the same per-instance data before advancing in the buffer by one element. This value must be 0 for an element that contains per-vertex data 


	hr = gpID3D11Device->CreateInputLayout(d3d11InputElementDesc,//address of InputElementDesc
											_ARRAYSIZE(d3d11InputElementDesc),//number of InputElementDesc
											pID3DBlob_VertexShaderCode->GetBufferPointer(),
											pID3DBlob_VertexShaderCode->GetBufferSize(),
											&gpID3D11InputLayout);

	pID3DBlob_VertexShaderCode->Release();
	pID3DBlob_VertexShaderCode = NULL;

	if (FAILED(hr))
	{
		fprintf(gpFile, "\nError | Initialize()->CreateInputLayout() failed ");
		fflush(gpFile);
		return(hr);
	}

	gpID3D11DeviceContext->IASetInputLayout(gpID3D11InputLayout);

	//OpenGL - anti clock wise and In DirectX is clock wise
	float vertices[] =
	{
		0.0f,1.0f,0.0f,
		1.0f,-1.0f,0.0f,
		-1.0f,-1.0f,0.0f
	};

	float colors[] =
	{
		+1.0f, +1.0f, +1.0f,
		+1.0f, +1.0f, +1.0f,
		+1.0f, +1.0f, +1.0f,
	};

	D3D11_BUFFER_DESC bufferDesc;
	ZeroMemory((void*)&bufferDesc, sizeof(D3D11_BUFFER_DESC));

	bufferDesc.ByteWidth = sizeof(float) * _ARRAYSIZE(vertices);
	bufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER; //like glBindBuffer()//Identify how the buffer will be bound to the pipeline.
	bufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;//define cpu access type
	bufferDesc.Usage = D3D11_USAGE_DYNAMIC;//Identify how the buffer is expected to be read from and written to.

	hr = gpID3D11Device->CreateBuffer(&bufferDesc,
										NULL,//If data is static data//we are not sending static data but we are using Map(),memcpy(),Unmap() dynamic way to vertex buffer.
										&gpID3D11Buffer_VertexBuffer_position);

	if (FAILED(hr))
	{
		fprintf(gpFile, "\nError | Initialize()->CreateBuffer() VertexBuffer position failed ");
		fflush(gpFile);
		return(hr);
	}

	//Indirect pushing of data.
	D3D11_MAPPED_SUBRESOURCE mappedSubresource;
	ZeroMemory((void*)&mappedSubresource, sizeof(D3D11_MAPPED_SUBRESOURCE));

	//just point/map two interface
	gpID3D11DeviceContext->Map(gpID3D11Buffer_VertexBuffer_position, //(point A)resource interface to be mapped
								0,//Index number in buffer for mapping
								D3D11_MAP_WRITE_DISCARD,//specifies the CPU's read and write 
								0,//specifies what the CPU does when the GPU is busy(0-push or wait)
								&mappedSubresource);//(point B)mappedSubresource to be mapped with
	
													//transfer the vertices data in mappedSubresource
	memcpy(mappedSubresource.pData,vertices,sizeof(vertices));
	
	//unmap the interface
	gpID3D11DeviceContext->Unmap(gpID3D11Buffer_VertexBuffer_position,
								0);//Index number in buffer for unmapping


	ZeroMemory((void*)&bufferDesc, sizeof(D3D11_BUFFER_DESC));

	bufferDesc.ByteWidth = sizeof(float) * _ARRAYSIZE(colors);
	bufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER; //like glBindBuffer()//Identify how the buffer will be bound to the pipeline.
	bufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;//define cpu access type
	bufferDesc.Usage = D3D11_USAGE_DYNAMIC;//Identify how the buffer is expected to be read from and written to.

	hr = gpID3D11Device->CreateBuffer(&bufferDesc,
		NULL,//If data is static data//we are not sending static data but we are using Map(),memcpy(),Unmap() dynamic way to vertex buffer.
		&gpID3D11Buffer_VertexBuffer_color);

	if (FAILED(hr))
	{
		fprintf(gpFile, "\nError | Initialize()->CreateBuffer() VertexBuffer color failed ");
		fflush(gpFile);
		return(hr);
	}

	//Indirect pushing of data.
	ZeroMemory((void*)&mappedSubresource, sizeof(D3D11_MAPPED_SUBRESOURCE));

	//just point/map two interface
	gpID3D11DeviceContext->Map(gpID3D11Buffer_VertexBuffer_color, //(point A)resource interface to be mapped
		0,//Index number in buffer for mapping
		D3D11_MAP_WRITE_DISCARD,//specifies the CPU's read and write 
		0,//specifies what the CPU does when the GPU is busy(0-push or wait)
		&mappedSubresource);//(point B)mappedSubresource to be mapped with

							//transfer the vertices data in mappedSubresource
	memcpy(mappedSubresource.pData, colors, sizeof(colors));

	//unmap the interface
	gpID3D11DeviceContext->Unmap(gpID3D11Buffer_VertexBuffer_color,
		0);//Index number in buffer for unmapping

	//VertexBuffer is going to set in pipeline in display()

	//constant
	ZeroMemory((void*)&bufferDesc, sizeof(D3D11_BUFFER_DESC));

	bufferDesc.ByteWidth = sizeof(CBUFFER);
	bufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER; //like glBindBuffer()//Identify how the buffer will be bound to the pipeline.
	bufferDesc.CPUAccessFlags = 0;
	bufferDesc.Usage = D3D11_USAGE_DEFAULT;//Identify how the buffer is expected to be read from and written to.

	hr = gpID3D11Device->CreateBuffer(&bufferDesc,
		NULL,//If data is static data//we are not sending static data but we are using Map(),memcpy(),Unmap() dynamic way to vertex buffer.
		&gpID3D11Buffer_ConstantBuffer);

	if (FAILED(hr))
	{
		fprintf(gpFile, "\nError | Initialize()->CreateBuffer() ConstantBuffer failed ");
		fflush(gpFile);
		return(hr);
	}

	gpID3D11DeviceContext->VSSetConstantBuffers(0,//start of index
												1,//number of buffers
												&gpID3D11Buffer_ConstantBuffer);



	//create and set rasterizer state
	D3D11_RASTERIZER_DESC d3d11RastizerDesc;
	ZeroMemory((void*)&d3d11RastizerDesc, sizeof(D3D11_RASTERIZER_DESC));

	d3d11RastizerDesc.AntialiasedLineEnable = FALSE;
	d3d11RastizerDesc.CullMode = D3D11_CULL_NONE;
	d3d11RastizerDesc.DepthBias = 0;
	d3d11RastizerDesc.DepthBiasClamp = 0.0f;
	d3d11RastizerDesc.DepthClipEnable = TRUE;
	d3d11RastizerDesc.FillMode = D3D11_FILL_SOLID; // polygon mode fill,line(WIREFRAME)
	d3d11RastizerDesc.FrontCounterClockwise = FALSE;
	d3d11RastizerDesc.MultisampleEnable = FALSE;
	d3d11RastizerDesc.ScissorEnable = FALSE;
	d3d11RastizerDesc.SlopeScaledDepthBias = 0.0f;

	hr = gpID3D11Device->CreateRasterizerState(&d3d11RastizerDesc,
												&gpID3D11RasterizerState);

	if (FAILED(hr))
	{
		fprintf(gpFile, "\nError | Initialize()->CreateRasterizerState() RasterizerState failed ");
		fflush(gpFile);
		return(hr);
	}

	gpID3D11DeviceContext->RSSetState(gpID3D11RasterizerState);


	gClearColor[0] = 0.0f;
	gClearColor[1] = 0.0f;
	gClearColor[2] = 0.0f;
	gClearColor[3] = 1.0f;

	gPerspectiveProjectionMatrix = XMMatrixIdentity();

	//Warmup resize call
	hr=Resize(WIN_WIDTH, WIN_HEIGHT);
	if (FAILED(hr))
	{
		fprintf(gpFile, "\nError | Initialize()->Resize() failed");
		fflush(gpFile);
		return(hr);
	}

	return(S_OK);
}

HRESULT Resize(int width, int height)
{
	/*
		Resize() is called to update render target view with new width and height.
		step 1:release previous render target view because new width and height of window required new render target view.
		step 2:resize the buffers in swapchain
		step 3:Get a texture buffer from swapchain because texture has color and depth in it
		step 4:Create new render target view based on that buffer
		step 5:release the buffer
		step 6:set render target view to output merger stage
		step 7:describe viewport with new width and height and set viewport to rasterizer stage
	*/

	//code
	HRESULT hr = S_OK;
	ID3D11Texture2D* pID3D11Texture2D_BackBuffer;
	D3D11_VIEWPORT d3dViewPort;

	//free any size-dependant resource
	if (gpID3D11RenderTargetView)
	{
		gpID3D11RenderTargetView->Release();
		gpID3D11RenderTargetView = NULL;
	}

	//free any size-dependant resource
	if (gpID3D11DepthStencilView)
	{
		gpID3D11DepthStencilView->Release();
		gpID3D11DepthStencilView = NULL;
	}

	//Changes the swap chain's back buffer size, format, and number of buffers. This should be called when the application window is resized.
	gpIDXGISwapChain->ResizeBuffers(1,//specify count of buffer to be resized
		width,
		height,
		DXGI_FORMAT_R8G8B8A8_UNORM,
		0);//flag to specifies options for swap-chain behavior.

//Accesses one of the swap-chain's back buffers.
	gpIDXGISwapChain->GetBuffer(0,//index in swapchain buffers
		__uuidof(ID3D11Texture2D),//The type of interface used to manipulate the buffer.
		(LPVOID*)&pID3D11Texture2D_BackBuffer);

	//Creates a render-target view for accessing resource data.
	hr = gpID3D11Device->CreateRenderTargetView(pID3D11Texture2D_BackBuffer,//resource to refer render target from
		NULL,//set RenderTargetView through desc//null means get a default
		&gpID3D11RenderTargetView);

	pID3D11Texture2D_BackBuffer->Release();
	pID3D11Texture2D_BackBuffer = NULL;

	if (FAILED(hr))
	{
		fprintf(gpFile, "\nError | Resize()->CreateRenderTargetView() failed");
		fflush(gpFile);
		return(hr);
	}

	//just like need for rtv dsp also needs a texture buffer
	//We need to define the depth stencil.
	//We dont define D3D11_TEXTURE2D_DESC for rtv because we are not creating the rtv by specifying config,we are creating rtv based on getting swap-chain's back buffers which alredy has the rtv config in it.

	D3D11_TEXTURE2D_DESC d3d11Texture2DDesc;
	ZeroMemory((void*)&d3d11Texture2DDesc, sizeof(D3D11_TEXTURE2D_DESC));
	d3d11Texture2DDesc.Width = (UINT)width;
	d3d11Texture2DDesc.Height = (UINT)height;
	d3d11Texture2DDesc.Format = DXGI_FORMAT_D32_FLOAT;
	d3d11Texture2DDesc.Usage = D3D11_USAGE_DEFAULT;
	d3d11Texture2DDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	d3d11Texture2DDesc.SampleDesc.Quality = 0;
	d3d11Texture2DDesc.SampleDesc.Count = 1; //this can be 1 to 4
	d3d11Texture2DDesc.ArraySize = 1;
	d3d11Texture2DDesc.MipLevels = 1;
	d3d11Texture2DDesc.CPUAccessFlags = 0;
	d3d11Texture2DDesc.MiscFlags = 0;

	ID3D11Texture2D* pID3D11Texture2D_depthBuffer = NULL;
	hr = gpID3D11Device->CreateTexture2D(&d3d11Texture2DDesc,
										NULL,//sub resource,it is used if you have data right here
										&pID3D11Texture2D_depthBuffer);

	if (FAILED(hr))
	{
		fprintf(gpFile, "\nError | Resize()->CreateTexture2D() depthBuffer failed");
		fflush(gpFile);
		return(hr);
	}

	D3D11_DEPTH_STENCIL_VIEW_DESC d3d11DepthStencilViewDesc;
	ZeroMemory((void*)&d3d11DepthStencilViewDesc, sizeof(D3D11_DEPTH_STENCIL_VIEW_DESC));

	d3d11DepthStencilViewDesc.Format= DXGI_FORMAT_D32_FLOAT;
	d3d11DepthStencilViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2DMS;//ms-multi sampling

	hr = gpID3D11Device->CreateDepthStencilView(pID3D11Texture2D_depthBuffer, 
												&d3d11DepthStencilViewDesc, 
												&gpID3D11DepthStencilView);
	if (FAILED(hr))
	{
		fprintf(gpFile, "\nError | Resize()->CreateDepthStencilView() failed");
		fflush(gpFile);
		return(hr);
	}


	//Bind one or more render targets atomically and the depth-stencil buffer to the output-merger stage.
	gpID3D11DeviceContext->OMSetRenderTargets(1, //Number of render targets to bind 
											&gpID3D11RenderTargetView, //bind color RenderTargetView
											gpID3D11DepthStencilView);//bind depth stencil view

	d3dViewPort.TopLeftX = 0;
	d3dViewPort.TopLeftY = 0;
	d3dViewPort.Width = (float)width;
	d3dViewPort.Height = (float)height;
	d3dViewPort.MinDepth = 0.0f;//Minimum depth of the viewport. 
	d3dViewPort.MaxDepth = 1.0f;//Maximum depth of the viewport. 

	//Bind an array of viewports to the rasterizer stage of the pipeline.
	gpID3D11DeviceContext->RSSetViewports(1, //Number of viewports to bind.
		&d3dViewPort);//bind ViewPort

	//field of view left handed.
	gPerspectiveProjectionMatrix = XMMatrixPerspectiveFovLH(XMConvertToRadians(45.0f),
															(float)width/(float)height,
															0.1f,
															100.0f);

	return (hr);
}


void Display(void)
{
	//code
	gpID3D11DeviceContext->ClearRenderTargetView(gpID3D11RenderTargetView, gClearColor);
	gpID3D11DeviceContext->ClearDepthStencilView(gpID3D11DepthStencilView, 
												D3D11_CLEAR_DEPTH,
												1.0f,//glCleardepth(1.0f)
												0);//clear stencil


	UINT stride = sizeof(float) * 3;//mentioning jumping points in array
	UINT offset = 0;//mentioning index to start from

	gpID3D11DeviceContext->IASetVertexBuffers(0,//entioning index to start from buffer arrays
											1,//len of  buffer arrays
											&gpID3D11Buffer_VertexBuffer_position,
											&stride,
											&offset);
	
	stride = sizeof(float) * 3;//mentioning jumping points in array
	offset = 0;//mentioning index to start from

	gpID3D11DeviceContext->IASetVertexBuffers(1,//The first input slot for binding. The first vertex buffer is explicitly bound to the start slot; this causes each additional vertex buffer in the array to be implicitly bound to each subsequent input slot. 
		1,//len of  buffer arrays
		&gpID3D11Buffer_VertexBuffer_color,
		&stride,
		&offset);

	gpID3D11DeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);//mention Primitive Topology

	XMMATRIX translationMatrix = XMMatrixTranslation(0.0f, 0.0f, 4.0f);
	XMMATRIX rotationMatrix = XMMatrixRotationY(angle);

	XMMATRIX worldMatrix =  rotationMatrix * translationMatrix;
	XMMATRIX viewMatrix = XMMatrixIdentity();

		// worldMatrix * viewMatrix * ProjectionMatrix; -->this is way for directX
	XMMATRIX wvpMatrix = worldMatrix * viewMatrix * gPerspectiveProjectionMatrix;

	CBUFFER constantBuffer;
	constantBuffer.WorldViewProjectionMatrix = wvpMatrix;

	//UpdateSubresource() its glUniformMatrix4fv();
	//The CPU copies data from memory to a subresource created in non-mappable memory.
	gpID3D11DeviceContext->UpdateSubresource(gpID3D11Buffer_ConstantBuffer, //dest resource 
											0,//mentioning index to start from
											NULL, //bounding box
											&constantBuffer, //source data in memory.
											0, // size of one row of the source data.
											0);// size of one depth slice of source data.

	//Draw() is like glDrawArray()
	gpID3D11DeviceContext->Draw(12, //number of vertex
								0);//mentioning index to start from

	//Presents a rendered image to the user.
	//switch between front and back buffers
	gpIDXGISwapChain->Present(0,//pecifies how to synchronize presentation of a frame with the vertical blank.
								0);//contains swap-chain presentation options


	angle += 0.0001f;
}

void UnInitialize(void)
{
	if (gbFullscreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, (dwStyle | WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

	//destructive order


	if (gpID3D11RasterizerState)
	{
		gpID3D11RasterizerState->Release();
		gpID3D11RasterizerState = NULL;
	}


	if (gpID3D11Buffer_ConstantBuffer)
	{
		gpID3D11Buffer_ConstantBuffer->Release();
		gpID3D11Buffer_ConstantBuffer = NULL;
	}

	if (gpID3D11InputLayout)
	{
		gpID3D11InputLayout->Release();
		gpID3D11InputLayout = NULL;
	}

	if (gpID3D11Buffer_VertexBuffer_position)
	{
		gpID3D11Buffer_VertexBuffer_position->Release();
		gpID3D11Buffer_VertexBuffer_position = NULL;
	}

	if (gpID3D11Buffer_VertexBuffer_color)
	{
		gpID3D11Buffer_VertexBuffer_color->Release();
		gpID3D11Buffer_VertexBuffer_color = NULL;
	}

	if (gpID3D11PixelShader)
	{
		gpID3D11PixelShader->Release();
		gpID3D11PixelShader = NULL;
	}

	if (gpID3D11VertexShader)
	{
		gpID3D11VertexShader->Release();
		gpID3D11VertexShader = NULL;
	}

	if (gpID3D11DepthStencilView)
	{
		gpID3D11DepthStencilView->Release();
		gpID3D11DepthStencilView = NULL;
	}

	if (gpID3D11RenderTargetView)
	{
		gpID3D11RenderTargetView->Release();
		gpID3D11RenderTargetView = NULL;
	}

	if (gpIDXGISwapChain)
	{
		gpIDXGISwapChain->Release();
		gpIDXGISwapChain = NULL;
	}

	if (gpID3D11DeviceContext)
	{
		gpID3D11DeviceContext->Release();
		gpID3D11DeviceContext = NULL;
	}

	if (gpID3D11Device)
	{
		gpID3D11Device->Release();
		gpID3D11Device = NULL;
	}

	if (gpFile)
	{
		fprintf(gpFile, "\nDirect3D 11 Application ended...!");
		fclose(gpFile);
		gpFile = NULL;
	}
}

// cl /EHsc file.cpp
// file.exe